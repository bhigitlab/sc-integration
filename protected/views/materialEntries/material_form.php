<?php
$tblpx = Yii::app()->db->tablePrefix;
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
$newQuery1 = "";
$newQuery2 = "";
$newQuery3 = "";
$newQuery4 = "";
foreach ($arrVal as $arr) {
    if ($newQuery) $newQuery .= ' OR';
    if ($newQuery1) $newQuery1 .= ' OR';
    if ($newQuery2) $newQuery2 .= ' OR';
    if ($newQuery3) $newQuery3 .= ' OR';
    if ($newQuery4) $newQuery4 .= ' OR';
    $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
    $newQuery1 .= " FIND_IN_SET('" . $arr . "', id)";
    $newQuery2 .= " FIND_IN_SET('" . $arr . "', b.company_id)";
    $newQuery3 .= " FIND_IN_SET('" . $arr . "', i.company_id)";
    $newQuery4 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "purchase_return.company_id)";
}

$billsDatasql = "SELECT b.bill_id as billid, b.bill_number as billnumber FROM " . $tblpx . "bills b LEFT JOIN " . $tblpx . "purchase p ON b.purchase_id = p.p_id WHERE (" . $newQuery2 . ") AND b.bill_totalamount IS NOT NULL AND b.purchase_id IS NOT NULL AND b.bill_id NOT IN (Select bill_id from " . $tblpx . "expenses WHERE bill_id IS NOT NULL) GROUP BY b.bill_id  ORDER BY b.bill_id DESC";
$billsData = Yii::app()->db->createCommand($billsDatasql)->queryAll();

$invoicesDatasql = "SELECT i.invoice_id, i.inv_no as invoice_no FROM " . $tblpx . "invoice i LEFT JOIN " . $tblpx . "projects p ON i.project_id = p.pid WHERE (" . $newQuery3 . ") AND i.amount >0 AND i.invoice_id NOT IN(Select invoice_id from " . $tblpx . "expenses WHERE invoice_id IS NOT NULL) GROUP BY i.invoice_id ORDER BY i.invoice_id DESC";
$invoicesData = Yii::app()->db->createCommand($invoicesDatasql)->queryAll();

$purchasereturnDatasql = "SELECT * FROM " . $tblpx . "purchase_return LEFT JOIN " . $tblpx . "bills ON " . $tblpx . "purchase_return.bill_id=" . $tblpx . "bills.bill_id WHERE " . $tblpx . "purchase_return.return_totalamount >0 AND (" . $newQuery4 . ") AND  " . $tblpx . "purchase_return.return_id NOT IN(Select return_id from " . $tblpx . "expenses WHERE return_id IS NOT NULL) GROUP BY " . $tblpx . "purchase_return.return_id ORDER BY " . $tblpx . "purchase_return.return_id";
$purchasereturnData = Yii::app()->db->createCommand($purchasereturnDatasql)->queryAll();

$worktype = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "daily_work_type")->queryAll();
?>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>

<style type="text/css">
    .select2-selection.select2-selection--single {
        min-width: 100px;
    }

    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .btn.addentries:focus,
    .btn.addentries:hover {
        border: 1px solid transparent;
        box-shadow: none;
    }

    .avoid_clicks {
        pointer-events: none;
    }

    .amntblock,
    .expvalue {
        display: none;
    }

    .deleteclass {
        background-color: #efca92;
    }

    .filter_elem.links_hold {
        margin-top: 6px;
    }

    .mr-10 {
        margin-right: 10px;
        ;
    }
    .add-btn,.remove-btn{
        text-align: right;
        margin-top: 23px;
        margin-right:2px;
        font-weight: bolder;
        border-radius:2px;
    }
    .hint{
        float:right;
        font-size:12px !important;
    }

</style>
<div class="container" id="expense">

    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="expenses-heading">
        <div class="clearfix">
           
           
                <button type="button" class="btn btn-info pull-right addentries collapsed" data-toggle="collapse" data-target="#daybookform"></button>
           
            <?php echo CHtml::link(CHtml::encode("Entries"), array('admin'), array('class' => 'btn btn-info pull-right addentries mr-10', 'target' => '_blank')); ?>
            <h2>Material ENTRIES</h2>
            <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>

        </div>
    </div>
    <div class="daybook form">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'material-form',
            'enableAjaxValidation' => false,
        ));
        ?>
        <div class="clearfix">
            <div class="datepicker">
                <div class="page_filter clearfix">
                    <div class="filter_elem">
                        <label>Entry Date:</label>
                        <?php echo CHtml::activeTextField($model, 'date', array('class' => 'form-control', 'readonly' => true, 'autocomplete' => 'off', 'id' => 'MaterialEntries_expense_date', 'value' => date('d-m-Y'), 'onchange' => 'changedate()')); ?>
                    </div>
                    <div class="filter_elem links_hold">
                        <a href="#" id="previous" class="link">Previous</a> |
                        <a href="#" id="current" class="link">Current</a> |
                        <a href="#" id="next" class="link">Next</a>
                    </div>
                    <?php
                    $tblpx = Yii::app()->db->tablePrefix;
                    $expenseData = Yii::app()->db->createCommand("select MAX(date) as entry_date FROM " . $tblpx . "expenses")->queryRow();
                    ?>
                    <div class="pull-right mt">
                        <span id="lastentry">
                            Last Entry date :
                            <span id="entrydate">
                                <?php echo (isset($lastdate['date']) ? date("d-M-Y", strtotime($lastdate['date'])) : ''); ?>
                            </span>
                        </span>
                    </div>
                </div>
            </div>
        </div>


        <div class="block_hold shdow_box clearfix collapse custom-form-style" id="daybookform">
            <div>
                <h4 class="section_title">Details</h4>
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="form-group">
                            <label for="project">Company</label>
                            <?php
                            echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array(
                                'select' => array('id, name'),
                                'order' => 'name ASC',
                                'condition' => '(' . $newQuery1 . ')',
                                'distinct' => true,
                            )), 'id', 'name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Company-', 'style' => 'width:100%;'));
                            ?>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="form-group">
                            <label for="project">Project</label>
                            <?php
                            echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll(array(
                                'select' => array('pid, name'),
                                'order' => 'name ASC',
                                'condition' => '(' . $newQuery . ')',
                                'distinct' => true,
                            )), 'pid', 'name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Project-', 'style' => 'width:100%;'));
                            ?>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="form-group">
                            <label for="vendor">Subcontractor</label>
                            <select class="form-control js-example-basic-single" name="MaterialEntries[subcontractor_id]" id="MaterialEntries_subcontractor_id" style="width:100%;">
                                <option value="">-Select Subcontractor-</option>
                            </select>
                            <input type="hidden" name="bill_vendor" id="bill_vendor" value="">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="form-group">
                            <label for="vendor">Expense Head</label>
                            <select class="form-control js-example-basic-single" name="MaterialEntries[expensehead_id]" id="MaterialEntries_expensehead_id" style="width:100%;">
                                <option value="">-Select Expense Head-</option>
                            </select>
                            <input type="hidden" name="bill_vendor" id="bill_vendor" value="">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="project">Description</label>
                            <?php echo $form->textArea($model, 'description', array('value' => $model['description'], 'rows' => 6, 'cols' => 50, 'class' => "form-control")); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <h4 class="section_title">Material Details</h4>
                <div class="row" id="target-row">
                    <div class="col-md-2">
                        <div class="form-group">
                            
                            <?php echo $form->labelEx($MaterialItems, 'material'); ?>
                            <?php
                                $materialData = CHtml::listData(Materials::model()->findAll(), 'id', 'material_name');
                                echo $form->dropDownList($MaterialItems, 'material[]', $materialData, array(
                                    'class' => 'form-control js-example-basic-single',
                                    'empty' => '-- Select Material --',
                                    'style' => 'width:100%;'
                                ));
                            ?>
                            <?php echo $form->error($MaterialItems, 'material'); ?>
                            <div class="errorMessageequipments" style="display:none;color:red;"></div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <?php echo $form->labelEx($MaterialItems, 'quantity'); ?>
                            <?php echo $form->textField($MaterialItems, 'quantity[]', array('class' => 'form-control','oninput' => 'this.value = this.value.replace(/[^0-9]/g, "");')); ?>
                            <?php echo $form->error($MaterialItems, 'quantity'); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <?php echo $form->labelEx($MaterialItems, 'unit'); ?>
                            <?php
                                $unitData = CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name');
                                echo $form->dropDownList($MaterialItems, 'unit[]', $unitData, array(
                                    'class' => 'form-control',
                                    'empty' => '-- Select Unit --'
                                ));
                            ?>
                            <?php echo $form->error($MaterialItems, 'unit'); ?>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <button type="button" id="add-more-button"  class="add-btn btn btn-info btn-sm" onClick="addFormField();">+</button>
                        </div>
                    </div>
                </div>
                <div id="add_more_item"></div>
            </div>
            <input type="hidden" name="MaterialEntries[dr_id]" value="" id="txtmaterialEntriesId" />

            <div class="form-group submit-button text-right">
                <button type="button" class="btn btn-info" id="buttonsubmit">ADD</button>
                <button type="button" class="btn btn-warning" id="btnReset">RESET</button>
            </div>

        </div>
        <?php $this->endWidget(); ?>
    </div>
    <div class="">
        <div class="">
            <div id="errormessage"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div id="errorMsg"></div>
        </div>
    </div>
    <div id="daybook-entry">
       
        <div id="due_bill_section">
        <h3>Materials</h3>
        <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $model->search(),
            'viewData' => array(),
            'itemView' => '_newview', 'template' => '<div class="sub-heading">{sorter}{summary}</div><div id="parent" class="subq_parent table-responsive"><table cellpadding="10" class="table tab1" id="subqtntbl">{items}</table></div><div class="pull-right">{pager}</div>',
            'enableSorting' => true,
            'sortableAttributes' => array(
                'project' => 'Project',
            ),
            'pager' => array(
                    'id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => '<<',
                    'nextPageLabel' => '>>'
                ),
        )); ?>
       
        </div>
    </div>
</div>

<?php $dropdownUrl = Yii::app()->createAbsoluteUrl("materialEntries/dynamicDropdown"); ?>
<?php $vendorUrl = Yii::app()->createAbsoluteUrl("materialEntries/dynamicVendor"); ?>
<?php $invoiceUrl = Yii::app()->createAbsoluteUrl("materialEntries/GetInvoiceDetails"); ?>
<?php $returnUrl = Yii::app()->createAbsoluteUrl("materialEntries/GetPurchaseReturnDetails"); ?>
<?php $getUrl = Yii::app()->createAbsoluteUrl("materialEntries/getDataByDate"); ?>
<?php $getBillUrl = Yii::app()->createAbsoluteUrl("materialEntries/GetBillDetails"); ?>

<?php $bill_invoice = Yii::app()->createAbsoluteUrl("materialEntries/DynamicBillorInvoice"); ?>
<?php $projectUrl = Yii::app()->createAbsoluteUrl("materialEntries/DynamicProject"); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-dateFormat/1.0/jquery.dateFormat.js"></script>
<script>
    

    $(document).ready(function () {

$(".popover-test").popover({
    html: true,
    content: function () {
        return $(this).next('.popover-content').html();
    }
});
$('[data-toggle=popover]').on('click', function (e) {
    $('[data-toggle=popover]').not(this).popover('hide');
});
$('body').on('hidden.bs.popover', function (e) {
    $(e.target).data("bs.popover").inState.click = false;
});
$('body').on('click', function (e) {
    $('[data-toggle=popover]').each(function () {
        // hide any open popovers when the anywhere else in the body is clicked
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});
$(document).ajaxComplete(function () {
    $(".popover-test").popover({
        html: true,
        content: function () {
            return $(this).next('.popover-content').html();
        }
    });
});
$(".js-example-basic-single").select2();
$(".project").select2();
$(".client").select2();
$(".company_id").select2();
});
      $(document).ready(function() {
       
        $('#MaterialItems_material').on('change', function(event, data = {}) {
            var material = $(this).val();
            
            if (!data.flag) {
                $.ajax({
                    url: 'index.php?r=materialEntries/GetUnit', 
                    type: 'POST',
                    data: { material_id: material },
                    success: function(response) {
                        try {
                            var obj = JSON.parse(response);

                            if (obj.status === "success" && obj.data) {
                                var materialUnit = obj.data.material_unit;

                                // Handle the response from the controller
                                console.log(response);

                                // Set the value of #MaterialItems_unit and trigger change
                                $('#MaterialItems_unit').val('');

                                if (materialUnit.length === 1) {
                                    // If there's only one unit, directly select it
                                    $('#MaterialItems_unit').val(materialUnit).trigger('change');
                                    $('#MaterialItems_unit').css('pointer-events', 'none');

                                } else {
                                    // Otherwise, loop through the options and show/hide based on materialUnit
                                    $('#MaterialItems_unit option').each(function() {
                                        var unitId = $(this).val();
                                        if (materialUnit.indexOf(unitId) === -1) {
                                            $(this).hide();
                                        } else {
                                            $(this).show();
                                        }
                                    });
                                }
                            } else {
                                console.log("Error: Invalid response format or status");
                                alert("Error: Invalid response format or status");
                            }
                        } catch (e) {
                            console.log('Error parsing JSON response: ' + e);
                            alert('Error parsing JSON response: ' + e);
                        }
                    },
                    error: function(xhr, status, error) {
                        // Handle any errors
                        console.log('Error: ' + error);
                    }
                });
            }   
        });
    });
    let count = 0;



$(document).on('click', '.remove-btn', function() {
    let buttonId = $(this).attr('id');
    $('#parent_id_' + buttonId).remove();
});


    $(".js-example-basic-single").select2();
    $(function() {
        $("#MaterialEntries_expense_date").datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: new Date()
        });
    });
    $(document).ready(function() {
        var urlParams = new URLSearchParams(window.location.search);
        if (urlParams.has('id')) {
            editRowClick('', urlParams.get('id'));
        }

        $('#loading').hide();
        $('#daybookform').on('shown.bs.collapse', function() {
            $('select').first().focus();
        });
       
        var model_company_id = '<?php echo $model->company_id; ?>';
        var model_project_id = '<?php echo $model->project_id; ?>';
        var model_subid ='<?php echo $model->subcontractor_id; ?>';
        var model_expensehead_id ='<?php echo $model->expensehead_id; ?>';
        
        getProjectList(model_company_id, 0, model_project_id, model_subid);
        getExpenseHead(model_subid, model_expensehead_id);

    });
</script>
<script type="text/javascript">
    function edit(id) { 
       window.location.href = '<?php echo Yii::app()->createAbsoluteUrl('materialEntries/create&id='); ?>' + id;
    }

    function approve(id) {
        
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('materialEntries/approve'); ?>',
            type: 'POST',
            data: { id: id },
            success: function(data) {
                if (data == 1) {
                    $("#errorMsg").show()
                        .html('<div class="alert alert-success"><b>Success!</b> Material Entry Approved Successfully.</div>')
                        .fadeOut(5000);
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                } else {
                    $("#errorMsg").show()
                        .html('<div class="alert alert-danger"><b>Error Occurred.</b></div>')
                        .fadeOut(5000);
                }
            }
        });
    }


    function getFullData(newDate) {
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            url: "<?php echo $getUrl; ?>",
            data: {
                "date": newDate
            },
            type: "POST",
            success: function(data) {
                $("#newlist").html(data);
            }
        });
    }

    function changedate() {
        var cDate = $("#MaterialEntries_expense_date").val();
        getFullData(cDate);
        validateDateSelection(cDate);
    }

    function getDropdownList(project_id, bill_id, exptypeid, edit, expId) {
        $('#loading').show();
        if (edit == undefined || edit == '') {
            edit = '';
        }
        $.ajax({
            url: "<?php echo $dropdownUrl; ?>",
            data: {
                "project": project_id,
                "bill": bill_id,
                "expId": expId
            },
            type: "POST",
            success: function(data) {
                var result = JSON.parse(data);
                $("#MaterialEntries_bill_id").html(result["bills"]);
                if (edit == 'edit') {
                    $("#MaterialEntries_project_id").select2("focus");
                } else {
                    $("#MaterialEntries_bill_id").select2("focus");
                }

                if (exptypeid == 0) {
                    $("#MaterialEntries_bill_id").val(0);
                    $("#MaterialEntries_vendor_id").html('<option value="">-Select Vendor-</option>');
                } else {
                    if (bill_id === null) {
                        $("#MaterialEntries_bill_id").val(0);
                    } else {
                        if (result['type'] == 'bill') {
                            $("#MaterialEntries_bill_id").val('1,' + bill_id);
                        } else if (result['type'] == 'invoice') {
                            $("#MaterialEntries_bill_id").val('2,' + bill_id);
                        } else {
                            $("#MaterialEntries_bill_id").val('3,' + bill_id);
                        }
                    }

                }

                $("#materialEntries_expensetype_id").html(result["expenses"]);
                if (exptypeid != 0)
                    $("#materialEntries_expensetype_id").val(exptypeid);
            },
        });
    }

    function getDynamicBillorInvoice() {
        $('#loading').show();
        $.ajax({
            url: "<?php echo $bill_invoice; ?>",
            type: "POST",
            success: function(data) {
                $("#MaterialEntries_bill_id").html(data);
            }
        });
    }

    function getProjectList(comId, opt, pro, sub) {
        var materialId = $("#txtmaterialEntriesId").val();
        $('#loading').show();
        $.ajax({
            url: "<?php echo $projectUrl; ?>",
            data: {
                "comId": comId,
                "materialId": materialId
            },
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data.materialItems);
                if (comId == 0) {
                    $("#MaterialEntries_project_id").val('').trigger('change.select2');
                    $("#MaterialEntries_subcontractor_id").val('').trigger('change.select2');
                } else {
                    $("#MaterialEntries_project_id").html(data.project);
                    $("#MaterialEntries_subcontractor_id").html(data.subcontractor);
                }
                
                $("#MaterialEntries_subcontractor_id").val(sub).trigger('change.select2');
                $("#MaterialEntries_project_id").val(pro).trigger('change.select2');
                
                if (sub != 0) {
                    $("#MaterialEntries_subcontractor_id").val(sub).trigger('change.select2');
                }
                if (pro != 0) {
                    $("#MaterialEntries_project_id").val(pro).trigger('change.select2');
                }
                
                // Populate the first row with the first item of the returned data
                if (data.materialItems.length > 0) {
                    let firstItem = data.materialItems[0];
                    console.log(firstItem);
                    $('#MaterialItems_material').val(firstItem.material).trigger('change', { flag: true });
                    $('#MaterialItems_quantity').val(firstItem.quantity);
                    $('#MaterialItems_unit').val(firstItem.unit);
                }

                // Add remaining material items
                for (let i = 1; i < data.materialItems.length; i++) {
                    let item = data.materialItems[i];
                    addFormField( item.material, item.quantity, item.unit);
                }

                $('#loading').hide();
            }
        });
    }
    count=0;
    function addFormField(materialId = '', quantity = '', unitId = '') {
        var materialOptions = `<?php
            $materialData = CHtml::listData(Materials::model()->findAll(), 'id', 'material_name');
            foreach ($materialData as $id => $name) {
                echo '<option value="' . $id . '">' . $name . '</option>';
            }
        ?>`;

        var unitOptions = `<?php
            $unitData = CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name');
            foreach ($unitData as $id => $name) {
                echo '<option value="' . $id . '">' . $name . '</option>';
            }
        ?>`;
        count++;
        $("#add_more_item").append(
            `
            <div id="parent_id_${count}" class="row">
                <div class="col-md-2">
                    <div class="form-group item-material">
                        <label for="material_${count}">Material</label>
                        <select name="MaterialItems[material][]" class="form-control material_add" id="material_${count}"style="width:100%;">
                            <option value="">-- Select Material --</option>
                            ${materialOptions}
                        </select>
                        <div class="errorMessageequipments" style="display:none;color:red;"></div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="quantity_${count}">Quantity</label>
                        <input type="text" name="MaterialItems[quantity][]" class="form-control" id="quantity_${count}" value="${quantity}" oninput="this.value = this.value.replace(/[^0-9]/g, '');">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="unit_${count}">Unit</label>
                        <select name="MaterialItems[unit][]" class="form-control" id="unit_${count}">
                            <option value="">-- Select Unit --</option>
                            ${unitOptions}
                        </select>
                    </div>
                </div>
               <div class="col-md-1 button-group">
                    <div class="form-group">
                        <button type="button" class="add-btn btn btn-info btn-sm" onClick="addFormField();" id="${count}">+</button>
                        <button type="button" class="remove-btn btn btn-danger btn-sm" id="${count}">-</button>
                    </div>
                </div>
            </div>
            `
        );
        $(`#material_${count}`).select2();
        // Set selected values
        if (materialId) {
            $(`#material_${count}`).val(materialId).trigger('change',{ flag: true });
        }
        if (unitId) {
            $(`#unit_${count}`).val(unitId).trigger('change');
        }
        // $(`#unit_${count}`).css('pointer-events', 'none');
    }

    $(document).on('click', '.remove-btn', function() {
        var id = $(this).attr('id');
        $(`#parent_id_${id}`).remove();
        count--;
    });

    $(document).ready(function() {
        $(document).on('change', '.material_add', function(event, data = {}) {
            var material = $(this).val();
            if (!data.flag) {
                $.ajax({
                    url: 'index.php?r=materialEntries/GetUnit', 
                    type: 'POST',
                    data: { material_id: material },
                    success: function(response) {
                    try {
                        var obj = JSON.parse(response);

                        if (obj.status === "success" && obj.data) {
                            var materialUnits = obj.data.material_unit;
                            $(`#unit_${count}`).val('');
                            console.log(response);
                            $(`#unit_${count}`).val('');
                            
                            if (materialUnits.length === 1) {
                            // If there's only one unit, directly select it
                            $(`#unit_${count}`).val(materialUnits).trigger('change');
                            $(`#unit_${count}`).css('pointer-events', 'none');
                            } else {
                                // Otherwise, loop through the options and show/hide based on materialUnit
                                $(`#unit_${count} option`).each(function() {
                                var unitId = $(this).val();
                                if (materialUnits.indexOf(unitId) === -1) {
                                    $(this).hide();
                                } else {
                                    $(this).show();
                                }
                            });
                            }
                            
                            
                        } else {
                            console.log("Error: Invalid response format or status");
                            alert("Error: Invalid response format or status");
                        }
                    } catch (e) {
                        console.log('Error parsing JSON response: ' + e);
                        alert('Error parsing JSON response: ' + e);
                    }
                },
                    error: function(xhr, status, error) {
                        // Handle any errors
                        console.log('Error: ' + error);
                        $(`#unit_${count}`).val('');
                    }
                });
            }
        })
    });

    $("#MaterialEntries_company_id").change(function() {
        var comId = $("#MaterialEntries_company_id").val();
        var comId = comId ? comId : 0;
        getProjectList(comId, 0, 0, 0);
    });

    $("#MaterialEntries_project_id").change(function() {
        var prId = $("#MaterialEntries_project_id").val();
        var prId = prId ? prId : 0;
        getVendorList(prId, 0);
    });

    function getVendorList(prId, expvendor, edit) {
        $('#loading').show();
        if (edit == undefined) {
            edit = '';
        }
        if (prId === null) {
            prId = 0;
        } else {
            prId = prId;
        }

        $.ajax({
            url: "<?php echo $vendorUrl; ?>",
            data: {
                "prId": prId
            },
            type: "POST",
            success: function(data) {
                $("#MaterialEntries_vendor_id").html(data);
                if (edit == 'edit') {
                    $("#MaterialEntries_company_id").select2("focus");
                } else {
                    $("#MaterialEntries_vendor_id").select2("focus");
                }
                if (expvendor != 0)
                    $("#MaterialEntries_vendor_id").val(expvendor);
            }
        });
    }

    function actdelete(id,elem){
        
        var item_count = $(elem).attr('data-item-id');                
        if(item_count > 0){
             var answer = confirm("This Quotation contains "+item_count +" items.Are you sure you want to delete?");
        }else{
            var answer = confirm("Are you sure you want to delete?");
        }
                
        if (answer) {
            $.ajax({
            url: 'index.php?r=materialEntries/DeleteMaterial',
                type: 'POST',
                dataType:'json',
                data: {
                   id: id
                },
                success: function(data) {                
                    $("#errorMsg").show()
                    .html('<div class="alert alert-'+data.response+'"><b>'+data.msg+'</b></div>')
                    .fadeOut(5000);

                    setTimeout(function() {
                    location.reload();
                    }, 10)
                }
            })
        }
    }

    $(document).ready(function() {
        $("#MaterialEntries_project_id").focus();
        $("#MaterialEntries_bank_id").attr("disabled", true);
        $("#txtChequeno").attr("readonly", true);
        $("#txtPurchaseType").attr("disabled", true);
        $("#txtPaid").attr("readonly", true);
        $("a#next").css("pointer-events", "none").css("color", "#ccccc");
        $("#buttonsubmit").click(function(event) {
            event.preventDefault()
            $("#buttonsubmit").attr('disabled', true);
            var crDate = $("#MaterialEntries_expense_date").val();
            var project = $("#MaterialEntries_project_id").val();
            var company = $("#MaterialEntries_company_id").val();
            var subcontractor_id = $("#MaterialEntries_subcontractor_id").val();
            var expensehead_id = $("#MaterialEntries_expensehead_id").val();

            var materail=$("#MaterialItems_material").val();
            var quantity=$("#MaterialItems_quantity").val();
            var unit=$("#MaterialItems_quantity").val();

            if (company == "") {
                $("#errormessage").show()
                    .html('<div class="alert alert-danger">Please select a company from the dropdown.</div>')
                    .fadeOut(5000);
                $("#buttonsubmit").attr('disabled', false);
                return false;
            }
            if (project == "") {
                $("#errormessage").show()
                    .html('<div class="alert alert-danger">Please select a project from the dropdown.</div>')
                    .fadeOut(5000);
                $("#buttonsubmit").attr('disabled', false);
                return false;
            }
            if (subcontractor_id == "") {
                $("#errormessage").show()
                    .html('<div class="alert alert-danger">Please select a subcontractor from the dropdown.</div>')
                    .fadeOut(5000);
                $("#buttonsubmit").attr('disabled', false);
                return false;
            }

            var materialId = $("#txtmaterialEntriesId").val();
            
            var actionUrl;
            if (materialId == "") {
                actionUrl = "<?php echo Yii::app()->createAbsoluteUrl("materialEntries/addMaterialentries"); ?>";
                localStorage.setItem("action", "add");
            } else {
                actionUrl = "<?php echo Yii::app()->createAbsoluteUrl("materialEntries/updateMaterialentries"); ?>";
                localStorage.setItem("action", "update");
            }
            var data = $("#material-form").serialize();
            console.log(data,1);
            $('.loading-overlay').addClass('is-active');
           
          
            $.ajax({
                type: 'POST',
                url: actionUrl,
                data: data,
                success: function(data) {
                    $("#buttonsubmit").attr('disabled', false);
                    $('.loading-overlay').removeClass('is-active');
                    if (data == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-warning"><strong>Failed !</strong> please try again.</div>')
                            .fadeOut(10000);
                        $("#buttonsubmit").attr('disabled', false);
                        return false;
                    } else {
                        
                        var currAction = localStorage.getItem("action");
                        if (currAction == "add") {
                            $("#errormessage").show()
                                .html('<div class="alert alert-success"><strong>Successfully !</strong> record added.</div>')
                                .fadeOut(10000);
                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth() + 1;
                            var yyyy = today.getFullYear();
                            if (dd < 10)
                                dd = '0' + dd
                            if (mm < 10)
                                mm = '0' + mm
                            today = dd + '-' + mm + '-' + yyyy;
                            $("#entrydate").text(today);
                            $('#MaterialEntries_subcontractor_id').val('').trigger('change.select2');
                            $('#MaterialEntries_expensehead_id').val('').trigger('change.select2');
                            $('#MaterialEntries_project_id').val('').trigger('change.select2');
                            $('#MaterialEntries_company_id').val('').trigger('change.select2');
                            $('#MaterialEntries_bill_id').val('0').trigger('change.select2');
                            $('#materialEntries_expensetype_id').html('<option value="">-Select Expense Head-</option>');
                            $('#txtPurchaseType').val("").trigger('change.select2');
                            $('#MaterialEntries_expense_type').val('').trigger('change.select2');
                            $('#MaterialEntries_employee_id').val('').trigger('change.select2');
                            $('#MaterialEntries_bank_id').val('').trigger('change.select2');
                            $('#MaterialEntries_vendor_id').html('<option value="">-Select Vendor-</option>');
                        } else if (currAction == "update") {
                            $("#txtDaybookId").val('');
                            $("#buttonsubmit").text("ADD");
                            if (data == 2) {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-success"><strong>Successfully !</strong> Update request send succesfully. Please wait for approval</div>')
                                    .fadeOut(10000);
                                $('.loading-overlay').addClass('is-active');
                                $.ajax({
                                    type: "POST",
                                    data: {
                                        date: crDate
                                    },
                                    url: "<?php echo Yii::app()->createUrl("materialEntries/getAllData") ?>",
                                    success: function(response) {
                                        document.getElementById("material-form").reset();
                                        console.log(response);
                                        $("#MaterialEntries_expense_date").val(crDate);
                                        $("#newlist").html(response);
                                        $("#MaterialEntries_project_id").focus();
                                        $("#MaterialEntries_bill_id").attr("disabled", false);
                                        $("#materialEntries_expensetype_id").attr("disabled", false);
                                        $("#materialEntries_payment_type").attr("disabled", false);
                                        $("#txtReceipt").attr("readonly", false);
                                        $("#buttonsubmit").attr("disabled", false);


                                        $('#MaterialEntries_project_id').val('').trigger('change.select2');
                                        $('#MaterialEntries_company_id').val('').trigger('change.select2');
                                        $('#MaterialEntries_bill_id').val('0').trigger('change.select2');
                                        $('#materialEntries_expensetype_id').html('<option value="">-Select Expense Head-</option>');
                                        $('#txtPurchaseType').val("").trigger('change.select2');
                                        $('#MaterialEntries_expense_type').val('').trigger('change.select2');
                                        $('#MaterialEntries_employee_id').val('').trigger('change.select2');
                                        $('#MaterialEntries_bank_id').val('').trigger('change.select2');
                                        $('#MaterialEntries_vendor_id').html('<option value="">-Select Vendor-</option>');
                                    }
                                });
                            } else if (data == 3) {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-danger"><strong>Failed !</strong> Cannot send update request.</div>')
                                    .fadeOut(10000);
                            } else if (data == 4) {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-warning"><strong>Failed !</strong> Cannot send update request. Already have a request for this.</div>')
                                    .fadeOut(10000);
                            } else {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-success"><strong>Successfully !</strong> record updated.</div>')
                                    .fadeOut(10000);
                            }
                        }
                        document.getElementById("material-form").reset();
                        $("#MaterialEntries_expense_date").val(crDate);
                        if (data == 2 || data == 3 || data == 4) {

                        } else {
                            $("#newlist").html(data);
                        }
                        $('#MaterialEntries_bill_id').val('0').trigger('change.select2');
                        $('#materialEntries_expensetype_id').html('<option value="">-Select Expense Head-</option>');
                        $('#txtPurchaseType').val("").trigger('change.select2');
                        $('#MaterialEntries_expense_type').val('').trigger('change.select2');
                        $('#MaterialEntries_employee_id').val('').trigger('change.select2');
                        $('#MaterialEntries_bank_id').val('').trigger('change.select2');
                        $('#MaterialEntries_vendor_id').html('<option value="">-Select Vendor-</option>');
                    }
                    window.location.href = '<?php echo Yii::app()->createAbsoluteUrl('materialEntries/create'); ?>';
                },
                error: function(data) {
                    alert("Error occured.please try again");
                    alert(data);
                }
            });
        });
        $("#txtPaid").keyup(function() {
            var totalamount = parseFloat($("#txtTotal").val());
            var purchasetype = $("#txtPurchaseType").val();
            var paid = parseFloat($("#txtPaid").val());
            if (purchasetype == 3) {
                if (paid >= totalamount) {
                    $("#errormessage").show()
                        .html('<div class="alert alert-warning">You have selected the purchase type as Partially Paid.<br/>The paid amount must be less than the total amount.</div>')
                        .fadeOut(10000);
                    $("#buttonsubmit").attr('disabled', true);
                    return false;
                } else if (paid <= 0) {
                    $("#errormessage").show()
                        .html('<div class="alert alert-warning">You have selected the purchase type as Partially Paid.<br/>The paid amount must be greater than 0.</div>')
                        .fadeOut(10000);
                    $("#buttonsubmit").attr('disabled', true);
                    return false;
                } else {

                    $("#buttonsubmit").attr('disabled', false);
                }
            }
            if (paid == "") {
                if (totalamount == "") {
                    getTdsCalculations(0);
                } else {
                    getTdsCalculations(totalamount);
                }
            } else {
                getTdsCalculations(paid);
            }
        });

        $("body").on("click", ".row-daybook", function(e) {
            var rowId = $(this).attr("id");
            var expId = $("#materialId" + rowId).val();
            editRowClick(rowId, expId);
        });



        $("#previous").click(function(e) {
            var cDate = $("#MaterialEntries_expense_date").val();
            cDate = cDate.split("-").reverse().join("-");
            var newDate = new Date(cDate);
            var prDate = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate() - 1);
            var newPrDate = $.format.date(prDate, "dd-MM-yyyy");
            $("#MaterialEntries_expense_date").val(newPrDate);
            getFullData(newPrDate);
            validateDateSelection(newPrDate);
        });
        $("#current").click(function() {
            var cDate = new Date();
            var newDate = $.format.date(cDate, "dd-MM-yyyy");
            $("#MaterialEntries_expense_date").val(newDate);
            getFullData(newDate);
            validateDateSelection(newDate);
        });
        $("#next").click(function() {
            var cDate = $("#MaterialEntries_expense_date").val();
            cDate = cDate.split("-").reverse().join("-");
            var newDate = new Date(cDate);
            var nxtDate = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate() + 1);
            var newNxtDate = $.format.date(nxtDate, "dd-MM-yyyy");
            $("#MaterialEntries_expense_date").val(newNxtDate);
            getFullData(newNxtDate);
            validateDateSelection(newNxtDate);
        });


        $('.numbersOnly').keyup(function() {
            this.value = this.value.replace(/[^0-9\.]/g, '');
        });


        $("#btnReset, [data-toggle='collapse']").click(function() {
            $("#txtDaybookId").val("");
            $("#buttonsubmit").text("ADD");
            $('#material-form').find('input, select, textarea').not("#MaterialEntries_expense_date").val('');
            $("#materialEntries_expensetype_id").html('<option value="">-Select Expense Head-</option>');
            $("#MaterialEntries_vendor_id").html('<option value="">-Select Vendor-</option>');
            $("#materialEntries_expensetype_id").attr("disabled", false);
            $("#txtPurchaseType").attr("disabled", true);
            $('#MaterialEntries_project_id').val('').trigger('change.select2');
            $('#MaterialEntries_bill_id').val('0').trigger('change.select2');
            $('#materialEntries_expensetype_id').val('').trigger('change.select2');
            $('#txtPurchaseType').val('').trigger('change.select2');
            $('#MaterialEntries_expense_type').val('').trigger('change.select2');
            $('#MaterialEntries_bank_id').val('').trigger('change.select2');
            $('#MaterialEntries_vendor_id').val('').trigger('change.select2');
            $('#MaterialEntries_employee_id').val('').trigger('change.select2');
            $('#MaterialEntries_company_id').val('').trigger('change.select2');
            $('.employee_box').hide();
            $("#txtSgst1").text("");
            $("#txtCgst1").text("");
            $("#txtIgst1").text("");
            $("#txtTotal1").html("");
            $("#txtgstTotal").text("");
            $("#txtTdsp").val("");
            $("#txtTdsp").attr("readonly", false);
            $("#txtTds1").text("");
            $("#txtTds").val("");
            $("#txtTdsPaid").html("");
            $("#materialEntries_paidamount").val("");
        });
    });


    $("#txtSgstp").keyup(function(e) {
        if (e.keyCode == 13) {
            $("#txtCgstp").focus();
        }
    });

    $("#txtCgstp").keyup(function(e) {
        if (e.keyCode == 13) {
            $("#txtIgstp").focus();
        }
    });

    $("#txtIgstp").keyup(function(e) {
        if (e.keyCode == 13) {
            $("#txtDescription").focus();
        }
    });

    $("#MaterialEntries_company_id").change(function() {
        $('#loading').show();
        var company_id = $("#MaterialEntries_company_id").val();
        if (company_id != "")
            $.ajax({
                method: "POST",
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('materialEntries/ajaxcall'); ?>',
                success: function(result) {
                    $("#MaterialEntries_project_id").select2("focus");
                }
            });
    });

    // decimal point check
    $(document).on('keydown', '.check', function(event) {
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
            (event.keyCode >= 96 && event.keyCode <= 105) ||
            event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
            event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
            var splitfield = $(this).val().split(".");
            if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
                event.preventDefault();
            }
        } else {
            event.preventDefault();
        }
        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();
    });

    $('.percentage').keyup(function() {
        if ($(this).val() > 100) {
            $(this).val('');
        }
    });
    // delete confirmation
    $(document).on('click', '.delete_confirmation', function(e) {
        var rowId = $(this).attr("id");
        var expId = $("#materialId" + rowId).val();
        var date = $("#MaterialEntries_expense_date").val();
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                type: "POST",
                data: {
                    expId: expId,
                    expense_date: date,
                    status: 1,
                    delId: "",
                    action: 1
                },
                url: "<?php echo Yii::app()->createUrl("materialEntries/deleteconfirmation") ?>",
                success: function(data) {
                    $('.loading-overlay').removeClass('is-active');
                    if (data == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-danger"><strong>Failed!</strong> This request unable to process now. Please try again later.</div>')
                            .fadeOut(5000);
                        return false;
                    } else {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Success!</strong> Request for deleting this row is send to admin. Please wait for response.</div>')
                            .fadeOut(5000);
                        $("#newlist").html(data);
                    }
                }
            });
        } else {
            return false;
        }
    });
    // delete restore
    $(document).on('click', '.delete_restore', function(e) {
        var rowId = $(this).attr("id");
        var delId = $(this).attr("data-id");
        var expId = $("#materialId" + rowId).val();
        var date = $("#MaterialEntries_expense_date").val();
        var answer = confirm("Are you sure you want to restore?");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                type: "POST",
                data: {
                    expId: expId,
                    expense_date: date,
                    status: 2,
                    delId: delId,
                    action: 1
                },
                url: "<?php echo Yii::app()->createUrl("materialEntries/deleteconfirmation") ?>",
                success: function(data) {
                    $('.loading-overlay').removeClass('is-active');
                    if (data == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-danger"><strong>Failed!</strong> This request unable to process now. Please try again later.</div>')
                            .fadeOut(5000);
                        return false;
                    } else {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Success!</strong> Restored data successfully.</div>')
                            .fadeOut(5000);
                        $("#newlist").html(data);
                    }
                }
            });
        } else {
            return false;
        }
    });
    // delete row
    $(document).on('click', '.delete_row', function(e) {
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            var rowId = $(this).attr("id");
            var expId = $("#materialId" + rowId).val();
            var date = $("#MaterialEntries_expense_date").val();
            $.ajax({
                type: "POST",
                data: {
                    expId: expId,
                    expense_date: date
                },
                url: "<?php echo Yii::app()->createUrl("materialEntries/deletereport") ?>",
                success: function(data) {
                    $('.loading-overlay').removeClass('is-active');
                    if (data == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-warning"><strong>Failed !</strong> please try again.</div>')
                            .fadeOut(5000);
                        $("#buttonsubmit").attr('disabled', false);
                        return false;
                    } else {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Success!</strong> Row Deleted.</div>')
                            .fadeOut(5000);
                        $("#newlist").html(data);
                    }

                    $('#MaterialEntries_project_id').val('').trigger('change.select2');
                    $('#MaterialEntries_bill_id').val('0').trigger('change.select2');
                    $('#materialEntries_expensetype_id').html('<option value="">-Select Expense Head-</option>');
                    $('#txtPurchaseType').val("").trigger('change.select2');
                    $('#MaterialEntries_expense_type').val('').trigger('change.select2');
                    $('#MaterialEntries_employee_id').val('').trigger('change.select2');
                    $('#MaterialEntries_bank_id').val('').trigger('change.select2');
                    $('#MaterialEntries_vendor_id').html('<option value="">-Select Vendor-</option>');
                    $('#MaterialEntries_company_id').val('').trigger('change.select2');
                    $("#txtAmount").attr("readonly", true);
                    $("#txtSgstp").attr("readonly", true);
                    $("#txtCgstp").attr("readonly", true);
                    $("#txtIgstp").attr("readonly", true);
                    $("#txtPurchaseType").attr("disabled", true);
                    $("#txtPaid").attr("readonly", true);
                    $("#MaterialEntries_bank_id").attr("disabled", true);
                    $("#txtChequeno").attr("readonly", true);
                    $("#txtSgst1").text("");
                    $("#txtCgst1").text("");
                    $("#txtIgst1").text("");
                    $("#txtTotal1").html("");
                    $("#txtgstTotal").text("");
                }
            })
        }

    })

    function getTdsCalculations(total) {
        if ($("#txtTdsp").val() != "") {
            var tdsp = parseFloat($("#txtTdsp").val());
        } else {
            var tdsp = 0;
        }

        var tdsamount = (tdsp / 100) * total;
        if (isNaN(tdsamount))
            tdsamount = 0;
        var paidamount = total - parseFloat(tdsamount);
        if (isNaN(paidamount))
            paidamount = 0;

        $("#txtTds1").text(tdsamount.toFixed(2));
        $("#txtTds").val(tdsamount.toFixed(2));
        $("#txtTdsPaid").html("<b>Amount To Be Paid: </b>" + paidamount.toFixed(2) + "");
        $("#materialEntries_paidamount").val(paidamount.toFixed(2));
    }

    function editRowClick(rowId, expId) {
        $('html, body').animate({
            scrollTop: 0
        }, 'slow');
        $(".daybook .collapse").addClass("in");
        $(".addentries").removeClass("collapsed");
        if ($(".daybook .collapse").css('display') == 'visible') {
            $(".daybook .collapse").css({
                "height": "auto"
            });
        } else {
            $(".daybook .collapse").css({
                "height": ""
            });
        }
        $(".popover").removeClass("in");
        $("#buttonsubmit").text("UPDATE");
        $("#txtmaterialEntriesId").val(expId);
        $('.loading-overlay').addClass('is-active');
        
        $.ajax({
            url: "",
            data: {
                "materialEntries_id": expId
            },
            type: "POST",
            success: function(data) {
                $('.loading-overlay').removeClass('is-active');
                var result = JSON.parse(data);

                $("#MaterialEntries_company_id").select2("focus");
                $('#MaterialEntries_company_id').val(result["company_id"]).trigger('change.select2');
                $('#MaterialEntries_project_id').val(result["project_id"]).trigger('change.select2');
                $("#MaterialEntries_project_id").val(result["project_id"]);
                getProjectList(result["company_id"], 0, result["project_id"], result["subcontractor_id"]);
                getExpenseHead(result["subcontractor_id"], result["expensehead_id"]);
                $('#MaterialEntries_subcontractor_id').val(result["subcontractor_id"]).trigger('change.select2');
                $('#materialEntries_amount').val(result["amount"]).trigger('change.select2');
                $(".field_details").html(result["html"]);
                $('#MaterialEntries_expense_date').val(result['date']);
                $('#materialEntries_description').val(result['description']);
            }

        });
    }


    /*  new section  */
    $("#MaterialEntries_expensehead_id").change(function() {
        var id = $(this).val();
        // if (id != '') {
        //     getExpenseFields(id);
        // }
    })

    $("#MaterialEntries_subcontractor_id").change(function() {
        var id = $(this).val();
        if (id != '') {
            id = id ? id : 0;
            getExpenseHead(id, 0);
        }
    });

    function getExpenseHead(id, exp) {
        $('#loading').show();

        $.ajax({
            url: "<?php echo $this->createUrl('materialEntries/getexpensehead'); ?>",
            data: {
                "id": id
            },
            type: "GET",
            success: function(response) {
                if (id == 0) {
                    $("#MaterialEntries_expensehead_id").val('-Select Expense Head-').trigger('change.select2');
                } else {
                    $("#MaterialEntries_expensehead_id").html(response);
                }

               if (exp != 0) {
                $("#MaterialEntries_expensehead_id").val(exp).trigger('change.select2');
               }
            }
        });
    }

    // function getExpenseFields(id) {
    //     $('#loading').show();
    //     $.ajax({
    //         type: "GET",
    //         data: {
    //             id: id
    //         },
    //         url: "",
    //         success: function(response) {
    //             $(".field_details").html(response);
    //         }
    //     });
    // }

    $(document).on('blur', '.calculation', function() {
        var amount = parseFloat($("#materialEntries_amount").val());
        var labour = parseFloat($("#materialEntries_labour").val());
        var wage = parseFloat($("#materialEntries_wage").val());
        var wage_rate = parseFloat($("#materialEntries_wage_rate").val());
        var helper = parseFloat($("#materialEntries_helper").val());
        var helper_labour = parseFloat($("#materialEntries_helper_labour").val());
        var lump_sum = parseFloat($("#materialEntries_lump_sum").val());

        var labour_wage = parseFloat($("#materialEntries_labour_wage").val());
        var helper_wage = parseFloat($("#materialEntries_helper_wage").val());

        var total_amount = 0;

        if (isNaN(labour))
            labour = 0;
        if (isNaN(wage))
            wage = 0;
        if (isNaN(wage_rate))
            wage_rate = 0;
        if (isNaN(helper))
            helper = 0;
        if (isNaN(helper_labour))
            helper_labour = 0;
        if (isNaN(lump_sum))
            lump_sum = 0;
        if (isNaN(labour_wage))
            labour_wage = 0;
        if (isNaN(helper_wage))
            helper_wage = 0;

        if ((lump_sum != '') && (labour == '' && wage == '' && wage_rate == '' && helper == '' && helper_labour == '')) {
            total_amount = lump_sum;
        } else if ((labour != '' && wage != '' && wage_rate != '') && (helper == '' && helper_labour == '' && lump_sum == '')) {
            total_amount = labour * wage * wage_rate;
        } else if ((labour != '' && helper != '' && wage != '' && wage_rate != '') && (helper_labour == '' && lump_sum == '')) {
            total_amount = (labour + helper) * (wage * wage_rate);
        } else if ((labour >= 0 && wage_rate >= 0 && labour_wage >= 0 && helper >= 0 && helper_labour >= 0 && helper_wage >= 0)) {
            total_amount = (labour * wage_rate * labour_wage) + (helper * helper_labour * helper_wage);
        } else {
            total_amount = '';
        }
        if (total_amount != '') {
            $("#materialEntries_amount").val(total_amount.toFixed(2));
        } else {
            $("#materialEntries_amount").val('');
        }
    });

    $(document).on('click', '.approve', function(e) {
        e.preventDefault();
        var element = $(this);
        var item_id = $(this).attr('id');
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            url: '<?php echo  Yii::app()->createAbsoluteUrl('materialEntries/approve'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                item_id: item_id
            },
            success: function(response) {
                if (response.response == 'success') {
                    $(".approveoption_" + item_id).hide();
                    element.closest("tr").removeClass("approve_section");
                    element.closest("tr").addClass("tobe_approve");
                    $("#errormessage").show()
                        .html('<div class="alert alert-success"><strong>Success!</strong>' + response.msg + '</div>')
                        .fadeOut(5000);
                } else if (response.response == 'warning') {

                    $("#errormessage").show()
                        .html('<div class="alert alert-warning"><strong>Sorry!</strong>' + response.msg + '</div>')
                        .fadeOut(5000);
                } else {
                    $("#errormessage").show()
                        .html('<div class="alert alert-danger"><strong>Sorry!</strong> ' + response.msg + '</div>')
                        .fadeOut(5000);
                }
            }
        });
    });

    function validateDateSelection(cDate) {
        var selDate = cDate;
        var currDate = new Date();
        currDate = $.format.date(currDate, "dd-MM-yyyy");
        if (selDate == currDate) {
            $("a#next").css("pointer-events", "none").css("color", "#ccccc");
        } else {
            $("a#next").css("pointer-events", "visible").css("color", "#337ab7");
        }
        $("#btnReset").click();
    }
    $(document).ajaxComplete(function() {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
</script>
<style>
    .tobe_approve td:first-child {
        background-color: #fff !important;
    }
    .popover{
        white-space:nowrap;
    }
</style>