<?php

/**
 * This is the model class for table "{{expenses}}".
 *
 * The followings are the available columns in table '{{expenses}}':
 * @property integer $exp_id
 * @property integer $projectid
 * @property integer $bill_id
 * @property integer $invoice_id
 * @property integer $subcontractor_id
 * @property integer $userid
 * @property string $date
 * @property double $amount
 * @property string $description
 * @property integer $type
 * @property integer $exptype
 * @property integer $expense_type
 * @property integer $vendor_id
 * @property double $expense_amount
 * @property double $expense_sgstp
 * @property double $expense_sgst
 * @property double $expense_cgstp
 * @property double $expense_cgst
 * @property double $expense_igstp
 * @property double $expense_igst
 * @property integer $payment_type
 * @property integer $bank_id
 * @property integer $cheque_no
 * @property double $receipt
 * @property string $works_done
 * @property string $materials
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 * @property integer $purchase_type
 * @property double $paid
 * @property string $data_entry
 * @property integer $company_id
 * @property integer $reconciliation_status
 * @property string $reconciliation_date
 *
 * The followings are the available model relations:
 * @property UsersOffline $createdBy
 * @property Status $paymentType
 * @property Projects $project
 * @property UsersOffline $updatedBy
 * @property UsersOffline $user
 */
class Expenses extends CActiveRecord
{

	public $fromdate;
	public $todate;
	public $sid;
	public $paymentids;
	// public $instance=0;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{expenses}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('projectid, userid, amount, description, type, created_by, created_date', 'required'),
			array(
				'projectid, bill_id, invoice_id, subcontractor_id, userid, type, exptype, expense_type, vendor_id, 
			payment_type, bank_id, created_by, updated_by, purchase_type, company_id, reconciliation_status,employee_id',
				'numerical', 'integerOnly' => true
			),
			array('amount, expense_amount, expense_sgstp, expense_sgst, expense_cgstp, expense_cgst, expense_igstp, 
			expense_igst, expense_tdsp, expense_tds, receipt, paid, paidamount', 'numerical'),
			array('description', 'length', 'max' => 300),
			array('works_done, materials', 'length', 'max' => 100),
			array('data_entry', 'length', 'max' => 30),
			array('date, updated_date, reconciliation_date', 'safe'),
			array('amount', 'checkNegativeBalance'),
			array('bill_id', 'checkBillDate'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('exp_id, projectid, bill_id, invoice_id, subcontractor_id, userid, date, amount, description, 
			type, exptype, expense_type, vendor_id, expense_amount, expense_sgstp, expense_sgst, expense_cgstp, 
			expense_cgst, expense_igstp, expense_igst, expense_tdsp, expense_tds, payment_type, bank_id, cheque_no, 
			receipt, works_done, materials, created_by, created_date, updated_by, updated_date, purchase_type, paid, 
			paidamount, data_entry, company_id, reconciliation_status, reconciliation_date, employee_id, 
			budget_percentage,payment_quotation_status', 'safe', 'on' => 'search'),
		);
	}

    public function checkNegativeBalance() {

		if($this->exp_id !=""){
			$sql = "SELECT paidamount FROM `jp_expenses` "
				. " WHERE exp_id=".$this->exp_id;
			$oldpaidamount = Yii::app()->db->createCommand($sql)->queryScalar();
		}

		if ((($this->exp_id !="") && ($oldpaidamount != $this->paidamount)) || ($this->exp_id =="")) {
			if ($this->type == 73) {
				$invoice_amount = 0;
				$expense_amount = 0;
				$payment_type = 0;
				$available_amount = "";
				$company_id = $this->company_id;
				if ($this->expense_type == '89' || $this->expense_type == '88' || $this->expense_type == '103') {
					$payment_type = $this->expense_type;
				}
				if (!empty($company_id)) {
					$invoice_amount = BalanceHelper::cashInvoice($payment_type, $company_id, $this->employee_id, $this->bank_id, $this->date);
					$expense_amount = BalanceHelper::cashExpense($payment_type, $company_id, $this->employee_id, $this->exp_id, $this->bank_id, $this->date);

					if (is_numeric($invoice_amount) && is_numeric($expense_amount)) {
						$available_amount = $invoice_amount - $expense_amount; //default closing balance
					}

					if (is_numeric($available_amount) && $available_amount >= $this->paidamount && $payment_type != "103" && $payment_type != "0") {
						$closing_balance = $available_amount - $this->paidamount;  //initial closing balace						
						$date = date('Y-m-d', strtotime('+1 day', strtotime($this->date)));
						$paymentArray = $this->getPaymentArray($date, NULL, $status = 0, $payment_type, $company_id, $this->bank_id);
						if(!empty($paymentArray)){
							if (count($paymentArray) < 20000) {
								foreach ($paymentArray as $data) {
									if ($data['type'] == 73) { //expense
										$closing_balance -= $data['paidamount'];
									} else {
										$closing_balance += $data['receipt'];
									}
									if ($closing_balance < 0) {
										$this->addError("amount", "Insufficient balance on " . Controller::formattedcommonDate($data['date']) . " in " . $data['tablename'] . " - ID: #" . $data['entry_id']);
										return;
									}
								}
							} else {
								$this->addError("amount", "Limit Of 20000 Entries Exceeded");
								return;
							}
						}
						
					} else if (($this->paidamount > $available_amount) && $payment_type != "103" && $payment_type != "") {
						$this->addError("amount", "insufficient balance on " . Controller::formattedcommonDate($this->date));
						$date = date('Y-m-d', strtotime($this->date));
						$this->addError("amount", "insufficient balance on " . Controller::formattedcommonDate($this->date));
					}

					if (!is_numeric($available_amount)) {
						$this->addError("", "Something went wrong! Check again or contact support team");
					}
				}
			}
		}
    }
        
	public function checkBillDate($attribute, $params) {
            if (!empty($this->bill_id)) {
                $sql = 'SELECT bill_date FROM jp_bills '
                        . ' WHERE bill_id =' . $this->bill_id;
                $bill_date = Yii::app()->db->createCommand($sql)->queryScalar();
                $billDate = date("Y-m-d", strtotime($bill_date));
                if ($this->date < $billDate) {
                    $this->addError($attribute, 'Date should be greater than start date');
                }
            }
        }

    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'createdBy' => array(self::BELONGS_TO, 'UsersOffline', 'created_by'),
			'paymentType' => array(self::BELONGS_TO, 'Status', 'payment_type'),
			'project' => array(self::BELONGS_TO, 'Projects', 'projectid'),
			'vendor' => array(self::BELONGS_TO, 'Vendors', 'vendor_id'),
			'subcontractor' => array(self::BELONGS_TO, 'SubcontractorPayment', 'subcontractor_id'),
			'updatedBy' => array(self::BELONGS_TO, 'UsersOffline', 'updated_by'),
			'user' => array(self::BELONGS_TO, 'UsersOffline', 'userid'),
			'bank' => array(self::BELONGS_TO, 'Bank', 'bank_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'exp_id' => 'Expense',
			'date' => 'Expense Date',
			'projectid' => 'Project',
			'bill_id' => 'Bill',
			'invoice_id' => 'Invoice',
			'subcontractor_id' => 'Subcontractor',
			'exptype' => 'Expensetype',
			'expense_type' => 'Expense Type',
			'vendor_id' => 'Vendor',
			'expense_amount' => 'Expense Amount',
			'expense_sgstp' => 'Expense Sgstp',
			'expense_sgst' => 'Expense Sgst',
			'expense_cgstp' => 'Expense Cgstp',
			'expense_cgst' => 'Expense Cgst',
			'expense_igstp' => 'Expense Igstp',
			'expense_igst' => 'Expense Igst',
			'expense_tdsp' => 'Expense Tdsp',
			'expense_tds' => 'Expense Tds',
			'amount' => 'Expense Totalamount',
			'description' => 'Expense Description',
			'type' => 'Type',
			'payment_type' => 'Receipt Type',
			'bank_id' => 'Bank',
			'cheque_no' => 'Cheque No',
			'receipt' => 'Receipt',
			'works_done' => 'Works Done',
			'materials' => 'Materials',
			'purchase_type' => 'Purchase Type',
			'paid' => 'Paidamount',
			'paidamount' => 'Amount To Be Paid',
			'userid' => 'User',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
			'data_entry' => 'Data Entry',
			'company_id' => 'Company',
			'reconciliation_status' => 'Reconciliation Status',
			'reconciliation_date' => 'Reconciliation Date',
			'employee_id' => 'Employee',
			'budget_percentage' => 'Budget Percentage',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('exp_id', $this->exp_id);
		$criteria->compare('projectid', $this->projectid);
		$criteria->compare('bill_id', $this->bill_id);
		$criteria->compare('invoice_id', $this->invoice_id);
		$criteria->compare('subcontractor_id', $this->subcontractor_id);
		$criteria->compare('userid', $this->userid);
		$criteria->compare('date', $this->date, true);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('type', $this->type);
		$criteria->compare('exptype', $this->exptype);
		$criteria->compare('expense_type', $this->expense_type);
		$criteria->compare('vendor_id', $this->vendor_id);
		$criteria->compare('expense_amount', $this->expense_amount);
		$criteria->compare('expense_sgstp', $this->expense_sgstp);
		$criteria->compare('expense_sgst', $this->expense_sgst);
		$criteria->compare('expense_cgstp', $this->expense_cgstp);
		$criteria->compare('expense_cgst', $this->expense_cgst);
		$criteria->compare('expense_igstp', $this->expense_igstp);
		$criteria->compare('expense_igst', $this->expense_igst);
		$criteria->compare('expense_tdsp', $this->expense_tdsp);
		$criteria->compare('expense_tds', $this->expense_tds);
		$criteria->compare('payment_type', $this->payment_type);
		$criteria->compare('bank_id', $this->bank_id);
		$criteria->compare('cheque_no', $this->cheque_no);
		$criteria->compare('receipt', $this->receipt);
		$criteria->compare('works_done', $this->works_done, true);
		$criteria->compare('materials', $this->materials, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);
		$criteria->compare('purchase_type', $this->purchase_type);
		$criteria->compare('paid', $this->paid);
		$criteria->compare('paidamount', $this->paidamount);
		$criteria->compare('data_entry', $this->data_entry, true);
		$criteria->compare('company_id', $this->company_id);
		$criteria->compare('reconciliation_status', $this->reconciliation_status);
		$criteria->compare('reconciliation_date', $this->reconciliation_date);
		$criteria->compare('employee_id', $this->employee_id);
		$criteria->compare('budget_percentage', $this->budget_percentage);
		$criteria->compare('payment_quotation_status', $this->payment_quotation_status);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
	public function usersearch()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('exp_id', $this->exp_id);
		$criteria->compare('projectid', $this->projectid);
		$criteria->compare('bill_id', $this->bill_id);
		$criteria->compare('invoice_id', $this->invoice_id);
		$criteria->compare('subcontractor_id', $this->subcontractor_id);
		$criteria->compare('userid', $this->userid);
		$criteria->compare('date', $this->date, true);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('type', $this->type);
		$criteria->compare('exptype', $this->exptype);
		$criteria->compare('expense_type', $this->expense_type);
		$criteria->compare('vendor_id', $this->vendor_id);
		$criteria->compare('expense_amount', $this->expense_amount);
		$criteria->compare('expense_sgstp', $this->expense_sgstp);
		$criteria->compare('expense_sgst', $this->expense_sgst);
		$criteria->compare('expense_cgstp', $this->expense_cgstp);
		$criteria->compare('expense_cgst', $this->expense_cgst);
		$criteria->compare('expense_igstp', $this->expense_igstp);
		$criteria->compare('expense_igst', $this->expense_igst);
		$criteria->compare('expense_tdsp', $this->expense_tdsp);
		$criteria->compare('expense_tds', $this->expense_tds);
		$criteria->compare('payment_type', $this->payment_type);
		$criteria->compare('bank_id', $this->bank_id);
		$criteria->compare('cheque_no', $this->cheque_no);
		$criteria->compare('receipt', $this->receipt);
		$criteria->compare('works_done', $this->works_done, true);
		$criteria->compare('materials', $this->materials, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);
		$criteria->compare('purchase_type', $this->purchase_type);
		$criteria->compare('paid', $this->paid);
		$criteria->compare('paidamount', $this->paidamount);
		$criteria->compare('data_entry', $this->data_entry, true);
		$criteria->compare('company_id', $this->company_id);
		$criteria->compare('reconciliation_status', $this->reconciliation_status);
		$criteria->compare('reconciliation_date', $this->reconciliation_date);
		$criteria->compare('employee_id', $this->employee_id);
		$criteria->compare('budget_percentage', $this->budget_percentage);
		$criteria->compare('payment_quotation_status', $this->payment_quotation_status);
		if (!empty($this->fromdate) && !empty($this->todate)) {
			$criteria->addCondition("date >= '" . date('Y-m-d', strtotime($this->fromdate)) . "'");
			$criteria->addCondition("date <= '" . date('Y-m-d', strtotime($this->todate)) . "'");
		} else if (empty($this->fromdate) && !empty($this->todate)) {
			$criteria->addCondition("date <= '" . date('Y-m-d', strtotime($this->todate)) . "'");
		} else if (!empty($this->fromdate) && empty($this->todate)) {
			$criteria->addCondition("date >= '" . date('Y-m-d', strtotime($this->fromdate)) . "'");
		}
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => false,
		));
	}
	public function tdssearch()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$criteria->compare('projectid', $this->projectid);
		$criteria->compare('bill_id', $this->bill_id);
		$criteria->compare('invoice_id', $this->invoice_id);
		$criteria->compare('userid', $this->userid);
		$criteria->compare('date', $this->date, true);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('type', 73);
		$criteria->compare('exptype', $this->exptype);
		$criteria->compare('expense_type', $this->expense_type);
		$criteria->compare('vendor_id', $this->vendor_id);
		$criteria->compare('expense_tds', $this->expense_tds);
		$criteria->compare('payment_type', $this->payment_type);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);
		$criteria->compare('purchase_type', $this->purchase_type);
		$criteria->compare('employee_id', $this->employee_id);
		$criteria->compare('payment_quotation_status', $this->payment_quotation_status);
		if (!empty($this->paymentids)) {
			$criteria->addCondition('subcontractor_id IN (' . $this->paymentids . ')');
		}
		$criteria->addCondition('expense_tdsp > 0');
		if (!empty($this->fromdate) && !empty($this->todate)) {
			$criteria->addCondition("date >= '" . date('Y-m-d', strtotime($this->fromdate)) . "'");
			$criteria->addCondition("date <= '" . date('Y-m-d', strtotime($this->todate)) . "'");
		} else if (empty($this->fromdate) && !empty($this->todate)) {
			$criteria->addCondition("date <= '" . date('Y-m-d', strtotime($this->todate)) . "'");
		} else if (!empty($this->fromdate) && empty($this->todate)) {
			$criteria->addCondition("date >= '" . date('Y-m-d', strtotime($this->fromdate)) . "'");
		}
		if (empty($this->company_id)) {
			$user = Users::model()->findByPk(Yii::app()->user->id);
			$arrVal = explode(',', $user->company_id);
			$newQuery = "";
			foreach ($arrVal as $arr) {
				if ($newQuery) $newQuery .= ' OR';
				$newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
			}
			$criteria->addCondition($newQuery);
		} else {
			$criteria->compare('company_id', $this->company_id);
		}
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => false,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Expenses the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function paymentreportcash($fromdate, $todate, $company_id, $companystatus, $projectSort)
	{

		$criteria = new CDbCriteria;
		$criteria->join = 'LEFT JOIN jp_projects p ON t.projectid = p.pid';
		if (!empty($fromdate) && empty($todate)) {
			$criteria->addCondition("t.date >= '" . date('Y-m-d', strtotime($fromdate)) . "'");  // date is database date column field
		} elseif (!empty($todate) && empty($fromdate)) {
			$criteria->addCondition("t.date <= '" . date('Y-m-d', strtotime($todate)) . "'");
		} elseif (!empty($todate) && !empty($fromdate)) {
			$criteria->addCondition("t.date between '" . date('Y-m-d', strtotime($fromdate)) . "' and  '" . date('Y-m-d', strtotime($todate)) . "'");
		}
		if ($companystatus == 1) {
			$criteria->addCondition("t.company_id = " . $company_id . "");
		} else if ($companystatus == 2) {
			$criteria->addCondition("t.company_id IN (" . $company_id . ")");
		}
		$criteria->compare('type', 72);
		$criteria->compare('payment_type', 89);
		if ($projectSort == "ASC")
			$sortorder = "p.name asc";
		else if ($projectSort == "DESC")
			$sortorder = "p.name desc";
		else
			$sortorder = "date asc";

		$data = new CActiveDataProvider($this, array(
			'pagination' => false,
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => $sortorder,
			),
		));

		Yii::app()->user->setState("paymentcash", $data); 

		return $data;
	}
	public function paymentreportbank($fromdate, $todate, $company_id, $companystatus, $projectSort)
	{

		$criteria = new CDbCriteria;
		$criteria->join = 'LEFT JOIN jp_projects p ON t.projectid = p.pid';
		if (!empty($fromdate) && empty($todate)) {;
			$criteria->addCondition("t.date >= '" . date('Y-m-d', strtotime($fromdate)) . "'");
		} elseif (!empty($todate) && empty($fromdate)) {
			$criteria->addCondition("t.date <= '" . date('Y-m-d', strtotime($todate)) . "'");
		} elseif (!empty($todate) && !empty($fromdate)) {

			$criteria->addCondition("t.date between '" . date('Y-m-d', strtotime($fromdate)) . "' and  '" . date('Y-m-d', strtotime($todate)) . "'");
		}
		if ($companystatus == 1) {
			$criteria->addCondition("t.company_id = " . $company_id . "");
		} else if ($companystatus == 2) {
			$criteria->addCondition("t.company_id IN (" . $company_id . ")");
		}
		$criteria->compare('type', 72);
		$criteria->compare('payment_type', 88);
		if ($projectSort == "ASC")
			$sortorder = "p.name asc";
		else if ($projectSort == "DESC")
			$sortorder = "p.name desc";
		else
			$sortorder = "date asc";
		$payementbank =  new CActiveDataProvider($this, array(
			'pagination' => array('pageSize' => 25,),
			'criteria' => $criteria, 'sort' => array(
				'defaultOrder' => $sortorder,
			),
		));
		Yii::app()->user->setState("payementbank", $payementbank);
		

		return new CActiveDataProvider($this, array(
			'pagination' => false,
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => $sortorder,
			),
		));
	}
	public function grandtotalcredit($where)
	{
		
		$tblpx = Yii::app()->db->tablePrefix;
		$datasql = "SELECT sum(amount) as sumamt,sum(paid) "
		. " as sumpaid,sum(receipt) as sumreceived FROM `{$tblpx}expenses` "
		. " left join {$tblpx}projects on projectid = pid "
		. " left join {$tblpx}users "
		. " on {$tblpx}users.userid = {$tblpx}expenses.userid "
		. " left join {$tblpx}status " 
		. " on sid = {$tblpx}expenses.type left join {$tblpx}vendors "
		. " on {$tblpx}vendors.vendor_id = {$tblpx}expenses.vendor_id "
		." left join {$tblpx}expense_type "
		. " on exptype = type_id " . $where . " AND {$tblpx}expenses.type=72";
		//die($datasql);
		
		$data = Yii::app()->db->createCommand($datasql)->queryAll();
		Yii::app()->user->setState("grandtotalcredit", $data); 
		return $data;
	}
	public function grandtotaldebit($where)
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$datasql = "SELECT sum(amount) as sumamt,sum(CASE WHEN paid = 0.00 THEN paidamount ELSE paid END) as sumpaid FROM `{$tblpx}expenses` 
		left join {$tblpx}projects on projectid = pid left join
		 {$tblpx}users on {$tblpx}users.userid = {$tblpx}expenses.userid left join
		  {$tblpx}status on sid = {$tblpx}expenses.type left join {$tblpx}vendors on
		   {$tblpx}vendors.vendor_id = {$tblpx}expenses.vendor_id left join {$tblpx}expense_type 
		   on exptype = type_id " . $where . " AND {$tblpx}expenses.type=73";
		  // die($datasql);
		$data = Yii::app()->db->createCommand($datasql)->queryAll();
		
		Yii::app()->user->setState("grandtotaldebit", $data); 
		return $data;
	}















	public function getTotalDuplicateEntryCount()
	{
		$tblpx  = Yii::app()->db->tablePrefix;

		$duplicate_expense_sql = "SELECT  count(e.projectid) as rowcount, e.* "
		. " FROM {$tblpx}expenses e WHERE subcontractor_id IS NULL "
		. " AND  duplicate_ignore_status ='0' AND duplicate_delete_status='0' "
		. " GROUP BY e.projectid, e.amount, e.paid, e.date,e.description
		HAVING COUNT(e.projectid) > 1";
		$duplicate_expense  = Yii::app()->db->createCommand($duplicate_expense_sql)->queryAll();

		$duplicate_daily_expense_sql = "SELECT  count(d.dailyexp_id) as rowcount, d.* "
		. " FROM {$tblpx}dailyexpense d WHERE d.dailyexpense_paidamount IS NOT NULL "
		. " AND  duplicate_ignore_status ='0' AND duplicate_delete_status='0' "
		. " GROUP BY d.amount, d.dailyexpense_paidamount, d.date,d.description HAVING COUNT(d.dailyexp_id) > 1";
		$duplicate_daily_expense  = Yii::app()->db->createCommand($duplicate_daily_expense_sql)->queryAll();

		$duplicate_scpayment_sql = "SELECT  count(s.payment_id) as rowcount, s.* "
		. " FROM {$tblpx}subcontractor_payment s WHERE   duplicate_ignore_status ='0' "
		. " AND duplicate_delete_status='0' GROUP BY s.subcontractor_id, "
		. " s.project_id, s.amount, s.date, s.description "
		. " HAVING COUNT(s.payment_id) > 1";
		$duplicate_scpayment  = Yii::app()->db->createCommand($duplicate_scpayment_sql)->queryAll();

		$duplicate_daily_vendor_sql = "SELECT  count(d.daily_v_id) as rowcount, d.* "
		. " FROM {$tblpx}dailyvendors d WHERE  duplicate_ignore_status ='0' "
		. " AND duplicate_delete_status='0' "
		. " GROUP BY d.vendor_id, d.date, d.amount, d.description "
		. " HAVING COUNT(d.daily_v_id) > 1";
		$duplicate_daily_vendor  = Yii::app()->db->createCommand($duplicate_daily_vendor_sql)->queryAll();

		$duplicate_bill_sql = "SELECT count(b.bill_number) as rowcount, b.* ,p.* "
		. " FROM jp_bills b LEFT JOIN jp_purchase p ON b.purchase_id=p.p_id "
		. " WHERE b.duplicate_ignore_status ='0' "
		. " GROUP BY b.bill_number, p.vendor_id HAVING COUNT(b.bill_number) > 1";
		$duplicate_bill  = Yii::app()->db->createCommand($duplicate_bill_sql)->queryAll();

		$duplicate_entry_count = count($duplicate_expense) + count($duplicate_daily_expense) + count($duplicate_scpayment) + count($duplicate_daily_vendor) + count($duplicate_bill);
		return $duplicate_entry_count;
	}

	
    public function getPaymentArray($date, $id, $status, $payment_type, $company_id, $bank_id) {

        $current_date = date('Y-m-d');
        //73=>expense , 72=>receipt
        $condition = "";
        $condition1 = "`date` BETWEEN '" . $date . "' AND '" . $current_date . "'";
        $condition2 = "";
		$condition3 = "";
		$dailyexpense_cond="";
		$dailyreciept_cond="";
		$vendor_condition="";
		$subcontractor_condition="";
		if ($id != "") {
            $condition = " AND  `exp_id` > " . $id;
        }
        if ($id != "" && $status == 1) {
            $condition = " AND  `exp_id` >= " . $id;
            $condition1 = "`date` ='" . $date . "'";
        }
		
        $company_condition = " AND company_id=  $company_id";
    

        if ($payment_type == 88) {
            $condition2 = " AND reconciliation_status = 1 AND bank_id = " . $bank_id;
			$condition3 = "  AND (expense_type=88 OR payment_type=88)  AND subcontractor_id IS NULL";
			$dailyreciept_cond =" AND (dailyexpense_receipt_type=88 AND "
			. " dailyexpense_chequeno  IS NOT NULL  "
			. " AND reconciliation_status =1"
			. " AND company_id=" . $company_id.")  "
			. " OR (dailyexpense_receipt_type=88 "
			. " AND expensehead_type=4  AND "
			. " company_id=" . $company_id.") AND "
			. " parent_status='1' AND "
			. " exp_type=72 ";
			$dailyexpense_cond =" AND (expense_type=88) AND exp_type=73 "
			. " AND parent_status='1' "
			. " AND dailyexpense_chequeno IS NOT NULL "
			. " AND reconciliation_status =1 "
			. " AND company_id=" . $company_id;
			$vendor_condition = " AND payment_type=88 ";
			$subcontractor_condition = " AND payment_type=88 AND approve_status ='Yes'  AND bank IS NOT NULL AND cheque_no IS NOT NULL ";
        }
		
		if ($payment_type == 89) {
            $condition3 = "  AND (expense_type=89 OR payment_type=89)  AND subcontractor_id IS NULL";
			$dailyreciept_cond =" AND (dailyexpense_receipt_type=89 AND "
			. " dailyexpense_chequeno IS NULL "
			. " AND reconciliation_status IS NULL "
			. " AND company_id=" . $company_id.")  "
			. " OR (dailyexpense_receipt_type=88 "
			. " AND expensehead_type=4  AND "
			. " company_id=" . $company_id.") AND "
			. " parent_status='1' AND "
			. " exp_type=72 ";
			$dailyexpense_cond =" AND (expense_type=89) AND exp_type=73 "
			. " AND parent_status='1' "
			. " AND dailyexpense_chequeno IS NULL "
			. " AND reconciliation_status IS NULL "
			. " AND company_id=" . $company_id;
			$vendor_condition = " AND payment_type=89 ";
			$subcontractor_condition = " AND payment_type=89 AND approve_status ='Yes'";
        }
		if ($payment_type == 103) {
			$tblpx = Yii::app()->db->tablePrefix;
           $user = Users::model()->findByPk(Yii::app()->user->id);
           $arrVal = explode(',', $user->company_id);
			$dailyexpense = array();
			$employee_id = '';
			$date_from = '';
			$date_to = '';
			$company_id = '';
			$daybook = array();
			$dailyexpense_refund = array();
			$dailyexpense_exp = array();
			$scpayment = array();
			$chquery1 = "";
			$chquery2 = "";
			$chcondition1 = "";
			$chcondition2 = "";
			$where ='';
			$where1 ='';
			$where2 ='';
			$where .= " AND {$tblpx}dailyexpense.date between '" . $date . "' and '" . $current_date . "'";
                $where1 .= " AND {$tblpx}expenses.date between '" . $date . "' and '" . $current_date . "'";
                $where2 .= " AND {$tblpx}subcontractor_payment.date between '" . $date . "' and '" . $current_date . "'";
			$reconcilsql = "SELECT *  FROM `jp_reconciliation` "
                . " WHERE `reconciliation_status`='0' "
                . " and reconciliation_payment='Dailyexpense Payment' and "
                . "reconciliation_table='jp_dailyexpense'";
            $reconcil_dexpense_payment_data = Yii::app()->db->createCommand($reconcilsql)->queryAll();

            $payment_cheque_array = array();
            foreach ($reconcil_dexpense_payment_data as  $data) {

                $value = "'{$data['reconciliation_chequeno']}'";
                if ($value != '') {
                    $recon_data = $value;
                    array_push($payment_cheque_array, $recon_data);
                }
            }
            $payment_cheque_array1 = implode(',', $payment_cheque_array);
            if (($payment_cheque_array1)) {
                $chquery1 = "(dailyexpense_chequeno NOT IN ( $payment_cheque_array1) OR dailyexpense_chequeno IS NULL)";
                $chcondition1 = " AND " . $chquery1 . "";
            }

            $petty_issued_id = Yii::app()->db->createCommand("SELECT `company_exp_id` 
            FROM `jp_company_expense_type` WHERE  UPPER(`name`)='PETTY CASH ISSUED'")->queryScalar();
            $dailyexpense = array();
            if($petty_issued_id){
                $dailyexpensesql = "SELECT " . $tblpx . "dailyexpense.dailyexp_id as entry_id,". $tblpx . "dailyexpense.exp_type as type,"
                 . $tblpx . "dailyexpense.dailyexpense_receipt as receipt,"
                 . $tblpx . "dailyexpense.dailyexpense_paidamount as paidamount,". $tblpx . "dailyexpense.date  FROM " . $tblpx . "dailyexpense "
                . " LEFT JOIN " . $tblpx . "users "
                . " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid"
                . " WHERE " . $tblpx . "dailyexpense.exp_type =73 "
                . " AND " . $tblpx . "dailyexpense.expense_type  IN('89','88') "  . $chcondition1 . "  "
                . " AND jp_dailyexpense.exp_type_id= $petty_issued_id " . $where . ""
                . " order by " . $tblpx . "dailyexpense.date asc";

            $dailyexpense = Yii::app()->db->createCommand($dailyexpensesql)->queryAll();
            }
            //petty cash refund
            $reconcil_receipt_sql = "SELECT *  FROM `jp_reconciliation` "
                . " WHERE `reconciliation_status`='0' "
                . " and reconciliation_payment='Dailyexpense Receipt' and "
                . "reconciliation_table='jp_dailyexpense'";
            $reconcil_dexpense_receipt_data = Yii::app()->db->createCommand($reconcil_receipt_sql)->queryAll();
            $receipt_cheque_array = array();
            foreach ($reconcil_dexpense_receipt_data as  $receiptdata) {
                $receiptvalue = "'{$receiptdata['reconciliation_chequeno']}'";
                if ($receiptvalue != '') {
                    $recon_receipt_data = $receiptvalue;
                    array_push($receipt_cheque_array, $recon_receipt_data);
                }
            }
            $receipt_cheque_array1 = implode(',', $receipt_cheque_array);
            if (($receipt_cheque_array1)) {
                $chquery2 = "(dailyexpense_chequeno NOT IN ( $receipt_cheque_array1) OR dailyexpense_chequeno IS NULL)";
                $chcondition2 = " AND " . $chquery2 . "";
            }

            $petty_refund_id = Yii::app()->db->createCommand("SELECT `company_exp_id` 
            FROM `jp_company_expense_type` WHERE  UPPER(`name`)='PETTY CASH REFUND'")->queryScalar();
            $dailyexpense_refund=array();
            if($petty_refund_id){
                $dexp_refund_sql = "SELECT " . $tblpx . "dailyexpense.dailyexp_id as entry_id,". $tblpx . "dailyexpense.exp_type as type,"
				. $tblpx . "dailyexpense.dailyexpense_receipt as receipt,"
				. $tblpx . "dailyexpense.dailyexpense_paidamount as paidamount,". $tblpx . "dailyexpense.date  FROM " . $tblpx . "dailyexpense "
                    . " LEFT JOIN " . $tblpx . "users "
                    . " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid "
                    . " WHERE " . $tblpx . "dailyexpense.exp_type =72 "
                    . " AND " . $tblpx . "dailyexpense.dailyexpense_receipt_type IN('88','89') "  . $chcondition2 . "  "
                    . "AND jp_dailyexpense.exp_type_id= $petty_refund_id  ".$where
                    . " order by " . $tblpx . "dailyexpense.date asc";
                $dailyexpense_refund = Yii::app()->db->createCommand($dexp_refund_sql)->queryAll();
				//echo "hi <pre>";print_r($dailyexpense_refund);exit;
			}

            //petty cash expense
            $daybook_sql = "SELECT " . $tblpx . "expenses.exp_id as entry_id , " . $tblpx . "expenses.type,"
			 . $tblpx . "expenses.receipt," . $tblpx . "expenses.paid as paidamount," . $tblpx . "expenses.date "
                . " FROM " . $tblpx . "expenses LEFT JOIN " . $tblpx . "users "
                . " ON " . $tblpx . "expenses.employee_id= " . $tblpx . "users.userid "
                . " LEFT JOIN " . $tblpx . "projects "
                . " ON " . $tblpx . "projects.pid = " . $tblpx . "expenses.projectid "
                . " WHERE " . $tblpx . "expenses.type = 73  "
                . " AND " . $tblpx . "expenses.expense_type=103 ".$where1
                . " GROUP BY " . $tblpx . "expenses.projectid";
            $daybook = Yii::app()->db->createCommand($daybook_sql)->queryAll();
            $dailyexpense_exp_sql = "SELECT " . $tblpx . "dailyexpense.dailyexp_id as entry_id,". $tblpx . "dailyexpense.exp_type as type,"
			. $tblpx . "dailyexpense.dailyexpense_receipt as receipt,"
			. $tblpx . "dailyexpense.dailyexpense_paidamount as paidamount,". $tblpx . "dailyexpense.date  FROM " . $tblpx . "dailyexpense "
                . " LEFT JOIN " . $tblpx . "users "
                . " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid "
                . " WHERE (" . $tblpx . "dailyexpense.dailyexpense_receipt_type = 103 "
                . " OR  " . $tblpx . "dailyexpense.expense_type = 103  )".$where
                . " order by " . $tblpx . "dailyexpense.date asc";
            $dailyexpense_exp = Yii::app()->db->createCommand($dailyexpense_exp_sql)->queryAll();
            $scp_sql = "SELECT  " . $tblpx . "subcontractor_payment.payment_id as entry_id,"
			. " ( IFNULL(jp_subcontractor_payment.amount, 0) + IFNULL(jp_subcontractor_payment.tax_amount, 0) ) 
		 as paidamount ," . $tblpx . "subcontractor_payment.date   FROM " . $tblpx . "subcontractor_payment LEFT JOIN " . $tblpx . "users "
                . " ON " . $tblpx . "subcontractor_payment.employee_id= " . $tblpx . "users.userid "
                . " LEFT JOIN " . $tblpx . "projects "
                . " ON " . $tblpx . "projects.pid = " . $tblpx . "subcontractor_payment.project_id "
                . " WHERE " . $tblpx . "subcontractor_payment.payment_type=103 ".$where2;
				//die($scp_sql)     ;       
            $scpayment = Yii::app()->db->createCommand($scp_sql)->queryAll();
			$expense = !empty($daybook) ? $daybook : array();
		$dailyexpense_only= !empty($dailyexpense_exp) ? $dailyexpense_exp : array();
        $dailyexpense = !empty($dailyexpense) ? $dailyexpense : array();
        $dailyexpense_refund = !empty($dailyexpense_refund) ? $dailyexpense_refund : array();
        $scPayment = !empty($scPayment) ? $scPayment : array();

        $final_array = array();

        foreach ($expense as $expenseData) {
            $expenseData['tablename'] = 'Daybook';
            if ($expenseData['entry_id'] != NULL) {
                $final_array[] = $expenseData;
            }
        }

        foreach ($dailyexpense as $dailyexpenseData) {
            $dailyexpenseData['tablename'] = 'Daily Expense';
            if ($dailyexpenseData['entry_id'] != NULL) {
                $final_array[] = $dailyexpenseData;
            }
        }
		foreach ($dailyexpense_only as $dailyexpenseDatas) {
            $dailyexpenseDatas['tablename'] = 'Daily Expense';
            if ($dailyexpenseDatas['entry_id'] != NULL) {
                $final_array[] = $dailyexpenseDatas;
            }
        }
		foreach ($dailyexpense_refund as $dailyexpenseDatas) {
            $dailyexpenseDatas['tablename'] = 'Daily Expense Refund';
            if ($dailyexpenseDatas['entry_id'] != NULL) {
                $final_array[] = $dailyexpenseDatas;
            }
        }

        foreach ($scPayment as $scPaymentData) {
            $scPaymentData['tablename'] = 'Subcontractor Payment';
            $scPaymentData['type'] = 73;
            $scPaymentData['receipt'] = NULL;
            if ($scPaymentData['entry_id'] != NULL) {
                $final_array[] = $scPaymentData;
            }
        }


        $date = array_column($final_array, 'date');
        array_multisort($date, SORT_ASC, $final_array);
        return $final_array;
        }


        $sql = "SELECT exp_id as entry_id , type,"
		. " receipt,paid as paidamount,date  "
		. " FROM `jp_expenses` WHERE   " . $condition1 . $condition . $condition2 .$condition3. "  AND company_id = " . $company_id ;
	    $expense = Yii::app()->db->createCommand($sql)->queryAll();
       //echo "hi <pre>";print_r($expense);exit;	

        $sql = "SELECT dailyexp_id as entry_id,exp_type as type,"
                . " dailyexpense_receipt as receipt,"
                . " dailyexpense_paidamount as paidamount,date "
                . " FROM `jp_dailyexpense`  WHERE   " . $condition1 . $condition2 . $dailyreciept_cond. " AND company_id = " . $company_id;
        $dailyexpense = Yii::app()->db->createCommand($sql)->queryAll();
       
		$sql = "SELECT dailyexp_id as entry_id,exp_type as type,"
                . " dailyexpense_receipt as receipt,"
                . " dailyexpense_paidamount as paidamount,date "
                . " FROM `jp_dailyexpense`  WHERE   " . $condition1 . $condition2 . $dailyexpense_cond;
        $dailyexpense_only = Yii::app()->db->createCommand($sql)->queryAll();
		
        if ($payment_type == 88) {
            $condition2 = " AND reconciliation_status = 1 AND bank = " . $bank_id;
			
        }
        //  ( only expense) add receipt and type column
        $sql = "SELECT daily_v_id as entry_id,"
                . " ( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) "
                . " as paidamount,date FROM `jp_dailyvendors` "
                . " WHERE   " . $condition1 . $condition2 .$vendor_condition . " AND company_id = " . $company_id;
        $vendorPayment = Yii::app()->db->createCommand($sql)->queryAll();
        //  ( only expense) add receipt and type column
        $sql = "SELECT payment_id as entry_id,"
                . " ( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) "
                . " as paidamount ,date FROM `jp_subcontractor_payment`"
                . " WHERE " . $condition1 . $condition2 .$subcontractor_condition. " AND company_id = " . $company_id;
        $scPayment = Yii::app()->db->createCommand($sql)->queryAll();
		    
        $expense = !empty($expense) ? $expense : array();
		$dailyexpense_only= !empty($dailyexpense_only) ? $dailyexpense_only : array();
        $dailyexpense = !empty($dailyexpense) ? $dailyexpense : array();
        $vendorPayment = !empty($vendorPayment) ? $vendorPayment : array();
        $scPayment = !empty($scPayment) ? $scPayment : array();

        $final_array = array();

        foreach ($expense as $expenseData) {
            $expenseData['tablename'] = 'Daybook';
            if ($expenseData['entry_id'] != NULL) {
                $final_array[] = $expenseData;
            }
        }

        foreach ($dailyexpense as $dailyexpenseData) {
            $dailyexpenseData['tablename'] = 'Daily Expense';
            if ($dailyexpenseData['entry_id'] != NULL) {
                $final_array[] = $dailyexpenseData;
            }
        }
		foreach ($dailyexpense_only as $dailyexpenseDatas) {
            $dailyexpenseDatas['tablename'] = 'Daily Expense';
            if ($dailyexpenseDatas['entry_id'] != NULL) {
                $final_array[] = $dailyexpenseDatas;
            }
        }

        foreach ($vendorPayment as $vendorPaymentData) {
            $vendorPaymentData['tablename'] = 'Vendor Payment';
            $vendorPaymentData['type'] = 73;
            $vendorPaymentData['receipt'] = NULL;
            if ($vendorPaymentData['entry_id'] != NULL) {
                $final_array[] = $vendorPaymentData;
            }
        }
        foreach ($scPayment as $scPaymentData) {
            $scPaymentData['tablename'] = 'Subcontractor Payment';
            $scPaymentData['type'] = 73;
            $scPaymentData['receipt'] = NULL;
            if ($scPaymentData['entry_id'] != NULL) {
                $final_array[] = $scPaymentData;
            }
        }
        $date = array_column($final_array, 'date');
        array_multisort($date, SORT_ASC, $final_array);
        return $final_array;

    }

    public function checkDeleteBalance($data) {

        if ($data['type'] == 72) {
			
            $exp_id = $data['exp_id'];
            $date = date('Y-m-d', strtotime($data['date']));
            $paymentArray = $this->getPaymentArray($date, $exp_id, $status = 0, $data['payment_type'], $data['company_id'], $data['bank_id']); //get the enetries after the selected entry
            //find the closing balance on the selected entry date
            $chekBalance = "";
            $invoice_amount = 0;
            $expense_amount = 0;
            $payment_type = 0;
            $available_amount = "";
            $company_id = $data['company_id'];
			
            if ($data['payment_type'] == '89' || $data['payment_type'] == '88' || $data['payment_type'] == '103') {
                $payment_type = $data['payment_type'];

            }
			
            if (!empty($company_id)) {
                $invoice_amount = BalanceHelper::cashInvoice($payment_type, $company_id, $data['employee_id'], $data['bank_id'], $date);
                $expense_amount = BalanceHelper::cashExpense($payment_type, $company_id, $data['employee_id'], $exp_id, $data['bank_id'], $date);

                if (is_numeric($invoice_amount) && is_numeric($expense_amount)) {
                    $available_amount = $invoice_amount - $expense_amount; //default closing balance
                }

                // get the  available amount just before the deleted entry
                $chekBalanceArray = $this->getPaymentArray($date, $exp_id, $status = 1, $payment_type, $company_id, $data['bank_id']);
                $chekBalance = $available_amount;
				
                foreach ($chekBalanceArray as $data) {
                    if ($data['type'] == 73) { //expense
                        $chekBalance += $data['paidamount'];
                    } else {
                        $chekBalance -= $data['receipt'];
                    }
                }
				
                $closingBalance = $chekBalance;
                $checkArray = array();

                foreach ($paymentArray as $data) {
                    if ($data['type'] == 73) { //expense
                        $closingBalance -= $data['paidamount'];
                    } else {
                        $closingBalance += $data['receipt'];
                    }
                    array_push($checkArray, $closingBalance);
                    if ($closingBalance < 0) {
                        return "Can't delete ! Insufficient balance on " . $data['date'] . " in " . $data['tablename'] . " - ID: #" . $data['entry_id'];
                    }
                }
            }
            return '1';
        } else {
            return '1';
        }
    }

}
