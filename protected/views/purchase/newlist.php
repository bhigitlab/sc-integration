<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript"
    src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>

<?php
Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".info").animate({opacity: 1.0}, 3000).fadeOut("slow");',
    CClientScript::POS_READY
);
?>
<?php
$this->breadcrumbs = array(
    'Purchase List',
)
    ?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<div class="container" id="project">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
    <div class="expenses-heading header-container">
        <h3>Purchase</h3>
        <div class="btn-container">


            <?php
            if (isset(Yii::app()->user->role) && (in_array('/purchase/addpurchase', Yii::app()->user->menuauthlist))) {
                ?>
                <button type="button" id="add-po" class="btn btn-info"
                    href="index.php?r=purchase/addpurchase">Add PO By Quantity</button>
                <button type="button" id="add-po-gls" class="btn btn-info" href="index.php?r=purchase/addglasspurchase">Add PO by Width x Height</button>
                <button type="button" id="add-po-len" class="btn btn-info"href="index.php?r=purchase/addpurchasebylength">Add PO by Length</button>
            <?php } ?>
        </div>
    </div>

    <?php
    $user = Users::model()->findByPk(Yii::app()->user->id);
    $arrVal = explode(',', $user->company_id);
    $newQuery = "";
    foreach ($arrVal as $arr) {
        if ($newQuery)
            $newQuery .= ' OR';
        $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
    }
    ?>

    <?php $this->renderPartial('_newsearch', array('model' => $model, 'project' => $project, 'vendor' => $vendor, 'purchase_no' => $purchase_no, 'expense' => $expense)) ?>

    <?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="info delete_msg text-center">
            <h4>
                <?php echo Yii::app()->user->getFlash('success'); ?>
            </h4>
        </div>
    <?php endif; ?>


    <?php if (Yii::app()->user->hasFlash('error')): ?>
        <div class="info"
            style="width: 500px;margin-left: 400px;padding: 8px;color: red;font-size: 20px;font-weight: bold; padding-top: 46px;">
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
    <?php endif; ?>

    <?php
    $this->renderPartial('_remarks', array('model' => $model));
    ?>

    <div id="msg_box"></div>

    <div class="table-wrapper">
        <?php
        $total_blanceamount = $model->getTotalBalanceAmountDataprovider($dataProvider->getData());
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'viewData' => array('total_billedamount' => $total_billedamount, 'total_blanceamount' => $total_blanceamount),
            'pager' => array('header' => '', ),
            'itemView' => '_newview',
            'template' => '<div id="parent" class="table-wrapper margin-top-10"><div class="clearfix"><div class="center-content"> <ul class="legend margin-bottom-0">
                <li><span class="approval_needed"></span> Approval Needed</li>
            <li><span class="declined"></span> Declined</li>
        </ul>{sorter}{summary}</div></div><div id="table-wrapper"><table cellpadding="10" class="table total-table " id="fixtable">{items}</table></div>{pager}</div>',
            'emptyText' => '<table cellpadding="10" class="table"><thead class="entry-table">
            <tr>
                <th>No</th>
                <th>Purchase No</th>
                <th>Company</th>
                <th>Project</th>
                <th>Expense Head</th>
                <th>Vendor</th>            
                <th>Total Amount</th>
                <th>Date</th>
                <th>Status</th>
                <th>Expected Delivery Date</th>
                <th>Total amount billed</th>
                <th>Balance to be billed</th>
                <th>Billing Status</th>
                <th>MR Status</th>
                <th></th>
             </tr></thead><tr><td colspan="13" class="text-center">No results found</td></tr></table>',
        ));
        ?>
    </div>
</div>

<script>
    var url = '<?php echo Yii::app()->getBaseUrl(true); ?>';
    $(document).ready(function () {
        $('#loading').hide();
        $("#parent").css('max-height', $(window).height() - $("#myNavbar").height() - $(".search-form").height() - (2 * $('footer').height()));
        $("#fixtable").tableHeadFixer({
            'left': false,
            'foot': true,
            'head': true
        });


        $(".popover-test").popover({
            html: true,
            content: function () {
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function (e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function (e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function (e) {
            $('[data-toggle=popover]').each(function () {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
        $(document).ajaxComplete(function () {
            $(".popover-test").popover({
                html: true,
                content: function () {
                    return $(this).next('.popover-content').html();
                }
            });
            $("#fixtable").tableHeadFixer({
                'left': false,
                'foot': true,
                'head': true
            });

        });

        $(document).on('click', '.update_status', function (e) {
            e.preventDefault();
            var element = $(this);
            var purchase_id = $(this).attr('id');
            if (confirm("Are you sure you want to change the status?")) {
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/updatepurchasestatus'); ?>',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        purchase_id: purchase_id
                    },
                    success: function (response) {
                        console.log(response);
                        if (response.response == 'success') {
                            $(".purchase_status_" + purchase_id).html('Saved');
                            $(".edit_option_" + purchase_id).hide();
                            element.closest('tr').find('.bill_label').text('Pending to be Billed');
                            element.closest('tr').find('.delete_po ').hide();
                            element.closest('tr').find('.bill_status').addClass('bill_class');
                            $("#msg_box").html('<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Success!</strong>  &nbsp;' + response.msg + '</div>');
                        } else {

                            $("#msg_box").html('<div class="alert alert-warning alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Sorry!</strong>  &nbsp;' + response.msg + '</div>');
                        }
                    }
                });
            } else {
                return false;
            }
        });


        $('.view_purchase').click(function () {
            var val = $(this).attr('id');
            location.href = url + '/index.php?r=purchase/viewpurchase&pid=' + val;
        })
    });



    $(document).on('click', '.edit_purchase', function () {
        var bill_count = $(this).attr('bill-count');
        if (bill_count == 0) {
            var val = $(this).attr('id');
            var pType = $(this).attr('data-id');
            if (pType == "A") {
                location.href = url + '/index.php?r=purchase/updatepurchasebylength&pid=' + val;
            } else if (pType == "G") {
                location.href = url + '/index.php?r=purchase/updatepurchaseglass&pid=' + val;
            } else if (pType == "O") {
                location.href = url + '/index.php?r=purchase/updatepurchase&pid=' + val;
            } else {
                location.href = url + '/index.php?r=purchase/updatepurchase&pid=' + val;
            }
        } else {
            alert("Can't Update ! Bills exist.");
            return;
        }

    });


    $(document).on('click', '.delete_bill', function () {
        var val = $(this).attr('id');
        location.href = url + '/index.php?r=purchase/deletepurchasebill&pid=' + val;
    })

    $(document).on('click', '.view_purchase', function () {
        var val = $(this).attr('id');
        location.href = url + '/index.php?r=purchase/viewpurchase&pid=' + val;
    })


    $(document).on('click', '.permission_item', function (e) {
        e.preventDefault();
        var element = $(this);
        var item_id = $(this).attr('id');
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/permission'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                item_id: item_id
            },
            success: function (response) {
                $(".popover").removeClass("in");
                if (response.response == 'success') {
                    element.closest('tr').removeClass('permission_style');
                    $(".approveoption_" + item_id).hide();
                    $().toastmessage('showSuccessToast', "" + response.msg + "");
                    $('.approvallabel_' + item_id).html('Draft<span class="fa fa-save"></span>');
                    $('.approvallabel_' + item_id).css("cursor", "pointer");
                    $('.approvallabel_' + item_id).addClass('update_status');
                } else if (response.response == 'warning') {
                    $(".approveoption_" + item_id).hide();
                    element.closest('tr').removeClass('permission_style');
                    $().toastmessage('showWarningToast', "" + response.msg + "");
                } else {
                    $().toastmessage('showErrorToast', "" + response.msg + "");
                }
            }
        });
    });

    $(document).on('click', '.delete_po', function (e) {
        e.preventDefault();
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            var element = $(this);
            var item_id = $(this).attr('id');
            $.ajax({
                url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/deletepurchaseorder'); ?>',
                type: 'POST',
                dataType: 'json',
                data: {
                    item_id: item_id
                },
                success: function (response) {
                    console.log(response)
                    $(".popover").removeClass("in");
                    if (response.status == '1') {
                        $().toastmessage('showSuccessToast', "" + response.msg + "");
                        window.location.reload();
                    } else if (response.status == '0') {
                        $().toastmessage('showWarningToast', "" + response.msg + "");
                    } else {
                        $().toastmessage('showErrorToast', "" + response.msg + "");
                    }
                }
            });
        }
    });

    $(document).ajaxComplete(function () {
        $("#parent").css('max-height', $(window).height() - $("#myNavbar").height() - $(".search-form").height() - (2 * $('footer').height()));
    });
    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });

    function closeaction() {
        $('#remark').val('');
        $('#addremark').hide("slide", { direction: "right" }, 200);
        setTimeout(function () {
            window.location.reload(1);
        }, 2000);
    }

    function addremark(pur_order, event) {
        var id = pur_order;
        $(".popover").removeClass("in");
        $('.remarkadd').show();
        $('.addnow').hide();
        $('#addremark').show("slide", { direction: "right" }, 500);
        $("#txtPurchaseId").val(id);
        event.preventDefault();
        $.ajax({
            type: "GET",
            data: { purchaseid: id },
            dataType: 'json',
            url: '<?php echo Yii::app()->createUrl('purchase/viewremarks'); ?>',
            success: function (response) {
                $("#remarkList").html(response.result);
                $(".decline_comment").html(response.comment);
                $(".po_no").html(response.po_no)
                $("#txtPurchaseId").val(id);
            }
        });

    }

    $(document).on('click', '#remarkSubmit', function (e) {
        var purchaseId = $("#txtPurchaseId").val();
        var remark = $("#remark").val();
        $.ajax({
            method: "GET",
            data: { purchaseid: purchaseId, remark: remark },
            url: '<?php echo Yii::app()->createUrl('purchase/addremarks'); ?>',
            success: function (response) {
                $("#remarkList").html(response);
                $('#addremark').show("slide", { direction: "right" }, 1000);
                $("#remark").val("");
            }
        });
    });
    $("#add-po").click(function () {
        var redirectUrl = $(this).attr("href");
        window.location.href = redirectUrl;
    });
    $("#add-po-len").click(function () {
        var redirectUrl = $(this).attr("href");
        window.location.href = redirectUrl;
    });
    $("#add-po-gls").click(function () {
        var redirectUrl = $(this).attr("href");
        window.location.href = redirectUrl;
    });
</script>

<style>
    .toast-container {
        width: 350px;
    }

    .toast-position-top-right {
        top: 57px;
        right: 6px;
    }

    .toast-item-close {
        background-image: none;
        cursor: pointer;
        width: 12px;
        height: 12px;
        text-align: center;
        border-radius: 2px;
    }

    .toast-item-image {
        font-size: 24px;
    }

    .toast-item-close:hover {
        color: red;
    }

    .toast-item {
        border: transparent;
        border-radius: 3px;
        font-size: 10px;
        opacity: 1;
        background-color: rgba(34, 45, 50, 0.8);
    }

    .toast-item-wrapper p {
        margin: 0px 5px 0px 42px;
        font-size: 14px;
        text-align: justify;
    }

    .toast-type-success {
        background-color: #00A65A;
        border-color: #00A65A;
    }

    .toast-type-error {
        background-color: #DD4B39;
        border-color: #DD4B39;
    }

    .toast-type-notice {
        background-color: #00C0EF;
        border-color: #00C0EF;
    }

    .toast-type-warning {
        background-color: #F39C12;
        border-color: #F39C12;
    }

    .legend {
        list-style: none;
        padding-left: 0px;

    }

    .legend li {
        float: left;
        margin-right: 10px;
    }

    .legend span {
        border: 1px solid #ccc;
        float: left;
        width: 12px;
        height: 12px;
        margin: 2px;
    }

    /* your colors */
    .legend .declined {
        background-color: #ffa50057;
        border: 2px solid #ffe0a8;
        margin-right: 4px;
    }

    .legend .approval_needed {
        background-color: #f8cbcb;
        border: 2px solid #f8cbcb;
        margin-right: 4px;
    }

    .badge1 {
        position: absolute;
        right: 0px;
        top: 0px;
        color: #0093dd;
        font-weight: 900;
        font-size: 11px;
    }

    .pur_remark {
        font-weight: 600;
        vertical-align: middle !important;
        white-space: nowrap;
        position: relative;
    }

    #addremark .all_remarks {
        overflow: auto;
        margin-top: 5px;
        max-height: 150px;
        border-top: 1px solid #ddd;
    }
</style>