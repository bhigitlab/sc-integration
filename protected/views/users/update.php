<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->userid=>array('view','id'=>$model->userid),
	'Update',
);
?>

<div class="panel panel-gray">
    <div class="panel-heading form-head">
        <h3 class="panel-title">Update User</h3>
    </div>
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>

