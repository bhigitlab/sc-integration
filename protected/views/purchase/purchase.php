<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/purchase.css">
<script type="text/javascript"
    src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/moment.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<script src="<?php echo Yii::app()->baseUrl . '/ckeditor/ckeditor.js'; ?>"></script>



<script>
    var shim = (function () {
        document.createElement('datalist');
    })();
</script>

<script>
    $(function () {
        $("#datepicker").datepicker({
            dateFormat: 'dd-mm-yy'
        }).datepicker("setDate", new Date());
        $("#delivery_date").datepicker({
            dateFormat: 'dd-mm-yy'
        });
    });
</script>
<?php
$tblpx = Yii::app()->db->tablePrefix;
$purchase_details = Yii::app()->db->createCommand("SELECT purchase_no FROM jp_purchase WHERE purchase_no IS NOT NULL ORDER BY p_id DESC LIMIT 0,1")->queryRow();
$lastpurchase_no = $purchase_details['purchase_no'];
if (is_numeric($lastpurchase_no)) {
    $lastpurchase_no = $lastpurchase_no + 1;
} else {
    $p_no = substr($lastpurchase_no, 0, -1);
    $last = substr($lastpurchase_no, -1);
    if (is_numeric($last)) {
        $last_no = $last + 1;
    } else {
        $last_no = $last . '1';
    }
    $lastpurchase_no = $p_no . $last_no;
}

$purchase_no = Purchase::model()->findAll(array("condition" => "purchase_no = '$lastpurchase_no'"));
if (!empty($purchase_no)) {
    if (is_numeric($lastpurchase_no)) {
        $lastpurchase_no = $lastpurchase_no + 1;
    } else {
        $p_no = substr($lastpurchase_no, 0, -1);
        $last = substr($lastpurchase_no, -1);
        if (is_numeric($last)) {
            $last_no = $last + 1;
        } else {
            $last_no = $last . '1';
        }
        $lastpurchase_no = $p_no . $last_no;
    }
}

$company_details = Company::model()->findByPk(Yii::app()->user->company_id);
$companyList = Yii::app()->user->company_ids;
?>
<div class="container">
    <div class="expenses-heading">
        <!-- remove addentries class -->
        <h3>Purchase Order</h3>
    </div>
    <div class="entries-wrapper">
        <input type="hidden" id="mrId" value="">
        <?php $this->renderPartial('_purchase_details_form'); ?>
        <?php $this->renderPartial('_purchase_items_form', array('specification' => $specification)); ?>
        <div class="margin-horizontal-10 margin-top-20">
            <div class="horizontal-scroll">
                <table class="table" id="main_table">
                    <thead class="entry-table">
                        <tr>
                            <th>Sl.No</th>
                            <th>Specification</th>
                            <th>HSN Code</th>
                            <th>Quantity</th>
                            <th>Unit</th>
                            <th>Rate</th>
                            <th>Base Quantity</th>
                            <th>Base Unit</th>
                            <th>Base Rate</th>
                            <th>Amount</th>
                            <th>Tax Slab (%)</th>
                            <th>SGST Amount</th>
                            <th>SGST (%)</th>
                            <th>CGST Amount</th>
                            <th>CGST (%)</th>
                            <th>IGST Amount</th>
                            <th>IGST (%)</th>
                            <th>Discount (%)</th>
                            <th>Discount Amount</th>
                            <th>Tax Amount</th>
                            <th>Total</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="addrow">
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="18" class="text-right">TOTAL: </th>
                            <th class="text-right">
                                <div id="discount_total">0.00</div>
                            </th>
                            <th class="text-right">
                                <div id="tax_total">0.00</div>
                            </th>
                            <th class="text-right">
                                <div id="amount_total">0.00</div>
                            </th>
                            <th></th>
                        </tr>
                        <tr>
                            <th colspan="22" class="text-right">GRAND TOTAL:
                                <span id="grand_total"> 0.00</span>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="22" class="text-right">ADDITIONAL CHARGE:
                                <span id="additional_charge"> 0.00</span>
                            </th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!-- ui changes ends -->

        <div style="padding-right: 0px;"
            class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6">
            <input type="hidden" value="<?php echo ((isset($subtot) && $subtot != '') ? $subtot : ''); ?>"
                class="txtBox pastweek" readonly=ture name="subtot" /></td>
            <input type="hidden" value="<?php echo ((isset($grand) && $grand != '') ? $grand : ''); ?>"
                class="txtBox pastweek grand" name="grand" readonly=true />
            <input type="hidden" name="previousvalue" id="previousvalue" value="0">
        </div>
        <div id="loader"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
        <div class="mt text-center">
            <a id="mail_send" class="mail_send btn btn-primary" href="#" title="Mail Send"> Save</a>
        </div>
        </form>
        <?php
        $this->renderPartial("_purchase_additional_charge");
        ?>
    </div>
</div>
<input type="hidden" name="final_amount" id="final_amount" value="0">
<input type="hidden" name="item_amount_temp" id="item_amount_temp" value="0">

<script>
    var $ = jQuery.noConflict();
    $(document).ready(function () {


        // $("#main_table").tableHeadFixer({
        //     'left': false,
        //     'foot': true,
        //     'head': true
        // });
        $(".popover-test").popover({
            html: true,
            content: function () {
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function (e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function (e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function (e) {
            $('[data-toggle=popover]').each(function () {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
        $(document).ajaxComplete(function () {
            // $(".popover-test").popover({
            //     html: true,
            //     content: function() {
            //         return $(this).next('.popover-content').html();
            //     }
            // });
        });
    });
    $('.mail_send').hide();

    jQuery.extend(jQuery.expr[':'], {
        focusable: function (el, index, selector) {
            return $(el).is('button, :input, [tabindex]');
        }
    });
    $(document).on('keypress', 'input,select', function (e) {
        if (e.which == 13) {
            e.preventDefault();
            var $canfocus = $(':focusable');
            var index = $canfocus.index(document.activeElement) + 1;
            if (index >= $canfocus.length) index = 0;
            $canfocus.eq(index).focus();
        }
    });
    $(document).ready(function () {
        $('#loading').hide();
        $(".js-example-basic-single").select2();
        $(".project").select2();
        $(".vendor").select2();
        $(".company").select2();
        $(".expense_head").select2();
    });

</script>
<script>
    $(document).ready(function () {
        // $().toastmessage({
        //     sticky: false,
        //     position: 'top-right',
        //     // inEffectDuration: 1000,
        //     stayTime: 7000,
        //     closeText: '<i class="icon ion-close-round"></i>',
        // });
        $(".purchase_items").addClass('checkek_edit');
    });

</script>
<script>









    function filterDigits(eventInstance) {
        eventInstance = eventInstance || window.event;
        key = eventInstance.keyCode || eventInstance.which;
        if ((47 < key) && (key < 58) || key == 8) {
            return true;
        } else {
            if (eventInstance.preventDefault)
                eventInstance.preventDefault();
            eventInstance.returnValue = false;
            return false;
        }
    }

    $(document).on('click', '.approve_item', function (e) {
        e.preventDefault();
        $('.loading-overlay').addClass('is-active');
        var element = $(this);
        var item_id = $(this).attr('id');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/permissionapprove'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                item_id: item_id
            },
            success: function (response) {
                if (response.response == 'success') {
                    $(".approveoption_" + item_id).hide();
                    element.closest('tr').find('.rate_highlight').removeClass('rate_highlight');
                    $(".toastmessage").addClass("alert alert-success").html("" + response.msg + "").fadeIn().delay(4000).fadeOut();
                    // $().toastmessage('showSuccessToast', "" + response.msg + "");
                    if (response.mail_send == 'Y') {
                        $('.mail_send').show();
                    } else {
                        $('.mail_send').hide();
                    }
                } else if (response.response == 'warning') {
                    $(".approveoption_" + item_id).hide();
                    element.closest('tr').find('.rate_highlight').removeClass('rate_highlight');
                    $(".toastmessage").addClass("alert alert-warning").html("" + response.msg + "").fadeIn().delay(4000).fadeOut();
                    // $().toastmessage('showWarningToast', "" + response.msg + "");
                } else {
                    $(".toastmessage").addClass("alert alert-danger").html("" + response.msg + "").fadeIn().delay(4000).fadeOut();
                    // $().toastmessage('showErrorToast', "" + response.msg + "");
                }
            }
        });
    });

    $("#purchaseno").keyup(function () {
        if (this.value.match(/[^a-zA-Z0-9]/g)) {
            this.value = this.value.replace(/[^a-zA-Z0-9\-/]/g, '');
        }
    });


</script>

<script>
    (function () {
        var mainTable = document.getElementById("main-table");
        if (mainTable !== null) {
            var tableHeight = mainTable.offsetHeight;
            if (tableHeight > 380) {
                var fauxTable = document.getElementById("faux-table");
                document.getElementById("table-wrap").className += ' ' + 'fixedON';
                var clonedElement = mainTable.cloneNode(true);
                clonedElement.id = "";
                fauxTable.appendChild(clonedElement);
            }
        }
    })();
    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });

    $("#myModal3").on('shown.bs.modal', function (e) {
        // alert("I want this to appear after the modal has opened!");
    });

    function addcomp(elem, event) {
        $('.add_new_form').show();
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: '<?php echo Yii::app()->createUrl('company/create&layout=1&popup=1'); ?>',
            success: function (response) {
                $(".add_new_form div").html(response);
            }
        });
    }

    function addproject(elem, event) {
        $('.add_new_form').show();
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: '<?php echo Yii::app()->createUrl('projects/create&layout=1'); ?>',
            success: function (response) {
                $(".add_new_form div").html(response);
            }
        });
    }

    $(document).on("click", ".close", function () {
        $(this).siblings('div').html('');
    })
    $('.addcharge').click(function () {
        $(".chargeadd").toggle();
        $(this).toggleClass("icon-minus hidecharge");
    });

    $(".savecharge").on('keyup keypress keydown click', function (e) {
        var pid = $("#purchase_id").val();

        if (pid != 0) {
            var label = $("#labelcat").val();
            var currentval = $("#currentval").val();

            if (label == '' && currentval == '') {
                $('.errorcharge').html('Both fields are mandatory');
            } else {
                $(".savecharge").attr('disabled', 'disabled');
                $.ajax({
                    url: "<?php echo Yii::app()->createAbsoluteUrl("purchase/additionalCharge"); ?>",
                    data: {
                        category: label,
                        amount: currentval,
                        pid: pid
                    },
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        // console.log(data);return;
                        $(".savecharge").removeAttr('disabled');
                        $('#labelcat').val('');
                        $('#currentval').val('');
                        $("#addtional_list").html(data.charges);
                        $("#additional_charge").text(data.total_additional_charge);
                    }
                });
            }
        } else {
            $('.errorcharge').html('Please add item');
        }
    });


    $(document).on("click", ".deletecharge", function () {

        var id = $(this).attr("id");
        var purchase_id = $(this).attr("purchase-id");
        var elem = $(this);
        $.ajax({
            url: "<?php echo Yii::app()->createAbsoluteUrl("purchase/deleteadditionalCharge"); ?>",
            data: {
                id: id,
                purchase_id: purchase_id
            },
            type: "POST",
            dataType: "json",
            success: function (data) {
                $(elem).parents(".display_additional").remove();
                $("#additional_charge").text(data.total_additional_charge);
            }
        })
    })

    function closeaction() {
        $(".modal").trigger("click");
    }


    // Form submissions
    $('#myModal1').on('hidden.bs.modal', function (e) {
        $(this).find(".panel-body")
            .find("input[type=text],textarea,select")
            .val('')
            .end()
            .find("input[type=checkbox], input[type=radio]")
            .prop("checked", "")
            .end();
    })

    $('#myModal2').on('hidden.bs.modal', function (e) {
        $(this).find(".panel-body")
            .find("input[type=text],textarea,select")
            .val('')
            .end()
            .find("input[type=checkbox], input[type=radio]")
            .prop("checked", "")
            .end();
    })
    $('#myModal3').on('hidden.bs.modal', function (e) {
        $(this).find(".panel-body")
            .find("input[type=text],textarea,select")
            .val('')
            .end()
            .find("input[type=checkbox], input[type=radio]")
            .prop("checked", "")
            .end();
    })
    $('#myModal4').on('hidden.bs.modal', function (e) {
        $(this).find(".panel-body")
            .find("input[type=text],textarea,select")
            .val('')
            .end()
            .find("input[type=checkbox], input[type=radio]")
            .prop("checked", "")
            .end();
    })
    // 1. COmpany
    $(document).on("submit", "#company-form", function () {
        var data = $("#company-form").serialize();
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('company/create&layout=1&popup=1'); ?>',
            method: 'POST',
            data: data,
            success: function (response) {
                console.log("Company added successfully");
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('purchase/dynamiccompany&popup=1'); ?>',
                    method: 'POST',
                    dataType: "json",
                    success: function (response) {
                        $("#company_id").html(response.html);
                        $(".modal").trigger("click");
                        $(".toastmessage").addClass("alert alert-success").html("New company added").fadeIn().delay(4000).fadeOut();

                    }
                })
            }
        })
        return false;
    })

    // 2. Project
    $(document).on("submit", "#projects-form", function () {
        var company = $("#company_id").val();
        if (company == "") {
            alert('Please choose a company');
            return false;
        }

        var data = $("#projects-form").serialize();
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('projects/create&layout=1'); ?>',
            method: 'POST',
            data: data,
            success: function (response) {
                console.log("Project added successfully");
                if (company != '') {
                    $("#project").html('<option value="">Select rty</option>');
                    $.ajax({
                        url: '<?php echo Yii::app()->createUrl('purchase/dynamicproject&popup=1'); ?>',
                        method: 'POST',
                        data: {
                            company_id: company
                        },
                        dataType: "json",
                        success: function (response) {
                            if (response.auto_pono == 1) {
                                $('#purchaseno').attr('readonly', true);
                                $('#purchaseno').val(response.purchase_no);
                            } else {
                                $('#purchaseno').attr('readonly', false);
                                $('#purchaseno').val('');
                            }
                            if (response.status == 'success') {
                                $("#project").html(response.html);
                                $(".modal").trigger("click");
                            } else {
                                $("#project").html(response.html);
                                $(".modal").trigger("click");
                            }
                            $(".toastmessage").addClass("alert alert-success").html("New project added").fadeIn().delay(4000).fadeOut();
                        }
                    })
                }
            }
        })

        return false;
    })

    // 3. Expense head
    $(document).on("submit", "#expense-type-form", function () {
        var company = $("#company_id").val();
        var project = $("#project").val();
        if (company == "") {
            alert('Please choose a company');
            return false;
        }
        if (project == "") {
            alert('Please choose a project');
            return false;
        }
        var data = $("#expense-type-form").serialize();
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('expensetype/create&layout=1&popup=1&projectid='); ?>' + project,
            method: 'POST',
            data: data,
            success: function (response) {
                console.log("Expense head added successfully");

                $("#vendor").html('<option value="">Select Vendor</option>');
                $("#expense_head").html('<option value="">Select Expense Head</option>');
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('purchase/dynamicexpensehead&popup=1'); ?>',
                    method: 'POST',
                    data: {
                        project_id: project
                    },
                    dataType: "json",
                    success: function (response) {
                        if (response.msg != "") {
                            alert(response.msg);
                        }
                        $("#shipping_address").val(response.address);

                        if (response.status == 'success') {
                            $("#expense_head").html(response.html);
                            $("#vendor").html('<option value="">Select Vendor</option>');
                            $(".modal").trigger("click");
                        }
                        $(".toastmessage").addClass("alert alert-success").html("New Expense head added").fadeIn().delay(4000).fadeOut();

                    }
                })
            }
        })
        return false;
    })

    // 4. Vendor
    $(document).on("submit", "#vendors-form", function () {
        var company = $("#company_id").val();
        var project = $("#project").val();
        var expense_head = $("#expense_head").val();
        if (company == "") {
            alert('Please choose a company');
            return false;
        }
        if (project == "") {
            alert('Please choose a project');
            return false;
        }
        if (expense_head == "") {
            alert('Please choose an Expense head');
            return false;
        }
        var data = $("#vendors-form").serialize();
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('vendors/create&layout=1'); ?>',
            method: 'POST',
            data: data,
            success: function (response) {
                console.log("Vendor added successfully");
                $("body").trigger("click");

                $("#vendor").html('<option value="">Select Vendor</option>');
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('purchase/dynamicvendor&popup=1'); ?>',
                    method: 'POST',
                    data: {
                        exp_id: expense_head
                    },
                    dataType: "json",
                    success: function (response) {
                        $("#vendor").html(response.html);
                        $(".modal").trigger("click");
                        $(".toastmessage").addClass("alert alert-success").html("New Vendor added").fadeIn().delay(4000).fadeOut();
                        var erroCount = $(".block_purchase").find(".errorMessage").not(":empty").length;
                        if (erroCount == 0) {
                            savePurchaseOrder();
                        }
                    }
                })
            }
        })
        return false;
    })
</script>
<style>
    .base-data {

        display: none;
    }

    .p-0 {
        padding: 0px;
    }


    .addRow input.textbox {
        width: 200px;
        display: inline-block;
        margin-right: 10px;
    }
</style>