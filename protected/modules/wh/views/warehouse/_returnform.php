<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<?php
/* @var $this BillsController */
/* @var $model Bills */
/* @var $form CActiveForm */
?>
<style type="text/css">
    table.total-table {
        font-size: 14px;
    }

    table.total-table div {
        text-align: right;
    }

    table.table .form-control {
        padding: 1px 1px;
        font-size: inherit;
        min-width: 70px;
    }

    table.table tr.pitems td div {
        padding-top: 8px;
    }

    table.table tr.pitems td div .fa {
        color: #060;
    }

    .formError .formErrorArrow div {
        display: none !important;
    }

    .formErrorContent {
        background-color: #333333 !important;
        border: 1px solid #ddd !important;
    }

    .bold {
        font-weight: bold;
    }


    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .addRow,
    .table-responsive {
        margin-bottom: 10px;
    }

    .pitems td .hideinput {
        box-shadow: none;
        border: none;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.js"></script>
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>
<link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css" />
<?php if (Yii::app()->user->hasFlash('success')): ?><br>
    <div class="example1consolesucces">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>.select();
<?php endif; ?>
<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'bills-form',
        'enableAjaxValidation' => false,
    ));
    ?>
    <div class="entries-wrapper">
        <div class="row">
            <div class="col-xs-12">
                <div class="heading-title">Details</div>
                <div class="dotted-line"></div>
            </div>
        </div>
        <div class="row">
            <div class="">
                <div class="" id="alert">

                </div>
            </div>
            <div class="form-group col-sm-4">
                <?php echo $form->labelEx($model, 'receipt_id'); ?>

                <?php
                $user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                foreach ($arrVal as $arr) {
                    if ($newQuery)
                        $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
                }

                echo $form->dropDownList($model, 'receipt_id', CHtml::listData(Warehousereceipt::model()->findAll(array(
                    'select' => array('warehousereceipt_id, warehousereceipt_no'),
                    "condition" => '(' . $newQuery . ')',
                    'order' => 'warehousereceipt_id DESC',
                    'distinct' => true
                )), 'warehousereceipt_id', 'warehousereceipt_no'), array('class' => 'form-control js-example-basic-single validate[required]', 'empty' => '-Select Bill-'));
                ?>
                <?php echo $form->error($model, 'receipt_id'); ?>
            </div>
            <div class="form-group col-sm-4">
                <?php echo $form->labelEx($model, 'return_number'); ?>
                <?php echo $form->textField($model, 'return_number', array('class' => 'form-control validate[required]', 'readonly' => !$model->isNewRecord)); ?>
                <?php echo $form->error($model, 'return_number'); ?>
            </div>
            <div class="form-group col-sm-4">
                <?php echo $form->labelEx($model, 'return_date'); ?>
                <?php echo CHtml::activeTextField($model, 'return_date', array('readonly' => 'true', "value" => (($model->isNewRecord) ? date('d-M-Y') : date('d-M-Y', strtotime($model->bill_date))), 'size' => 10, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'return_date'); ?>
            </div>
        </div>
        <div class="">
            <div class="">
                <div id="client">
                    <?php echo ((!$model->isNewRecord) ? '' : '') ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 ">
                <div class="data-amnts">
                    <div>
                        <label class="inline">Amount:</label>
                        <span id="PurchaseReturn_return_amount">
                            <?php echo $model->return_amount ? Controller::money_format_inr($model->return_amount, 2) : 0; ?>
                        </span>
                    </div>
                </div>
                <input type="hidden" name="billid" id="billid" value="" />
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-right">
                <?php echo CHtml::Button($model->isNewRecord ? 'Create' : 'Save', array('class' => 'buttonsubmit btn btn-info', 'id' => 'buttonsubmit')); ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
    <script>
        <?php $addUrl = Yii::app()->createAbsoluteUrl("wh/warehouse/addItemsForDefectReturn"); ?>
        <?php $valUrl = Yii::app()->createAbsoluteUrl("wh/warehouse/validateReturnNumber"); ?>
        <?php $reloadUrl = Yii::app()->createAbsoluteUrl("bills/updateBillsOnReload"); ?>

        $(".js-example-basic-single").select2();

        $(document).ready(function () {
            $('select').first().focus();
        });


        $(document).ready(function () {
            $("#bills-form").validationEngine({
                'custom_error_messages': {
                    'custom[number]': {
                        'message': 'Invalid number'
                    }
                }
            });
            $("#DefectReturn_receipt_id").focus();
            var billCookie = getCookie("billid");
            var billNoCookie = getCookie("billnumber");
            if (billCookie) {
                $.ajax({
                    url: "<?php echo $reloadUrl; ?>",
                    data: {
                        "billid": billCookie,
                        "billnumber": billNoCookie
                    },
                    type: "POST",
                    success: function (data) {
                        if (data == 1) {
                            setCookie("billid", "", -1);
                            setCookie("billnumber", "", -1);
                        }
                    }
                });
            }


            $("body").on('change', '.table tr.pitems input', function (e) {
                var rowId = $(this).parent().attr("id");
                var aStat;
                var billNo = $("#DefectReturn_return_number").val();
                var quantity = parseFloat($("#biquantity" + rowId).val());
                var amount = parseFloat($("#biamount" + rowId).text());
                var damount = parseFloat($("#damount" + rowId).val());
                var dpercent = parseFloat($("#dpercent" + rowId).text());
                var newDAmt = amount * (dpercent / 100);
                if (newDAmt != 0)
                    newDAmt = newDAmt.toFixed(2);
                var availQty = parseFloat($("#availablequantity" + rowId).val());
                var crId = $(this).attr("id");
                if (amount < damount && (amount != 0 || quantity == 0)) {
                    $("#alert").addClass("alert alert-danger").html("<strong>Discount amount must be less than the amount<strong>");
                    $("#damount" + rowId).val(newDAmt);
                } else if (availQty < quantity) {
                    $("#alert").addClass("alert alert-danger").html("<strong>Total available quantity is " + availQty + "<strong>");
                    $("#biquantity" + rowId).val($("#savedquantity" + rowId).val());

                    return false;
                } else if (quantity < 0) {
                    $("#alert").addClass("alert alert-danger").html("<strong>Quantity must be greater than or equal to zero<strong>");
                    $("#biquantity" + rowId).val(0);
                    return false;
                } else if (billNo == "") {
                    $("#alert").addClass("alert alert-danger").html("<strong>Please enter return number<strong>");
                    this.checked = false;
                    return false;
                } else {
                    $("#alert").removeClass("alert alert-danger").html("");

                    validateBillNumber(billNo, rowId, 2, crId);
                }
            });

            $('#bills-form').on('keypress', ':input', function (event) {
                if (event.keyCode == 13) {
                    /* FOCUS ELEMENT */
                    var thisId = $(this).attr("id");
                    var thisVal = $(this).val();

                    if (thisId == "DefectReturn_receipt_id" && thisVal == "") {
                        $("#DefectReturn_receipt_id").click();
                    } else {
                        var inputs = $(this).parents("form").eq(0).find(":input");
                        var idx = inputs.index(this);

                        if (idx == inputs.length - 1) {

                        } else {
                            inputs[idx + 1].focus(); //  handles submit buttons
                            inputs[idx + 1].select();
                        }
                        return false;
                    }
                }
            });
            $("#PurchaseReturn_return_date").keypress(function (e) {
                if (e.keyCode == 13) {
                    $("#biquantity0").focus();
                    $("#biquantity0").select();
                }
            });
            $('#bills-form').on('keypress', '.lnext', function (e) {

                if (e.keyCode == 13) {
                    var rowId = parseInt($(this).parent().attr("id"));
                    var totrows = $("#totrows").val();
                    var nextId = rowId + 1;
                    if (nextId == totrows) {
                        var billNo = $("#DefectReturn_return_number").val();
                        if (billNo == "")
                            $("#DefectReturn_return_number").focus()
                        else
                            $("#buttonsubmit").focus();
                    } else {
                        $("#biquantity" + nextId).focus();
                        $("#biquantity" + nextId).select();
                    }
                }
            });


            $("#buttonsubmit").keypress(function (e) {
                if (e.keyCode == 13) {
                    $('.buttonsubmit').click();
                }
            });

            $(".buttonsubmit").click(function () {
                $("#bills-form").submit();
            })


            $("#DefectReturn_return_number").change(function () {
                var billNo = $("#DefectReturn_return_number").val();
                var crId = "";
                $('#loading').show();
                $.ajax({
                    url: "<?php echo $valUrl; ?>",
                    data: {
                        "billno": billNo
                    },
                    type: "POST",
                    success: function (data) {
                        if (data == 1) {
                            $("#alert").addClass("alert alert-danger").html("<strong>Return number already exist. Please enter a new return number.<strong>");
                            $("#DefectReturn_return_number").val('');
                            //$("#chkitem"+rowId).checked = false;
                            $('#buttonsubmit').prop('disabled', true);
                            return false;
                        } else {
                            $("#alert").removeClass("alert alert-danger").html("");
                            $('#buttonsubmit').prop('disabled', true);
                            var totrows = parseInt($("#totrows").val());
                            for (var i = 0; i < totrows; i++) {
                                var biQuantity = parseFloat($("#biquantity" + i).val());
                                if (biQuantity > 0) {
                                    validateBillNumber(billNo, i, 2, crId);
                                }
                            }
                        }
                    }
                });
            });

        });

        function convertEnterToTab() {

            if (event.keyCode == 13) {

            }

        }

        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
        //document.onkeydown = convertEnterToTab;
        function addItemToBills(rowId, aStat, crId) {
            $(".formError").hide();
            var bill_Id = $("#DefectReturn_receipt_id").val();
            var returnNumber = $("#DefectReturn_return_number").val();
            var returnDate = $("#DefectReturn_return_date").val();
            var totrows = parseInt($("#totrows").val());
            var billId = $("#billid").val();
            var itemId = parseInt($("#ids" + rowId).val());
            var quantity = parseFloat($("#biquantity" + rowId).val());
            quantity = isNaN(quantity) ? 0 : quantity;
            $("#biquantity" + rowId).val(quantity);
            var unit = $("#biunit" + rowId).text();
            var rate = parseFloat($("#birate" + rowId).val());
            var orRate = parseFloat($("#orrate" + rowId).val());
            rate = isNaN(rate) ? orRate : rate;
            if (rate < 0)
                rate = orRate;
            if (quantity % 1 == 0)
                quantity = quantity;
            else
                quantity = quantity.toFixed(2);
            $("#biquantity" + rowId).val(quantity);
            if (rate != 0)
                rate = parseFloat(rate.toFixed(2));
            $("#birate" + rowId).val(rate);
            if (crId == "biquantity" + rowId) {
                $("#birate" + rowId).select();
            }
            var amount = parseFloat($("#biamount" + rowId).text());

            var totalamount = parseFloat($("#totalamount" + rowId).text());
            var categoryId = $("#category" + rowId).val();
            var billTotal = parseFloat($("#PurchaseReturn_return_amount").text());

            var billGTotal = parseFloat($("#PurchaseReturn_return_totalamount").text());
            var categoryName = $("#bicategoryname" + rowId).text();
            var availQty = parseFloat($("#availablequantity" + rowId).val());
            var billItem = $("#billitem" + rowId).val();
            if (quantity > 0) {
                var newAmt = (quantity * rate).toFixed(2);


                var newAmtD = newAmt

                newAmt = isNaN(newAmt) ? 0 : newAmt;


                var newTotal = parseFloat(newAmtD);
                newTotal = isNaN(newTotal) ? 0 : newTotal;
                newTotal = newTotal.toFixed(2);
            } else {
                var newAmt = 0;

                var newAmtD = 0;

                var newTotal = 0;
            }
            $("#biamount" + rowId).text(newAmt ? newAmt : 0);

            $("#totalamount" + rowId).text(newTotal ? newTotal : 0);
            var billNTotal = 0;

            var billNGTotal = 0;
            for (var i = 0; i < totrows; i++) {
                if ($("#biquantity" + i).val() > 0) {
                    billNTotal = billNTotal + parseFloat($("#biamount" + i).text());

                    billNGTotal = billNGTotal + parseFloat($("#totalamount" + i).text());
                }
            }
            $("#PurchaseReturn_return_amount").text(billNTotal ? billNTotal.toFixed(2) : 0);

            $("#PurchaseReturn_return_totalamount").text(billNGTotal ? billNGTotal.toFixed(2) : 0);
            if (quantity == 0 && billItem == "") {
                var opStatus = 0;
            } else if (quantity == 0 && billItem != "") {
                var opStatus = 2;
            } else if (quantity > 0 && billItem == "") {
                var opStatus = 1;
            } else if (quantity > 0 && billItem != "") {
                var opStatus = 3;
            } else {
                var opStatus = 2;
            }
            var tqty = parseFloat($("#tqty").val());
            var btqty = parseFloat($("#btqty").val());
            var newQT = 0;
            for (var i = 0; i < totrows; i++) {
                newQT = newQT + parseFloat($("#biquantity" + i).val());
            }
            var newTotalQty = btqty + newQT;
            if (tqty > newTotalQty)
                var purchaseStatus = 94;
            else
                var purchaseStatus = 93;

            $('.loading-overlay').addClass('is-active');
            $.ajax({
                url: "<?php echo $addUrl; ?>",
                data: {
                    "bill_id": bill_Id,
                    "returnnumber": returnNumber,
                    "returndate": returnDate,
                    "billamount": billNTotal,

                    "billtotal": billNGTotal,
                    "itemid": itemId,
                    "quantity": quantity,
                    "unit": unit,
                    "rate": rate,
                    "amount": newAmt,

                    "totalamount": newTotal,
                    "billid": billId,
                    "astat": opStatus,
                    "categoryid": categoryId,
                    "categoryname": categoryName,
                    "availqty": availQty,
                    "billitem": billItem,
                    "purchasestatus": purchaseStatus
                },
                type: "POST",
                success: function (data) {
                    var result = JSON.parse(data);
                    $("#billid").val(result[0]);
                    $("#billitem" + rowId).val(result[1]);
                    $("#savedquantity" + rowId).val(quantity);
                    setCookie("billid", result[0], 1);
                    setCookie("billnumber", returnNumber, 1);
                    if (quantity > 0) {
                        $("#tickmark" + rowId).html('<i class="fa fa-check" aria-hidden="true"></i>');
                        $('#buttonsubmit').prop('disabled', false);
                    } else {
                        $("#tickmark" + rowId).html("");
                        $('#buttonsubmit').prop('disabled', true);
                    }

                }
            });

        }

        function validateBillNumber(billNo, rowId, stat, crId) {
            $('#loading').show();
            $.ajax({
                url: "<?php echo $valUrl; ?>",
                data: {
                    "billno": billNo
                },
                type: "GET",
                success: function (data) {
                    if (data == 1) {
                        $("#alert").addClass("alert alert-danger").html("<strong>Bill number already exist. Please enter a new bill number.<strong>");
                        $(".chkitem").prop('checked', false);
                        //$("#chkitem"+rowId).checked = false;
                        return false;
                    } else if (data == 2) {
                        $("#alert").removeClass("alert alert-danger").html("");
                        $("#DefectReturn_return_number").attr("readonly", true)
                        var aStat;
                        if (stat == 1) {
                            if ($("input[name='chkitem[" + rowId + "]']").is(':checked')) {
                                aStat = 1;
                            } else {
                                aStat = 2;
                            }
                            addItemToBills(rowId, aStat, crId);
                        } else if (stat == 2) {
                            if ($("input[name='chkitem[" + rowId + "]']").is(':checked')) {
                                aStat = 3
                            } else {
                                aStat = 4;
                            }
                            addItemToBills(rowId, aStat, crId);
                        }
                    }
                }
            });
        }
        $(document).ajaxComplete(function () {
            $('.loading-overlay').removeClass('is-active');
            $('#loading').hide();
        });
    </script>
    <?php $url = Yii::app()->createAbsoluteUrl("wh/warehouse/GetItemsByWarehouse"); ?>
    <?php Yii::app()->clientScript->registerScript('myscript', '
$(document).ready(function(){
    var bill_id = $("#DefectReturn_receipt_id").val();
    
    if(bill_id != "") {
        getAllItems(bill_id);
    }
    $("#DefectReturn_receipt_id").change(function(){
        var bill_id= $("#DefectReturn_receipt_id").val();
        // alert(bill_id);
        if(bill_id == "")
            bill_id = 0;
        getAllItems(bill_id);
    });
});
function getAllItems(bill_id) {
        $.ajax({
           url: "' . $url . '",
            data: {"id": bill_id},
            //dataType: "json",
            type: "GET",
            success:function(data){
                //alert(data);
                $("#client").html(data);
                $("#DefectReturn_return_number").focus();
                //var amount = $("#aj-amount").val();
                //alert(amount);
                //$("#PurchaseReturn_return_amount").val(amount);
                //$("#PurchaseReturn_return_totalamount").val(amount);
            }
        });
}
        '); ?>