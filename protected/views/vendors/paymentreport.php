<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<style>
    .inner h3 {
        padding: 23px 0px 20px 0px;
    }
</style>
<?php
Yii::app()->clientScript->registerScript('search', "

    $('#btSubmit').click(function(){

            $('#vendorform').submit();

    });
");
$tblpx = Yii::app()->db->tablePrefix;
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
$newQuery_1 = "";
$newQuery1 = "";
$newQuery2 = "";
$newQuery3 = "";
foreach ($arrVal as $arr) {
    if ($newQuery)
        $newQuery .= ' OR';
    if ($newQuery_1)
        $newQuery_1 .= ' OR';
    $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
    $newQuery_1 .= " FIND_IN_SET('" . $arr . "', company_id)";
}

if ($company_id == '') {
    foreach ($arrVal as $arr) {
        if ($newQuery1)
            $newQuery1 .= ' OR';
        if ($newQuery2)
            $newQuery2 .= ' OR';
        if ($newQuery3)
            $newQuery3 .= ' OR';
        $newQuery1 .= " FIND_IN_SET('" . $arr . "', {$tblpx}dailyvendors.company_id)";
        $newQuery2 .= " FIND_IN_SET('" . $arr . "', {$tblpx}expenses.company_id)";
        $newQuery3 .= " FIND_IN_SET('" . $arr . "', {$tblpx}bills.company_id)";
    }
} else {
    $newQuery1 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}dailyvendors.company_id)";
    $newQuery2 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}expenses.company_id)";
    $newQuery3 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}bills.company_id)";
}
if ($company_id != NULL) {
    $company = Company::model()->findbyPk($company_id);
    $company_name = ' - ' . $company->name;
} else {
    $company_name = "";
}
$company_address = Company::model()->find(array('order' => ' id ASC'));
$total_Amount_paid_to_vendor = 0;
$project = 0;
if (!empty($daybookPayment)) {
    foreach ($daybookPayment as $daybookPayments) {
        if ($project == 0) {
            if (!empty($daybookPayments['sumtotalamountpaidtovendor'])) {
                $total_Amount_paid_to_vendor += $daybookPayments['sumtotalamountpaidtovendor'];

            }
            $project = $daybookPayments['projectid'];

        }
        if ($project !== $daybookPayments['projectid']) {
            $total_Amount_paid_to_vendor += $daybookPayments['sumtotalamountpaidtovendor'];
            $project = $daybookPayments['projectid'];
        }

    }
}

?>
<?php
$url_data = '';
if (isset($_GET['vendor_id'])) {
    $url_data .= '&vendor_id=' . $_GET['vendor_id'];
}
if (isset($_GET['company_id'])) {
    $url_data .= '&company_id=' . $_GET['company_id'];
}
if (isset($_GET['date_from'])) {
    $url_data .= '&date_from=' . $_GET['date_from'];
}
if (isset($_GET['date_to'])) {
    $url_data .= '&date_to=' . $_GET['date_to'];
}
if (isset($_GET['view_type'])) {
    $url_data .= '&view_type=' . $_GET['view_type'];
}

$reconcil_type = isset($_GET['reconcil_type']) ? $_GET['reconcil_type'] : 'reconcil';
// $view_type = $_GET['view_type'];
$checked = false;
$checked_zero = false;
if (isset($_GET['summary']) && $_GET['summary'] != "") {
    $checked = ($_GET['summary'] == 0) ? false : true;
}
if (isset($_GET['show_zerobalance']) && $_GET['show_zerobalance'] != "") {
    $checked_zero = ($_GET['show_zerobalance'] == 0) ? false : true;
}
?>

<div class="container" id="project">
    <div class="expenses-heading">
        <?php if (!isset($_GET['view_type']) || (isset($_GET['view_type']) && $_GET['vendor_id'] == "")) { ?>
            <div class="header-container">
                <?php if (filter_input(INPUT_GET, 'export')) {
                    $datefrom = isset($_REQUEST['date_from']) ? $_REQUEST['date_from'] : "";
                    $dateto = isset($_REQUEST['date_to']) ? $_REQUEST['date_to'] : date('d-m-Y');
                    $company_id = isset($_REQUEST['company_id']) ? $_REQUEST['company_id'] : '';

                    ?>
                    <h3 class="<?php echo filter_input(INPUT_GET, 'export') ? 'pdf_spacing' : '' ?> pull-left">
                        Vendors Payment Reports
                        <?php
                        // Display date range only if both dates are set
                        if (!empty($datefrom) && !empty($dateto)) {
                            echo "Between " . date('d-m-Y', strtotime($datefrom)) . " And " . date('d-m-Y', strtotime($dateto));
                        } elseif (!empty($datefrom)) {
                            echo "From " . date('d-m-Y', strtotime($datefrom));
                        } elseif (!empty($dateto)) {
                            echo "Up to " . date('d-m-Y', strtotime($dateto));
                        } else {
                            echo ""; // No date range to display
                        }
                        ?>
                    </h3>
                <?php } else { ?>
                    <h3 class="<?php echo (filter_input(INPUT_GET, 'export') ? 'pdf_spacing' : '') ?> pull-left">Vendors Payment
                        Reports</h3>
                <?php } ?>
                <div class="btn-container">
                    <?php if (!filter_input(INPUT_GET, 'export')) { ?>
                        <button type="button" id="pdf-btn" class="btn btn-info"
                            href="<?php echo Yii::app()->request->requestUri . '&export=pdf' . $url_data ?>">
                            <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
                        </button>


                        <button type="button" id="excel-btn" class="save_btn btn btn-info button-icon"
                            href="<?php echo $this->createAbsoluteUrl('vendors/savetoexcel', array("vendor_id" => $vendor_id, "company_id" => $company_id, "date_from" => $date_from, "date_to" => $date_to)) ?>">
                            <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i></button>

                        <button type="button" id="payment_report_aging_two"
                            href="<?php echo Yii::app()->createUrl("vendors/paymentReport&aging=2") ?>"
                            class="save_btn btn btn-info">Aging Summary By Due
                            Date</button>
                        <button type="button" id="payment_report_aging_one"
                            href="<?php echo Yii::app()->createUrl("vendors/ageingvendorReport") ?>"
                            class="save_btn btn btn-info">Aging Summary</button>
                    <?php } ?>

                </div>
            </div>

        <?php } elseif (!empty($vendor_id) && !empty($model)) { ?>
            <div id="contenthtml" class="contentdiv <?php echo (filter_input(INPUT_GET, 'export') ? 'pdf_spacing' : '') ?>">
                <div class="expenses-heading">
                    <div class="header-container">

                        <div>
                            <?php if ($_GET["view_type"] == "vendor") { ?>
                                <h3 class="summary_head">Vendor Summary Reports </h3>
                            <?php } else { ?>
                                <?php if (!filter_input(INPUT_GET, 'export')) { ?>
                                    <h3 class="ledger_head">Ledger View</h3>
                                <?php } ?>
                            <?php } ?>

                        </div>

                        <?php
                        if (!filter_input(INPUT_GET, 'export')) { ?>
                            <div class="btn-container">


                                <button type="button" id="pdf-btn" class="btn btn-info"
                                    href="<?php echo Yii::app()->request->requestUri . '&export=pdf' . $url_data ?>">
                                    <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
                                </button>


                                <button type="button" id="excel-btn" class="save_btn btn btn-info button-icon"
                                    href="<?php echo $this->createAbsoluteUrl('vendors/savetoexcel', array("vendor_id" => $vendor_id, "company_id" => $company_id, "date_from" => $date_from, "date_to" => $date_to, "view_type" => 'vendor', 'reconcil_type' => $reconcil_type, "summary" => isset($_GET['summary']) ? $_GET['summary'] : 0)) ?>">
                                    <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i></button>

                                <?php if (isset($_GET['reconcil_type']) && ($_GET['reconcil_type'] == "reconcil")) { ?>

                                    <a class="unreconciled save_btn btn btn-info"
                                        href="<?php echo $this->createAbsoluteUrl('vendors/Paymentreport', array("view_type" => $_GET['view_type'], "reconcil_type" => 'unreconcil', "vendor_id" => $vendor_id, "company_id" => $company_id, "date_from" => $date_from, "date_to" => $date_to, "summary" => isset($_GET['summary']) ? $_GET['summary'] : 0)) ?>">UnReconciled
                                        Report</a>
                                <?php } ?>
                                <?php if (isset($_GET['reconcil_type']) && ($_GET['reconcil_type'] == "unreconcil")) { ?>

                                    <a class="reconciled save_btn btn btn-info"
                                        href="<?php echo $this->createAbsoluteUrl('vendors/Paymentreport', array("view_type" => $_GET['view_type'], "reconcil_type" => 'reconcil', "vendor_id" => $vendor_id, "company_id" => $company_id, "date_from" => $date_from, "date_to" => $date_to, "summary" => isset($_GET['summary']) ? $_GET['summary'] : 0)) ?>">Reconciled
                                        Report</a>
                                <?php } ?>

                                <?php
                                if ($_GET['view_type'] == 'vendor') {
                                    ?>
                                    <a class="save_btn btn btn-info"
                                        href="<?php echo $this->createAbsoluteUrl('vendors/Paymentreport', array("view_type" => 'ledger', "vendor_id" => $vendor_id, "company_id" => $company_id, "date_from" => $date_from, "date_to" => $date_to, 'reconcil_type' => $reconcil_type, "summary" => isset($_GET['summary']) ? $_GET['summary'] : 0)) ?>">Ledger
                                        View</a>
                                <?php } else { ?>

                                    <a class="save_btn btn btn-info"
                                        href="<?php echo $this->createAbsoluteUrl('vendors/Paymentreport', array("view_type" => 'vendor', "vendor_id" => $vendor_id, "company_id" => $company_id, "date_from" => $date_from, "date_to" => $date_to, 'reconcil_type' => $reconcil_type, "summary" => isset($_GET['summary']) ? $_GET['summary'] : 0)) ?>">Vendor
                                        Summary</a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php }
        } ?>

        </div>

        <?php
        if (!filter_input(INPUT_GET, 'export')) { ?>
            <div class="page_filter clearfix custom-form-style">
                <?php $form = $this->beginWidget('CActiveForm', array(
                    'action' => Yii::app()->createUrl($this->route),
                    'method' => 'get',
                )); ?>

                <div class="row">
                    <div class="form-group col-xs-12 col-sm-3">
                        <label>Vendor </label>
                        <?php
                        echo CHtml::dropDownList('vendor_id', 'vendor_id', CHtml::listData(Vendors::model()->findAll(array(
                            'select' => array('vendor_id, name'),
                            'order' => 'name',
                            'condition' => '(' . $newQuery_1 . ')',
                            'distinct' => true
                        )), 'vendor_id', 'name'), array('empty' => 'Select Vendor', 'id' => 'vendor_id', 'class' => "select_box form-control form-control js-example-basic-single", 'style' => 'padding: 2.5px 0px;', 'options' => array($vendor_id => array('selected' => true))));
                        ?>
                    </div>
                    <div class="form-group col-xs-12 col-sm-3">
                        <label>Company </label>
                        <?php
                        echo CHtml::dropDownList('company_id', 'company_id', CHtml::listData(Company::model()->findAll(array(
                            'select' => array('id, name'),
                            'order' => 'id DESC',
                            'condition' => '(' . $newQuery . ')',
                            'distinct' => true
                        )), 'id', 'name'), array('empty' => 'Select Company', 'id' => 'company_id', 'class' => "select_box form-control", 'style' => 'padding: 2.5px 0px;', 'options' => array($company_id => array('selected' => true))));
                        ?>
                        <input type="hidden" name="view_type" value="vendor">
                        <input type="hidden" name="reconcil_type" value="reconcil">

                    </div>
                    <div class="form-group col-xs-12 col-sm-3">
                        <label>From Date</label>
                        <input class="form-control" type="date" id="date_from" name="date_from"
                            value="<?php echo $date_from; ?>" />
                    </div>
                    <div class="form-group col-xs-12 col-sm-3">
                        <label>To Date</label>
                        <input class="form-control" type="date" id="date_to" name="date_to"
                            value="<?php echo $date_to; ?>" />
                    </div>
                    <div class=" col-xs-12 text-right">

                        <button type="submit" id=" btSubmit" class="btn btn-sm btn-primary">Go</button>
                        <button type="reset" class="btn btn-sm btn-default"
                            onclick="javascript:location.href='<?php echo $this->createUrl('vendors/paymentReport'); ?>'">Reset</button>


                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12 col-md-3 margin-top-0 margin-bottom-0">
                        <div class="form-check">
                            <input type="hidden" name="summary"
                                value="<?php echo isset($_GET['summary']) ? ($_GET['summary'] == 0) ? 1 : 0 : 0 ?>"
                                class="summary_checkbox">

                            <?php if (isset($_GET['vendor_id']) && $_GET['vendor_id'] != "") {
                                echo CHtml::checkBox('summary', $checked, array('style' => 'margin:0px'));
                                echo "&nbsp;";
                                echo CHtml::label(' Show Summary', '', array('class' => 'form-check-label'));
                            } else {
                                echo CHtml::checkBox('show_zerobalance', $checked_zero, array('class' => 'form-check-input'));
                                echo "&nbsp;";
                                echo CHtml::label('Show vendors with no balance', '', array('class' => 'form-check-label'));

                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
            </div>
        <?php } ?>
        <?php if (!empty($vendor_id) && !empty($model)) { ?>
            <div id="contenthtml" class="contentdiv <?php echo (filter_input(INPUT_GET, 'export') ? 'pdf_spacing' : '') ?>">
                <div class="row">


                    <div class="col-xs-12">


                        <?php if ($_GET["view_type"] == "vendor") { ?>
                            <?php if (filter_input(INPUT_GET, 'export')) {
                                $datefrom = isset($_REQUEST['date_from']) ? $_REQUEST['date_from'] : "";
                                $dateto = isset($_REQUEST['date_to']) ? $_REQUEST['date_to'] : date('d-m-Y');
                                $company_id = isset($_REQUEST['company_id']) ? $_REQUEST['company_id'] : '';

                                ?>
                                <div class="heading-title">Vendor Summary of <b>
                                        <?php echo $model->name; ?> &nbsp;
                                        <?php echo $company_name ?>
                                    </b>
                                    <?php
                                    // Display date range only if both dates are set
                                    if (!empty($datefrom) && !empty($dateto)) {
                                        echo "Between " . date('d-m-Y', strtotime($datefrom)) . " And " . date('d-m-Y', strtotime($dateto));
                                    } elseif (!empty($datefrom)) {
                                        echo "From " . date('d-m-Y', strtotime($datefrom));
                                    } elseif (!empty($dateto)) {
                                        echo "Up to " . date('d-m-Y', strtotime($dateto));
                                    } else {
                                        echo ""; // No date range to display
                                    }
                                    ?>
                                </div>
                            <?php } else { ?>
                                <div class="heading-title">Vendor Summary of <b>
                                        <?php echo $model->name; ?> &nbsp;
                                        <?php echo $company_name ?>
                                    </b></div>

                            <?php }


                        } else { ?>
                            <?php if (!filter_input(INPUT_GET, 'export')) { ?>

                                <div class="heading-title">Ledger View</div>
                            <?php } ?>
                        <?php } ?>

                        <div class="dotted-line"></div>
                    </div>

                </div>

                <?php if ($_GET["view_type"] == 'vendor') { ?>
                    <div class="table-wrapper margin-top-10">

                        <!-- overall summary -->
                        <?php if (isset($_GET['reconcil_type']) && ($_GET['reconcil_type'] == "reconcil")) { ?>

                            <div class="d-flex">
                                <p class="margin-bottom-0">
                                    **NOTE: Please check if there is any Pending Bills. Pending Bills are not included in Invoice
                                    Calculations
                                </p>

                            </div>
                            <div id="table-wrapper">
                                <table class="table total-table reconcil_table">

                                    <thead class="entry-table">
                                        <tr bgcolor="#eee">
                                            <th width="25%">Overall Summary</th>
                                            <th>Invoice amount / Vendor amount</th>
                                            <th width="20%">Paid</th>
                                            <th>TDS Amount</th>
                                            <th>Paid To Vendor</th>
                                            <th rowspan="4" style="vertical-align: top;white-space:nowrap">Balance (with TDS)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td width="25%">Daybook</td>
                                            <?php
                                            ob_start();
                                            if (!empty($daybookPayment)) {
                                                foreach ($daybookPayment as $vendor) {
                                                }
                                                ?>
                                            <?php }
                                            ob_end_clean();
                                            ?>
                                            <td class="text-right ">
                                                <?php echo !empty($daybookPayment) ? Yii::app()->Controller->money_format_inr($total_vendor_Amount, 2) : '0'; ?>
                                            </td>
                                            <td class="text-right" width="20%">
                                                <?php echo !empty($daybookPayment) ? Yii::app()->Controller->money_format_inr($total_Amount_paid_to_vendor, 2) : '0'; ?>
                                            </td>
                                            <td class="text-right">
                                                <?php echo !empty($daybookPayment) ? Yii::app()->Controller->money_format_inr($daybookPayment[0]["sumtotaltds"], 2) : '0'; ?>
                                            </td>
                                            <td class="text-right">
                                                <?php echo !empty($daybookPayment) ? Yii::app()->Controller->money_format_inr($total_Amount_paid_to_vendor, 2) : '0'; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <?php
                                            $purchase_return = Projects::model()->setPurchaseReturn($expreceipt_return);
                                            // echo "<pre>";print_r($purchase_return);exit;
                                            $credit_datas = isset($purchase_return['credit']) ? $purchase_return['credit'] : array();
                                            $other_datas = isset($purchase_return['other']) ? $purchase_return['other'] : array();
                                            $sum_ret = 0;
                                            $credit_sum1 = 0;
                                            $other_sum1 = 0;
                                            ob_start();
                                            foreach ($credit_datas as $return_data) {
                                                $credit_sum1 += $return_data['amount'];
                                            }
                                            foreach ($other_datas as $return_data1) {
                                                $other_sum1 += $return_data1['paidamount'];
                                            }
                                            $sum_ret = $credit_sum1 + $other_sum1;
                                            ob_end_clean();
                                            ?>
                                            <td width="25%">Purchase Return</td>
                                            <td class="text-right">
                                                <?php echo ("-" . $sum_ret) ?>
                                            </td>
                                            <td width="20%" class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                            </td>
                                            <td class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                            </td>
                                            <td class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="25%">Direct Vendor Payment</td>
                                            <td class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                            </td>
                                            <?php if (!empty($dailyvPayment)) {
                                                $totalAmount = 0;
                                                $totalTds = 0;
                                                $totalPaidVendor = 0;
                                                ob_start();
                                                foreach ($dailyvPayment as $dailyvendor) {
                                                    $totalAmount += ($dailyvendor["amount"] + $dailyvendor["tax_amount"]);
                                                    $totalTds += $dailyvendor["tds_amount"];
                                                    $totalPaidVendor += $dailyvendor["paidamount"];
                                                }
                                                ob_end_clean();
                                                ?>

                                                <td width="20%" class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr($totalAmount, 2); ?>
                                                </td>
                                                <td class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr($totalTds, 2); ?>
                                                </td>
                                                <td class="text-right total_4 direct_pay_sum">
                                                    <?php echo Yii::app()->Controller->money_format_inr($totalPaidVendor, 2); ?>
                                                </td>
                                            <?php } else { ?>

                                                <td width="20%" class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                                </td>
                                                <td class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                                </td>
                                                <td class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <?php
                                            $dayinvtotal = !empty($daybookPayment) ? $total_vendor_Amount : '0';
                                            $retinvtotal = ("-" . $sum_ret);
                                            $directinvtotal = 0.00;

                                            $daypaid = !empty($daybookPayment) ? $total_Amount_paid_to_vendor : '0';
                                            $retpaid = 0.00;
                                            $directpaid = !empty($dailyvPayment) ? $totalAmount : '0';

                                            $daytds = !empty($daybookPayment) ? $daybookPayment[0]["sumtotaltds"] : '0';
                                            $rettds = 0.00;
                                            $directtds = !empty($dailyvPayment) ? $totalTds : '0';

                                            $daypv = !empty($daybookPayment) ? $total_Amount_paid_to_vendor : '0';
                                            $retpv = 0.00;
                                            $directpv = !empty($dailyvPayment) ? $totalPaidVendor : '0';

                                            $overallinvtotal = $dayinvtotal + $retinvtotal + $directinvtotal;
                                            $overallpaidtotal = $daypaid + $retpaid + $directpaid;
                                            $overalltdstotal = $daytds + $rettds + $directtds;
                                            $overallpvtotal = $daypv + $retpv + $directpv;
                                            $overallbalance = (str_replace(",", "", $overallinvtotal) - str_replace(",", "", $overallpaidtotal));
                                            ?>
                                            <th width="25%" class="text-right">Total</th>
                                            <th class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr($overallinvtotal, 2);
                                                ?>
                                            </th>
                                            <th width="20%" class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr($overallpaidtotal, 2);
                                                ?>
                                            </th>
                                            <th class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr($overalltdstotal, 2);
                                                ?>
                                            </th>
                                            <th class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr($overallpvtotal, 2);
                                                ?>
                                            </th>
                                            <th class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr($overallbalance, 2);
                                                ?>
                                            </th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- overall summary ends -->
                            <!-- Unreconcil overall summary starts if pending bill exist -->
                            <?php if (!empty($paymentData)) { ?>



                                <?php if ($_GET["view_type"] == "vendor") { ?>



                                    <div class="row margin-top-10">
                                        <div class="col-xs-12">
                                            <div class="heading-title">Unreconciled Report of <b>
                                                    <?php echo $model->name; ?> &nbsp;
                                                    <?php echo $company_name ?>
                                            </div>
                                            <div class="dotted-line"></div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="table-wrapper margin-top-10">
                                    <table class="table total-table ureconcil_table">
                                        <thead class="entry-table ">
                                            <tr bgcolor="#eee">
                                                <th width="25%">Overall Summary</th>
                                                <th>Invoice amount / Vendor amount</th>
                                                <th width="20%">Paid</th>
                                                <th>TDS Amount</th>
                                                <th>Paid To Vendor</th>
                                                <th rowspan="5">Balance (with TDS)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td width="25%">Daybook</td>
                                                <?php
                                                ob_start();
                                                if (!empty($daybookPayment1)) {
                                                    foreach ($daybookPayment1 as $vendor1) {
                                                    }
                                                    ?>
                                                <?php }
                                                ob_end_clean();
                                                ?>
                                                <td class="text-right ">
                                                    <?php echo !empty($daybookPayment1) ? Yii::app()->Controller->money_format_inr($daybookPayment1[0]["sumtotalamount"], 2) : '0'; ?>
                                                </td>
                                                <td class="text-right" width="20%">
                                                    <?php echo !empty($daybookPayment1) ? Yii::app()->Controller->money_format_inr($daybookPayment1[0]["sumtotalpaid"], 2) : '0'; ?>
                                                </td>
                                                <td class="text-right">
                                                    <?php echo !empty($daybookPayment1) ? Yii::app()->Controller->money_format_inr($daybookPayment1[0]["sumtotaltds"], 2) : '0'; ?>
                                                </td>
                                                <td class="text-right">
                                                    <?php echo !empty($daybookPayment1) ? Yii::app()->Controller->money_format_inr($daybookPayment1[0]["sumtotalpaidtovendor"], 2) : '0'; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <?php
                                                $purchase_return = Projects::model()->setPurchaseReturn($expreceipt_return1);
                                                $credit_datas = isset($purchase_return['credit']) ? $purchase_return['credit'] : array();
                                                $other_datas = isset($purchase_return['other']) ? $purchase_return['other'] : array();
                                                $sum_ret1 = 0;
                                                $credit_sum1 = 0;
                                                $other_sum1 = 0;
                                                ob_start();
                                                foreach ($credit_datas as $return_data) {
                                                    $credit_sum1 += $return_data['paidamount'];
                                                }
                                                foreach ($other_datas as $return_data1) {
                                                    $other_sum1 += $return_data1['paidamount'];
                                                }
                                                $sum_ret1 = $credit_sum1 + $other_sum1;
                                                ob_end_clean();
                                                ?>
                                                <td width="25%">Purchase Return</td>
                                                <td class="text-right">
                                                    <?php echo ("-" . $sum_ret1) ?>
                                                </td>
                                                <td width="20%" class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                                </td>
                                                <td class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                                </td>
                                                <td class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%">Pending Bills</td>
                                                <?php
                                                $sum_amount = 0;
                                                ?>
                                                <?php if (!empty($paymentData)) { ?>
                                                    <?php

                                                    ob_start();
                                                    foreach ($paymentData as $payments) {
                                                        $sum_amount += $payments['total_amount'] + $payments['additional_tot_amount'];
                                                    }
                                                    $pending_bill_items = ob_get_contents();
                                                    ob_end_clean();
                                                    ?>
                                                    <td class="text-right">
                                                        <?php echo Yii::app()->Controller->money_format_inr($sum_amount, 2); ?>
                                                    </td>
                                                <?php } ?>
                                                <td width="20%" class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                                </td>
                                                <td class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                                </td>
                                                <td class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%">Direct Vendor Payment</td>
                                                <td class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                                </td>
                                                <?php if (!empty($dailyvPayment1)) {
                                                    $totalAmount = 0;
                                                    $totalTds = 0;
                                                    $totalPaidVendor = 0;
                                                    ob_start();
                                                    foreach ($dailyvPayment1 as $dailyvendor1) {
                                                        $totalAmount += ($dailyvendor1["amount"] + $dailyvendor1["tax_amount"]);
                                                        $totalTds += $dailyvendor1["tds_amount"];
                                                        $totalPaidVendor += $dailyvendor1["paidamount"];
                                                    }
                                                    ob_end_clean();
                                                    ?>

                                                    <td width="20%" class="text-right">
                                                        <?php echo Yii::app()->Controller->money_format_inr($totalAmount, 2); ?>
                                                    </td>
                                                    <td class="text-right">
                                                        <?php echo Yii::app()->Controller->money_format_inr($totalTds, 2); ?>
                                                    </td>
                                                    <td class="text-right total_4 direct_pay_sum">
                                                        <?php echo Yii::app()->Controller->money_format_inr($totalPaidVendor, 2); ?>
                                                    </td>
                                                <?php } else { ?>

                                                    <td width="20%" class="text-right">
                                                        <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                                    </td>
                                                    <td class="text-right">
                                                        <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                                    </td>
                                                    <td class="text-right">
                                                        <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <?php
                                                $dayinvtotal1 = !empty($daybookPayment1) ? $daybookPayment1[0]["sumtotalamount"] : '0';
                                                $retinvtotal1 = ("-" . $sum_ret1);
                                                $pendinvtotal = $sum_amount;
                                                $directinvtotal1 = 0.00;

                                                $daypaid1 = !empty($daybookPayment1) ? $daybookPayment1[0]["sumtotalpaid"] : '0';
                                                $retpaid1 = 0.00;
                                                $pendpaid = 0.00;
                                                $directpaid1 = !empty($dailyvPayment1) ? $totalAmount : '0';

                                                $daytds1 = !empty($daybookPayment1) ? $daybookPayment1[0]["sumtotaltds"] : '0';
                                                $rettds1 = 0.00;
                                                $pendtds = 0.00;
                                                $directtds1 = !empty($dailyvPayment1) ? $totalTds : '0';

                                                $daypv1 = !empty($daybookPayment1) ? $daybookPayment1[0]["sumtotalpaidtovendor"] : '0';
                                                $retpv1 = 0.00;
                                                $pendpv = 0.00;
                                                $directpv1 = !empty($dailyvPayment1) ? $totalPaidVendor : '0';

                                                $overallinvtotal1 = $dayinvtotal1 + $retinvtotal1 + $pendinvtotal + $directinvtotal1;
                                                $overallpaidtotal1 = $daypaid1 + $retpaid1 + $pendpaid + $directpaid1;
                                                $overalltdstotal1 = $daytds1 + $rettds1 + $pendtds + $directtds1;
                                                $overallpvtotal1 = $daypv1 + $retpv1 + $pendpv + $directpv1;
                                                $overallbalance1 = $overallinvtotal1 - $overallpvtotal1;
                                                ?>
                                                <th width="25%" class="text-right">Total</th>
                                                <th class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr($overallinvtotal1, 2);
                                                    ?>
                                                </th>
                                                <th width="20%" class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr($overallpaidtotal1, 2);
                                                    ?>
                                                </th>
                                                <th class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr($overalltdstotal1, 2);
                                                    ?>
                                                </th>
                                                <th class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr($overallpvtotal1, 2);
                                                    ?>
                                                </th>
                                                <th class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr($overallbalance1, 2);
                                                    ?>
                                                </th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            <?php } ?>


                            <?php ?>
                            <!-- Unreconcil overall summary starts if pending bill exist ends -->

                        <?php } ?>
                        <?php if (isset($_GET['reconcil_type']) && ($_GET['reconcil_type'] == "unreconcil")) { ?>
                            <div id="table-wrapper">
                                <table class="table total-table ureconcil_table">
                                    <thead class="entry-table">
                                        <tr bgcolor="#eee">
                                            <th width="25%">Overall Summary</th>
                                            <th>Invoice amount / Vendor amount</th>
                                            <th width="20%">Paid</th>
                                            <th>TDS Amount</th>
                                            <th>Paid To Vendor</th>
                                            <th rowspan="5">Balance (with TDS)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td width="25%">Daybook</td>
                                            <?php
                                            ob_start();
                                            if (!empty($daybookPayment1)) {
                                                foreach ($daybookPayment1 as $vendor1) {
                                                }
                                                ?>
                                            <?php }
                                            ob_end_clean();
                                            ?>
                                            <td class="text-right ">
                                                <?php echo !empty($daybookPayment1) ? Yii::app()->Controller->money_format_inr($daybookPayment1[0]["sumtotalamount"], 2) : '0'; ?>
                                            </td>
                                            <td class="text-right" width="20%">
                                                <?php echo !empty($daybookPayment1) ? Yii::app()->Controller->money_format_inr($daybookPayment1[0]["sumtotalpaid"], 2) : '0'; ?>
                                            </td>
                                            <td class="text-right">
                                                <?php echo !empty($daybookPayment1) ? Yii::app()->Controller->money_format_inr($daybookPayment1[0]["sumtotaltds"], 2) : '0'; ?>
                                            </td>
                                            <td class="text-right">
                                                <?php echo !empty($daybookPayment1) ? Yii::app()->Controller->money_format_inr($daybookPayment1[0]["sumtotalpaidtovendor"], 2) : '0'; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <?php
                                            $purchase_return = Projects::model()->setPurchaseReturn($expreceipt_return1);
                                            $credit_datas = isset($purchase_return['credit']) ? $purchase_return['credit'] : array();
                                            $other_datas = isset($purchase_return['other']) ? $purchase_return['other'] : array();
                                            $sum_ret1 = 0;
                                            $credit_sum1 = 0;
                                            $other_sum1 = 0;
                                            ob_start();
                                            foreach ($credit_datas as $return_data) {
                                                $credit_sum1 += $return_data['paidamount'];
                                            }
                                            foreach ($other_datas as $return_data1) {
                                                $other_sum1 += $return_data1['paidamount'];
                                            }
                                            $sum_ret1 = $credit_sum1 + $other_sum1;
                                            ob_end_clean();
                                            ?>
                                            <td width="25%">Purchase Return</td>
                                            <td class="text-right">
                                                <?php echo ("-" . $sum_ret1) ?>
                                            </td>
                                            <td width="20%" class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                            </td>
                                            <td class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                            </td>
                                            <td class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="25%">Pending Bills</td>
                                            <?php
                                            $sum_amount = 0;
                                            ?>
                                            <?php if (!empty($paymentData)) { ?>
                                                <?php

                                                ob_start();
                                                foreach ($paymentData as $payments) {
                                                    $sum_amount += $payments['total_amount'] + $payments['additional_tot_amount'];
                                                }
                                                $pending_bill_items = ob_get_contents();
                                                ob_end_clean();
                                                ?>
                                                <td class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr($sum_amount, 2); ?>
                                                </td>
                                            <?php } ?>
                                            <td width="20%" class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                            </td>
                                            <td class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                            </td>
                                            <td class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="25%">Direct Vendor Payment</td>
                                            <td class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                            </td>
                                            <?php if (!empty($dailyvPayment1)) {
                                                $totalAmount = 0;
                                                $totalTds = 0;
                                                $totalPaidVendor = 0;
                                                ob_start();
                                                foreach ($dailyvPayment1 as $dailyvendor1) {
                                                    $totalAmount += ($dailyvendor1["amount"] + $dailyvendor1["tax_amount"]);
                                                    $totalTds += $dailyvendor1["tds_amount"];
                                                    $totalPaidVendor += $dailyvendor1["paidamount"];
                                                }
                                                ob_end_clean();
                                                ?>

                                                <td width="20%" class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr($totalAmount, 2); ?>
                                                </td>
                                                <td class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr($totalTds, 2); ?>
                                                </td>
                                                <td class="text-right total_4 direct_pay_sum">
                                                    <?php echo Yii::app()->Controller->money_format_inr($totalPaidVendor, 2); ?>
                                                </td>
                                            <?php } else { ?>

                                                <td width="20%" class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                                </td>
                                                <td class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                                </td>
                                                <td class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr(0, 2); ?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <?php
                                            $dayinvtotal1 = !empty($daybookPayment1) ? $daybookPayment1[0]["sumtotalamount"] : '0';
                                            $retinvtotal1 = ("-" . $sum_ret1);
                                            $pendinvtotal = $sum_amount;
                                            $directinvtotal1 = 0.00;

                                            $daypaid1 = !empty($daybookPayment1) ? $daybookPayment1[0]["sumtotalpaid"] : '0';
                                            $retpaid1 = 0.00;
                                            $pendpaid = 0.00;
                                            $directpaid1 = !empty($dailyvPayment1) ? $totalAmount : '0';

                                            $daytds1 = !empty($daybookPayment1) ? $daybookPayment1[0]["sumtotaltds"] : '0';
                                            $rettds1 = 0.00;
                                            $pendtds = 0.00;
                                            $directtds1 = !empty($dailyvPayment1) ? $totalTds : '0';

                                            $daypv1 = !empty($daybookPayment1) ? $daybookPayment1[0]["sumtotalpaidtovendor"] : '0';
                                            $retpv1 = 0.00;
                                            $pendpv = 0.00;
                                            $directpv1 = !empty($dailyvPayment1) ? $totalPaidVendor : '0';

                                            $overallinvtotal1 = $dayinvtotal1 + $retinvtotal1 + $pendinvtotal + $directinvtotal1;
                                            $overallpaidtotal1 = $daypaid1 + $retpaid1 + $pendpaid + $directpaid1;
                                            $overalltdstotal1 = $daytds1 + $rettds1 + $pendtds + $directtds1;
                                            $overallpvtotal1 = $daypv1 + $retpv1 + $pendpv + $directpv1;
                                            $overallbalance1 = $overallinvtotal1 - $overallpvtotal1;
                                            ?>
                                            <th width="25%" class="text-right">Total</th>
                                            <th class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr($overallinvtotal1, 2);
                                                ?>
                                            </th>
                                            <th width="20%" class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr($overallpaidtotal1, 2);
                                                ?>
                                            </th>
                                            <th class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr($overalltdstotal1, 2);
                                                ?>
                                            </th>
                                            <th class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr($overallpvtotal1, 2);
                                                ?>
                                            </th>
                                            <th class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr($overallbalance1, 2);
                                                ?>
                                            </th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                        <?php } ?>
                        <!-- overall summary ends -->
                        <!-- daybook summary -->

                        <?php if (isset($_GET['reconcil_type']) && ($_GET['reconcil_type'] == "reconcil")) {
                            if (isset($_GET['summary']) && ($_GET['summary'] == 0)) { ?>
                                <div class="row margin-top-10">
                                    <div class="col-xs-12">
                                        <div class="heading-title">Daybook Summary</div>
                                        <div class="dotted-line"></div>
                                    </div>
                                </div>
                                <div class="table-wrapper margin-top-10">
                                    <div class="d-flex">
                                        <div class="summary text-right margin-left-auto">Total
                                            <?php echo count($daybookPayment) ?> results.
                                        </div>
                                    </div>
                                    <div id="table total-table">
                                        <table class="table vendor_table reconcil_table">
                                            <thead class="entry-table">

                                                <tr class="first-thead-row">
                                                    <th width="25%">Project Name</th>
                                                    <th>Invoice amount / Vendor amount</th>
                                                    <th>Paid</th>
                                                    <th>TDS Amount</th>
                                                    <th>Paid To Vendor</th>
                                                    <th>Balance (with TDS)</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php
                                                $totalAmount = 0;
                                                $totalPaid = 0;
                                                $totalTds = 0;
                                                $totalPaidVendor = 0;
                                                ?>
                                                <?php
                                                if (!empty($daybookPayment)) {
                                                    $projectid_array = array();
                                                    ob_start();
                                                    foreach ($daybookPayment as $key => $value) {
                                                        $project_id = $value['projectid'];
                                                        if (!in_array($project_id, $projectid_array)) {
                                                            array_push($projectid_array, $project_id);

                                                            ?>
                                                            <tr>
                                                                <td>
                                                                    <?php echo $value['projectname'];
                                                                    ?>
                                                                </td>
                                                                <td class="text-right">
                                                                    <?php echo Yii::app()->Controller->money_format_inr($value['sumtotalamount'], 2) ?>
                                                                </td>
                                                                <td class="text-right">
                                                                    <?php echo Yii::app()->Controller->money_format_inr($value['totalpaid'], 2) ?>
                                                                </td>
                                                                <td class="text-right">
                                                                    <?php echo Yii::app()->Controller->money_format_inr($value['totaltds'], 2) ?>
                                                                </td>
                                                                <td class="text-right">
                                                                    <?php echo Yii::app()->Controller->money_format_inr($value['totalpaidtovendor'], 2) ?>
                                                                </td>
                                                                <td class="text-right">
                                                                    <?php echo Yii::app()->Controller->money_format_inr(($value['sumtotalamount'] - $value['totalpaid']), 2) ?>
                                                                </td>
                                                            </tr>
                                                        <?php }
                                                    }
                                                    $daybooksum = ob_get_contents();
                                                    ob_end_clean();
                                                    ?>

                                                    <tr bgcolor="#ededed">
                                                        <th class="text-right"><b>Total</b></th>
                                                        <th class="text-right "><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($dayinvtotal, 2); ?>
                                                            </b></th>
                                                        <th class="text-right"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($daypaid, 2); ?>
                                                            </b></th>
                                                        <th class="text-right"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($daytds, 2); ?>
                                                            </b></th>
                                                        <th class="text-right"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($daypaid, 2); ?>
                                                            </b></th>
                                                        <th class="text-right"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr(($dayinvtotal - $daypaid), 2); ?>
                                                            </b></th>
                                                    </tr>
                                                    <?php
                                                    echo $daybooksum;
                                                } ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            <?php }
                        } ?>
                        <?php if (isset($_GET['reconcil_type']) && $_GET['reconcil_type'] == "unreconcil") {
                            if (isset($_GET['summary']) && ($_GET['summary'] == 0)) { ?>
                                <div class="row margin-top-10">
                                    <div class="col-xs-12">
                                        <div class="heading-title">Daybook Summary</div>
                                        <div class="dotted-line"></div>
                                    </div>
                                </div>
                                <div class="table-wrapper margin-top-10">
                                    <div class="d-flex">
                                        <div class="summary text-right margin-left-auto">Total
                                            <?php echo count($daybookPayment1) ?> results.
                                        </div>
                                    </div>

                                    <div id="table-wrapper">
                                        <table class="table total-table">
                                            <thead class="entry-table">

                                                <tr class="first-thead-row">
                                                    <th width="25%">Project Name</th>
                                                    <th>Invoice amount / Vendor amount</th>
                                                    <th>Paid</th>
                                                    <th>TDS Amount</th>
                                                    <th>Paid To Vendor</th>
                                                    <th>Balance (with TDS)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (!empty($daybookPayment1)) {
                                                    $projectid_array = array();
                                                    ob_start();
                                                    foreach ($daybookPayment1 as $key => $value) {
                                                        $project_id = $value['projectid'];
                                                        if (!in_array($project_id, $projectid_array)) {
                                                            array_push($projectid_array, $project_id); ?>
                                                            <tr>
                                                                <td>
                                                                    <?php echo $value['projectname'];
                                                                    ?>
                                                                </td>
                                                                <td class="text-right">
                                                                    <?php echo Yii::app()->Controller->money_format_inr($value['totalamount'], 2) ?>
                                                                </td>
                                                                <td class="text-right">
                                                                    <?php echo Yii::app()->Controller->money_format_inr($value['totalpaid'], 2) ?>
                                                                </td>
                                                                <td class="text-right">
                                                                    <?php echo Yii::app()->Controller->money_format_inr($value['totaltds'], 2) ?>
                                                                </td>
                                                                <td class="text-right">
                                                                    <?php echo Yii::app()->Controller->money_format_inr($value['totalpaidtovendor'], 2) ?>
                                                                </td>
                                                                <td class="text-right">
                                                                    <?php echo Yii::app()->Controller->money_format_inr(($value['totalamount'] - $value['totalpaid']), 2) ?>
                                                                </td>
                                                            </tr>
                                                        <?php }
                                                    }
                                                    $daybooksum1 = ob_get_contents();
                                                    ob_end_clean();
                                                    ?>
                                                    <tr bgcolor="#ededed">
                                                        <th class="text-right"><b>Total</b></th>
                                                        <th class="text-right "><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($value['sumtotalamount'], 2); ?>
                                                            </b></th>
                                                        <th class="text-right"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($value['sumtotalpaid'], 2); ?>
                                                            </b></th>
                                                        <th class="text-right"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($value['sumtotaltds'], 2); ?>
                                                            </b></th>
                                                        <th class="text-right"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($value['sumtotalpaidtovendor'], 2); ?>
                                                            </b></th>
                                                        <th class="text-right"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr(($value['sumtotalamount'] - $value['sumtotalpaid']), 2); ?>
                                                            </b></th>
                                                    </tr>
                                                    <?php
                                                    echo $daybooksum1;
                                                } ?>

                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                                <!-- daybook summary ends-->
                            <?php }
                        } ?>
                        <!-- daybook details -->

                        <?php if (isset($_GET['reconcil_type']) && ($_GET['reconcil_type'] == "reconcil")) {
                            if (isset($_GET['summary']) && ($_GET['summary'] == 0)) { ?>
                                <div class="row margin-top-10">
                                    <div class="col-xs-12">
                                        <div class="heading-title">DAYBOOK Details</div>
                                        <div class="dotted-line"></div>
                                    </div>
                                </div>
                                <div class="table-wrapper margin-top-10">
                                    <div class="d-flex">
                                        <div class="summary text-right margin-left-auto">Total
                                            <?php echo count($daybookPayment) ?> results.
                                        </div>
                                    </div>

                                    <div id="table-wrapper">
                                        <table class="table total-table reconcil_table">
                                            <thead class="entry-table">

                                                <tr class="vendor-first-thead-row">
                                                    <th>#ID</th>
                                                    <th>Project</th>
                                                    <th>Date</th>
                                                    <th>Bill No / Description</th>
                                                    <th>Invoice / Vendor amount</th>
                                                    <th>Paid</th>
                                                    <th>TDS Amount</th>
                                                    <th>Paid To Vendor</th>
                                                    <th>Balance without TDS</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php
                                                $totalAmount = 0;
                                                $totalPaid = 0;
                                                $totalTds = 0;
                                                $totalPaidVendor = 0;
                                                ?>

                                                <?php
                                                if (!empty($daybookPayment)) {
                                                    $projectid_array = array();

                                                    $counts = array_count_values(
                                                        array_column($daybookPayment, 'projectid')
                                                    );
                                                    ob_start();
                                                    $balance_details = 0;
                                                    $processed_bills_arr = [];
                                                    foreach ($daybookPayment as $key => $value) {
                                                        if (!empty($value['bill_id'])) {
                                                            if (in_array($value['bill_id'], $processed_bills_arr)) {

                                                                $balance_details += 0 - $value['paid'];
                                                            } else {

                                                                $balance_details += $value['amount'] - $value['paid'];
                                                                $processed_bills_arr[] = $value['bill_id'];
                                                            }

                                                        }


                                                        ?>
                                                        <tr>
                                                            <td><b>#
                                                                    <?php echo $value['exp_id'] ?>
                                                                </b></td>
                                                            <?php
                                                            $project_id = $value['projectid'];
                                                            if (!in_array($project_id, $projectid_array)) {
                                                                array_push($projectid_array, $project_id); ?>

                                                                <td rowspan="<?php echo $counts[$value['projectid']]; ?>">
                                                                    <?php echo $value['projectname'] ?>
                                                                </td>
                                                            <?php } ?>
                                                            <td class="nowrap">
                                                                <?php echo date('d-m-Y', strtotime($value['date'])); ?>
                                                            </td>
                                                            <td>
                                                                <?php echo CHtml::link($value['billnumber'], Yii::app()->createUrl('bills/view&id=' . $value['bill_id']), array('class' => '', 'target' => '_blank')) .
                                                                    " - " . $value["description"]; ?>
                                                            </td>
                                                            <td class="text-right">
                                                                <?php echo Yii::app()->Controller->money_format_inr($value['amount'], 2) ?>
                                                            </td>
                                                            <td class="text-right">
                                                                <?php echo Yii::app()->Controller->money_format_inr($value['paid'], 2) ?>
                                                            </td>
                                                            <td class="text-right">
                                                                <?php echo Yii::app()->Controller->money_format_inr($value['expense_tds'], 2) ?>
                                                            </td>
                                                            <td class="text-right">
                                                                <?php
                                                                $paidtovendor = ($value['expense_type'] != 0) ? Yii::app()->Controller->money_format_inr($value['paidamount'], 2) : "0.00";
                                                                echo $paidtovendor;
                                                                ?>
                                                            </td>
                                                            <td class="text-right">
                                                                <?php echo Yii::app()->Controller->money_format_inr(($balance_details), 2) ?>
                                                            </td>

                                                        </tr>
                                                    <?php }
                                                    $daybookdetailsum = ob_get_contents();
                                                    ob_end_clean();
                                                    ?>
                                                    <tr bgcolor="#ededed">
                                                        <th class="text-right" colspan="4"><b>Total</b></th>
                                                        <th class="text-right"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($dayinvtotal, 2); ?>
                                                            </b></th>
                                                        <th class="text-right"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($daypaid, 2); ?>
                                                            </b></th>
                                                        <th class="text-right"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($daytds, 2); ?>
                                                            </b></th>
                                                        <th class="text-right"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($daypv, 2); ?>
                                                            </b></th>
                                                        <th class="text-right"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr(($dayinvtotal - $daypaid), 2); ?>
                                                            </b></th>
                                                    </tr>
                                                    <?php
                                                    echo $daybookdetailsum;
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            <?php }
                        } ?>
                        <?php if (isset($_GET['reconcil_type']) && $_GET['reconcil_type'] == "unreconcil") {
                            if (isset($_GET['summary']) && ($_GET['summary'] == 0)) { ?>
                                <div class="row margin-top-10">
                                    <div class="col-xs-12">
                                        <div class="heading-title">DAYBOOK Details</div>
                                        <div class="dotted-line"></div>
                                    </div>
                                </div>
                                <div class="table-wrapper margin-top-10">
                                    <div class="d-flex">
                                        <div class="summary text-right margin-left-auto">Total
                                            <?php echo count($daybookPayment1) ?> results.
                                        </div>

                                    </div>

                                    <div id="table-wrapper">
                                        <table class="table total-table ureconcil_table">
                                            <thead class="entry-table">

                                                <tr class="vendor-first-thead-row">
                                                    <th>#ID</th>
                                                    <th>Project</th>
                                                    <th>Date</th>
                                                    <th>Bill Number / Description</th>
                                                    <th>Invoice amount / Vendor amount</th>
                                                    <th>Paid</th>
                                                    <th>TDS Amount</th>
                                                    <th>Paid To Vendor</th>
                                                    <th>Balance without TDS</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (!empty($daybookPayment1)) {
                                                    $projectid_array = array();
                                                    $counts = array_count_values(
                                                        array_column($daybookPayment1, 'projectid')
                                                    );
                                                    ob_start();
                                                    foreach ($daybookPayment1 as $key => $value) {
                                                        ?>
                                                        <tr>
                                                            <td><b>#
                                                                    <?php echo $value['exp_id'] ?>
                                                                </b></td>
                                                            <?php
                                                            $project_id = $value['projectid'];
                                                            if (!in_array($project_id, $projectid_array)) {
                                                                array_push($projectid_array, $project_id); ?>

                                                                <td rowspan="<?php echo $counts[$value['projectid']]; ?>">
                                                                    <?php echo $value['projectname'] ?>
                                                                </td>
                                                            <?php } ?>
                                                            <td>
                                                                <?php echo $value['date'] ?>
                                                            </td>
                                                            <td>
                                                                <?php echo CHtml::link($value['billnumber'], Yii::app()->createUrl('bills/view&id=' . $value['bill_id']), array('class' => '', 'target' => '_blank')) .
                                                                    " - " . $value["description"]; ?>
                                                            </td>
                                                            <td class="text-right">
                                                                <?php echo Yii::app()->Controller->money_format_inr($value['amount'], 2) ?>
                                                            </td>
                                                            <td class="text-right">
                                                                <?php echo Yii::app()->Controller->money_format_inr($value['paid'], 2) ?>
                                                            </td>
                                                            <td class="text-right">
                                                                <?php echo Yii::app()->Controller->money_format_inr($value['expense_tds'], 2) ?>
                                                            </td>
                                                            <td class="text-right">
                                                                <?php echo Yii::app()->Controller->money_format_inr($value['paidamount'], 2) ?>
                                                            </td>
                                                            <td class="text-right">
                                                                <?php echo Yii::app()->Controller->money_format_inr(($value['amount'] - $value['paid']), 2) ?>
                                                            </td>

                                                        </tr>
                                                    <?php }
                                                    $daybookdetailsum1 = ob_get_contents();
                                                    ob_end_clean();
                                                    ?>
                                                    <tr bgcolor="#ededed">
                                                        <th class="text-right" colspan="4"><b>Total</b></th>
                                                        <th class="text-right"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($value['sumtotalamount'], 2); ?>
                                                            </b></th>
                                                        <th class="text-right"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($value['sumtotalpaid'], 2); ?>
                                                            </b></th>
                                                        <th class="text-right"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($value['sumtotaltds'], 2); ?>
                                                            </b></th>
                                                        <th class="text-right"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($value['sumtotalpaidtovendor'], 2); ?>
                                                            </b></th>
                                                        <th class="text-right"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr(($value['sumtotalamount'] - $value['sumtotalpaid']), 2); ?>
                                                            </b></th>
                                                    </tr>
                                                    <?php
                                                    echo $daybookdetailsum1;
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                                <!-- daybook details end -->
                            <?php }
                        } ?>
                        <!-- Purchase return -->

                        <?php if (isset($_GET['reconcil_type']) && ($_GET['reconcil_type'] == "reconcil")) {
                            if (isset($_GET['summary']) && ($_GET['summary'] == 0)) { ?>
                                <?php
                                $purchase_return = Projects::model()->setPurchaseReturn($expreceipt_return);

                                $credit_datas = isset($purchase_return['credit']) ? $purchase_return['credit'] : array();
                                ?>
                                <div class="row margin-top-10">
                                    <div class="col-xs-12">
                                        <div class="heading-title">Purchase Returns</div>
                                        <div class="dotted-line"></div>
                                    </div>
                                </div>
                                <div class="table-wrapper margin-top-10">
                                    <div class="d-flex">
                                        <div class="summary text-right margin-left-auto">Total
                                            <?php echo count($purchase_return); ?> results
                                        </div>
                                    </div>

                                    <div id="table-wrapper">
                                        <table class="table total-table reconcil_table">
                                            <thead class="entry-table">

                                                <?php
                                                $purchase_return = Projects::model()->setPurchaseReturn($expreceipt_return);

                                                $credit_datas = isset($purchase_return['credit']) ? $purchase_return['credit'] : array();
                                                $other_datas = isset($purchase_return['other']) ? $purchase_return['other'] : array();
                                                $h = 1;
                                                $credit_sum = 0;
                                                $q = 1;
                                                $other_sum = 0;
                                                ?>
                                                <tr>
                                                    <th>Payment Type</th>
                                                    <th>Date</th>
                                                    <th>Description</th>
                                                    <th width="20%">Invoice amount / Vendor amount</th>
                                                    <th></th>
                                                </tr>
                                                <?php
                                                $sum_ret = 0;
                                                $credit_sum1 = 0;
                                                $other_sum1 = 0;
                                                ob_start();
                                                foreach ($credit_datas as $return_data) {
                                                    $credit_sum1 += $return_data['amount'];
                                                }
                                                foreach ($other_datas as $return_data1) {
                                                    $other_sum1 += $return_data1['paidamount'];
                                                }
                                                $sum_ret = $credit_sum1 + $other_sum1;
                                                ob_end_clean();
                                                ?>
                                                <tr>
                                                    <th colspan="4" class="text-right">Total</th>
                                                    <th width="20%" class="text-right">
                                                        <?php echo Yii::app()->Controller->money_format_inr($sum_ret, 2) ?>
                                                    </th>
                                                </tr>

                                            </thead>
                                            <tbody>
                                                <?php
                                                ob_start();
                                                foreach ($credit_datas as $return_data) {
                                                    $credit_sum += $return_data['amount'];
                                                }
                                                foreach ($other_datas as $return_data1) {
                                                    $other_sum += $return_data1['paidamount'];
                                                }
                                                ob_end_clean();

                                                foreach ($credit_datas as $return_data) {

                                                    ?>
                                                    <tr>
                                                        <?php
                                                        if ($h == 1) {
                                                            ?>
                                                            <td rowspan="<?php echo count($credit_datas) ?>">
                                                                Credit
                                                            </td>
                                                            <?php
                                                        }
                                                        ?>
                                                        <td>
                                                            <?php echo Yii::app()->controller->changeDateForamt($return_data['date']); ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $return_data['description']; ?>
                                                        </td>
                                                        <td class="text-right">
                                                            <?php echo Yii::app()->Controller->money_format_inr($return_data['amount'], 2); ?>
                                                        </td>
                                                        <?php
                                                        if ($h == 1) {
                                                            ?>
                                                            <td rowspan="<?php echo count($credit_datas) ?>" class="text-right">
                                                                <?php echo Yii::app()->Controller->money_format_inr($credit_sum, 2); ?>
                                                            </td>
                                                        <?php } ?>


                                                    </tr>
                                                    <?php
                                                    $h++;
                                                }


                                                foreach ($other_datas as $return_data1) {
                                                    ?>
                                                    <tr>
                                                        <?php
                                                        if ($q == 1) { ?>
                                                            <td rowspan="<?php echo count($other_datas) ?>">
                                                                Cash/Cheque/Online/Petty Cash
                                                            </td>
                                                        <?php }
                                                        ?>
                                                        <td>
                                                            <?php echo Yii::app()->controller->changeDateForamt($return_data1['date']); ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $return_data1['description']; ?>
                                                        </td>
                                                        <td class="text-right">
                                                            <?php echo Yii::app()->Controller->money_format_inr($return_data1['paidamount'], 2); ?>
                                                        </td>
                                                        <?php
                                                        if ($q == 1) { ?>
                                                            <td rowspan="<?php echo count($other_datas) ?>" class="text-right">
                                                                <?php echo Yii::app()->Controller->money_format_inr($other_sum, 2); ?>
                                                            </td>
                                                        <?php } ?>

                                                    </tr>
                                                    <?php
                                                    $q++;
                                                }
                                                ?>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            <?php }
                        } ?>

                        <?php if (isset($_GET['reconcil_type']) && $_GET['reconcil_type'] == "unreconcil") {
                            if (isset($_GET['summary']) && ($_GET['summary'] == 0)) { ?>
                                <div class="row margin-top-10">
                                    <div class="col-xs-12">
                                        <div class="heading-title">Purchase Returns</div>
                                        <div class="dotted-line"></div>
                                    </div>
                                </div>
                                <?php

                                $purchase_return = Projects::model()->setPurchaseReturn($expreceipt_return1);

                                $credit_datas = isset($purchase_return['credit']) ? $purchase_return['credit'] : array();

                                ?>
                                <div class="table-wrapper margin-top-10">
                                    <div class="d-flex">
                                        <div class="summary text-right margin-left-auto">Total
                                            <?php echo count($purchase_return) ?> results
                                        </div>
                                    </div>

                                    <div id="table-wrapper">
                                        <table class="table total-table ureconcil_table">
                                            <thead class="entry-table">
                                                <tr>
                                                    <th colspan="5">Purchase Return</th>
                                                </tr>
                                                <?php
                                                $purchase_return = Projects::model()->setPurchaseReturn($expreceipt_return1);

                                                $credit_datas = isset($purchase_return['credit']) ? $purchase_return['credit'] : array();
                                                $other_datas = isset($purchase_return['other']) ? $purchase_return['other'] : array();
                                                $h = 1;
                                                $credit_sum = 0;
                                                $q = 1;
                                                $other_sum = 0;
                                                ?>
                                                <tr>
                                                    <th>Payment Type</th>
                                                    <th>Date</th>
                                                    <th>Description</th>
                                                    <th width="20%">Invoice amount / Vendor amount</th>
                                                    <th></th>
                                                </tr>
                                                <?php
                                                $sum_ret = 0;
                                                $credit_sum1 = 0;
                                                $other_sum1 = 0;
                                                ob_start();
                                                foreach ($credit_datas as $return_data) {
                                                    $credit_sum1 += $return_data['amount'];
                                                }
                                                foreach ($other_datas as $return_data1) {
                                                    $other_sum1 += $return_data1['paidamount'];
                                                }
                                                $sum_ret = $credit_sum1 + $other_sum1;
                                                ob_end_clean();
                                                ?>
                                                <tr>
                                                    <th colspan="4" class="text-right">Total</th>
                                                    <th width="20%" class="text-right">
                                                        <?php echo Yii::app()->Controller->money_format_inr($sum_ret, 2) ?>
                                                    </th>
                                                </tr>

                                            </thead>
                                            <tbody>
                                                <?php
                                                ob_start();
                                                foreach ($credit_datas as $return_data) {
                                                    $credit_sum += $return_data['amount'];
                                                }
                                                foreach ($other_datas as $return_data1) {
                                                    $other_sum += $return_data1['paidamount'];
                                                }
                                                ob_end_clean();

                                                foreach ($credit_datas as $return_data) {

                                                    ?>
                                                    <tr>
                                                        <?php
                                                        if ($h == 1) {
                                                            ?>
                                                            <td rowspan="<?php echo count($credit_datas) ?>">
                                                                Credit
                                                            </td>
                                                            <?php
                                                        }
                                                        ?>
                                                        <td>
                                                            <?php echo Yii::app()->controller->changeDateForamt($return_data['date']); ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $return_data['description']; ?>
                                                        </td>
                                                        <td class="text-right">
                                                            <?php echo Yii::app()->Controller->money_format_inr($return_data['amount'], 2); ?>
                                                        </td>
                                                        <?php
                                                        if ($h == 1) {
                                                            ?>
                                                            <td rowspan="<?php echo count($credit_datas) ?>" class="text-right">
                                                                <?php echo Yii::app()->Controller->money_format_inr($credit_sum, 2) ?>
                                                            </td>
                                                        <?php } ?>


                                                    </tr>
                                                    <?php
                                                    $h++;
                                                }


                                                foreach ($other_datas as $return_data1) {
                                                    ?>
                                                    <tr>
                                                        <?php
                                                        if ($q == 1) { ?>
                                                            <td rowspan="<?php echo count($other_datas) ?>">
                                                                Cash/Cheque/Online/Petty Cash
                                                            </td>
                                                        <?php }
                                                        ?>
                                                        <td>
                                                            <?php echo Yii::app()->controller->changeDateForamt($return_data1['date']); ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $return_data1['description']; ?>
                                                        </td>
                                                        <td class="text-right">
                                                            <?php echo Yii::app()->Controller->money_format_inr($return_data1['paidamount'], 2) ?>
                                                        </td>
                                                        <?php
                                                        if ($q == 1) { ?>
                                                            <td rowspan="<?php echo count($other_datas) ?>" class="text-right">
                                                                <?php echo Yii::app()->Controller->money_format_inr($other_sum, 2); ?>
                                                            </td>
                                                        <?php } ?>

                                                    </tr>
                                                    <?php
                                                    $q++;
                                                }
                                                ?>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>


                                <!-- Purchase return ends -->
                            <?php }
                        } ?>
                        <!-- DEFECT RETURN -->
                        <?php if (isset($_GET['reconcil_type']) && ($_GET['reconcil_type'] == "reconcil")) {
                            if (isset($_GET['summary']) && ($_GET['summary'] == 0)) {
                                ?>
                                <div class="row margin-top-10">
                                    <div class="col-xs-12">
                                        <div class="heading-title">Defect Return</div>
                                        <div class="dotted-line"></div>
                                    </div>
                                </div>
                                <div class="table-wrapper margin-top-10">
                                    <div class="d-flex">
                                        <div class="summary text-right margin-left-auto">Total
                                            <?php echo count($defect_return); ?> results.
                                        </div>
                                    </div>

                                    <div id="table-wrapper">
                                        <table class="table total-table reconcil_table">
                                            <thead class="entry-table">

                                                <tr>
                                                    <th>Warehouse</th>
                                                    <th>Receipt Number</th>
                                                    <th>Return Number</th>
                                                    <th>Return Date</th>
                                                    <th>Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($defect_return as $key => $value) { ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $value['warehouse_name'] ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $value['warehousereceipt_no'] ?>
                                                        </td>
                                                        <td>
                                                            <?php echo CHtml::link(
                                                                $value['return_number'],
                                                                'index.php?r=wh/warehouse/DefectView&id=' . $value['return_id'],
                                                                array('class' => 'link', 'target' => '_blank')
                                                            ) ?>
                                                        </td>
                                                        <td>
                                                            <?php echo Yii::app()->controller->changeDateForamt($value['return_date']) ?>
                                                        </td>
                                                        <td>
                                                            <?php echo Yii::app()->Controller->money_format_inr($value['return_amount'], 2) ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php }
                        } ?>
                        <!-- DEFECT RETUN ENDS -->
                        <!-- Pending bills -->

                        <?php if (isset($_GET['reconcil_type']) && $_GET['reconcil_type'] == "unreconcil") {
                            if (isset($_GET['summary']) && ($_GET['summary'] == 0)) {
                                ?>
                                <div class="row margin-top-10">
                                    <div class="col-xs-12">
                                        <div class="heading-title">Pending Bills</div>
                                        <div class="dotted-line"></div>
                                    </div>
                                </div>
                                <div class="table-wrapper margin-top-10">
                                    <div class="d-flex">
                                        <div class="summary text-right margin-left-auto">Total
                                            <?php echo count($paymentData) ?> results.
                                        </div>
                                    </div>

                                    <div id="table-wrapper">
                                        <table class="table total-table ureconcil_table">
                                            <?php if (!empty($paymentData)) {
                                                ?>
                                                <thead class="entry-table">

                                                    <tr>
                                                        <th>Project</th>
                                                        <th>Date</th>
                                                        <th>Bill Number / Description</th>
                                                        <th width="20%">Invoice amount / Vendor amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $sum_amount = 0;
                                                    ob_start();
                                                    foreach ($paymentData as $payments) {

                                                        $sum_amount += $payments['total_amount'] + $payments['additional_tot_amount'];

                                                        $bills = $payments['datas'];

                                                        $count = count($bills);
                                                        ?>

                                                        <tr>
                                                            <td rowspan="<?php echo $count ?>">
                                                                <?php echo $payments['project_name']; ?>
                                                            </td>
                                                            <?php
                                                            foreach ($bills as $bill) {

                                                                if (!empty($bill['bill_additionalcharge']) && isset($bill['bill_additionalcharge']))
                                                                    $bill["bill_totalamount"] += $bill['bill_additionalcharge'];
                                                                ?>


                                                                <td>
                                                                    <?php echo Yii::app()->controller->changeDateForamt($bill["bill_date"]); ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $bill["bill_number"]; ?>
                                                                </td>
                                                                <td class="text-right">
                                                                    <?php echo Yii::app()->Controller->money_format_inr($bill["bill_totalamount"], 2); ?>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                            }
                                                            ?>

                                                        <?php
                                                    }

                                                    $pending_bill_items = ob_get_contents(); // read ob1 ("a" . "b" . "c")
                                                    ob_end_clean();
                                                    ?>
                                                    <tr bgcolor="#eee">
                                                        <th colspan="3" class="text-right"><b>Total</b></th>
                                                        <th class="text-right total_1"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($sum_amount, 2); ?>
                                                            </b></th>
                                                    </tr>
                                                    <?php
                                                    echo $pending_bill_items;
                                                    ?>
                                                <?php }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- Pending bills end -->
                            <?php }
                        } ?>
                        <!-- Direct vendor payment -->
                        <?php if (isset($_GET['reconcil_type']) && ($_GET['reconcil_type'] == "reconcil")) {
                            if (isset($_GET['summary']) && ($_GET['summary'] == 0)) {
                                ?>

                                <div class="row margin-top-10">
                                    <div class="col-xs-12">
                                        <div class="heading-title">Direct Vendor Payment</div>
                                        <div class="dotted-line"></div>
                                    </div>
                                </div>
                                <div class="table-wrapper margin-top-10">
                                    <div class="d-flex">
                                        <div class="summary text-right margin-left-auto">Total
                                            <?php echo count($dailyvPayment) ?> results
                                        </div>
                                    </div>

                                    <div id="table-wrapper">
                                        <table class="table total-table reconcil_table">
                                            <?php if (!empty($dailyvPayment)) {
                                                ?>
                                                <?php
                                                $totalAmount = 0;
                                                $totalPaid = 0;
                                                $totalTds = 0;
                                                $totalPaidVendor = 0;
                                                ?>
                                                <thead class="entry-table">

                                                    <tr class="first-thead-row">
                                                        <th>#ID</th>
                                                        <th>Date</th>
                                                        <th>Purchase Number/Description</th>
                                                        <th>Amount Paid</th>
                                                        <th>TDS Amount</th>
                                                        <th width="20%">Paid to vendor</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php
                                                    ob_start();
                                                    foreach ($dailyvPayment as $dailyvendor) {
                                                        $totalAmount += ($dailyvendor["amount"] + $dailyvendor["tax_amount"]);
                                                        $totalTds += $dailyvendor["tds_amount"];
                                                        $totalPaidVendor += $dailyvendor["paidamount"];
                                                        ?>
                                                        <tr>
                                                            <td><b>#
                                                                    <?php echo $dailyvendor['daily_v_id'] ?>
                                                                </b></td>
                                                            <td>
                                                                <?php echo Yii::app()->controller->changeDateForamt($dailyvendor["date"]); ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                $po_number = "";
                                                                if ($dailyvendor["purchase_id"] != "") {
                                                                    $purchasedata = Purchase::model()->findByPk($dailyvendor["purchase_id"]);
                                                                    $po_number = $purchasedata['purchase_no'] . "-";
                                                                    echo CHtml::link($po_number, Yii::app()->createUrl('purchase/viewpurchase&pid=' . $dailyvendor["purchase_id"]), array('class' => '', 'target' => '_blank')) .
                                                                        " - " . $dailyvendor["description"];

                                                                } else {
                                                                    echo $dailyvendor["description"];
                                                                }
                                                                ?>
                                                            </td>
                                                            <td class="text-right">
                                                                <?php echo Yii::app()->Controller->money_format_inr($dailyvendor["amount"] + $dailyvendor["tax_amount"], 2); ?>
                                                            </td>
                                                            <td class="text-right">
                                                                <?php echo Yii::app()->Controller->money_format_inr($dailyvendor["tds_amount"], 2); ?>
                                                            </td>
                                                            <td class="text-right">
                                                                <?php echo Yii::app()->Controller->money_format_inr($dailyvendor["paidamount"], 2); ?>
                                                            </td>
                                                        </tr>
                                                    <?php }
                                                    $directdata = ob_get_contents();
                                                    ob_end_clean();
                                                    ?>
                                                    <tr>
                                                        <th colspan="3" class="text-right"><b>Total</b></th>
                                                        <th class="text-right total_2"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($totalAmount, 2); ?>
                                                            </b></th>
                                                        <th class="text-right total_3"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($totalTds, 2); ?>
                                                            </b></th>
                                                        <th class="text-right total_4 direct_pay_sum"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($totalPaidVendor, 2); ?>
                                                            </b></th>
                                                    </tr>
                                                    <?php
                                                    echo $directdata;
                                                    ?>
                                                    <?php

                                            }
                                            ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php }
                        } ?>
                        <?php if (isset($_GET['reconcil_type']) && $_GET['reconcil_type'] == "unreconcil") {
                            if (isset($_GET['summary']) && ($_GET['summary'] == 0)) {
                                ?>

                                <div class="row margin-top-10">
                                    <div class="col-xs-12">
                                        <div class="heading-title">Direct Vendor Payment</div>
                                        <div class="dotted-line"></div>
                                    </div>
                                </div>
                                <div class="table-wrapper margin-top-10">
                                    <div class="d-flex">
                                        <div class="summary text-right margin-left-auto">Total
                                            <?php echo count($dailyvPayment1) ?> results
                                        </div>
                                    </div>

                                    <div id="table-wrapper">
                                        <table class="table total-table ureconcil_table">
                                            <?php if (!empty($dailyvPayment1)) {

                                                ?>
                                                <?php
                                                $totalAmount = 0;
                                                $totalPaid = 0;
                                                $totalTds = 0;
                                                $totalPaidVendor = 0;
                                                ?>
                                                <thead class="entry-table">

                                                    <tr class="first-thead-row">
                                                        <th>#ID</th>
                                                        <th>Date</th>
                                                        <th>Description</th>
                                                        <th>Amount Paid</th>
                                                        <th>TDS Amount</th>
                                                        <th width="20%">Paid to vendor</th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="3" class="text-right"><b>Total</b></th>
                                                        <th class="text-right total_2"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($dailyvPayment1[0]["sumtotalamount"], 2); ?>
                                                            </b></th>
                                                        <th class="text-right total_3"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($dailyvPayment1[0]["sumtdsamount"], 2); ?>
                                                            </b></th>
                                                        <th class="text-right total_4 direct_pay_sum"><b>
                                                                <?php echo Yii::app()->Controller->money_format_inr($dailyvPayment1[0]["sumpaidamount"], 2); ?>
                                                            </b></th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php
                                                    foreach ($dailyvPayment1 as $dailyvendor) {
                                                        ?>
                                                        <tr>
                                                            <td><b>#
                                                                    <?php echo $dailyvendor['daily_v_id'] ?>
                                                                </b></td>
                                                            <td>
                                                                <?php echo Yii::app()->controller->changeDateForamt($dailyvendor["date"]); ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $dailyvendor["description"]; ?>
                                                            </td>
                                                            <td class="text-right">
                                                                <?php echo Yii::app()->Controller->money_format_inr($dailyvendor["amount"] + $dailyvendor["tax_amount"], 2); ?>
                                                            </td>
                                                            <td class="text-right">
                                                                <?php echo Yii::app()->Controller->money_format_inr($dailyvendor["tds_amount"], 2); ?>
                                                            </td>
                                                            <td class="text-right">
                                                                <?php echo Yii::app()->Controller->money_format_inr($dailyvendor["paidamount"], 2); ?>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>

                                                    <?php
                                                    $totalAmount = $totalAmount + $dailyvPayment1[0]["sumtotalamount"];
                                                    $totalPaid = $totalPaid + $dailyvPayment1[0]["sumtotalamount"];
                                                    $totalTds = $totalTds + $dailyvPayment1[0]["sumtdsamount"];
                                                    $totalPaidVendor = $totalPaidVendor + $dailyvPayment1[0]["sumpaidamount"];
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php }
                        } ?>
                    </div>
                <?php } else { ?>
                    <div class="ledger_view">
                        <?php if (filter_input(INPUT_GET, 'export')) { ?>
                            <div class="text-right">
                                Dt. &nbsp;&nbsp;
                                <?php
                                if ($_GET["date_to"]) {
                                    echo Yii::app()->Controller->changeDateForamt($_GET["date_to"]);
                                } else {
                                    echo Yii::app()->Controller->changeDateForamt(date('d-M-Y'));
                                }
                                ?>
                            </div>
                        <?php } ?>
                        <div class="text-center" style="font-family:initial;">
                            <?php if (!empty($vendor_id) && !empty($model)) {
                                ?>

                                <h4 style="margin-bottom:4px !important;">
                                    <?php echo $model->name; ?>
                                </h4>
                                <div style="text-transform: uppercase;
    margin-bottom: 2px;font-weight:bold;">
                                    <?php echo $model->address; ?>
                                </div>
                                <p style="margin-bottom:0px;">Email :
                                    <?php echo $model->email_id; ?>, Ph :
                                    <?php echo $model->phone; ?>
                                </p>
                            <?php } ?>
                            <div><small><b>Ledger for the period ended
                                        <?php
                                        if ($_GET["date_to"]) {
                                            echo Yii::app()->Controller->changeDateForamt($_GET["date_to"]);
                                        } else {
                                            echo Yii::app()->Controller->changeDateForamt(date('d-M-Y'));
                                        }
                                        ?>

                                    </b></small></div>
                        </div>

                        <?php if (isset($_GET['reconcil_type']) && ($_GET['reconcil_type'] == "reconcil")) { ?>
                            **NOTE: If the same bill is partially paid multiple times, then expense amount is included only once in
                            the total calculation .
                            <table class="table ledger_table reconcil_table">
                                <thead>
                                    <tr>
                                        <th colspan="6">
                                            <div>
                                                <?php echo isset($company_address->name) ? $company_address->name : ''; ?>
                                            </div>
                                            <div>
                                                <?php echo isset($company_address->address) ? $company_address->address : ''; ?>
                                            </div>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>Date</th>
                                        <th>Reff</th>
                                        <th>Narration</th>
                                        <th>Expenses</th>
                                        <th>Payments</th>
                                        <th>Balance</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php

                                    $totalAmount = 0;
                                    $totalPaid = 0;
                                    $totalTds = 0;
                                    $totalPaidVendor = 0;
                                    $balanceledger = 0;
                                    $processed_bill_ids = [];
                                    ?>

                                    <?php
                                    $purchase_return = Projects::model()->setPurchaseReturn($expreceipt_return);
                                    $vendor_summary = Vendors::model()->setVendorLedgerViewArray($daybookPayment, $dailyvPayment, $purchase_return);
                                    //echo "<pre>";print_r($vendor_summary);exit;
                                    foreach ($vendor_summary as $ledger_summary) {

                                        if ($ledger_summary['key'] == 1) {
                                            if (!empty($ledger_summary['bill_id'])) {
                                                // Check if the bill_id has been processed before
                                                if (in_array($ledger_summary['bill_id'], $processed_bill_ids)) {
                                                    // Apply the formula for duplicate bill_id
                                                    $balanceledger += 0 - $ledger_summary['paid'];
                                                } else {
                                                    // Apply the formula for new bill_id and mark it as processed
                                                    $balanceledger += $ledger_summary['amount'] - $ledger_summary['paid'];
                                                    $processed_bill_ids[] = $ledger_summary['bill_id'];
                                                }
                                            } else {
                                                // Apply the formula for records without a bill_id
                                                $balanceledger += $ledger_summary['amount'] - $ledger_summary['paid'];
                                            }
                                        } else if ($ledger_summary['key'] == 2) {
                                            $balanceledger += 0 - $ledger_summary['amount'];
                                        } else if ($ledger_summary['key'] == 3) {
                                            if ($ledger_summary['payment_type'] == 107) {
                                                $balanceledger -= $ledger_summary['amount'] - 0;
                                            } else {
                                                $balanceledger -= $ledger_summary['paidamount'] - 0;
                                            }

                                        } else {
                                            $balanceledger += ($ledger_summary['bill_totalamount'] + $ledger_summary['bill_additionalcharge']) - 0;
                                        }
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo Yii::app()->controller->changeDateForamt($ledger_summary['date']); ?>
                                            </td>

                                            <td>
                                                <?php
                                                if ($ledger_summary['key'] == 3) {
                                                    if (isset($ledger_summary['return_number'])) {
                                                        echo CHtml::link($ledger_summary['return_number'], Yii::app()->createUrl('PurchaseReturn/view&id=' . $ledger_summary['return_id']), array('class' => '', 'target' => '_blank'));
                                                    } else {
                                                        echo "";
                                                    }

                                                } else {
                                                    if (isset($ledger_summary['billnumber'])) {
                                                        echo CHtml::link($ledger_summary['billnumber'], Yii::app()->createUrl('bills/view&id=' . $ledger_summary['bill_id']), array('class' => '', 'target' => '_blank'));
                                                    } else if (isset($ledger_summary['bill_number'])) {
                                                        echo CHtml::link($ledger_summary['bill_number'], Yii::app()->createUrl('bills/view&id=' . $ledger_summary['bill_id']), array('class' => '', 'target' => '_blank'));
                                                    } else {
                                                        echo "--";
                                                    }
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php

                                                if (isset($ledger_summary['description'])) {
                                                    echo $ledger_summary['description'];
                                                } else {
                                                    echo "--";
                                                }
                                                ?>

                                            </td>
                                            <td class="text-right">
                                                <?php
                                                if ($ledger_summary['key'] == 1) {
                                                    echo Yii::app()->Controller->money_format_inr($ledger_summary['amount'], 2);
                                                } else if ($ledger_summary['key'] == 2) {
                                                    echo "";
                                                } else if ($ledger_summary['key'] == 3) {
                                                    if ($ledger_summary['payment_type'] == 107) {
                                                        echo Yii::app()->Controller->money_format_inr($ledger_summary['amount'], 2);
                                                    } else {
                                                        echo Yii::app()->Controller->money_format_inr($ledger_summary['paidamount'], 2);
                                                    }

                                                } else {
                                                    $total_bill_amount = $ledger_summary['bill_totalamount'] + $ledger_summary['bill_additionalcharge'];
                                                    echo Yii::app()->Controller->money_format_inr($total_bill_amount, 2);
                                                }
                                                ?>
                                            </td>
                                            <td class="text-right">
                                                <?php
                                                if ($ledger_summary['key'] == 1) {
                                                    echo Yii::app()->Controller->money_format_inr($ledger_summary['paid'], 2);
                                                } else if ($ledger_summary['key'] == 2) {
                                                    echo Yii::app()->Controller->money_format_inr($ledger_summary['amount'], 2);
                                                } else if ($ledger_summary['key'] == 3) {
                                                    echo "";
                                                } else {
                                                    echo "";
                                                }
                                                ?>
                                            </td>
                                            <td class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr($balanceledger, 2); ?>
                                            </td>
                                        </tr>

                                    <?php } ?>

                                </tbody>
                                <?php
                                if (!empty($dailyvPayment)) {
                                    $totalAmount = 0;
                                    $totalTds = 0;
                                    $totalPaidVendor = 0;
                                    ob_start();
                                    foreach ($dailyvPayment as $dailyvendor) {
                                        $totalAmount += ($dailyvendor["amount"] + $dailyvendor["tax_amount"]);
                                        $totalTds += $dailyvendor["tds_amount"];
                                        $totalPaidVendor += $dailyvendor["paidamount"];
                                    }
                                    ob_end_clean();
                                }
                                $purchase_return = Projects::model()->setPurchaseReturn($expreceipt_return);
                                // echo "<pre>";print_r($purchase_return);exit;
                                $credit_datas = isset($purchase_return['credit']) ? $purchase_return['credit'] : array();
                                $other_datas = isset($purchase_return['other']) ? $purchase_return['other'] : array();
                                $sum_ret = 0;
                                $credit_sum1 = 0;
                                $other_sum1 = 0;
                                ob_start();
                                foreach ($credit_datas as $return_data) {
                                    $credit_sum1 += $return_data['amount'];
                                }
                                foreach ($other_datas as $return_data1) {
                                    $other_sum1 += $return_data1['paidamount'];
                                }
                                $sum_ret = $credit_sum1 + $other_sum1;
                                ob_end_clean();
                                $dayinvtotal = !empty($daybookPayment) ? $total_vendor_Amount : '0';
                                $retinvtotal = ("-" . $sum_ret);
                                $directinvtotal = 0.00;

                                $daypaid = !empty($daybookPayment) ? $total_Amount_paid_to_vendor : '0';
                                $retpaid = 0.00;
                                $directpaid = !empty($dailyvPayment) ? $totalAmount : '0';

                                $daytds = !empty($daybookPayment) ? $daybookPayment[0]["sumtotaltds"] : '0';
                                $rettds = 0.00;
                                $directtds = !empty($dailyvPayment) ? $totalTds : '0';

                                $daypv = !empty($daybookPayment) ? $total_Amount_paid_to_vendor : '0';
                                $retpv = 0.00;
                                $directpv = !empty($dailyvPayment) ? $totalPaidVendor : '0';

                                $overallinvtotal = $dayinvtotal + $retinvtotal + $directinvtotal;
                                $overallpaidtotal = $daypaid + $retpaid + $directpaid;
                                $overalltdstotal = $daytds + $rettds + $directtds;
                                $overallpvtotal = $daypv + $retpv + $directpv;
                                $overallbalance = (str_replace(",", "", $overallinvtotal) - str_replace(",", "", $overallpaidtotal));

                                ?>
                                <?php
                                $sum1 = 0;
                                $sum2 = 0;
                                $sum3 = 0;
                                $sum3a = 0;
                                $sum4 = 0;
                                $sum5 = 0;
                                $processed_bills = [];
                                ob_start();
                                $purchase_return = Projects::model()->setPurchaseReturn($expreceipt_return);
                                $credit_datas = isset($purchase_return['credit']) ? $purchase_return['credit'] : array();
                                $other_datas = isset($purchase_return['other']) ? $purchase_return['other'] : array();
                                //echo "<pre>";print_r($daybookPayment);exit;
                                foreach ($daybookPayment as $key => $value) {

                                    if (!empty($value['bill_id'])) {
                                        if (in_array($value['bill_id'], $processed_bills)) {

                                            $sum1 += 0;
                                        } else {

                                            $sum1 += $value['amount'];
                                            $processed_bills[] = $value['bill_id'];
                                        }

                                    } else {
                                        $sum1 += $value['amount'];
                                    }
                                    $sum5 += $value["paid"];
                                }

                                foreach ($dailyvPayment as $key => $value) {
                                    $sum2 += $value['amount'] + $value["tax_amount"];
                                }
                                foreach ($credit_datas as $return_data) {
                                    $sum3 += $return_data['amount'];
                                }
                                foreach ($other_datas as $return_data1) {
                                    $sum3a += $return_data1['paidamount'];
                                }


                                $expense_sum = $sum1 + $sum4 + -$sum3 + -$sum3a;
                                ob_end_clean();
                                ?>
                                <tfoot>
                                    <tr>

                                        <th colspan="3"></th>
                                        <th class="text-right">
                                            <?php echo Yii::app()->Controller->money_format_inr($expense_sum, 2) ?>
                                        </th>
                                        <th class="text-right">
                                            <?php echo Yii::app()->Controller->money_format_inr(($sum2 + $sum5), 2) ?>
                                        </th>
                                        <th class="text-right">&nbsp;</th>
                                    </tr>
                                    <tr>
                                        <th colspan="5" class="text-right">Closing Balance:</th>
                                        <th class="text-right">
                                            <?php echo Yii::app()->Controller->money_format_inr(round($balanceledger), 2) ?>
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        <?php } ?>
                        <?php if (isset($_GET['reconcil_type']) && ($_GET['reconcil_type'] == "unreconcil")) { ?>
                            <table class="table ledger_table ureconcil_table">
                                <thead>
                                    <tr>
                                        <th colspan="6">
                                            <div>
                                                <?php echo isset($company_address->name) ? $company_address->name : ''; ?>
                                            </div>
                                            <div>
                                                <?php echo isset($company_address->address) ? $company_address->address : ''; ?>
                                            </div>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>Date</th>
                                        <th>Reff</th>
                                        <th>Narration</th>
                                        <th>Expenses</th>
                                        <th>Payments</th>
                                        <th>Balance</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php

                                    $totalAmount = 0;
                                    $totalPaid = 0;
                                    $totalTds = 0;
                                    $totalPaidVendor = 0;
                                    $balanceledger = 0;
                                    ?>

                                    <?php
                                    $purchase_return = Projects::model()->setPurchaseReturn($expreceipt_return1);
                                    $vendor_summary = Vendors::model()->setVendorLedgerViewArray1($daybookPayment1, $dailyvPayment1, $purchase_return, $paymentData);
                                    foreach ($vendor_summary as $ledger_summary) {

                                        if ($ledger_summary['key'] == 1) {
                                            $balanceledger += $ledger_summary['amount'] - $ledger_summary['paid'];
                                        } else if ($ledger_summary['key'] == 2) {
                                            $balanceledger += 0 - $ledger_summary['amount'];
                                        } else if ($ledger_summary['key'] == 3) {
                                            $balanceledger -= $ledger_summary['paidamount'] - 0;
                                        } else {
                                            $balanceledger += ($ledger_summary['bill_totalamount'] + $ledger_summary['bill_additionalcharge']) - 0;
                                        }
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo Yii::app()->controller->changeDateForamt($ledger_summary['date']); ?>
                                            </td>

                                            <td>
                                                <?php
                                                if (isset($ledger_summary['billnumber'])) {
                                                    echo CHtml::link($ledger_summary['billnumber'], Yii::app()->createUrl('bills/view&id=' . $ledger_summary['bill_id']), array('class' => '', 'target' => '_blank'));
                                                } else if (isset($ledger_summary['bill_number'])) {
                                                    echo CHtml::link($ledger_summary['bill_number'], Yii::app()->createUrl('bills/view&id=' . $ledger_summary['bill_id']), array('class' => '', 'target' => '_blank'));
                                                } else {
                                                    echo "--";
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php

                                                if (isset($ledger_summary['description'])) {
                                                    echo $ledger_summary['description'];
                                                } else {
                                                    echo "--";
                                                }
                                                ?>

                                            </td>
                                            <td class="text-right">
                                                <?php
                                                if ($ledger_summary['key'] == 1) {
                                                    echo Yii::app()->Controller->money_format_inr($ledger_summary['amount'], 2);
                                                } else if ($ledger_summary['key'] == 2) {
                                                    echo "";
                                                } else if ($ledger_summary['key'] == 3) {
                                                    echo Yii::app()->Controller->money_format_inr($ledger_summary['paidamount'], 2);
                                                } else {
                                                    $total_bill_amount = $ledger_summary['bill_totalamount'] + $ledger_summary['bill_additionalcharge'];
                                                    echo Yii::app()->Controller->money_format_inr($total_bill_amount, 2);
                                                }
                                                ?>
                                            </td>
                                            <td class="text-right">
                                                <?php
                                                if ($ledger_summary['key'] == 1) {
                                                    echo Yii::app()->Controller->money_format_inr($ledger_summary['paid'], 2);
                                                } else if ($ledger_summary['key'] == 2) {
                                                    echo Yii::app()->Controller->money_format_inr($ledger_summary['amount'], 2);
                                                } else if ($ledger_summary['key'] == 3) {
                                                    echo "";
                                                } else {
                                                    echo "";
                                                }
                                                ?>
                                            </td>
                                            <td class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr($balanceledger, 2); ?>
                                            </td>
                                        </tr>

                                    <?php } ?>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <?php
                                        $sum1 = 0;
                                        $sum2 = 0;
                                        $sum3 = 0;
                                        $sum3a = 0;
                                        $sum4 = 0;
                                        $sum5 = 0;
                                        ob_start();
                                        $purchase_return = Projects::model()->setPurchaseReturn($expreceipt_return1);
                                        $credit_datas = isset($purchase_return['credit']) ? $purchase_return['credit'] : array();
                                        $other_datas = isset($purchase_return['other']) ? $purchase_return['other'] : array();

                                        foreach ($daybookPayment1 as $key => $value) {
                                            $sum1 += $value['amount'];
                                            $sum5 += $value["paid"];
                                        }

                                        foreach ($dailyvPayment1 as $key => $value) {
                                            $sum2 += $value['amount'] + $value["tax_amount"];
                                        }
                                        foreach ($credit_datas as $return_data) {
                                            $sum3 += $return_data['paidamount'];
                                        }
                                        foreach ($other_datas as $return_data1) {
                                            $sum3a += $return_data1['paidamount'];
                                        }

                                        foreach ($paymentData as $payments) {
                                            $bills = $payments['datas'];
                                            foreach ($bills as $bill) {
                                                $sum4 += $bill["bill_totalamount"] + $bill['bill_additionalcharge'];
                                            }
                                        }
                                        $expense_sum = $sum1 + $sum4 + -$sum3 + -$sum3a;
                                        ob_end_clean();
                                        ?>
                                        <th colspan="3"></th>
                                        <th class="text-right">
                                            <?php echo Yii::app()->Controller->money_format_inr($expense_sum, 2) ?>
                                        </th>
                                        <th class="text-right">
                                            <?php echo Yii::app()->Controller->money_format_inr(($sum2 + $sum5), 2) ?>
                                        </th>
                                        <th class="text-right">&nbsp;</th>
                                    </tr>
                                    <tr>
                                        <th colspan="5" class="text-right">Closing Balance:</th>
                                        <th class="text-right">
                                            <?php echo Yii::app()->Controller->money_format_inr(round($balanceledger), 2) ?>
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        <?php } ?>
                    </div>
                <?php } ?>

            </div>
            <?php
        } else if ((empty($sql) || empty($client)) && ($vendor_id != 0)) {

            echo "<br><p> No results found.</p>";
        }
        if ((empty($sql) || empty($client)) && ($vendor_id == 0 || $vendor_id == '')) {
            ?>
            <div id="contenthtml" class="contentdiv <?php echo (filter_input(INPUT_GET, 'export') ? 'pdf_spacing' : '') ?>">
                <div class="row margin-top-10">
                    <div class="col-xs-12">
                        <div class="heading-title">Main Vendors</div>
                        <div class="dotted-line"></div>
                    </div>
                </div>
                <?php
                $datefrom = isset($_REQUEST['date_from']) ? $_REQUEST['date_from'] : "";
                $dateto = isset($_REQUEST['date_to']) ? $_REQUEST['date_to'] : "";
                $count = 0;
                if (!empty($datefrom)) {
                    foreach ($paymentData as $paymentList) {
                        if (!empty($paymentList["daybookpaid"]) || !empty($paymentList["vendorpaid"])) {
                            $count++;
                        }
                    }
                }

                ?>


                <div class="table-wrapper margin-top-10">
                    <div class="d-flex">
                        <p class="margin-bottom-0">
                            **NOTE: Please check if there is any Pending Bills. Pending Bills are not included in Invoice
                            Calculations
                        </p>
                        <div class="summary text-right margin-left-auto">Total
                            <?php
                            if (!empty($datefrom)) {
                                echo $count;
                            } else {
                                echo count($paymentData);
                            } ?> results.
                        </div>
                    </div>
                    <div id="table-wrapper">

                        <table class="table total-table" id="fix_table">
                            <thead class="entry-table">
                                <tr>
                                    <th>Sl No.</th>
                                    <th>Vendor Name</th>
                                    <th class="text-right">Invoice</th>
                                    <th class="text-right">Payments</th>
                                    <th class="text-right">Balance</th>
                                </tr>
                                <tr class="grandtotal">
                                    <?php
                                    $finalbalance = 0;
                                    $finalinvoice_sum = 0;
                                    $finalpayment_sum = 0;
                                    $totalPayment = 0;
                                    $datefrom = isset($_REQUEST['date_from']) ? $_REQUEST['date_from'] : "";
                                    $dateto = isset($_REQUEST['date_to']) ? $_REQUEST['date_to'] : "";
                                    if (!empty($datefrom)) {
                                        foreach ($paymentData as $paymentList) {
                                            if (!empty($paymentList["daybookpaid"]) || !empty($paymentList["vendorpaid"])) {
                                                if (!empty($paymentList['billadditionaltotal'])) {
                                                    $paymentList["billtotal"] += $paymentList['billadditionaltotal'];
                                                }
                                                $totalPayment = $paymentList["daybookpaid"] + $paymentList["vendorpaid"];
                                                $balancePaid = $paymentList["billtotal"] - $totalPayment;
                                                $invoice = Yii::app()->Controller->money_format_inr($paymentList['billtotal'], 2);
                                                $datefrom = isset($_REQUEST['date_from']) ? $_REQUEST['date_from'] : "";
                                                $dateto = isset($_REQUEST['date_to']) ? $_REQUEST['date_to'] : "";
                                                $invoiceSum = Vendors::model()->getinvoiceSum($paymentList['vendor_id'], $datefrom, $dateto);
                                                // $invoiceSum = Vendors::model()->getinvoiceSum($paymentList['vendor_id']);
                                                $payment = $totalPayment;
                                                $finalinvoice_sum += $invoiceSum;
                                                $finalpayment_sum += (str_replace(",", "", $payment));
                                                $finalbalance = ($finalinvoice_sum - $finalpayment_sum);

                                            }

                                        }

                                    } else {
                                        foreach ($paymentData as $paymentList) {
                                            if (!empty($paymentList['billadditionaltotal'])) {
                                                $paymentList["billtotal"] += $paymentList['billadditionaltotal'];
                                            }
                                            $totalPayment = $paymentList["daybookpaid"] + $paymentList["vendorpaid"];
                                            $balancePaid = $paymentList["billtotal"] - $totalPayment;
                                            $invoice = Yii::app()->Controller->money_format_inr($paymentList['billtotal'], 2);
                                            $datefrom = isset($_REQUEST['date_from']) ? $_REQUEST['date_from'] : "";
                                            $dateto = isset($_REQUEST['date_to']) ? $_REQUEST['date_to'] : "";
                                            $invoiceSum = Vendors::model()->getinvoiceSum($paymentList['vendor_id'], $datefrom, $dateto);
                                            // $invoiceSum = Vendors::model()->getinvoiceSum($paymentList['vendor_id']);
                                            $payment = $totalPayment;
                                            $finalinvoice_sum += $invoiceSum;
                                            $finalpayment_sum += (str_replace(",", "", $payment));
                                            $finalbalance = ($finalinvoice_sum - $finalpayment_sum);
                                        }

                                    }

                                    $sumTotal = $paymentSum["daybooksum"] + $paymentSum["vendorsum"] - $paymentSum["daybookcreditsum"];
                                    $balanceSum = $paymentSum["billsum"] - $sumTotal;
                                    ?>
                                    <td style="border-right: none !important;"></td>
                                    <td colspan="1" class="text-right"><b>Total</b></td>
                                    <td class="text-right">
                                        <b>
                                            <?php echo Yii::app()->Controller->money_format_inr($finalinvoice_sum, 2); ?>
                                        </b>
                                    </td>
                                    <td class="text-right">
                                        <b>
                                            <?php echo Yii::app()->Controller->money_format_inr($finalpayment_sum, 2); ?>
                                        </b>
                                    </td>
                                    <td class="text-right">
                                        <b>
                                            <?php echo Yii::app()->Controller->money_format_inr($finalbalance, 2); ?>
                                        </b>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                //echo "<pre>";print_r($paymentData);exit;
                                $datefrom = isset($_REQUEST['date_from']) ? $_REQUEST['date_from'] : "";
                                $dateto = isset($_REQUEST['date_to']) ? $_REQUEST['date_to'] : "";
                                if (!empty($datefrom)) {
                                    foreach ($paymentData as $paymentList) {
                                        if (!empty($paymentList["daybookpaid"]) || !empty($paymentList["vendorpaid"])) {
                                            if (!empty($paymentList['billadditionaltotal'])) {
                                                $paymentList["billtotal"] += $paymentList['billadditionaltotal'];
                                            }
                                            $totalPayment = $paymentList["daybookpaid"] + $paymentList["vendorpaid"];

                                            $balancePaid = $paymentList["billtotal"] - $totalPayment;
                                            $invoice = Yii::app()->Controller->money_format_inr($paymentList['billtotal'], 2);


                                            $invoiceSum = Vendors::model()->getinvoiceSum($paymentList['vendor_id'], $datefrom, $dateto);
                                            $invoiceSum = round((float) $invoiceSum, 2);
                                            $payment = round(str_replace(",", "", $totalPayment), 2);
                                            $balance = ($invoiceSum - round($payment, 2));
                                            if ($checked_zero == 0) {
                                                if ($balance != 0) {

                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $i; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $paymentList['name']; ?>
                                                        </td>
                                                        <td class="text-right">

                                                            <?php

                                                            echo Yii::app()->Controller->money_format_inr($invoiceSum, 2); ?>
                                                        </td>
                                                        <td class="text-right">
                                                            <?php echo Yii::app()->Controller->money_format_inr($totalPayment, 2); ?>
                                                        </td>
                                                        <td class="text-right">
                                                            <a class=""
                                                                href="<?php echo $this->createAbsoluteUrl('vendors/Paymentreport', array("view_type" => 'vendor', "vendor_id" => $paymentList["vendor_id"], "company_id" => $company_id, "date_from" => $date_from, "date_to" => $date_to, 'reconcil_type' => $reconcil_type, "summary" => isset($_GET['summary']) ? $_GET['summary'] : 0)) ?>">
                                                                <?php echo Yii::app()->Controller->money_format_inr($balance, 2); ?>
                                                        </td></a>

                                                    </tr>

                                                    <?php
                                                    $i = $i + 1;

                                                }
                                            } else { ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $i; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $paymentList['name']; ?>
                                                    </td>
                                                    <td class="text-right">

                                                        <?php

                                                        echo Yii::app()->Controller->money_format_inr($invoiceSum, 2); ?>
                                                    </td>
                                                    <td class="text-right">
                                                        <?php echo Yii::app()->Controller->money_format_inr($totalPayment, 2); ?>
                                                    </td>
                                                    <td class="text-right">
                                                        <?php if (!filter_input(INPUT_GET, 'export')) { ?>
                                                            <a class="" href="<?php echo $this->createAbsoluteUrl(
                                                                'vendors/Paymentreport',
                                                                array(
                                                                    "view_type" => 'vendor',
                                                                    "vendor_id" => $paymentList["vendor_id"],
                                                                    "company_id" => $company_id,
                                                                    "date_from" => $date_from,
                                                                    "date_to" => $date_to,
                                                                    'reconcil_type' => $reconcil_type,
                                                                    "summary" => isset($_GET['summary']) ? $_GET['summary'] : 0
                                                                )
                                                            ) ?>">
                                                                <?php echo Yii::app()->Controller->money_format_inr($balance, 2); ?>
                                                            </a>
                                                        <?php } else { ?>
                                                            <?php echo Yii::app()->Controller->money_format_inr($balance, 2); ?>
                                                        <?php } ?>
                                                    </td>
                                                </tr>

                                                <?php
                                                $i = $i + 1;
                                            }
                                        }
                                    }

                                } else {
                                    foreach ($paymentData as $paymentList) {

                                        if (!empty($paymentList['billadditionaltotal'])) {
                                            $paymentList["billtotal"] += $paymentList['billadditionaltotal'];
                                        }
                                        $totalPayment = $paymentList["daybookpaid"] + $paymentList["vendorpaid"];

                                        $balancePaid = $paymentList["billtotal"] - $totalPayment;
                                        $invoice = Yii::app()->Controller->money_format_inr($paymentList['billtotal'], 2);


                                        $invoiceSum = Vendors::model()->getinvoiceSum($paymentList['vendor_id'], $datefrom, $dateto);
                                        $invoiceSum = round((float) $invoiceSum, 2);
                                        $payment = round(str_replace(",", "", $totalPayment), 2);
                                        $balance = ($invoiceSum - round($payment, 2));
                                        if ($checked_zero == 0) {
                                            if ($balance != 0) {

                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $i; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $paymentList['name']; ?>
                                                    </td>
                                                    <td class="text-right">

                                                        <?php

                                                        echo Yii::app()->Controller->money_format_inr($invoiceSum, 2); ?>
                                                    </td>
                                                    <td class="text-right">
                                                        <?php echo Yii::app()->Controller->money_format_inr($totalPayment, 2); ?>
                                                    </td>
                                                    <td class="text-right">
                                                        <a class=""
                                                            href="<?php echo $this->createAbsoluteUrl('vendors/Paymentreport', array("view_type" => 'vendor', "vendor_id" => $paymentList["vendor_id"], "company_id" => $company_id, "date_from" => $date_from, "date_to" => $date_to, 'reconcil_type' => $reconcil_type, "summary" => isset($_GET['summary']) ? $_GET['summary'] : 0)) ?>">
                                                            <?php echo Yii::app()->Controller->money_format_inr($balance, 2); ?>
                                                    </td></a>

                                                </tr>

                                                <?php
                                                $i = $i + 1;

                                            }
                                        } else { ?>
                                            <tr>
                                                <td>
                                                    <?php echo $i; ?>
                                                </td>
                                                <td>
                                                    <?php echo $paymentList['name']; ?>
                                                </td>
                                                <td class="text-right">

                                                    <?php

                                                    echo Yii::app()->Controller->money_format_inr($invoiceSum, 2); ?>
                                                </td>
                                                <td class="text-right">
                                                    <?php echo Yii::app()->Controller->money_format_inr($totalPayment, 2); ?>
                                                </td>
                                                <td class="text-right">
                                                    <?php if (!filter_input(INPUT_GET, 'export')) { ?>
                                                        <a class="" href="<?php echo $this->createAbsoluteUrl(
                                                            'vendors/Paymentreport',
                                                            array(
                                                                "view_type" => 'vendor',
                                                                "vendor_id" => $paymentList["vendor_id"],
                                                                "company_id" => $company_id,
                                                                "date_from" => $date_from,
                                                                "date_to" => $date_to,
                                                                'reconcil_type' => $reconcil_type,
                                                                "summary" => isset($_GET['summary']) ? $_GET['summary'] : 0
                                                            )
                                                        ) ?>">
                                                            <?php echo Yii::app()->Controller->money_format_inr($balance, 2); ?>
                                                        </a>
                                                    <?php } else { ?>
                                                        <?php echo Yii::app()->Controller->money_format_inr($balance, 2); ?>
                                                    <?php } ?>
                                                </td>
                                            </tr>

                                            <?php
                                            $i = $i + 1;
                                        }

                                    }

                                }

                                ?>
                            </tbody>


                        </table>
                    </div>
                </div>
                <div class="row margin-top-10">
                    <div class="col-xs-12">
                        <div class="heading-title">Other Vendors</div>
                        <div class="dotted-line"></div>
                    </div>
                </div>
                <?php
                $datefrom = isset($_REQUEST['date_from']) ? $_REQUEST['date_from'] : "";
                $dateto = isset($_REQUEST['date_to']) ? $_REQUEST['date_to'] : "";
                $miscellaneouscount = 0;
                if (!empty($datefrom)) {
                    foreach ($miscellaneouspaymentData as $paymentList) {
                        if (!empty($paymentList["daybookpaid"]) || !empty($paymentList["vendorpaid"])) {
                            $miscellaneouscount++;
                        }
                    }
                }
                ?>

                <div class="table-wrapper margin-top-10">
                    <div class="d-flex">
                        <div class="summary text-right margin-left-auto">Total
                            <?php if (!empty($datefrom)) {
                                echo $miscellaneouscount;
                            } else {
                                echo count($miscellaneouspaymentData);
                            } ?> results.
                        </div>
                    </div>
                    <div id="table-wrapper">

                        <table class="table total-table">
                            <thead class="entry-table">
                                <tr>
                                    <th>Sl No.</th>
                                    <th>Vendor Name</th>
                                    <th class="text-right">Invoice</th>
                                    <th class="text-right">Payments</th>
                                    <th class="text-right">Balance</th>
                                </tr>
                                <tr class="grandtotal">
                                    <?php
                                    $datefrom = isset($_REQUEST['date_from']) ? $_REQUEST['date_from'] : "";
                                    $dateto = isset($_REQUEST['date_to']) ? $_REQUEST['date_to'] : "";
                                    if (!empty($datefrom)) {

                                        if (!empty($miscellaneouspaymentSum)) {
                                            $sumTotal = $miscellaneouspaymentSum["daybooksum"] + $miscellaneouspaymentSum["vendorsum"];
                                            $balanceSum = $miscellaneouspaymentSum["billsum"] - $sumTotal;
                                        }

                                    } else {
                                        $sumTotal = $miscellaneouspaymentSum["daybooksum"] + $miscellaneouspaymentSum["vendorsum"];
                                        $balanceSum = $miscellaneouspaymentSum["billsum"] - $sumTotal;
                                    } ?>
                                    <td colspan="2" class="text-right"><b>Total</b></td>
                                    <td class="text-right">
                                        <b>
                                            <?php echo Yii::app()->Controller->money_format_inr($miscellaneouspaymentSum["billsum"], 2); ?>
                                        </b>
                                    </td>
                                    <td class="text-right">
                                        <b>
                                            <?php echo Yii::app()->Controller->money_format_inr($sumTotal, 2); ?>
                                        </b>
                                    </td>
                                    <td class="text-right">
                                        <b>
                                            <?php echo Yii::app()->Controller->money_format_inr($balanceSum, 2); ?>
                                        </b>
                                    </td>




                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                $totalPayment = 0;
                                $balancePaid = 0;
                                if (!empty($datefrom)) {
                                    // echo "<pre>";print_r($miscellaneouspaymentData);exit;
                                    foreach ($miscellaneouspaymentData as $paymentLists) {
                                        if (!empty($paymentLists["daybookpaid"]) || !empty($paymentLists["vendorpaid"]))
                                            $totalPayment = $paymentLists["daybookpaid"] + $paymentLists["vendorpaid"];
                                        $balancePaid = $paymentLists["billtotal"] - $totalPayment;
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $i; ?>
                                            </td>
                                            <td>
                                                <?php echo $paymentLists['name']; ?>
                                            </td>
                                            <td class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr($paymentLists['billtotal'], 2); ?>
                                            </td>
                                            <td class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr($totalPayment, 2); ?>
                                            </td>
                                            <td class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr($balancePaid, 2); ?>
                                            </td>
                                        </tr>
                                        <?php
                                        $i = $i + 1;
                                    }
                                } else {
                                    foreach ($miscellaneouspaymentData as $paymentList) {
                                        $totalPayment = $paymentList["daybookpaid"] + $paymentList["vendorpaid"];
                                        $balancePaid = $paymentList["billtotal"] - $totalPayment;
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $i; ?>
                                            </td>
                                            <td>
                                                <?php echo $paymentList['name']; ?>
                                            </td>
                                            <td class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr($paymentList['billtotal'], 2); ?>
                                            </td>
                                            <td class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr($totalPayment, 2); ?>
                                            </td>
                                            <td class="text-right">
                                                <?php echo Yii::app()->Controller->money_format_inr($balancePaid, 2); ?>
                                            </td>
                                        </tr>
                                        <?php
                                        $i = $i + 1;
                                    }

                                }

                                ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        <?php } ?>

    </div>
</div>
<style>
    .ledger_table,
    .vendor_table {
        border: 1px solid #ddd;
    }

    .ledger_table thead tr th {
        border-bottom: 1px solid #ddd;
        border-right: 1px solid #ddd;
    }

    .ledger_table tbody tr td {
        border-right: 1px solid #ddd !important;
        border: none;
    }

    .ledger_table tfoot tr th {
        border-top: 1px solid #ddd;
        border-right: 1px solid #ddd;
    }

    .vendor_table thead tr th,
    .vendor_table tbody tr th,
    .vendor_table tbody tr td,
    .vendor_table tfoot tr th {
        border-right: 1px solid #ddd;
        border-bottom: 1px solid #ddd;
    }


    #parent2 {
        max-height: 500px;
    }


    .fixed {
        top: 70px;
        position: fixed;
        width: auto;
        display: none;
        border: none;
    }
</style>
<script>
    $(function () {
        // $("#date_from").datepicker({
        //     dateFormat: 'dd-mm-yy'
        // });
        // $("#date_to").datepicker({
        //     dateFormat: 'dd-mm-yy'
        // });
        $(".select_box").select2();
        $("#parent2").css('max-height', $(window).height() - $("#myNavbar").height() - $(".search-form").height() - (2 * $('footer').height()));

        $("#fix_table").tableHeadFixer({
            'head': true,
            'foot': true
        });
    });
    (function ($) {
        $.fn.fixMe = function () {
            return this.each(function () {
                var $this = $(this),
                    $t_fixed;

                function init() {
                    $this.wrap('<div class="container1" />');
                    $t_fixed = $this.clone();
                    $t_fixed.find("tbody").remove().end().addClass("fixed").insertBefore($this);
                    resizeFixed();
                }

                function resizeFixed() {
                    $t_fixed.find("th").each(function (index) {
                        $(this).css("width", $this.find("th").eq(index).outerWidth() + "px");
                    });
                }

                function scrollFixed() {
                    var offset = $(this).scrollTop(),
                        tableOffsetTop = $this.offset().top,
                        tableOffsetBottom = tableOffsetTop + $this.height() - $this.find("thead").height();
                    if (offset < tableOffsetTop || offset > tableOffsetBottom)
                        $t_fixed.hide();
                    else if (offset >= tableOffsetTop && offset <= tableOffsetBottom && $t_fixed.is(":hidden"))
                        $t_fixed.show();
                }
                $(window).resize(resizeFixed);
                $(window).scroll(scrollFixed);
                init();
            });
        };
    })(jQuery);

    $(document).ready(function () {
        $("#fixtable").fixMe();
        $(".up").click(function () {
            $('html, body').animate({
                scrollTop: 0
            }, 2000);
        });
        tot_1_sum = 0;
        tot_2_sum = 0;
        tot_3_sum = 0;
        tot_4_sum = 0;
        tot_5_sum = 0;
        $(".total_1").each(function () {
            var value = $(this).text();
            var val = value.replace(/,/ig, '');
            tot_1_sum += parseFloat(val, 10);
            var cash_sum_format = ReplaceNumberWithCommas(tot_1_sum);
            $('.total_top_1').text(cash_sum_format);
        });
        $(".total_2").each(function () {
            var value = $(this).text();
            var val = value.replace(/,/ig, '');
            tot_2_sum += parseFloat(val, 10);
            var cash_sum_format = ReplaceNumberWithCommas(tot_2_sum);
            $('.total_top_2').text(cash_sum_format);
        });
        $(".total_3").each(function () {
            var value = $(this).text();
            var val = value.replace(/,/ig, '');
            tot_3_sum += parseFloat(val, 10);
            var cash_sum_format = ReplaceNumberWithCommas(tot_3_sum);
            $('.total_top_3').text(cash_sum_format);
        });
        $(".total_4").each(function () {
            var value = $(this).text();
            var val = value.replace(/,/ig, '');
            tot_4_sum += parseFloat(val, 10);
            var cash_sum_format = ReplaceNumberWithCommas(tot_4_sum);
            $('.total_top_4').text(cash_sum_format);
        });
        $(".total_5").each(function () {
            var value = $(this).text();
            var val = value.replace(/,/ig, '');
            tot_5_sum += parseFloat(val, 10);
            var total_inv = $('.total_top_1').text()
            total_inv = total_inv.replace(/,/ig, '');
            var total_paid = $('.total_top_4').text()
            total_paid = total_paid.replace(/,/ig, '');
            var balance_sum = parseFloat(total_inv, 10) - parseFloat(total_paid, 10);
            var cash_sum_format = ReplaceNumberWithCommas(tot_5_sum);
            var cash_sum_format = ReplaceNumberWithCommas(balance_sum);
            balance_sum = parseFloat(balance_sum).toFixed(2)
            $('.total_top_5').text(balance_sum);
        });

    });

    function test(num) {
        return parseFloat(num).toFixed(2).toLocaleString();
    };

    function ReplaceNumberWithCommas(yourNumber) {
        yourNumber = parseFloat(yourNumber).toFixed(2)
        var n = yourNumber.toString().split(".");
        n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return n.join(".");
    }
    $(".unreconcil_btn").click(function () {
        $("input[name='reconcil_type']").val("unreconcil");
        $("form").submit();
    });
    $(".reconcil_btn").click(function () {
        $("input[name='reconcil_type']").val("reconcil");
        $("form").submit();
    });

    $(function () {
        $("input#summary").click(function () {
            $("form").submit();
        });
        $("input#show_zerobalance").click(function () {
            $("form").submit();
        });
    })
    $("#payment_report_aging_one").click(function () {
        var redirectUrl = $(this).attr("href");
        window.location.href = redirectUrl;
    });
    $("#payment_report_aging_two").click(function () {
        var redirecturl = $(this).attr("href");
        window.location.href = redirecturl;
    });
    $("#excel-btn").click(function () {
        var redirectUrl = $(this).attr("href");
        window.location.href = redirectUrl;
    });
    $("#pdf-btn").click(function () {
        var redirectUrl = $(this).attr("href");
        window.location.href = redirectUrl;
    });
</script>