<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs = array(
    'performa invoice',
)
    ?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<div class="container" id="project">
    <div class="sub-heading mb-10">
        <h3>Vendor Payment By User</h3>
    </div>
    <?php $this->renderPartial('_newdailyvendorssearch', array('model' => $model)) ?>
    <div class="" id="newlist">
        <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            //'itemView'=>'_view',
            'itemView' => '_newdailyvendorsview',
            'template' => '<div>{summary}{sorter}</div><div id="parent"><table class="table" id="fixTable">{items}</table></div>{pager}',
            'ajaxUpdate' => false,
        )); ?>
    </div>
</div>
<style>
    #parent {
        max-height: 60vh;
        border: 1px solid #ddd;
        border-top: 0px;
        border-right: 0px;
    }

    table thead th {
        background: #eee;
    }

    .table {
        margin-bottom: 0px;
    }
</style>
<script>
    $(document).ready(function () {
        $("#fixTable").tableHeadFixer({ 'head': true });
    });
    $(function () {
        $("#Dailyvendors_fromdate").datepicker({ dateFormat: 'dd-mm-yy' });
    });
    $(function () {
        $("#Dailyvendors_todate").datepicker({ dateFormat: 'dd-mm-yy' });
    });
    $("#clearForm").click(function () {
        window.location.href = "<?php echo Yii::app()->createAbsoluteUrl("vendors/vendorpaymentsbyuser") ?>";
    });
</script>