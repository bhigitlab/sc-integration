UPDATE `jp_specification` SET `spec_status`= '1' WHERE  `id` = 2486;
DELETE FROM `jp_specification` WHERE `spec_status` ="0";

ALTER TABLE `jp_billitem` ADD CONSTRAINT `fk_category_id` FOREIGN KEY (`category_id`) REFERENCES `jp_specification`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `jp_purchase_items` ADD CONSTRAINT `jp_purchase_itemse_category_id` FOREIGN KEY (`category_id`) REFERENCES `jp_specification`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

