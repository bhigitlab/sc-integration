<table class="table table-bordered" id="fixtable">
    <?php if (!empty($result_data)) { ?>
        <thead>
            <tr>
                <td><b>Warehouse</b></td>
                <td><b>Image</b></td>
                <td><b>Item</b></td>
                <td><b>Dimension</b></td>
                <td><b>Batch</b></td>
                <td><b>Type</b></td>
                <td><b>Unit</b></td>
                <td><b>Quantity</b></td>
                <td><b>Rate</b></td>
                <td><b>Amount</b></td>
                <td><b>Date</b></td>
            </tr>
        </thead>
        <tbody id="fixtable_body">
            <?php
            $total_qty = 0;
            $total_amount = 0;
            $i = 0;


            $despatch_sum = 0;
            $receipt_sum = 0;

            foreach ($result_data as $data) {
                $row = 1;
                $qty_sum = 0;
                $amount_sum = 0;
                if (!empty($data)) {
                    $count = count($data);
                    if ($count == 1) {
                        $rowspan = 1;
                    } else {
                        $rowspan = $count;
                    }

                    foreach ($data as $dat) {
                        $total_qty += $dat['qty'];
                        $total_amount += $dat['amount'];
                        $qty_sum += $dat['qty'];
                        $amount_sum += $dat['amount'];
                        if ($data[0]['type'] == 'despatch') {
                            $despatch_sum += $dat['qty'];
                        }
                        if ($data[0]['type'] == 'receipt') {
                            $receipt_sum += $dat['qty'];
                        }
            ?>
                        <tr>
                            <td><?= $dat['warehouse'] ?></td>
                            <td class="imd_div2" id="<?php echo $dat['item_id']; ?>">
                                <?php
                                if ($dat['item_img'] != '') {
                                    $path = realpath(Yii::app()->basePath . '/../uploads/purchase_category/' . $dat['item_img']);
                                    if (file_exists($path)) {
                                ?>
                                        <img class="img1 img_border pop" id="img1_<?php echo $i ?>_<?php echo $dat['item_id']; ?>" src="<?php echo Yii::app()->request->baseUrl . "/uploads/purchase_category/thumbnail/" . $dat['item_img'] ?>" alt='' style='' data-toggle="popover-hover" data-img='<?php echo Yii::app()->request->baseUrl . "/uploads/purchase_category/" . $dat['item_img']; ?>' />
                                    <?php   } else { ?>
                                        <img class="img1" id="img1_<?php echo $i; ?>_<?php echo $dat['item_id']; ?>" src="<?php echo Yii::app()->request->baseUrl . "/uploads/purchase_category/thumbnail/no_img.png" ?>" alt='' style='cursor:pointer;height:60px;width:60px;' />
                                    <?php } ?>
                                <?php } else { ?>
                                    <img class="img1" id="img1_<?php echo $i; ?>_<?php echo $dat['item_id']; ?>" src="<?php echo Yii::app()->request->baseUrl . "/uploads/purchase_category/thumbnail/no_img.png" ?>" alt='' style='cursor:pointer;height:60px;width:60px;' />
                                <?php } ?>
                            </td>
                            <td><?= $dat['item'] ?></td>
                            <td><?= ($dat['dimension'] != '') ? "(" . $dat['dimension'] . ")" : "N/A" ?></td>
                            <td><?= $dat['batch'] ?></td>
                            <td><?= $dat['type'] ?></td>
                            <td><?= $dat['unit'] ?></td>
                            <td><?= round($dat['qty'], 2) ?></td>
                            <td><?= Controller::money_format_inr($dat['rate'], 2, 1) ?></td>
                            <td><?= Controller::money_format_inr($dat['amount'], 2, 1) ?></td>
                            <td><?= !empty($dat['date']) ? date('d-M-Y', strtotime($dat['date'])) : '' ?></td>
                        </tr>
                    <?php
                        $row++;
                    }
                    ?>
                    <tr>
                        <td colspan="4"> <i class="text-danger">Rate adjusted to 2 decimal points. Amount might vary from actual amount</i></td>
                        <td align="right"><b>Total</b></td>
                        <td></td>
                        <td><b><?= $dat['unit'] ?></b></td>
                        <td><b><?= round($qty_sum, 2) ?></b></td>
                        <td></td>
                        <td><b><?= Controller::money_format_inr($amount_sum, 2, 1) ?></b>
                        </td>
                        <td></td>
                    </tr>
            <?php
                }
            }
            ?>

            <tr class='notfound' id="notfound">
                <td colspan='10'>No record found</td>
            </tr>
            <?php $totalqty_stock = $receipt_sum - $despatch_sum; ?>
        </tbody>
        <tfoot id="grand_total_value">
            <td colspan="5" align="right"><b>Total<b></td>
            <td colspan="2">&nbsp;</td>
            <td><b><?php if ($totalqty_stock > 0) {
                        echo $totalqty_stock;
                    } ?> </b></td>
            <td>&nbsp;</td>
            <td><b><?= Controller::money_format_inr($total_amount, 2, 1) ?></b></td>
            <td></td>
        </tfoot>
    <?php } else { ?>
        <span class="empty">No results found</span>
    <?php } ?>
</table>
<script>
    $(document).ready(function() {
        $("#fixtable").tableHeadFixer({
            'left': false,
            'foot': true,
            'head': true
        });
        $('[data-toggle="popover-hover"]').popover({
            html: true,
            container: "body",
            trigger: 'hover',
            placement: 'bottom',
            content: function() {

                return '<img src="' + $(this).data('img') + '" style="height:auto;"/>';
            }
        });
    });
</script>