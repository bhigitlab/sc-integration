<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>

<?php
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery1 = "";
$newQuery = "";
foreach ($arrVal as $arr) {
    if ($newQuery1)
        $newQuery1 .= ' OR';
    if ($newQuery)
        $newQuery .= ' OR';
    $newQuery1 .= " FIND_IN_SET('" . $arr . "', company_id)";
    $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
}
?>
<style type="text/css">
    .pending_reconil {
        background-color: #5bc0de3b;
        border: 1px solid #5bc0de3b;
        width: 12px;
        height: 12px;
        margin-right: 10px;
    }

    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .btn.addentries:focus,
    .btn.addentries:hover {
        border: 1px solid transparent;
        box-shadow: none;
    }

    .deleteclass {
        background-color: #efca92;
    }

    .filter_elem.links_hold {
        margin-top: 6px;
    }
</style>
<div class="container" id="expense">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="expenses-heading">
        <div class="clearfix">
            <?php if ((isset(Yii::app()->user->role) && (in_array('/vendors/paymentcreate', Yii::app()->user->menuauthlist)))) { ?>
                <button type="button" class="btn btn-info pull-right mt-0 mb-10 addentries collapsed" data-toggle="collapse"
                    data-target="#dailyform" id="dailyformbutton"></button>

            <?php } ?>
            <h3>Vendor Payments<span id="payment_id"></span></h3>
            <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
        </div>
    </div>
    <div>
        <div id="errormessage"></div>
    </div>
    <!-- <div class="daybook form"> -->
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'dailyexpense-form',
        'enableAjaxValidation' => false,
    ));
    ?>
    <div class="clearfix">
        <div class="datepicker">
            <div class="page_filter clearfix filter-wrapper">
                <div class="filter_elem">
                    <label>Entry Date:</label>
                    <?php echo CHtml::activeTextField($model, 'date', array('class' => 'form-control', 'autocomplete' => 'off', 'id' => 'Dailyvendors_date', 'value' => date('d-m-Y'), 'onchange' => 'changedate()', 'style' => 'height: 25px; size: 10;')); ?>
                </div>
                <div class="filter_elem links_hold">
                    <a href="#" id="previous" class="link">Previous</a> |
                    <a href="#" id="current" class="link">Current</a> |
                    <a href="#" id="next" class="link">Next</a>
                </div>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <?php
                $user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);

                $comapny_condition = "";
                foreach ($arrVal as $arr) {
                    if ($comapny_condition)
                        $comapny_condition .= ' OR';
                    $comapny_condition .= " FIND_IN_SET('" . $arr . "', d.company_id)";
                }

                $tblpx = Yii::app()->db->tablePrefix;
                $expenseData = Yii::app()->db->createCommand("select MAX(date) as entry_date FROM " . $tblpx . "dailyvendors d WHERE  $comapny_condition")->queryRow();

                ?>
                <div class="ml-auto last-entry">
                    <span id="lastentry">Last Entry date : <span id="entrydate">
                            <?php echo !is_null($expenseData["entry_date"]) ? date("d-m-Y", strtotime($expenseData["entry_date"])) : ""; ?>
                        </span></span>
                </div>
            </div>
        </div>
    </div>


    <div class="entries-wrapper daybook collapse" id="dailyform">
        <div>
            <!-- <div class="vendorpayment"> -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="heading-title">Vendor Payment Details</div>
                    <div class="dotted-line"></div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-xs-12 col-md-3">

                    <label for="vendor">Company</label>

                    <?php
                    echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array(
                        'select' => array('id, name'),
                        'order' => 'name ASC',
                        'condition' => '(' . $newQuery . ')',
                        'distinct' => true,
                    )), 'id', 'name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Company-', 'style' => 'width:100%'));
                    ?>


                </div>

                <div class="form-group col-xs-12 col-md-3">

                    <label for="vendor">Project</label>

                    <?php
                    echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll(array(
                        'select' => array('pid, name'),
                        'order' => 'name ASC',
                        'condition' => '(' . $newQuery1 . ')',
                        'distinct' => true,
                    )), 'pid', 'name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Project-', 'style' => 'width:100%'));
                    ?>


                </div>

                <div class="form-group col-xs-12 col-md-3">

                    <label for="vendor">Vendor</label>

                    <?php
                    echo $form->dropDownList($model, 'vendor_id', CHtml::listData(Vendors::model()->findAll(array(
                        'select' => array('vendor_id, name'),
                        'order' => 'name ASC',
                        'condition' => '(' . $newQuery1 . ')',
                        'distinct' => true,
                    )), 'vendor_id', 'name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Vendor-', 'style' => 'width:100%'));
                    ?>


                </div>
                <div class="form-group col-xs-12 col-md-3">

                    <label for="purchase">Purchase Number</label>

                    <?php

                    $sql = "SELECT * FROM jp_purchase p LEFT JOIN jp_bills b "
                        . " ON p.p_id=b.purchase_id "
                        . " WHERE p.p_id NOT IN(SELECT DISTINCT purchase_id "
                        . " FROM jp_bills) AND p.purchase_status='saved'";
                    $po_array = Yii::app()->db->createCommand($sql)->queryAll();
                    echo $form->dropDownList(
                        $model,
                        'purchase_id',
                        CHtml::listData($po_array, 'p_id', 'purchase_no'),
                        array(
                            'class' => 'form-control js-example-basic-single',
                            'empty' => '-Select Purchase-',
                            'style' => 'width:100%'
                        )
                    );
                    ?>


                </div>
                <div class="form-group col-xs-12 col-md-3">

                    <label for="receiptType">Transaction Type</label>

                    <?php
                    echo $form->dropDownList($model, 'payment_type', CHtml::listData(Status::model()->findAll(array(
                        'select' => array('sid, caption'),
                        "condition" => 'status_type = "payment_type" AND sid NOT IN(103,107)',
                        'order' => 'sid',
                        'distinct' => true,
                    )), 'sid', 'caption'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Payment Type-', 'style' => 'width:100%'));
                    ?>


                </div>
                <div class="form-group col-xs-12 col-md-3">

                    <label for="bank">Bank</label>

                    <?php
                    echo $form->dropDownList($model, 'bank', CHtml::listData(Bank::model()->findAll(array(
                        'select' => array('bank_id, bank_name'),
                        'order' => 'bank_name ASC',
                        'condition' => '(' . $newQuery1 . ')',
                        'distinct' => true,
                    )), 'bank_id', 'bank_name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Bank-', 'style' => 'width:100%'));
                    ?>


                </div>
                <div class="form-group col-xs-12 col-md-3">

                    <label for="cheque">Cheque No/ Transaction ID</label>

                    <input type="text" class="form-control" id="Dailyvendors_cheque_no"
                        name="Dailyvendors[cheque_no]" />

                    <div class="chq_error text-danger"></div>

                </div>
            </div>
            <!-- </div> -->
            <!-- <div class="amountdetails"> -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="heading-title">Amount Details</div>
                    <div class="dotted-line"></div>
                </div>
            </div>
            <div class="row d-flex daybook-inner">

                <div class="col-xs-12 col-md-9">
                    <div class="row child-row">
                        <div class="form-group col-xs-12 col-md-2">

                            <label for="cheque">Amount</label>

                            <input type="text" class="form-control amountvalidation" id="Dailyvendors_amount"
                                name="Dailyvendors[amount]" />


                        </div>
                        <div class="form-group col-md-2  gsts">

                            <label for="cheque">SGST %</label>
                            <div class="input-info">
                                <input type="text" class="form-control amountvalidation percentage"
                                    id="Dailyvendors_sgst" name="Dailyvendors[sgst]" />
                                <span class="gstvalue value-label" id="txtSgst1"></span>
                            </div>

                        </div>

                        <div class="form-group col-md-2 gsts">

                            <label for="cheque">CGST %</label>
                            <div class="input-info">
                                <input type="text" class="form-control amountvalidation percentage"
                                    id="Dailyvendors_cgst" name="Dailyvendors[cgst]" />
                                <span class="gstvalue value-label" id="txtCgst1"></span>
                            </div>

                        </div>
                        <div class="form-group col-md-2 gsts">

                            <label for="cheque">IGST %</label>
                            <div class="input-info">
                                <input type="text" class="form-control amountvalidation percentage"
                                    id="Dailyvendors_igst" name="Dailyvendors[igst]" />
                                <span class="gstvalue value-label" id="txtIgst1"></span>
                            </div>

                        </div>
                        <div class="form-group col-md-2 gsts">

                            <label for="tds">TDS %</label>
                            <div class="input-info">
                                <input type="text" class="form-control amountvalidation percentage"
                                    id="Dailyvendors_tds" name="Dailyvendors[tds]" />
                                <span class="gstvalue value-label" id="txtTds1"></span>
                            </div>

                        </div>
                        <div class="form-group col-md-2 amntblock">

                            <label for="cheque"></label>
                            <div>
                                <input type="hidden" class="form-control amountvalidation" id="Dailyvendors_sgst_amount"
                                    name="Dailyvendors[sgst_amount]" readonly="true" />
                            </div>

                        </div>
                        <div class="form-group col-md-2 amntblock">

                            <label for="cheque"></label>
                            <div>
                                <input type="hidden" class="form-control amountvalidation" id="Dailyvendors_cgst_amount"
                                    name="Dailyvendors[cgst_amount]" readonly="true" />
                            </div>

                        </div>





                        <div class="form-group col-md-2 amntblock">

                            <label for="cheque"></label>
                            <div>
                                <input type="hidden" class="form-control amountvalidation" id="Dailyvendors_igst_amount"
                                    name="Dailyvendors[igst_amount]" readonly="true" />
                            </div>

                        </div>



                        <div class="form-group col-md-2 amntblock">

                            <label for="tds"></label>
                            <div>
                                <input type="hidden" class="form-control amountvalidation" id="Dailyvendors_tds_amount"
                                    name="Dailyvendors[tds_amount]" />
                            </div>

                        </div>



                    </div>

                </div>


                <div class="col-xs-12 col-md-3 bottom-flex">
                    <div class="form-group pull-right total-value-wrapper margin-top-18 totalamnt">


                        <label for="cheque"></label>
                        <div class="input-info margin-bottom-10 " id="txtgstTotal">




                        </div>
                        <input type="hidden" class="form-control amountvalidation" id="Dailyvendors_tax_amount"
                            name="Dailyvendors[tax_amount]" readonly="true" />
                        <div class="input-info margin-bottom-10" id="txtTotal1">


                        </div>
                        <div class="input-info" id="txtTdsPaid">


                        </div>
                        <input type="hidden" class="form-control amountvalidation" id="Dailyvendors_paidamount"
                            name="Dailyvendors[paidamount]" readonly="true" />
                    </div>

                </div>







            </div>
            <!-- </div> -->
            <!-- <div class="otherdetails"> -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="heading-title">Other Details</div>
                    <div class="dotted-line"></div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-xs-12 col-md-6">

                    <label for="description">Description</label>
                    <textarea type="text" class="form-control" rows="1" id="Dailyvendors_description"
                        name="Dailyvendors[description]"></textarea>

                </div>
            </div>
            <!-- </div> -->
            <div class="row">
                <input type="hidden" name="Dailyvendors[txtDailyVendorId]" value="" id="txtDailyVendorId" />
                <div class="form-group col-xs-12 text-right submit-button">

                    <label style="display:block;">&nbsp;</label>
                    <button type="button" class="btn btn-primary" id="buttonsubmit">ADD</button>
                    <button type="button" class="btn btn-default" id="btnReset">RESET</button>

                </div>
            </div>
        </div>

    </div>

    <?php $this->endWidget(); ?>
    <!-- </div> -->
    <div class="daybook table-wrapper margin-top-25" id="daybook-entry">
        <?php if (!empty($newmodel)) { ?>
            <ul class="legend margin-bottom-0">
                <li><span class="unreconciled"></span> UnReconciled</li>
            </ul>
        <?php } ?>
        <!-- <div class="pending_reconil"></div> UnReconciled  -->
        <div class="summary text-center">
            <?php $this->renderPartial('dailyentries_list', array('newmodel' => $newmodel, )); ?>
        </div>

    </div>
</div>
<?php $vendorUrl = Yii::app()->createAbsoluteUrl("dailyexpense/dynamicVendor"); ?>
<?php $getUrl = Yii::app()->createAbsoluteUrl("vendors/getDataByDate"); ?>
<?php $getDailyvendor = Yii::app()->createAbsoluteUrl("vendors/getDailyVendorDetails"); ?>
<?php $getproject = Yii::app()->createAbsoluteUrl("vendors/dynamicproject"); ?>
<?php $getpo = Yii::app()->createAbsoluteUrl("vendors/dynamicpurchase"); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-dateFormat/1.0/jquery.dateFormat.js"></script>
<script>

    $("#Dailyvendors_vendor_id").change(function () {
        var comId = $("#Dailyvendors_company_id").val();
        var projectId = $("#Dailyvendors_project_id").val();
        var vendorId = $("#Dailyvendors_vendor_id").val();
        getPurchaseList(comId, projectId, vendorId, "");
    });

    function getPurchaseList(comId, projectId, vendorId, purchaseId) {
        $.ajax({
            url: "<?php echo $getpo; ?>",
            data: {
                "comId": comId,
                "projectId": projectId,
                "vendorId": vendorId,
                "purchaseId": purchaseId
            },
            type: "POST",
            dataType: 'json',
            success: function (data) {
                console.log(data);
                $("#Dailyvendors_purchase_id").html(data.purchase);
            }
        });
    }

    function getCompanyList(comId, opt, pro, bnk) {
        $('#loading').show();
        $.ajax({
            url: "<?php echo $getproject; ?>",
            data: {
                "comId": comId
            },
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $("#Dailyvendors_project_id").html(data.project);
                $("#Dailyvendors_bank").html(data.bank);
                if (opt != 0)
                    $("#Dailyvendors_project_id").val(pro).trigger('change.select2');
            }
        });
    }

    $("#Dailyvendors_company_id").change(function () {
        var comId = $("#Dailyvendors_company_id").val();
        var comId = comId ? comId : 0;
        getCompanyList(comId, 0, 0, 0);
    });
    $(function () {
        $("#Dailyvendors_date").datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: new Date()
        });
    });

    $(".js-example-basic-single").select2();

    $('#Dailyvendors_vendor_id').change(function () {
        if (($(this).val()) != '') {
            $('#loading').show();
            $.ajax({
                method: "GET",
                data: {
                    invoice_id: 'test'
                },
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('vendors/ajaxcall'); ?>',
                success: function (result) {
                    $("#Dailyvendors_payment_type").select2("focus");
                }
            });
        } else {
            $.ajax({
                method: "GET",
                data: {
                    invoice_id: 'test'
                },
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('vendors/ajaxcall'); ?>',
                success: function (result) {
                    $("#Dailyvendors_vendor_id").select2('focus');
                }
            });

        }
    })

    $('#Dailyvendors_company_id').change(function () {
        $('#loading').show();
        $.ajax({
            method: "GET",
            dataType: "json",
            url: '<?php echo Yii::app()->createUrl('vendors/ajaxcall'); ?>',
            success: function (result) {
                $("#Dailyvendors_project_id").select2('focus');
            }
        });
    });

    $('#Dailyvendors_project_id').change(function () {
        $('#loading').show();
        $.ajax({
            method: "GET",
            data: {
                invoice_id: 'test'
            },
            dataType: "json",
            url: '<?php echo Yii::app()->createUrl('vendors/ajaxcall'); ?>',
            success: function (result) {
                $('#Dailyvendors_vendor_id').focus();
            }
        });
    });

    $('#Dailyvendors_vendor_id').click(function () {
        $('#loading').show();
        $.ajax({
            method: "GET",
            data: {
                invoice_id: 'test'
            },
            dataType: "json",
            url: '<?php echo Yii::app()->createUrl('vendors/ajaxcall'); ?>',
            success: function (result) {
                $("#Dailyvendors_vendor_id").select2("focus");
            }
        });
    })

    $('#Dailyvendors_payment_type').change(function () {
        if (($(this).val()) != '') {
            var val = $(this).val();
            if (val == 89) {
                $('#loading').show();
                $.ajax({
                    method: "GET",
                    data: {
                        invoice_id: 'test'
                    },
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('vendors/ajaxcall'); ?>',
                    success: function (result) {
                        $('#Dailyvendors_amount').focus();
                    }
                });
            } else if (val == 88) {
                $('#loading').show();
                $.ajax({
                    method: "GET",
                    data: {
                        invoice_id: 'test'
                    },
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('vendors/ajaxcall'); ?>',
                    success: function (result) {
                        $("#Dailyvendors_bank").select2("focus");
                    }
                });
            }
        } else {
            $('#loading').show();
            $.ajax({
                method: "GET",
                data: {
                    invoice_id: 'test'
                },
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('vendors/ajaxcall'); ?>',
                success: function (result) {
                    $("#Dailyvendors_payment_type").select2("focus");
                }
            });
        }

    })


    $('#Dailyvendors_bank').change(function () {
        if (($(this).val()) != '') {
            $('#loading').show();
            $.ajax({
                method: "GET",
                data: {
                    invoice_id: 'test'
                },
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('vendors/ajaxcall'); ?>',
                success: function (result) {
                    $('#Dailyvendors_cheque_no').focus();
                }
            });
        } else {
            $("#Dailyvendors_bank").select2('focus');
        }
    });


    $(document).ready(function () {
        $('#loading').hide();
        $('#dailyform').on('shown.bs.collapse', function () {
            $('select').first().focus();
        });
    });
</script>
<script type="text/javascript">
    function getFullData(newDate) {
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            url: "<?php echo $getUrl; ?>",
            data: {
                date: newDate
            },
            type: "POST",
            success: function (data) {
                $("#newlist").html(data);
                //alert(data);
            }
        });
    }

    function changedate() {
        var cDate = $("#Dailyvendors_date").val();
        getFullData(cDate);
        validateDateSelection(cDate);
    }

    function getVendorList(expId, expvendor) {
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            url: "<?php echo $vendorUrl; ?>",
            data: {
                "expid": expId
            },
            type: "POST",
            success: function (data) {
                $("#Dailyexpense_vendor_id").html(data);
                if (expvendor != 0)
                    $("#Dailyexpense_vendor_id").val(expvendor);
            }
        });
    }




    $(document).ready(function () {
        $("#Dailyvendors_bank").attr("disabled", true);
        $("#Dailyvendors_cheque_no").attr("readonly", true);
        $("a#next").css("pointer-events", "none").css("color", "#ccccc");
        $("#Dailyvendors_vendor_id").focus();
        $("#buttonsubmit").click(function () {
            $("#buttonsubmit").attr('disabled', true);
            var crDate = $("#Dailyvendors_date").val();
            var vendor_id = $("#Dailyvendors_vendor_id").val();
            var amount = parseFloat($("#Dailyvendors_amount").val());
            var description = $("#Dailyvendors_description").val();
            var payment_type = $("#Dailyvendors_payment_type").val();
            var bank = $("#Dailyvendors_bank").val();
            var cheque_no = $("#Dailyvendors_cheque_no").val();
            var company = $("#Dailyvendors_company_id").val();
            if (vendor_id == "" || amount == "" || description == "" || payment_type == "" || company == "") {
                $("#errormessage").show()
                    .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                    .fadeOut(5000);
                $("#buttonsubmit").attr('disabled', false);
                return false;
            }

            if (payment_type == 88) {
                if (bank == "" || cheque_no == "") {
                    $("#errormessage").show()
                        .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                        .fadeOut(5000);
                    $("#buttonsubmit").attr('disabled', false);
                    return false;
                }
            }

            var dayBookId = $("#txtDailyVendorId").val();
            var actionUrl;
            if (dayBookId == "") {
                actionUrl = "<?php echo Yii::app()->createAbsoluteUrl("vendors/addDailyentries"); ?>";
                localStorage.setItem("action", "add");
            } else {
                actionUrl = "<?php echo Yii::app()->createAbsoluteUrl("vendors/updateDailyentries"); ?>";
                localStorage.setItem("action", "update");
            }
            $('.loading-overlay').addClass('is-active');
            var data = $("#dailyexpense-form").serialize();
            $.ajax({
                type: 'GET',
                url: actionUrl,
                data: data,
                success: function (data) {

                    if (data.status == 1) {
                        var message = data.error_message;
                    } else if (data.status == 5) {
                        var message = data.error_message;
                    }
                    if (data == 1 || data == 5) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-warning"><strong>Failed !</strong> ' + message + '</div>')
                            .fadeOut(10000);
                        $("#buttonsubmit").attr('disabled', false);
                        return false;
                    } else {
                        var currAction = localStorage.getItem("action");

                        if (currAction == "add") {
                            var last_inserted_id = data;
                            $("#errormessage").show()
                                .html('<div class="alert alert-success"><strong>Successfully !</strong> record added.ID is ' + last_inserted_id + '</div>')
                                .fadeOut(7000);
                            $("#dailyform").removeClass("in");  // Close the collapsed section
                            $(".addentries").addClass("collapsed");
                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth() + 1; //January is 0!
                            var yyyy = today.getFullYear();
                            if (dd < 10)
                                dd = '0' + dd
                            if (mm < 10)
                                mm = '0' + mm
                            today = dd + '-' + mm + '-' + yyyy;
                            $.ajax({
                                type: "POST",
                                data: {
                                    date: crDate
                                },
                                url: "<?php echo Yii::app()->createUrl("vendors/getAllData") ?>",
                                success: function (response) {
                                    document.getElementById("dailyexpense-form").reset();
                                    $("#Dailyvendors_date").val(crDate);
                                    $("#newlist").html(response);
                                    $('#Dailyvendors_company_id').select2('focus');
                                    $("#buttonsubmit").attr('disabled', false);
                                    $('#Dailyvendors_vendor_id').val('').trigger('change.select2');
                                    $('#Dailyvendors_purchase_id').val('').trigger('change.select2');
                                    $('#Dailyvendors_payment_type').val('').trigger('change.select2');
                                    $('#Dailyvendors_bank').val('').trigger('change.select2');
                                    $('#Dailyvendors_project_id').val('').trigger('change.select2');
                                    $('#Dailyvendors_company_id').val('').trigger('change.select2');
                                    $("#txtSgst1").text("");
                                    $("#txtCgst1").text("");
                                    $("#txtIgst1").text("");
                                    $("#txtgstTotal").text("");
                                    $("#txtTotal1").text("");
                                    $("#txtTds1").text("");
                                    $("#Dailyvendors_tds_amount").val("");
                                    $("#txtTdsPaid").html("");
                                    $("#Dailyvendors_paidamount").html("");
                                    $('#txtDailyVendorId').val("");
                                }
                            });
                            $("#entrydate").text(today);
                        } else if (currAction == "update") {
                            if (data == 2) {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-success"><strong>Successfully !</strong> Update request send succesfully. Please wait for approval</div>')
                                    .fadeOut(10000);
                                $('.loading-overlay').addClass('is-active');
                                $.ajax({
                                    type: "POST",
                                    data: {
                                        date: crDate
                                    },
                                    url: "<?php echo Yii::app()->createUrl("vendors/getAllData") ?>",
                                    success: function (response) {
                                        document.getElementById("dailyexpense-form").reset();
                                        $("#Dailyvendors_date").val(crDate);
                                        $("#newlist").html(response);
                                        $('#Dailyvendors_company_id').select2('focus');
                                        $("#buttonsubmit").attr('disabled', false);
                                        $('#Dailyvendors_vendor_id').val('').trigger('change.select2');
                                        $('#Dailyvendors_payment_type').val('').trigger('change.select2');
                                        $('#Dailyvendors_bank').val('').trigger('change.select2');
                                        $('#Dailyvendors_project_id').val('').trigger('change.select2');
                                        $('#Dailyvendors_company_id').val('').trigger('change.select2');
                                        $("#txtSgst1").text("");
                                        $("#txtCgst1").text("");
                                        $("#txtIgst1").text("");
                                        $("#txtgstTotal").text("");
                                        $("#txtTotal1").text("");
                                        $("#txtTds1").text("");
                                        $("#Dailyvendors_tds_amount").val("");
                                        $("#txtTdsPaid").html("");
                                        $("#Dailyvendors_paidamount").html("");
                                        $('#txtDailyVendorId').val("");
                                    }
                                });
                            } else if (data == 3) {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-danger"><strong>Failed !</strong> Cannot send update request.</div>')
                                    .fadeOut(10000);
                            } else if (data == 4) {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-warning"><strong>Failed !</strong> Cannot send update request. Already have a request for this.</div>')
                                    .fadeOut(10000);
                            } else {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-success"><strong>Successfully !</strong> record updated.</div>')
                                    .fadeOut(5000);

                                $(".daybook .collapse").removeClass("in");  // Close the collapsed section
                                $(".addentries").addClass("collapsed");
                                var last_inserted_id = data;
                                $("#errormessage").show()
                                    .html('<div class="alert alert-success"><strong>Successfully !</strong> record updated</div>')
                                    .fadeOut(7000);
                                var today = new Date();
                                var dd = today.getDate();
                                var mm = today.getMonth() + 1; //January is 0!
                                var yyyy = today.getFullYear();
                                if (dd < 10)
                                    dd = '0' + dd
                                if (mm < 10)
                                    mm = '0' + mm
                                today = dd + '-' + mm + '-' + yyyy;
                                $.ajax({
                                    type: "POST",
                                    data: {
                                        date: crDate
                                    },
                                    url: "<?php echo Yii::app()->createUrl("vendors/getAllData") ?>",
                                    success: function (response) {
                                        document.getElementById("dailyexpense-form").reset();
                                        $("#Dailyvendors_date").val(crDate);
                                        $("#newlist").html(response);
                                        $('#Dailyvendors_company_id').select2('focus');
                                        $("#buttonsubmit").attr('disabled', false);
                                        $('#Dailyvendors_vendor_id').val('').trigger('change.select2');
                                        $('#Dailyvendors_purchase_id').val('').trigger('change.select2');
                                        $('#Dailyvendors_payment_type').val('').trigger('change.select2');
                                        $('#Dailyvendors_bank').val('').trigger('change.select2');
                                        $('#Dailyvendors_project_id').val('').trigger('change.select2');
                                        $('#Dailyvendors_company_id').val('').trigger('change.select2');
                                        $("#txtSgst1").text("");
                                        $("#txtCgst1").text("");
                                        $("#txtIgst1").text("");
                                        $("#txtgstTotal").text("");
                                        $("#txtTotal1").text("");
                                        $("#txtTds1").text("");
                                        $("#Dailyvendors_tds_amount").val("");
                                        $("#txtTdsPaid").html("");
                                        $("#Dailyvendors_paidamount").html("");
                                        $('#txtDailyVendorId').val("");
                                    }
                                });
                                $("#entrydate").text(today);
                                $('#payment_id').text('');
                            }
                        }
                        document.getElementById("dailyexpense-form").reset();
                        $("#Dailyvendors_date").val(crDate);
                        if (data == 2 || data == 3 || data == 4) {

                        } else {
                            $("#newlist").html(data);
                        }
                        $('#Dailyvendors_company_id').select2('focus');
                        $("#buttonsubmit").attr('disabled', false);
                        $('#Dailyvendors_vendor_id').val('').trigger('change.select2');
                        $('#Dailyvendors_payment_type').val('').trigger('change.select2');
                        $('#Dailyvendors_bank').val('').trigger('change.select2');
                        $('#Dailyvendors_project_id').val('').trigger('change.select2');
                        $('#Dailyvendors_company_id').val('').trigger('change.select2');
                        $("#txtSgst1").text("");
                        $("#txtCgst1").text("");
                        $("#txtIgst1").text("");
                        $("#txtgstTotal").text("");
                        $("#txtTotal1").text("");
                        $("#txtTds1").text("");
                        $("#Dailyvendors_tds_amount").val("");
                        $("#txtTdsPaid").html("");
                        $("#Dailyvendors_paidamount").html("");
                    }
                    $('#txtDailyVendorId').val("");
                },

                error: function (data) { }
            });

            $('#Dailyvendors_project_id').select2('focus');
        });
        $("#txtPaid").blur(function () {
            var totalamount = parseFloat($("#txtTotal").val());
            var purchasetype = $("#txtPurchaseType").val();
            var paid = parseFloat($("#txtPaid").val());
            if (purchasetype == 3) {
                if (paid >= totalamount) {
                    $("#errormessage").show()
                        .html('<div class="alert alert-warning">You have selected the purchase type as Partially Paid.<br/>The paid amount must be less than the total amount.</div>')
                        .fadeOut(10000);
                    return false;
                }
            }
        });
        $("body").on("click", ".row-daybook", function (e) {
            var reconStatus = $(this).parents("td").attr("data-status");
            if (reconStatus == 1) {
                alert("Can't update !Reconciled Entry");
                return;
            }
            $("#dailyform").addClass("in");
            $(".addentries").removeClass("collapsed");
            if ($("daybook .collapse").css('display') == 'visible') {
                $("daybook .collapse").css({
                    "height": "auto"
                });
            } else {
                $(".collapse").css({
                    "height": ""
                });
            }
            $(".popover").removeClass("in");

            var rowId = $(this).attr("id");
            // var vendId = $("#dailyvendorid" + rowId).val();

            var expenseId = $(this).parents("tr").attr('data-id');
            var vendId = $(this).parents("tr").attr('id');
            var vendStatus = $(this).parents("tr").attr('data-status');

            $("#buttonsubmit").text("UPDATE");
            $("#txtDailyVendorId").val(expenseId);
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                url: "<?php echo $getDailyvendor; ?>",
                data: {
                    "dailyvendorid": vendId,
                    "vendStatus": vendStatus
                },
                type: "GET",
                success: function (data) {
                    $('.loading-overlay').removeClass('is-active');
                    var result = JSON.parse(data);
                    console.log(result);
                    $(".gstvalue").css({
                        "display": "inline-block"
                    });
                    $('#payment_id').text('#' + result['daily_v_id']);
                    $("#Dailyvendors_company_id").select2('focus');
                    var bnk = (result["bank"] != '') ? result["bank"] : 0;
                    getCompanyList(result["company"], 1, result["project_id"], bnk);
                    $("#Dailyvendors_tds").val(result["tds"]);
                    $("#txtTds1").text(result["tds_amount"]);

                    $("#Dailyvendors_tds_amount").val(result["tds_amount"]);
                    $("#Dailyvendors_tds_amount").val(result["tds_amount"]);

                    $("#txtTdsPaid").html('<label class="final-section-label w-100p">Amount To Be Paid :</label><span class="total-value-label w-100p">' + result["paidamount"] + '');
                    $("#Dailyvendors_paidamount").val(result["paidamount"]);
                    if (!result["vendor_id"]) {
                        $('#Dailyvendors_vendor_id').val(result["vendor_id"]).trigger('change.select2');
                        $("#Dailyvendors_description").val("");
                        $('#Dailyvendors_project_id').val(result["project_id"]).trigger('change.select2');
                        $('#Dailyvendors_payment_type').val('').trigger('change.select2');
                        $("#Dailyvendors_amount").val("");
                        $("#Dailyvendors_sgst").val("");
                        $("#Dailyvendors_sgst_amount").val("");
                        $("#Dailyvendors_cgst").val("");
                        $("#Dailyvendors_cgst_amount").val("");
                        $("#Dailyvendors_igst").val("");
                        $("#Dailyvendors_igst_amount").val("");
                        $("#Dailyvendors_tax_amount").val("");
                        $('#Dailyvendors_company_id').val(result["company"]).trigger('change.select2');
                        if (result["payment_type"] == 88) {
                            $("#Dailyvendors_bank").attr("disabled", false);
                            $('#Dailyvendors_bank').val(result["bank"]).trigger('change.select2');
                            $("#Dailyvendors_cheque_no").attr("readonly", false);
                            $("#Dailyvendors_cheque_no").val(result["cheque_no"]);
                        } else {
                            $('#Dailyvendors_bank').val('').trigger('change.select2');
                            $("#Dailyvendors_cheque_no").val("");
                            $("#Dailyvendors_bank").attr("disabled", true);
                            $("#Dailyvendors_cheque_no").attr("readonly", true);

                        }

                    } else {
                        $('#Dailyvendors_company_id').val(result["company"]).trigger('change.select2');
                        $('#Dailyvendors_vendor_id').val(result["vendor_id"]).trigger('change.select2');
                        $('#Dailyvendors_payment_type').val(result["payment_type"]).trigger('change.select2');
                        $('#Dailyvendors_project_id').val(result["project_id"]).trigger('change.select2');
                        getPurchaseList(result["company"], result["project_id"], result["vendor_id"], result["purchase_id"]);
                        $("#Dailyvendors_description").val(result["description"]);
                        $("#Dailyvendors_amount").val(result["amount"]);
                        $("#Dailyvendors_sgst").val(result["sgst"]);
                        $("#Dailyvendors_sgst_amount").val(result["sgst_amount"]);
                        $("#Dailyvendors_cgst").val(result["cgst"]);
                        $("#Dailyvendors_cgst_amount").val(result["cgst_amount"]);
                        $("#Dailyvendors_igst").val(result["igst"]);
                        $("#Dailyvendors_igst_amount").val(result["igst_amount"]);
                        $("#Dailyvendors_tax_amount").val(result["tax_amount"]);

                        $("#txtSgst1").text((result["sgst_amount"] === null) ? '' : result["sgst_amount"]);
                        $("#txtCgst1").text((result["cgst_amount"] === null) ? '' : result["cgst_amount"]);
                        $("#txtIgst1").text((result["igst_amount"] === null) ? '' : result["igst_amount"]);

                        var tax_total = 0;
                        if (result["tax_amount"] === null) {
                            tax_total = 0;
                        } else {
                            tax_total = result["tax_amount"];
                        }

                        $("#txtgstTotal").html('<label class="final-section-label w-100p">Total Tax :</label><span class="total-value-label w-100p">' + tax_total + '</span>');
                        var total = (Number(result["amount"]) + Number(tax_total)).toFixed(2);

                        $("#txtTotal1").html('<label class="final-section-label w-100p">Total :</label><span class="total-value-label w-100p">' + total + '');

                        if (result["payment_type"] == 88) {
                            $("#Dailyvendors_bank").attr("disabled", false);
                            $('#Dailyvendors_bank').val(result["bank"]).trigger('change.select2');
                            $("#Dailyvendors_cheque_no").attr("readonly", false);
                            $("#Dailyvendors_cheque_no").val(result["cheque_no"]);
                        } else {
                            $('#Dailyvendors_bank').val('').trigger('change.select2');
                            $("#Dailyvendors_cheque_no").val("");
                            $("#Dailyvendors_bank").attr("disabled", true);
                            $("#Dailyvendors_cheque_no").attr("readonly", true);

                        }
                    }
                }
            });


        });

        $(".amountvalidation").keydown(function (event) {

            if (event.shiftKey == true) {
                event.preventDefault();
            }

            if ((event.keyCode >= 48 && event.keyCode <= 57) ||
                (event.keyCode >= 96 && event.keyCode <= 105) ||
                event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
                event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
                var splitfield = $(this).val().split(".");
                if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
                    event.preventDefault();
                }
            } else {
                event.preventDefault();
            }

            if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
                event.preventDefault();
        });



        $("#previous").click(function (e) {
            var cDate = $("#Dailyvendors_date").val();

            cDate = cDate.split("-").reverse().join("-");
            var newDate = new Date(cDate);
            var prDate = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate() - 1);
            var newPrDate = $.format.date(prDate, "dd-MM-yyyy");
            $("#Dailyvendors_date").val(newPrDate);
            getFullData(newPrDate);
            validateDateSelection(newPrDate);
        });
        $("#current").click(function () {
            var cDate = new Date();
            var newDate = $.format.date(cDate, "dd-MM-yyyy");
            $("#Dailyvendors_date").val(newDate);
            getFullData(newDate);
            validateDateSelection(newDate);
        });
        $("#next").click(function () {
            var cDate = $("#Dailyvendors_date").val();
            cDate = cDate.split("-").reverse().join("-");
            var newDate = new Date(cDate);
            var nxtDate = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate() + 1);
            var newNxtDate = $.format.date(nxtDate, "dd-MM-yyyy");
            $("#Dailyvendors_date").val(newNxtDate);
            getFullData(newNxtDate);
            validateDateSelection(newNxtDate);
        });
        $("#Dailyvendors_payment_type").change(function () {
            var receiptType = $("#Dailyvendors_payment_type").val();
            if (receiptType == 88) {
                $("#Dailyvendors_cheque_no").attr("readonly", false);
                $("#Dailyvendors_bank").attr("disabled", false);
            } else {
                $("#Dailyvendors_cheque_no").attr("readonly", true);
                $("#Dailyvendors_bank").attr("disabled", true);
            }
        });

        $("#txtPurchaseType").change(function () {
            var purchaseType = $("#txtPurchaseType").val();
            var amount = $("#txtTotal").val();
            if (purchaseType == 1) {
                $("#txtPaid").val(0);
                $("#txtPaid").attr("readOnly", true);
            } else if (purchaseType == 2) {
                $("#txtPaid").val(amount);
                $("#txtPaid").attr("readOnly", true);
            } else if (purchaseType == 3) {
                $("#txtPaid").val(0);
                $("#txtPaid").attr("readOnly", false);
            } else {
                $("#txtPaid").val("0");
                $("#txtPaid").attr("readOnly", false);
            }
        });

        $("#btnReset, [data-toggle='collapse']").click(function () {
            $("#txtDailyVendorId").val("");
            $("#buttonsubmit").text("ADD");
            $('#dailyexpense-form').find('input, select, textarea').not("#Dailyvendors_date").val('');
            $("#Dailyexpense_bill_id").val("");
            $("#Dailyexpense_expensehead_id").val("");
            $("#Dailyexpense_vendor_id").html('<option value="">-Select Vendor-</option>');
            $("#Dailyexpense_vendor_id").attr("disabled", true);

            $('#Dailyvendors_vendor_id').val('').trigger('change.select2');
            $('#Dailyvendors_project_id').val('').trigger('change.select2');
            $('#Dailyvendors_payment_type').val('').trigger('change.select2');
            $('#Dailyvendors_bank').val('').trigger('change.select2');

            $("#txtSgst1").text("");
            $("#txtCgst1").text("");
            $("#txtIgst1").text("");
            $("#txtgstTotal").text("");
            $("#txtTotal1").text("");
        });

        $("#Dailyvendors_vendor_id").click(function () {
            var vendor_id = $("#Dailyvendors_vendor_id").val();
            if (vendor_id != "")
                $("#Dailyvendors_payment_type").focus();
        });

        $("#Dailyvendors_payment_type").change(function () {
            var rType = $("#Dailyvendors_payment_type").val();
            if (rType == 88) {
                $("#Dailyvendors_bank").attr("disabled", false);
                $("#Dailyvendors_cheque_no").attr("readonly", false);
            } else {
                $("#Dailyvendors_bank").val("");
                $("#Dailyvendors_cheque_no").val("");
                $("#Dailyvendors_bank").attr("disabled", true);
                $("#Dailyvendors_cheque_no").attr("readonly", true);
            }

        });

        $("#Dailyvendors_payment_type").click(function () {
            var rType = $("#Dailyvendors_payment_type").val();
            if (rType == 88) {
                $("#Dailyvendors_bank").attr("disabled", false);
                $("#Dailyvendors_cheque_no").attr("readonly", false);
                $("#Dailyvendors_bank").focus();
            } else {
                $("#Dailyvendors_bank").val("");
                $("#Dailyvendors_cheque_no").val("");
                $("#Dailyvendors_bank").attr("disabled", true);
                $("#Dailyvendors_cheque_no").attr("readonly", true);
                $("#Dailyvendors_amount").focus();
            }

        });

        $("#Dailyvendors_bank").click(function () {
            var bank = $("#Dailyvendors_bank").val();
            if (bank != "")
                $("#Dailyvendors_cheque_no").focus();
        });

        $("#Dailyvendors_cheque_no").keyup(function (e) {
            if (e.keyCode == 13) {
                var cheque_no = $("#Dailyvendors_cheque_no").val();
                if (cheque_no != "")
                    $("#Dailyvendors_amount").focus();
            }
        });


        $("#Dailyvendors_amount").keyup(function (e) {
            if (e.keyCode == 13) {
                var amount = $("#Dailyvendors_amount").val();
                if (amount != "")
                    $("#Dailyvendors_sgst").focus();

            }
        });

        $("#Dailyvendors_sgst").keyup(function (e) {
            if (e.keyCode == 13) {
                var amount = $("#Dailyvendors_sgst").val();
                $("#Dailyvendors_cgst").focus();

            }
        });

        $("#Dailyvendors_cgst").keyup(function (e) {
            if (e.keyCode == 13) {
                var amount = $("#Dailyvendors_cgst").val();
                $("#Dailyvendors_igst").focus();

            }
        });

        $("#Dailyvendors_igst").keyup(function (e) {
            if (e.keyCode == 13) {
                var amount = $("#Dailyvendors_igst").val();
                $("#Dailyvendors_description").focus();

            }
        });

        $("#Dailyvendors_description").keyup(function (e) {
            if (e.keyCode == 13) {
                var description = $("#Dailyvendors_description").val();
                if (description != "")
                    $("#buttonsubmit").focus();

            }
        });



    });

    function validateDateSelection(cDate) {
        var selDate = cDate;
        var currDate = new Date();
        currDate = $.format.date(currDate, "dd-MM-yyyy");
        if (selDate == currDate) {
            $("a#next").css("pointer-events", "none").css("color", "#ccccc");
        } else {
            $("a#next").css("pointer-events", "visible").css("color", "#337ab7");
        }
        $("#btnReset").click();
        $("#txtDailyVendorId").val("");
        $("#Dailyvendors_date").val(selDate);
        $("#Dailyexpense_vendor_id").html('<option value="">-Select Vendor-</option>');
        $("#Dailyexpense_vendor_id").attr("disabled", true);
    }

    function validateData() {
        var billNumber = $("#Dailyexpense_bill_id").val();
        var expenseType = $("#Dailyexpense_expensehead_id").val();
        var vendorId = $("#Dailyexpense_vendor_id").val();
        var amount = $("#txtAmount").val();
        var sgstP = $("#txtSgstp").val();
        var sgst = $("#txtSgst").val();
        var cgstP = $("#txtCgstp").val();
        var cgst = $("#txtCgst").val();
        var total = $("#txtTotal").val();
        var receiptType = $("#Dailyexpense_dailyexpense_receipt_type").val();
        var receipt = $("#txtReceipt").val();
        var purchaseType = $("#txtPurchaseType").val();
        var paid = $("#txtPaid").val();
        var currId = $(this).attr(id);
        if (expenseType != "" || expenseType != 0) {
            $("#Dailyexpense_dailyexpense_receipt_type").attr("disabled", true);
            $("#txtReceipt").attr("readonly", true);
            $("#Dailyexpense_vendor_id").val("");
            $("#Dailyexpense_vendor_id").attr("disabled", false);
            $("#txtAmount").attr("readonly", false);
            $("#txtSgstp").attr("readonly", false);
            $("#txtSgst").attr("readonly", false);
            $("#txtCgstp").attr("readonly", false);
            $("#txtCgst").attr("readonly", false);
            $("#txtTotal").attr("readonly", false);
        }
        if (currId == "Dailyexpense_expensehead_id" && (expenseType == "" || expenseType == 0)) {
            $("#Dailyexpense_dailyexpense_receipt_type").attr("disabled", false);
            $("#txtReceipt").attr("readonly", false);
        }
    }

    $("#Dailyvendors_amount, #Dailyvendors_sgst, #Dailyvendors_cgst, #Dailyvendors_igst, #Dailyvendors_tds").blur(function () {
        var amount = parseFloat($("#Dailyvendors_amount").val());
        var sgstp = parseFloat($("#Dailyvendors_sgst").val());
        var cgstp = parseFloat($("#Dailyvendors_cgst").val());
        var igstp = parseFloat($("#Dailyvendors_igst").val());

        var sgst = ((sgstp / 100) * amount).toFixed(2);
        var cgst = ((cgstp / 100) * amount).toFixed(2);
        var igst = ((igstp / 100) * amount).toFixed(2);

        if (isNaN(sgst))
            sgst = 0;
        if (isNaN(cgst))
            cgst = 0;
        if (isNaN(igst))
            igst = 0;
        if (isNaN(amount))
            amount = 0;

        var gsttotal = (parseFloat(sgst) + parseFloat(cgst) + parseFloat(igst)).toFixed(2);
        var total = (amount + parseFloat(sgst) + parseFloat(cgst) + parseFloat(igst)).toFixed(2);

        var tds = $("#Dailyvendors_tds").val();
        var tdsamount = (tds / 100) * total;
        if (isNaN(tdsamount))
            tdsamount = 0;
        var paidamount = (total - tdsamount).toFixed(2);

        $("#Dailyvendors_sgst_amount").val(sgst);
        $("#Dailyvendors_cgst_amount").val(cgst);
        $("#Dailyvendors_igst_amount").val(igst);
        $("#Dailyvendors_tax_amount").val(gsttotal);

        $(".gstvalue").css({
            "display": "inline-block"
        });
        $("#txtSgst1").text(sgst);
        $("#txtCgst1").text(cgst);
        $("#txtIgst1").text(igst);
        $("#txtTds1").text(tdsamount);
        $("#Dailyvendors_tds_amount").val(tdsamount);
        $("#txtTotal1").html('<label class="final-section-label w-100p">Total :</label><span class="total-value-label w-100p">' + total + '');
        $("#txtgstTotal").html('<label class="final-section-label w-100p">Total Tax:</label><span class="total-value-label w-100p">' + gsttotal + '</span>');
        $("#txtTdsPaid").html('<label class="final-section-label w-100p">Amount To Be Paid :</label><span class="total-value-label w-100p">' + paidamount + '');
        $("#Dailyvendors_paidamount").val(paidamount);
    });

    $('.percentage').keyup(function () {
        if ($(this).val() > 100) {
            $(this).val('');
        }
    });
    $(document).on('click', '.delete_confirmation', function (e) {
        var rowId = $(this).attr("id");
        var expId = $("#dailyvendorid" + rowId).val();
        var date = $("#Dailyvendors_date").val();
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                type: "GET",
                data: {
                    expId: expId,
                    expense_date: date,
                    status: 1,
                    delId: "",
                    action: 3
                },
                url: "<?php echo Yii::app()->createUrl("vendors/deleteconfirmation") ?>",
                success: function (data) {
                    $("#daybook-entry").html(data);
                    if (data == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-danger"><strong>Failed!</strong> This request unable to process now. Please try again later.</div>')
                            .fadeOut(5000);
                        return false;
                    } else {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Success!</strong> Request for deleting this row is send to admin. Please wait for response.</div>')
                            .fadeOut(5000);
                    }
                }
            });
        } else {
            return false;
        }
    });
    $(document).on('click', '.delete_restore', function (e) {
        var rowId = $(this).attr("id");
        var delId = $(this).attr("data-id");
        var expId = $("#dailyvendorid" + rowId).val();
        var date = $("#Dailyvendors_date").val();
        var answer = confirm("Are you sure you want to restore?");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                type: "POST",
                data: {
                    expId: expId,
                    expense_date: date,
                    status: 2,
                    delId: delId,
                    action: 3
                },
                url: "<?php echo Yii::app()->createUrl("vendors/deleteconfirmation") ?>",
                success: function (data) {
                    $("#daybook-entry").html(data);
                    if (data == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-danger"><strong>Failed!</strong> This request unable to process now. Please try again later.</div>')
                            .fadeOut(5000);
                        return false;
                    } else {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Success!</strong> Restored data successfully.</div>')
                            .fadeOut(5000);
                    }
                }
            });
        } else {
            return false;
        }
    });
    $(document).on('click', '.delete_row', function (e) {
        var reconStatus = $(this).parents("td").attr("data-status");
        if (reconStatus == 1) {
            alert("Can't delete !Reconciled Entry");
            return;
        }
        var answer = confirm("Are you sure you want to delete?");
        $(".popover").removeClass("in");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            var rowId = $(this).attr("id");
            var vendId = $("#dailyvendorid" + rowId).val();
            var date = $("#Dailyvendors_date").val();
            $.ajax({
                type: "POST",
                data: {
                    vendId: vendId,
                    vendors_date: date
                },
                url: "<?php echo Yii::app()->createUrl("vendors/deletdailyvendors") ?>",
                success: function (data) {
                    if (data == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-warning"><strong>Failed !</strong> please try again.</div>')
                            .fadeOut(5000);
                        $("#buttonsubmit").attr('disabled', false);
                        return false;
                    } else {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Success!</strong> Row Deleted.</div>')
                            .fadeOut(5000);
                        $("#newlist").html(data);
                    }
                }
            })
        }

    })
    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });

    $("#Dailyvendors_cheque_no").change(function (e) {
        var cheque_no = $(this).val();
        var bank = $("#Dailyvendors_bank").val();
        $.ajax({
            type: 'POST',
            data: {
                cheque_no: cheque_no,
                bank: bank
            },
            url: "<?php echo Yii::app()->createUrl("expenses/testChequeNumber") ?>",
            success: function (data) {
                $('.chq_error').html(data);
                return;
            }
        })
    });
</script>