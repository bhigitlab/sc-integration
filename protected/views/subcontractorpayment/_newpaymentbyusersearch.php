<?php
/* @var $this ExpensesController */
/* @var $model Expenses */
/* @var $form CActiveForm */
?>

<div class="page_filter clearfix custom-form-style">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'action'=>Yii::app()->createUrl($this->route),
		'method'=>'get',
	)); ?>
        <div class="row">
            <div class="form-group col-xs-12 col-md-2">
                <label for="from">From</label>
                <input type="text" name="SubcontractorPayment[fromdate]" id="SubcontractorPayment_fromdate" autocomplete="off" readonly=true class="form-control" placeholder="From Date" value="<?php echo ($model->fromdate)?date("d-m-Y", strtotime($model->fromdate)):""; ?>"/>
            </div>
            <div class="form-group col-xs-12 col-md-2">
                <label for="to">To</label>
                <input type="text" name="SubcontractorPayment[todate]" id="SubcontractorPayment_todate" class="form-control" autocomplete="off" readonly=true  placeholder="To Date" value="<?php echo ($model->todate)?date("Y-m-d", strtotime($model->todate)):""; ?>"/>
            </div>
            <div class="form-group col-xs-12 col-md-2 filter_btns pull-right margin-top-20">
                <div class="pull-right">
                    <button type="submit" class="btn btn-sm btn-primary" name="submitForm" id="submitForm">Go</button>
                    <button type="button" class="btn btn-sm btn-default" name="clearForm" id="clearForm">Clear</button>
                </div>
            </div>
        </div>
	<?php $this->endWidget(); ?>

</div><!-- search-form -->
