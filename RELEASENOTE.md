## Version 25.03.1

### `05 March 2025`


**We are excited to announce the following updates:**

#### Bill Without PO:
> _Enhanced edit functionality for seamless updates._

#### Consumption Request:
> _Improved performance._

#### Labour Entry Listing Table:
> _UI improvements for better readability._

#### Master Tables:
> _Added pagination for better data management._

#### PO - Add PO by Length:
>_Improved purchase item loading functionality._

#### Project Creation:
> _Fixed error message issue during project creation._

#### Project Estimation:
>_Enhanced labour entry functionality._

#### Project Report:
>_Improved listing functionality for discount amounts._

#### Sales Invoice:
> _Fixed receipt duplication issue._

#### Sales Quotation:
> _Resolved percentage calculation issue._

#### SC Quotation & Sales Invoice Forms:
> _UI enhancements for better usability._

#### Settings Menu:
>_Submenus are now arranged alphabetically._

#### Subcontractor Payment Report:
> _Added Labour Entry with Quotation feature, improved dependent filters, and enhanced UI._



*****
