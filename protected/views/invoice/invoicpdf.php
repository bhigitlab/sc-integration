<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">

<div class="container"  style="margin:0px 30px">
    <?php
    $company_address = Company::model()->findBypk($model->company_id);
    ?>
    <br><br>
    <h4 class="page_heading">GST INVOICE</h4>
    <?php
    $company = Company::model()->findByPk($model->company_id);
    ?>

    <table class="details"> 
        <tr>
            <td><label>PROJECT : </label><b><?php echo $project; ?></b></td>
            <td><label>CLIENT : </label><b><?php echo (isset($client)) ? $client : ''; ?></b></td>
        </tr>
        <tr>
            <td><label>Site : </label><b>MARADU</b></td>            
            <td><label>Tel : </label><b>9072654431</b></td>
        </tr>
        <tr>
            <td><label>ADDRESS : </label><b>Iyattil Junction,chittor Road Eranakulam, 682011</b></td>
            <td><label>GST NO : </label><b><?php echo (isset($model->inv_no)) ? $model->inv_no : ''; ?></b></td>
        </tr>
    </table>	
    <br>
    <table class="detail-table">
        <thead>
            <tr>
                <th>Sl.No</th>
                <th>Description</th>
                <th>HSN Code</th>
                <?php
                if ($model->type == 'quantity_rate') {
                    ?>
                    <th>Quantity</th>
                    <th>Unit</th>
                    <th>Rate</th>
                <?php } ?>
                <th>SGST (%)</th>
                <th>SGST Amount</th>
                <th>CGST (%)</th>
                <th>CGST Amount</th>
                <th>IGST (%)</th>
                <th>IGST Amount</th>
                <th>Amount</th>
                <th>Tax Amount</th>
            </tr>
        </thead>
        <tbody class="addrow">
            <?php
            if (!empty($itemmodel)) {
                foreach ($itemmodel as $i => $value) {
                    ?>

                    <tr  class="tr_class">
                        <td><input type="hidden" name="sl_No[]" value="<?php echo $i + 1; ?>"> <?php echo $i + 1; ?></td>
                        <td><input type="hidden" name="description[]" value=""><?php echo $value->description; ?> </td>
                        <td><input type="hidden" name="description[]" value=""><?php echo $value->hsn_code; ?> </td>
                        <?php
                        if ($model->type == 'quantity_rate') {
                            ?>
                            <td class="text-right"><input type="hidden" name="quantity[]" value="<?php echo $value->quantity; ?>"> <?php echo $value->quantity; ?> </td>
                            <td><input type="hidden" name="unit[]" value="<?php echo $value->unit; ?>" readonly><?php echo $value->unit; ?></td>
                            <td class="text-right"><input type="hidden" name="rate[]" value="<?php echo $value->rate; ?>" readonly><?php echo Controller::money_format_inr($value->rate, 2, 1); ?></td>
                        <?php } ?>
                        <td class="text-right"><?php echo number_format($value->sgst, 2); ?></td>
                        <td class="text-right"><?php echo Controller::money_format_inr($value->sgst_amount, 2, 1); ?></td>
                        <td class="text-right"><?php echo number_format($value->cgst, 2); ?></td>
                        <td class="text-right"><?php echo Controller::money_format_inr($value->cgst_amount, 2, 1); ?></td>
                        <td class="text-right"><?php echo number_format($value->igst, 2); ?></td>
                        <td class="text-right"><?php echo Controller::money_format_inr($value->igst_amount, 2, 1); ?></td>
                        <td class="text-right"><input type="hidden" name="amount[]" value="<?php echo $value->amount; ?>" readonly><?php echo Controller::money_format_inr($value->amount, 2, 1); ?></td>
                        <td class="text-right"><input type="hidden" name="amount[]" value="<?php echo $value->amount; ?>" readonly><?php echo Controller::money_format_inr($value->tax_amount, 2, 1); ?></td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
        <tfoot>
            <?php
            $colspan1 = 9;
            $colspan2 = 11;
            if ($model->type == 'quantity_rate') {
                $colspan1 = 12;
                $colspan2 = 14;
            }
            ?>
            <tr>
                <th style="text-align: right;" colspan="<?php echo $colspan1; ?>" class="text-right">TOTAL: </th>
                <th class="text-right"><div id="grand_total"><?php echo number_format($model->amount, 2); ?></div></th>
                <th class="text-right"><div id="grand_total"><?php echo number_format($model->tax_amount, 2); ?></div></th>
            </tr>
            <tr>
                <th colspan="<?php echo $colspan2 - 1; ?>" class="text-right">GRAND TOTAL:</th>
                <th class="text-right"><div id="net_amount"><?php echo number_format($model->tax_amount + $model->amount, 2, '.', '') ?></div></th>
            </tr>
            <tr>
                <th colspan="<?php echo $colspan2 - 1; ?>" class="text-right">WRITE OFF:</th>
                <th class="text-right"><div id="net_amount"><?php echo number_format($writeoff->amount, 2, '.', '') ?></div></th>
            </tr>
            <tr>
                <th colspan="<?php echo $colspan2 - 1; ?>" class="text-right">BALANCE:</th>
                <th class="text-right"><div id="net_amount"><?php echo number_format(($model->tax_amount + $model->amount) - $writeoff->amount, 2, '.', '') ?></div></th>
            </tr>
        </tfoot>
    </table>


    <br><br>

    <p>Thanking you and assuring you the best services at all times.</p>
    <b>For <?php echo Yii::app()->name ?></b>
</div>
<style>
    .page_heading{
        margin-top:10px;
    }
</style>