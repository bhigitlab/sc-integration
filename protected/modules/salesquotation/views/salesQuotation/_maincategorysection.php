
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'section-form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    //'action' => Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/addquotationitems"),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => false,
    ),
        ));
?>
<div class="row mt-2 section_holder">        
    <?php if(isset($_GET['rev']) && $_GET['status']==1) {?>
    <input id="row_revision" type="hidden" value="<?php echo 'REV-'.$_GET['rev']?>" name=""> 
    <?php } ?>      
    <div class="col-md-3">
        <?php echo CHtml::hiddenField('section_id', '', array('id' => 'section_id')); ?>
        <?php echo $form->textField($model, 'section_name', array('class' => 'form-control  section_name', 'autocomplete' => 'off', 'placeholder' => 'Section', 'name' => 'section_name', 'id' => 'section_name')); ?>
        <div class="errorMessage"></div>
    </div>
    <?php
    
    if ($quotation_model->template_type == '5') {
    ?>
    <div class="col-md-2">
        <?php
            echo $form->dropDownList($main_wtype_model, 'master_cat_id',
                    CHtml::listData(QuotationCategoryMaster::model()->findAll(), 'id', 'name'),
                    array('class' => 'form-control js-example-basic-single field_change require QuotationGenCategory_master_cat_id', 'empty' => '-Select Category-', 'style' => 'width:100%'));
        ?>
        <?php echo CHtml::hiddenField('category_id_data', '', array('id' => 'category_id_data')); ?>
    </div>
    <?PHP } ?>
    <div class="col-md-2">
        <?php
        echo $form->dropDownList($model, 'unit', CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name'), array('class' => 'form-control js-example-basic-single   require ', 'empty' => '-Select Unit-', 'name' => 'unit', 'style' => 'width:100%', 'id' => 'sec_unit')
        );
        ?>
    </div>
    <?php
    
    if ($quotation_model->template_type == '5') {
    ?>
    <div class="col-md-2">
        <?php
        echo $form->textField($model, 'hsn_code', array('class' => 'form-control sec_hsn_code', 'name' => 'hsn_code','placeholder'=>'HSN Code', 'style' => 'width:100%', 'id' => 'sec_hsn_code')
        );
        ?>
    </div>
    <?php } ?>
    <div class="col-md-1">
        <?php echo $form->textField($model, 'quantity', array('class' => 'form-control allownumericdecimal quantity_sub', 'autocomplete' => 'off', 'placeholder' => 'qty', 'name' => 'quantity', 'id' => 'sec_quantity')); ?>
        <?php echo $form->error($model, 'quantity'); ?>
    </div>
    <?php 
    
    if ($quotation_model->template_type == '5') {
        ?>
    <div class="col-md-1">
        <?php echo $form->textField($model, 'quantity_nos', array('class' => 'form-control allownumericdecimal ', 'autocomplete' => 'off', 'placeholder' => 'qty nos', 'name' => 'quantity_nos', 'id' => 'sec_quantity_nos')); ?>
        <?php echo $form->error($model, 'quantity_nos'); ?>
    </div>
    <?php } ?>
    <div class="col-md-1">
        <?php echo $form->textField($model, 'mrp', array('class' => 'form-control allownumericdecimal ', 'autocomplete' => 'off', 'placeholder' => 'MRP', 'name' => 'mrp', 'id' => 'sec_mrp')); ?>
        <?php echo $form->error($model, 'mrp'); ?>
    </div>
    <div class="col-md-2">
        <?php
        
        if ($quotation_model->template_type == '5') {
        ?>
            <label>&nbsp;</label>
        <?php } ?>
        <?php echo $form->textField($model, 'amount_after_discount', array('class' => 'form-control allownumericdecimal amountafter-discount', 'autocomplete' => 'off', 'name' => 'amount_after_discount', 'placeholder' => 'amt after disc', 'id' => 'sec_amount_after_discount')); ?>
        <?php echo $form->error($model, 'amount_after_discount'); ?>
    </div>
</div>
<?php
   if ($quotation_model->template_type == '5') {
?>
    <div class="clearfix">
        <button type="button" class="pull-right btn btn-danger btn-sm add_item_details_list" 
       >Add Item Details</b>
        </button>
    </div>
    <div class="item_details_tbl" id="quotation_item"></div>
    <div class="panel-footer save-btnHold text-center button-panel item_details_tbl">
        <button type="button" class = 'btn btn-info additem'>Save</button>
        <button type="button" class ='btn btn-default btn_close'>Close</button>
    </div>
<?php } ?>
<?php $this->endWidget(); ?>
<div class="add_sub_section toggle_btn" title="Add Sub Section">        
    <i class="toggle-caret fa fa-plus"></i>
</div>

<script>
    $(document).on("click",".add_item_details_list",function(){
        $(this).parents(".quotation_sec").find(".item_details_tbl").toggleClass("hide");
    })
</script>
