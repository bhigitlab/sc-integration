<div class="container" id="project">
	<div class="loading-overlay">
		<span class="fa fa-spinner fa-3x fa-spin"></span>
	</div>
	<h4>Filter By</h4>
	<div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
	<div class="">

		<div class="">
			<form id="projectsearch" method="post" action="<?php echo $this->createAbsoluteUrl('projects/projectlist'); ?>">
				<label>Project :</label>
				<input type="text" name="project" id="project">
			</form>
		</div>
	</div>


	<div class="add-btn">
		<button data-toggle="modal" data-target="#addProject" class="createProject">+</button>
	</div>

	<div class="exp-list">
		<?php
		$tblpx = Yii::app()->db->tablePrefix;
		foreach ($model as $mod) {

			$id = $mod['pid'];
			//echo $id;
			$credit = Yii::app()->db->createCommand('select projectid,sum(amount) as credit from ' . $tblpx . 'expenses
						where type=72 and projectid=' . $id)->queryAll();
			//print_r($credit[0]['credit']); 

			$debit = Yii::app()->db->createCommand('select projectid,sum(amount) as debit from ' . $tblpx . 'expenses
						where type=73 and projectid=' . $id)->queryAll();
			//print_r($debit); 

			//$array=array();
			$exp = Yii::app()->db->createCommand('select first_name from ' . $tblpx . 'expenses
						inner join ' . $tblpx . 'users on ' . $tblpx . 'expenses.userid = ' . $tblpx . 'expenses.userid
						where  projectid=' . $id)->queryAll();
			//print_r($exp); die();
			//print_r($exp[0]['first_name']); die();


			$rendering = array();
			for ($i = 0; $i < count($exp); $i++) {
				$rend = $exp[$i]['first_name'];
				array_push($rendering, $rend);
			}
			// print_r($rendering); die();

		?>
			<div class="row">
				<div class="col-md-6 col-sm-7 col-xs-5">
					<div><?php echo $mod['name']; ?></div>
					<div>Assigned to:<?php echo implode(", ", $rendering); ?></div>
					<div><?php echo $mod['caption']; ?></div>
				</div>
				<div class="col-md-3 col-sm-4 col-xs-4">
					<div>Credit:<?php echo $credit[0]['credit'] ?></div>
					<div>Debit:<?php echo $debit[0]['debit'] ?></div>
				</div>
				<div class="col-md-3 col-sm-1 col-xs-1">
					<span class="fa fa-edit editProject" data-toggle="modal" data-target=".edit" data-id="<?php echo $id; ?>"></span>
				</div>
			</div>
		<?php
		}
		?>

	</div>



	<!-- Add Project Popup -->
	<div id="addProject" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">

		</div>
	</div>


	<!-- Edit Project Popup -->

	<div class="modal fade edit" role="dialog">
		<div class="modal-dialog modal-lg">


		</div>
	</div>

	<script>
		$(document).ready(function() {
			$('.createProject').click(function() {
				$('.loading-overlay').addClass('is-active');
				$.ajax({
					type: "GET",

					url: "<?php echo $this->createUrl('projects/create') ?>",
					success: function(response) {
						$("#addProject").html(response);

					}
				});
			});

			$('.editProject').click(function() {
				// alert("hai");
				var id = $(this).attr('data-id');
				$('.loading-overlay').addClass('is-active');

				$.ajax({
					type: "GET",
					url: "<?php echo $this->createUrl('projects/update&id=') ?>" + id,
					success: function(response) {
						$(".edit").html(response);

					}
				});

			});

			jQuery(function($) {
				$('#project').on('keydown', function(event) {
					if (event.keyCode == 13) {
						$("#projectsearch").submit();
					}


				});
			});

		});
		$(document).ajaxComplete(function() {
			$('.loading-overlay').removeClass('is-active');
			$('#loading').hide();
		});
	</script>