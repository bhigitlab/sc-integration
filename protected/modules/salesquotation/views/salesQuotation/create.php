<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<style>
    .subitem_detais{
        padding: 5px 10px;    
        box-shadow: 3px 3px 11px 7px #eee;
    }
    .position-relative{
        position:relative;
    }
    .toggle_btn{
        position: absolute;
        right: 0;
        top: 8px;
        width:120px;
        text-align:center;
        cursor: pointer;
    }
    .toggle_btn1{
        position: absolute;
        right: 40px;
        top: 12px;
        /* width:120px; */
        text-align:center;
        cursor: pointer;
    }
    .toggle-caret{
        font-size: 4rem;
        font-weight: bold;
        color: #3383ca;
    }
    
    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown::before {
        position: absolute;
        content: " \2193";
        top: 0px;
        right: -8px;
        height: 20px;
        width: 20px;
    }

    button#caret {
        border: none;
        background: none;
        position: absolute;
        top: 0px;
        right: 0px;
    }

    .invoicemaindiv th,
    .invoicemaindiv td {
        padding: 10px;
    }

    .addcolumn {
        cursor: pointer;
        background-color: black;
        border: none;
        color: white;
        text-align: center;
        text-decoration: none;
        margin: 2px;
        font-size: 30px;
        width: 40px;
    }

    .invoicemaindiv .pull-right,
    .invoicemaindiv .pull-left {
        float: none !important;
    }
    .checkek_edit {
        pointer-events: none;
    }
    .add_selection {
        background: #000;
    }
    .toast-container {
        width: 350px;
    }

    .toast-position-top-right {
        top: 57px;
        right: 6px;
    }

    .toast-item-close {
        background-image: none;
        cursor: pointer;
        width: 12px;
        height: 12px;
        text-align: center;
        border-radius: 2px;
    }

    .toast-item-image {
        font-size: 24px;
    }
    .toast-item-close:hover {
        color: red;
    }
    .toast-item {
        border: transparent;
        border-radius: 3px;
        font-size: 10px;
        opacity: 1;
        background-color: rgba(34, 45, 50, 0.8);
    }
    .toast-item-wrapper p {
        margin: 0px 5px 0px 42px;
        font-size: 14px;
        text-align: justify;
    }
    .toast-type-success {
        background-color: #00A65A;
        border-color: #00A65A;
    }
    .toast-type-error {
        background-color: #DD4B39;
        border-color: #DD4B39;
    }
    .toast-type-notice {
        background-color: #00C0EF;
        border-color: #00C0EF;
    }

    .toast-type-warning {
        background-color: #F39C12;
        border-color: #F39C12;
    }
    .span_class {
        min-width: 70px;
        display: inline-block;
    }
    .purchase-title {
        border-bottom: 1px solid #ddd;
    }
    .purchase_items h3 {
        font-size: 18px;
        color: #333;
        margin: 0px;
        padding: 0px;
        text-align: left;
        padding-bottom: 10px;
    }
    .purchase_items h4 {
        margin: 0px 15px 10px 15px !important;
        line-height: 24px;
        background-color: #fafafa;
        font-size: 16px;
    }
    .purchase_items {
        padding: 15px;
        /* box-shadow: 0 0 13px 1px rgba(0, 0, 0, 0.25); */
        margin-bottom: 15px;
    }

    .purchaseitem {
        display: inline-block;
        margin-right: 20px;
    }

    .purchaseitem .form-control {
        height: 28px;
        display: inline-block;
        padding: 6px 3px;
    }

    .purchaseitem label {
        display: block;
    }

    .purchaseitem last-child {
        margin-right: 0px;
    }

    .purchaseitem label {
        font-size: 12px;
    }

    .remark {
        display: none;
    }

    .padding-box {
        padding: 3px 0px;
        min-height: 17px;
        display: inline-block;
    }

    th {
        height: auto;
    }

    .quantity,
    .rate {
        max-width: 80px;
    }

    .p_block {
        margin-bottom: 10px;
        padding: 0px 15px;
    }

    .gsts input {
        width: 50px;
    }

    .amt_sec {
        float: right;
    }

    .amt_sec:after,
    .padding-box:after {
        display: block;
        content: '';
        clear: both;
    }

    .client_data {
        margin-top: 6px;
    }

    #previous_details {
        position: fixed;
        top: 94px;
        left: 70px;
        right: 70px;
        z-index: 2;
        max-width: 1150px;
        margin: 0 auto;
    }

    .text_align {
        text-align: center;
    }

    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .tooltip-hiden {
        width: auto;
    }

    .form-fields {
        margin-bottom: 10px;
    }

    .row_field {
        margin-bottom: 15px;
    }

    .mr-3{
        margin-right:15px;
    }
    @media(max-width: 767px) {
        .purchaseitem {
            display: block;
            padding-bottom: 5px;
            margin-right: 0px;
        }

        .quantity,
        .rate,
        .small_class {
            width: auto !important;
        }

        .p_block {
            margin-bottom: 0px;
        }

        .padding-box {
            float: right;
        }

        #tax_amount,
        #item_amount {
            display: inline-block;
            float: none;
            text-align: right;
        }
    }


    @media(min-width: 767px) {
        .invoicemaindiv .pull-right {
            float: right !important;
        }

        .invoicemaindiv .pull-left {
            float: left !important;
        }
    }

    .error_message {
        color: red;
    }

    a.pdf_excel {
        background-color: #6a8ec7;
        display: inline-block;
        padding: 8px;
        color: #fff;
        border: 1px solid #6a8ec8;
    }
    .bold{
        font-weight:bold;
    }
    .alert-msg-box {    
        margin-left: 20px;    
        max-width: 400px;
    }
</style>
<?php
/* @var $this SalesQuotationController */
/* @var $model SalesQuotation */

$this->breadcrumbs = array(
    'Sales Quotations' => array('index'),
    'Create',
);
?>
<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="flash-holder flash-success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<?php if (Yii::app()->user->hasFlash('error')) : ?>
    <div class="alert alert-danger  alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php endif; ?>
<?php
$main_items = $model->getCategories($model->id);
$qid = '';
if (isset($_GET['qid'])) {
    $qid = $_GET['qid'];
}
$rev = '';
if (isset($_GET['rev'])) {
    $rev = $_GET['rev'];
}
$status = '';
if (isset($_GET['status'])) {
    $status = $_GET['status'];
}
$activeProjectTemplate = $this->getActiveTemplate();
?>
<input type="hidden" value="<?php echo $activeProjectTemplate ?>" class="temp_type">
<div class="container">
    <div class="clearfix purchase-title">
        <h2 class="pull-left ">
            <?php echo $model->isNewRecord ? 'Create' : 'Update' ?>  Quotation
        </h2>
        <?php echo CHtml::link('Quotations', array('salesQuotation/index'), array('class' => 'btn btn-primary pull-right mt')); ?>
    </div>
    <br>
    <div id="msg_box"></div>
    <div class="">
        

        <div id="main_div">
            <?php
            if($model->isNewRecord){
                $file = '_form';
                $activeProjectTemplate = Controller::getActiveTemplate();
                if ($activeProjectTemplate == 'TYPE-2') {
                    $file = '_formnew';
                }else if ($activeProjectTemplate == 'TYPE-5') {
                    $file = '_concordform';
                }
            }else{                
                $file = '_form';                
                if ($model->template_type == '2') {
                    $file = '_formnew';
                }else if ($model->template_type == '5') {
                    $file = '_concordform';
                }  
            }
            
            $this->renderPartial($file, array(
                'model' => $model,
                'modelsales' => $modelsales,
                'user_companies' => $user_companies,
                'main_item_model' => $main_item_model,
                'main_item_master' => $main_item_master,
                'location_data' => $location_data,
                'salesquotation_edit' => $salesquotation_edit,
                'salesquotation_extra_edit' => $salesquotation_extra_edit,
                'salesquotation' => $salesquotation,
                'salesquotation_extra' => $salesquotation_extra,
                'main_sec_model' => $main_sec_model,
                'main_cat_model' => $main_cat_model,
                'main_wtype_model' => $main_wtype_model,
                'qid' => $qid,
                'rev' => $rev,
                'status' => $status
            ));
            ?>
        </div>
    </div>
</div>
<?php echo CHtml::hiddenField('master_id', $qid, array('id' => 'master_id')); ?>
<?php echo CHtml::hiddenField('revision_no', $rev, array('id' => 'revision_no')); ?>
<?php echo CHtml::hiddenField('status', $status, array('id' => 'status')); ?>


<script>

    function countOfLetters(str) {
        var letter = 0;
        for (i = 0; i < str.length; i++) {
            if ((str[i] >= 'A' && str[i] <= 'Z')
                || (str[i] >= 'a' && str[i] <= 'z'))
                letter++;
        }            
        return letter;
    }

    $(document).find(".alert-msg-box").fadeOut( "slow" );
    //add section
    $(document).on('change', '.section_name,#sec_unit,#sec_quantity,#sec_quantity_nos,#sec_mrp,#sec_amount_after_discount,#sec_hsn_code', function () {
        
        if($(this).closest(".add_sec").length>0){       
            var $t = $(this).parents(".row");
            var section = $(this).parents(".row").find('.section_name').val();
            var revision_no = $("#revision_no").val();
            var qid = $("#master_id").val();
            var sec_id = $(this).parents(".row").find("#section_id").val();
            var unit = $(this).parents(".row").find("#sec_unit").val();
            var hsn_code =  $(this).parents(".row").find("#sec_hsn_code").val();
            var quantity = $(this).parents(".row").find("#sec_quantity").val();
            var quantity_nos = $(this).parents(".row").find("#sec_quantity_nos").val();
            var mrp = $(this).parents(".row").find("#sec_mrp").val();
            var amount_after_discount = $(this).parents(".row").find("#sec_amount_after_discount").val();
            var status = "<?php echo isset($_GET['status'])?$_GET['status']:""?>";
            if(status == 1 && sec_id !=""){
                var revision_no = $(this).parents(".row").find("#row_revision").val();
            }

            var url = "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/createsection'); ?>";

            if (section != "") {
                var alphabetCount =  countOfLetters(section);  
                var message ="";                      
                if(alphabetCount < 1){
                    $t.find('.section_name').siblings(".errorMessage").text("Invalid Section name");                 
                    $(".alert-msg-box").hide();
                    return false;
                } else{
                    $t.find('.section_name').siblings(".errorMessage").text("");
                }
                
                $.ajax({
                    url: url,
                    data: {
                        section_name: section,
                        qtn_id: qid,
                        unit: unit,
                        quantity: quantity,
                        quantity_nos: quantity_nos,
                        mrp: mrp,
                        amount_after_discount: amount_after_discount,
                        sec_id: sec_id,
                        revision_no: revision_no,
                        status:status,
                        hsn_code:hsn_code,

                    },
                    type: "POST",
                    dataType: 'json',
                    success: function (result) {
                        $t.find("#section_id").val(result.id);
                        $(document).find("#total_after_discount").val(result.total_amount)
                        $(document).find("#total_after_discount").attr('data-val',result.data_val);
                        console.log("section Saved");  
                        $("#sgst_percent").change();
                        $("#cgst_percent").change();
                        $("#igst_percent").change(); 
                        $('#discount_precent').change();

                    }
                })
            }
        }
    })

    $(document).on('click', '.toggle_btn1 ', function () {       
        $(this).siblings(".worktype_label_sec").slideToggle();
        $(this).children("i").toggleClass("fa-plus fa-minus");
    })

    $(document).on('click', '.toggle_btn', function () {
        $(this).siblings(".sub_sec_block").slideToggle();
        $(this).children("i").toggleClass("fa-plus fa-minus");

    })
//add category label
    $(document).on('change', '#category_label,#cat_unit,#cat_quantity,#cat_quantity_nos,#cat_mrp,#cat_amount_after_discount', function () {
        if($(this).closest(".add_sec").length>0){ 
            var $t = $(this).parents(".row");
            var category_label = $(this).parents(".row").find('#category_label').val();
            var revision_no = $("#revision_no").val();
            var status = "<?php echo isset($_GET['status'])?$_GET['status']:""?>";
            
            var qid = $("#master_id").val();
            var sec_id = $(this).parents(".quotation_sec").find("#section_id").val();
            if((status == 1 && category_label !="")|| (status== 1 && category_label == "" && sec_id !="")){
                var revision_no = $(".inter-rev").val();
            }
            var master_cat_id = $(this).parents(".row").find(".QuotationGenCategory_master_cat_id").val();
            var unit = $(this).parents(".row").find("#cat_unit").val();
            var quantity = $(this).parents(".row").find("#cat_quantity").val();
            var quantity_nos = $(this).parents(".row").find("#cat_quantity_nos").val();
            var mrp = $(this).parents(".row").find("#cat_mrp").val();
            var amount_after_discount = $(this).parents(".row").find("#cat_amount_after_discount").val();
            var cat_label_id = $(this).parents(".row").find('#category_label_id').val();
            var url = "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/createcategorylabel'); ?>";

            if (category_label != "") {
                var alphabetCount =  countOfLetters(category_label);  
                var message ="";                      
                if(alphabetCount < 1){
                    $t.find('#category_label').siblings(".errorMessage").text("Invalid Sub Section");                 
                    $(".alert-msg-box").hide();
                    return false;
                } else{
                    $t.find('#category_label').siblings(".errorMessage").text("");
                }
                $.ajax({
                    url: url,
                    data: {
                        category_label: category_label,
                        qid: qid,
                        section_id: sec_id,
                        unit: unit,
                        quantity: quantity,
                        quantity_nos: quantity_nos,
                        mrp: mrp,
                        amount_after_discount: amount_after_discount,
                        sec_id: sec_id,
                        master_cat_id: master_cat_id,
                        cat_label_id: cat_label_id,
                        revision_no: revision_no,
                        status:status

                    },
                    type: "POST",
                    dataType: 'json',
                    success: function (result) {
                        $t.find("#category_label_id").val(result.id);
                        $t.parents(".panel-body").siblings(".panel-heading").find(".delete_sub").attr('data-id',result.id);
                        var id = 'QuotationGenCategory_master_cat_id' + result.id;
                        $t.find(".QuotationGenCategory_master_cat_id").attr("id", id);
                        $(document).find("#total_after_discount").val(result.total_amount)
                        $(document).find("#total_after_discount").attr('data-val',result.data_val);
                        console.log("Sub section Saved");
                        $("#sgst_percent").change();
                        $("#cgst_percent").change();
                        $("#igst_percent").change();
                        $('#discount_precent').change();
                    }
                })
            }
        }
    })

//add worktype label
    $(document).on('change', '#worktype_label,#wrktype_unit,#wrktype_quantity,#wrktype_quantity_nos,#wrktype_mrp,#wrktype_amount_after_discount,#wrktype_desc', function () {    
        if($(this).closest(".add_sec").length>0){ 
            var $t = $(this);
            var revision_no = $("#revision_no").val();
            var qid = $("#master_id").val();
            var sec_id = $(this).parents(".quotation_sec").find("#section_id").val();
            var cat_label_id = $(this).parents("#items_holder").find('#category_label_id').val();
            var worktype_label = $(this).parents('.worktype_label_row').find('#worktype_label').val();
            var master_cat_id = $(this).parents('.worktype_label_row').find(".QuotationGenWorktype_master_cat_id").val();
            var unit = $(this).parents('.worktype_label_row').find("#wrktype_unit").val();
            var quantity = $(this).parents('.worktype_label_row').find("#wrktype_quantity").val();
            var quantity_nos = $(this).parents('.worktype_label_row').find("#wrktype_quantity_nos").val();
            var mrp = $(this).parents('.worktype_label_row').find("#wrktype_mrp").val();
            var amount_after_discount = $(this).parents('.worktype_label_row').find("#wrktype_amount_after_discount").val();
            var worktype_label_id = $(this).parents('.worktype_label_row').find('#worktype_label_id').val();
            var description = $(this).parents('.worktype_label_row').find('#wrktype_desc').val();
            var url = "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/createworktypelabel'); ?>";


            if (worktype_label != "") {                
                $(this).parents('.worktype_label_row').find('#worktype_label').siblings('.errorMessage').html("");                
                $.ajax({
                    url: url,
                    data: {
                        qid: qid,
                        section_id: sec_id,
                        category_label_id: cat_label_id,
                        worktype_label: worktype_label,
                        unit: unit,
                        quantity: quantity,
                        quantity_nos: quantity_nos,
                        mrp: mrp,
                        amount_after_discount: amount_after_discount,
                        master_cat_id: master_cat_id,
                        worktype_label_id: worktype_label_id,
                        description: description,
                        revision_no: revision_no

                    },
                    type: "POST",
                    dataType: 'json',
                    success: function (result) {
                        $t.parents('.worktype_label_row').find("#worktype_label_id").val(result.id);
                        var id = 'QuotationGenWorktype_master_cat_id' + result.id;
                        $t.parents('.worktype_label_row').find(".QuotationGenWorktype_master_cat_id").attr("id", id);
                        $(document).find("#total_after_discount").val(result.total_amount)
                        $(document).find("#total_after_discount").attr('data-val',result.data_val);
                        console.log("Sub items  Saved");  
                        $("#sgst_percent").change();
                        $("#cgst_percent").change();
                        $("#igst_percent").change(); 
                        $('#discount_precent').change();                    
                    }
                })
            }else{
                $(this).parents('.worktype_label_row').find('#worktype_label').siblings('.errorMessage').html("WorkType is Mandatory"); 
            }
        }
    })
    $(document).on('click', '.add_more_wtype', function (e) {
        $(this).siblings(".remove_wtype").removeClass("hide");
        $(this).parents("#items_holder").find(".worktype_label_data").addClass("uuuy");
        var rows = $(this).parents("#items_holder").find(".worktype_label_data");
        var row_len = $(rows).children().length;
        if (row_len > 0) {
            $(this).siblings(".remove_row").show();
        } else {
            $(this).siblings(".remove_row").hide();
        }

        $clone = $(rows).find('.worktype_label_row:first').clone();
        $clone.addClass("d-block");
        $clone.removeAttr("style");
        $clone.find("#quotation_item").html("");
        $clone.find(".subitem_detais").html("");
        $clone.find(".errorMessage").html("");
        $clone.find("input,textarea").val("").end().appendTo(rows);

    });
    $(document).on('click','.remove_wtype',function(){
        
        var count =  $(this).parents('.panel-body').find('.worktype_label_row').length;               
        if(count == 2){
            $(this).addClass('hide');
        }
        var lastElem =  $(this).parents('.panel-body').find( ".worktype_label_row" ).last();
        
        if($(lastElem).find("#worktype_label").val() == ""){
            $(lastElem).remove();
        } else {
            $(lastElem).find(".worktype_label").siblings(".errorMessage").html("Can't delete the row");
        }
        
    })

    $(document).on('change', '.QuotationGenCategory_master_cat_id', function () {
        $t = $(this);
        var category_id = $(this).val();
        var category_label_id = $(this).parents(".row").find("#section_id").val();
        var qid = "<?php echo $qid ?>";
        var rev = "<?php echo $rev ?>";
        var status = "<?php echo $status ?>";
        var parent_type = '0';
        var parent_id = $(this).parents(".row").find("#section_id").val();
        $.ajax({
            url: "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getquotationitem') ?>",
            data: {
                category_id: category_id,
                category_label_id: category_label_id,
                // form:form,
                qid: qid,
                rev: rev,
                status: status,
                parent_type: parent_type,
                parent_id: parent_id

            },
            type: "POST",

            success: function (data) {
                $t.parents(".row").siblings(".item_details_tbl").removeClass('hide');
                $t.parents(".row").siblings("#quotation_item").html(data);
                $(".panel-footer").show();
            }
        })
    })

    $(document).on('change', '.QuotationGenWorktype_master_cat_id', function () {
        var workType = $(this).parents(".worktype_label_row").find("#worktype_label").val();
        if(workType !=""){            
            $t = $(this);
            var category_id = $(this).val();
            var category_label_id = $(this).parents(".row").find("#worktype_label_id").val();
            var qid = "<?php echo $qid ?>";
            var rev = "<?php echo $rev ?>";
            var status = "<?php echo $status ?>";
            var parent_type = '1';
            var parent_id = $(this).parents(".row").find("#worktype_label_id").val();
            $.ajax({
                url: "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getquotationitem') ?>",
                data: {
                    category_id: category_id,
                    category_label_id: category_label_id,
                    qid: qid,
                    rev: rev,
                    status: status,
                    parent_type: parent_type,
                    parent_id: parent_id

                },
                type: "POST",

                success: function (data) {
                    $t.parents('.worktype_label_row').find("#quotation_item").html(data);
                    var qItemCount = $t.parents('.worktype_label_row').find("#quotation_item").length;                
                    var emptyqItemCount = document.querySelectorAll("#wrapper div:empty").length;
                    
                    if(qItemCount !=emptyqItemCount){
                        $(".panel-footer").show();
                    }else{
                        $(".panel-footer").hide();
                    }                

                }
            })
        }else{ 
            $(this).val('');          
            $(this).parents(".worktype_label_row").find("#worktype_label").siblings('.errorMessage').html("WorkType is Mandatory");            
        }
    })
    $(document).on('click', '.add_more_section', function (e) {
        $(".remove_section").removeClass('hide');
        var rows = $(this).parents(".addmore_btns").siblings(".sections_block");
        var row_len = $(rows).children().length;
        if (row_len > 0) {
            $(this).siblings(".remove_row").show();
        } else {
            $(this).siblings(".remove_row").hide();
        }
        var clone = $(rows).find('.quotation_sec:first').clone();
        $(clone).find(".items_holder").slice(1).remove();
        $(clone).find(".worktype_label_row").slice(1).remove();
        $(clone).find(".panel-footer").hide();
        $(clone).find("input,textarea").val("");
        $(clone).find(".sub_sec_block").hide();
        $(clone).find(".worktype_label_sec").hide();
        $(clone).find(".remove_sub").hide();
        $(clone).find(".remove_wtype").hide();
        $(clone).find("i").removeClass("fa-minus");
        $(clone).find("i").addClass("fa-plus");
        $(clone).find(".addmore_btns").hide();
        $(clone).find("#quotation_item").html("");
        $(clone).find(".item_details_tbl").removeClass("hide");
        var count = $(clone).find('.worktype_label_row').length;
        
        if (count > 1) {
            var lastElem = $(clone).find('.worktype_label_row').last();
            $(lastElem).remove();
        }        

        $(clone).appendTo(rows);

    });
    $(".remove_section").click(function () {
        var count = $(this).parents('.clearfix').siblings(".sections_block").children('.quotation_sec').length;
        if (count == 2) {
            $(".remove_section").addClass('hide');
        }
        var lastElem = $(this).parents('.clearfix').siblings(".sections_block").children('.quotation_sec').last();
        if ($(lastElem).find(".section_name").val() == "") {
            $(lastElem).remove();
        } else {
            $(lastElem).find(".section_name").siblings(".errorMessage").html("Can't delete the row");
        }

    })

    $(document).on('click', '.add_more_subsec', function (e) {
        $(this).siblings(".remove_sub").removeClass('hide');
        var rows = $(this).parents(".clearfix").siblings(".sub_section");
        var row_len = $(rows).children().length;
        if (row_len > 0) {
            $(this).siblings(".remove_row").show();
        } else {
            $(this).siblings(".remove_row").hide();
        }
        var clone = $(rows).find('#items_holder:first').clone();
        $(clone).find(".worktype_label_sec").hide();
        $(clone).find(".remove_wtype").hide();
        $(clone).find("i").removeClass("fa-minus");
        $(clone).find("i").addClass("fa-plus");
        $(clone).find(".worktype_label_row").slice(1).remove();
        $(clone).find(".panel-footer").hide();
        var count = $(clone).find('.worktype_label_row').length;
        
        if (count > 1) {
            var lastElem = $(clone).find('.worktype_label_row').last();
            $(lastElem).remove();
        } 
        $(clone).find("#quotation_item").html("");
        $(clone).find("input,textarea").val("").end().appendTo(rows);

    });
    $(document).on('click', '.remove_sub', function () {

        var count = $(this).parents(".clearfix").siblings(".sub_section").find('.items_holder').length;
        if (count == 2) {
            $(this).addClass('hide');
        }
        var lastElem = $(this).parents(".clearfix").siblings(".sub_section").find(".items_holder").last();

        if ($(lastElem).find("#category_label").val() == "") {
            $(lastElem).remove();
        } else {
            $(lastElem).find("#category_label").siblings(".errorMessage").html("Can't delete the row");
        }

    })

    $(document).on('click', '.additem', function () {
        var elem = $(this);
        var editcount  = $(this).parents(".panel-footer").siblings(".worktype_label_sec").length;
        var type = 0;
        if(editcount >0){
            var type = 1;
        }
        $(this).parents("#section-form").addClass("jjhu");
            var cat_id =  $(this).parents("#section-form").find("#cat_id").val();            
            var master_id =  $(this).parents("#section-form").find("#master_id").val();
            var revision_no =  $(this).parents("#section-form").find("#revision_no").val();             
            var parent_type =  $(this).parents("#section-form").find("#parent_type").val();  
            var parent_id =  $(this).parents("#section-form").find("#parent_id").val();
            var subsec_id = $(this) .parents("#items_holder").find("#category_label").siblings("#category_label_id").val(); 
            
            var temp_type = $('.temp_type').val();
           
            if (temp_type == 'TYPE-5') {
                var subsec_id = $(this) .parents(".quotation_sec").find("#section_id").val();
            }
            var status = "<?php echo isset($_GET['status']) ? $_GET['status'] : "" ?>";

            if(status == 0){
                remoeOldData(master_id,revision_no,parent_type,parent_id,cat_id,subsec_id,type);
            }                                    
                var submitData = $(this).parents("#section-form").find(".clients-form");
                $(submitData).each(function(){
                    var data = $(this).serialize();
                    
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/addquotationitemsDetails'); ?>',
                        data:data,
                        dataType: 'json',
                        success: function (result) {                    
                            $(document).find(".alert-msg-box").show(); 
                            $('html, body').animate({
                                scrollTop: $(document).find(".alert-msg-box").offset().top - 80
                            }, 'slow');                                     
                            $(document).find(".alert-msg").html('Items Saved Successfully');
                            $(document).find("#total_after_discount").val(result.total_amount)
                            $(document).find("#total_after_discount").attr('data-val',result.data_val);
                            $("#sgst_percent").change();
                            $("#cgst_percent").change();
                            $("#igst_percent").change();
                            $('#discount_precent').change();
                            $(elem).parents(".worktype_label_sec").slideToggle();
                            $(elem).parents(".worktype_label_sec").siblings(".toggle_btn1").children("i").toggleClass("fa-plus fa-minus");
                        }
                    })
        
                })
                if(status == 1){
                    location.reload();
                }
            
    });

    function remoeOldData(master_id,revision_no,parent_type,parent_id,cat_id,subsec_id,type){
        
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/removeOldQuotationData'); ?>',
            data:{
                master_id:master_id,
                revision_no:revision_no,
                parent_type:parent_type,
                parent_id:parent_id,
                cat_id:cat_id ,
                subsec_id:subsec_id,
                type :type
            },
                
            success: function (data) {
                console.log("deleted successfully");                    
                    
            }
        })
         
    }

    $(document).on('click', '.btn_close', function () {
        $(this).parents("#section-form").find(".worktype_label_row").slideUp();
        $(this).parents("#section-form").siblings(".clearfix").find(".add_more_wtype").trigger("click");        
        $(this).parents(".panel-footer").hide();

    });

    $(document).on("click",".btn-preview",function(){
        var qtid = $(this).attr("data-qtid");
        var rev = $(this).attr("data-rev");
        var status = $(this).attr('data-status');
        var interRev = $(this).attr('data-inter-rev');
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/quotationPreview'); ?>',
            data: {
                qtid:qtid,
                rev:rev,
                status:status,
                interRev:interRev
            },            
            success: function (result) {
                console.log(result);
                $(".modal-body").html(result);
            }
        })
       
    })

    $(".panel-footer").hide();

    $(function(){        
        var status = "<?php echo isset($_GET['status']) ? $_GET['status'] : "" ?>";
        var qtid = "<?php echo isset($_GET['qid']) ? $_GET['qid'] : "" ?>";
        var rev = "<?php echo isset($_GET['rev']) ? $_GET['rev'] : "" ?>";
        if(status ==1){            
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/updateRevisionStatus'); ?>',
                data: {
                    qtid:qtid,
                    rev:rev
                },            
                success: function (result) {
                    console.log(result);                
                }
            })
        }
    })

    $(document).on("click",".updateRevision",function(){
        var rev = $(this).attr('data-rev');
        var qtid = $(this).attr('data-qid');
        var new_rev = "<?php echo isset($_GET['rev']) ? $_GET['rev'] : "" ?>";
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/updateInterRevision'); ?>',
            data: {
                qtid:qtid,
                rev:rev,
                new_rev:new_rev
            },            
            success: function (result) {
                console.log(result);                
            },
            complete: function (result) {
                setTimeout(function () {
                    location.reload(true);
                },2000);               
            }
        })
    })

    $(document).on("click",".removeQItem",function(){
        
        var id = $(this).parent().attr('id');
        
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/removeQItem'); ?>',
            data: {
                id:id,                
            },            
            success: function (result) {
                console.log(result);                
            },
            complete: function (result) {
                setTimeout(function () {
                    location.reload(true);
                },1000);               
            }
        })
    })

    $(document).on("click",".restoreQItem",function(){
        
        var id = $(this).parent().attr('id');
        
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/restoreQItem'); ?>',
            data: {
                id:id,                
            },            
            success: function (result) {
                console.log(result);                
            },
            complete: function (result) {
                setTimeout(function () {
                    location.reload(true);
                },1000);               
            }
        })
    })

    $(document).on("click",".delete_sub",function(){
        var elem = $(this);
        var subsec_id = $(this).attr('data-id');
        var rev_no =$(this).attr('rev-no');
        var qtid = '<?php echo isset($_REQUEST['qid'])?$_REQUEST['qid']:'' ?>';
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/deleteSubSection'); ?>',
            data: {
                subsec_id:subsec_id,
                rev_no:rev_no  ,
                qtid:qtid              
            },            
            success: function (result) {
                console.log(result);                
            },
            complete: function (result) {
                setTimeout(function () {
                    $(elem).parents(".items_holder").remove();
                },1000);               
            }
        })
        // alert(subsec_id);alert(rev_no);
    })

</script>