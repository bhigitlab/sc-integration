<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<?php if (filter_input(INPUT_GET, 'export')) { ?>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
<?php } ?>
<?php
$style = "";
if (filter_input(INPUT_GET, 'export')) {
    $style = "style='margin:0px 30px'";
} ?>
<div class="container" id="project" <?php echo $style ?>>
    <div class="expenses-heading header-container">
        <h3>Bank Transaction Reports</h3>
        <div class="btn-container">
            <!-- Ledger/List View Button -->
            <?php
            if (isset($_GET['ledger'])) {
                if ($_GET['ledger'] == 1) { ?>
                    <a type="button" class="btn btn-info" style="cursor:pointer;"
                        href="<?php echo $this->createAbsoluteUrl('bank/banktransaction_report', array('date_to' => $date_to, 'date_from' => $date_from, 'ledger' => 0, 'bank_id' => $bank_id)); ?>">
                        List View
                    </a>
                <?php } else { ?>
                    <a type="button" class="btn btn-info" style="cursor:pointer;"
                        href="<?php echo $this->createAbsoluteUrl('bank/banktransaction_report', array('date_to' => $date_to, 'date_from' => $date_from, 'ledger' => 1, 'bank_id' => $bank_id)); ?>">
                        Ledger View
                    </a>
                <?php }
            } ?>
            <?php if (!filter_input(INPUT_GET, 'export')) { ?>
                <!-- Excel Button -->
                <a type="button" class="btn btn-info" style="cursor:pointer;" title="SAVE AS EXCEL"
                    href="<?php echo $this->createAbsoluteUrl('bank/banktransaction_report', array('date_to' => $date_to, 'date_from' => $date_from, 'ledger' => $_GET['ledger'], 'bank_id' => $bank_id, 'export' => 'csv')); ?>">
                    <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>
                </a>

                <!-- PDF Button -->
                <a type="button" class="btn btn-info" style="cursor:pointer;" title="SAVE AS PDF"
                    href="<?php echo $this->createAbsoluteUrl('bank/banktransaction_report', array('date_to' => $date_to, 'date_from' => $date_from, 'ledger' => $_GET['ledger'], 'bank_id' => $bank_id, 'export' => 'pdf')); ?>">
                    <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
                </a>


            <?php } ?>
        </div>
    </div>

    <div class="page_filter clearfix">
        <?php
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $query_company = "";
        $query_company1 = "";
        foreach ($arrVal as $arr) {
            if ($query_company)
                $query_company .= ' OR';
            if ($query_company1)
                $query_company1 .= ' OR';
            $query_company .= " FIND_IN_SET('" . $arr . "', id)";
            $query_company1 .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        $sql = "SELECT * FROM {$tblpx}bank WHERE" . $query_company1 . ";";
        $banks = Yii::app()->db->createCommand($sql)->queryAll();
        //echo "<pre>";print_r($banks);exit;
        $bank_name = array();
        foreach ($banks as $bank) {
            $bank_name[$bank['bank_id']] = $bank["bank_name"];
        }
        asort($bank_name);
        $bankOptions = array();
        foreach ($banks as $bank) {
            $bankOptions[$bank['bank_id']] = $bank['bank_name'];
        }
        if (!isset($_REQUEST['date_from']) || $_REQUEST['date_from'] == '') {
            $date_from = '';
        } else {
            $date_from = date('Y-m-d', strtotime($_REQUEST['date_from']));
        }
        if (!isset($_REQUEST['date_to']) || $_REQUEST['date_to'] == '') {
            $date_to = '';
        } else {
            $date_to = date('Y-m-d', strtotime($_REQUEST['date_to']));
        }
        if (!isset($_REQUEST['bank_id']) || $_REQUEST['bank_id'] == '') {
            $bank_id = '';
        } else {
            $bank_id = $_REQUEST['bank_id'];
        }
        $current_date = date("Y-m-d");
        ?>
        <?php if (!filter_input(INPUT_GET, 'export')) { ?>
            <?php $form = $this->beginWidget('CActiveForm', array(
                'action' => Yii::app()->createUrl($this->route),
                'method' => 'get',
                'id' => "cashreportform"
            )); ?>
            <div class="search-form">
                <div class="row">
                    <div class="form-group  col-xs-12 col-sm-3 col-md-2">
                        <!-- <div class="display-flex"> -->
                        <label>From</label>
                        <?php echo CHtml::textField('date_from', $date_from, array('placeholder' => 'Date From', "id" => "date_from", "class" => "form-control", 'style' => 'display:inline-block', 'autoComplete' => 'off')); ?>
                        <!-- </div> -->
                    </div>
                    <div class="form-group  col-xs-12 col-sm-3 col-md-2">
                        <!-- <div class="display-flex"> -->
                        <label>To</label>
                        <?php echo CHtml::textField('date_to', $date_to, array('placeholder' => 'Date To', "class" => "form-control", 'style' => 'display:inline-block', "id" => "date_to", 'autoComplete' => 'off')); ?>
                        <!-- </div> -->
                    </div>
                    <div class="form-group  col-xs-12 col-sm-3 col-md-2">
                        <!-- <div class="display-flex"> -->
                        <label>Bank</label>
                        <?php
                        $bankList = CHtml::listData(Bank::model()->findAll(array(
                            'select' => array('bank_id', 'bank_name'),
                            'order' => 'bank_name ASC',
                            'condition' => '(' . $query_company1 . ')',
                            'distinct' => true,
                        )), 'bank_id', 'bank_name');

                        $bankList = array('' => '-Select Bank-') + $bankList;
                        $selectedBankName = '';
                        if (isset($_GET['bank_id']) && isset($bankList[$_GET['bank_id']])) {
                            $selectedBankName = $bankList[$_GET['bank_id']];
                        }
                        echo CHtml::dropDownList('bank_id', isset($_GET['bank_id']) ? $_GET['bank_id'] : '', $bankList, array(
                            'class' => 'form-control js-example-basic-single',
                            'style' => 'width:100%;',
                            'onchange' => 'getBankIdValue(this.value)',
                            'required' => 'required',
                        ));
                        ?>
                        <!-- </div> -->
                    </div>
                    <input type="hidden" name="bankid" id="bankid">
                    <input type="hidden" name="ledger" value="<?php echo isset($_GET['ledger']) ? $_GET['ledger'] : 0; ?>">
                    <div class="form-group col-xs-12 col-md-2 text-right text-sm-left">
                        <lable>&nbsp;</lable>
                        <div>
                            <?php echo CHtml::submitButton('GO', array('id' => 'btSubmit', 'class' => 'btn btn-sm btn-primary')); ?>
                            <?php echo CHtml::resetButton('Clear', array('onclick' => 'clearBankDropDown()', 'class' => 'btn btn-sm btn-default', )); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        <?php } ?>
    </div>
    <div id="contenthtml" class="contentdiv">
        <?php
        $heading = '';
        if (empty($date_from) && empty($date_to)) {
            $heading .= date('d-m-Y');
        } else {
            if (!empty($date_from) && !empty($date_to)) {
                $heading .= date('d-M-Y', strtotime($date_from)) . ' to ' . date('d-M-Y', strtotime($date_to));
            } else {
                if (!empty($_REQUEST['date_from'])) {
                    $heading .= date('d-M-Y', strtotime($date_from));
                }
                if (!empty($_REQUEST['date_to'])) {
                    $heading .= date('d-M-Y', strtotime($date_to));
                }
            }
        }
        $ledger_heading = '';
        if ($_GET['ledger'] == 1) {
            $ledger_heading = ' - Ledger View';
        }
        ?>
        <h4>Bank Transaction Report :
            <?php echo $heading . $ledger_heading; ?>
        </h4>
        <div id="parent">

            <?php
            $where = '';

            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
            }

            if (isset($_REQUEST["bank_id"]) && $_REQUEST["bank_id"] !== '') {
                $bankid = $_REQUEST["bank_id"];
                $sql = "SELECT * FROM `jp_cashbalance` "
                    . " WHERE bank_id=$bankid AND (" . $newQuery . ") ";
            } else {
                $sql = "SELECT * FROM `jp_cashbalance` "
                    . " WHERE bank_id IS NOT NULL AND (" . $newQuery . ") ";
            }

            $cash_blance = Yii::app()->db->createCommand($sql)->queryRow();
            //echo "<pre>"; print_r($sql);exit;
            
            if (empty($date_from) && empty($date_to)) {
                $where .= " AND (`date` >= '" . $cash_blance['cashbalance_date'] . "' and `date`<='" . date("Y-m-d") . "') ";
            } else {
                if (!empty($date_from) && !empty($date_to)) {
                    $where .= " AND (`date` >= '" . $date_from . "' and `date`<='" . $date_to . "') ";
                } else {
                    if (!empty($date_from)) {
                        $where .= " AND `date` >= '" . $date_from . "' ";
                    }
                    if (!empty($date_to)) {
                        $where .= $where .= " AND `date`<='" . $date_to . "' ";
                    }
                }
            }
            $bankQuery = "";
            if (isset($_REQUEST["bank_id"]) && $_REQUEST["bank_id"] !== '') {
                $bank_id = $_REQUEST["bank_id"];
                $bankQuery .= "AND bank_id=$bank_id ";
            }

            $data_array = array();
            $sql = "SELECT * FROM {$tblpx}expenses WHERE (" . $newQuery . ") " . $where . $bankQuery . " AND (expense_type=88 OR payment_type=88) AND subcontractor_id IS NULL AND reconciliation_status=1";  // OR expense_type=103              
            $daybook_data = Yii::app()->db->createCommand($sql)->queryAll();
            //echo "<pre>";print_r($daybook_data);exit;
            foreach ($daybook_data as $key => $value) {
                $data_array[$key]['payment_type'] = 'Daybook';
                $data_array[$key]['expense_amount'] = $value['paid'] - $value['expense_tds'];
                $data_array[$key]['receipt_amount'] = $value['receipt'];
                $data_array[$key]['payment_date'] = $value['date'];
                if ($value['bank_id'] != '') {
                    $bank_det = Bank::model()->findByPk($value['bank_id']);
                    if (!empty($bank_det)) {
                        $data_array[$key]['bank'] = $bank_det->bank_name;

                    }
                    $data_array[$key]['cheque_no'] = $value['cheque_no'];

                }

                if ($value['type'] == 73) {
                    $vendor = Vendors::model()->findByPk($value['vendor_id']);
                    $vendor_name = $vendor['name'];
                    if ($value['bill_id'] != NULL) {
                        $bills = Bills::model()->findByPk($value['bill_id']);
                        $expense_head = ExpenseType::model()->findByPk($value['exptype']);
                        $bill_thead = $bills['bill_number'] . ' / ' . $expense_head['type_name'];
                    } else {
                        $expense_head = ExpenseType::model()->findByPk($value['exptype']);
                        $bill_thead = $expense_head['type_name'];
                    }
                } else {
                    $vendor_name = '';
                    $bill_thead = '';
                }
                $data_array[$key]['vendor'] = $vendor_name;
                $data_array[$key]['description'] = $value['description'];
                $data_array[$key]['bill_exphead'] = $bill_thead;
                $data_array[$key]['company_id'] = $value['company_id'];
            }
            // echo "<pre>";print_r($data_array);exit;
            // dailyexpense
            
            $where_con = "";

            if (empty($date_from) && empty($date_to)) {
                $where_con .= " `date` >= '" . $cash_blance['cashbalance_date'] . "' and `date`<='" . date("Y-m-d") . "' AND ";
            } else {
                if (!empty($date_from) && !empty($date_to)) {
                    $where_con .= " `date` >= '" . $date_from . "' and `date`<='" . $date_to . "'AND ";
                } else {
                    if (!empty($date_from)) {
                        $where_con .= " `date` >= '" . $date_from . "' AND ";
                    }
                    if (!empty($date_to)) {
                        $where_con .= " `date`<='" . $date_to . "' AND ";
                    }
                }
            }

            // receipt
            $sql = "SELECT * FROM {$tblpx}dailyexpense "
                . " WHERE $where_con (dailyexpense_receipt_type=88 AND "
                . " dailyexpense_chequeno IS NOT NULL " . $bankQuery
                . " AND reconciliation_status = 1 "
                . " AND company_id=" . $cash_blance['company_id'] . ")  "
                . " OR (dailyexpense_receipt_type=88 "
                . " AND expensehead_type=4  AND "
                . " company_id=" . $cash_blance['company_id'] . ") AND "
                . " parent_status='1' AND "
                . " exp_type=72 ";

            if (isset($cash_blance['company_id'])) {
                $dailyexpense_data = Yii::app()->db->createCommand($sql)->queryAll();
                //echo "<pre>";print_r($dailyexpense_data);exit;
            } else {
                $dailyexpense_data = array();
            }

            $a = count($daybook_data);

            foreach ($dailyexpense_data as $key => $value) {
                $data_array[$a]['payment_type'] = 'Dailyexpense';
                $data_array[$a]['expense_amount'] = $value['dailyexpense_paidamount'];
                $data_array[$a]['receipt_amount'] = $value['dailyexpense_receipt'];
                $data_array[$a]['payment_date'] = $value['date'];
                if ($value['bank_id'] != '') {
                    $bank_det = Bank::model()->findByPk($value['bank_id']);
                    if (!empty($bank_det)) {
                        $data_array[$a]['bank'] = $bank_det->bank_name;

                    }
                    $data_array[$a]['cheque_no'] = $value['dailyexpense_chequeno'];

                }
                if ($value["dailyexpense_type"] == "deposit") {
                    $depositId = $value["expensehead_id"];
                    $deposit = Deposit::model()->findByPk($depositId);
                    $expense_head = $deposit->deposit_name;
                } else if ($value["dailyexpense_type"] == "expense") {
                    $expense = Companyexpensetype::model()->findByPk($value['expensehead_id']);
                    $expense_head = isset($expense->name) ? $expense->name : "";
                } else {
                    $receipt = Companyexpensetype::model()->findByPk($value['exp_type_id']);
                    $expense_head = isset($receipt->name) ? $receipt->name : "";
                }
                if ($value['bill_id'] != NULL) {
                    $bill_thead = $value['bill_id'] . ' / ' . $expense_head;
                } else {
                    $bill_thead = $expense_head;
                }

                $data_array[$a]['vendor'] = '';
                $data_array[$a]['description'] = $value['description'];
                $data_array[$a]['bill_exphead'] = $bill_thead;
                $data_array[$a]['company_id'] = $value['company_id'];
                $a++;
            }

            //expense
            $sql = "SELECT * FROM {$tblpx}dailyexpense "
                . " WHERE $where_con (expense_type=88) AND exp_type=73 "
                . " AND parent_status='1' "
                . $bankQuery
                . " AND dailyexpense_chequeno IS NOT NULL "
                . " AND reconciliation_status = 1 "
                . " AND company_id=" . $cash_blance['company_id']
                . " $bankQuery";
            if (isset($cash_blance['company_id'])) {
                $dailyexpense_data1 = Yii::app()->db->createCommand($sql)->queryAll();
            } else {
                $dailyexpense_data1 = array();
            }

            $a1 = count($daybook_data) + count($dailyexpense_data);
            //echo "<pre>";print_r($dailyexpense_data1);exit;
            foreach ($dailyexpense_data1 as $key => $value) {
                $data_array[$a1]['payment_type'] = 'Dailyexpense';
                $data_array[$a1]['expense_amount'] = $value['dailyexpense_paidamount'];
                $data_array[$a1]['receipt_amount'] = $value['dailyexpense_receipt'];
                $data_array[$a1]['payment_date'] = $value['date'];
                if ($value["dailyexpense_type"] == "deposit") {
                    $depositId = $value["expensehead_id"];
                    $deposit = Deposit::model()->findByPk($depositId);
                    $expense_head = $deposit->deposit_name;
                } else if ($value["dailyexpense_type"] == "expense") {
                    $expense = Companyexpensetype::model()->findByPk($value['expensehead_id']);
                    $expense_head = isset($expense->name) ? $expense->name : "";
                } else {
                    $receipt = Companyexpensetype::model()->findByPk($value['exp_type_id']);
                    $expense_head = isset($receipt->name) ? $receipt->name : "";
                }
                if ($value['bank_id'] != '') {
                    $bank_det = Bank::model()->findByPk($value['bank_id']);
                    if (!empty($bank_det)) {
                        $data_array[$a1]['bank'] = $bank_det->bank_name;

                    }
                    $data_array[$a1]['cheque_no'] = $value['dailyexpense_chequeno'];

                }
                if ($value['bill_id'] != NULL) {
                    $bill_thead = $value['bill_id'] . ' / ' . $expense_head;
                } else {
                    $bill_thead = $expense_head;
                }

                $data_array[$a1]['vendor'] = '';
                $data_array[$a1]['description'] = $value['description'];
                $data_array[$a1]['bill_exphead'] = $bill_thead;
                $data_array[$a1]['company_id'] = $value['company_id'];
                $a1++;
            }


            // Vendor payment
            $bankQuery1 = "";
            if (isset($_REQUEST["bank_id"]) && $_REQUEST["bank_id"] !== '') {
                $bank_id = $_REQUEST["bank_id"];
                $bankQuery1 .= "AND bank=$bank_id";
            }

            $vendorpayment_data = array();
            $sql = "SELECT * FROM {$tblpx}dailyvendors 
                WHERE (" . $newQuery . ") " . $where . $bankQuery1 . " 
                AND payment_type=88 AND reconciliation_status = 1";

            $vendorpayment_data = Yii::app()->db->createCommand($sql)->queryAll();
            // echo "<pre>";
            // print_r($vendorpayment_data);exit;
            $b = count($daybook_data) + count($dailyexpense_data) + count($dailyexpense_data1);
            foreach ($vendorpayment_data as $key => $value) {
                $data_array[$b]['payment_type'] = 'Vendor  Payment';
                $data_array[$b]['expense_amount'] = ($value['amount'] + $value['tax_amount']) - $value['tds_amount'];
                $data_array[$b]['receipt_amount'] = '';
                $data_array[$b]['payment_date'] = $value['date'];
                $vendor = Vendors::model()->findByPk($value['vendor_id']);
                $vendor_name = isset($vendor->name) ? $vendor->name : '';
                $data_array[$b]['vendor'] = $vendor_name;
                $data_array[$b]['description'] = $value['description'];
                $data_array[$b]['bill_exphead'] = '';
                $data_array[$b]['company_id'] = $value['company_id'];
                if ($value['bank'] != '') {
                    $bank_det = Bank::model()->findByPk($value['bank']);
                    if (!empty($bank_det)) {
                        $data_array[$b]['bank'] = $bank_det->bank_name;

                    }
                    $data_array[$b]['cheque_no'] = $value['cheque_no'];

                }
                $b++;
            }

            // Subcontractor payment
            $subcontractorpayment_data = array();
            $sql = "SELECT * FROM {$tblpx}subcontractor_payment 
                        WHERE (" . $newQuery . ") " . $where . " AND payment_type=88 
                        AND approve_status ='Yes' AND bank IS NOT NULL AND cheque_no IS NOT NULL " . $bankQuery1 . " AND reconciliation_status = 1";

            $subcontractorpayment_data = Yii::app()->db->createCommand($sql)->queryAll();
            $c = count($daybook_data) + count($dailyexpense_data) + count($dailyexpense_data1) + count($vendorpayment_data);
            //echo "<pre>";print_r($subcontractorpayment_data);exit;
            foreach ($subcontractorpayment_data as $key => $value) {
                $subcontactor_det = Subcontractor::model()->findByPk($value['subcontractor_id']);
                //echo "<pre>";print_r($subcontactor_det);exit;
                $data_array[$c]['payment_type'] = 'Subcontractor  Payment';
                $data_array[$c]['expense_amount'] = ($value['amount'] + $value['tax_amount']) - $value['tds_amount'];
                $data_array[$c]['receipt_amount'] = '';
                $data_array[$c]['payment_date'] = $value['date'];
                $data_array[$c]['vendor'] = $subcontactor_det['subcontractor_name'];
                $data_array[$c]['description'] = $value['description'];
                $data_array[$c]['bill_exphead'] = '';
                $data_array[$c]['company_id'] = $value['company_id'];
                if ($value['bank'] != '') {
                    $bank_det = Bank::model()->findByPk($value['bank']);
                    if (!empty($bank_det)) {
                        $data_array[$c]['bank'] = $bank_det->bank_name;
                    }
                    $data_array[$c]['cheque_no'] = $value['cheque_no'];
                }
                $c++;
            }
            //echo "<pre>";print_r($data_array);exit;
            $buyer_module = GeneralSettings::model()->checkBuyerModule();
            if ($buyer_module) {
                $buyer_transactions = BuyerTransactions::model()->findAll(array('condition' => '(' . $newQuery . ') ' . $where));
                $buyer_transactions_cash = BuyerTransactions::model()->findAll(array('condition' => '(' . $newQuery . ') ' . $where . ' AND transaction_type = 89'));
                $buyer_transactions_bank = BuyerTransactions::model()->findAll(array('condition' => '(' . $newQuery . ') ' . $where . $bankQuery . ' AND transaction_type = 88 AND reconciliation_status=1'));
                foreach ($buyer_transactions_bank as $key => $value) {
                    $data_array[$c]['payment_type'] = 'Buyer Transactions';
                    if ($value['transaction_for'] == 1) {
                        $data_array[$c]['receipt_amount'] = $value['total_amount'];
                        $data_array[$c]['expense_amount'] = 0;
                    } elseif ($value['transaction_for'] == 2) {
                        $data_array[$c]['receipt_amount'] = $value['total_amount'];
                        $data_array[$c]['expense_amount'] = 0;
                    } elseif ($value['transaction_for'] == 3) {
                        $data_array[$c]['expense_amount'] = $value['total_amount'];
                        $data_array[$c]['receipt_amount'] = 0;
                    } else {
                        $data_array[$c]['expense_amount'] = 0;
                        $data_array[$c]['receipt_amount'] = 0;
                    }
                    if ($value['bank_id'] != '') {
                        $bank_det = Bank::model()->findByPk($value['bank_id']);
                        if (!empty($bank_det)) {
                            $data_array[$c]['bank'] = $bank_det->bank_name;

                        }
                        $data_array[$c]['cheque_no'] = $value['cheque_no'];

                    }
                    $data_array[$c]['payment_date'] = $value['date'];
                    $data_array[$c]['vendor'] = '';
                    $data_array[$c]['description'] = $value['description'];
                    $data_array[$c]['bill_exphead'] = $value['transaction_no'];
                    $data_array[$c]['company_id'] = $value['company_id'];
                    $c++;
                }
            }

            $total_expense = 0;
            $total_recept = 0;
            if (!empty($data_array)) {
                $total_expense = array_sum(array_column($data_array, 'expense_amount'));
                $total_recept = array_sum(array_column($data_array, 'receipt_amount'));
            }
            $date = array_column($data_array, 'payment_date');
            array_multisort($date, SORT_DESC, $data_array);
            ?>
            <?php
            if ($_GET['ledger'] == 0) {
                ?>
                <div class="report-table-wrapper">
                    <table class="table total-table " id="fixTable">
                        <thead class="entry-table">
                            <tr>
                                <th>Sl No.</th>
                                <th>Payment Name</th>
                                <th>Company</th>
                                <th>Bank</th>
                                <th>Cheque</th>
                                <th>Bill No. / Transaction head</th>
                                <th>Vendors</th>
                                <th>Description</th>
                                <th>Payment date</th>
                                <th>Expense Amount</th>
                                <th>Receipt Amounts</th>
                            </tr>
                            <tr>
                                <th colspan="9" class="text-right fw-bold"><b>Total</b></th>
                                <th class="text-right">
                                    <?php echo Controller::money_format_inr($total_expense, 2); ?>
                                </th>
                                <th class="text-right">
                                    <?php echo Controller::money_format_inr($total_recept, 2); ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($data_array)) {
                                foreach ($data_array as $key => $data) {
                                    // $total_expense += $data['expense_amount'];
                                    // $total_recept += floatval($data['receipt_amount']);
                        
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $key + 1 ?>
                                        </td>
                                        <td>
                                            <?php echo $data['payment_type']; ?>
                                        </td>
                                        <td>
                                            <?php
                                            $company = Company::model()->findByPk($data['company_id']);
                                            echo $company->name;
                                            ?>
                                        </td>
                                        <td>
                                            <?php echo $data['bank'] ?>
                                        </td>
                                        <td>
                                            <?php echo $data['cheque_no'] ?>
                                        </td>
                                        <td>
                                            <?php echo $data['bill_exphead']; ?>
                                        </td>
                                        <td>
                                            <?php echo $data['vendor']; ?>
                                        </td>
                                        <td>
                                            <?php echo $data['description']; ?>
                                        </td>
                                        <td>
                                            <?php echo date('d-m-Y', strtotime($data['payment_date'])); ?>
                                        </td>
                                        <td align="right">
                                            <?php echo ($data['expense_amount'] != 0) ? Controller::money_format_inr($data['expense_amount'], 2) : '0'; ?>
                                        </td>
                                        <td align="right">
                                            <?php echo ($data['receipt_amount'] != 0) ? Controller::money_format_inr($data['receipt_amount'], 2) : '0'; ?>
                                        </td>
                                    </tr>

                                <?php } ?>
                            </tbody>
                        <?php } else { ?>
                            <tr>
                                <td colspan="9" class="text-center">No data found</td>
                            </tr>
                        <?php }
                            ?>
                    </table>
                </div>
                <div class="summarytext">
                    Total
                    <?php echo count($data_array); ?> results
                </div>
            <?php } else {

                ?>

                <div>
                    <?php
                    $this->renderPartial(
                        '_banktransaction_ledger_view',
                        array(
                            'date_to' => $date_to,
                            'date_from' => $date_from,
                            'bank_id' => $bank_id,
                            'data_array' => $data_array
                        )
                    );
                    ?>
                </div>
            <?php } ?>
        </div>
        <?php
        if ($date_from != '' || $date_to != '') {
            $newQuery = "";
            $newQuery2 = "";
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}cashbalance.company_id)";
            }
            $heading = '';
            if (!empty($date_from) && !empty($date_to)) {
                $heading .= date('d-M-Y', strtotime($date_from)) . ' to ' . date('d-M-Y', strtotime($date_to));
            } else {
                if (!empty($_REQUEST['date_from'])) {
                    $heading .= date('d-M-Y', strtotime($date_from));
                }
                if (!empty($_REQUEST['date_to'])) {
                    $heading .= date('d-M-Y', strtotime($date_to));
                }
            }

            $sql = "SELECT {$tblpx}status.caption,{$tblpx}cashbalance.cashbalance_type,{$tblpx}cashbalance.bank_id,{$tblpx}bank.bank_name,{$tblpx}cashbalance.cashbalance_opening_balance,{$tblpx}cashbalance.cashbalance_deposit,{$tblpx}cashbalance.cashbalance_withdrawal,{$tblpx}cashbalance.cashbalance_closing_balance,{$tblpx}cashbalance.cashbalance_date,{$tblpx}cashbalance.company_id FROM {$tblpx}cashbalance LEFT JOIN {$tblpx}bank ON {$tblpx}cashbalance.bank_id={$tblpx}bank.bank_id LEFT JOIN {$tblpx}status ON {$tblpx}cashbalance.cashbalance_type={$tblpx}status.sid WHERE (" . $newQuery . ") AND {$tblpx}cashbalance.cashbalance_type = 99 AND {$tblpx}cashbalance.bank_id IS NOT NULL ORDER BY bank_id ASC";
            // echo $sql;die;
            if (isset($_REQUEST["bank_id"]) && $_REQUEST["bank_id"] !== '') {
                $bank_id = $_REQUEST["bank_id"];
                $sql = "SELECT {$tblpx}status.caption,{$tblpx}cashbalance.cashbalance_type,{$tblpx}cashbalance.bank_id,{$tblpx}bank.bank_name,{$tblpx}cashbalance.cashbalance_opening_balance,{$tblpx}cashbalance.cashbalance_deposit,{$tblpx}cashbalance.cashbalance_withdrawal,{$tblpx}cashbalance.cashbalance_closing_balance,{$tblpx}cashbalance.cashbalance_date,{$tblpx}cashbalance.company_id FROM {$tblpx}cashbalance LEFT JOIN {$tblpx}bank ON {$tblpx}cashbalance.bank_id={$tblpx}bank.bank_id LEFT JOIN {$tblpx}status ON {$tblpx}cashbalance.cashbalance_type={$tblpx}status.sid WHERE (" . $newQuery . ") AND {$tblpx}cashbalance.cashbalance_type = 99 AND {$tblpx}cashbalance.bank_id IS NOT NULL AND {$tblpx}cashbalance.bank_id =$bank_id ORDER BY bank_id ASC";

            }
            $cash_blance = Yii::app()->db->createCommand($sql)->queryAll();
            if (!empty($cash_blance)) {
                // echo "<pre>";print_r($cash_blance);exit;
                ?>
                <?php
                if ($_GET['ledger'] == 0) {
                    ?>
                    <h4>Balance Report :
                        <?php echo $heading; ?>
                    </h4>
                    <div class="clearfix">
                        <div class="pull-right"><span>Total</span> <?php echo count($cash_blance); ?> <span>results</span></div>
                    </div>
                    <div id="parent2">
                        <table class="table table-bordered " id="fixTable2">
                            <thead>
                                <tr>
                                    <th>Sl No.</th>
                                    <th>Company</th>
                                    <th>Cash Balance Type</th>
                                    <th>Bank</th>
                                    <th>Start Date</th>
                                    <th>Opening Balance</th>
                                    <th>Receipt</th>
                                    <th>Expense</th>
                                    <th>Closing Balance</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($cash_blance as $key => $value) {

                                    $tblpx = Yii::app()->db->tablePrefix;
                                    $where_bank = '';
                                    $where_hand = '';

                                    $opening_hand = '';
                                    $opening_bank = '';
                                    $requested_bank_id = '';
                                    if (isset($_REQUEST["bank_id"]) && $_REQUEST["bank_id"] !== '') {
                                        $requested_bank_id = $_REQUEST["bank_id"];
                                    }
                                    if (!empty($date_from) && !empty($date_to)) {
                                        $opening_date = date('Y-m-d', strtotime('-1 day', strtotime($date_from)));

                                        $where_bank .= " AND date between '" . $date_from . "' and '" . $date_to . "'";
                                        $where_hand .= " `date` between '" . $date_from . "' and '" . $date_to . "' AND ";
                                        $opening_hand .= " AND date <= '" . $opening_date . "'";
                                        $opening_bank .= " AND date <= '" . $opening_date . "'";
                                    } else {
                                        if (!empty($_REQUEST['date_from'])) {
                                            $opening_date = date('Y-m-d', strtotime('-1 day', strtotime($_REQUEST['date_from'])));
                                            $date_from = date('Y-m-d', strtotime($_REQUEST['date_from']));

                                            $where_bank .= " AND date between '" . $date_from . "' and '" . $current_date . "'";
                                            $where_hand .= " `date` between '" . $date_from . "' and '" . $current_date . "' AND ";
                                            $opening_hand .= " AND date <= '" . $opening_date . "'";
                                            $opening_bank .= " AND date <= '" . $opening_date . "'";
                                        }
                                        if (!empty($_REQUEST['date_to'])) {
                                            $opening_date = $value['cashbalance_date'];
                                            $date_to = date('Y-m-d', strtotime($_REQUEST['date_to']));

                                            $where_bank .= " AND date between '" . $value['cashbalance_date'] . "' and '" . $date_to . "'";
                                            $where_hand .= " `date` between '" . $value['cashbalance_date'] . "' and '" . $date_to . "' AND ";
                                            $opening_hand .= " AND date <= '" . $opening_date . "'";
                                            $opening_bank .= " AND date <= '" . $opening_date . "'";
                                        }
                                    }



                                    if ($value['bank_id'] != '') {
                                        // deposit
                                        $reconcil = "reconciliation_status = 1 " . $where_bank;
                                        $unreconcil = "reconciliation_status = 0  "
                                            . " AND date IS NULL";

                                        // opening balance calculation
                                        // deposit
                                        $sql = "SELECT SUM(receipt) as total_amount "
                                            . " FROM {$tblpx}expenses "
                                            . " WHERE reconciliation_status = 1 " . $opening_bank
                                            . " AND payment_type=88 AND bank_id=" . $value['bank_id']
                                            . " AND type=72 AND company_id=" . $value['company_id'];
                                        $daybook_opening = Yii::app()->db->createCommand($sql)->queryRow();

                                        $sql = "SELECT SUM(dailyexpense_receipt) as total_amount "
                                            . " FROM {$tblpx}dailyexpense "
                                            . " WHERE reconciliation_status = 1 " . $opening_bank
                                            . " AND dailyexpense_receipt_type=88 "
                                            . " AND bank_id=" . $value['bank_id']
                                            . " AND exp_type=72 AND company_id=" . $value['company_id'];
                                        $dailyexpense_opening = Yii::app()->db->createCommand($sql)->queryRow();

                                        $daybook_check_opening = $daybook_opening['total_amount'];
                                        $dailyexpense_check_opening = $dailyexpense_opening['total_amount'];
                                        $deposit_opening = $daybook_check_opening + $dailyexpense_check_opening;


                                        //withdrawal
                                        $sql = "SELECT  SUM((IFNULL(paid, 0))  - IFNULL(expense_tds, 0)) as total_amount "
                                            . " FROM {$tblpx}expenses "
                                            . " WHERE reconciliation_status = 1 " . $opening_bank
                                            . " AND expense_type=88 AND bank_id=" . $value['bank_id']
                                            . " AND type=73 AND company_id=" . $value['company_id'];
                                        $daybook_with_opening = Yii::app()->db->createCommand($sql)->queryRow();

                                        $sql = "SELECT SUM(dailyexpense_paidamount) as total_amount "
                                            . " FROM {$tblpx}dailyexpense "
                                            . " WHERE reconciliation_status = 1 " . $opening_bank
                                            . " AND expense_type=88 AND bank_id=" . $value['bank_id']
                                            . " AND exp_type=73 AND company_id=" . $value['company_id'];
                                        $dailyexpense_with_opening = Yii::app()->db->createCommand($sql)->queryRow();

                                        $daybook_with_amount_opening = $daybook_with_opening['total_amount'];
                                        $dailyexpense_with_amount_opening = $dailyexpense_with_opening['total_amount'];

                                        $sql = "SELECT SUM( (IFNULL(amount, 0) + IFNULL(tax_amount, 0))- IFNULL(tds_amount,0) ) "
                                            . " as total_amount FROM {$tblpx}dailyvendors "
                                            . " WHERE reconciliation_status = 1 " . $opening_bank
                                            . " AND payment_type=88  AND bank=" . $value['bank_id']
                                            . " AND company_id=" . $value['company_id'];
                                        $dailyvendors_opening = Yii::app()->db->createCommand($sql)->queryRow();

                                        $dailyvendors_check_opening = $dailyvendors_opening['total_amount'];

                                        $withdrawal_opening = $dailyvendors_check_opening + $daybook_with_amount_opening + $dailyexpense_with_amount_opening;

                                        // $opening_balance = ($deposit_opening + $value['cashbalance_opening_balance']) - $withdrawal_opening;
                                        $opening_date = date('Y-m-d', strtotime('-1 day', strtotime($current_date)));
                                        $opening_bank = " AND date <= '" . $opening_date . "'";

                                        $opening_balance = floatval($value['cashbalance_opening_balance']);
                                        $where_con = '';
                                        $where = '';
                                        if (isset($_REQUEST['date_from']) && ($_REQUEST['date_from']) != '' && isset($_REQUEST["bank_id"]) && $_REQUEST["bank_id"] !== '') {
                                            $newQuery = "";
                                            $user = Users::model()->findByPk(Yii::app()->user->id);
                                            $arrVal = explode(',', $user->company_id);
                                            foreach ($arrVal as $arr) {
                                                if ($newQuery)
                                                    $newQuery .= ' OR';
                                                $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
                                            }
                                            $where_con .= " `date` >= '" . $value['cashbalance_date'] . "' and `date`<'" . $date_from . "' AND ";
                                            $where .= " AND (`date` >= '" . $value['cashbalance_date'] . "' and `date`<'" . $date_from . "') ";
                                            $bankQuery = "";
                                            if (isset($_REQUEST["bank_id"]) && $_REQUEST["bank_id"] !== '') {
                                                $bank_id = $_REQUEST["bank_id"];
                                                $bankQuery .= "AND bank_id=$bank_id";
                                            }
                                            $bankQuery1 = "";
                                            if (isset($_REQUEST["bank_id"]) && $_REQUEST["bank_id"] !== '') {
                                                $bank_id = $_REQUEST["bank_id"];
                                                $bankQuery1 .= "AND bank=$bank_id";
                                            }
                                            $sql = "SELECT SUM((IFNULL(paid, 0))  - IFNULL(expense_tds, 0)) AS paid FROM {$tblpx}expenses WHERE (" . $newQuery . ") " . $where . $bankQuery . " AND (expense_type=88 OR payment_type=88) AND subcontractor_id IS NULL  AND reconciliation_status=1";  // OR expense_type=103              
                                            $daybook_exp_total = Yii::app()->db->createCommand($sql)->queryRow();

                                            $expense_total = 0;
                                            if (!empty($daybook_exp_total)) {
                                                $expense_total += $daybook_exp_total["paid"];
                                            }
                                            $sql = "SELECT SUM(IFNULL(receipt,0)) AS receipt FROM {$tblpx}expenses WHERE (" . $newQuery . ") " . $where . $bankQuery . " AND (expense_type=88 OR payment_type=88) AND subcontractor_id IS NULL  AND reconciliation_status=1";  // OR expense_type=103 
                    
                                            $daybook_recpt_total = Yii::app()->db->createCommand($sql)->queryRow();

                                            $receipt_total = 0;
                                            if (!empty($daybook_recpt_total)) {
                                                $receipt_total += $daybook_recpt_total["receipt"];
                                            }
                                            //echo $receipt_total;exit;
                                            // receipt
                                            $sql = "SELECT SUM( IFNULL(dailyexpense_paidamount,0)) AS paid FROM {$tblpx}dailyexpense "
                                                . " WHERE $where_con (dailyexpense_receipt_type=88 AND "
                                                . " dailyexpense_chequeno IS NOT NULL " . $bankQuery
                                                . " AND reconciliation_status = 1 "
                                                . " AND company_id=" . $value['company_id'] . ")  "
                                                . " OR (dailyexpense_receipt_type=88 "
                                                . " AND expensehead_type=4  AND "
                                                . " company_id=" . $value['company_id'] . ") AND "
                                                . " parent_status='1' AND "
                                                . " exp_type=72 ";
                                            $dailyexp_expense_total = Yii::app()->db->createCommand($sql)->queryRow();
                                            if (!empty($dailyexp_expense_total)) {
                                                $expense_total += $dailyexp_expense_total["paid"];
                                            }

                                            //echo $expense_total;exit;
                                            $sql = "SELECT SUM( IFNULL(dailyexpense_receipt,0)) AS receipt FROM {$tblpx}dailyexpense "
                                                . " WHERE $where_con (dailyexpense_receipt_type=88 AND "
                                                . " dailyexpense_chequeno IS NOT NULL " . $bankQuery
                                                . " AND reconciliation_status = 1 "
                                                . " AND company_id=" . $value['company_id'] . ")  "
                                                . " OR (dailyexpense_receipt_type=88 "
                                                . " AND expensehead_type=4  AND "
                                                . " company_id=" . $value['company_id'] . ") AND "
                                                . " parent_status='1' AND "
                                                . " exp_type=72 ";
                                            $dailyexp_receipt_total = Yii::app()->db->createCommand($sql)->queryRow();
                                            if (!empty($dailyexp_receipt_total)) {
                                                $receipt_total += $dailyexp_receipt_total["receipt"];
                                            }

                                            $sql = "SELECT SUM( IFNULL(dailyexpense_paidamount,0)) AS paid FROM {$tblpx}dailyexpense "
                                                . " WHERE $where_con (expense_type=88) AND exp_type=73 "
                                                . " AND parent_status='1' "
                                                . $bankQuery
                                                . " AND dailyexpense_chequeno IS NOT NULL "
                                                . " AND reconciliation_status = 1 "
                                                . " AND company_id=" . $value['company_id']
                                                . " $bankQuery";

                                            $dailyexp_one_expense_total = '';
                                            if (isset($value['company_id'])) {
                                                $dailyexp_one_expense_total = Yii::app()->db->createCommand($sql)->queryRow();
                                            }

                                            if (!empty($dailyexp_one_expense_total)) {
                                                $expense_total += $dailyexp_one_expense_total["paid"];
                                            }

                                            $sql = "SELECT SUM( IFNULL(dailyexpense_receipt,0)) AS receipt FROM {$tblpx}dailyexpense "
                                                . " WHERE $where_con (expense_type=88) AND exp_type=73 "
                                                . " AND parent_status='1' "
                                                . $bankQuery
                                                . " AND dailyexpense_chequeno IS NOT NULL "
                                                . " AND reconciliation_status = 1 "
                                                . " AND company_id=" . $value['company_id']
                                                . " $bankQuery";
                                            $dailyexp_one_receipt_total = '';
                                            if (isset($value['company_id'])) {
                                                $dailyexp_one_receipt_total = Yii::app()->db->createCommand($sql)->queryRow();
                                            }

                                            if (!empty($dailyexp_one_receipt_total)) {
                                                $receipt_total += $dailyexp_one_receipt_total["receipt"];
                                            }
                                            //echo $receipt_total;exit;
                                            $sql = "SELECT SUM((IFNULL(amount, 0) + IFNULL(tax_amount, 0))- IFNULL(tds_amount,0)) AS expense FROM {$tblpx}dailyvendors 
                                                WHERE (" . $newQuery . ") " . $where . $bankQuery1 . " 
                                                AND payment_type=88 AND reconciliation_status = 1";
                                            $vendor_expense_total = '';

                                            if (isset($value['company_id'])) {
                                                $vendor_expense_total = Yii::app()->db->createCommand($sql)->queryRow();
                                            }
                                            //print_r($vendor_expense_total); exit;
                                            if (!empty($vendor_expense_total)) {
                                                $expense_total += $vendor_expense_total["expense"];
                                            }
                                            $sql = "SELECT SUM((IFNULL(amount, 0) + IFNULL(tax_amount, 0))- IFNULL(tds_amount,0)) AS expense FROM {$tblpx}subcontractor_payment 
                                                WHERE (" . $newQuery . ") " . $where . " AND payment_type=88 
                                                AND approve_status ='Yes' AND bank IS NOT NULL 
                                                AND cheque_no IS NOT NULL " . $bankQuery1 . " 
                                                AND reconciliation_status = 1";
                                            $subcontractor_expense_total = '';

                                            if (isset($value['company_id'])) {
                                                $subcontractor_expense_total = Yii::app()->db->createCommand($sql)->queryRow();
                                            }
                                            if (!empty($subcontractor_expense_total)) {
                                                $expense_total += $subcontractor_expense_total["expense"];
                                            }
                                            $opening_balance += $receipt_total;
                                            $opening_balance -= $expense_total;

                                        }
                                    }
                                    if (!empty($data_array)) {
                                        $total_expense = array_sum(array_column($data_array, 'expense_amount'));
                                        $total_recept = array_sum(array_column($data_array, 'receipt_amount'));

                                    }

                                    ?>

                                    <tr>
                                        <td>
                                            <?php echo $key + 1; ?>
                                        </td>
                                        <td>
                                            <?php
                                            $company = Company::model()->findByPk($value['company_id']);
                                            echo $company->name;
                                            ?>
                                        </td>
                                        <td>
                                            <?php echo $value['caption']; ?>
                                        </td>
                                        <td>
                                            <?php echo $value['bank_name']; ?>
                                        </td>
                                        <td>
                                            <?php echo $value['cashbalance_date']; ?>
                                        </td>
                                        <td align="right">
                                            <?php echo Controller::money_format_inr($opening_balance, 2); ?>
                                        </td>
                                        <td align="right">
                                            <?php echo Controller::money_format_inr($total_recept, 2); ?>
                                        </td>
                                        <td align="right">
                                            <?php echo Controller::money_format_inr($total_expense, 2); ?>
                                        </td>

                                        <td align="right">
                                            <?php echo Controller::money_format_inr((($opening_balance + $total_recept) - $total_expense), 2); ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>

                        </table>
                    </div>

                    <?php
                }
            } else {

                echo "No data found";
            }
        }
        ?>


    </div>



</div>
<style>
    #parent {
        max-height: 300px;
        margin-bottom: 20px;
    }

    

    .contentdiv {
        position: relative;
    }

    .summarytext {
        position: absolute;
        right: 3px;
        top: 15px;
    }
</style>
<script>
    $(document).ready(function () {
        $("#fixTable").tableHeadFixer({
            'left': false,
            'foot': true,
            'head': true
        });
        $("#fixTable2").tableHeadFixer({
            'left': false,
            'foot': true,
            'head': true
        });
    });
    $(function () {
        $("#date_from").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: $("#date_to").val()
        });
        $("#date_to").datepicker({
            dateFormat: 'yy-mm-dd',
        });
        $("#date_from").change(function () {
            $("#date_to").datepicker('option', 'minDate', $(this).val());
        });
        $("#date_to").change(function () {
            $("#date_from").datepicker('option', 'maxDate', $(this).val());
        });
    });
    function clearBankDropDown() {
        document.getElementById('bank_id').selectedIndex = 0;
        window.location.href = "<?php echo $this->createUrl('bank/banktransaction_report&ledger=0'); ?>";
    }
    function getBankIdValue(bankno) {
        document.getElementById('bankid').value = '';
        if (bankno)
            document.getElementById('bankid').value = bankno;
    }
</script>