<?php
$tblpx = Yii::app()->db->tablePrefix;
$current_date = date("Y-m-d");
$opening_date = date('Y-m-d', strtotime('-1 day', strtotime($current_date)));
$opening_date = '2000-01-01';
?>
<table class="table table-bordered ">

    <thead>
        <tr>
            <th>Sl No.</th>
            <th>Company</th>
            <th>Cash Balance Type</th>
            <th>Bank</th>
            <th>Start Date</th>
            <th>Opening Balance</th>
            <th>Deposit</th>
            <th>Withdrawal</th>
            <th>Closing Balance</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $newQuery = "";
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', c.company_id)";
        }
        $sql = "SELECT s.caption,c.*,b.bank_name"
                . " FROM {$tblpx}cashbalance  c "
                . " LEFT JOIN {$tblpx}bank b"
                . " ON c.bank_id=b.bank_id "
                . " LEFT JOIN {$tblpx}status s ON c.cashbalance_type=s.sid "
                . " WHERE (" . $newQuery . ") "
                . " ORDER BY bank_id ASC";

        $cash_blance = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($cash_blance as $key => $value) {

            if ($value['bank_id'] != '') {
                $opening_bank = " AND reconciliation_date <= '" . $opening_date . "'";

                $reconcil_condition = "reconciliation_status = 1 AND date "
                        . " BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "'";
                $unreconcil_condition = "(reconciliation_status = 0 OR reconciliation_status IS NULL) AND reconciliation_date IS NULL";

//bank deposit
                $bdeposit = CashBalanceHelper::bankDeposit($reconcil_condition, $unreconcil_condition, $value['bank_id'], $value['company_id']);
                $deposit = $bdeposit['daybook'] + $bdeposit['dailyexpense'];

//bank withdraw
                $bwithdraw = CashBalanceHelper::bankWithdrawal($reconcil_condition, $unreconcil_condition, $value['bank_id'], $value['company_id']);
                $subcontractor_check = $bwithdraw['subcontractor'];
                $withdrawal = $bwithdraw['dailyvendors'] + $bwithdraw['daybook_with'] + $bwithdraw['dailyexpense_with'];

//deposit opening               
                $bopendeposit = CashBalanceHelper::depositOpening($opening_bank, $value['bank_id'], $value['company_id']);
                $deposit_opening = $bopendeposit['daybook_check_opening'] + $bopendeposit['dailyexpense_check_opening'];

//withdraw opening
                $bopenwithdraw = CashBalanceHelper::withdrawOpening($opening_bank, $value['bank_id'], $value['company_id']);
                $withdrawal_opening = $bopenwithdraw['dailyvendors_check_opening'] + $bopenwithdraw['daybook_with_amount_opening'] +
                        $bopenwithdraw['dailyexpense_with_amount_opening'];

                $opening_balance = ($deposit_opening + $value['cashbalance_opening_balance']) - $withdrawal_opening;
            } else {
                $opening_hand = " AND date <= '" . $opening_date . "'";

//cash deposit
                $cdeposit = CashBalanceHelper::cashDeposit($value['cashbalance_date'], $current_date, $value['company_id']);
                $deposit = $cdeposit['daybook_cash'] + $cdeposit['dailyexpense_cash'];

//cash withdraw                
                $cwithdraw = CashBalanceHelper::cashWithdrawal($value['cashbalance_date'], $current_date, $value['company_id']);
                $withdrawal = $cwithdraw['dailyvendors_cash'] + $cwithdraw['daybook_with_amount'] + $cwithdraw['dailyexpense_with_amount'];

//deposit opening               
                $copendeposit = CashBalanceHelper::cdepositOpening($opening_hand, $value['company_id']);
                $deposit_opening = $copendeposit['daybook_cash_opening'] + $copendeposit['dailyexpense_cash_opening'];

//withdrwa opening               
                $copenwithdraw = CashBalanceHelper::cwithdrawOpening($opening_hand, $value['company_id']);
                $withdrawal_opening = floatval($copenwithdraw['dailyvendors_cash_opening']) + floatval($copenwithdraw['dailyexpense_with_amount_opening']) + floatval($copenwithdraw['daybook_with_amount_opening']);

                $opening_balance = ($deposit_opening + floatval($value['cashbalance_opening_balance'])) - $withdrawal_opening;
//buyer module
                $buyer_module = GeneralSettings::model()->checkBuyerModule();
                if ($buyer_module) {
                    $buyer_transactions_cash = BuyerTransactions::model()->findAll(array('condition' => 'transaction_type = 89'));
                    $buyer_transactions_bank = BuyerTransactions::model()->findAll(array('condition' => 'transaction_type = 88'));
                    foreach ($buyer_transactions_cash as $buyer_transaction) {
                        if ($buyer_transaction['transaction_for'] == 3) {
                            $withdrawal += $buyer_transaction['total_amount'];
                        } elseif ($buyer_transaction['transaction_for'] == 1) {
                            $deposit += $buyer_transaction['total_amount'];
                        }
                    }
                    foreach ($buyer_transactions_bank as $buyer_transaction) {
                        if ($buyer_transaction['transaction_for'] == 3) {
                            $withdrawal += $buyer_transaction['total_amount'];
                        } elseif ($buyer_transaction['transaction_for'] == 1) {
                            $deposit += $buyer_transaction['total_amount'];
                        }
                    }
                }
            }
        ?>

            <tr>
                <td><?php echo $key + 1; ?></td>
                <td>
                    <?php
                    $company = Company::model()->findByPk($value['company_id']);
                    echo $company['name'];
                    ?>
                </td>
                <td><?php echo $value['caption']; ?></td>
                <td><?php echo $value['bank_name']; ?></td>
                <td><?php echo $value['cashbalance_date']; ?></td>
                <td align="right"><?php echo Controller::money_format_inr($opening_balance, 2); ?></td>
                <td align="right"><?php echo Controller::money_format_inr($deposit, 2); ?></td>
                <td align="right"><?php echo Controller::money_format_inr($withdrawal, 2); ?></td>
                <td align="right"><?php echo Controller::money_format_inr((($opening_balance + $deposit) - $withdrawal), 2); ?></td>
            </tr>
        <?php } ?>
    </tbody>

</table>