<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap-theme.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" />
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/tablestyles.css" />
<?php  setlocale(LC_MONETARY, 'en_IN'); ?>
<h1>DAILY EXPENSE REPORT of <?= $currentmonth ?>,<?= $currentYear?></h1>

        <div class="tblwrap scrollTable">
            <table class="table table-bordered table-striped tab2">

                    <thead>

                        <tr>
                            <th style="display:none;"></th>
                            <th>Sl No.</th>
                            <th>Date</th>
                            <th>Expense Type</th>
                            <th>Receipt Type</th>
                            <th>Receipt</th>
                            <th>Payment</th>
                            <th>Description</th>
<!--                            <th>User</th>-->
                            <th>Data Entry</th>

                        </tr>
                    </thead>

                    <tbody>


<?php
$i = 0;
$total = 0;
$receiptamt = 0;
$paymentamt = 0;
$type_exp = '';
$flag = 0;
if ($dailyexpenses == NULL) {
    echo '<tr><td colspan="8">No records Found</td></tr>';
} else {
    foreach ($dailyexpenses as $daily) {
        $i++;
        $type = $daily['expense_name'];

        $tblpx = Yii::app()->db->tablePrefix;
        $totalqry1 = "SELECT sum(amount) as sum FROM " . $tblpx . "dailyexpense  as e
							left join " . $tblpx . "company_expense_type as p on p.company_exp_id=e.exp_type_id
							WHERE YEAR(e.date)='$currentYear' AND MONTH(e.date)='$currentmonth' AND p.name = '" . $type . "' AND e.exp_type = 0 order by company_exp_id";
        $sql1 = Yii::app()->db->createCommand($totalqry1)->queryRow();
        //print_r($sql1);

        $totalqry2 = "SELECT sum(amount) as sum FROM " . $tblpx . "dailyexpense  as e
							left join " . $tblpx . "company_expense_type as p on p.company_exp_id=e.exp_type_id
							WHERE YEAR(e.date)='$currentYear' AND MONTH(e.date)='$currentmonth' AND p.name = '" . $type . "' AND e.exp_type = 1 order by company_exp_id";
        $sql2 = Yii::app()->db->createCommand($totalqry2)->queryRow();
        //print_r($sql2);



        if ($type_exp != $type) {

            $type_exp = $type;
            $total = $total + $daily['amount'];

            $flag++;
            if ($flag != 1) {
                ?>


                                        <tr>	
                                            <td colspan="3"></td>
                                              <td></td>
                                            <td  align="right">Sub Total</td>
                                            <td align="right"><?php echo $sum1; ?></td>
                                            <td align="right"><?php echo $sum2; ?></td>
                                            <td></td>
                                            <td></td>
                                        </tr>


                <?php
            }
            ?>
            <?php
            $sum1 = money_format('%!.0n', $sql1['sum']);
            $sum2 = money_format('%!.0n', $sql2['sum']);
            ?>
                                    <tr>
                                        <td colspan="9"><b><?php echo $daily['expense_name']; ?></b></td>
                                    </tr>


                                    <?php
                                }
                                ?>
                                <tr>
                                    <th style="display:none;"></th>
                                    <td><?= $i++ ?></td>
                                    <td><?php echo date('d-M-Y', strtotime($daily['date'])); ?></td>
                                <?php if ($daily['expensetype'] == 1) { ?>
                                        <td><?php echo $daily['expense_name']; ?></td>
                                <?php } else { ?>
                                        <td></td>
        <?php } ?>
        <?php if ($daily['expensetype'] == 0) { ?>

                                        <td><?php echo $daily['expense_name']; ?></td>
                                    <?php } else { ?>
                                        <td></td>
                                    <?php } ?>
                                    <?php if ($daily['expensetype'] == 0) { ?>
                                        <td align="right"><?php
                            $amount = money_format('%!.0n', $daily['amount']);
                            echo $amount;
                            //echo $daily['amount'];
                            $receiptamt = $receiptamt + $daily['amount'];
                                        ?></td>
                                    <?php } else { ?>
                                        <td></td>
                                        <?php } ?>
                                        <?php if ($daily['expensetype'] == 1) { ?>
                                        <td align="right"><?php
                                            $amount = money_format('%!.0n', $daily['amount']);
                                            echo $amount;
                                            //echo $daily['amount'];
                                            $paymentamt = $paymentamt + $daily['amount'];
                                            ?></td>
                                    <?php } else { ?>
                                        <td></td>
                                        <?php } ?>

                                    <td align="left"><?php echo $daily['description']; ?></td>
<!--                                    <td align="left"><?php // echo $daily['first_name']; ?></td>-->
                                    <td align="left"><?php echo $daily['data_entry']; ?></td>

                                </tr>
                                    <?php
                                    $newexp = $daily['expense_name'];

                                    $tblpx = Yii::app()->db->tablePrefix;
                                    $newtotalqry1 = "SELECT sum(amount) as sum FROM " . $tblpx . "dailyexpense  as e
							left join " . $tblpx . "company_expense_type as p on p.company_exp_id=e.exp_type_id
							WHERE YEAR(e.date)='$currentYear' AND MONTH(e.date)='$currentmonth' AND p.name = '" . $newexp . "' AND e.exp_type = 0 order by company_exp_id";
                                    $newsql1 = Yii::app()->db->createCommand($newtotalqry1)->queryRow();
                                    //print_r($sql1);

                                    $newtotalqry2 = "SELECT sum(amount) as sum FROM " . $tblpx . "dailyexpense  as e
							left join " . $tblpx . "company_expense_type as p on p.company_exp_id=e.exp_type_id
							WHERE YEAR(e.date)='$currentYear' AND MONTH(e.date)='$currentmonth' AND p.name = '" . $newexp . "' AND e.exp_type = 1 order by company_exp_id";
                                    $newsql2 = Yii::app()->db->createCommand($newtotalqry2)->queryRow();

                                    $newsum1 = money_format('%!.0n', $newsql1['sum']);
                                    $newsum2 = money_format('%!.0n', $newsql2['sum']);
                                }
                                ?>
                            <tr>	
                                <td colspan="3"></td>
                                <td></td>
                                <td  align="right">Sub Total</td>
                                <td align="right"><?php echo $newsum1; ?></td>
                                <td align="right"><?php echo $newsum2; ?></td>
                                <td></td>
                                <td></td>
                                
                            </tr> 
    <?php
}
?>

                    </tbody>
                    
				    <tfoot>
						<tr>
                            <th colspan="3"></th>
                            <th  align="right">Total</th>
                             <th></th>
                            <th align="right"><?= money_format('%!.0n', $receiptamt); ?></th>
                            <th align="right"><?= money_format('%!.0n', $paymentamt); ?></th>
                            <th></th>
                            <th></th>
                        </tr>
				    </tfoot>




                </table>
        </div>


