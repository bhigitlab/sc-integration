<?php

class VouchersController extends Controller
{

	public $layout = '//layouts/column2';


	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		$controller = Yii::app()->controller->id;
		$hidden_actions =  array('dynamicproject', 'index', 'dynamicexpense', 'additems', 'deletevoucher', 'create', 'update', 'deleteitem', 'view');
		$rules = AccessRulesHelper::getRules($controller, $hidden_actions);
		//Additional action rules
		return $rules;
	}

	public function actionCreate()
	{
		$model = new Vouchers;
		$items_model = $voucher_items = new VoucherItems();
		$this->performAjaxValidation($model);
		$user = Users::model()->findByPk(Yii::app()->user->id);
		if (isset($_POST['Vouchers'])) {
			$model->attributes = $_POST['Vouchers'];
			$model->voucher_date = date('Y-m-d', strtotime($model->voucher_date));
			$model->created_by = Yii::app()->user->id;
			$model->created_date = date('Y-m-d H:i:s');
			if (!$model->save()) {
				$errors = json_encode($model->getErrors());
				Yii::app()->user->setFlash('error', $errors);
			} else {
				Yii::app()->user->setFlash('success', "Successfully Created");
				$this->redirect(array('vouchers/update', 'id' => $model->id));
			}
		}
		$this->render('_form', array(
			'model' => $model, 'user_companies' => $user->company_id, 'items_model' => $items_model, 'voucher_items' => $voucher_items
		));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$item_id = filter_input(INPUT_GET, 'item', FILTER_SANITIZE_SPECIAL_CHARS);
		$voucher_items = VoucherItems::model()->findAllByAttributes(array('voucher_id' => $id));
		$items_model = new VoucherItems();
		if (!empty($item_id)) {
			$items_model = VoucherItems::model()->findByPk($item_id);
		}
		$this->performAjaxValidation($model);
		$user = Users::model()->findByPk(Yii::app()->user->id);
		if (isset($_POST['Vouchers'])) {
			$model->attributes = $_POST['Vouchers'];
			$model->voucher_date = date('Y-m-d', strtotime($model->voucher_date));
			$model->updated_by = Yii::app()->user->id;
			if (!$model->save()) {
				$errors = json_encode($model->getErrors());
				Yii::app()->user->setFlash('error', $errors);
			} else {
				Yii::app()->user->setFlash('success', "Successfully Updated");
			}
		}

		$this->render('_form', array(
			'model' => $model, 'user_companies' => $user->company_id, 'items_model' => $items_model, 'voucher_items' => $voucher_items
		));
	}



	public function actionIndex()
	{
		$model = new Vouchers('search');
		$model->unsetAttributes();  // clear any default values
		$user = Users::model()->findByPk(Yii::app()->user->id);
		if (isset($_GET['Vouchers']))
			$model->attributes = $_GET['Vouchers'];
		$voucher_items = Vouchers::model()->findAll();
		$this->render('index', array(
			'dataProvider' => $voucher_items, 'model' => $model, 'user_companies' => $user->company_id
		));
	}


	public function actiondynamicProject()
	{
		$voucher_type = filter_input(INPUT_POST, 'voucher_type', FILTER_SANITIZE_SPECIAL_CHARS);
		$company_id = filter_input(INPUT_POST, 'company_id', FILTER_SANITIZE_SPECIAL_CHARS);
		$projects_list = "<option value=''>Select Project</option>";
		$voucher_user_lists = "<option value=''>Select </option>";
		$voucher_users = array();
		if (!empty($company_id)) {
			if ($voucher_type == 1) {
				$voucher_user_lists = "<option value=''>Select Subcontractor</option>";
				$voucher_users = Subcontractor::model()->findAll(
					'FIND_IN_SET(:company_id,company_id)',
					array(':company_id' => (int) $_POST['company_id'])
				);
				$voucher_users = CHtml::listData($voucher_users, 'subcontractor_id', 'subcontractor_name');
			} elseif ($voucher_type == 2) {
				$voucher_user_lists = "<option value=''>Select Vendor</option>";
				$voucher_users = Vendors::model()->findAll(
					'FIND_IN_SET(:company_id,company_id)',
					array(':company_id' => (int) $_POST['company_id'])
				);
				$voucher_users = CHtml::listData($voucher_users, 'vendor_id', 'name');
			}
			$projects = Projects::model()->findAll(
				'FIND_IN_SET(:company_id,company_id)',
				array(':company_id' => (int) $_POST['company_id'])
			);
			$projects = CHtml::listData($projects, 'pid', 'name');
			foreach ($projects as $value => $name)
				$projects_list .= CHtml::tag('option', array('value' => $value), CHtml::encode($name), true);
			foreach ($voucher_users as $value => $name)
				$voucher_user_lists .= CHtml::tag('option', array('value' => $value), CHtml::encode($name), true);
		}

		$result = array('projects_list' => $projects_list, 'voucher_for_list' => $voucher_user_lists);
		echo json_encode($result);
	}

	public function actiondynamicExpense()
	{
		$data = ExpenseType::model()->findAll(
			'company_id=:company_id',
			array(':company_id' => (int) $_POST['company_id'])
		);
		$data = CHtml::listData($data, 'type_id', 'type_name');
		echo "<option value=''>Select Expense</option>";
		foreach ($data as $value => $name)
			echo CHtml::tag('option', array('value' => $value), CHtml::encode($name), true);
	}
	public function loadModel($id)
	{
		$model = Vouchers::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'vouchers-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionaddItems()
	{
		if (isset($_POST['VoucherItems'])) {
			if (!empty($_POST['VoucherItems']['id'])) {
				$items_model = VoucherItems::model()->findByPk($_POST['VoucherItems']['id']);
			} else {
				$items_model = new VoucherItems();
			}

			$items_model->attributes = $_POST['VoucherItems'];
			$items_model->created_by = Yii::app()->user->id;
			$items_model->created_date = date('Y-m-d H:i:s');
			if (!$items_model->save()) {
				$errors = json_encode($items_model->getErrors());
				Yii::app()->user->setFlash('error', $errors);
			} else {
				Yii::app()->user->setFlash('success', "Successfully Created");
			}
			$this->redirect(array('vouchers/update', 'id' => $items_model->voucher_id));
		}
	}

	public function actiondeleteVoucher()
	{
		$id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
		if (!empty($id)) {
			$model = $this->loadModel($id);
			$voucher_items = VoucherItems::model()->findAllByAttributes(array('voucher_id' => $id));
			$transaction = Yii::app()->db->beginTransaction();
			$result =  $message = '';
			try {
				$result = 1;
				if (!empty($voucher_items)) {
					$items_delete = VoucherItems::model()->deleteAll('voucher_id = :voucher_id', array(':voucher_id' => $id));
					if (!$items_delete) {
						$errors = json_encode($items_delete->getErrors());
						$result = 0;
						throw new Exception($errors);
					}
				}
				if (!$model->delete()) {
					$errors = json_encode($model->getErrors());
					$result = 0;
					throw new Exception($errors);
				}
				$transaction->commit();
			} catch (Exception $error) {
				$message = $error->getMessage();
				$result = 0;
				$transaction->rollBack();
			} finally {
				if (!empty($result) && ($result == 1)) {
					Yii::app()->user->setFlash('success', "Successfully deleted");
				} elseif ($result == 0) {
					Yii::app()->user->setFlash('error', $message);
				} else {
					Yii::app()->user->setFlash('error', 'An error ocured');
				}
			}
		}
	}

	public function actiondeleteItem()
	{
		$id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
		$result = array('status' => 0, 'message' => '');
		if (!empty($id)) {
			$model = VoucherItems::model()->findByPk($id);
			if ($model->delete()) {
				Yii::app()->user->setFlash('success', "Successfully deleted");
				$result = array('status' => 1, 'message' => 'Successfully deleted');
			} else {
				$message = json_encode($model->getErrors());
				Yii::app()->user->setFlash('error', $message);
				$result = array('status' => 0, 'message' => $message);
			}
		}
		echo json_encode($result);
	}

	public function actionView($id)
	{
		$voucher_items = VoucherItems::model()->findAllByAttributes(array('voucher_id' => $id));
		$this->render('view', array(
			'model' => $this->loadModel($id), 'voucher_items' => $voucher_items
		));
	}
}
