DELETE FROM `jp_company_expense_type` WHERE `jp_company_expense_type`.`name` LIKE 'Staff Salary';

INSERT INTO `jp_company_expense_type` (`company_exp_id`, `name`, `type`, `company_id`, `status`) VALUES
(NULL, 'PETTY CASH REFUND', 0, 1, '1'),
(NULL, 'STATIONERY', 2, 1, '1'),
(NULL, 'PETTY CASH ISSUED', 1, 1, '1'),
(NULL, 'SALARY', 1, 1, '1'),
(NULL, 'SALARY ADVANCE', 1, 1, '1'),
(NULL, 'Office Maintance Work', 1, 1, '1'),
(NULL, 'OFFICE ASSETS', 1, 1, '1'),
(NULL, 'KSEB', 1, 1, '1'),
(NULL, 'DRAWINGS', 1, 1, '1'),
(NULL, 'Office Expenss', 1, 1, '1'),
(NULL, 'PICKUP EXPENSES', 1, 1, '1'),
(NULL, 'LOAN FROM DIRECTORS', 0, 1, '1'),
(NULL, 'BANK LOAN', 0, 1, '1'),
(NULL, 'BANK LOAN INTEREST', 1, 1, '1'),
(NULL, 'INDIRECT INCOME - REFUND', 0, 1, '1'),
(NULL, 'BANK CHARGES', 1, 1, '1');