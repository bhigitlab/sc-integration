<?php

/* @var $this BillsController */
/* @var $model Bills */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<style type="text/css">
    tr.pitems td input, tr.pitems td div.pricediv {
        text-align: right!important;
    }
</style>
<div class="row">
    <div class="col-sm-12 table-responsive">
        <table class="table">
            <thead class="thead-inverse">
                <tr>
                    <th>Specifications/Remark</th>
                    <th>HSN Code</th>
                    <th>Actual Quantity</th>
                    <th>Billed Quantity</th>
                    <th>Remaining Quantity</th>
                    <th>Quantity</th>
                    <th>Unit</th>
                    <th>Rate</th>
                    <th>Amount</th>
                    <th>Tax Slab (%)</th>
                    <th>Discount Amount</th>
                    <th>Discount (%)</th>
                    <th>CGST (%)</th>
                    <th>CGST</th>
                    <th>SGST (%)</th>
                    <th>SGST</th>
                    <th>IGST (%)</th>
                    <th>IGST</th>
                    <th>Total Tax</th>
                    <th>Tax (%)</th>
                    <th>Total Amount</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <?php
            $number    = 1;
            $amount    = 0;
            $totalQty  = 0;
            $bTotalQty = 0;
            // foreach($purchase as $item) {
            //     if($item['category_id']) {
            //         $categorymodel = $this->GetItemCategoryById($parent = 0, $spacing = '', $user_tree_array = '', $item['category_id']);
            //         if($categorymodel)
            //             $categoryName = $categorymodel["data"];
            //         else
            //             $categoryName = "Not available";
            //     } else {
            //        $categoryName  = $item['remark'];
            //     }
            //     $purchasemodel = PurchaseItems::model()->findByPk($item['item_id']);
            //     $itemQuantity  = $purchasemodel->quantity;
            //     $tblpx         = Yii::app()->db->tablePrefix;
            //     $itemData      = Yii::app()->db->createCommand("SELECT SUM(billitem_quantity) as quantitysum FROM ".$tblpx."billitem WHERE purchaseitem_id = ".$item['item_id'])->queryRow();  
            //     $bItemQuantity = $itemData['quantitysum']?$itemData['quantitysum']:0;
            //     if(strpos($bItemQuantity,".") !== false){
            //         $bItemQuantity = number_format((float)$bItemQuantity, 2, '.', '');
            //     }else{
            //         $bItemQuantity = $bItemQuantity;
            //     }
            //     $quantityAvail = $itemQuantity - $bItemQuantity;
            //     $totalQty      = $totalQty + $itemQuantity;
            //     $bTotalQty     = $bTotalQty + $bItemQuantity;
            ?>
              
            <?php
            // $amount = $amount + $item["amount"];
            // $number = $number + 1;
            // }
            ?>
                <input type="hidden" name="tqty[]" id="tqty" value="<?php echo $totalQty; ?>"/>
                <input type="hidden" name="btqty[]" id="btqty" value="<?php echo $bTotalQty; ?>"/>
                <input type="hidden" id="totrows" value ="<?php echo $number-1; ?>" class="txtBox pastweek totrows" name="totrows"/>
            </tbody>
        </table>
    </div>
</div>

