<?php

/**
 * This is the model class for table "{{company_expense_type}}".
 *
 * The followings are the available columns in table '{{company_expense_type}}':
 * @property integer $company_exp_id
 * @property string $name
 */
class Companyexpensetype extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{company_expense_type}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('company_exp_id,type', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>50),
                        array('name', 'unique', 'message' => '{attribute} already exists.'),
                        array('name', 'filter', 'filter'=>'trim'),
                        array('type,exp_type','safe'),
                        //array('name', 'checkexist'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('company_exp_id, name,type,status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'dailyexpenses' => array(self::HAS_MANY, 'Dailyexpense', 'exp_type_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'company_exp_id' => 'Company Exp',
			'name' => 'Name',
                        'type' => 'Type',
                        'status' => 'Status'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('company_exp_id',$this->company_exp_id);
		$criteria->compare('name',$this->name,true);
                $criteria->compare('type',$this->type);
                $criteria->compare('status',$this->status);
                $user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                foreach($arrVal as $arr) {
                    if ($newQuery) $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET('".$arr."', company_id)";
                }
                $criteria->addCondition($newQuery);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => array('defaultOrder' => 'name ASC',
            ),
                    'pagination'=>false
		));
	}
        
        public function checkexist($attribute, $params) {

            $id = Companyexpensetype::model()->findByAttributes(array('company_exp_id' => $this->company_exp_id));
            if(!empty($id)){
                $name = Companyexpensetype::model()->findByAttributes(array('name' => $this->name, 'company_id' => Yii::app()->user->company_id),array('condition'=>'company_exp_id != '.$this->company_exp_id.''));
                if(!empty($name)) {
                    $this->addError("name", ("Expense type already exists"));
               }
            } else{
                $name = Companyexpensetype::model()->findByAttributes(array('name' => $this->name, 'company_id' => Yii::app()->user->company_id));
                if(!empty($name)) {
                    $this->addError("name", ("Expense type already exists"));
                }
            }
        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Companyexpensetype the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
