<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript"
    src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>

<script>
    var shim = (function () {
        document.createElement('datalist');
    })();
</script>
<style>
    .select2 {
        width: 100%;
    }
</style>
<script>
    $(function () {
        $("#datepicker").datepicker({
            dateFormat: 'dd-mm-yy'
        }).datepicker("setDate", new Date());

    });
</script>

<div class="container">
    <div class="expenses-heading">
        <h3>Quotation</h3>
    </div>
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
    <div id="msg_box"></div>
    <div class="entries-wrapper">
        <form id="pdfvals1" method="post" action="<?php echo $this->createAbsoluteUrl('purchase/addpurchase'); ?>">
            <input type="hidden" name="remove" id="remove" value="">
            <input type="hidden" name="quotation_id" id="quotation_id" value="0">
            <div class="row">
                <div class="col-xs-12">
                    <div class="heading-title">Add Details</div>
                    <div class="dotted-line"></div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-xs-12 col-sm-3 col-md-2">
                    <label>COMPANY : <span id="client_label"></span></label>
                    <?php
                    $user = Users::model()->findByPk(Yii::app()->user->id);
                    $arrVal = explode(',', $user->company_id);
                    $newQuery = "";
                    foreach ($arrVal as $arr) {
                        if ($newQuery)
                            $newQuery .= ' OR';
                        $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
                    }
                    $companyInfo = Company::model()->findAll(array('condition' => $newQuery));
                    ?>
                    <select name="company_id" class="inputs target company_id w-100" id="company_id">
                        <option value="">Choose company</option>
                        <?php
                        foreach ($companyInfo as $key => $value) {
                            ?>
                            <option value="<?php echo $value['id']; ?>">
                                <?php echo $value['name']; ?>
                            </option>
                            <?php
                        }
                        ?>

                    </select>
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-2">

                    <label>CLIENT : <span id="client_label"></span></label>
                    <select name="client" class="inputs target client w-100" id="client">
                        <option value="">Choose client</option>
                        <?php
                        foreach ($client as $key => $value) {
                            ?>
                            <option value="<?php echo $value['cid']; ?>">
                                <?php echo $value['name']; ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>

                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-2">
                    <label>PROJECT : <span id="project_label"></span></label>
                    <select name="project" class="inputs target project w-100" id="project">
                        <option value="">No Project</option>
                    </select>
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-2">

                    <label>DATE : </label>
                    <input type="text"
                        value="<?php echo ((isset($date) && $date != '') ? date("d-m-Y", strtotime($date)) : ''); ?>"
                        id="datepicker" class="txtBox date inputs target form-control" name="date"
                        placeholder="Please click to edit">

                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-2">

                    <label>Quotation No : </label>
                    <?php
                    $readonly = '';
                    $activeProjectTemplate = $this->getActiveTemplate();
                    if ($activeProjectTemplate == 'TYPE-4' && ENV == 'production') {
                        $readonly = 'readonly';
                    }
                    ?>
                    <input type="text" required
                        value="<?php echo ((isset($inv_no) && $inv_no != '') ? $inv_no : ''); ?>"
                        class="txtBox inputs target check_type inv_no  form-control" name="inv_no" id="inv_no"
                        placeholder="" <?php echo $readonly ?>>

                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-2">
                    <label>Total Project Cost : </label>
                    <input type="text" required value=""
                        class="txtBox inputs target check_type project_cost  form-control" name="project_cost"
                        id="project_cost" autocomplete="off">
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-xs-12">
                <div class="heading-title">Payment Details</div>
                <div class="dotted-line"></div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-xs-12 col-sm-3 col-md-2">
                <label>Payment Stage:</label>
                <input type="hidden" class="stage_id" />
                <input type="text" class="payment_stage form-control" />
            </div>
            <div class="form-group col-xs-12 col-sm-3 col-md-2">
                <label>Percentage:</label>
                <input type="text" class="percentage form-control" />
            </div>
            <div class="form-group col-xs-12 col-sm-3 col-md-2">
                <label>Amount:</label>
                <input type="text" class="stage_amount form-control" />
            </div>
            <div class="form-group col-xs-12 col-sm-3 col-md-2">
                <label>Sl Number:</label>
                <input type="text" class="serial_number form-control" value="1" readonly />
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 text-right">
                <button class="btn btn-primary addstage" type="button">Add</button>
                <button class="btn btn-default resetStage" type="reset">Reset</button>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery.extend(jQuery.expr[':'], {
        focusable: function (el, index, selector) {
            return $(el).is('button, :input, [tabindex]');
        }
    });

    $(document).on('keypress', 'input,select', function (e) {
        if (e.which == 13) {
            e.preventDefault();
            var $canfocus = $(':focusable');
            var index = $canfocus.index(document.activeElement) + 1;
            if (index >= $canfocus.length)
                index = 0;
            $canfocus.eq(index).focus();
        }
    });

    $(document).ready(function () {

        $(".popover-test").popover({
            html: true,
            content: function () {
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function (e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function (e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function (e) {
            $('[data-toggle=popover]').each(function () {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
        $(document).ajaxComplete(function () {
            $(".popover-test").popover({
                html: true,
                content: function () {
                    return $(this).next('.popover-content').html();
                }
            });

        });
        $(".js-example-basic-single").select2();
        $(".project").select2();
        $(".client").select2();
        $(".company_id").select2();
    });

    $('.client').change(function () {
        $('#loading').show();
        var val = $(this).val();
        $.ajax({
            type: "POST",
            url: '<?php echo Yii::app()->createUrl('quotation/getProject'); ?>',
            data: 'client_id=' + val,
            dataType: 'json',
            success: function (data) {
                if (data.status === 'success') {
                    if (data.qtn_no == "") {
                        $("#inv_no").attr("readonly", false);
                    } else {
                        $("#inv_no").attr("readonly", true);
                    }
                    $('#project').html(data.projectlist);
                    $('#inv_no').val(data.qtn_no);
                    $('#inv_no').trigger("blur");
                } else {
                    $('#project').val('No Projects');
                }

            },
        })
    })

    $(document).ready(function () {
        $('select').first().focus();
    });
</script>



<script>
    $(document).ready(function () {
        $().toastmessage({
            sticky: false,
            position: 'top-right',
            inEffectDuration: 1000,
            stayTime: 3000,
            closeText: '<i class="icon ion-close-round"></i>',
        });

        $(".purchase_items").addClass('checkek_edit');


    });


    $(document).on('click', '.removebtn', function (e) {
        e.preventDefault();
        element = $(this);
        var item_id = $(this).attr('id');
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            var item_id = $(this).attr('id');
            var quotation_id = $("#quotation_id").val();
            var data = {
                'quotation_id': quotation_id,
                'item_id': item_id
            };
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                method: "GET",
                async: false,
                data: {
                    data: data
                },
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('quotation/removequotationitem'); ?>',
                success: function (response) {
                    if (response.response == 'success') {
                        element.closest('tr').remove();
                        $('#amount_total').html(response.amount_total);
                        $('#tax_total').html(response.tax_total);
                        $('#grand_total').html(response.grand_total);
                        $().toastmessage('showSuccessToast', "" + response.msg + "");
                        $('.addrow').html(response.html);
                    } else { }
                }
            });

        } else {

            return false;
        }

    });



    $(document).on("click", ".addcolumn, .removebtn", function () {
        $("table.table  input[name='sl_No[]']").each(function (index, element) {
            $(element).val(index + 1);
            $('.sl_No').html(index + 1);
        });
    });

    $(".inputSwitch span").on("click", function () {
        var $this = $(this);
        $this.hide().siblings("input").val($this.text()).show();

    });

    $(".inputSwitch input").bind('blur', function () {

        var $this = $(this);

        $(this).attr('value', $(this).val());

        $this.hide().siblings("span").text($this.val()).show();

    }).hide();


    $(".allownumericdecimal").keydown(function (event) {



        if (event.shiftKey == true) {
            event.preventDefault();
        }

        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
            (event.keyCode >= 96 && event.keyCode <= 105) ||
            event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
            event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
            var splitfield = $(this).val().split(".");
            if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
                event.preventDefault();
            }
        } else {
            event.preventDefault();
        }

        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();

    });

    $('.other').click(function () {
        if (this.checked) {
            $('#autoUpdate').fadeIn('slow');
            $('#select_div').hide();
        } else {
            $('#autoUpdate').fadeOut('slow');
            $('#select_div').show();
        }

    })


    $(document).on("change", ".other", function () {
        if (this.checked) {
            $(this).closest('tr').find('#autoUpdate').fadeIn('slow');
            $(this).closest('tr').find('#select_div').hide();
        } else {
            $(this).closest('tr').find('#autoUpdate').fadeOut('slow');
            $(this).closest('tr').find('#select_div').show();
        }
    });
</script>


<script>
    /* Neethu  */

    $(document).on("change", "#company_id", function () {
        var element = $(this);
        var quotation_id = $("#quotation_id").val();
        var default_date = $(".date").val();
        var company = $(this).val();
        var client = $('#client').val();
        var project = $('#project').val();
        var inv_no = $('#inv_no').val();
        var project_cost = $("#project_cost").val();

        if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
            if (client == '' || default_date == '' || inv_no == '' || company == '') {
                $.ajax({
                    method: "GET",
                    data: {
                        quotation_id: 'test'
                    },
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('quotation/ajaxdate'); ?>',
                    success: function (result) {
                        $('#client').focus();
                    }
                });
            } else {
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    method: "GET",
                    async: false,
                    data: {
                        quotation_id: quotation_id,
                        inv_no: inv_no,
                        default_date: default_date,
                        project: project,
                        client: client,
                        company: company,
                        project_cost: project_cost
                    },
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('quotation/createnewquotation'); ?>',
                    success: function (result) {
                        if (result.response == 'success') {
                            $(".purchase_items").removeClass('checkek_edit');
                            $('#client').select2('focus');
                            $("#quotation_id").val(result.quotation_id);
                            window.location.href = '<?php echo Yii::app()->createAbsoluteUrl('quotation/updatequotation&qtid='); ?>' + result.quotation_id;
                            $('.client').trigger("change");
                            $().toastmessage('showSuccessToast', "" + result.msg + "");
                        } else {
                            $().toastmessage('showErrorToast', "" + result.msg + "");
                            element.val('');
                        }

                    }
                });
            }
        } else {
            $(this).focus();
            $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
            $(this).focus();
        }
    });

    $(document).on("change", "#project", function () {
        var element = $(this);
        var quotation_id = $("#quotation_id").val();
        var default_date = $(".date").val();
        var project = $(this).val();
        var client = $('#client').val();
        var inv_no = $('#inv_no').val();
        var company = $('#company_id').val();
        var project_cost = $("#project_cost").val();

        if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
            if (client == '' || default_date == '' || inv_no == '' || company == '' || project == '') {
                $.ajax({
                    method: "GET",
                    data: {
                        quotation_id: 'test'
                    },
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('quotation/ajaxdate'); ?>',
                    success: function (result) {
                        $('.date').focus();
                    }
                });
            } else {
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    method: "GET",
                    async: false,
                    data: {
                        quotation_id: quotation_id,
                        inv_no: inv_no,
                        default_date: default_date,
                        project: project,
                        client: client,
                        company: company,
                        project_cost: project_cost
                    },
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('quotation/createnewquotation'); ?>',
                    success: function (result) {
                        if (result.response == 'success') {
                            $(".purchase_items").removeClass('checkek_edit');
                            $('.js-example-basic-single').select2('focus');
                            $("#quotation_id").val(result.quotation_id);
                            $().toastmessage('showSuccessToast', "" + result.msg + "");
                        } else {
                            $().toastmessage('showErrorToast', "" + result.msg + "");
                            element.val('');
                        }

                    }
                });
            }
        } else {
            $(this).focus();
            $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
            $(this).focus();
        }
    });

    $(document).on("change", "#client", function () {
        var element = $(this);
        var quotation_id = $("#quotation_id").val();
        var default_date = $(".date").val();
        var client = $(this).val();
        var company = $('#company_id').val();
        var project = $('#project').val();
        var inv_no = $('#inv_no').val();
        var project_cost = $("#project_cost").val();
        if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
            if (client == '' || default_date == '' || inv_no == '' || company == '' || project == '') {
                $.ajax({
                    method: "GET",
                    data: {
                        quotation_id: 'test'
                    },
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('quotation/ajaxdate'); ?>',
                    success: function (result) {
                        $('.date').focus();
                    }
                });
            } else {
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    method: "GET",
                    data: {
                        quotation_id: quotation_id,
                        inv_no: inv_no,
                        default_date: default_date,
                        project: project,
                        client: client,
                        company: company,
                        project_cost: project_cost
                    },
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('quotation/createnewquotation'); ?>',
                    success: function (result) {
                        if (result.response == 'success') {
                            $(".purchase_items").removeClass('checkek_edit');
                            $('.js-example-basic-single').select2('focus');
                            $("#quotation_id").val(result.quotation_id);
                            $().toastmessage('showSuccessToast', "" + result.msg + "");

                        } else {
                            $().toastmessage('showErrorToast', "" + result.msg + "");
                        }
                    }
                });

            }

        } else {
            $(this).focus();
            $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
            $(this).focus();
        }

    });



    $(document).on("change", ".date", function () {
        var element = $(this);
        var quotation_id = $("#quotation_id").val();
        var default_date = $(this).val();
        var project = $('#project').val();
        var client = $('#client').val();
        var inv_no = $('#inv_no').val();
        var company = $('#company_id').val();
        var project_cost = $("#project_cost").val();

        if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
            if (client == '' || default_date == '' || inv_no == '' || company == '' || project == '') { } else {
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    method: "GET",
                    async: false,
                    data: {
                        quotation_id: quotation_id,
                        inv_no: inv_no,
                        default_date: default_date,
                        project: project,
                        client: client,
                        company: company,
                        project_cost: project_cost
                    },
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('quotation/createnewquotation'); ?>',
                    success: function (result) {
                        if (result.response == 'success') {
                            $(".purchase_items").removeClass('checkek_edit');
                            $('.js-example-basic-single').select2('focus');
                            $("#quotation_id").val(result.quotation_id);
                            $().toastmessage('showSuccessToast', "" + result.msg + "");
                        } else {
                            $().toastmessage('showErrorToast', "" + result.msg + "");
                        }
                    }

                });
            }
        } else {
            $(this).focus();
            $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
            $(this).focus();
        }

        $("#inv_no").focus();

    });


    $(document).on("blur", "#inv_no", function (event) {
        var element = $(this);
        var quotation_id = $("#quotation_id").val();
        var default_date = $(".date").val();
        var inv_no = $(this).val();

        if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
            if (inv_no == '') {
                event.preventDefault();
            } else {
                var project = $('#project').val();
                var client = $('#client').val();
                var date = $('.date').val();
                var company = $('#company_id').val();
                var project_cost = $("#project_cost").val();
                if (client == '' || default_date == '' || company == '' || project == '') { } else {
                    var confirmSave = confirm("Do you want to save Quotation without Project cost");
                    if (confirmSave) {
                        $('.loading-overlay').addClass('is-active');
                        $.ajax({
                            method: "GET",
                            data: {
                                quotation_id: quotation_id,
                                inv_no: inv_no,
                                default_date: default_date,
                                project: project,
                                client: client,
                                company: company,
                                project_cost: project_cost
                            },
                            dataType: "json",
                            url: '<?php echo Yii::app()->createUrl('quotation/createnewquotation'); ?>',
                            success: function (result) {
                                if (result.response == 'success') {
                                    $(".purchase_items").removeClass('checkek_edit');
                                    $("#quotation_id").val(result.quotation_id);
                                    window.location.href = '<?php echo Yii::app()->createAbsoluteUrl('quotation/updatequotation&qtid='); ?>' + result.quotation_id;
                                    $().toastmessage('showSuccessToast', "" + result.msg + "");
                                } else {
                                    $().toastmessage('showErrorToast', "" + result.msg + "");
                                }

                                $('#description').focus();
                            }
                        });



                    }
                }
            }

        } else {
            $(this).focus();
            $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
            $(this).focus();
        }

    });

    $(document).on("change", "#project_cost", function () {
        var element = $(this);
        var project_cost = $("#project_cost").val();
        var quotation_id = $("#quotation_id").val();
        var default_date = $(".date").val();
        var company = $('#company_id').val();
        var client = $('#client').val();
        var project = $('#project').val();
        var inv_no = $('#inv_no').val();

        if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
            if (client == '' || default_date == '' || inv_no == '' || company == '' || project == '') { } else {
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    method: "GET",
                    async: false,
                    data: {
                        quotation_id: quotation_id,
                        inv_no: inv_no,
                        default_date: default_date,
                        project: project,
                        client: client,
                        company: company,
                        project_cost: project_cost
                    },
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('quotation/createnewquotation'); ?>',
                    success: function (result) {
                        if (result.response == 'success') {
                            $(".purchase_items").removeClass('checkek_edit');
                            $('#client').select2('focus');
                            $("#quotation_id").val(result.quotation_id);
                            window.location.href = '<?php echo Yii::app()->createAbsoluteUrl('quotation/updatequotation&qtid='); ?>' + result.quotation_id;
                            $().toastmessage('showSuccessToast', "" + result.msg + "");
                        } else {
                            $().toastmessage('showErrorToast', "" + result.msg + "");
                            element.val('');
                        }

                    }
                });
            }
        } else {
            $(this).focus();
            $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
            $(this).focus();
        }
    });


    $("#purchaseno").keypress(function (event) {
        if (event.keyCode == 13) {
            $("#purchaseno").blur();
        }
    });

    $("#date").keypress(function (event) {
        if (event.keyCode == 13) {
            if ($(this).val()) {
                $("#inv_no").focus();
            }
        }
    });

    $(".date").keyup(function (event) {
        if (event.keyCode == 13) {
            $(".date").click();
        }
    });

    $("#vendor").keyup(function (event) {
        if (event.keyCode == 13) {
            $("#vendor").click();
        }
    });

    $("#project").keyup(function (event) {
        if (event.keyCode == 13) {
            $("#project").click();
        }
    });


    var sl_no = 1;
    var howMany = 0;
    $('.item_save').click(function () {
        $("#previous_details").hide();
        var element = $(this);
        var item_id = $(this).attr('id');

        if (item_id == 0) {


            var description = $('#description').val();
            var hsn_code = $('#hsn_code').val();
            var quantity = $('#quantity').val();
            var unit = $('#unit').val();
            var rate = $('#rate').val();

            var cgst = $('#cgst').val();
            var cgst_amount = $('#cgst_amount').html();
            var sgst = $('#sgst').val();
            var sgst_amount = $('#sgst_amount').html();
            var igst = $('#igst').val();
            var igst_amount = $('#igst_amount').html();
            var tax_amount = $('#tax_amount').html();
            var company = $('#company_id').val();
            var amount = $('#item_amount').html();
            var project = $('input[name="project"]').val();
            var client = $('input[name="client"]').val();
            var date = $('input[name="date"]').val();
            var inv_no = $('input[name="inv_no"]').val();
            var rowCount = $('.table .addrow tr').length;


            if (client == '' || date == '' || inv_no == '' || company == '' || project == '') {
                $().toastmessage('showErrorToast', "Please enter quotation details");
            } else {
                if ((moment(date, 'DD-MM-YYYY', true).isValid())) {

                    if (description == '' || quantity == '' || rate == '' || amount == 0) {
                        if (amount == 0) {
                            $().toastmessage('showErrorToast', "Please fill valid quantity and rate");
                        } else {
                            $().toastmessage('showErrorToast', "Please fill the details");
                        }
                    } else {
                        howMany += 1;
                        if (howMany == 1) {
                            var quotation_id = $('input[name="quotation_id"]').val();
                            var subtot = $('#grand_total').text();
                            var grand = $('#grand_total').text();
                            var grand_tax = $('#grand_taxamount').val();
                            var data = {
                                'sl_no': rowCount,
                                'quantity': quantity,
                                'description': description,
                                'unit': unit,
                                'rate': rate,
                                'amount': amount,
                                'sgst': sgst,
                                'sgst_amount': sgst_amount,
                                'cgst': cgst,
                                'cgst_amount': cgst_amount,
                                'igst': igst,
                                'igst_amount': igst_amount,
                                'tax_amount': tax_amount,
                                'quotation_id': quotation_id,
                                'grand': grand,
                                'subtot': subtot,
                                'grand_tax': grand_tax,
                                'hsn_code': hsn_code
                            };
                            $('.loading-overlay').addClass('is-active');
                            $.ajax({
                                url: '<?php echo Yii::app()->createAbsoluteUrl('quotation/quotationitem'); ?>',
                                type: 'GET',
                                dataType: 'json',
                                data: {
                                    data: data
                                },
                                success: function (response) {
                                    if (response.response == 'success') {
                                        $('#discount_total').html(response.discount_total);
                                        $('#amount_total').html(response.amount_total);
                                        $('#tax_total').html(response.tax_total);
                                        $('#grand_total').html(response.grand_total);
                                        $().toastmessage('showSuccessToast', "" + response.msg + "");
                                        $('.addrow').html(response.html);
                                    } else {
                                        $().toastmessage('showErrorToast', "" + response.msg + "");
                                    }
                                    howMany = 0;
                                    $('#description').val('').trigger('change');
                                    $('#remarks').val('');
                                    $('#quantity').val('');
                                    $('#unit').val('');
                                    $('#rate').val('');
                                    $('#item_amount').html('');
                                    $('#tax_amount').val('0.00');
                                    $('#sgst').val('');
                                    $('#sgst_amount').html('0.00');
                                    $('#cgst').val('');
                                    $('#cgst_amount').html('0.00');
                                    $('#igst').val('');
                                    $('#igst_amount').html('0.00');
                                    $('#description').focus();
                                    $('#hsn_code').val('');
                                }
                            });

                        }
                    }

                } else {
                    $(this).focus();
                    $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
                    $(this).focus();
                }
            }

        } else {
            var description = $('#description').val();
            var hsn_code = $('#hsn_code').val();
            var quantity = $('#quantity').val();
            var unit = $('#unit').val();
            var rate = $('#rate').val();
            var amount = $('#item_amount').html();
            var project = $('input[name="project"]').val();
            var client = $('input[name="client"]').val();
            var date = $('input[name="date"]').val();
            var inv_no = $('input[name="inv_no"]').val();
            var company = $('#company_id').val();
            var cgst = $('#cgst').val();
            var cgst_amount = $('#cgst_amount').html();
            var sgst = $('#sgst').val();
            var sgst_amount = $('#sgst_amount').html();
            var igst = $('#igst').val();
            var igst_amount = $('#igst_amount').html();
            var tax_amount = $('#tax_amount').html();

            if (client == '' || date == '' || inv_no == '' || company == '' || project == '') {
                $().toastmessage('showErrorToast', "Please enter quotation details");
            } else {

                if ((moment(date, 'DD-MM-YYYY', true).isValid())) {
                    if (description == '' || quantity == '' || rate == '' || amount == 0) {
                        if (amount == 0) {
                            $().toastmessage('showErrorToast', "Please fill valid quantity and rate");
                        } else {
                            $().toastmessage('showErrorToast', "Please fill the details");
                        }
                    } else {

                        howMany += 1;
                        if (howMany == 1) {
                            var quotation_id = $('input[name="quotation_id"]').val();
                            var subtot = $('#grand_total').text();
                            var grand = $('#grand_total').text();
                            var grand_tax = $('#grand_taxamount').val();
                            var data = {
                                'item_id': item_id,
                                'sl_no': sl_no,
                                'quantity': quantity,
                                'description': description,
                                'unit': unit,
                                'rate': rate,
                                'amount': amount,
                                'sgst': sgst,
                                'sgst_amount': sgst_amount,
                                'cgst': cgst,
                                'cgst_amount': cgst_amount,
                                'igst': igst,
                                'igst_amount': igst_amount,
                                'tax_amount': tax_amount,
                                'quotation_id': quotation_id,
                                'grand': grand,
                                'subtot': subtot,
                                'grand_tax': grand_tax,
                                'hsn_code': hsn_code
                            };
                            $('.loading-overlay').addClass('is-active');
                            $.ajax({
                                url: '<?php echo Yii::app()->createAbsoluteUrl('quotation/updatesquotationitem'); ?>',
                                type: 'GET',
                                dataType: 'json',
                                data: {
                                    data: data
                                },
                                success: function (response) {
                                    if (response.response == 'success') {
                                        $('#amount_total').html(response.amount_total);
                                        $('#tax_total').html(response.tax_total);
                                        $('#grand_total').html(response.grand_total);
                                        $().toastmessage('showSuccessToast', "" + response.msg + "");
                                        $('.addrow').html(response.html);
                                    } else {
                                        $().toastmessage('showErrorToast', "" + response.msg + "");
                                    }
                                    $('#description').val('').trigger('change');
                                    $('#remarks').val('');
                                    $('#quantity').val('');
                                    $('#unit').val('');
                                    $('#rate').val('');
                                    $('#item_amount').html('');
                                    $('#tax_amount').val('0.00');
                                    $('#sgst').val('');
                                    $('#sgst_amount').html('0.00');
                                    $('#cgst').val('');
                                    $('#cgst_amount').html('0.00');
                                    $('#igst').val('');
                                    $('#igst_amount').html('0.00');
                                    $('#description').focus();
                                    $('#item_amount_temp').val(0);
                                    $('#item_tax_temp').val(0);
                                    $(".item_save").attr('value', 'Save');
                                    $(".item_save").attr('id', 0);
                                    $('#description').focus();
                                    $('#hsn_code').val('');
                                    howMany = 0;
                                }
                            });

                        }

                    }

                } else {
                    $(this).focus();
                    $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
                    $(this).focus();
                }



            }


        }


    });





    $(document).on('click', '.edit_item', function (e) {
        e.preventDefault();
        var item_id = $(this).attr('id');
        var $tds = $(this).closest('tr').find('td');
        var description = $tds.eq(1).text();
        var hsn_code = $tds.eq(2).text();
        var quantity = $tds.eq(3).text();
        var unit = $tds.eq(4).text();
        var rate = $tds.eq(5).text();
        var sgst = $tds.eq(6).text();
        var sgst_amount = $tds.eq(7).text();
        var cgst = $tds.eq(8).text();
        var cgst_amount = $tds.eq(9).text();
        var igst = $tds.eq(10).text();
        var igst_amount = $tds.eq(11).text();
        var tax_amount = $tds.eq(12).text();
        var amount = $tds.eq(13).text();
        var sgsct1 = eval($tds.eq(7).text()).toFixed(2);
        var sgsct2 = eval($tds.eq(9).text()).toFixed(2);
        var sgsct3 = eval($tds.eq(11).text()).toFixed(2);
        var tax_amount = (eval(sgsct1) + eval(sgsct2) + eval(sgsct3));

        $('#description').val(description.trim());
        $('#hsn_code').val(parseFloat(hsn_code));
        $('#quantity').val(parseFloat(quantity));
        $('#unit').val(unit.trim());
        $('#item_amount').html(parseFloat(amount))
        $('#item_amount_temp').val(parseFloat(amount))
        $('#rate').val(parseFloat(rate))

        $('#tax_amount').html(parseFloat(tax_amount.toFixed(2)))
        $('#item_tax_temp').val(parseFloat(tax_amount))
        $('#cgst').val(cgst.trim())
        $('#cgst_amount').text(cgst_amount)
        $('#sgst').val(sgst.trim())
        $('#sgst_amount').text(sgst_amount)
        $('#igst').val(igst.trim())
        $('#igst_amount').text(igst_amount)
        $('#tax_amount').text(tax_amount.toFixed(2))

        $(".item_save").attr('value', 'Update');
        $(".item_save").attr('id', item_id);
        $(".item_save").attr('id', item_id);
        $('#description').focus();


    })


    $('.item_save').keypress(function (e) {
        if (e.keyCode == 13) {
            $('.item_save').click();
        }
    });


    $("#quantity, #rate, #sgst, #cgst, #igst").blur(function () {
        var quantity = parseFloat($("#quantity").val());
        var rate = parseFloat($("#rate").val());
        var sgst = parseFloat($("#sgst").val());
        var cgst = parseFloat($("#cgst").val());
        var igst = parseFloat($("#igst").val());
        var amount = quantity * rate;
        var new_amount = 0;

        var sgst_amount = (sgst / 100) * amount;
        var cgst_amount = (cgst / 100) * amount;
        var igst_amount = (igst / 100) * amount;

        if (isNaN(sgst_amount))
            sgst_amount = 0;
        if (isNaN(cgst_amount))
            cgst_amount = 0;
        if (isNaN(igst_amount))
            igst_amount = 0;
        if (isNaN(amount))
            amount = 0;

        var tax_amount = sgst_amount + cgst_amount + igst_amount;

        var total_amount = amount + tax_amount;

        $("#sgst_amount").html(sgst_amount.toFixed(2));
        $("#cgst_amount").html(cgst_amount.toFixed(2));
        $("#igst_amount").html(igst_amount.toFixed(2));
        $("#item_amount").html(amount.toFixed(2));
        $("#tax_amount").html(tax_amount.toFixed(2));
        $("#total_amount").html(total_amount.toFixed(2));

    });

    $("#company_id").change(function () {
        var val = $(this).val();
        $("#client").html('<option value="">Select Client</option>');
        $('#loading').show();
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('quotation/dynamicclient'); ?>',
            method: 'POST',
            data: {
                company_id: val
            },
            dataType: "json",
            success: function (response) {
                if (response.status == 'success') {
                    $("#client").html(response.html);
                } else {
                    $("#client").html(response.html);
                }
            }

        })
    })
</script>

<script>
        (function () {
            var mainTable = document.getElementById("main-table");
            if (mainTable !== null) {
                var tableHeight = mainTable.offsetHeight;
                if (tableHeight > 380) {
                    var fauxTable = document.getElementById("faux-table");
                    document.getElementById("table-wrap").className += ' ' + 'fixedON';
                    var clonedElement = mainTable.cloneNode(true);
                    clonedElement.id = "";
                    fauxTable.appendChild(clonedElement);
                }
            }
        })();
    $(document).ready(function () {
        $('#loading').hide();
    });

    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });


</script>