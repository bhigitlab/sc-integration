<?php
/* @var $this LogController */
/* @var $model JpLog */

$this->breadcrumbs=array(
	'Jp Logs'=>array('index'),
	$model->log_id=>array('view','id'=>$model->log_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List JpLog', 'url'=>array('index')),
	array('label'=>'Create JpLog', 'url'=>array('create')),
	array('label'=>'View JpLog', 'url'=>array('view', 'id'=>$model->log_id)),
	array('label'=>'Manage JpLog', 'url'=>array('admin')),
);
?>

<h1>Update JpLog <?php echo $model->log_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>