<?php
/* @var $this ProjectsController */
/* @var $model Projects */
/* @var $form CActiveForm */
?>
<?php 
$pms_api_integration=0;
$pms_api_integration_model=ApiSettings::model()->findByPk(1);
 $pms_api_integration =$pms_api_integration_model->api_integration_settings;
?>
<div class="">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'labour-template-form',
         'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
            'validateOnType' => false,
        ),
    ));
    ?>
    <!-- Popup content-->



    <div class="panel-body">
    <input type="hidden" id="pms_integration" value="<?php echo $pms_api_integration ;?>">
        <div class="row addRow">

            <div class="col-md-6">
                <?php echo $form->labelEx($model, 'template_label'); ?>
                <?php echo $form->textField($model, 'template_label', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'template_label'); ?>
            </div>
            <div class="col-md-6">
            <?php echo $form->labelEx($model, 'labour_type'); ?>
                <ul class="checkboxList">
                    <li><input type="checkbox" id='select_alltype' value='0'>Select All</li>
                    <?php
                    $typelist = LabourWorktype::model()->findAll();
                    if ($pms_api_integration == 1) {
                        // Filter only those where pms_labour_id != 0
                        $typelist = array_filter($typelist, function ($type) {
                            return $type->pms_labour_id != 0;
                        });
                    }
                    $assigned_types_array = array();
                    $dailyLabourData='';
                    $usedLabourIds=array();
                    if (!$model->isNewRecord) {
                        // Get assigned labour types if the record is not new
                        $assigned = $model->labour_type;
                        $assigned_types = explode(',', $assigned);

                        foreach ($assigned_types as $atype) {
                            $assigned_types_array[] = $atype;
                        }

                        $existingDailyReport = Dailyreport::model()->findAllByAttributes(array('labour_template' => $model->id));
                        foreach ($existingDailyReport as $dailyReport){
                            $dailyLabourData = Yii::app()->db->createCommand()
                            ->select('*')
                            ->from('jp_daily_report_labour_details')
                            ->where(array('in', 'daily_report_id', $dailyReport->dr_id))
                            ->queryAll();
                        }
                       if(!empty($dailyLabourData)){
                        $usedLabourIds = array_column($dailyLabourData, 'labour_id');
                       }
                        

                        foreach ($typelist as $type) {
                            $isChecked = in_array($type->type_id, $assigned_types_array) ? 'checked="checked"' : '';
                            $isUnclickable = in_array($type->type_id, $usedLabourIds) ? 'onclick="return false;"' : ''; // Prevent deselect if used
                            ?>
                            <li>
                                <input type="checkbox" class="checkboxtype" <?php echo $isChecked . ' ' . $isUnclickable; ?> name="labour_type[]" value="<?php echo $type->type_id ?>" />
                                <?php echo $type->worktype ?>
                            </li>
                            <?php
                        }
                    } else {
                        foreach ($typelist as $type) {
                        ?>
                            <li><input type="checkbox" class="checkboxtype" name="labour_type[]" value='<?php echo $type->type_id ?>' /> <?php echo $type->worktype ?></li>
                        <?php
                        }
                    }
                    ?>
                </ul>
                <span style="color:red">**</span><span>NOTE: Used labour types cannot be editable to ensure accurate record-keeping </span>
            </div>
        </div>
        
        <div class="row addRow">
            <div class="col-md-6 textArea-box">
                <?php echo $form->labelEx($model, 'description'); ?>
                <?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' => 50, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'description'); ?>
            </div>
            
        </div>
       
        
    </div>
    <div class="panel-footer save-btnHold text-center">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info')); ?>
        <!--                <button data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>-->
        <?php if (!$model->isNewRecord) {

            echo CHtml::Button('Close', array('onclick' => 'closeaction(this,event)', 'class' => 'btn'));
        } else {
            echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;', 'class' => 'btn btn-default'));
            echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event)', 'class' => 'btn'));
        }
        echo CHtml::hiddenField('execute_api', 0);
        ?>
        <?php echo $form->hiddenField($model, 'pms_labour_template_id'); ?>
    </div>


    <style>
        .tableborder td {
            border: 1px solid #c2c2c2;
        }

        .checkboxList li input {
            margin-right: 5px;
        }

        .addRow label {
            display: inline-block;
        }

        input[type="radio"] {
            margin: 4px 4px 0;
        }
    </style>



    <?php $this->endWidget(); ?>

</div><!-- form -->

<script>
    $(document).ready(function() {

        //select all checkboxes
        $("#select_all").change(function() { //"select all" change
            var status = this.checked; // "select all" checked status
            $('.checkbox').each(function() { //iterate all listed checkbox items
                this.checked = status; //change ".checkbox" checked status
            });
        });

        $('.checkbox').change(function() { //".checkbox" change
            //uncheck "select all", if one of the listed checkbox item is unchecked
            if (this.checked == false) { //if this item is unchecked
                $("#select_all")[0].checked = false; //change "select all" checked status to false
            }

            //check "select all" if all checkbox items are checked
            if ($('.checkbox:checked').length == $('.checkbox').length) {
                $("#select_all")[0].checked = true; //change "select all" checked status to true
            }
        });


        $("#select_alltype").change(function() { //"select all" change
            var status = this.checked; // "select all" checked status
            $('.checkboxtype').each(function() { //iterate all listed checkbox items
                this.checked = status; //change ".checkbox" checked status
            });
        });


        $('.checkboxtype').change(function() { //".checkbox" change
            //uncheck "select all", if one of the listed checkbox item is unchecked
            if (this.checked == false) { //if this item is unchecked
                $("#select_alltype")[0].checked = false; //change "select all" checked status to false
            }

            //check "select all" if all checkbox items are checked
            if ($('.checkboxtype:checked').length == $('.checkboxtype').length) {
                $("#select_alltype")[0].checked = true; //change "select all" checked status to true
            }
        });


    });
    function onlySpecialchars(str)
    {        
        var regex = /^[^a-zA-Z0-9]+$/;
        var message ="";  
        var disabled=false;                         
        var matchedAuthors = regex.test(str);
         
        if (matchedAuthors) {
            var message ="Special characters not allowed";
            var disabled=true;   
        } 
                
        return {"message":message,"disabled":disabled};        
    }
      
    $("#template_name").keyup(function () {              
        var response =  onlySpecialchars(this.value);        
        $(this).siblings(".errorMessage").show().html(response.message).addClass('d-block');    
        $(".btn-info").attr('disabled',response.disabled);
              
    });

    $(document).ready(function() {
        document.getElementById('labour-template-form').onsubmit = function() {
        var pms_integration = $("#pms_integration").val();
        var pmsLabourTemplateId = $('#LabourTemplate_pms_labour_template_id').val();
        if(pms_integration =='1'){
            if(pmsLabourTemplateId=='0'){
                var confirmApiCall = confirm("Do you want to save the Labour Template in Pms?");
                if (confirmApiCall) {
                    document.getElementById('execute_api').value = 1;
                }
            }
            if (pmsLabourTemplateId!=0) {
                document.getElementById('execute_api').value = 1;
            }
       }
    };
    });
</script>