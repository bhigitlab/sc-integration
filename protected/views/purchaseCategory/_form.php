<?php
/* @var $this ExpenseTypeController */
/* @var $model ExpenseType */
/* @var $form CActiveForm */

function fetchCategoryTree($parent = 'NULL', $spacing = '', $user_tree_array = '') {

		  if (!is_array($user_tree_array))
		  $user_tree_array = array();
		 $tblpx = Yii::app()->db->tablePrefix;
		 if($parent == 'NULL') {
			 		 $category = Yii::app()->db->createCommand("SELECT id , category_name FROM {$tblpx}purchase_category WHERE parent_id IS NULL AND type !='S'")->queryAll();
		 } else {
			 		 $category = Yii::app()->db->createCommand("SELECT id , category_name FROM {$tblpx}purchase_category WHERE  parent_id='".$parent."' AND type !='S'")->queryAll();
		}
		 //$category = Yii::app()->db->createCommand("SELECT id , category_name FROM {$tblpx}purchase_category WHERE parent_id".$query." AND type !='S'")->queryAll();
		 foreach($category as $key=> $value)
		 {
			  $user_tree_array[] = array("id" => $value['id'], "name" => $spacing . $value['category_name']);
			  $user_tree_array = fetchCategoryTree($value['id'], $spacing. '&nbsp;&nbsp; -', $user_tree_array);
	}
		
		return $user_tree_array;
}
$categoryList = fetchCategoryTree();
//$category
?>

<div class="modal-body">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id'=>'purchase-category-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>

    <!-- Popup content-->



    <div class="clearfix">

            <div class="row addRow">
				<div class="col-md-12">
					<?php //echo $form->labelEx($model,'category_name'); ?>
                    <?php //echo $form->textField($model,'category_name',array('size'=>30,'maxlength'=>30,'required')); ?>
                    <?php //echo $form->error($model,'category_name'); ?>
                    
               
					
					<select name="parent_id" id="parent_id" style="width:200px;height:35px;border:1px solid #6d37b0;padding:5px;">
                                            <option value="">Choose Category</option>
                                            <?php
											foreach($categoryList as $key => $value){
													?>	
													<option value="<?php echo $value['id']; ?>" ><?php echo $value['name']; ?></option>
													<?php		
										}		
                                            ?>

                                    </select>
					
				</div>	

                <div class="col-md-12">
                    <?php echo $form->labelEx($model,'category_name'); ?>
                    <?php echo $form->textField($model,'category_name',array('size'=>30,'maxlength'=>30,'required')); ?>
                    <?php echo $form->error($model,'category_name'); ?>
                    
                    <!--<input type="text" name="PurchaseCategory[category_name]" id="PurchaseCategory_category_name" required style="">-->
                </div>

            </div>
            <div class="modal-footer save-btnHold">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?> 
                <?php if (Yii::app()->user->role == 1) {
                	if(!$model->isNewRecord){ /*
                 ?> 
                    <a class="btn del-btn" href="<?php echo $this->createUrl('projects/deleteprojects', array('id' => $model->type_id)) ?>" class="deletebtn" onclick="return confirm('Are you sure that you want to delete?');">Delete </a>                           
                <?php */ }} ?>
                <button data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
            </div>
            
    </div><!-- form -->

<?php $this->endWidget(); ?>


    
    <script>
      

    </script>
