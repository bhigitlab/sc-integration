INSERT INTO `jp_project_template` (`template_id`, `template_name`, `status`, `description`) VALUES (NULL, 'TYPE-5', '0', 'CONCORD');
ALTER TABLE `jp_quotation_section` ADD `hsn_code` VARCHAR(100) NULL DEFAULT NULL AFTER `unit`;
ALTER TABLE `jp_quotation_gen_category`  ADD `hsn_code` VARCHAR(100) NULL DEFAULT NULL AFTER `unit`;
ALTER TABLE `jp_sales_quotation`  ADD `hsn_code` VARCHAR(100) NULL DEFAULT NULL AFTER `unit`;
ALTER TABLE `jp_quotation_gen_worktype` ADD `hsn_code` VARCHAR(100) NULL DEFAULT NULL AFTER `unit`;
ALTER TABLE `jp_sales_quotation_master` CHANGE `template_type` `template_type` ENUM('1','2','5') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '1' COMMENT '1=>agac, 2=>d2r,5=>concord';
ALTER TABLE `jp_warehouse_stock_correction` ADD `remarks` VARCHAR(300) NULL DEFAULT NULL AFTER `approval_status`;
ALTER TABLE `jp_sales_quotation` CHANGE `quantity_nos` `quantity_nos` FLOAT NULL DEFAULT NULL;
