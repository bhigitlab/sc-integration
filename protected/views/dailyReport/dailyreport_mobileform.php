<?php

$tblpx = Yii::app()->db->tablePrefix;
$newQuery = "";
$newQuery1 = "";
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
foreach($arrVal as $arr) {
    if ($newQuery) $newQuery .= ' OR';
    if ($newQuery1) $newQuery1 .= ' OR';
    $newQuery .= " FIND_IN_SET('".$arr."', id)";
    $newQuery1 .= " FIND_IN_SET('".$arr."', company_id)";
}

$url = Yii::app()->createAbsoluteUrl("dailyReport/completiondatedaily");
$dailyid = isset($_GET['id']) ? $_GET['id'] : 0;
$pro_id=isset($_GET['pid']) ? $_GET['pid'] : 0;
$company_id=isset($_GET['company_id']) ? $_GET['company_id'] : 0;
$bookdate=isset($_GET['bookdate']) ? $_GET['bookdate'] : 0;
//echo $pro_id;
if($pro_id>0){
   $model->projectid = $pro_id;
}
if($company_id>0){
   $model->company_id = $company_id;
}
?>
<?php if (Yii::app()->user->hasFlash('success')): ?><br>
    <div class="example1consolesucces">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>
<?php if (Yii::app()->user->hasFlash('error')): ?><br>
    <div class="example1consoleerror">
    <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
    <?php endif; ?>
 <?php
Yii::app()->clientScript->registerCss('mycss', '    
   .example1consoleerror{
        padding: 5px;
        color: #fff;
        background-color: red;
        width: 120px;
        margin: 0 auto;
        text-align: center;
    }
');
?>
<div class="row addRow">
    <h2 style="padding:15px 25px;">Create Daily Report</h2>
</div>

<input type ="hidden" class="mobile_daily_id" value="<?php echo $dailyid;?>">
<div class="form col-md-6" style="margin-top:-20px">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'dailyreport-form',
        'enableAjaxValidation' => false,
    ));
    ?>
   

    <div class="clearfix">

        <div class="row addRow">
            <div class="col-md-6">
                <?php echo $form->labelEx($model, 'company_id'); ?>
                <?php
            echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array(
                                        'select' => array('id, name'),
                                        'condition' => '('.$newQuery.')',
                                        'order' => 'id',
                                        'distinct' => true
                                    )), 'id', 'name'), array('class' => 'form-control', 'style' => 'width:200px;', 'empty' => '-----------'));
            ?>

                <?php echo $form->error($model, 'company_id'); ?>
            </div>
            <div class="col-md-6">
                <?php echo $form->labelEx($model, 'projectid'); ?>
                <?php
                if (Yii::app()->user->role == 1) {
                    echo $form->dropDownList($model, 'projectid', CHtml::listData(Projects::model()->findAll(array(
                                        'select' => array('pid, name'),
                                        'condition' => '('.$newQuery1.')',
                                        //"condition" => 'pid in (select projectid from project_assign where userid=' . Yii::app()->user->id . ')',
                                        'order' => 'name',
                                        'distinct' => true
                                    )), 'pid', 'name'), array('class' => 'form-control ','empty' => '-----------'));
                } else {
                    echo $form->dropDownList($model, 'projectid', CHtml::listData(Projects::model()->findAll(array(
                                        'select' => array('pid, name'),
                                        'condition' => '('.$newQuery1.')',
                                        "condition" => 'pid in (select projectid from jp_project_assign where userid=' . Yii::app()->user->id . ')',
                                        'order' => 'name',
                                        'distinct' => true
                                    )), 'pid', 'name'), array('class' => 'form-control', 'empty' => '-----------'));
                }
                ?>

                <?php echo $form->error($model, 'projectid'); ?>
            </div>
        </div>
            <?php //print_r($completiondate); ?>
            <div class="row addRow">	
            <div class="col-md-6">
                <label>Completion date:</label><input type="text" id="comple_date" class="form-control"  readonly="true" value = "<?php echo ((!$model->isNewRecord) ? date('d-M-Y', strtotime($completiondate)) : '') ?>" />
            </div>

            <div class="col-md-6">
                <?php echo $form->labelEx($model, 'book_date'); ?>
                <?php 
                
               
                //$model->book_date = ($model->book_date=='1970-01-01' || $model->book_date=='0000:00:00') ? date('Y-m-d') : $model->book_date;  
               // echo CHtml::activeTextField($model, 'book_date', array('readonly' => 'true', "value" => (($model->isNewRecord) ? date('d-M-Y') : date('d-M-Y', strtotime($model->book_date))), 'size' => 10, 'class' => 'form-control')); 
                if($bookdate != 0)
                {
				$model->book_date = 	$bookdate;
				echo CHtml::activeTextField($model, 'book_date', array('size' => 10, 'class' => 'form-control'));
					
					$this->widget('application.extensions.calendar.SCalendar', array(
						'inputField' => 'Dailyreport_book_date',
						'ifFormat' => '%d-%b-%Y',
					));
				}else{
					if($dailyid == 0){
					
					$model->book_date =  date('d-M-Y', strtotime(date('Y-m-d')));
					echo CHtml::activeTextField($model, 'book_date', array('size' => 10, 'class' => 'form-control'));
					
					$this->widget('application.extensions.calendar.SCalendar', array(
						'inputField' => 'Dailyreport_book_date',
						'ifFormat' => '%d-%b-%Y',
					));
					}else{
						
				   // $model->book_date = $model->book_date;
					echo CHtml::activeTextField($model, 'book_date', array('value' =>date('d-M-Y', strtotime($model->book_date)), 'size' => 10, 'class' => 'form-control')); 
					$this->widget('application.extensions.calendar.SCalendar', array(
						'inputField' => 'Dailyreport_book_date',
						'ifFormat' => '%d-%b-%Y',
					));
					}
               }
                ?>
                <?php //echo $form->textField($model,'book_date', array('size' => 10, 'class' => 'form-control')); ?>
               <?php 
               /* $this->widget('application.extensions.calendar.SCalendar', array(
                    'inputField' => 'Dailyreport_book_date',
                    'ifFormat' => '%d-%b-%Y',
                )); */
                ?>
                <?php echo $form->error($model, 'book_date'); ?>
            </div>
        </div>
        <div class="row addRow">
			<div class="col-md-12 line-seperator_dailymobile"></div>
            <div class="col-md-12">
                <div class="mob_addrow clearfix">
                    <div class="pull-right">
                        <button type="button" class="btn btn-sm btn-primary" id="add_row_work_done">Add Row</button>
                    </div>
                    <h4 class="pull-left">Work Done</h4>
                    
                </div>
                <div id="addwork_done"></div>

                <?php
                $workdons = array();
                $workcnt = $extraworkcnt = 0;
                $workdons = json_decode($model->wrktype_and_numbers);
                
                $types = '';
                if ($model->projectid != '') {
                    $id = $model->projectid;
                    $types = $list = Yii::app()->db->createCommand("SELECT DISTINCT {$tblpx}work_type.wtid,{$tblpx}work_type.work_type FROM {$tblpx}work_type LEFT JOIN {$tblpx}project_work_type ON {$tblpx}project_work_type.wrk_type_id = {$tblpx}work_type.wtid WHERE {$tblpx}project_work_type.project_id = '" . $id . "'")->queryAll();
                }
                
                if ($dailyid != 0) {
                    foreach ($workdons as $workdon) {
                        if ($workdon->work_done == 0) {
                            $workcnt++;
                            ?>
                            <div class="row workdone_div">
                                <div class="col-md-4">
                                    <input type="hidden" name="work_done_type[]" value="<?= $workdon->work_done; ?>">
                                    <label>Type</label>
                                    <select class="form-control typelist" id="worktype_list" name="type[]" value="<?= $workdon->work_type; ?>">
                                        <option>Select type</option>
                                        <?php foreach ($types as $type) { ?>
                                            <option value="<?= $type['wtid']; ?>"
                                            <?php
                                            if ($type['wtid'] == $workdon->work_type) {
                                                echo 'selected="true"';
                                            }
                                            ?>   ><?= $type['work_type']; ?></option>
                                                <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Numbers</label>
                                    <input type="text" class="form-control typelisting" name="numbers[]" value="<?= $workdon->numbers; ?>">
                                </div>
                                <div class="col-md-4">
                                    <label>Work Done</label>
                                    <input type="text" class="form-control" name="work_done[]" value="<?= $workdon->works_done; ?>">
                                </div>
                            </div>

                            <?php
                        }
                    }
                }
                ?>
                <?php if ($workcnt == 0) { ?>
                    <div class="row workdone_div">
                        <div class="col-md-4">
                            <input type="hidden" name="work_done_type[]" value="0">
                            <label>Type</label>
                            <select class="form-control typelist" id="worktype_list" name="type[]">
                                <!--option>Select type</option-->
                                <?php if ($model->projectid != '') { ?>
								<option>Select type</option>
                                <?php foreach ($types as $type) { ?>
                                <option value="<?= $type['wtid']; ?>"><?= $type['work_type']; ?></option>
                               <?php } } else {?>
							<option>Select type</option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Numbers</label>
                            <input type="text" class="form-control typelisting" name="numbers[]">
                        </div>
                        <div class="col-md-4">
                            <label>Work Done</label>
                            <input type="text" class="form-control" name="work_done[]">
                        </div>
                    </div>
                <?php } ?>
                
            </div>
		
			<div class="col-md-12 line-seperator_dailymobile"></div>
			
            <div class="col-md-12">
                <div class="mob_addrow clearfix">
                    <div class="pull-right">
                        <button type="button" class="btn btn-sm btn-primary" id="add_row_extrawork_done">Add Row</button>
                    </div>
                    <h4 class="pull-left">Extra Work Done</h4>
                    
                </div>
                <div id="addextrawork_done"></div>
                <?php
                if ($dailyid != 0) {
                    foreach ($workdons as $workdon) {
                        if ($workdon->work_done == 1) {
                            $extraworkcnt++;
                            ?>
                            <div class="row extra_workdone_div">
                                <div class="col-md-4">
                                    <input type="hidden" name="work_done_type[]" value="<?= $workdon->work_done; ?>">
                                    <label>Type</label>
                                    <select class="form-control typelist" name="type[]" value="<?= $workdon->work_type; ?>">
                                        <option>Select type</option>
                                        <?php foreach ($types as $type) { ?>
                                            <option value="<?= $type['wtid']; ?>"
                                            <?php
                                            if ($type['wtid'] == $workdon->work_type) {
                                                echo 'selected="true"';
                                            }
                                            ?>   ><?= $type['work_type']; ?></option>
                                                <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Numbers</label>
                                    <input type="text" class="form-control typelisting" name="numbers[]" value="<?= $workdon->numbers; ?>">
                                </div>
                                <div class="col-md-4">
                                    <label>Work Done</label>
                                    <input type="text" class="form-control" name="work_done[]" value="<?= $workdon->works_done; ?>">
                                </div>
                            </div>

                            <?php
                        }
                    }
                }
                ?>
                <?php if ($extraworkcnt == 0) { ?>
                    <div class="row extra_workdone_div">
                        <div class="col-md-4">
                            <input type="hidden" name="work_done_type[]" value="1">
                            <label>Type</label>
                            <select class="form-control typelist" name="type[]">
                                <!--option>Select type</option-->
                                
                                <?php if ($model->projectid != '') { ?>
								<option>Select type</option>
                                <?php foreach ($types as $type) { ?>
                                <option value="<?= $type['wtid']; ?>"><?= $type['work_type']; ?></option>
                               <?php } } else {?>
								<option>Select type</option>
                                <?php } ?>
								
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Numbers</label>
                            <input type="text" class="form-control typelisting" name="numbers[]">
                        </div>
                        <div class="col-md-4">
                            <label>Extra Work Done</label>
                            <input type="text" class="form-control" name="work_done[]">
                        </div>
                    </div>
                <?php } ?>
                
            </div>

        </div>
        <div class="row addRow">
            <input type="hidden" id="hdtest"  name="hdtest">
            <div class="col-md-12">
                <div id="example1" class="hot handsontable htColumnHeaders"></div>
            </div>
        </div> 


        <div class="row addRow">
            <div class="col-md-3">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Submit', array('class' => 'btn pull-left btn-info', 'id' => 'Save','style'=>'margin-bottom:30px;')); ?>
                <?php echo CHtml::resetButton('Clear', array('class' => 'btn pull-right btn-info','onclick' => 'javascript:location.href="' . $this->createUrl('dailyReport/addreport',array('id' =>0)) . '"')); ?>
            </div>
            <div class="col-md-8"></div>
            
        </div>
        <br/>
    </div>
    <?php $this->endWidget(); ?>
</div>


<style type="text/css">
    .row{
        padding:4px 0px;
    }

    .subrow{
        width: 125px;
    }
    
</style>
<?php $geturl = Yii::app()->createAbsoluteUrl("dailyReport/getdata"); ?>
<?php $redirecturl = Yii::app()->createAbsoluteUrl("dailyReport/redirecturl"); ?>
<?php $redurl = Yii::app()->createAbsoluteUrl("dailyReport/addreport&id="); ?>
<?php $newredurl = Yii::app()->createAbsoluteUrl("dailyReport/newredirecturl"); ?>


<?php //echo $worktype_list;?>
<script type="text/javascript">
    $(document).ready(function(){
//        alert("<?php //echo $worktype_list; ?>");
    <?php if($pro_id!=0 && $dailyid==0){?>
        var types=" <?php echo addslashes($worktype_list);?> ";
        $(".typelist").html("<?php echo addslashes($worktype_list); ?>");
   <?php } ?>
    });
    
    
    
    <?php if($dailyid!=0 && $pro_id!=0 ){?>
    
        var types=" <?php echo addslashes($worktype_list);?> ";
        
   <?php  }
   else{?> 
    var types=" <?php echo addslashes($worktype_list);?> ";
  <?php } ?>
   
</script>

<?php Yii::app()->clientScript->registerScript('myscript', '
   // var selectedItem = sessionStorage.getItem("SelectedItem");  
   // $("#Dailyreport_projectid").val(selectedItem);
    $(document).ready(function(){
    
    var res;
    var res1;
    var selectedItem3 = sessionStorage.getItem("SelectedItem3");
    document.getElementById("comple_date").value=selectedItem3;
    
    var selectedItem4 = sessionStorage.getItem("SelectedItem4");
    document.getElementsByClassName("worktype_list").value=selectedItem4;


    $("#Dailyreport_projectid").change(function(){
   
    // var dropVal = $(this).val();
    // sessionStorage.setItem("SelectedItem", dropVal);
    var pro_id= $("#Dailyreport_projectid").val();
    var dailydate= $("#Dailyreport_book_date").val();
    var dailyid= "' . Yii::app()->user->id. '";
    var company= $("#Dailyreport_company_id").val();
    pro_ids = pro_id+"/"+company ;
     if(pro_id!="" && dailydate!="" || pro_id!="" && dailydate==""){
        $.ajax({
            type: "get",
            data:{projectid:pro_ids,bookdate: dailydate, dailyid:dailyid},
            url: "' . $redirecturl . '",
            dataType: "json",
            success:function(data){
                document.getElementById("comple_date").value=data.completion_date;
                jsonArr = JSON.parse(JSON.stringify(data)); 
                    types=jsonArr.worktype_list;
                    $(".typelist").html(types);
                sessionStorage.setItem("SelectedItem3", data.completion_date);
                sessionStorage.setItem("SelectedItem4", data.worktype_list);
                //alert(data.worktype_list);
                window.location.href = "'.$redurl.'"+data.record+"&pid="+pro_id+"&company_id="+company;
                    
            }
            
        });
        
    $.ajax({
       url: "' . $url . '",
        data: {"id": pro_id,"date":dailydate}, 
        dataType: "json",
        type: "POST",
        success:function(data){
        //alert(data.worktype);
            jsonArr = JSON.parse(JSON.stringify(data)); 
            types=jsonArr.worktype;
            //alert(types);
            $(".typelist").html(types);
        }
});
}


});
var selectedItem2 = sessionStorage.getItem("SelectedItem2");  

//$("#Dailyreport_book_date").val(selectedItem2);
$(document).on("click",".daysrow",function() {
 //alert("hi");
    var dropVal2 = $(Dailyreport_book_date).val();
    sessionStorage.setItem("SelectedItem2", dropVal2);
    var pro_id= $("#Dailyreport_projectid").val();
    var dailydate= $("#Dailyreport_book_date").val();
   
   //alert(dailydate);
   
     var dailyid= "' . Yii::app()->user->id. '";
     if(pro_id!="" && dailydate!=""){
        $.ajax({
            type: "get",
            data:{projectid:pro_id,bookdate: dailydate, dailyid:dailyid},
            url: "' . $redirecturl . '",
            dataType: "json",
            success:function(data){
			   if(data.record != 0){
					window.location.href =  "'.$redurl.'"+data.record+"&pid="+pro_id ; 
				}else{
					window.location.href =  "'.$redurl.'"+data.record+"&pid="+pro_id+"&bookdate="+dailydate ;
				}
            }
        });
    $.ajax({
       url: "' . $url . '",
        data: {"id": pro_id,"date":dailydate}, 
        dataType: "json",
        type: "POST",
        success:function(data){
            document.getElementById("comple_date").value=data.completion_date;
             jsonArr = JSON.parse(JSON.stringify(data)); 
             var data=jsonArr.worktype;
             var data1=jsonArr.result;
             var data2=jsonArr.result1;
             res=data1;
             res1=data2;
            $(".typelist").html(data);
            return data1;
            return data2;

        }
        
});
}

});



 
$(document).on("click", "#add_row_work_done", function(){
	
	
	$(".workdone_div :input[name=\"numbers[]\"]").each(function() {
    var value = $(this).val();
    if(value != ""){	 
	}else{
		
		alert("Please enter all fields");
		xhr.abort();
	}
	});
	
	$(".workdone_div :input[name=\"work_done[]\"]").each(function() {
    var val = $(this).val();
    if(val != ""){	 
	}else{
		
		alert("Please enter all fields");
		xhr.abort();
		// $("#add_row_work_done").attr("disabled","disabled");
		
	}
	});
	
	$("#addwork_done :input[name=\"numbers[]\"]").each(function() {
    var value = $(this).val();
    if(value != ""){	 
	}else{
		
		alert("Please enter all fields");
		xhr.abort();
	}
	});
	
	$("#addwork_done :input[name=\"work_done[]\"]").each(function() {
    var val = $(this).val();
    if(val != ""){	 
	}else{
		
		alert("Please enter all fields");
		xhr.abort();
		// $("#add_row_work_done").attr("disabled","disabled");
		
	}
	});
	
	var data=$("#dailyreport-form").serialize();
	//alert(data);
		var xhr =	$.ajax({
			    url:"'.Yii::app()->createUrl("dailyReport/savemobiledata&id=".$dailyid."&pid=".$pro_id).'",
				data: data, 
				dataType: "json",
				type: "POST",
				success:function(response){
					if(response.response == "success"){
						
						if(response.status == "empty"){
						window.location.href =  "'.$redurl.'"+response.id+"&pid="+response.proid ; 
						}
						else{
						$(".dleet_workdone").hide();
						var html ="";
						html += "<input type=\"hidden\" name=\"work_done_type[]\" value=\"0\">";
						html += "<div class=\"row addwork_done\"><div class=\"col-md-4\"><label>Type</label><select class=\"form-control typelist typeappend\" name=\"type[]\">"+types+"</select></div>";
						html +="<div class=\"col-md-4\"><label>Numbers</label><input type=\"text\" class=\"form-control\" name=\"numbers[]\"></div>";
						html +="<div class=\"col-md-4\"><label>Work Done</label><input type=\"text\" class=\"form-control\" name=\"work_done[]\"></div></div>";
						$("#addwork_done").prepend(html);
						}
					}if(response.response == "fail"){
						alert("Please enter fields");
					}if(response.response == "error"){
						alert("Save Error");
					}

				}
				
		});
	
    
    
    
    
});

$(document).on("click", "#add_row_extrawork_done", function(){
	
	
	$(".extra_workdone_div :input[name=\"numbers[]\"]").each(function() {
    var value = $(this).val();
    if(value != ""){	 
	}else{
		alert("Please enter all fields");
		xhr.abort();
	}
	});
	
	$(".extra_workdone_div :input[name=\"work_done[]\"]").each(function() {
    var val = $(this).val();
    if(val != ""){	 
	}else{
		alert("Please enter all fields");
		xhr.abort();
	}
	});
	
	
	$("#addextrawork_done :input[name=\"numbers[]\"]").each(function() {
    var value = $(this).val();
    if(value != ""){	 
	}else{
		alert("Please enter all fields");
		xhr.abort();
	}
	});
	
	$("#addextrawork_done :input[name=\"work_done[]\"]").each(function() {
    var val = $(this).val();
    if(val != ""){	 
	}else{
		alert("Please enter all fields");
		xhr.abort();
	}
	});
	
	
	var data=$("#dailyreport-form").serialize();
	//alert(data);
	var xhr = $.ajax({
		url:"'.Yii::app()->createUrl("dailyReport/savemobiledata&id=".$dailyid."&pid=".$pro_id).'",
		data: data, 
		dataType: "json",
		type: "POST",
		success:function(response){
			if(response.response == "success"){
				
				if(response.status == "empty"){
				window.location.href =  "'.$redurl.'"+response.id+"&pid="+response.proid ; 
				}
				else{
				$(".dleet_workdone").hide();
				var html ="";
				html += "<input type=\"hidden\" name=\"work_done_type[]\" value=\"1\">";
				html += "<div class=\"row addextrawork_done\"><div class=\"col-md-4\"><label>Type</label><select class=\"form-control typelist typeappend\" name=\"type[]\">"+types+"</select></div>";
				html +="<div class=\"col-md-4\"><label>Numbers</label><input type=\"text\" class=\"form-control\" name=\"numbers[]\"></div>";
				html +="<div class=\"col-md-4\"><label>Extra Work Done</label><input type=\"text\" class=\"form-control\" name=\"work_done[]\"></div></div>";
				$("#addextrawork_done").prepend(html);
				}
			}if(response.response == "fail"){
				alert("please enter fields");
			}if(response.response == "error"){
				alert("Save Error");
			}

		}
		
	});
   
});

/*
$(document).on("click", ".dleet_workdone", function(){
	//$(this).find("div.addwork_done").remove();   
	$( ".addwork_done" ).remove();
    //$(this).parent().parent(".row").remove();

});

$(document).on("click", ".dleet_workdone1", function(){
	//$(this).find("div.addextrawork_done").remove();   
	$( ".addextrawork_done" ).remove();
    //$(this).parent().parent(".row").remove();

});

*/


$(".flash-success").animate({opacity: 1.0}, 5000).fadeOut("slow");




    });
        '); ?>
<script>
$("#Dailyreport_company_id").change(function(){
	var val =$(this).val();
        if(val != ''){
            $("#Dailyreport_projectid").html('<option value="">Select Project</option>');
		$.ajax({
                url:'<?php echo Yii::app()->createUrl('dailyReport/dynamicproject'); ?>',
                method:'POST',
                data:{company_id:val},
                dataType:"json",
                success:function(response){
                     $("#Dailyreport_projectid").html(response.html);
                }
			
		})
        } else{
            $("#Dailyreport_projectid").html('<option value="">Select Project</option>');
        }
		
})
</script>