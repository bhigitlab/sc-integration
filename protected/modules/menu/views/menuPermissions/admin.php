<?php
/* @var $this MenuPermissionsController */
/* @var $model MenuPermissions */

$this->breadcrumbs=array(
	'Menu Permissions'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List MenuPermissions', 'url'=>array('index')),
	array('label'=>'Create MenuPermissions', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('menu-permissions-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Menu Permissions</h1>



<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'menu-permissions-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
		//'mp_id',
                array(
                    'name' => 'username',
                    'value'=>'$data->user ? $data->user->first_name: "-"' 
                    
                ),
		//'user.first_name',
                array(
                    'name' => 'menuname',
                    'value'=>'$data->menu ? $data->menu->menu_name: "-"' 
                    
                ),
		//'menu.menu_name',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
