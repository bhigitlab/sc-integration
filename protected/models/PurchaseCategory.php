<?php

/**
 * This is the model class for table "{{purchase_category}}".
 *
 * The followings are the available columns in table '{{purchase_category}}':
 * @property integer $id
 * @property integer $parent_id
 * @property string $category_name
 * @property string $specification
 * @property string $spec_flag
 * @property integer $created_by
 * @property string $created_date
 */
class PurchaseCategory extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public $rules_array = array();
    public $company_id;

    public function tableName() {
        return '{{purchase_category}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {

        return array(
            array('category_name, spec_flag, created_by, created_date, company_id', 'required'),
           
            array('parent_id, created_by', 'numerical', 'integerOnly' => true),
            array('category_name', 'length', 'max' => 100),
            array('specification', 'length', 'max' => 255),
            array('spec_flag', 'length', 'max' => 1),
            //array('category_name', 'unique'),
           array('category_name', 'unique', 'caseSensitive' => false, 'criteria' => array(
                'condition' => 'parent_id IS NULL',
            )),
            array('company_id', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, parent_id, category_name, specification, spec_flag, created_by, created_date, company_id', 'safe', 'on' => 'search'),
        );
    }

    public function checkexist() {
        $id = PurchaseCategory::model()->findByAttributes(array('id' => $this->id));
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        if (!empty($id)) {
            $name = PurchaseCategory::model()->findByAttributes(array('category_name' => $this->category_name), array('condition' => 'id != ' . $this->id . ' AND(' . $newQuery . ')'));
            if (!empty($name)) {
                $this->addError("category_name", ("Category name already exists"));
            }
        } else {
            $name = PurchaseCategory::model()->findByAttributes(array('category_name' => $this->category_name), array('condition' => $newQuery));
            if (!empty($name)) {
                $this->addError("category_name", ("Category name already exists"));
            }
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'parent_id' => 'Items ',
            'category_name' => 'Category Name',
            'specification' => 'Specification',
            'spec_flag' => 'Spec Flag',
            'created_by' => 'Created By',
            'unit' => 'Unit',
            'created_date' => 'Created Date',
            'company_id' => 'Company',
            'hsn_code' =>'HSN Code'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('parent_id', NULL);
        $criteria->compare('category_name', $this->category_name, true);
        $criteria->compare('specification', $this->specification, true);
        $criteria->compare('unit', $this->unit, true);
        $criteria->compare('spec_flag', $this->spec_flag, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('company_id', $this->company_id);
        $criteria->compare('hsn_code', $this->hsn_code);
        $criteria->addCondition('spec_flag= "N" AND parent_id IS NULL');
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        $criteria->order = 'id DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => false,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PurchaseCategory the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
