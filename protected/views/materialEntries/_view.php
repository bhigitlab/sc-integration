<?php
/* @var $this SubcontractorController */
/* @var $data Subcontractor */
?>

<?php
$tblpx = Yii::app()->db->tablePrefix;
$id= $data['id'];
?>



<?php
if ($index == 0) {
?>

    <thead>
        <tr>
            <th style="width:20px;">Sl No.</th>
            <th>Company</th>
            <th>Project</th>
            <th>Sub Contractor</th>
            <th>Expense Head</th>
            <th>Date</th> 
            <th>Description</th>
            <th>Material</th>
                               
        </tr>
    </thead>
    
<?php
}

?>

<tr>
    <td style="width:20px;"><?php echo $index + 1; ?></td>
    <td>
        <?php
        $company = Company::model()->findByPk($data['company_id']);
        echo $company['name'];
        ?>
    </td>
    <td>
        <?php
        $projectModel = Projects::model()->findByPk($data['project_id']);
        echo !empty($projectModel)?CHtml::encode($projectModel->name):"";
        ?>
    </td>
    <td>
        <?php
        $subModel = Subcontractor::model()->findByPk($data['subcontractor_id']);
        echo CHtml::encode($subModel->subcontractor_name);
        ?>
    </td>
    <td>
        <?php
        $expensehead = ExpenseType::model()->findByPk($data["expensehead_id"]);
        echo CHtml::encode($expensehead->type_name);
        ?>
    </td>
    <td style="white-space:nowrap;"><?php echo CHtml::encode($data['Date']); ?></td>
    <td style="white-space:nowrap;"><?php echo CHtml::encode($data['description']); ?></td>
    <td>
    <?php
    // Assuming $id is the material_entry_id
    $materialItems = MaterialItems::model()->findAllByAttributes(['material_entry_id' => $id]);
    $materialData = [];

    if ($materialItems) {
        foreach ($materialItems as $material) {
            // Find the corresponding material in the Materials model
            $materialDetail = Materials::model()->findByPk($material->material);
            
            if ($materialDetail) {
                // Encode the 'material_name' value and add it to the array
                $materialData[] = CHtml::encode($materialDetail->material_name);
            }
        }
        
        echo implode(", ", $materialData);
    } else {
        echo "No material found.";
    }
    ?>


    </td>
    
</tr>
