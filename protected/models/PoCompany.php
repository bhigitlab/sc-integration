<?php

/**
 * This is the model class for table "{{po_company}}".
 *
 * The followings are the available columns in table '{{po_company}}':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $address
 * @property string $pincode
 * @property string $phone
 * @property string $email_id
 * @property string $company_gstnum
 * @property integer $company_id
 * @property integer $created_by
 * @property string $created_date
 */
class PoCompany extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{po_company}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, description, address, pincode, phone, email_id, company_gstnum, created_by, created_date', 'required'),
			array('company_id, created_by', 'numerical', 'integerOnly'=>true),
			array('name, pincode', 'length', 'max'=>100),
			array('phone, email_id, company_gstnum', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, description, address, pincode, phone, email_id, company_gstnum, company_id, created_by, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'description' => 'Description',
			'address' => 'Address',
			'pincode' => 'Pincode',
			'phone' => 'Phone',
			'email_id' => 'Email',
			'company_gstnum' => 'Company GST NO',
			'company_id' => 'Company',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('pincode',$this->pincode,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email_id',$this->email_id,true);
		$criteria->compare('company_gstnum',$this->company_gstnum,true);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PoCompany the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
