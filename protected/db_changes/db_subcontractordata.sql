ALTER TABLE `jp_pre_expenses` DROP FOREIGN KEY `exp_sub_fk`;
ALTER TABLE `jp_subcontractor` CHANGE `subcontractor_id` `subcontractor_id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `jp_pre_expenses` ADD CONSTRAINT `exp_sub_fk1` FOREIGN KEY (`subcontractor_id`) REFERENCES `jp_subcontractor`(`subcontractor_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
INSERT INTO `jp_subcontractor` (`subcontractor_id`, `subcontractor_name`, `subcontractor_phone`, `subcontractor_email`, `subcontractor_status`, `subcontractor_description`, `gst_no`, `company_id`, `expense_type_id`, `created_date`, `created_by`) VALUES
(1, 'Sunil', '1236547890', 'pc@bluehorizoninfotech.com', 1, 'Sunil', '3543567658778', '1', NULL, '2018-02-02 17:12:49', 4),
(2, 'Basheer', '1236547890', 'pc@bluehorizoninfotech.com', 1, 'Basheer', '4766876997856765765', '1', NULL, '2018-02-02 17:13:31', 4),
(3, 'Rajesh', '1236547890', 'pc@bluehorizoninfotech.com', 1, 'Rajesh', '3423453465546456546', '1', NULL, '2018-02-02 17:14:19', 4),
(4, 'Yeldho', '1236547890', 'pc@bluehorizoninfotech.com', 1, 'Yeldho', '23534654756765746657', '1', NULL, '2018-02-02 17:14:43', 4),
(5, 'SAP Pallikkara ', '1236547890', 'pc@bluehorizoninfotech.com', 1, 'SAP Pallikkara ', '5687689767575676587', '1', NULL, '2018-02-02 17:15:48', 4),
(6, 'CEMEX ENGINEERS', '2375642', 'pc@bluehorizoninfotech.com', 1, 'Scaffoldings', '32AABFC4767B1ZY', '1', NULL, '2018-07-14 12:07:14', 96),
(7, 'Dorma India Pvt Ltd', '123456', 'pc@bluehorizoninfotech.com', 1, 'Installation of Automatic Doors', '33AAACD3980D1Z1', '1', NULL, '2018-07-21 15:18:28', 96),
(8, 'SAFA FACADE SYSTEM', '9995841114', 'pc@bluehorizoninfotech.com', 1, 'Installation labour charges\r\n', '32ADKFS9265LIZF', '1', NULL, '2018-08-01 15:22:56', 96),
(9, 'Viswakarma Logistics Pvt Ltd', '9444026008', 'pc@bluehorizoninfotech.com', 1, 'Freight Charges', '32AAHFS9495E1ZV', '1', NULL, '2018-08-01 16:12:49', 96),
(10, 'Rocky Transports & Crane Services', '7902228927', 'pc@bluehorizoninfotech.com', 1, 'Crane Services ', '32ACMPR3368L', '1', NULL, '2018-11-19 12:36:31', 96),
(11, 'Kripa Associates', '9847060631', 'pc@bluehorizoninfotech.com', 1, 'Crane Services', '32BCTPP4802P1Z0', '1', NULL, '2018-11-19 12:49:07', 96),
(12, 'Drive Road Assistance', '1234567', 'pc@bluehorizoninfotech.com', 1, 'Crane service, Transporation ', '123456789', '1', NULL, '2018-11-21 11:22:36', 96),
(13, 'Unity Scaffoldings', '9898989898', 'pc@bluehorizoninfotech.com', 1, 'Scaffoldings', '', '1', NULL, '2019-08-01 14:31:26', 96),
(14, 'Sreeraj', '9898989898', 'pc@bluehorizoninfotech.com', 1, 'Steel Work', '', '1', NULL, '2019-08-01 14:57:43', 96),
(15, 'SAP Kurishupally', '04844029143', 'pc@bluehorizoninfotech.com', 1, 'SAP Kurisupally . Aluminium fabrication works', '', '1', NULL, '2019-09-20 15:44:02', 96),
(16, 'Safe Road Transports ', '9898989898', 'pc@bluehorizoninfotech.com', 1, 'transportation ', '', '1', NULL, '2020-05-28 11:16:30', 96);
