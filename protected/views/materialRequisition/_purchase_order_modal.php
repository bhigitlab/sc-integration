<?php
$tblpx = Yii::app()->db->tablePrefix;
$purchase_details = Yii::app()->db->createCommand("SELECT purchase_no FROM jp_purchase WHERE purchase_no IS NOT NULL ORDER BY p_id DESC LIMIT 0,1")->queryRow();
$lastpurchase_no = $purchase_details['purchase_no'];
if (is_numeric($lastpurchase_no)) {
    $lastpurchase_no = $lastpurchase_no + 1;
} else {
    $p_no = substr($lastpurchase_no, 0, -1);
    $last = substr($lastpurchase_no, -1);
    if (is_numeric($last)) {
        $last_no = $last + 1;
    } else {
        $last_no = $last . '1';
    }
    $lastpurchase_no = $p_no . $last_no;
}

$purchase_no = Purchase::model()->findAll(array("condition" => "purchase_no = '$lastpurchase_no'"));
if (!empty($purchase_no)) {
    if (is_numeric($lastpurchase_no)) {
        $lastpurchase_no = $lastpurchase_no + 1;
    } else {
        $p_no = substr($lastpurchase_no, 0, -1);
        $last = substr($lastpurchase_no, -1);
        if (is_numeric($last)) {
            $last_no = $last + 1;
        } else {
            $last_no = $last . '1';
        }
        $lastpurchase_no = $p_no . $last_no;
    }
}

$company_details = Company::model()->findByPk(Yii::app()->user->company_id);
$companyList    = Yii::app()->user->company_ids;
?>

<div class="invoicemaindiv">
        <!-- <h2 class="purchase-title">Purchase Order</h2> -->
    <!-- <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div> -->
        <div id="msg_box"></div>
        <form id="pdfvals1" method="post" action="">
            <input type="hidden" name="remove" id="remove" value="">
            <input type="hidden" name="purchase_id" id="purchase_id" value="0">
            <div class="block_purchase po-add">
                <div class="row">
                    <div class="col-sm-3 col-lg-2">
                        <div class="form-group select2-width">
                            <label>COMPANY : 
                                <a data-toggle="modal" href="<?php echo  Yii::app()->createAbsoluteUrl('company/create&layout=1'); ?>" data-target="#myModal1">Add New</a></label>
                            <?php
                            $user = Users::model()->findByPk(Yii::app()->user->id);
                            $arrVal = explode(',', $user->company_id);
                            $newQuery = "";
                            foreach ($arrVal as $arr) {
                                if ($newQuery) $newQuery .= ' OR';
                                $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
                            }
                            $companyInfo = Company::model()->findAll(array('condition' => $newQuery));
                            ?>
                            <select name="company" class="inputs target company change_val" id="company_id" >
                                <option value="">Choose Company</option>
                                <?php
                                foreach ($companyInfo as $key => $value) {
                                ?>
                                    <option value="<?php echo $value['id'] ?>"><?php echo $value['name']; ?></option>
                                <?php
                                }
                                ?>

                            </select>
                        </div>
                    </div>

                    <div class="col-sm-3 col-lg-2">
                        <div class="form-group select2-width">
                            <label>PROJECT : 
                                <a data-toggle="modal" href="<?php echo  Yii::app()->createAbsoluteUrl('projects/create&layout=1'); ?>" data-target="#myModal2">Add New</a></label>
                            <select name="project" class="inputs target project change_val" id="project">
                                <option value="">Choose Project</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3 col-lg-2">
                        <div class="form-group select2-width">
                            <label>EXPENSE HEAD : 
                                <a data-toggle="modal" href="<?php echo  Yii::app()->createAbsoluteUrl('expensetype/create&layout=1'); ?>" data-target="#myModal3" class="myModal3">Add New</a></label>
                            <select name="expense_head" class="inputs target expense_head change_val" id="expense_head">
                                <option value="">Choose Expense Head</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-3 col-lg-2">
                        <div class="form-group select2-width">
                            <label>VENDOR : 
                                <a data-toggle="modal" href="<?php echo  Yii::app()->createAbsoluteUrl('vendors/create&layout=1'); ?>" data-target="#myModal4">Add New</a></label>
                            <select name="vendor" class="inputs target vendor change_val" id="vendor">
                                <option value="">Choose Vendor</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3 col-lg-1 desktop-only-padding-left-0">
                        <div class="form-group">
                            <label class="po-add-label">DATE : </label>
                            <input type="text" value="<?php echo ((isset($date) && $date != '') ? date("d-m-Y", strtotime($date)) : ''); ?>" id="datepicker" class="txtBox date inputs target form-control change_val" name="date" placeholder="Please click to edit">
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-3 d-flex">
                        <div class="form-group margin-bottom-20">
                            <label class="po-add-label">PURCHASE NO : </label>
                            <input type="text" required value="<?php echo ((isset($purchaseno) && $purchaseno != '') ? $purchaseno : ''); ?>" class="txtBox inputs target check_type purchaseno form-control change_val" name="purchaseno" id="purchaseno">
                        </div>
                        &nbsp;&nbsp;
                        <div class="form-group tab-only-padding-left-20">
                            <label class="po-add-label">CONTACT NO : </label>
                            <?php
                            $user = Users::model()->findByPk(Yii::app()->user->id);                            
                            $contact_no = $user->phonenumber;
                            ?>
                            <input type="text" required value="<?php echo ((isset($contact_no) && $contact_no != '') ? $contact_no : ''); ?>" class="txtBox inputs target check_type purchaseno form-control change_val" name="contact_no" id="contact_no" placeholder="">
                            <div class="errorMessage"></div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-lg-2">
						<div class="form-group">
							<label>EXPECTED DELIVERY DATE : </label>
							<input type="text"  value="" class="txtBox inputs target check_type delivery_date form-control" name="delivery_date" id="delivery_date" placeholder="">
						</div>
					</div>
                    <div class="col-sm-6 col-lg-4">
                        <div class="form-group">
                            <label>SHIPPING ADDRESS : </label>
                            <textarea name="shipping_address" id="shipping_address" class="form-control change_val" style="height:100px;"></textarea>
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="inclusive" name="inclusive">
                            <label class="form-check-label" for="inclusive">Inclusive of GST
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group">
                            <label>DESCRIPTION : </label>
                            <textarea name="po_description" id="po_description" class="form-control change_val"></textarea>
                        </div>
                    </div>
                </div>
            </div>

        </form>
   <script>
    $(document).ready(function() {
        $(function() {
            $("#datepicker").datepicker({
                dateFormat: 'dd-mm-yy'
            }).datepicker("setDate", new Date());
            $("#delivery_date").datepicker({
                dateFormat: 'dd-mm-yy'
            });
        });
    });
   </script>