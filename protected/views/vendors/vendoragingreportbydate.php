<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<?php 
$tblpx  = Yii::app()->db->tablePrefix;
$where = ' where 1=1 ';       
$sql = "SELECT ven.*  FROM {$tblpx}bills as bill "
        . " left join {$tblpx}purchase as pu on pu.p_id = bill.purchase_id "
        . " LEFT join {$tblpx}vendors as ven on ven.vendor_id = pu.vendor_id   "
        . " $where group by ven.vendor_id order by ven.name ASC";
$aging_array = Yii::app()->db->createCommand($sql)->queryAll();
?>
<div class="container">
    <div class="clearfix">
        <br>
    <h5 class="text-center"><?php echo Yii::app()->name; ?></h5>
        <h2 class="text-center" style="padding-top: 3px;">Aging Summary By Bill Due Date</h2>
        <h5 class="text-center">As of <?php echo date ("d/m/Y")?></h5>
        <a href="<?php echo Yii::app()->createUrl("vendors/paymentReport")?>" class="pull-right save_btn mt-2">Vendor Payment Report</a>
    </div>
    <br>
    <?php if(count($aging_array) > 0){ ?>
            <div class="clearfix">
                <div class="pull-right">Total <?php echo count($aging_array) ?> records</div>
            </div>
            <?php } ?>
        <div id="parent">
            
            <table class="table" id="fixTable">
                <thead>
                    <tr>
                        <th>Vendor Name</th>
                        <th>Current</th>
                        <th>1-15 Days</th>
                        <th>16-30 Days</th>
                        <th>31-45 Days</th>
                        <th>>45 Days</th>
                        <th>Total</th> 
                                               
                    </tr>
                </thead>
                <tbody>
                    <?php                     
                    $tblpx  = Yii::app()->db->tablePrefix; 
                    $total_current_amount  = 0;
                    $total_amount2  = 0;
                    $total_amount3  = 0;
                    $total_amount4  = 0;
                    $total_amount5  = 0; 
                    $total_amount_main  = 0;                                        
                    foreach ($aging_array as $value) { 
                        if($value['vendor_id']!=""){
                        ?>
                        <tr>
                            <td><?php echo $value['name']; ?></td>                            
                            <td class="text-right">
                                <?php                                
                                $where = " AND bill_date = DATE_SUB(NOW(), INTERVAL 0 DAY)";
                                $startDate = date('Y-m-d');

                                $sql = "SELECT SUM(bill_totalamount) as amount,SUM(bill_additionalcharge) AS additional_amount "
                                . " FROM {$tblpx}bills as bill "
                                . " left join {$tblpx}purchase as pu "
                                . " on pu.p_id = bill.purchase_id "
                                . " WHERE pu.vendor_id= ".$value['vendor_id'] 
                                . " $where"; 
                                
                                
                                $data = Yii::app()->db->createCommand($sql)->queryRow();
                                $paidamount = Controller::getPaidBillAmount(1,$value['vendor_id']);
                                $amount1 = ($data['amount'] + $data['additional_amount'])-$paidamount;
                                if($amount1 >0){
                                    $params = [
                                    
                                        'Bills[project]' => '',
                                        'Bills[vendor]' => $value['vendor_id'],
                                        'company_id' => '',
                                        'Bills[pu_num]'=> '',
                                        'Bills[bill_num]' =>'',
                                        'Bills[date_from]' => $startDate,
                                        'Bills[date_to]' => $startDate,
                                        'yt0' => 'Go'
                                    ];
                                    $url = Yii::app()->createUrl('bills/purchasebillreport', $params);
                                   

                                     echo "<a href='" . $url . "'>" . Controller::money_format_inr((($data['amount'] + $data['additional_amount']) - $paidamount), 2) . "</a>";

                                }else{
                                    echo Controller::money_format_inr((($data['amount']+ $data['additional_amount'])-$paidamount),2);
                                }
                                
                                ?>
                                
                            </td>
                            <td class="text-right">
                                <?php
                                $where = " AND bill_date < DATE_SUB(NOW(), INTERVAL 1 DAY) AND bill_date > DATE_SUB(NOW(), INTERVAL 15 DAY) ";
                                $sql = "SELECT SUM(bill_totalamount) as amount,SUM(bill_additionalcharge) AS additional_amount "
                                . " FROM {$tblpx}bills as bill "
                                . " left join {$tblpx}purchase as pu "
                                . " on pu.p_id = bill.purchase_id "
                                . " WHERE pu.vendor_id= ".$value['vendor_id'] 
                                . " $where";
                                $data = Yii::app()->db->createCommand($sql)->queryRow();
                                $paidamount = Controller::getPaidBillAmount(2,$value['vendor_id']);
                                $amount2 = ($data['amount']+$data['additional_amount'])-$paidamount;
                               $startDate = date('Y-m-d', strtotime('-15 days')); 
                                $endDate = date('Y-m-d', strtotime('-1 days')); 

                                if($amount2 >0){
                                    $params = [
                                    
                                        'Bills[project]' => '',
                                        'Bills[vendor]' => $value['vendor_id'],
                                        'company_id' => '',
                                        'Bills[pu_num]'=> '',
                                        'Bills[bill_num]' =>'',
                                        'Bills[date_from]' => $startDate,
                                        'Bills[date_to]' => $endDate,
                                        'yt0' => 'Go'
                                    ];
                                    $url = Yii::app()->createUrl('bills/purchasebillreport', $params);
                                   
                                   

                                     echo "<a target='blank' href='" . $url . "'>" . Controller::money_format_inr((($data['amount'] + $data['additional_amount']) - $paidamount), 2) . "</a>";

                                }else{
                                    echo Controller::money_format_inr((($data['amount']+ $data['additional_amount'])-$paidamount),2);
                                }
                                ?>
                            </td>
                            <td class="text-right">
                            <?php
                                $where = " AND bill_date < DATE_SUB(NOW(), INTERVAL 16 DAY) AND bill_date > DATE_SUB(NOW(), INTERVAL 30 DAY) ";
                                $sql = "SELECT SUM(bill_totalamount) as amount,SUM(bill_additionalcharge) AS additional_amount "
                                . " FROM {$tblpx}bills as bill "
                                . " left join {$tblpx}purchase as pu "
                                . " on pu.p_id = bill.purchase_id "
                                . " WHERE pu.vendor_id= ".$value['vendor_id'] 
                                . " $where";
                                //die($sql);
                                $data = Yii::app()->db->createCommand($sql)->queryRow();
                                //echo "<pre>";print_r($data);exit;
                                $paidamount = Controller::getPaidBillAmount(3,$value['vendor_id']);
                                $amount3 = ($data['amount']+$data['additional_amount'])-$paidamount;
                                $startDate = date('Y-m-d', strtotime('-30 days')); 
                                $endDate = date('Y-m-d', strtotime('-16 days')); 
                                if($amount3 >0){
                                     $params = [
                                    
                                        'Bills[project]' => '',
                                        'Bills[vendor]' => $value['vendor_id'],
                                        'company_id' => '',
                                        'Bills[pu_num]'=> '',
                                        'Bills[bill_num]' =>'',
                                        'Bills[date_from]' => $startDate,
                                        'Bills[date_to]' => $endDate,
                                        'yt0' => 'Go'
                                    ];
                                    $url = Yii::app()->createUrl('bills/purchasebillreport', $params);
                                   
                                   

                                     echo "<a target='blank' href='" . $url . "'>" . Controller::money_format_inr((($data['amount'] + $data['additional_amount']) - $paidamount), 2) . "</a>";

                                }else{
                                echo Controller::money_format_inr((($data['amount']+ $data['additional_amount'])-$paidamount),2);
                                }
                                ?>
                            </td>
                            <td class="text-right">
                                <?php
                                $where = " AND bill_date < DATE_SUB(NOW(), INTERVAL 31 DAY) AND bill_date > DATE_SUB(NOW(), INTERVAL 45 DAY) ";
                                $sql = "SELECT SUM(bill_totalamount) as amount ,SUM(bill_additionalcharge) AS additional_amount"
                                . " FROM {$tblpx}bills as bill "
                                . " left join {$tblpx}purchase as pu "
                                . " on pu.p_id = bill.purchase_id "
                                . " WHERE pu.vendor_id= ".$value['vendor_id'] 
                                . " $where";
                                $data = Yii::app()->db->createCommand($sql)->queryRow();
                                $paidamount = Controller::getPaidBillAmount(4,$value['vendor_id']);
                                $amount4 = ($data['amount']+ $data['additional_amount'])-$paidamount;
                                $startDate = date('Y-m-d', strtotime('-45 days')); 
                                $endDate = date('Y-m-d', strtotime('-31 days')); 
                                if($amount4 >0){
                                    $params = [
                                    
                                        'Bills[project]' => '',
                                        'Bills[vendor]' => $value['vendor_id'],
                                        'company_id' => '',
                                        'Bills[pu_num]'=> '',
                                        'Bills[bill_num]' =>'',
                                        'Bills[date_from]' => $startDate,
                                        'Bills[date_to]' => $endDate,
                                        'yt0' => 'Go'
                                    ];
                                    $url = Yii::app()->createUrl('bills/purchasebillreport', $params);
                                   
                                   

                                     echo "<a target='blank' href='" . $url . "'>" . Controller::money_format_inr((($data['amount'] + $data['additional_amount']) - $paidamount), 2) . "</a>";

                                }else{
                                echo Controller::money_format_inr((($data['amount']+ $data['additional_amount'])-$paidamount),2);
                                }
                                ?></td>
                            <td class="text-right">
                            <?php
                                $where = " AND bill_date < DATE_SUB(NOW(), INTERVAL 45 DAY)";
                                $sql = "SELECT SUM(bill_totalamount) as amount,SUM(bill_additionalcharge) AS additional_amount "
                                . " FROM {$tblpx}bills as bill "
                                . " left join {$tblpx}purchase as pu "
                                . " on pu.p_id = bill.purchase_id "
                                . " WHERE pu.vendor_id= ".$value['vendor_id'] 
                                . " $where";
                                $data = Yii::app()->db->createCommand($sql)->queryRow();
                                $paidamount = Controller::getPaidBillAmount(5,$value['vendor_id']);
                                $amount5 = ($data['amount']+ $data['additional_amount'])-$paidamount;
                               
                              $startDate = date('Y-m-d', strtotime('-4 Years')); // 45 days ago
                                $endDate = date('Y-m-d', strtotime('-45 days')); 
                                if($amount5 >0){
                                     $params = [
                                    
                                        'Bills[project]' => '',
                                        'Bills[vendor]' => $value['vendor_id'],
                                        'company_id' => '',
                                        'Bills[pu_num]'=> '',
                                        'Bills[bill_num]' =>'',
                                        'Bills[date_from]' => $startDate,
                                        'Bills[date_to]' =>$endDate,
                                        'yt0' => 'Go'
                                    ];
                                    $url = Yii::app()->createUrl('bills/purchasebillreport', $params);
                                   
                                   

                                     echo "<a target='blank'  href='" . $url . "'>" . Controller::money_format_inr((($data['amount'] + $data['additional_amount']) - $paidamount), 2) . "</a>";

                                }else{
                                     echo Controller::money_format_inr((($data['amount']+ $data['additional_amount'])-$paidamount),2);
                                }
                               
                                ?>
                            </td>
                            <td class="text-right">
                            <?php 
                            $total_amount = $amount2 + $amount3 + $amount4 +$amount5;
                            echo Controller::money_format_inr(($total_amount),2);
                            ?>
                            </td>
                        </tr>
                    <?php 
                    $total_current_amount = $total_current_amount+$amount1;
                    $total_amount2 = $total_amount2 + $amount2;
                    $total_amount3 = $total_amount3 + $amount3;
                    $total_amount4 = $total_amount4 + $amount4;
                    $total_amount5 = $total_amount5 + $amount5;
                    $total_amount_main = $total_amount_main +  $total_amount;

                    } } ?>
                    <tr>
                        <td> <b>Total</b> </td>
                        <td class="text-right"><?php echo  Controller::money_format_inr(($total_current_amount),2)  ?></td>
                        <td class="text-right"><?php echo  Controller::money_format_inr(($total_amount2),2)  ?></td>
                        <td class="text-right"><?php echo  Controller::money_format_inr(($total_amount3),2)  ?></td>
                        <td class="text-right"><?php echo  Controller::money_format_inr(($total_amount4),2)  ?></td>
                        <td class="text-right"><?php echo  Controller::money_format_inr(($total_amount5),2)  ?></td>
                        <td class="text-right"><?php echo  Controller::money_format_inr(($total_amount_main),2)  ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    
</div>
<script>
    $("#fixTable").tableHeadFixer({
        'foot': true,
        'head': true
    });
</script>
<style>
    #parent {
        max-height: 400px;
        overflow:auto;
    }
</style>
