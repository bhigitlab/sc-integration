ALTER TABLE `jp_consumption_request` ADD `remarks` VARCHAR(255) NULL AFTER `pms_wpr_status`;

ALTER TABLE `jp_equipments_entries` ADD `remarks` VARCHAR(255) NULL AFTER `Date`;

ALTER TABLE `jp_equipments_entries` ADD `pms_wpr_status` INT NOT NULL DEFAULT '0' AFTER `remarks`;
