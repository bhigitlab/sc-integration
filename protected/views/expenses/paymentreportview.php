<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<?php
/* @var $this ProjectsController */
/* @var $model Projects */

$this->breadcrumbs = array(
    'Expenses' => array('index'),
    'Manage',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('projects-grid', {
		data: $(this).serialize()
	});
	return false;
});

    /* $('#download').click(function () {


        html2canvas($('#financialreport1'), {
            onrendered: function (canvas) {
                var imgData = canvas.toDataURL('image/jpeg');

                var imgWidth = 210;
                var pageHeight = 295;
                var imgHeight = canvas.height * imgWidth / canvas.width;
                var heightLeft = imgHeight;
                var doc = new jsPDF('p', 'mm');
                var position = 0;
                doc.page = 1; // use this as a counter.

                doc.addImage(imgData, 'jpeg', 0, position, imgWidth, imgHeight);
                heightLeft -= pageHeight;

                while (heightLeft >= 0) {
                    position = heightLeft - imgHeight;
                    doc.addPage();
                    doc.addImage(imgData, 'jpeg', 0, position, imgWidth, imgHeight);
                    heightLeft -= pageHeight;
                }
                doc.save('paymentreport.pdf');

            }
        });
    }); */
");

$tblpx = Yii::app()->db->tablePrefix;
?>


<div class="container" id="project">

    <?php
    if (!empty($fromdate) && !empty($todate)) {
        $duration = "from $fromdate to $todate";
    } else if (!empty($fromdate) && empty($todate)) {
        $duration = "from $fromdate onwards";
    } else if (empty($fromdate) && !empty($todate)) {
        $duration = "as on $todate";
    } else {
        $start = date('d-M-Y');
        $to = date('d-M-Y');
        $duration = "from $start to $to";
    }
    ?>
    <div class="sub-heading mb-10">
        <h3>Receipt Report</h3>
        <div id="paymentreport1" class="btn-container">
            <a class="btn btn-info" title="SAVE AS PDF"
                href="<?php echo $this->createAbsoluteUrl('expenses/savetopdf', array('fromdate' => $fromdate, 'todate' => $todate, 'company_id' => $company_id, 'companystatus' => $companystatus)) ?>"
                style="text-decoration: none; color: white;">
                <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
            </a>
            <a class="btn btn-info" title="SAVE AS EXCEL"
                href="<?php echo $this->createAbsoluteUrl('expenses/savetoexcel', array('fromdate' => $fromdate, 'todate' => $todate, 'company_id' => $company_id, 'companystatus' => $companystatus)) ?>"
                style="text-decoration: none; color: white;">
                <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>
            </a>
        </div>
    </div>
    <div class="page_filter clearfix">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'expenses-form',
            'action' => Yii::app()->createAbsoluteUrl('expenses/paymentreport'),
            'method' => 'get',
        ));
        ?>
        <div class="row">
            <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
                <label>From</label>
                <?php echo CHtml::activeTextField($model, 'fromdate', array('size' => 10, 'value' => $fromdate, 'class' => 'form-control')); ?>
            </div>
            <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
                <label>To</label>
                <?php echo CHtml::activeTextField($model, 'todate', array('size' => 10, 'value' => $todate, 'class' => 'form-control')); ?>
            </div>
            <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
                <label for="company">Company</label>
                <?php
                echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array(
                    'select' => array('id, name'),
                    "condition" => 'id IN(' . Yii::app()->user->company_ids . ')',
                    'order' => 'name',
                    'distinct' => true
                )), 'id', 'name'), array('empty' => 'Select Company', 'id' => 'company_id', 'class' => 'form-control', 'style' => 'padding:2.5px 0px;'));
                ?>
            </div>
            <div class="form-group col-xs-12 col-sm-3 col-md-2 text-sm-left text-right">
                <label class="d-sm-block d-none">&nbsp;</label>
                <div>
                    <?php echo CHtml::submitButton('Go', array('class' => 'btn btn-sm btn-primary')); ?>
                    <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('paymentreport') . '"', 'class' => 'btn btn-sm btn-default')); ?>
                </div>
            </div>
            <input type="hidden" name="Expenses[project_order]" id="Expenses_project_order"
                value="<?php echo $project_order; ?>" />
        </div>
        <?php $this->endWidget(); ?>
    </div>


    <span>Sort By: </span><a class="sort_project" id="sort_project">Project</a><?php if ($project_order == "ASC") { ?><i
            class="fa fa-sort-asc" aria-hidden="true"></i> <?php } else if ($project_order == "DESC") { ?> <i
                class="fa fa-sort-desc" aria-hidden="true"></i> <?php } ?>
    <h4 class="head_secon">Cash Receipt
        <?= $duration; ?>
    </h4>
    <div class="table-wrapper">
        <div id="parent">
            <table class="table table-bordered tab1" id="fixtable">
                <thead class="entry-table sticky-thead">
                    <tr>
                        <th>Sl No.</th>
                        <th>Project</th>
                        <th>Party Name</th>
                        <th>Site</th>
                        <th>Date</th>
                        <th>Receipt Amount</th>
                    </tr>
                    <tr class="grandtotal">
                        <td colspan="5" align="right"><b>Total</b></td>
                        <td align="right"><b id="cash_total_top"></b></td>
                    </tr>

                </thead>
                <?php
                $reportdata = $paymentcash->getData();
                if (!empty($reportdata)) {
                    $i = 0;
                    $cash_grandtotal = 0;
                    ?>
                    <tbody>
                        <?php
                        foreach ($reportdata as $data) {
                            $i++;
                            $projectid = $data['projectid'];
                            $client = Yii::app()->db->createCommand("SELECT {$tblpx}clients.name as client, {$tblpx}projects.site, {$tblpx}projects.name as projectname FROM {$tblpx}projects LEFT JOIN {$tblpx}clients  ON {$tblpx}projects.client_id = {$tblpx}clients.cid  WHERE {$tblpx}projects.pid = {$projectid}")->queryRow();
                            $cash_grandtotal += $data['paidamount'];
                            ?>
                            <tr>
                                <td>
                                    <?= $i; ?>
                                </td>
                                <td>
                                    <?= $client['projectname']; ?>
                                </td>
                                <td>
                                    <?= $client['client']; ?>
                                </td>
                                <td>
                                    <?= $client['site']; ?>
                                </td>
                                <td>
                                    <?= date('d-M-Y', strtotime($data['date'])); ?>
                                </td>
                                <td align="right" class="cash_total">
                                    <?php echo Yii::app()->Controller->money_format_inr($data['paidamount'], 2); ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                <?php } else { ?>
                    <tfoot class="entry-table">
                        <tr>
                            <th colspan="6" class="text-center">No results found.</th>
                        </tr>
                        <tfoot>
                        <?php } ?>
            </table>
        </div>
        <?php
        echo ('<div class="clearfix" style="right: 3px;position: absolute;top: -19px;">' . 'Total' . ' ' . count($reportdata) . ' ' . 'results' . '</div>');
        ?>
    </div>

    <h4 class="head_secon margin-top-20">Bank Receipt
        <?= $duration; ?>
    </h4>
    <div class="table-wrapper">
        <div id="parent2">
            <table class="table table-bordered  tab2" id="fixtable2">
                <thead class="entry-table sticky-thead">
                    <tr>
                        <th style="display:none;"></th>
                        <th>Sl No.</th>
                        <th>Project Name</th>
                        <th>Party Name</th>
                        <th>Site</th>
                        <th>Date</th>
                        <th>Receipt Amount</th>

                    </tr>
                    <tr class="grandtotal">
                        <td colspan="5" align="right"><b>Total</b></td>
                        <td align="right"><b id="bank_total_top"></b></td>

                    </tr>

                </thead>
                <?php
                $reportdata = $paymentbank->getData();

                if (!empty($reportdata)) {
                    $i = 0;
                    $bank_grandtotal = 0;
                    ?>
                    <tbody>
                        <?php
                        foreach ($reportdata as $data) {
                            $i++;
                            $projectid = $data['projectid'];
                            $client = Yii::app()->db->createCommand("SELECT {$tblpx}clients.name as client, {$tblpx}projects.site, {$tblpx}projects.name as projectname FROM {$tblpx}projects LEFT JOIN {$tblpx}clients  ON {$tblpx}projects.client_id = {$tblpx}clients.cid  WHERE {$tblpx}projects.pid = {$projectid}")->queryRow();

                            $bank_grandtotal += $data['paidamount'];
                            ?>
                            <tr>
                                <th style="display:none;"></th>
                                <td>
                                    <?= $i; ?>
                                </td>
                                <td>
                                    <?= $client['projectname'] ?>
                                </td>
                                <td>
                                    <?= $client['client']; ?>
                                </td>
                                <td>
                                    <?= $client['site']; ?>
                                </td>
                                <td>
                                    <?= date('d-M-Y', strtotime($data['date'])); ?>
                                </td>
                                <td align="right" class="bank_total">
                                    <?php echo Yii::app()->Controller->money_format_inr($data['paidamount'], 2); ?>
                                </td>

                            </tr>
                        <?php } ?>
                    </tbody>
                <?php } else { ?>
                    <tfoot class="entry-table">
                        <tr>
                            <th colspan="6" class="text-center">No results found.</th>
                        </tr>
                    </tfoot>
                <?php } ?>
            </table>
        </div>
        <?php echo ('<div class="clearfix" style="right: 3px;position: absolute;top: -19px;">' . 'Total' . ' ' . count($reportdata) . ' ' . 'results' . '</div>'); ?>
    </div>
</div>
<style>
    #parent,
    #parent2 {
        max-height: 300px;
    }
</style>

<script>
    $(document).ready(function () {
        $("#fixtable").tableHeadFixer({ 'left': false, 'foot': true, 'head': true });
        $("#fixtable2").tableHeadFixer({ 'foot': true, 'head': true });
    });
    $("#Expenses_fromdate").change(function () {
        $("#Expenses_todate").datepicker('option', 'minDate', $(this).val());
    });
    $("#Expenses_todate").change(function () {
        $("#Expenses_fromdate").datepicker('option', 'maxDate', $(this).val());
    });
    $("#sort_project").click(function () {
        var order = $("#Expenses_project_order").val();
        if (order == "ASC") {
            $("#Expenses_project_order").val("DESC");
        } else if (order == "DESC") {
            $("#Expenses_project_order").val("ASC");
        } else {
            $("#Expenses_project_order").val("ASC");
        }
        $("#expenses-form").submit();
    });
    //    $('.container1').scroll(function () {
    //        //$('.container1').css("height","300px");
    //        con1('.container1', '.tab1 tbody th', '.tab1 thead th');
    //    });
    //    $('.container2').scroll(function () {
    //        con1('.container2', '.tab2 tbody th', '.tab2 thead th');
    //    });

    /*function con1(e, f, g) {
        'use strict';
        var container = document.querySelector(e);
        var table = document.querySelector('table');
        var leftHeaders = [].concat.apply([], document.querySelectorAll(f));
        var topHeaders = [].concat.apply([], document.querySelectorAll(g));
        var topLeft = document.createElement('div');
        var computed = window.getComputedStyle(topHeaders[0]);
        container.appendChild(topLeft);
        topLeft.classList.add('top-left');
        topLeft.style.width = computed.width;
        topLeft.style.height = computed.height;

        container.addEventListener('scroll', function (e) {
            var x = container.scrollLeft;
            var y = container.scrollTop;

            leftHeaders.forEach(function (leftHeader) {
                leftHeader.style.transform = translate(x, 0);
            });
            topHeaders.forEach(function (topHeader, i) {
                if (i === 0) {
                    topHeader.style.transform = translate(0, y);
                } else {
                    topHeader.style.transform = translate(0, y);
                }
            });
            topLeft.style.transform = translate(x, y);
        });

        function translate(x, y) {
            return 'translate(' + x + 'px, ' + y + 'px)';
        }


    }*/
    $(document).ready(function () {
        function formatMoneyIndian(number) {
            var [integerPart, decimalPart] = number.toFixed(2).split(".");
            var lastThreeDigits = integerPart.slice(-3);
            var otherDigits = integerPart.slice(0, -3);

            if (otherDigits) {
                otherDigits = otherDigits.replace(/\B(?=(\d{2})+(?!\d))/g, ",");
            }

            return (otherDigits ? otherDigits + "," : "") + lastThreeDigits + "." + decimalPart;
        }

        $("#Expenses_fromdate").datepicker({
            dateFormat: 'dd-M-yy',
            maxDate: $("#Expenses_todate").val(),
        });
        $("#Expenses_todate").datepicker({
            dateFormat: 'dd-M-yy'
        });
        var cash_sum = 0;
        var bank_sum = 0;
        $(".cash_total").each(function () {
            var value = $(this).text().trim();
            var val = parseFloat(value.replace(/,/g, '')) || 0; // Remove existing commas, parse to float
            if (!isNaN(val)) {
                cash_sum += val;
            }
        });

        $(".bank_total").each(function () {
            var value = $(this).text().trim();
            var val = parseFloat(value.replace(/,/g, '')) || 0; // Remove existing commas, parse to float
            if (!isNaN(val)) {
                bank_sum += val;
            }
        });

        $('#cash_total_top').text(formatMoneyIndian(cash_sum));
        $('#bank_total_top').text(formatMoneyIndian(bank_sum));

        //        var tabHeight = $('.tab1').height();
        //        if (tabHeight < 150) {
        //            $('.container1').css('min-height', tabHeight + 'px');
        //        } else
        //        {
        //            $('.container1').css('min-height', '300px');
        //        }
        //        var tabHeight2 = $('.tab2').height();
        //        if (tabHeight2 < 150) {
        //            $('.container2').css('min-height', tabHeight2 + 'px');
        //        } else
        //        {
        //            $('.container2').css('min-height', '300px');
        //        }
    });
    function ReplaceNumberWithCommas(yourNumber) {
        //Seperates the components of the number
        var n = yourNumber.toString().split(".");
        //Comma-fies the first part
        n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        //Combines the two sections
        return n.join(".");
    }
</script>

<style>
    /*.container1 ,.container2{
        width: 100%;
        max-height: 750px;
        overflow: auto;
        position: relative;
    }

th,
.top-left {
    background: #eee;
}
.top-left {
    box-sizing: border-box;
    position: absolute;
    left: 0;
    border-right: 1px solid #ccc;
    border-bottom: 1px solid #ccc;
}

table {

    border-spacing: 0;
    /* position: absolute;

}
th,
td {
    border-right: 1px solid #ccc !important;

}*/
</style>