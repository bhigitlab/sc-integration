<div class="parent_row clearfix">
    <div class="row box-collapse">
        <div class="col-md-1 logo_sec">
            <button class="btn_logo">PO</button>
        </div>
        <div class="col-md-8 content_sec">
            <div class="row  clearfix">
                <div class="col-md-8">
                    <div class="list-item">
                        <?php
                        $user = Users::model()->findByPk($data['created_by']);
                        ?>
                        <?php
                        if ($data->permission_status == 'No') {
                        ?>
                            <b><?php echo $user->first_name . ' ' . $user->last_name; ?></b> requested rate approval for <b>
                                PO <?php echo $data->purchase_no ?></b> for <b><?php echo $data->project->name; ?></b>
                        <?php }  ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="list-item pull-right">
                        <label>Date:</label>
                        <span><?php echo date('d-m-Y', strtotime($data['purchase_date'])); ?></span>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="list-item text-right act_btn">
                <?php
                if ($data->permission_status == 'No') {
                ?>
                    <button class="btn btn-xs btn_approve btn_sec approve_po" id="<?php echo $data['p_id']; ?>">Rate Approve</button>
                <?php } else { ?>
                    <a class="btn btn-xs btn_approve btn_sec" target="_blank" href="<?php echo Yii::app()->createAbsoluteUrl('purchase/updatepurchase', array('pid' => $data->p_id)); ?>">Item Approve</a>
                <?php } ?>
                <button class="btn btn-xs btn_decline btn_sec po_rate_decline" id="<?php echo $data['p_id']; ?>">Decline </button>
                <button class="compare_icon popup btn btn-xs btn-default" title="Compare">Details</i></button>
                <button class="box_close btn btn-xs btn-default" style="display:none;">Close</i></button>
            </div>
        </div>
    </div>
    
    <div class="box">
        <div class="box-inner">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">                   
                    <h5 class="panel-title pull-left">Add Comments</h5>
                    <div class="panel-close pull-right">×</div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-9">
                            <textarea rows="1" cols="12" class="form-control decline_remarks"></textarea>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-primary btn-sm decline_comment_submit" id="<?php echo $data['p_id']; ?>">submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="compare_box popuptext" style="display:none;">
        <div class="box_content">
            <table class="table w-100">
                <thead>
                    <tr>
                        <th class="text-center">Company</th>
                        <th class="text-center">Project</th>
                        <th class="text-center">Expense Head</th>
                        <th class="text-center">Vendor</th>
                        <th class="text-center">Date</th>
                        <th class="text-center">Purchase No</th>
                        <th class="text-center">Amount</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <?php

                        $company = Company::model()->findByPk($data->company_id);
                        ?>
                        <td class="text-center"><?php echo $company->name; ?></td>
                        <td class="text-center"><?php echo $data->project->name; ?></td>
                        <td class="text-center"><?php echo $data->expensehead->type_name; ?></td>
                        <td class="text-center"><?php echo $data->vendor->name; ?></td>
                        <td class="text-center"><?php echo date('d-m-Y', strtotime($data->purchase_date)); ?></td>
                        <td class="text-center"><?php echo CHtml::link($data->purchase_no,  $this->createAbsoluteUrl('purchase/viewpurchase', array('pid' => $data->p_id)), array('class' => 'link-text', 'target' => '_blank')); ?></td>
                        <td class="text-center"><?php echo $data->total_amount; ?></td>

                    </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>