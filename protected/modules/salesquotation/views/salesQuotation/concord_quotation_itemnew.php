<div id='previous_details'></div>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'action' => Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/addquotationitemsDetails"),
    'htmlOptions' => array('enctype' => 'multipart/form-data', 'class' => 'clients-form'),
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => false,
    ),
        ));
?>
<?php echo CHtml::hiddenField('cat_id', $category_id, array('id' => 'cat_id')); ?>
<?php echo CHtml::hiddenField('cat_name', $category_label, array('id' => 'cat_name')); ?>
<?php echo CHtml::hiddenField('master_id', $qid, array('id' => 'master_id')); ?>
<?php echo CHtml::hiddenField('revision_no', $rev, array('id' => 'revision_no')); ?>
<?php echo CHtml::hiddenField('status', $status, array('id' => 'status')); ?>
<?php echo CHtml::hiddenField('parent_type', $parent_type, array('id' => 'parent_type')); ?>
<?php echo CHtml::hiddenField('parent_id', $parent_id, array('id' => 'parent_id')); ?>
<?php echo CHtml::hiddenField('category_label_id', $category_label_id, array('id' => 'category_label_id')); ?>

<span id="error-add"></span>
<div class="panel-body">
    <?php if ($quotation_count > 0) { ?>
        <div class="mainitem"> 
            <table class="table qitem_table mt-2" id="qitem_table">
                <thead>
                    <tr>
                        <th>Work Type</th>
                        <th>WorkType Label</th>
                        <th></th>
                        <th>Description</th>
                        <th>Unit</th>
                      
                        <th>Quantity</th>
                        <?php
                        $activeProjectTemplate = Controller::getActiveTemplate();
                        if ($activeProjectTemplate == 'TYPE-2') { ?>
                        <th>Quantity (in NOS)</th>
                        <?php } ?>
                        <th>MRP</th>
                        <th>Amount after discount</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $worktypearray = array();
                    foreach ($all_quotation_items as $key => $value) {

                        $worktypeid = $value->work_type_id;
                        array_push($worktypearray, $worktypeid);
                        $work_type_label = QuotationWorktypeMaster::model()->findByPk($value->work_type_id);
                        ?>
                        <tr>
                            <td class="worktypedata" data-id ='<?php echo $value->work_type_id ?>'    data-incremnt ='<?php echo $key ?>' data-template ='<?php echo $work_type_label['template_id'] ?>' >
                                <span><?php echo $work_type_label['name'] ?></span>
                                <?php echo CHtml::hiddenField('work_type_id[]', $value->work_type_id, array('id' => 'hiddenworktype' . $key,'class'=>'hiddenworktype')); ?>
                                <?php echo CHtml::hiddenField('item_id[]', '', array('id' => 'hiddenitem' . $key,'class'=>'hiddenitem')); ?>
                                <?php echo CHtml::hiddenField('template_id[]', $work_type_label['template_id'], array('id' => 'hiddentemplate' . $key,'class'=>'hiddentemplate')); ?>

                            </td>
                            <td>
                                <?php
                                $newQuery = 'work_type_id =' . $value->work_type_id . ' AND quotation_category_id =' . $category_id . '';
                                echo $form->dropDownList(
                                        $model, 'worktype_label', CHtml::listData(QuotationItemMaster::model()->findAll(array('condition' => $newQuery)), 'worktype_label', 'worktype_label'), array('class' => 'form-control js-example-basic-single field_change invoice_add require worktype_label ', 'name' => 'worktype_label[]', 'empty' => '-Select Worktype Label-', 'id' => 'worktype_label' . $key, 'style' => 'width:100%')
                                );
                                ?>

                            </td>
                            <?php 
                            $style="visibility:hidden;height:0px;";
                            $style1="visibility:visible";
                            if ($work_type_label['template_id'] == 1) { 
                                $style="visibility:visible";
                                $style1="visibility:hidden;height:0px;";
                            }  ?>
                                <td>
                                    <label style="<?php echo $style ?>">Shutter Work</label>
                                    <?php echo $form->textField($model, 'shutterwork_description', array('class' => 'form-control require', 'name' => 'shutterwork_description[]', 'value' => '', 'id' => 'shutterwork_description' . $key, 'readonly' => true,'style'=>$style)); ?>
                                
                                <!-- OR -->
                                    <?php echo $form->hiddenField($model, 'material', array('class' => 'form-control require', 'name' => 'material[]', 'value' => '', 'id' => 'material' . $key)); ?>
                                    <?php echo $form->hiddenField($model, 'finish', array('class' => 'form-control require', 'name' => 'finish[]', 'value' => '', 'id' => 'finish' . $key)); ?>
                                
                                </td>
                                <td>
                                    <label style="<?php echo $style ?>">Carcass Box Work</label>
                                    <?php echo $form->textField($model, 'carcass_description', array('class' => 'form-control require ', 'name' => 'caracoss_description[]', 'value' => '', 'id' => 'caracoss_description' . $key, 'readonly' => true,'style'=>$style)); ?>
                                    <?php echo $form->hiddenField($model, 'shutterwork_material', array('class' => 'form-control require', 'name' => 'shutterwork_material[]', 'value' => '', 'id' => 'shutterwork_material' . $key)); ?>
                                    <?php echo $form->hiddenField($model, 'shutterwork_finish', array('class' => 'form-control require', 'name' => 'shutterwork_finish[]', 'value' => '', 'id' => 'shutterwork_finish' . $key)); ?>
                                    <?php echo $form->hiddenField($model, 'carcass_material', array('class' => 'form-control require', 'name' => 'carcass_material[]', 'value' => '', 'id' => 'carcass_material' . $key)); ?>
                                    <?php echo $form->hiddenField($model, 'carcass_finish', array('class' => 'form-control require', 'name' => 'carcass_finish[]', 'value' => '', 'id' => 'carcass_finish' . $key)); ?>
                                    <?php echo $form->hiddenField($model, 'material', array('class' => 'form-control require', 'name' => 'material[]', 'value' => '', 'id' => 'material' . $key)); ?>
                                    <?php echo $form->hiddenField($model, 'finish', array('class' => 'form-control require', 'name' => 'finish[]', 'value' => '', 'id' => 'finish' . $key)); ?>
                                <!-- OR -->    
                                <?php echo $form->textField($model, 'description', array('class' => 'form-control require description', 'autocomplete' => 'off', 'name' => 'description[]', 'value' => '', 'id' => 'description' . $key, 'readonly' => true,'style'=>$style1)); ?>

                                </td>
                                

                            <td>
                                <?php
                                echo $form->dropDownList(
                                        $model, 'unit', CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name'), array('class' => 'form-control js-example-basic-single field_change invoice_add require ', 'name' => 'unit[]', 'empty' => '-Select Unit-', 'id' => 'unit' . $key, 'style' => 'width:100%')
                                );
                                ?>
                            </td>
                            
                            <td>
                                <?php echo $form->textField($model, 'quantity', array('class' => 'form-control allownumericdecimal  require quantity ', 'autocomplete' => 'off', 'name' => 'qty[]', 'data-incremntid' => $key, 'id' => 'qty' . $key)); ?>
                                <?php echo $form->error($model, 'quantity'); ?>
                            </td>
                            <?php
                            $activeProjectTemplate = Controller::getActiveTemplate();
                            if ($activeProjectTemplate == 'TYPE-2') { ?>
                            <td>
                                <?php echo $form->textField($model, 'quantity_nos', array('class' => 'form-control   require ', 'autocomplete' => 'off', 'name' => 'qtynos[]')); ?>
                                <?php echo $form->error($model, 'quantity_nos'); ?>
                            </td>
                            <?php } ?>
                            <td>
                                <?php echo $form->textField($model, 'mrp', array('class' => 'form-control allownumericdecimal require mrp', 'autocomplete' => 'off', 'name' => 'mrp[]')); ?>
                                <?php echo $form->error($model, 'mrp'); ?>
                            </td>
                            <td>
                                <?php echo $form->textField($model, 'amount_after_discount', array('class' => 'form-control allownumericdecimal  require amountafter-discount', 'autocomplete' => 'off', 'name' => 'amount_after_discount[]', 'id' => 'amount_after_discount' . $key)); ?>
                                <?php echo $form->error($model, 'amount_after_discount'); ?>
                            </td>
                        </tr>

                    <?php } ?>
                </tbody>
            </table>
        </div>
    <?php } ?>
    <?php $this->endWidget(); ?>

</div>
<script>
<?php
$addItem = Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/addquotationitemsDetails");
$url = Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/index");
$amountget = Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/getamountdetails");
?>
    function  displaysub() {
        $("#subitem").show();
    }
    function displayextra() {
        $("#extrawork").show();
    }

    $(document).on('change', '.sub_worktype_label', function () {
        var element = $(this);
        var worktypelabel = $('option:selected', this).text();
        var cat_id = '<?php echo $category_id ?>';
        var id = $(this).attr('data-subincrement');
        var worktypeid = $("#sub_worktype" + id).val();
        var template_id = $('option:selected', this).attr('data-template');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getdatasofworktype'); ?>',
            type: 'GET',
            dataType: 'json',
            data: {
                cat_id: cat_id,
                worktypelabel: worktypelabel,
                worktypeid: worktypeid,
                template_id: template_id
            },
            success: function (result) {

                if (result.status == 1) {
                    $('#sub_item' + id).val(result.item_id);
                    $('#previous_details').html(result.html);
                    $("#previous_details").show();//
                   // $("#pre_fixtable").tableHeadFixer();
                    getamountsub(id);
                } else {
                    $('#previous_details').html(result.html);
                  //  $("#pre_fixtable").tableHeadFixer();
                }
                if (template_id == 1) {
                    $('#sub_shutterwork_description' + id).val(result.shutterdesc);
                    $('#sub_caracoss_description' + id).val(result.carcassdesc);
                    $('#sub_shutterwork_material' + id).val(result.shuttermaterial);
                    $('#sub_shutterwork_finish' + id).val(result.shutterfinish);
                    $('#sub_carcass_material' + id).val(result.carcasmaterial);
                    $('#sub_carcass_finish' + id).val(result.carcassfinish);
                } else {
                    if (template_id == 3) {
                        $('#sub_material' + id).val(result.material);
                        $('#sub_finish' + id).val(result.finish);
                    }
                    $('#sub_description' + id).val(result.description);
                }
            }
        })
    })
    function getamountsub(id) {
        var item_id = $("#sub_item" + id).val();
        var qty = $("#sub_quantity" + id).val();
        var total_amount = $("#total-amount").val();
        var total_amount_val = '<?php echo $model->mainitem_total ?>';
        var status = '<?php echo $status; ?>';
        $.ajax({
            url: "<?php echo $amountget; ?>",
            data: {
                item_id: item_id,
                qty: qty
            },
            type: "POST",
            dataType: 'json',
            success: function (result) {
                if (result != '') {
                    var amount = result.profit_amount;
                    $("#sub_amount_after_discount" + id).val(result.profit_amount);
                    var mrp_after_discount = parseInt(result.profit_amount) + parseInt((result.profit_amount * result.discount) / 100);
                    $("#sub_mrp" + id).val(mrp_after_discount);
                    var sum_value = 0;
                    $('.amountafter-discount').each(function () {
                        sum_value += +$(this).val();
                    })
                    $('.amount-after-discount-extra').each(function () {
                        sum_value += +$(this).val();
                    })
                    var total = sum_value;
                    if (status == 0) {
                        total = parseFloat(sum_value) + parseFloat(total_amount_val);
                    }
                    $('#total-amount-val').val(total);
                    if (total_amount != '') {
                        $("#sgstp").change();
                        $("#cgstp").change();
                        $("#igstp").change();
                    }

                }
            }
        });
    }

    $(document).on('change', '.quantity_sub', function () {
        var element = $(this);
        var id = $(this).attr('data-subincrement');
        var worktypeid = $("#sub_worktype" + id).val();
        var template_id = $('option:selected', $("#sub_worktypelabel" + id)).attr('data-template');
        getamountsub(id);
    });

    $(".allownumericdecimal").keydown(function (event) {
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
                (event.keyCode >= 96 && event.keyCode <= 105) ||
                event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
                event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
            var splitfield = $(this).val().split(".");
            if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
                event.preventDefault();
            }
        } else {
            event.preventDefault();
        }

        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();

    });



    function closeaction(elem, event) {
        $(elem).parents(".clients-form").hide();

    }


    function getamount(element, id) {
        var item_id = $("#hiddenitem" + id).val();
        var qty = $(element).parents('tr').find(".quantity").val();
        var total_amount = $("#total-amount").val();
        var total_amount_val = '<?php echo $model->mainitem_total ?>';
        var status = '<?php echo $status; ?>';
        $.ajax({
            url: "<?php echo $amountget; ?>",
            data: {
                item_id: item_id,
                qty: qty
            },
            type: "POST",
            dataType: 'json',
            success: function (result) {
                if (result != '') {
                    var amount = result.profit_amount;
                    $(element).parents('tr').find(".amountafter-discount").val(result.profit_amount);
                    var mrp_after_discount = parseInt(result.profit_amount) + parseInt((result.profit_amount * result.discount) / 100);
                    $(element).parents('tr').find(".mrp").val(mrp_after_discount);
                    var sum_value = 0;
                    $('.amountafter-discount').each(function () {
                        sum_value += +$(this).val();
                    })
                    $('.amount-after-discount-extra').each(function () {
                        sum_value += +$(this).val();
                    })
                    var total = sum_value;
                    if (status == 0) {
                        total = parseFloat(sum_value) + parseFloat(total_amount_val);
                    }
                    $('#total-amount-val').val(total);
                    if (total_amount != '') {
                        $("#sgstp").change();
                        $("#cgstp").change();
                        $("#igstp").change();
                    }

                }
            }
        });
    }
    $(document).on('change', '.worktype_label', function () {
        var element = $(this);
        var worktypelabel = $('option:selected', this).text();
        if ($(this).parents("#quotation_item").hasClass("sub_worktype_selector")) {
            var cat_id = element.parents(".worktype_label_row").find(".QuotationGenWorktype_master_cat_id").val();
        } else {
            var cat_id = element.parents("#quotation_item").siblings(".section_holder").find(".QuotationGenCategory_master_cat_id").val();
        }

        var worktypeid = element.parent().siblings('.worktypedata').attr('data-id');
        var template_id = element.parent().siblings('.worktypedata').attr('data-template');
        if (worktypelabel != '') {
            element.parent().siblings().find('input[type=text],select').addClass('commonadd-val');
        } else {
            element.parent().siblings().find('input[type=text],select').removeClass('commonadd-val');
        }
        var id = element.parent().siblings('.worktypedata').attr('data-incremnt');

        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getdatasofworktype'); ?>',
            type: 'GET',
            dataType: 'json',
            data: {
                cat_id: cat_id,
                worktypelabel: worktypelabel,
                worktypeid: worktypeid,
                template_id: template_id
            },
            success: function (result) {

                if (result.status == 1) {
                    $('#hiddenitem' + id).val(result.item_id);
                    $('#previous_details').html(result.html);
                    $("#previous_details").show();
                    // $("#pre_fixtable").tableHeadFixer();
                    getamount(element, id);
                } else {
                    $('#previous_details').html(result.html);
                    // $("#pre_fixtable").tableHeadFixer();
                }

                if (template_id == 1) {
                    $(element).parents("tr").find('#shutterwork_description' + id).val(result.shutterdesc);
                    $(element).parents("tr").find('#caracoss_description' + id).val(result.carcassdesc);
                    $(element).parents("tr").find('#shutterwork_material' + id).val(result.shuttermaterial);
                    $(element).parents("tr").find('#shutterwork_finish' + id).val(result.shutterfinish);
                    $(element).parents("tr").find('#carcass_material' + id).val(result.carcasmaterial);
                    $(element).parents("tr").find('#carcass_finish' + id).val(result.carcassfinish);
                } else {
                    if (template_id == 3) {
                        $('#material' + id).val(result.material);
                        $('#finish' + id).val(result.finish);
                    }
                    element.parents('tr').find('.description').val(result.description);

                }
            }
        })
    })
    $(function () {
        $('#add_field_demo').click(function () {
            var clone = $('.extra-work:last-child').clone();
            clone.appendTo("#container_extrawork");
            clone.find('input:text').val('');
        });
    });
    $(function () {
        $('#add_field_demo_remove').click(function () {
            var rows = $(this).parent(".btn-holder").siblings('#container_extrawork');
            var row_len = $(rows).children().length - 1;
            if (row_len >= 1) {
                $(rows).find('.extra-work:last-child').remove();
                return false;
            }
        });
    });

    $(function () {
        var regex = /^(.+?)(\d+)$/i;
        var cloneIndex = $(".sub_item").length;
        $('#add_field_sub').click(function () {
            var clone = $('.sub_item:last-child').clone();
            clone.find('input:text').val('');
            clone.appendTo("#container_subitem")
                    .attr("id", "subiteminput" + (cloneIndex + 1))
                    .find("*")
                    .each(function () {
                        var id = this.id || "";
                        var match = id.match(regex) || [];
                        if (match.length == 3) {
                            this.id = match[1] + (cloneIndex + 1);
                            $(this).attr('data-subincrement', (cloneIndex + 1));
                        }
                    })
            cloneIndex++;
        });
    });

    $(function () {
        $('#add_field_remove_sub').click(function () {
            var rows = $(this).parent(".sub_btn").siblings('#container_subitem');
            var row_len = $(rows).children().length - 1;
            if (row_len >= 1) {
                $(rows).find('.sub_item:last-child').remove();
                return false;
            }
        });
    });

    function getWorkType(category_id, callback){        
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getWorkType'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                cat_id: category_id
            },
            success: callback
        })        
    }
    
    $(document).on('change', '.quantity', function () {
        var totalRow = $(this).parents('tbody').find('tr').length - 1;
        var rowIndex = $(this).parents('tr').index();        
        var element = $(this);
        var category_id = $(this).parents(".clients-form").find("#cat_id").val();         
        var val = element.attr('data-incremntid');
        var worktype = $('#hiddenworktype' + val).val();
        var template = $('#hiddentemplate' + val).val();
        getamount(element, val);
        var increment_id = $('#qitem_table tr:last').children('td:first').attr('data-incremnt');
        var count = $('.worktypedata').length;
        var worktype_label = element.parents("tr").find('#worktype_label' + val).val();        
        if (worktype_label != '') {                        
            var $tr = $(this).parents('tbody').find('tr:last-child');
            var allTrs = $tr.closest('table').find('tr');
            var lastTr = allTrs[allTrs.length - 1];
            var $clone = $tr.clone();
            var key = parseInt(val, 10) + parseInt(1, 10);
            $clone.find('td').each(function () {
                var el = $(this).children('input,select');
                $(this).children('input,select').removeClass('commonadd-val');
                el.each(function () {
                    var id = $(this).attr('id') || null;
                    if (id) {
                        var i = id.substr(id.length - 1);
                        var name = $(this).attr('name');
                        var classname = $(this).attr('class');
                        var prefix = id.substr(0, (id.length - 1));
                        $(this).attr('id', prefix + (+count));
                        $(this).parents('tr').find('td:first-child').attr('data-incremnt', count);
                        $(this).attr('name', name);
                        $(this).attr('class', classname);
                        $(this).parents('tr').attr('class', 'clone');
                    }
                });
            });
            getWorkType(category_id, function(response) {
                $clone.find('.worktypedata').find('span').hide();
                $clone.find('.worktypedata').find('select').hide();
                $clone.find('.worktypedata').append('<select class="form-control worktypeClone">'+response.workTypeData+'</select>')           
            });
            
            $clone.find('.quantity').attr('data-incremntid', count);
            $clone.find('input:text').val('');
            if(($("tr.clone").length !=0)){                  
               var  $lastclone  = $(this).parents('tbody').find('tr.clone:last-child');
                var worktypeClone = $lastclone.find(".worktypeClone").val();
                var worktype_label = $lastclone.find(".worktype_label").val();
                var quantity = $lastclone.find(".quantity").val();
                var description =  $lastclone.find(".description").val();
                if(worktypeClone != "" && worktype_label!="" && quantity!="" && description!=""){                    
                    $tr.append($clone);
                    $($clone).insertAfter($tr);
                }else if(totalRow == rowIndex){
                    $tr.append($clone);
                    $($clone).insertAfter($tr);
                }
            }else{
                $tr.append($clone);
                $($clone).insertAfter($tr); 
            }
            
        }
    });

    function getWorkTypeLabel(workType_id,category_id, callback){        
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getWorkTypeLabelArray'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                category_id:category_id,
                workType_id: workType_id
            },
            success: callback
        })        
    }
    
    $(document).on('change','.worktypeClone',function(){
        var el = $(this);    
        var category_id = $(this).parents(".clients-form").find("#cat_id").val(); 
        var workType_id = $(this).val();
        getWorkTypeLabel(workType_id,category_id, function(response) {  
            var incId =  el.parents('td').attr('data-incremnt');
            el.parents('td').find(".hiddenworktype").attr('id','hiddenworktype'+incId);
            el.parents('td').find(".hiddenitem").attr('id','hiddenitem'+incId);
            el.parents('td').find(".hiddentemplate").attr('id','hiddentemplate'+incId);
            el.parents('td').attr('data-id',workType_id);
            el.parents('td').attr('data-template',response.template);
            el.parents('td').find('#hiddenworktype'+incId).val(workType_id);
            el.parents('td').find('#hiddentemplate'+incId).val(response.template);
            el.parents('tr').find('td:nth-child(2)').find('select').html(response.workTypeLabelData);
            el.parents('tr').find('td:nth-child(3)').html('<label style="visibility:hidden;height:0px;">Shutter Work</label><input class="form-control require commonadd-val" name="shutterwork_description[]" value="" id="shutterwork_description'+incId+'" readonly="readonly" style="visibility:hidden;height:0px;" type="text" maxlength="255"><input class="form-control require" name="material[]" value="" id="material'+incId+'" type="hidden"><input class="form-control require" name="finish[]" value="" id="finish'+incId+'" type="hidden">');
            el.parents('tr').find('td:nth-child(4)').html('<label style="visibility:hidden;height:0px;">Carcass Box Work</label><input class="form-control require  commonadd-val" name="caracoss_description[]" value="" id="caracoss_description'+incId+'"  readonly="readonly" style="visibility:hidden;height:0px;" type="text" maxlength="255"><input class="form-control require" name="shutterwork_material[]" value="" id="shutterwork_material'+incId+'"  type="hidden"><input class="form-control require" name="shutterwork_finish[]" value="" id="shutterwork_finish'+incId+'"  type="hidden"><input class="form-control require" name="carcass_material[]" value="" id="carcass_material'+incId+'"  type="hidden"><input class="form-control require" name="carcass_finish[]" value="" id="carcass_finish'+incId+'"  type="hidden"><input class="form-control require" name="material[]" value="" id="material'+incId+'"  type="hidden"><input class="form-control require" name="finish[]" value="" id="finish'+incId+'"  type="hidden"> <input class="form-control require description commonadd-val" autocomplete="off" name="description[]" value="" id="description'+incId+'"  readonly="readonly" style="visibility:visible;" type="text" maxlength="255">');

            if(response.template == 1){
                el.parents('tr').find('td:nth-child(3)').html('<label style="visibility:visible">Shutter Work</label><input class="form-control require commonadd-val" name="shutterwork_description[]" value="" id="shutterwork_description'+incId+'" readonly="readonly" style="visibility:visible" type="text" maxlength="255"><input class="form-control require" name="material[]" value="" id="material'+incId+'" type="hidden"><input class="form-control require" name="finish[]" value="" id="finish'+incId+'" type="hidden">');
                el.parents('tr').find('td:nth-child(4)').html('<label style="visibility:visible">Carcass Box Work</label><input class="form-control require  commonadd-val" name="caracoss_description[]" value="" id="caracoss_description'+incId+'"  readonly="readonly" style="visibility:visible" type="text" maxlength="255"><input class="form-control require" name="shutterwork_material[]" value="" id="shutterwork_material'+incId+'"  type="hidden"><input class="form-control require" name="shutterwork_finish[]" value="" id="shutterwork_finish'+incId+'"  type="hidden"><input class="form-control require" name="carcass_material[]" value="" id="carcass_material'+incId+'"  type="hidden"><input class="form-control require" name="carcass_finish[]" value="" id="carcass_finish'+incId+'"  type="hidden"><input class="form-control require" name="material[]" value="" id="material'+incId+'"  type="hidden"><input class="form-control require" name="finish[]" value="" id="finish'+incId+'"  type="hidden"> <input class="form-control require description commonadd-val" autocomplete="off" name="description[]" value="" id="description'+incId+'"  readonly="readonly" style="visibility:hidden;height:0px;" type="text" maxlength="255">');
            }
        });
    })
    $(document).on('change', '.amountafter-discount', function () {
        var total_amount = $("#total-amount").val();
        var total_amount_val = '<?php echo $model->mainitem_total ?>'
        var status = '<?php echo $status; ?>';
        var sum_value = 0;
        $('.amountafter-discount').each(function () {
            sum_value += +$(this).val();
        })
        $('.amount-after-discount-extra').each(function () {
            sum_value += +$(this).val();
        })
        var total = sum_value;
        if (status == 0) {
            total = parseFloat(sum_value) + parseFloat(total_amount_val);
        }
        $('#total-amount-val').val(total);
        if (total_amount != '') {
            $("#sgstp").change();
            $("#cgstp").change();
            $("#igstp").change();
        }
    });
    $(document).on('change', '.amount-after-discount-extra', function () {
        var total_amount = $("#total-amount").val();
        var total_amount_val = '<?php echo $model->mainitem_total ?>'
        var status = '<?php echo $status; ?>';
        var sum_value = 0;
        $('.amountafter-discount').each(function () {
            sum_value += +$(this).val();
        })
        $('.amount-after-discount-extra').each(function () {
            sum_value += +$(this).val();
        })
        var total = sum_value;
        if (status == 0) {
            total = parseFloat(sum_value) + parseFloat(total_amount_val);
        }
        $('#total-amount-val').val(total);
        if (total_amount != '') {
            $("#sgstp").change();
            $("#cgstp").change();
            $("#igstp").change();
        }
    });
    $(document).on('change', '.sub_worktype', function () {
        var val = $(this).val();
        var cat_id = '<?php echo $category_id ?>';
        var id = $(this).attr('data-subincrement');
        if (val != '') {
            $(this).parent().siblings().find('input[type=text],select').addClass('commonadd-val');
        } else {
            $(this).parent().siblings().find('input[type=text],select').removeClass('commonadd-val');
        }
        $("#sub_worktypelabel" + id).html('<option value="">Select Label</option>');
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('salesquotation/SalesQuotation/getworktypelabel'); ?>',
            method: 'POST',
            data: {
                worktype: val,
                cat_id: cat_id
            },
            dataType: "json",
            success: function (response) {
                if (response.status == 'success') {
                    $("#sub_worktypelabel" + id).html(response.labellist);
                } else {
                    $("#sub_worktypelabel" + id).html(response.labellist);
                }
            }

        })
    })

    $(function(){
        $("#sgstp").change();
        $("#cgstp").change();
        $("#igstp").change();
    })
</script>