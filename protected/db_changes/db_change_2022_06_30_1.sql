ALTER TABLE `jp_quotation` ADD `total_project_cost` FLOAT(20,2) NULL DEFAULT NULL AFTER `convet_invoice_status`;

CREATE TABLE `jp_payment_stage` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `quotation_id` INT(11) NOT NULL , `payment_stage` TEXT NOT NULL , `stage_percent` FLOAT NOT NULL , `stage_amount` FLOAT(11,2) NOT NULL , `invoice_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>pending,1=>generated' , `created_by` INT(11) NOT NULL , `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `updated_by` INT(11)  NULL , `updated_date` DATETIME  NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `jp_invoice` ADD `stage_id` INT(11)  NULL AFTER `type`;

ALTER TABLE `jp_invoice` CHANGE `fees` `fees` FLOAT(16,2) NULL DEFAULT NULL;
ALTER TABLE `jp_invoice` CHANGE `amount` `amount` FLOAT(20,2) NULL DEFAULT NULL;
ALTER TABLE `jp_invoice` CHANGE `subtotal` `subtotal` FLOAT(20,2) NULL DEFAULT NULL;