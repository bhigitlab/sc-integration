<?php
/* @var $this LogController */
/* @var $model JpLog */

$this->breadcrumbs=array(
	'Jp Logs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List JpLog', 'url'=>array('index')),
	array('label'=>'Manage JpLog', 'url'=>array('admin')),
);
?>

<h1>Create JpLog</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>