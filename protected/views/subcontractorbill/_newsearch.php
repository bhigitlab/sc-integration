<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<?php
/* @var $this SubcontractorController */
/* @var $model Subcontractor */
/* @var $form CActiveForm */
?>
<?php
$user = Users::model()->findByPk(Yii::app()->user->id);
$userId = Yii::app()->user->id;
$arrVal = explode(',', $user->company_id);
$query_company = "";
$query_company1 = "";
foreach ($arrVal as $arr) {
    if ($query_company)
        $query_company .= ' OR';
    if ($query_company1)
        $query_company1 .= ' OR';
    $query_company .= " FIND_IN_SET('" . $arr . "', id)";
    $query_company1 .= " FIND_IN_SET('" . $arr . "', company_id)";
}
$tblpx = Yii::app()->db->tablePrefix;
$bills_data = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "subcontractorbill WHERE (" . $query_company1 . ") ORDER BY id desc")->queryAll();
$qoutation_data = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "scquotation WHERE (" . $query_company1 . ") AND approve_status = 'Yes'  ORDER BY scquotation_id desc")->queryAll();
//sub contractor name filter 
$subcontractor_data = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "subcontractor WHERE (" . $query_company1 . ") ORDER BY subcontractor_id desc")->queryAll();
$sql = "SELECT * FROM " . $tblpx . "projects pr LEFT JOIN " . $tblpx . "project_assign pa ON pr.pid = pa.projectid WHERE (" . $query_company1 . ") AND pa.userid = '$userId' ORDER BY pid desc";
$project_data = Yii::app()->db->createCommand($sql)->queryAll();
$vendor_data = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "vendors WHERE (" . $query_company1 . ") ORDER BY vendor_id desc")->queryAll();
$companyInfo = Company::model()->findAll(array('condition' => $query_company));
?>
<div class="page_filter clearfix custom-form-style">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    )); ?>
    <div class="row">
        <div class="form-group col-xs-12 col-md-2 filter_elem">
            <label>Company</label>
            <?php echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array(
                'select' => array('id, name'),
                "condition" => '(' . $query_company . ')',
                'order' => 'name',
                'distinct' => true
            )), 'id', 'name'), array('class' => 'form-control js-example-basic-single companyid', 'empty' => 'Select Company', 'onchange' => 'handleCompanyChange(this)')); ?>
        </div>
        <div class="form-group col-xs-12 col-md-2 filter_elem">
            <label>Project</label>
            <select id="project_id" onchange="handleProjectChange(this)" class="select_box form-control project_id"
                name="project_id">
                <option value="">Select Project</option>
                <?php
                if (!empty($project_data)) {
                    foreach ($project_data as $key => $value) {
                        ?>
                        <option value="<?php echo $value['pid'] ?>" <?php echo ($value['pid'] == $model->project_id) ? 'selected' : ''; ?>>
                            <?php echo $value['name']; ?>
                        </option>
                    <?php }
                } ?>
            </select>
        </div>
        <div class="form-group col-xs-12 col-md-2 filter_elem">
            <label>Subcontractor</label>
            <select id="subcontractor_name" class="select_box form-control" name="subcontractor_name"
                onchange="subnameOnChange(this)">


                <option value="">Select subcontractor name</option>
                <?php
                if (!empty($subcontractor_data)) {
                    foreach ($subcontractor_data as $key => $value) {
                        ?>
                        <option value="<?php echo $value['subcontractor_name'] ?>" <?php echo ($value['subcontractor_name'] == $model->subcontractor_name) ? 'selected' : ''; ?>>
                            <?php echo $value['subcontractor_name']; ?>
                        </option>
                    <?php }
                } ?>
            </select>
        </div>

        <div class="form-group col-xs-12 col-md-2 filter_elem">
            <label>Sc Quotation No</label>
            <select id="scquotation_no" class="select_box form-control" name="scquotation_no"
                onchange="quotationOnChange(this)">
                <option value="">Select Quotation no</option>
                <?php
                if (!empty($qoutation_data)) {
                    foreach ($qoutation_data as $key => $value) {
                        ?>
                        <option value="<?php echo $value['scquotation_no'] ?>" <?php echo ($value['scquotation_no'] == $model->scquotation_no) ? 'selected' : ''; ?>>
                            <?php echo $value['scquotation_no']; ?>
                        </option>
                    <?php }
                } ?>
            </select>
        </div>


        <div class="form-group col-xs-12 col-md-2 filter_elem">
            <label>Bill no</label>
            <select id="Bills_bill_number" class="select_box form-control js-example-basic-single"
                name="Subcontractorbill[bill_number]">
                <option value="">Select Bill no</option>
                <?php
                if (!empty($bills_data)) {
                    foreach ($bills_data as $key => $value) {
                        ?>
                        <option value="<?php echo $value['bill_number'] ?>" <?php echo ($value['bill_number'] == $model->bill_number) ? 'selected' : ''; ?>>
                            <?php echo $value['bill_number']; ?>
                        </option>
                    <?php }
                } ?>
            </select>
        </div>
        <?php
        if (!isset($_GET['date_from']) || $_GET['date_from'] == '') {
            // $datefrom = date("Y-m-") . "01";
            //$datefrom = date("Y-")."01-" . "01";
            //$datefrom = date('d-m-Y', strtotime('today - 30 days'));
            $datefrom = "";
        } else {
            $datefrom = $_GET['date_from'];
        }
        ?>
        <div class="form-group col-xs-12 col-md-2">
            <label>Date From </label>
            <?php echo CHtml::textField('date_from', $datefrom, array('value' => $datefrom, 'readonly' => true, 'class' => 'form-control w-100')); ?>
        </div>
        <?php
        if (!isset($_GET['date_to']) || $_GET['date_to'] == '') {
            //$date_to = date("Y-m-d");
            //$date_to = date("d-m-Y");
            $date_to = "";
        } else {
            $date_to = $_GET['date_to'];
        }
        ?>
        <div class="form-group col-xs-12 col-md-2">
            <label>Date To </label>
            <?php echo CHtml::textField('date_to', $date_to, array('value' => $date_to, 'readonly' => true, 'class' => 'form-control w-100')); ?>
        </div>
        <div class="form-group col-xs-12 col-md-2 margin-top-20">
            <input type="submit" id="submitButton" value="Go" class="btn btn-sm btn-primary" />
            <input type="reset" id="reset" value="Clear" class="btn btn-sm btn-default" />
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.js-example-basic-single').select2({
            width: '100%',
        });
    });
    $('#reset').click(function () {
        location.href = '<?php echo $this->createUrl('admin'); ?>';
    })
    $(document).ready(function () {
        $(".select_box").select2();
        $(function () {
            $("#date_from").datepicker({
                dateFormat: 'dd-mm-yy'
            });

            $("#date_to").datepicker({
                dateFormat: 'dd-mm-yy'
            });

            $("#created_date_from").datepicker({
                dateFormat: 'dd-mm-yy'
            });

            $("#created_date_to").datepicker({
                dateFormat: 'dd-mm-yy'
            });

        });
        $("#date_from").change(function () {
            $("#date_to").datepicker('option', 'minDate', $(this).val());
        });
        $("#date_to").change(function () {
            $("#date_from").datepicker('option', 'maxDate', $(this).val());
        });
        $("#created_date_from").change(function () {
            $("#created_date_to").datepicker('option', 'minDate', $(this).val());
        });
        $("#created_date_to").change(function () {
            $("#created_date_from").datepicker('option', 'maxDate', $(this).val());
        });



    });
    function handleCompanyChange(elem) {
        let company_id = $(elem).val();
        let scquotation_no = $("#scquotation_no").val();
        // alert(company_id);
        $.ajax({
            type: "GET",
            // url: ' echo Yii::app()->createUrl('subcontractorbill/getAllQuotations'); ?>',
            url: '<?php echo Yii::app()->createUrl('subcontractorbill/getAllProject'); ?>',
            data: {
                company_id: company_id,
            },
            dataType: 'json',
            success: function (response) {
                $("#project_id").html(response.projects);
                $("#subcontractor_name").html(response.subcontractors);
            }
        });
    }
    function handleProjectChange(elem) {
        let project_id = $(elem).val();
        $.ajax({
            type: "GET",
            url: '<?php echo Yii::app()->createUrl('subcontractorbill/getSubContractorName'); ?>',
            data: {
                project_id: project_id,
            },
            dataType: 'json',
            success: function (response) {
                $("#subcontractor_name").html(response.subcontractors);
            }
        });
    }
    function getsubcontractors(elem) {
        let company_id = $('#company_id').val();
        $.ajax({
            method: "GET",
            url: "<?php echo Yii::app()->createUrl('subcontractorbill/getAllSubContractorName'); ?>",
            data: {
                company_id: company_id,
            },
            dataType: "json",
            success: function (response) {
                //alert(response);
                $("#subcontractor_name").html(response.subcontractors);
            }
        });
    }
    function quotationOnChange(elem) {
        let quotation_no = $(elem).val();
        let bill_no = $("#Bills_bill_number").val();
        //alert(quotation_no);
        $.ajax({
            method: "GET",
            url: '<?php echo Yii::app()->createUrl('subcontractorbill/getAllBills'); ?>',
            data: {
                quotation_no: quotation_no,
            },
            dataType: "json",
            success: function (response) {
                $("#Bills_bill_number").html(response.bills);
            }

        })
    }
    function subnameOnChange(elem) {
        let subcontractor_name = $(elem).val();
        let scquotation_no = $("#scquotation_no").val();
        $.ajax({
            type: "GET",
            url: '<?php echo Yii::app()->createUrl('subcontractorbill/getAllSubQuotation'); ?>',
            data: {
                subcontractor_name: subcontractor_name,
            },
            dataType: 'json',
            success: function (response) {
                $("#scquotation_no").html(response.quotations);

            }
        });
    }
    $(document).ready(function () {
        function dynamiBillLoading() {
            let company_id = $(".companyid").val();
            let scquotation_no = $("#scquotation_no").val();
            let bill_no = $("#Bills_bill_number").val();

            let project_id = $("#project_id").val();
            let subcontractor_name = $("#subcontractor_name").val();
            if (company_id !== '') {
                // if(project_id == '' ){
                $.ajax({
                    type: "GET",
                    url: '<?php echo Yii::app()->createUrl('subcontractorbill/getAllProject'); ?>',
                    data: {
                        company_id: company_id,
                    },
                    dataType: 'json',
                    success: function (response) {
                        $("#project_id").html(response.projects);
                        if (project_id !== '') {
                            $("#project_id").val(project_id);
                        }
                        // if(subcontractor_name !== ''){
                        //     subnameOnChange(subcontractor_name);
                        // }else{
                        //     getsubcontractors(company_id);
                        // }


                    }
                });

                // }else{
                //     if(subcontractor_name == ''){
                //         getsubcontractors(company_id);
                //     }

                // }
                // if(subcontractor_name !== ''){
                //     subnameOnChange(subcontractor_name);
                // }


                // if(scquotation_no!=='' && bill_no == ''){
                //     $.ajax({
                // method:"GET",
                // url: '<?php echo Yii::app()->createUrl('subcontractorbill/getAllBillsDynamic'); ?>',
                // data:{
                //     quotation_no:scquotation_no,
                //     company_id:company_id
                // },
                // dataType:"json",
                // success:function(response){
                //     $("#Bills_bill_number").html(response.bills);
                // }

                // })
                // }


            }
            if (project_id !== '') {
                $.ajax({
                    type: "GET",
                    url: '<?php echo Yii::app()->createUrl('subcontractorbill/getSubContractorName'); ?>',
                    data: {
                        project_id: project_id,
                    },
                    dataType: 'json',
                    success: function (response) {
                        $("#subcontractor_name").html(response.subcontractors);
                        if (subcontractor_name !== '') {
                            $("#subcontractor_name").val(subcontractor_name);
                        }

                    }
                });
            }
            if (subcontractor_name !== '') {
                let scquotation_no = $("#scquotation_no").val();
                $.ajax({
                    type: "GET",
                    url: '<?php echo Yii::app()->createUrl('subcontractorbill/getAllSubQuotation'); ?>',
                    data: {
                        subcontractor_name: subcontractor_name,
                    },
                    dataType: 'json',
                    success: function (response) {
                        $("#scquotation_no").html(response.quotations);
                        if (scquotation_no !== '') {
                            $("#scquotation_no").val(scquotation_no);
                        }
                    }
                });
            }
            if (scquotation_no !== '') {
                let bill_no = $("#Bills_bill_number").val();
                let quotation_no = $("#scquotation_no").val();
                $.ajax({
                    method: "GET",
                    url: '<?php echo Yii::app()->createUrl('subcontractorbill/getAllBills'); ?>',
                    data: {
                        quotation_no: quotation_no,
                    },
                    dataType: "json",
                    success: function (response) {
                        $("#Bills_bill_number").html(response.bills);
                        if (bill_no !== '') {

                            $("#Bills_bill_number").val(bill_no);
                        }
                    }

                })
            }

        }
        dynamiBillLoading();
    });


</script>