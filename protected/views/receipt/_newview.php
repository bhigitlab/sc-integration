<?php
$id=$data->rec_id; 
		//echo $id; die();
	?>
	<div class="row">
		<div class="col-md-4 col-sm-4 col-xs-4">
			<div class="list_item"><span>Date: </span><?php if($data->date!= ""){ echo date("d-M-Y", strtotime($data->date)); } ?></div>
			<div class="list_item"><span>Description: </span><?php echo $data->description;?></div>
		</div>
		<div class="col-md-5 col-sm-3 col-xs-3">
			<div class="list_item"><span>Project Name: </span><?php echo $data->project['name'];?></div>
			
		</div>
		<div class="col-md-2 col-sm-4 col-xs-3">
                    <div class="list_item"><span>Amount: </span><span style="float:right"><?php echo ($data->receipt_amount  ? number_format($data->receipt_amount,0):0);?></span></div>
		    
		</div>
		<div class="col-md-1 col-sm-1 col-xs-2">
			<?php if(Yii::app()->user->role==1){ ?>
			<span class="fa fa-edit editReceipt" data-toggle="modal" data-target=".edit" data-id="<?php echo $id; ?>"></span>
			<?php } ?>

		</div>
	</div>
