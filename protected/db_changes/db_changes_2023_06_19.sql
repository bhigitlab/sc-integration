ALTER TABLE `jp_purchase_items` CHANGE `base_qty` `base_qty` DOUBLE NULL DEFAULT NULL;
ALTER TABLE `jp_purchase_items` CHANGE `base_unit` `base_unit` VARCHAR(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `base_rate` `base_rate` DOUBLE NULL DEFAULT NULL;
