<?php


class MaterialEntriesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
    {
        $accessArr = array();
        $accessauthArr = array();
        $controller = Yii::app()->controller->id;
        if (isset(Yii::app()->user->menuall)) {
            if (array_key_exists($controller, Yii::app()->user->menuall)) {
                $accessArr = Yii::app()->user->menuall[$controller];
            }
        }

        if (isset(Yii::app()->user->menuauth)) {
            if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                $accessauthArr = Yii::app()->user->menuauth[$controller];
            }
        }
        $access_privlg = count($accessauthArr);
        return array(
            array(
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => $accessArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),

            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),

            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('ajaxcall', 'dynamicVendor'),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$id = isset($_GET['id'])?$_GET['id']:'';
        if($id){
            $model = MaterialEntries::model()->findByPk($id);
        }else{
            $model = new MaterialEntries;
			$materialItem = new MaterialItems;
        }
        $MaterialItems = new MaterialItems;
        $id = Yii::app()->user->id;
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
		
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
        }
        $currDate = date('Y-m-d', strtotime(date("Y-m-d")));
        $expense_data_sql = "SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $this->tableName('MaterialEntries') . " e";
        $expense_data_sql .= " LEFT JOIN " . $this->tableName('Projects') . " p ON e.project_id = p.pid ";
        $expense_data_sql .= "LEFT JOIN " . $this->tableName('Subcontractor') . " s ON e.subcontractor_id = s.subcontractor_id ";
        $expense_data_sql .= "LEFT JOIN " . $this->tableName('ExpenseType') . " exp on exp.type_id= e.expensehead_id ";
        $expense_data_sql .= "WHERE e.date = '" . $currDate . "' AND (" . $newQuery . ")";
        $expenseData    = Yii::app()->db->createCommand($expense_data_sql)->queryAll();
        $last_date_sql = "SELECT date FROM " . $this->tableName('MaterialEntries') . " ORDER BY date DESC LIMIT 1";
        $lastdate = Yii::app()->db->createCommand($last_date_sql)->queryRow();
        
		// Fetch unit data
		$unitData = CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name');

		$this->render('material_form', array(
			'model' => $model,
			'MaterialItems' => $MaterialItems,
			'newmodel' => $expenseData,
			'id' => $id,
			'lastdate' => $lastdate,
			'unitData' => $unitData,  
		));
	}

	public function actionDynamicProject()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $comId = $_POST["comId"];
        $materialId = $_POST["materialId"];
        
        // Retrieve material items and convert to array
        $materialItems = MaterialItems::model()->findAll('material_entry_id=:material_entry_id', array(':material_entry_id' => $materialId));
        $materialItemsArray = array();
        foreach ($materialItems as $item) {
            $materialItemsArray[] = $item->attributes;
        }
        
        // Retrieve project data
        $projectData = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects WHERE FIND_IN_SET(" . $comId . ", company_id) ORDER BY name ASC")->queryAll();
        $projectOptions = "<option value=''>-Select Project-</option>";
        foreach ($projectData as $vData) {
            $projectOptions .= "<option value='" . $vData["pid"] . "'>" . $vData["name"] . "</option>";
        }
    
        // Retrieve subcontractor data
        $bankData = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}subcontractor WHERE FIND_IN_SET(" . $comId . ", company_id) ORDER BY subcontractor_name ASC")->queryAll();
        $sub_option = "<option value=''>-Select Subcontractor-</option>";
        foreach ($bankData as $vData) {
            $sub_option .= "<option value='" . $vData["subcontractor_id"] . "'>" . $vData["subcontractor_name"] . "</option>";
        }
        
        // Encode the response as JSON
        echo json_encode(array(
            'project' => $projectOptions,
            'subcontractor' => $sub_option,
            'materialItems' => $materialItemsArray
        ));
    }

    public function actionAjaxcall()
    {
        echo json_encode('success');
    }

    public function actionzamicVendor()
    {
        $expId = $_POST["prId"];
        $tblpx = Yii::app()->db->tablePrefix;

        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', v.company_id)";
        }

        if ($expId != 0) {
            $vendorData = Yii::app()->db->createCommand("SELECT v.vendor_id as vendorid, v.name as vendorname FROM " . $tblpx . "vendors v LEFT JOIN " . $tblpx . "vendor_exptype vet ON v.vendor_id = vet.vendor_id LEFT JOIN " . $tblpx . "project_exptype pexp ON pexp.type_id=vet.type_id WHERE pexp.project_id = " . $expId . " AND (" . $newQuery . ") GROUP BY v.vendor_id ORDER BY v.name ASC")->queryAll();
        } else {
            $vendorData = Yii::app()->db->createCommand("SELECT v.vendor_id as vendorid, v.name as vendorname FROM " . $tblpx . "vendors v  WHERE (" . $newQuery . ") ORDER BY v.name ASC")->queryAll();
        }
        $vendorOptions = "<option value=''>-Select Vendor-</option>";
        foreach ($vendorData as $vData) {
            $vendorOptions .= "<option value='" . $vData["vendorid"] . "'>" . $vData["vendorname"] . "</option>";
        }
        echo $vendorOptions;
    }

    public function actionGetexpensehead()
    {
        $id = $_REQUEST['id'];
        $tblpx = Yii::app()->db->tablePrefix;

        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', sub.company_id)";
        }
        $expheadData = Yii::app()->db->createCommand("SELECT ex.type_id as typeid, ex.type_name FROM " . $tblpx . "expense_type ex LEFT JOIN " . $tblpx . "subcontractor_exptype sbe ON ex.type_id = sbe.type_id LEFT JOIN " . $tblpx . "subcontractor sub ON sub.subcontractor_id=sbe.subcontractor_id WHERE sub.subcontractor_id = " . $id . " AND (" . $newQuery . ") GROUP BY ex.type_id")->queryAll();
        $expOptions = "<option value=''>-Select Expense Head-</option>";
        foreach ($expheadData as $eData) {
            $expOptions .= "<option value='" . $eData["typeid"] . "'>" . $eData["type_name"] . "</option>";
        }
        echo $expOptions;
    }
    // Assuming you are querying inside a controller action
    public function actionGetUnit()
    {
        if (isset($_POST['material_id'])) {
            $materialId = $_POST['material_id'];
            
            // Querying using Yii1's CActiveRecord
            $materialUnit = Materials::model()->findByAttributes(array('id' => $materialId));

            if ($materialUnit !== null) {
                // Material found, do something with $materialUnit
                echo CJSON::encode(array(
                    'status' => 'success',
                    'data' => $materialUnit->attributes, // Example: return attributes as JSON
                ));
            } else {
                // Material not found
                echo CJSON::encode(array(
                    'status' => 'error',
                    'data' => 'Material not found',
                ));
            }
        } else {
            echo CJSON::encode(array(
                'status' => 'error',
                'data' => 'No material ID provided',
            ));
        }
        Yii::app()->end();
    }
	public function actionAddMaterialentries()
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$model = new MaterialEntries;
		if(!empty($materail_id)){
			$model = MaterialEntries::model()->findByPk($materail_id);
		}
		if (isset($_POST['MaterialEntries'])) {
			$user = Users::model()->findByPk(Yii::app()->user->id);
			$arrVal = explode(',', $user->company_id);
			$newQuery = "";
			foreach ($arrVal as $arr) {
				if ($newQuery) $newQuery .= ' OR';
				$newQuery .= " FIND_IN_SET('" . $arr . "',e.company_id)";
			}

			// Set model attributes
			$model->attributes = $_POST['MaterialEntries'];
			if(isset($_POST['MaterialEntries']['date'])) {
				$date = DateTime::createFromFormat('d-m-Y', $_POST['MaterialEntries']['date']);
				if ($date !== false) {
					$model->Date = $date->format('Y-m-d');
				} else {
					
					$model->addError('Date', 'Invalid date format.');
				}
			}
			$model->company_id = $_POST['MaterialEntries']['company_id'];
			$model->project_id = $_POST['MaterialEntries']['project_id'];
			$model->description = $_POST['MaterialEntries']['description'];

			// Set additional fields
			$model->subcontractor_id = $_POST['MaterialEntries']['subcontractor_id'];
			$model->expensehead_id = $_POST['MaterialEntries']['expensehead_id'];
			
			

			if (Yii::app()->user->role == 1) {
				$model->status = 1;
			} 

			if ($model->save()) {
				// Save MaterialItems data
				if (isset($_POST['MaterialItems']) && is_array($_POST['MaterialItems'])) {
					// echo '<pre>';
					// 	print_r($_POST['MaterialItems']);exit;
					foreach ($_POST['MaterialItems']['material'] as $key => $material) {
						$materialItem = new MaterialItems;
						$materialItem->material_entry_id = $model->id;
						$materialItem->material = $material;  // Use $material directly
						$materialItem->quantity = $_POST['MaterialItems']['quantity'][$key];
						$materialItem->unit = $_POST['MaterialItems']['unit'][$key];
						$materialItem->save();
					}
				}

				// Fetch the latest data for rendering
				$expDate = date('Y-m-d', strtotime($_POST['MaterialEntries']['date']));
				$expenseData = Yii::app()->db->createCommand()
					->select('e.*, p.name as pname, s.subcontractor_name as subname, exp.type_name')
					->from($tblpx . 'material_entries e')
					->leftJoin($tblpx . 'projects p', 'e.project_id = p.pid')
					->leftJoin($tblpx . 'subcontractor s', 'e.subcontractor_id = s.subcontractor_id')
					->leftJoin($tblpx . 'expense_type exp', 'exp.type_id = e.expensehead_id')
					->where('e.date = :date AND (' . $newQuery . ')', array(':date' => $expDate))
					->queryAll();
			    
				$client = $this->renderPartial('newlist', array('newmodel' => $expenseData));
				return $client;
			} else {
				echo 1;
			}
		}
	}

	public function actionGetAllData()
    {
        $date       = $_REQUEST["date"];
        $tblpx      = Yii::app()->db->tablePrefix;
        $user       = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal     = explode(',', $user->company_id);
        $newQuery   = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
        }

        $expDate        = date('Y-m-d', strtotime($date));
        $expenseData = Yii::app()->db->createCommand("SELECT e.*, p.name as pname,s.subcontractor_name as subname,exp.type_name FROM " . $tblpx . "dailyreport e
                                LEFT JOIN " . $tblpx . "projects p ON e.projectid = p.pid
                                LEFT JOIN " . $tblpx . "subcontractor s ON e.subcontractor_id = s.subcontractor_id LEFT JOIN " . $tblpx . "expense_type exp on
                                exp.type_id= e.expensehead_id
                                WHERE e.date = '" . $expDate . "' AND (" . $newQuery . ")")->queryAll();
        $client         = $this->renderPartial('newlist', array('newmodel' => $expenseData));
        return $client;
    }
	public function actionUpdateMaterialentries()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        
        if (isset($_POST['MaterialEntries']['dr_id'])) {
            $material_id = $_POST['MaterialEntries']['dr_id'];
            $model = MaterialEntries::model()->findByPk($material_id);
          
            if ($model !== null && isset($_POST['MaterialEntries'])) {
                $user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                foreach ($arrVal as $arr) {
                    if ($newQuery) $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET(:company_id_$arr, e.company_id)";
                }

                // Update model attributes
                $model->attributes = $_POST['MaterialEntries'];
                if (isset($_POST['MaterialEntries']['date'])) {
                    $date = DateTime::createFromFormat('d-m-Y', $_POST['MaterialEntries']['date']);
                    if ($date !== false) {
                        $model->Date = $date->format('Y-m-d');
                    } else {
                        $model->addError('Date', 'Invalid date format.');
                    }
                }
                $model->company_id = $_POST['MaterialEntries']['company_id'];
                $model->project_id = $_POST['MaterialEntries']['project_id'];
                $model->description = $_POST['MaterialEntries']['description'];

                $model->subcontractor_id = $_POST['MaterialEntries']['subcontractor_id'];
                $model->expensehead_id = $_POST['MaterialEntries']['expensehead_id'];

                // Set status based on user role
                $model->status =  1 ;

                if ($model->save()) {
                    // Update MaterialItems data
                    if (isset($_POST['MaterialItems']) && is_array($_POST['MaterialItems'])) {
                        // First, delete existing MaterialItems for this entry
                        MaterialItems::model()->deleteAllByAttributes(['material_entry_id' => $model->id]);

                        // Then, save the new MaterialItems
                        foreach ($_POST['MaterialItems']['material'] as $key => $material) {
                            $materialItem = new MaterialItems;
                            $materialItem->material_entry_id = $model->id;
                            $materialItem->material = $material;
                            $materialItem->quantity = $_POST['MaterialItems']['quantity'][$key];
                            $materialItem->unit = $_POST['MaterialItems']['unit'][$key];
                            $materialItem->save();
                        }
                    }

                    // Fetch the latest data for rendering
                    $expDate = date('Y-m-d', strtotime($_POST['MaterialEntries']['date']));
                    $params = [':date' => $expDate];
                    foreach ($arrVal as $index => $arr) {
                        $params[":company_id_$arr"] = $arr;
                    }

                    $expenseData = Yii::app()->db->createCommand()
                        ->select('e.*, p.name as pname, s.subcontractor_name as subname, exp.type_name')
                        ->from($tblpx . 'material_entries e')
                        ->leftJoin($tblpx . 'projects p', 'e.project_id = p.pid')
                        ->leftJoin($tblpx . 'subcontractor s', 'e.subcontractor_id = s.subcontractor_id')
                        ->leftJoin($tblpx . 'expense_type exp', 'exp.type_id = e.expensehead_id')
                        ->where("e.date = :date AND ($newQuery)", $params)
                        ->queryAll();

                    $client = $this->renderPartial('newlist', ['newmodel' => $expenseData]);
                    return $client;
                } else {
                    echo 1;
                }
            } else {
                throw new CHttpException(404, 'The requested material entry does not exist.');
            }
        } else {
            throw new CHttpException(400, 'Invalid request. Please provide a valid material entry ID.');
        }
    }

    public function actionApprove()
    {
        $id = $_POST['id'];
        $tblpx = Yii::app()->db->tablePrefix;
        $transaction = Yii::app()->db->beginTransaction();

        try {
            $update = Yii::app()->db->createCommand()
                ->update("{$tblpx}material_entries", array(
                    'status' => 2,
                ), 'id=:id', array(':id' => $id));

            if ($update) {
                $notification = new Notifications;
                $notification->action = "Material Entry Approved";
                $materialEntry = MaterialEntries::model()->findByPk($id);
                $notification->message = "Material entry with ID " . $id . " has been approved.";
                $notification->parent_id = $materialEntry->id;
                $notification->date = date("Y-m-d");
                $notification->requested_by = Yii::app()->user->id;
                $notification->approved_by = Yii::app()->user->id;

                if (!$notification->save()) {
                    echo 0;
                    throw new Exception($notification->getErrors());
                }

                echo 1;
            } else {
                echo 0;
                throw new Exception('Error updating material entry status.');
            }

            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollback();
            echo 0;
            throw new CHttpException(500, $error->getMessage());
        }
    }

    public function actionDeleteMaterial()
    {
        $id = Yii::app()->request->getParam('id');
        $transaction = Yii::app()->db->beginTransaction();
        
        try {
            $model = MaterialEntries::model()->findByAttributes(array('id' => $id));
            
            if ($model !== null) {
                $materialItems = MaterialItems::model()->findAllByAttributes(array('material_entry_id' => $id));
                
                // Delete each MaterialItems
                foreach ($materialItems as $item) {
                    $item->delete();
                }
                // Now delete the main MaterialEntries model
                if ($model->delete()) {
                    $transaction->commit();
                    $response = [
                        'success' => 'success',
                        'message' => 'Record deleted successfully.'
                    ];
                } else {
                    throw new Exception(json_encode($model->getErrors()));
                }
            } else {
                throw new Exception('Material entry not found.');
            }
            
            echo json_encode($response);
            
        } catch (Exception $error) {
            $transaction->rollback();
            $response = [
                'error' => 'error',
                'message' => $error->getMessage()
            ];
            
            echo json_encode($response);
        }
    }


	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('MaterialEntries');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new MaterialEntries('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MaterialEntries']))
			$model->attributes=$_GET['MaterialEntries'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MaterialEntries the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MaterialEntries::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MaterialEntries $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='material-entries-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
