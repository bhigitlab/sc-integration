<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Projects',
);
$page = Yii::app()->request->getParam('user_page');
if ($page != '') {
    Yii::app()->user->setReturnUrl($page);
} else {
    Yii::app()->user->setReturnUrl(0);
}
$active_modules = ApiSettings::model()->activeModules();
$crm_integration_status = in_array('crm', $active_modules);
$pms_integration_status = in_array('pms', $active_modules);
$integration_status = $pms_integration_status || $crm_integration_status;
$project_map_legend = '';
if ($integration_status) {
    $project_map_legend = '
        $(".dataTables_filter").wrap("<div class=\'custom-table-head-wrap\'></div>");
        $(".custom-table-head-wrap").prepend("<div class=\'legend status-legend\'><span class=\'project_not_mapped\'></span> Unmapped Projects</div>");
    ';
} ?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container" id="project">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="sub-heading margin-bottom-10">
        <h2>Projects</h2>
        <?php
        if (isset(Yii::app()->user->role) && (in_array('/projects/createproject', Yii::app()->user->menuauthlist)) && !ANCAPMS_LINKED) {
            ?>
            <button class="createpro btn btn-info">Add Project</button>
        <?php } ?>
    </div>
    <div id="errMsg"></div>
    <div class="margin-bottom-10" id="projectform" style="display:none;"></div>
    <?php $this->renderPartial('_newsearch', array('model' => $model)) ?>
    <?php
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '_newview',
        'template' => '<div class=""><table cellpadding="10" class="table" id="protable">{items}</table>{pager}</div>',
        'sortableAttributes' => array('ex_percent' => '% of Expense'),
        'ajaxUpdate' => false,
    ));
    ?>
</div>
<?php
Yii::app()->clientScript->registerScript('myjavascript', '
             $(document).ready(function(){
                $("#protable").dataTable( {
                    "scrollY": "300px",
                    "scrollCollapse": true,
                    "scrollX": true,
                    
                    "autoWidth": false,
                     "columnDefs"        : [
                        {
                            "searchable"    : false,
                            "targets"       : [0,3]
                        },
                        { "bSortable": false, "aTargets": [-1]  }
                    ],
                } );
                ' . $project_map_legend . '
                var table = $("#protable").DataTable();
                $("#Projects_name").on( "keyup", function () {
                var proname = $(this).val();
                    table
                        .columns( 1 )
                        .search( proname )
                        .draw();
                } );
                $("#Projects_project_status").on( "change", function () {
                var selectedText=" ";
                if($(this).val()!=""){
                var selectedText = $(this).find("option:selected").text();
                }
                    table
                        .columns( 10 )
                        .search( selectedText )
                        .draw();
                } );
                $("#Projects_company_id").on( "change", function () {
                var selectedText=" ";
                if($(this).val()!=""){
                var selectedText = $(this).find("option:selected").text();
                }
                    table
                        .columns( 2 )
                        .search( selectedText )
                        .draw();
                } );
                $("#clear").on( "click", function () {
                var selectedText=" ";
                    table
                        .columns([ 8, 1, 2 ,10] )
                        .search( selectedText )
                        .draw();
                } );
                $("#Projects_project_category").on( "change", function () {
                var selectedText=" ";
                if($(this).val()!=""){
                var selectedText = $(this).find("option:selected").text();
                }
                    table
                        .columns( 8 )
                        .search( selectedText )
                        .draw();
                } );
                $("#clear").on( "click", function () {
                var selectedText=" ";
                    table
                        .columns([ 8, 1, 2, 10 ] )
                        .search( selectedText )
                        .draw();
                } );

            

            });

            ');
?>
<script>

    function closeaction() {
        $('#projectform').slideUp();
    }

    function editpro(pro_id) {
        $('html,body').animate({
            scrollTop: $("body").offset().top
        }, 'slow');
        $('.loading-overlay').addClass('is-active');
        var id = pro_id;
        $.ajax({
            type: "GET",
            url: "<?php echo $this->createUrl('projects/update&layout=1&id=') ?>" + id,
            success: function (response) {
                $("#projectform").html(response).slideDown();
                $('.loading-overlay').removeClass('is-active');

            }
        });

    };
    $(document).ready(function () {
        $('.createpro').click(function () {
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                type: "GET",
                url: "<?php echo $this->createUrl('projects/create&layout=1') ?>",
                success: function (response) {
                    $("#projectform").html(response).slideDown();

                }
            });
        });
        jQuery(function ($) {
            $('#project').on('keydown', function (event) {
                if (event.keyCode == 13) {
                    $("#projectsearch").submit();
                }


            });
        });

    });
</script>


<script>
    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });

    function deletProject(elem, id) {

        if (confirm("Do you want to remove this item?")) {
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                url: '<?php echo Yii::app()->createAbsoluteUrl('projects/removeproject'); ?>',
                type: 'POST',
                dataType: 'json',
                data: {
                    pjId: id
                },
                success: function (data) {
                    console.log(data);
                    $("#errMsg").show()
                        .html('<div class="alert alert-' + data.response + '">' + data.msg + '</div>')
                        .fadeOut(10000);
                    setTimeout(function () {
                        location.reload(true);
                    }, 3000);

                }
            });
        } else {
            return false;
        }

    }
</script>