<!--<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>-->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<?php
Yii::app()->clientScript->registerScript('search', "
    $('#btSubmit').click(function(){

            $('#projectform').submit();

    });
");
$tblpx = Yii::app()->db->tablePrefix;
$fromdate = (isset($date_from) ? date('d-M-Y', strtotime($date_from)) : '');
$todate = (isset($date_to) ? date('d-M-Y', strtotime($date_to)) : '');
?>

<div class="container" id="project">
    <div class="sub-heading mb-10">
        <h3>Previous Purchase</h3>
        <div class="btn-container">
            <?php if ($date_from != '' || $date_to != '' || $item_id != '' || $vendor_id != '') { ?>
                <a style="cursor:pointer;" class="save_btn btn btn-info" title="SAVE AS EXCEL"
                    href="<?php echo $this->createAbsoluteUrl('purchase/previouspurchasexcel', array('date_from' => $date_from, 'date_to' => $date_to, 'item_id' => $item_id,/* 'items' => $items, */ 'vendor_id' => $vendor_id)) ?>">
                    <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i></a>
                <a style="cursor:pointer;" class="save_btn btn btn-info" title="SAVE AS PDF"
                    href="<?php echo $this->createAbsoluteUrl('purchase/previouspurchasepdf', array('date_from' => $date_from, 'date_to' => $date_to, 'item_id' => $item_id,/* 'items' => $items, */ 'vendor_id' => $vendor_id)) ?>">
                    <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i></a>
            <?php } ?>
        </div>
    </div>
    <?php $this->renderPartial('_searchprevious', array('date_from' => $date_from, 'date_to' => $date_to, 'item_id' => $item_id, 'items' => $items, 'vendor_id' => $vendor_id)) ?>
    <?php
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataprovider,
        'viewData' => array('date_from' => $date_from, 'date_to' => $date_to, 'item_id' => $item_id, 'items' => $items, 'vendor_id' => $vendor_id),
        'itemView' => '_prepurchase',
        'template' => '<div>{summary}{sorter}</div><div id="parent"><table class="table table-bordered list-view sorter" id="fixTable">{items}</table></div>',
    ));
    ?>
</div>
<script>
    $(document).ready(function () {
        $("#date_from").datepicker({
            dateFormat: 'dd-M-yy',
            maxDate: $("#date_to").val()
        });
        $("#date_to").datepicker({
            dateFormat: 'dd-M-yy',
        });
        $("#date_from").change(function () {
            $("#date_to").datepicker('option', 'minDate', $(this).val());
        });
        $("#date_to").change(function () {
            $("#date_from").datepicker('option', 'maxDate', $(this).val());
        });
        $("#fixTable").tableHeadFixer({ 'head': true });

    });

    $(".select2").select2();


</script>
