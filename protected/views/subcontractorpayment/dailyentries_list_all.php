<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<?php
$total_amount    = 0;
$total_taxamount = 0;
foreach ($newmodel as $expenses) {
    $total_amount = $total_amount + $expenses["amount"];
    $total_taxamount = $total_taxamount + $expenses["tax_amount"];
}
?>

<div class="" id="newlist">
    <?php
    $sort = new CSort;
    $sort->defaultOrder = 'payment_id DESC';
    $sort->attributes = array(
        'payment_id' => 'Daily Expense Id',
        'payment_id' => 'Payment ID',
        'amount' => 'Total Amount',
        'amount' => 'Paid Amount'
    );
    $dataProvider = new CArrayDataProvider($newmodel, array(
        'id' => 'payment_id', //this is an identifier for the array data provider
        'sort' => $sort,
        'keyField' => 'payment_id', //this is what will be considered your key field
        'pagination' => array(
            'pageSize' => 20, // Set the number of items per page
        ),
    ));

    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        //'data'=>$newmodel,
        'viewData' => array('total_amount' => $total_amount, 'tax_amount' => $total_taxamount),
        'itemView' => '_newviewlist_all',
        'id' => 'payment_id',
        'enableSorting' => 1,
        'enablePagination' => true,
        'template' => '<div>{summary}{sorter}</div><div id="parent"><table width="100" id="fixTable" class="table total-table">{items}</table></div>{pager}',
        //'itemView' => '_newview', 'template' => '<div>{summary}{sorter}</div><div class="tblwrap scrollTable"><div class="container1"><table cellpadding="10" class="table  list-view sorter">{items}</table></div></div>{pager}',
        'enablePagination' => true
    )); ?>


</div>
<style>
    #fixTable {
    width: 100%;
    border-collapse: collapse; /* Ensures borders don't double */
}

#fixTable td, #fixTable th {
    padding: 8px;
    text-align: left;
    border: 1px solid #ddd;
    white-space: nowrap; /* Prevents text from wrapping */
}

#fixTable th {
    background-color: #f2f2f2; /* A simple background for headers */
}

#parent {
    overflow-x: auto; /* Horizontal scrolling if needed */
}

</style>
<script>
    $(document).ready(function() {
        $("#fixTable").tableHeadFixer({
            'left': false,
            'foot': true,
            'head': true
        });
        //$("#fixTable").tableHeadFixer({'foot' : true, 'head' : true}); 
    });
    $(document).ready(function() {
        $(".popover-test").popover({
            html: true,
            content: function() {
                //return $('#popover-content').html();
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function(e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function(e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function(e) {
            $('[data-toggle=popover]').each(function() {
                // hide any open popovers when the anywhere else in the body is clicked
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });

    });
</script>
<style>
    #parent {
        max-height: 400px;
    }

    th {
        background: #eee;
    }

    .pro_back {
        background: #fafafa;
    }

    .request_back {
        background-color: #FF6347;
        color: #FFFFFF;
    }

    .request_back td:first-child {
        background-color: #FF6347;
    }


    .declined {
        background: #ffa50057 !important;
    }

    .pending-icon {
        color: #000000;
        font-size: 18px;
    }

    .total-table {
        margin-bottom: 0px;
    }

    th,
    td {
        border-right: 1px solid #ccc !important;
    }

    .legend {
        list-style: none;
        padding-left: 0px;

    }

    .legend li {
        float: left;
        margin-right: 10px;
    }

    .legend span {
        border: 1px solid #ccc;
        float: left;
        width: 12px;
        height: 12px;
        margin: 2px;
    }

    /* your colors */
    .legend .declined {
        background-color: #ffa50057;
        border: 2px solid #ffe0a8;
        margin-right: 4px;
    }

    .legend .approval_needed {
        background-color: #FF6347;
        border: 2px solid #FF6347;
        margin-right: 4px;
    }
</style>