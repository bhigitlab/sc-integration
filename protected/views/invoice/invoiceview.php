<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript"
    src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">

<style>
    .lefttdiv {
        float: left;
    }

    .purchase-title {
        border-bottom: 1px solid #ddd;
    }

    .purchase_items h3 {
        font-size: 18px;
        color: #333;
        margin: 0px;
        padding: 0px;
        text-align: left;
        padding-bottom: 10px;
    }

    .purchase_items {
        padding: 15px;
        box-shadow: 0 0 13px 1px rgba(0, 0, 0, 0.25);
    }

    .purchaseitem {
        display: inline-block;
        margin-right: 20px;
    }

    .purchaseitem last-child {
        margin-right: 0px;
    }

    .purchaseitem label {
        font-size: 12px;
    }

    .remark {
        display: none;
    }

    .padding-box {
        padding: 3px 0px;
        min-height: 17px;
        display: inline-block;
    }

    th {
        height: auto;
    }

    .quantity,
    .rate {
        max-width: 80px;
    }

    .text_align {

        text-align: center;
    }


    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .text-right {
        text-align: right;
    }

    a.btn.btn_writeoff {
        font-size: inherit;
        border: 1px solid #36b0d5;
    }

    .excel-btn {
        float: right;
        ;
        margin-right: 2px;
    }
</style>
<script>
    $(function () {
        $("#datepicker").datepicker({ dateFormat: 'dd-mm-yy' }).datepicker("setDate", new Date());
    });
</script>
<div class="container" <?php echo isset($_GET['exportpdf']) ? 'style="margin:0px 30px"' : '' ?>>
    <div class="expenses-heading header-container">
        <h3>Sales Book</h3>
        <div class="btn-container">
            <?php if (!isset($_GET['exportpdf'])) { ?>
                <?php $buttonId = ($writeoff->invoice_id == "") ? "invoiceWriteoff" : "invoiceWriteoffUpdate"; ?>
                <?php $buttonText = ($writeoff->invoice_id == "") ? "WRITEOFF" : "UPDATE WRITEOFF"; ?>
                <a id="<?php echo $buttonId; ?>" class=" btn btn-primary  btn_writeoff excel-btn">
                    <?php echo $buttonText; ?>
                </a>
                <?php echo CHtml::link('<i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>', Yii::app()->request->requestUri . "&exportpdf=1", array('class' => 'pdfbtn1  btn btn-primary ', 'title' => 'SAVE AS PDF')); ?>
                <a id="download" class="excelbtn1 btn btn-primary   excel-btn" title="SAVE AS EXCEL">
                    <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>
                </a>
            <?php } ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?php
            foreach (Yii::app()->user->getFlashes() as $key => $message) {
                $alertType = ($key == 'error') ? 'danger' : 'success';

                echo '<div class="alert alert-' . $alertType . '">' . $message . "</div>\n";
            }
            ?>
        </div>
    </div>
    <div class="box_holder">
        <form id="pdfvals1" method="post" action="">
            <input type="hidden" name="purchaseview">
            <?php if (!isset($_GET['exportpdf'])) { ?>
                <div class="row item_view">
                    <div class="col-md-3">
                        <div>
                            <label>COMPANY : </label>
                            <?php
                            $company = Company::model()->findByPk($model->company_id);
                            echo $company['name'];
                            ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div>
                            <label>PROJECT : </label>
                            <?php echo isset($project->name) ? $project->name : ''; ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div>
                            <label>CLIENT : </label>
                            <?php echo isset($client->name) ? $client->name : ''; ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div>
                            <label>DATE : </label>
                            <?php echo isset($model->date) ? date('d-m-Y', strtotime($model->date)) : ''; ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div>
                            <label>INVOICE No : </label>
                            <?php echo isset($model->inv_no) ? $model->inv_no : ''; ?>
                        </div>
                    </div>
                    <?php
                    if ($model->stage_id != 0 || $model->stage_id != "") { ?>
                        <div class="col-md-2">
                            <div>
                                <label>STAGE: </label>
                                <?php
                                $paymentstage = $payment_stage = PaymentStage::model()->findByPk($model->stage_id);
                                $stgage_name = $paymentstage['payment_stage'];
                                ?>

                                <?php echo $stgage_name; ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div>
                                <label>AMOUNT: </label>
                                <?php echo Controller::money_format_inr(($model->amount + $model->tax_amount), 2, 1); ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } else { ?>

                <table class="details">
                    <tr>
                        <td><label>COMPANY : </label><span>
                                <?php
                                $company = Company::model()->findByPk($model->company_id);
                                echo $company['name'];
                                ?>
                            </span></td>
                        <td><label>PROJECT : </label><span>
                                <?php echo isset($project->name) ? $project->name : ''; ?>
                            </span></td>
                    </tr>
                    <tr>
                        <td><label>CLIENT : </label><span>
                                <?php echo isset($client->name) ? $client->name : ''; ?>
                            </span></td>
                        <td><label>DATE : </label><span>
                                <?php echo isset($model->date) ? date('d-m-Y', strtotime($model->date)) : ''; ?>
                            </span></td>

                    </tr>

                    <tr>
                        <td><label>INVOICE No : </label><span>
                                <?php echo isset($model->inv_no) ? $model->inv_no : ''; ?>
                            </span></td>

                    </tr>
                    <tr>
                        <td><label>STAGE : </label><span>
                                <?php
                                $paymentstage = $payment_stage = PaymentStage::model()->findByPk($model->stage_id);
                                $stgage_name = $paymentstage['payment_stage'];
                                ?>
                                <?php echo $stgage_name; ?>
                            </span></td>
                        <td><label>AMOUNT : </label><span>
                                <?php echo Controller::money_format_inr(($model->amount + $model->tax_amount), 2, 1); ?>
                            </span></td>
                    </tr>

                </table>
            <?php } ?>

    </div>
    <div class="entries-wrapper">
        <div class="clearfix"></div>
        <?php // if($model->stage_id == 0 || $model->stage_id ==""){ ?>
        <div id="table-scroll" class="table-scroll">
            <div id="faux-table" class="faux-table" aria="hidden"></div>
            <div id=" table-wrapper" class="table-wrap">
                <table border="1" class="table total-table">
                    <thead class="entry-table">
                        <tr>
                            <th>Sl.No</th>
                            <th>Description</th>
                            <th>HSN Code</th>
                            <?php
                            if ($model->type == 'quantity_rate') {
                                ?>
                                <th>Quantity</th>
                                <th>Unit</th>
                                <th>Rate</th>
                            <?php } ?>
                            <th>SGST (%)</th>
                            <th>SGST Amount</th>
                            <th>CGST (%)</th>
                            <th>CGST Amount</th>
                            <th>IGST (%)</th>
                            <th>IGST Amount</th>
                            <th>Amount</th>
                            <th>Tax Amount</th>
                        </tr>
                    </thead>
                    <tbody class="addrow">
                        <?php
                        if (!empty($itemmodel)) {
                            foreach ($itemmodel as $i => $value) {
                                ?>

                                <tr class="tr_class">
                                    <td><input type="hidden" name="sl_No[]" value="<?php echo $i + 1; ?>">
                                        <?php echo $i + 1; ?>
                                    </td>
                                    <td><input type="hidden" name="description[]" value="">
                                        <?php echo $value->description; ?>
                                    </td>
                                    <td><input type="hidden" name="description[]" value="">
                                        <?php echo $value->hsn_code; ?>
                                    </td>
                                    <?php
                                    if ($model->type == 'quantity_rate') {
                                        ?>
                                        <td class="text-right"><input type="hidden" name="quantity[]"
                                                value="<?php echo $value->quantity; ?>"> <?php echo $value->quantity; ?>
                                        </td>
                                        <td><input type="hidden" name="unit[]" value="<?php echo $value->unit; ?>" readonly>
                                            <?php echo $value->unit; ?>
                                        </td>
                                        <td class="text-right"><input type="hidden" name="rate[]"
                                                value="<?php echo $value->rate; ?>" readonly>
                                            <?php echo Controller::money_format_inr($value->rate, 2, 1); ?>
                                        </td>
                                    <?php } ?>
                                    <td class="text-right">
                                        <?php echo number_format($value->sgst, 2); ?>
                                    </td>
                                    <td class="text-right">
                                        <?php echo Controller::money_format_inr($value->sgst_amount, 2, 1); ?>
                                    </td>
                                    <td class="text-right">
                                        <?php echo number_format($value->cgst, 2); ?>
                                    </td>
                                    <td class="text-right">
                                        <?php echo Controller::money_format_inr($value->cgst_amount, 2, 1); ?>
                                    </td>
                                    <td class="text-right">
                                        <?php echo number_format($value->igst, 2); ?>
                                    </td>
                                    <td class="text-right">
                                        <?php echo Controller::money_format_inr($value->igst_amount, 2, 1); ?>
                                    </td>
                                    <td class="text-right"><input type="hidden" name="amount[]"
                                            value="<?php echo $value->amount; ?>" readonly>
                                        <?php echo Controller::money_format_inr($value->amount, 2, 1); ?>
                                    </td>
                                    <td class="text-right"><input type="hidden" name="amount[]"
                                            value="<?php echo $value->amount; ?>" readonly>
                                        <?php echo Controller::money_format_inr($value->tax_amount, 2, 1); ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <?php
                        $colspan1 = 9;
                        $colspan2 = 11;
                        if ($model->type == 'quantity_rate') {
                            $colspan1 = 12;
                            $colspan2 = 14;
                        }
                        ?>
                        <tr>
                            <th style="text-align: right;" colspan="<?php echo $colspan1; ?>" class="text-right">TOTAL:
                            </th>
                            <th class="text-right">
                                <div id="grand_total">
                                    <?php echo Controller::money_format_inr($model->amount, 2, 1); ?>
                                </div>
                            </th>
                            <th class="text-right">
                                <div id="grand_total">
                                    <?php echo Controller::money_format_inr($model->tax_amount, 2, 1); ?>
                                </div>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="<?php echo $colspan2 - 1; ?>" class="text-right">ROUND OFF:</th>
                            <th class="text-right">
                                <?php echo Controller::money_format_inr($model->round_off, 2, 1) ?>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="<?php echo $colspan2 - 1; ?>" class="text-right">GRAND TOTAL:</th>
                            <th class="text-right">
                                <div id="net_amount">
                                    <?php echo Controller::money_format_inr($model->tax_amount + $model->amount + ($model->round_off), 2, 1) ?>
                                </div>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="<?php echo $colspan2 - 1; ?>" class="text-right">WRITE OFF:</th>
                            <th class="text-right">
                                <div id="net_amount">
                                    <?php echo Controller::money_format_inr($writeoff->amount, 2, 1) ?>
                                </div>
                            </th>
                        </tr>

                        <tr>
                            <th colspan="<?php echo $colspan2 - 1; ?>" class="text-right">ADDITIONAL CHARGES:
                            </th>
                            <?php $additional = Yii::app()->db->createCommand("SELECT ROUND(SUM(amount), 2) as amount FROM jp_invoice_additional_charges WHERE `invoice_id`=" . $model->invoice_id)->queryRow()['amount'];
                            ?>
                            <th class="text-right">
                                <div id="net_amount">
                                    <?php echo Controller::money_format_inr($additional, 2, 1) ?>
                                </div>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="<?php echo $colspan2 - 1; ?>" class="text-right">BALANCE:</th>

                            <th class="text-right">
                                <div id="net_amount">
                                    <?php echo Controller::money_format_inr(($model->tax_amount + $model->amount + ($model->round_off) + $additional) - $writeoff->amount, 2, 1) ?>
                                </div>
                            </th>
                        </tr>
                    </tfoot>
                </table>

            </div>
        </div>
        <?php //} ?>
        <div style="padding-right: 0px;"
            class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6"></div>
        </form>
    </div>
    <?php
    if (isset($_GET['exportpdf'])) {
        ?>
        <h4>Authorized Signatory,</h4>
        <h4>For
            <?php echo Yii::app()->name; ?>
        </h4>
        <br /><br />
        <?php
    }
    ?>
</div>
<script>
    $(function () {
        $('.pdfbtn1').click(function () {
            $("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('invoice/SaveInvoice', array('invid' => $invoice_id)); ?>");
            $("form#pdfvals1").submit();
        });
        $('.excelbtn1').click(function () {
            $("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('invoice/ExportInvoice', array('invid' => $invoice_id)); ?>");
            $("form#pdfvals1").submit();
        });
        $('#invoiceWriteoff').click(function () {
            //var invoiceId = '<?php //echo $writeoff->invoice_id; ?>';
            $("#writeoffModal").removeClass("fade");
            $("#writeoffModal").addClass("open");
            $("#writeoffModal").show();
        });
        $('#invoiceWriteoffUpdate').click(function () {
            //var invoiceId = '<?php //echo $writeoff->invoice_id; ?>';
            $("#writeoffModal").removeClass("fade");
            $("#writeoffModal").addClass("open");
            $("#writeoffModal").show();
        });
        $('.closeWriteoff').click(function () {
            //var invoiceId = '<?php //echo $writeoff->invoice_id; ?>';
            $("#writeoffModal").removeClass("open");
            $("#writeoffModal").addClass("fade");
            $("#writeoffModal").hide();
        });
    });
</script>
<?php if (!isset($_GET['exportpdf'])) { ?>
    <!-- Writeoff modal starts -->
    <div id="writeoffModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close closeWriteoff">&times;</button>
                    <h4 class="modal-title">Write off
                        <?php echo ($writeoff->invoice_id != "") ? "Update" : ""; ?>
                    </h4>
                </div>
                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'writeoff-form',
                    'action' => Yii::app()->createUrl('invoice/writeoff'),
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation' => false,
                )); ?>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <?php echo $form->labelEx($writeoff, 'amount'); ?>
                                <?php echo $form->textField($writeoff, 'amount', array('class' => 'form-control')); ?>
                                <?php echo $form->error($writeoff, 'amount'); ?>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <?php echo $form->labelEx($writeoff, 'description'); ?>
                                <?php echo $form->textField($writeoff, 'description', array('class' => 'form-control')); ?>
                                <?php echo $form->error($writeoff, 'description'); ?>
                            </div>
                        </div>
                        <?php echo $form->hiddenField($writeoff, 'id', array('type' => 'hidden')); ?>
                        <?php echo $form->hiddenField($writeoff, 'invoice_id', array('type' => 'hidden', 'value' => $model->invoice_id)); ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default closeWriteoff">Close</button>
                    <button type="submit" class="btn btn-info" id="actionWriteoff">
                        <?php echo ($writeoff->invoice_id == "") ? "ADD" : "UPDATE"; ?>
                    </button>
                </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
    <!-- Writeoff modal ends -->
<?php } ?>

<script>
    (function () {
        var mainTable = document.getElementById("main-table");
        if (mainTable !== null) {
            var tableHeight = mainTable.offsetHeight;
            if (tableHeight > 380) {
                var fauxTable = document.getElementById("faux-table");
                document.getElementById("table-wrap").className += ' ' + 'fixedON';
                var clonedElement = mainTable.cloneNode(true);
                clonedElement.id = "";
                fauxTable.appendChild(clonedElement);
            }
        }
    })();
</script>