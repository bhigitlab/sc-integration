<?php
/* @var $this VouchersController */
/* @var $model Vouchers */

$this->breadcrumbs = array(
    'Vouchers' => array('index'),
    $model->id,
);

?>
<div class="container">
    <div class="clearfix">
        <div class="pull-left">
            <h3 class="viewhead">Voucher #<?php echo $model->voucher_number; ?></h3>
        </div>
        <?php echo CHtml::link('Vouchers', array('vouchers/index'), array('class' => 'btn btn-primary pull-right mt')); ?>

    </div>

    <div class="block_purchase">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>COMPANY :</label>
                    <div><?php echo $model->company0->name; ?></div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>PROJECT :</label>
                    <div><?php echo $model->project0->name; ?></div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>SUBCONTRACTOR :</label>
                    <div><?php echo $model->scVendorData($model->voucher_for, $model->voucher_for_user_id); ?></div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>VOUCHER DATE :</label>
                    <div><?php echo $model->voucher_date; ?></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>EXPENSE HEAD :</label>
                    <div><?php echo $model->expenseHead->type_name; ?></div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>VEHICLE NUMBER :</label>
                    <div><?php echo $model->vehicle_number ?></div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>VOUCHER NUMBER :</label>
                    <div><?php echo $model->voucher_number; ?></div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>SITE ENGINEER :</label>
                    <div><?php echo $model->staff->first_name; ?></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>RECEIVED BY :</label>
                    <div><?php echo $model->received_by; ?></div>
                </div>
            </div>
        </div>
    </div>  
    <div id="table-wrap" class="table-wrap">
        <div class="table-responsive">
            <table border="1" class="table">
                <thead>
                    <tr class="table-header">
                        <th class="text-center">Sl.No</th>
                        <?php
                        if ($model->voucher_for == 1) {
                        ?>
                            <th class="text-center">Work Label</th>
                            <th class="text-center">No Of Workers</th>
                        <?php } ?>
                        <th class="text-center">Description</th>
                        <th class="text-center">Work Timing/Size</th>
                        <th class="text-center">Net Qty</th>
                    </tr>
                </thead>
                <tbody class="addrow">
                    <?php
                    $i = 1;
                    foreach ($voucher_items as $items) {
                    ?>
                        <tr>
                            <td><?php echo $i; ?></td>

                            <?php
                            if ($model->voucher_for == 1) {
                            ?>
                                <td><?php echo $items->worker_label; ?></td>
                                <td><?php echo $items->no_workers; ?></td>
                            <?php } ?>
                            <td><?php echo $items->description; ?></td>
                            <td><?php echo $items->working_time_size; ?></td>
                            <td><?php echo $items->net_quantity . ' '; ?></td>
                        </tr>
                    <?php
                        $i++;
                    }
                    ?>

                </tbody>
            </table>
        </div>
    </div>
</div>