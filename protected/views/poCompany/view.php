<?php
/* @var $this PoCompanyController */
/* @var $model PoCompany */

$this->breadcrumbs=array(
	'Po Companies'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List PoCompany', 'url'=>array('index')),
	array('label'=>'Create PoCompany', 'url'=>array('create')),
	array('label'=>'Update PoCompany', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PoCompany', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PoCompany', 'url'=>array('admin')),
);
?>

<h1>View PoCompany #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'description',
		'address',
		'pincode',
		'phone',
		'email_id',
		'company_gstnum',
		'company_id',
		'created_by',
		'created_date',
	),
)); ?>
