<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<?php
/* @var $this SubcontractorbillController */
/* @var $model Subcontractorbill */

$this->breadcrumbs=array(
	'Subcontractorbills'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

/* $this->menu=array(
	array('label'=>'List Subcontractorbill', 'url'=>array('index')),
	array('label'=>'Create Subcontractorbill', 'url'=>array('create')),
	array('label'=>'View Subcontractorbill', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Subcontractorbill', 'url'=>array('admin')),
); */
?>
<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="info" id="success_div" style="width: 500px;margin-left: 400px;padding: 5px;color: green;font-size: 20px;font-weight: bold; padding-top: 46px;">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>
<div class="container" id="vendors">
    <div class="clearfix">
        <div class="add-btn pull-right">
            <?php // if((isset(Yii::app()->user->role) && (in_array('/subcontractor/subcontractorcreate', Yii::app()->user->menuauthlist)))){ ?>
                <a href="<?php echo $this->createUrl('subcontractorbill/admin') ?>" class="button">View Bills</a>
            <?php // } ?>
        </div>
        <!-- <h2>Update Subcontractor Bill</h2> -->
    </div>
		<?php $this->renderPartial('_updateform_bill', array('model'=>$model,'itemmodel'=>$itemmodel,'quotation_items'=>$quotation_items)); ?>					
</div>
<script>
$(document).ready(function(){
    setTimeout(function(){
  $('#success_div').remove();
}, 5000);
});
</script>
