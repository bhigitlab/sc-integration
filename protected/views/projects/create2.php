<?php
/* @var $this ProjectsController */
/* @var $model Projects */

$this->breadcrumbs = array(
    'Projects' => array('index'),
    'Create',
);
?>

<div class="entries-wrapper">
    <div class="row">
        <div class="col-xs-12">
            <div class="heading-title">Add Project</div>
            <div class="dotted-line"></div>
        </div>
    </div>
    <?php echo $this->renderPartial('_form2', array('model' => $model)); ?>
</div>