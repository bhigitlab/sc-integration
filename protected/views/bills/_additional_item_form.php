    <h4>Additional Items <span class="icon icon-plus additional_item"></span></h4>
    <span class="errorcharge" style="color: red;"></span>
    <div class="itemadd" style="display:none;">
        <div class="panel panel-default ">
            <div class="panel-heading">
                <h5>Add additional Item</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="d-block">Specification:</label>

                            <?php
                            $specification = '';

                            $data = CHtml::listData(Yii::app()->controller->GetItemCategory(), 'id', 'data');
                            $static = array(
                                'other'     => Yii::t('description[]', 'other'),

                            );
                            echo CHtml::dropDownList(
                                'description[]',
                                $specification,
                                $data + $static,
                                array('empty' => 'Select one', 'class' => 'txtBox specification description', 'id' => 'description')
                            ); ?>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>HSN Code:</label>
                            <div class="hsn_value" id="hsn_value">&nbsp;&nbsp;</div>
                        </div>
                    </div>

                    <div class="col-md-1">
                        <div class="form-group">
                            <label>Batch:</label>
                            <?php echo CHtml::textField('batch', '', array('value' => '', 'class' => 'form-control batch', 'id' => "batch")); ?>

                        </div>
                    </div>

                    <div class="col-md-1">
                        <div class="form-group">
                            <label>Quantity:</label>

                            <?php echo CHtml::textField('add_quantity', '', array('value' => '', 'class' => 'form-control', 'id' => "add_quantity")); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Rate:</label>
                            <?php echo CHtml::textField('add_rate', '', array('value' => '', 'class' => 'form-control', 'id' => "add_rate")); ?>

                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="d-block">Units:</label>
                            <select class="txtBox unit_value" id="unit_value" name="unit_value" style="width: 100px;"></select>

                        </div>
                    </div>

                </div>

                <div class="row base">

                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Base Quantity:</label>

                            <?php echo CHtml::textField('base_quantity', '', array('value' => '', 'class' => 'form-control', 'id' => "base_quantity")); ?>
                            </td>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>Base Unit:</label>
                            <div class="base_unit" id="base_unit">&nbsp;&nbsp;</div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Base Rate:</label>
                            <?php echo CHtml::textField('base_rate', '', array('value' => '', 'class' => 'form-control', 'id' => "base_rate")); ?>

                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Dis amount :</label>
                            <?php echo CHtml::textField('add_dis_amount', '', array('value' => '', 'class' => 'form-control', 'id' => "add_dis_amount")); ?>
                            <div class="padding-box" id="add_dis_perc">0.00</div>(%)
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Tax Slab:</label>
                            <?php
                            $datas = TaxSlabs::model()->getAllDatas();
                            ?>
                            <select class="form-control" name="add_tax_slab" id="add_tax_slab">
                                <option value="">Select one</option>
                                <?php
                                foreach ($datas as  $value) {
                                    if ($value['set_default'] == 1) {
                                        echo ' <option data-id="' . $value['id'] . '" value="' . $value['tax_slab_value']  . '" selected>' . $value['tax_slab_value'] . '%</option>';
                                    } else {
                                        echo ' <option  data-id="' . $value['id'] . '" value="' . $value['tax_slab_value'] . '">' . $value['tax_slab_value'] . '%</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="gsts col-md-2">
                        <div class="form-group">
                            <label>SGST :</label>
                            <?php echo CHtml::textField('add_sgstp', '', array('value' => '', 'class' => 'form-control', 'id' => "add_sgstp")); ?>
                            <div class="padding-box" id="add_sgst">0.00</div>
                        </div>
                    </div>
                    <div class="gsts col-md-2">
                        <div class="form-group">
                            <label>CGST :</label>
                            <?php echo CHtml::textField('add_cgstp', '', array('value' => '', 'class' => 'form-control', 'id' => "add_cgstp")); ?>
                            <div class="padding-box" id="add_cgst">0.00</div>
                        </div>
                    </div>
                    <div class="gsts col-md-2">
                        <div class="form-group">
                            <label>IGST :</label>
                            <?php echo CHtml::textField('add_igstp', '', array('value' => '', 'class' => 'form-control', 'id' => "add_igstp")); ?>
                            <div class="padding-box" id="add_igst">0.00</div>
                        </div>
                    </div>

                    <?php echo CHtml::hiddenField('bill_additonal_item_id', '', array('id' => "bill_additonal_item_id")); ?>
                    <div class="col-md-2 mt">
                        <div class="text-right"><span>Tax Amount</span>:<b name="add_tax_amount" id="add_tax_amount" class="ml"></b></div>
                        <div class="text-right"><span>Tax %</span>:<b name="add_tax_perc" id="add_tax_perc" class="ml"></b></div>
                        <div class="text-right"><span>Total Amount</span>:<b name="add_total_amount" id="add_total_amount" class="ml"></b></div>
                    </div>
                </div>
                <div class="text-right">
                    <input type="button" class="btn btn-info" id="additional_item_add" value="Add">
                </div>
            </div>
        </div>
    </div>

    <script>
        <?php $hsngeturl = Yii::app()->createAbsoluteUrl("PurchaseCategory/gethsncode"); ?>
        <?php $additionalItem = Yii::app()->createAbsoluteUrl("bills/additionalitems"); ?>
        $(document).ready(function() {
            $(".specification").select2();
            $("#unit_value").select2();
            $('.additional_item').click(function() {
                var bill_number = $('#Bills_bill_number').val();
                if (bill_number !== '') {
                    $(".itemadd").toggle();
                    $(this).toggleClass("icon-minus hidecharge");
                } else {
                    $("#alert").addClass("alert alert-danger").html("<strong>Please enter bill number<strong>");
                }
            });
            $('#description').change(function() {

                $('#loading').show();
                var item_id = this.value;
                $('select[id="unit_value"]').empty();
                $.ajax({
                    url: "<?php echo $hsngeturl; ?>",
                    data: {
                        "id": item_id,
                    },
                    type: "POST",
                    dataType: 'json',
                    success: function(data) {
                        $('#hsn_value').text(data.hsn_code);
                        if (data.unit_title != '') {
                            $.each(data.unit_title, function(key, value) {
                                $('#base_unit').text(value.base_unit);
                                if (value.base_unit == value.conversion_unit) {
                                    var selected = 'selected';
                                } else {
                                    var selected = '';
                                }
                                $('select[id="unit_value"]').append('<option value="' + value.conversion_unit + '" ' + selected + '>' + value.conversion_unit + '</option>');
                            });
                        } else {
                            $('#base_unit').text(data.baseunit);
                            $('select[id="unit_value"]').append('<option value="' + data.baseunit + '" >' + data.baseunit + '</option>');
                        }
                    }
                });

            })


            $("#add_quantity, #add_rate,#add_dis_amount, #add_sgstp, #add_cgstp, #add_igstp").blur(function() {
                var quantity = parseFloat($("#add_quantity").val());
                var rate = parseFloat($("#add_rate").val());
                var length = $("#bilength").text();
                var width = $("#biwidth").text();
                var height = $("#biheight").text();
                var purchase_type = $("#purchase_type").val();
                var amount = (quantity * rate).toFixed(2);

                var new_amount = 0;
                var dis_amount = parseFloat($("#add_dis_amount").val());
                var sgst = parseFloat($("#add_sgstp").val());


                if (sgst > 100 || sgst < 0) {
                    $("#alert").addClass("alert alert-danger").html("<strong>Percentage value should be between 0 and 100");
                    $("#add_sgstp").val(0);
                    sgst = 0;
                }

                var cgst = parseFloat($("#add_cgstp").val());
                if (cgst > 100 || cgst < 0) {
                    $("#alert").addClass("alert alert-danger").html("<strong>Percentage value should be between 0 and 100");
                    $("#add_cgstp").val(0);
                    cgst = 0;
                }
                var igst = parseFloat($("#add_igstp").val());
                if (igst > 100 || igst < 0) {
                    $("#alert").addClass("alert alert-danger").html("<strong>Percentage value should be between 0 and 100");
                    $("#add_igstp").val(0);
                    igst = 0;
                }
                if (isNaN(dis_amount)) dis_amount = 0;
                if (isNaN(amount)) amount = 0;
                new_amount = amount - dis_amount;
                if (isNaN(new_amount)) new_amount = 0;
                var sgst_amount = (sgst / 100) * new_amount;
                var cgst_amount = (cgst / 100) * new_amount;
                var igst_amount = (igst / 100) * new_amount;

                if (isNaN(sgst_amount)) sgst_amount = 0;
                if (isNaN(cgst_amount)) cgst_amount = 0;
                if (isNaN(igst_amount)) igst_amount = 0;
                var tax_amount = sgst_amount + cgst_amount + igst_amount;
                var newTaxP = ((tax_amount / new_amount) * 100);
                if (newTaxP % 1 !== 0)
                    newTaxP = newTaxP.toFixed(2);
                newTaxP = isNaN(newTaxP) ? 0 : newTaxP;
                var total_amount = new_amount + tax_amount;

                var disp = (dis_amount / amount) * 100;
                if (isNaN(disp)) disp = 0;
                $("#add_dis_perc").html(disp.toFixed(2));
                $("#add_dis_amount").html(dis_amount.toFixed(2));
                $("#add_sgst").html(sgst_amount.toFixed(2));
                $("#add_cgst").html(cgst_amount.toFixed(2));
                $("#add_igst").html(igst_amount.toFixed(2));
                console.log(tax_amount);

                console.log(total_amount);
                $("#add_tax_amount").html(tax_amount.toFixed(2));
                $("#add_tax_perc").html(newTaxP);
                $("#add_total_amount").html(total_amount.toFixed(2));

                var type = 1;
                getconversionFactor(type);
            });

            $("#base_quantity").blur(function() {
                var type = 2;
                getconversionFactor(type);
            });

            $("#unit_value").change(function() {
                var type = 1;
                getconversionFactor(type);

            });

            function getconversionFactor(type) {

                var itemId = parseInt($("#description option:selected").val());
                var base_unit = $.trim($("#base_unit").text());
                var amt = $("#add_total_amount").text();
                var quantity = parseFloat($("#add_quantity").val());
                quantity = isNaN(quantity) ? 0 : quantity;
                var purchase_unit = $("#unit_value option:selected").text();
                var rate = parseFloat($("#add_rate").val());

                if (quantity % 1 == 0)
                    quantity = quantity;
                else
                    quantity = quantity.toFixed(4);
                if (rate != 0)
                    rate = parseFloat(rate.toFixed(2));
                var purchase_type = $("#purchase_type").val();
                var baseqty = parseFloat($("#base_quantity").val());
                var baserate = 0;



                if (quantity > 0) {
                    var amt = (quantity * rate).toFixed(2);
                } else {
                    amt = 0;
                }

                if (purchase_unit !== '' && base_unit !== '' && itemId !== "") {
                    if (base_unit != purchase_unit) {

                        $.ajax({
                            method: "POST",
                            data: {
                                'purchase_unit': purchase_unit,
                                'base_unit': base_unit,
                                'item_id': itemId
                            },
                            dataType: "json",
                            url: '<?php echo Yii::app()->createAbsoluteUrl('bills/getUnitconversionFactor'); ?>',
                            success: function(result) {


                                $(".base").show();
                                if (type == 1) {
                                    var base_quantity = parseFloat(result * quantity).toFixed(2);
                                } else {
                                    var base_quantity = baseqty;
                                }

                                if (isNaN(base_quantity))
                                    base_quantity = 0;
                                if (base_quantity != 0) {
                                    baserate = amt / base_quantity;
                                    baserate = baserate.toFixed(2);
                                    if (isNaN(baserate))
                                        baserate = 0;
                                }
                                $("#base_quantity").attr("readonly", false);
                                $("#base_quantity").val(base_quantity);
                                $("#base_rate").val(baserate);
                            }
                        });
                    } else {
                        var baseval = 0;
                        $("#base_quantity").attr("readonly", true);
                        $("#base_quantity").val(quantity);
                        $("#base_rate").val(rate);
                        $(".base").hide();
                    }

                }

            }

            $('#additional_item_add').click(function() {

                var purchaseId = $("#Bills_purchase_id").val();
                var company_id = $("#Bills_company_id").val();
                var billNumber = $("#Bills_bill_number").val();
                var billDate = $("#Bills_bill_date").val();
                var batch = $("#batch").val();

                var billId = $("#billid").val();
                var quantity = parseFloat($("#add_quantity").val());
                quantity = isNaN(quantity) ? 0 : quantity;
                var unit = $("#unit_value option:selected").text();
                var rate = parseFloat($("#add_rate").val());
                var hsn_code = $("#hsn_value").text();
                var purch_unit = $("#base_unit").text();


                var Warehouse_id = $('#Warehousestock_warehousestock_warehouseid').val();
                if (Warehouse_id == "") {
                    $('#Warehousestock_warehousestock_warehouseid_em_').show();
                    $('#Warehousestock_warehousestock_warehouseid_em_').html('Warehouse is required');
                } else {
                    $('#Warehousestock_warehousestock_warehouseid_em_').hide();
                    $('#Warehousestock_warehousestock_warehouseid_em_').html('');
                }



                rate = isNaN(rate) ? 0 : rate;
                var amount = quantity * rate;
                var damount = parseFloat($("#add_dis_amount").val());
                damount = isNaN(damount) ? 0 : damount;
                var dpercent = parseFloat($("#add_dis_perc").text());
                if (dpercent % 1 !== 0)
                    dpercent = dpercent.toFixed(2);
                var cgstpercent = parseFloat($("#add_cgstp").val());
                cgstpercent = isNaN(cgstpercent) ? 0 : cgstpercent;
                var cgst = parseFloat($("#add_cgst").text());
                if (cgst % 1 !== 0)
                    cgst = cgst.toFixed(2);
                var sgstpercent = parseFloat($("#add_sgstp").val());
                sgstpercent = isNaN(sgstpercent) ? 0 : sgstpercent;
                var igstpercent = parseFloat($("#add_igstp").val());
                igstpercent = isNaN(igstpercent) ? 0 : igstpercent;
                var sgst = parseFloat($("#add_sgst").text());
                if (sgst % 1 !== 0)
                    sgst = sgst.toFixed(2);
                var igst = parseFloat($("#add_igst").text());
                if (igst % 1 !== 0)
                    igst = igst.toFixed(2);
                var taxtotal = parseFloat($("#add_tax_amount").text());
                if (taxtotal % 1 !== 0)
                    taxtotal = taxtotal.toFixed(2);
                var taxpercent = parseFloat($("#add_tax_perc").text());
                if (taxpercent % 1 !== 0)
                    taxpercent = taxpercent.toFixed(2);
                var totalamount = parseFloat($("#add_total_amount").text());
                var categoryId = $("#description").val();
                var categoryName = $("#description option:selected").text();
                var tax_slab = parseFloat($("#add_tax_slab").val());
                var bill_add_item_id = $("#bill_additonal_item_id").val();
                dpercent = isNaN(dpercent) ? 0 : dpercent;
                cgst = isNaN(cgst) ? 0 : cgst;
                sgst = isNaN(sgst) ? 0 : sgst;
                igst = isNaN(igst) ? 0 : igst;
                taxtotal = isNaN(taxtotal) ? 0 : taxtotal;
                $('.loading-overlay').addClass('is-active');


                if (purch_unit != unit) {
                    var purchasequantity = $("#base_quantity").val();
                    var purchaseunit = $.trim(purch_unit);
                    var purchaserate = $("#base_rate").val();
                } else {
                    var purchasequantity = quantity;
                    var purchaseunit = $.trim(purch_unit);
                    var purchaserate = rate;
                }

                $.ajax({
                    url: "<?php echo $additionalItem; ?>",
                    data: {
                        "pid": purchaseId,
                        "billnumber": billNumber,
                        "billdate": billDate,
                        "billitembatch": batch,
                        "warehouse": Warehouse_id,
                        "quantity": quantity,
                        "unit": unit,
                        "rate": rate,
                        "purchasequantity": purchasequantity,
                        "purchaseunit": purchaseunit,
                        "purchaserate": purchaserate,
                        "amount": amount,
                        "damount": damount,
                        "dpercent": dpercent,
                        "cgst": cgst,
                        "cgstpercent": cgstpercent,
                        "sgst": sgst,
                        "sgstpercent": sgstpercent,
                        "igst": igst,
                        "igstpercent": igstpercent,
                        "totaltax": taxtotal,
                        "totaltaxp": taxpercent,
                        "totalamount": totalamount,
                        "billid": billId,
                        "categoryid": categoryId,
                        "categoryname": categoryName,
                        "company_id": company_id,
                        "tax_slab": tax_slab,
                        "hsn_code": hsn_code,
                        'billitemid': bill_add_item_id
                    },
                    type: "POST",
                    dataType: 'json',
                    success: function(result) {

                        $("#billid").val(result[0]);
                        $('#description').val('').trigger('change.select2');
                        $('#hsn_value').text('');
                        $('#add_quantity').val('');
                        $('#unit_value').text('');
                        $('#add_rate').val('');
                        $('#add_dis_amount').val('');
                        $('#add_dis_perc').text('');
                        $('#add_tax_slab').val('18').trigger('change.select2');;
                        $('#add_sgstp').val('');
                        $('#add_sgst').text('');
                        $('#add_cgstp').val('');
                        $('#add_cgst').text('');
                        $('#add_igstp').val('');
                        $('#add_igst').text('');
                        $('#add_tax_amount').text('');
                        $('#add_tax_perc').text('');
                        $('#bill_additonal_item_id').val('');
                        $('#add_total_amount').text();
                        $('#base_quantity').val('');
                        $('#batch').val('');
                        $('#base_rate').val('');
                        $('#base_unit').text('');
                        $(".base").hide();

                        if (result.status === '1') {

                            setCookie("billid", result[0], 1);
                            setCookie("billnumber", billNumber, 1);
                            if (result['amount'] !== '') {
                                var amount = parseFloat(result['amount']);
                                $('#Bills_bill_amount').text(amount.toFixed(2));
                            }
                            if (result['disc_amount'] !== '') {
                                var disc_amount = parseFloat(result['disc_amount']);
                                $('#Bills_bill_discountamount').text(disc_amount.toFixed(2));
                            }
                            if (result['tax_amount'] !== '') {
                                var tax_amount = parseFloat(result['tax_amount']);
                                $('#Bills_bill_taxamount').text(tax_amount.toFixed(2));
                            }
                            if (result['tot_amount'] !== '') {
                                var tot_amount = parseFloat(result['tot_amount']);
                                $('#Bills_bill_totalamount').val(tot_amount.toFixed(2));
                            }
                            $("#alert").addClass("alert alert-success").html("<strong>Raw has been modified!<strong>");
                            $("html, body").animate({
                                scrollTop: 0
                            }, 600);
                        } else {
                            $("#alert").addClass("alert alert-success").html("<strong>" + result.message + "<strong>");
                        }
                        $('#buttonsubmit').prop('disabled', false);
                        $('.loading-overlay').addClass('is-active');
                        $.ajax({
                            url: "<?php echo Yii::app()->createUrl("bills/listAdditionalItems") ?>",
                            data: {
                                'billid': result[0]
                            },
                            type: "POST",
                            success: function(results) {
                                if (results) {
                                    $(".new_items").html(results);
                                }
                            }
                        });

                    }
                });

            })

            function setCookie(cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                var expires = "expires=" + d.toUTCString();
                document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
            }
        });
        $(document).ajaxComplete(function() {
            $('.loading-overlay').removeClass('is-active');
            $('#loading').hide();
        });
    </script>