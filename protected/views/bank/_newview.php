<?php //
/* @var $this BankController */
/* @var $data Bank */
?>
<?php
if ($index == 0) {
    ?>
    <thead>
    <tr> 
        <th>No</th>
        <th>Bank Name</th>
        <th>Company</th>
        <th></th>
    </tr>   
    </thead>
<?php } ?>

    <tr> 
        <td style="width: 50px;"><?php echo $index+1; ?></td>
        <td  class="text_black"><?php echo CHtml::encode($data->bank_name); ?></td>
        <td>
                    <?php
                    $company = Company::model()->findByPk($data->company_id);	
		 echo $company['name'];
                    ?>
                </td> 
         <?php
            if(isset(Yii::app()->user->role) && (in_array('/bank/updatebank', Yii::app()->user->menuauthlist))){
            ?>
                <td  style="width:40px;"><span class="fa fa-edit editbank" data-id="<?php echo  $data->bank_id; ?>"></span></td>
                <?php } ?>
                       
    </tr>