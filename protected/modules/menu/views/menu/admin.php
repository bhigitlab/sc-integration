<?php
/* @var $this MenuController */
/* @var $model Menu */

$this->breadcrumbs=array(
	'Menus'=>array('index'),
	'Manage',
);

$this->menu=array(
	//array('label'=>'List Menu', 'url'=>array('index')),
	//array('label'=>'Create Menu', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('menu-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="container">
<h2>Manage Menus</h2>


<?php echo CHtml::link('Add Menu',array('menu/create'),array('class'=>'btn  btn-success')); ?>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<!--div class="search-form" style="display:none">
<?php //$this->renderPartial('_search',array(
	//'model'=>$model,
//)); ?>
</div--><!-- search-form -->

<?php
/*$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'menu-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
                //array('name'=>'menu_id', 'htmlOptions' => array('width' => '40px','style'=>'font-weight: bold;text-align:center')),
		//'menu_id',
		'menu_name',
                array(
                    'name' => 'parent_menu',
                    'value' => function (Menu $data){
                                    if($data->parent_id)
                                        return $data->parent->menu_name; // "parent" - relation name, defined in "relations" method 
                                    return "";
                                },
                    
                ),
                //'parent.menu_name',

		'controller',
		'action',
                array(
                    'name' => 'status',
                    'value' => function (Menu $data){
                                    if($data->status == 0)
                                        return "Active"; // "parent" - relation name, defined in "relations" method 
                                    return "Inactive";
                                }   
                ),
                        
                array(
                    'name' => 'showmenu',
                    'value' => function (Menu $data){
                                    if($data->showmenu == 0)
                                        return "All Users";
                                    else if($data->showmenu == 1)
                                        return "Guest Users";
                                    else if($data->showmenu == 2)
                                        return "Authenticated Users";
                                    else
                                        return "";
                                }   
                ),
		
		//'params',
		//'showmenu',
		
		array(
			'class'=>'CButtonColumn',
		),
	),
));*/ ?>




<?php
/*
$this->widget('ext.groupgridview.GroupGridView', array(
      'id' => 'grid1',
      'dataProvider' => $dataProvider,
      'mergeColumns' => array('parent_menu','controller'),
      'columns' => array(
            
            array(
                'name' => 'parent_menu',
                'value' => function (Menu $data){
                                if($data->parent_id)
                                    return $data->parent->menu_name; // "parent" - relation name, defined in "relations" method 
                                return "";
                            },

            ),
            'menu_name',
            'controller',
            'action',
            array(
                'name' => 'status',
                'value' => function (Menu $data){
                                if($data->status == 0)
                                    return "Active"; // "parent" - relation name, defined in "relations" method 
                                return "Inactive";
                            }   
            ),
                        
            array(
                'name' => 'showmenu',
                'value' => function (Menu $data){
                                if($data->showmenu == 0)
                                    return "All Users";
                                else if($data->showmenu == 1)
                                    return "Guest Users";
                                else if($data->showmenu == 2)
                                    return "Authenticated Users";
                                else
                                    return "";
                            }   
            ),
        //array('class' => 'CLinkColumn'),
        array('class' => 'CButtonColumn'),        
      ),
    ));*/
?>

<?php
 $this->widget('ext.groupgridview.GroupGridView', array(
        'id' => 'grid1',
        'itemsCssClass' =>'greytable',
        'dataProvider' => $dataProvider,
        'extraRowColumns' => array(
            'parent_menu'

        ),
        'extraRowExpression' => function (Menu $data){
                                if($data->parent_id)
                                    return "<b style=\"font-size: 16px; \">".$data->parent->menu_name."</b>"; // "parent" - relation name, defined in "relations" method 
                                return "<b style=\"font-size: 16px; \">Main Menu Items</b>";
                            },
      'extraRowPos' => 'above',
      'columns' => array(
            array(
                'name' => 'parent_menu',
                'value' => function (Menu $data){
                                if($data->parent_id)
                                    return (isset($data->parent) ? $data->parent->menu_name : ''); // "parent" - relation name, defined in "relations" method 
                                return "";
                            },

            ),
            'menu_name',
            //'controller',
            'action',
            array(
                'name' => 'status',
                'value' => function (Menu $data){
                                if($data->status == 0)
                                    return "Active"; // "parent" - relation name, defined in "relations" method 
                                return "Inactive";
                            }   
            ),
                        
            array(
                'name' => 'showmenu',
                'value' => function (Menu $data){
                                if($data->showmenu == 0)
                                    return "All Users";
                                else if($data->showmenu == 1)
                                    return "Guest Users";
                                else if($data->showmenu == 2)
                                    return "Authenticated Users";
                                else
                                    return "";
                            }   
            ),
            array(
                'name' => 'show_list',
                'value' => function (Menu $data){
                                if($data->show_list == 0)
                                    return "Inactive in list";
                                else if($data->show_list == 1)
                                    return "Active in list";
                                else
                                    return "";
                            }   
            ),
                    
                    
                    
       // array('class' => 'CLinkColumn'),
       // array('class' => 'CButtonColumn', 'template' => '{update} {delete}'),  
         array('class' => 'CButtonColumn', 'template' => '{update} '),             
      ),
    ));

?>

</div>


