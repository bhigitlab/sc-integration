<?php
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
$newQuery1 = "";
foreach ($arrVal as $arr) {
  if ($newQuery)
    $newQuery .= ' OR';
  if ($newQuery1)
    $newQuery1 .= ' OR';
  $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
  $newQuery1 .= " FIND_IN_SET('" . $arr . "', company_id)";
}
?>
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">-->

<script>
  $(function () {
    $("#Reconciliation_fromdate").datepicker({
      dateFormat: 'yy-mm-dd'
    });
    $("#Reconciliation_todate").datepicker({
      dateFormat: 'yy-mm-dd'
    });
    $("#Reconciliation_fromdate").change(function () {
      $("#Reconciliation_todate").datepicker('option', 'minDate', $(this).val());
    });
    $("#Reconciliation_todate").change(function () {
      $("#Reconciliation_fromdate").datepicker('option', 'maxDate', $(this).val());
    });
  });

</script>

<div class="page_filter clearfix">

  <?php $form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
  )); ?>
  <div class="row">
    <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
      <label>From</label>
      <?php echo $form->textField($model, 'fromdate', array('class' => 'date form-control', "readonly" => true, 'placeholder' => 'Date From', 'autocomplete' => 'off')); ?>
    </div>
    <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
      <label>To</label>
      <?php echo $form->textField($model, 'todate', array('class' => 'date form-control', "readonly" => true, 'placeholder' => 'Date To', 'autocomplete' => 'off')); ?>
    </div>
    <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
      <label for="payment">Payment Name</label>
      <?php echo $form->textField($model, 'reconciliation_payment', array('placeholder' => 'Payment Name', 'class' => 'form-control')); ?>
    </div>
    <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
      <label for="bank">Bank</label>
      <?php echo $form->dropDownList($model, 'reconciliation_bank', CHtml::listData(Bank::model()->findAll(array('order' => 'bank_id ASC', 'condition' => '(' . $newQuery1 . ')')), 'bank_id', 'bank_name'), array('empty' => '-Choose a bank-', 'class' => 'form-control')); ?>
    </div>
    <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
      <label for="company">Company</label>
      <?php echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array('order' => 'id DESC', 'condition' => '(' . $newQuery . ')')), 'id', 'name'), array('empty' => '-Choose a company-', 'class' => 'form-control')); ?>
    </div>
    <div class="form-group col-xs-12 col-sm-3 col-md-2 text-sm-left text-right">
      <label class="d-sm-block d-none">&nbsp;</label>
      <div>
        <?php echo CHtml::submitButton('GO', array('class' => 'btn btn-sm btn-primary')); ?>
        <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('reconciliationreport') . '"', 'class' => 'btn btn-sm btn-default')); ?>
      </div>
    </div>
  </div>
  <?php $this->endWidget(); ?>

</div><!-- search-form -->