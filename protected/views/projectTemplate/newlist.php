<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Templates',
);
$page = Yii::app()->request->getParam('Templates_page');
if ($page != '') {
    Yii::app()->user->setReturnUrl($page);
} else {
    Yii::app()->user->setReturnUrl(0);
}
?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container" id="project">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix">
        <div class="add-btn pull-right">
            <?php
            if (isset(Yii::app()->user->role) && (in_array('/templates/createtemplates', Yii::app()->user->menuauthlist))) {
            ?>
                <button class="createtemplate btn btn-primary">Add Template</a>                    
            <?php } ?>
        </div>
        <h2 class="pull-left">Templates</h2>
    </div>
    <div id="errMsg"></div>
    <div id="templateform" style="display:none;"></div>
    <div class="">
        <?php
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_newview', 'template' => '<div class=""><table cellpadding="10" class="table" id="templateab">{items}</table></div>',
            'ajaxUpdate' => false,
        ));
        ?>
        <?php
        Yii::app()->templateScript->registerScript('myjavascript', '
             $(document).ready(function(){
               $("#templateab").dataTable( {
                    "scrollY": "300px",
                    "scrollCollapse": true,
                    "paging": false,
                     "columnDefs"        : [       
                        { 
                            "searchable"    : false, 
                            "targets"       : [0,3] 
                        },
                        { "bSortable": false, "aTargets": [-1]  }
                    ],
                } );
                });

            ');
        ?>

        <script>
            jQuery(function($) {
                $('#template').on('keydown', function(event) {
                    if (event.keyCode == 13) {
                        $("#templatesearch").submit();
                    }
                });
            });

            function closeaction() {
                $('#templateform').slideUp();
            }

            function edittemplate(template_id) {
                $('.loading-overlay').addClass('is-active');
                var id = template_id;
                $.ajax({
                    type: "GET",
                    url: "<?php echo $this->createUrl('templates/update&layout=1&id=') ?>" + id,
                    success: function(response) {
                        $('.loading-overlay').removeClass('is-active');
                        $("#templateform").html(response).slideDown();

                    }
                });

            };
            $(document).ready(function() {
                $('.createtemplate').click(function() {
                    $('.loading-overlay').addClass('is-active');
                    $.ajax({
                        type: "GET",
                        url: "<?php echo $this->createUrl('templates/create&layout=1') ?>",
                        success: function(response) {
                            $("#templateform").html(response).slideDown();
                            var role = "<?php echo Yii::app()->user->role; ?>";
                            if (role == 3)
                                $("#Templates_company_id_all").click();
                        }
                    });
                });
            });
            $(document).ajaxComplete(function() {
                console.log('ss');
                $('.loading-overlay').removeClass('is-active');
                $('#loading').hide();
            });

            function deletTemplate(elem,id){
        
                if (confirm("Do you want to remove this item?")) {
                    $('.loading-overlay').addClass('is-active');
                    $.ajax({
                        url: '<?php echo  Yii::app()->createAbsoluteUrl('templates/removetemplate'); ?>',
                        type: 'POST',
                        dataType:'json',
                        data: {
                            cid: id
                        },
                        success: function(data) {    
                            console.log(data)                ;
                            $("#errMsg").show()
                                .html('<div class="alert alert-'+data.response+'">'+data.msg+'</div>')
                                .fadeOut(10000);
                            setTimeout(function () {                        
                                location.reload(true);
                            }, 3000);
                                
                        }
                    });
                } else {
                    return false;
                }
                
            }
        </script>



    </div>
</div>



<style>
    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    .panel {
        border: 1px solid #ddd;
    }

    .panel-heading {
        background-color: #eee;
        height: 40px;
    }

    input[type="checkbox"] {
        margin: 11px 3px 0;
    }
</style>