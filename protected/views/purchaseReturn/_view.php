<?php
if($index == 0) {
?>
    <thead class="entry-table">
        <tr>
            <?php
                if((isset(Yii::app()->user->role) && ((in_array('/purchaseReturn/view', Yii::app()->user->menuauthlist)) || (in_array('/purchaseReturn/update', Yii::app()->user->menuauthlist))))){
            ?>
            <th></th>
            <?php } ?>
            <th>Sl No</th>
            <th>Purchase Number</th>
            <th>Bill Number</th>
            <th>Return Number</th>
            <th>Return Date</th>
            <th>Created Date</th>
            <th>Amount</th>
            <th>Total Amount</th>
            
        </tr>
    </thead>
<?php } ?>
    
    <?php
if ($index == 0) {
    ?>
    <tfoot class="entry-table">
        <tr>
            <?php
                if((isset(Yii::app()->user->role) && ((in_array('/purchaseReturn/view', Yii::app()->user->menuauthlist)) || (in_array('/purchaseReturn/update', Yii::app()->user->menuauthlist))))){
            ?>
            <th></th>
            <?php } ?>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th class="text-right"><?php echo ($amount !='')?Controller::money_format_inr($amount,2,1):'0.00'; ?></th>
            <th class="text-right"><?php echo ($total_amount !='')?Controller::money_format_inr($total_amount,2,1):'0.00'; ?></th>
            
        </tr>
    </tfoot>
<?php }?>
        <tr>
            <?php
                if((isset(Yii::app()->user->role) && ((in_array('/purchaseReturn/view', Yii::app()->user->menuauthlist)) || (in_array('/purchaseReturn/update', Yii::app()->user->menuauthlist))))){
            ?>
            <td>
                <span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="right" type="button" data-html="true" style="cursor: pointer;" ></span>
                    <div class="popover-content hide">
                        <ul class="tooltip-hiden">
                            <?php
                            if((in_array('/purchaseReturn/view', Yii::app()->user->menuauthlist))){
                            ?>
                            <li><a  href="<?php echo Yii::app()->createAbsoluteUrl('PurchaseReturn/view',array('id' => $data->return_id)); ?>" class="btn btn-xs btn-default">View</a></li>
                            <?php } ?>
                            <?php
                            if((in_array('/purchaseReturn/update', Yii::app()->user->menuauthlist))){
                            ?>
                            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('PurchaseReturn/update',array('id' => $data->return_id)); ?>" class="btn btn-xs btn-default">Edit</a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </td>
            <?php } ?>
            <td><?php echo  $index+1; ?></td>
            <td>
                <?php
                $bills = Bills::model()->findByPk($data->bill_id);
                if($bills['purchase_id'] != NULL) {
                    $purchase = Purchase::model()->findByPk($bills->purchase_id);
                    echo $purchase->purchase_no;
                }
                ?>
            </td>
            <td>
                <?php 
                if($data->bill_id != NULL) {
                    $bills = Bills::model()->findByPk($data->bill_id);
                    echo $bills['bill_number'];
               }
               ?>
            </td>
            <td><?php echo $data->return_number; ?></td>
            <td><?php echo  date("d-m-Y", strtotime($data->return_date)); ?></td>
            <td><?php echo  date("d-m-Y", strtotime($data->created_date)); ?></td>
            <td class="text-right"><?php echo ($data->return_amount !='')?Controller::money_format_inr($data->return_amount,2,1):'0.00';  ?></td>
            <td class="text-right"><?php echo ($data->return_totalamount !='')?Controller::money_format_inr($data->return_totalamount,2,1):'0.00'; ?></td>
            
        </tr>





<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
