<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<?php if (filter_input(INPUT_GET, 'export')) { ?>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
<?php } ?>
<?php
$style = "";
if (filter_input(INPUT_GET, 'export')) {
    $style = "style='margin:0px 30px'";
} ?>
<div class="container" id="project" <?php echo $style ?>>
    <div class="expenses-heading header-container">
        <h3>Cash Balance Report</h3>
        <div class="btn-container">
            <?php if (!filter_input(INPUT_GET, 'export')) { ?>
                <button type="button" class="btn btn-info">
                    <a href="<?php echo $this->createAbsoluteUrl('cashbalance/cashbalance_report', array('date_to' => $date_to, 'date_from' => $date_from, 'ledger' => $_GET['ledger'], 'export' => 'pdf')); ?>"
                        style="text-decoration: none; color: white;" title="SAVE AS PDF">
                        <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
                    </a>
                </button>
                <button type="button" class="btn btn-info">
                    <a href="<?php echo $this->createAbsoluteUrl('cashbalance/cashbalance_report', array('date_to' => $date_to, 'date_from' => $date_from, 'ledger' => $_GET['ledger'], 'export' => 'csv')); ?>"
                        style="text-decoration: none; color: white;" title="SAVE AS EXCEL">
                        <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>
                    </a>
                </button>

                <?php if (isset($_GET['ledger'])) { ?>
                    <?php if ($_GET['ledger'] == 1) { ?>
                        <button type="button" class="btn btn-info">
                            <a href="<?php echo $this->createAbsoluteUrl('cashbalance/cashbalance_report', array('date_to' => $date_to, 'date_from' => $date_from, 'ledger' => 0)); ?>"
                                style="text-decoration: none; color: white;">
                                List View
                            </a>
                        </button>
                    <?php } else { ?>
                        <button type="button" class="btn btn-info">
                            <a href="<?php echo $this->createAbsoluteUrl('cashbalance/cashbalance_report', array('date_to' => $date_to, 'date_from' => $date_from, 'ledger' => 1)); ?>"
                                style="text-decoration: none; color: white;">
                                Ledger View
                            </a>
                        </button>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <div class="page_filter clearfix">
        <?php
        $tblpx = Yii::app()->db->tablePrefix;
        if (!isset($_REQUEST['date_from']) || $_REQUEST['date_from'] == '') {
            $date_from = '';
        } else {
            $date_from = date('Y-m-d', strtotime($_REQUEST['date_from']));
        }
        if (!isset($_REQUEST['date_to']) || $_REQUEST['date_to'] == '') {
            $date_to = '';
        } else {
            $date_to = date('Y-m-d', strtotime($_REQUEST['date_to']));
        }
        $current_date = date("Y-m-d");
        ?>
        <?php if (!filter_input(INPUT_GET, 'export')) { ?>
            <?php $form = $this->beginWidget('CActiveForm', array(
                'action' => Yii::app()->createUrl($this->route),
                'method' => 'get',
                'id' => "cashreportform"
            )); ?>
            <div class="search-form">
                <div class="row">
                    <div class=" form-group col-xs-12 col-sm-3 col-md-2">
                        <!-- <div class="display-flex"> -->
                        <label>From </label>
                        <?php echo CHtml::textField('date_from', $date_from, array('placeholder' => 'Date From', "id" => "date_from", "class" => "form-control", 'style' => 'display:inline-block', 'autoComplete' => 'off')); ?>
                        <!-- </div> -->
                    </div>
                    <div class=" form-group col-xs-12 col-sm-3 col-md-2">
                        <!-- <div class="display-flex"> -->
                        <label>To </label>
                        <?php echo CHtml::textField('date_to', $date_to, array('placeholder' => 'Date To', "class" => "form-control", 'style' => 'display:inline-block', "id" => "date_to", 'autoComplete' => 'off')); ?>
                        <!-- </div> -->
                    </div>
                    <input type="hidden" name="ledger" value="<?php echo isset($_GET['ledger']) ? $_GET['ledger'] : 0; ?>">
                    <div class="form-group col-xs-12 col-sm-3 col-md-2 text-sm-left text-right">
                        <label>&nbsp;</label>
                        <div>
                            <?php
                            echo CHtml::submitButton('GO', array(
                                'id' => 'btSubmit',
                                'class' => 'btn btn-sm btn-primary'
                            ));
                            ?>
                            <?php
                            echo CHtml::resetButton('Clear', array(
                                'onclick' => 'javascript:location.href="' . $this->createUrl('cashbalance/cashbalance_report&ledger=0') . '"',
                                'class' => 'btn btn-sm btn-default'
                            ));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        <?php } ?>
    </div>
    <div id="contenthtml" class="contentdiv">
        <?php
        $heading = '';
        if (empty($date_from) && empty($date_to)) {
            $heading .= date('d-m-Y');
        } else {
            if (!empty($date_from) && !empty($date_to)) {
                $heading .= date('d-M-Y', strtotime($date_from)) . ' to ' . date('d-M-Y', strtotime($date_to));
            } else {
                if (!empty($_REQUEST['date_from'])) {
                    $heading .= date('d-M-Y', strtotime($date_from));
                }
                if (!empty($_REQUEST['date_to'])) {
                    $heading .= date('d-M-Y', strtotime($date_to));
                }
            }
        }
        $ledger_heading = '';
        if ($_GET['ledger'] == 1) {
            $ledger_heading = ' - Ledger View';
        }
        ?>
        <h4>Cash In-Hand Report :
            <?php echo $heading . $ledger_heading; ?>
        </h4>
        <div id="parent">

            <?php
            $where = '';

            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
            }

            $sql = "SELECT * FROM `jp_cashbalance` "
                . " WHERE bank_id IS NULL AND (" . $newQuery . ") ";
            $cash_blance = Yii::app()->db->createCommand($sql)->queryRow();

            if (empty($date_from) && empty($date_to)) {
                $where .= " AND (`date` >= '" . $cash_blance['cashbalance_date'] . "' and `date`<='" . date("Y-m-d") . "') ";
            } else {
                if (!empty($date_from) && !empty($date_to)) {
                    $where .= " AND (`date` >= '" . $date_from . "' and `date`<='" . $date_to . "') ";
                } else {
                    if (!empty($date_from)) {
                        $where .= " AND `date` >= '" . $date_from . "' ";
                    }
                    if (!empty($date_to)) {
                        $where .= $where .= " AND `date`<='" . $date_to . "' ";
                    }
                }
            }


            $data_array = array();
            $sql = "SELECT * FROM {$tblpx}expenses WHERE (" . $newQuery . ") " . $where . " AND (expense_type=89 OR payment_type=89) AND subcontractor_id IS NULL";  // OR expense_type=103              
            $daybook_data = Yii::app()->db->createCommand($sql)->queryAll();

            foreach ($daybook_data as $key => $value) {
                $data_array[$key]['payment_type'] = 'Daybook';
                //$data_array[$key]['expense_amount'] = $value['paid'];
            
                $data_array[$key]['expense_amount'] = $value['paid'] - $value['expense_tds'];
                $data_array[$key]['receipt_amount'] = $value['receipt'];
                $data_array[$key]['payment_date'] = $value['date'];
                if ($value['type'] == 73) {
                    $vendor = Vendors::model()->findByPk($value['vendor_id']);
                    $vendor_name = $vendor['name'];
                    if ($value['bill_id'] != NULL) {
                        $bills = Bills::model()->findByPk($value['bill_id']);
                        $expense_head = ExpenseType::model()->findByPk($value['exptype']);
                        $bill_thead = $bills['bill_number'] . ' / ' . $expense_head['type_name'];
                    } else {
                        $expense_head = ExpenseType::model()->findByPk($value['exptype']);
                        $bill_thead = $expense_head['type_name'];
                    }
                } else {
                    $vendor_name = '';
                    $bill_thead = '';
                }
                $data_array[$key]['vendor'] = $vendor_name;
                $data_array[$key]['description'] = $value['description'];
                $data_array[$key]['bill_exphead'] = $bill_thead;
                $data_array[$key]['company_id'] = $value['company_id'];
            }

            // dailyexpense
            
            $where_con = "";

            if (empty($date_from) && empty($date_to)) {
                $where_con .= " `date` >= '" . $cash_blance['cashbalance_date'] . "' and `date`<='" . date("Y-m-d") . "' AND ";
            } else {
                if (!empty($date_from) && !empty($date_to)) {
                    $where_con .= " `date` >= '" . $date_from . "' and `date`<='" . $date_to . "'AND ";
                } else {
                    if (!empty($date_from)) {
                        $where_con .= " `date` >= '" . $date_from . "' AND ";
                    }
                    if (!empty($date_to)) {
                        $where_con .= " `date`<='" . $date_to . "' AND ";
                    }
                }
            }

            // receipt
            $sql = "SELECT * FROM {$tblpx}dailyexpense "
                . " WHERE $where_con (dailyexpense_receipt_type=89 AND "
                . " dailyexpense_chequeno IS NULL "
                . " AND reconciliation_status IS NULL "
                . " AND company_id=" . $cash_blance['company_id'] . ")  "
                . " OR (dailyexpense_receipt_type=88 "
                . " AND expensehead_type=4  AND "
                . " company_id=" . $cash_blance['company_id'] . ") AND "
                . " parent_status='1' AND "
                . " exp_type=72 ";
            if (isset($cash_blance['company_id'])) {
                $dailyexpense_data = Yii::app()->db->createCommand($sql)->queryAll();
            } else {
                $dailyexpense_data = array();
            }

            $a = count($daybook_data);
            foreach ($dailyexpense_data as $key => $value) {
                $data_array[$a]['payment_type'] = 'Dailyexpense';
                $data_array[$a]['expense_amount'] = $value['dailyexpense_paidamount'];
                $data_array[$a]['receipt_amount'] = $value['dailyexpense_receipt'];
                $data_array[$a]['payment_date'] = $value['date'];
                if ($value["dailyexpense_type"] == "deposit") {
                    $depositId = $value["expensehead_id"];
                    $deposit = Deposit::model()->findByPk($depositId);
                    $expense_head = $deposit->deposit_name;
                } else if ($value["dailyexpense_type"] == "expense") {
                    $expense = Companyexpensetype::model()->findByPk($value['expensehead_id']);
                    $expense_head = isset($expense->name) ? $expense->name : "";
                } else {
                    $receipt = Companyexpensetype::model()->findByPk($value['exp_type_id']);
                    $expense_head = isset($receipt->name) ? $receipt->name : "";
                }
                if ($value['bill_id'] != NULL) {
                    $bill_thead = $value['bill_id'] . ' / ' . $expense_head;
                } else {
                    $bill_thead = $expense_head;
                }

                $data_array[$a]['vendor'] = '';
                $data_array[$a]['description'] = $value['description'];
                $data_array[$a]['bill_exphead'] = $bill_thead;
                $data_array[$a]['company_id'] = $value['company_id'];
                $a++;
            }

            //expense
            $sql = "SELECT * FROM {$tblpx}dailyexpense "
                . " WHERE $where_con (expense_type=89) AND exp_type=73 "
                . " AND parent_status='1' "
                . " AND dailyexpense_chequeno IS NULL "
                . " AND reconciliation_status IS NULL "
                . " AND company_id=" . $cash_blance['company_id'];
            if (isset($cash_blance['company_id'])) {
                $dailyexpense_data1 = Yii::app()->db->createCommand($sql)->queryAll();
            } else {
                $dailyexpense_data1 = array();
            }
            $a1 = count($daybook_data) + count($dailyexpense_data);

            foreach ($dailyexpense_data1 as $key => $value) {
                $data_array[$a1]['payment_type'] = 'Dailyexpense';
                $data_array[$a1]['expense_amount'] = $value['dailyexpense_paidamount'];
                $data_array[$a1]['receipt_amount'] = $value['dailyexpense_receipt'];
                $data_array[$a1]['payment_date'] = $value['date'];
                if ($value["dailyexpense_type"] == "deposit") {
                    $depositId = $value["expensehead_id"];
                    $deposit = Deposit::model()->findByPk($depositId);
                    $expense_head = $deposit->deposit_name;
                } else if ($value["dailyexpense_type"] == "expense") {
                    $expense = Companyexpensetype::model()->findByPk($value['expensehead_id']);
                    $expense_head = isset($expense->name) ? $expense->name : "";
                } else {
                    $receipt = Companyexpensetype::model()->findByPk($value['exp_type_id']);
                    $expense_head = isset($receipt->name) ? $receipt->name : "";
                }

                if ($value['bill_id'] != NULL) {
                    $bill_thead = $value['bill_id'] . ' / ' . $expense_head;
                } else {
                    $bill_thead = $expense_head;
                }

                $data_array[$a1]['vendor'] = '';
                $data_array[$a1]['description'] = $value['description'];
                $data_array[$a1]['bill_exphead'] = $bill_thead;
                $data_array[$a1]['company_id'] = $value['company_id'];
                $a1++;
            }


            // Vendor payment
            
            $vendorpayment_data = array();
            $vendorpayment_data = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyvendors WHERE (" . $newQuery . ") " . $where . " AND payment_type=89")->queryAll();
            $b = count($daybook_data) + count($dailyexpense_data) + count($dailyexpense_data1);
            foreach ($vendorpayment_data as $key => $value) {
                $data_array[$b]['payment_type'] = 'Vendor  Payment';
                $data_array[$b]['expense_amount'] = ($value['amount'] + $value['tax_amount']) - $value['tds_amount'];
                $data_array[$b]['receipt_amount'] = '';
                $data_array[$b]['payment_date'] = $value['date'];
                $vendor = Vendors::model()->findByPk($value['vendor_id']);
                $vendor_name = isset($vendor->name) ? $vendor->name : '';
                $data_array[$b]['vendor'] = $vendor_name;
                $data_array[$b]['description'] = $value['description'];
                $data_array[$b]['bill_exphead'] = '';
                $data_array[$b]['company_id'] = $value['company_id'];
                $b++;
            }

            // Subcontractor payment
            $subcontractorpayment_data = array();
            $subcontractor_paysql = "SELECT * FROM {$tblpx}subcontractor_payment WHERE (" . $newQuery . ") " . $where . " AND payment_type=89 AND approve_status ='Yes'";
            $subcontractorpayment_data = Yii::app()->db->createCommand($subcontractor_paysql)->queryAll();
            $c = count($daybook_data) + count($dailyexpense_data) + count($dailyexpense_data1) + count($vendorpayment_data);
            //die ($subcontractor_paysql);
            foreach ($subcontractorpayment_data as $key => $value) {
                $data_array[$c]['payment_type'] = 'Subcontractor  Payment';
                $data_array[$c]['expense_amount'] = ($value['amount'] + $value['tax_amount']) - $value['tds_amount'];
                // $data_array[$c]['expense_amount']=$value['paidamount'];
                $data_array[$c]['receipt_amount'] = '';
                $data_array[$c]['payment_date'] = $value['date'];
                $data_array[$c]['vendor'] = '';
                $data_array[$c]['description'] = $value['description'];
                $data_array[$c]['bill_exphead'] = '';
                $data_array[$c]['company_id'] = $value['company_id'];
                $c++;
            }
            $buyer_module = GeneralSettings::model()->checkBuyerModule();
            if ($buyer_module) {
                $buyer_transactions = BuyerTransactions::model()->findAll(array('condition' => '(' . $newQuery . ') ' . $where));
                $buyer_transactions_cash = BuyerTransactions::model()->findAll(array('condition' => '(' . $newQuery . ') ' . $where . ' AND transaction_type = 89'));
                $buyer_transactions_bank = BuyerTransactions::model()->findAll(array('condition' => '(' . $newQuery . ') ' . $where . ' AND transaction_type = 88'));
                foreach ($buyer_transactions as $key => $value) {
                    $data_array[$c]['payment_type'] = 'Buyer Transactions';
                    if ($value['transaction_for'] == 1) {
                        $data_array[$c]['receipt_amount'] = $value['total_amount'];
                        $data_array[$c]['expense_amount'] = 0;
                    } elseif ($value['transaction_for'] == 2) {
                        $data_array[$c]['receipt_amount'] = $value['total_amount'];
                        $data_array[$c]['expense_amount'] = 0;
                    } elseif ($value['transaction_for'] == 3) {
                        $data_array[$c]['expense_amount'] = $value['total_amount'];
                        $data_array[$c]['receipt_amount'] = 0;
                    } else {
                        $data_array[$c]['expense_amount'] = 0;
                        $data_array[$c]['receipt_amount'] = 0;
                    }
                    $data_array[$c]['payment_date'] = $value['date'];
                    $data_array[$c]['vendor'] = '';
                    $data_array[$c]['description'] = $value['description'];
                    $data_array[$c]['bill_exphead'] = $value['transaction_no'];
                    $data_array[$c]['company_id'] = $value['company_id'];
                    $c++;
                }
            }

            $total_expense = 0;
            $total_recept = 0;
            if (!empty($data_array)) {
                $total_expense = array_sum(array_column($data_array, 'expense_amount'));
                $total_recept = array_sum(array_column($data_array, 'receipt_amount'));
            }
            $date = array_column($data_array, 'payment_date');
            array_multisort($date, SORT_DESC, $data_array);
            ?>
            <?php
            if ($_GET['ledger'] == 0) {
                ?>
                <table class="table total-table " id="fixTable">
                    <thead class="entry-table">
                        <tr>
                            <th>Sl No.</th>
                            <th>Payment Name</th>
                            <th>Company</th>
                            <th>Bill No. / Transaction head</th>
                            <th>Vendors</th>
                            <th>Description</th>
                            <th>Payment date</th>
                            <th>Expense Amount</th>
                            <th>Receipt Amounts</th>
                        </tr>
                        <tr>
                            <th colspan="7" class="text-right"><b>Total</b></th>
                            <th style="text-align: right;">
                                <?php echo Controller::money_format_inr($total_expense, 2); ?>
                            </th>
                            <th style="text-align: right;">
                                <?php echo Controller::money_format_inr($total_recept, 2); ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($data_array)) {
                            foreach ($data_array as $key => $data) {
                                $total_expense += $data['expense_amount'];
                                $total_recept += floatval($data['receipt_amount']);
                                ?>
                                <tr>
                                    <td>
                                        <?php echo $key + 1 ?>
                                    </td>
                                    <td>
                                        <?php echo $data['payment_type']; ?>
                                    </td>
                                    <td>
                                        <?php
                                        $company = Company::model()->findByPk($data['company_id']);
                                        echo $company->name;
                                        ?>
                                    </td>
                                    <td>
                                        <?php echo $data['bill_exphead']; ?>
                                    </td>
                                    <td>
                                        <?php echo $data['vendor']; ?>
                                    </td>
                                    <td>
                                        <?php echo $data['description']; ?>
                                    </td>
                                    <td>
                                        <?php echo date('d-m-Y', strtotime($data['payment_date'])); ?>
                                    </td>
                                    <td align="right">
                                        <?php echo ($data['expense_amount'] != 0) ? Controller::money_format_inr($data['expense_amount'], 2) : '0'; ?>
                                    </td>
                                    <td align="right">
                                        <?php echo ($data['receipt_amount'] != 0) ? Controller::money_format_inr($data['receipt_amount'], 2) : '0'; ?>
                                    </td>
                                </tr>

                            <?php } ?>
                        </tbody>
                    <?php } else { ?>
                        <tr>
                            <td colspan="9" class="text-center">No data found</td>
                        </tr>
                    <?php }
                        ?>
                </table>
                <div class="summarytext">
                    Total
                    <?php echo count($data_array); ?> results
                </div>
            <?php } else { ?>
                <div>
                    <?php
                    $this->renderPartial(
                        '_cashbalance_ledger_view',
                        array(
                            'date_to' => $date_to,
                            'date_from' => $date_from,
                            'data_array' => $data_array
                        )
                    );
                    ?>
                </div>
            <?php } ?>
        </div>
        <?php
        if ($date_from != '' || $date_to != '') {
            $newQuery = "";
            $newQuery2 = "";
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}cashbalance.company_id)";
            }
            $heading = '';
            if (!empty($date_from) && !empty($date_to)) {
                $heading .= date('d-M-Y', strtotime($date_from)) . ' to ' . date('d-M-Y', strtotime($date_to));
            } else {
                if (!empty($_REQUEST['date_from'])) {
                    $heading .= date('d-M-Y', strtotime($date_from));
                }
                if (!empty($_REQUEST['date_to'])) {
                    $heading .= date('d-M-Y', strtotime($date_to));
                }
            }

            $sql = "SELECT {$tblpx}status.caption,{$tblpx}cashbalance.cashbalance_type,{$tblpx}cashbalance.bank_id,{$tblpx}bank.bank_name,{$tblpx}cashbalance.cashbalance_opening_balance,{$tblpx}cashbalance.cashbalance_deposit,{$tblpx}cashbalance.cashbalance_withdrawal,{$tblpx}cashbalance.cashbalance_closing_balance,{$tblpx}cashbalance.cashbalance_date,{$tblpx}cashbalance.company_id FROM {$tblpx}cashbalance LEFT JOIN {$tblpx}bank ON {$tblpx}cashbalance.bank_id={$tblpx}bank.bank_id LEFT JOIN {$tblpx}status ON {$tblpx}cashbalance.cashbalance_type={$tblpx}status.sid WHERE (" . $newQuery . ") ORDER BY bank_id ASC";
            $cash_blance = Yii::app()->db->createCommand($sql)->queryAll();
            if (!empty($cash_blance)) {
                ?>
                <?php
                if ($_GET['ledger'] == 0) {
                    ?>
                    <h4>Balance Report :
                        <?php echo $heading; ?>
                    </h4>
                    <div class="clearfix">
                        <div class="pull-right"><span>Total</span> <?php echo count($cash_blance); ?> <span>results</span></div>
                    </div>
                    <div id="parent2">
                        <table class="table table-bordered " id="fixTable2">
                            <thead>
                                <tr>
                                    <th>Sl No.</th>
                                    <th>Company</th>
                                    <th>Cash Balance Type</th>
                                    <th>Bank</th>
                                    <th>Start Date</th>
                                    <th>Opening Balance</th>
                                    <th>Receipt</th>
                                    <th>Expense</th>
                                    <th>Closing Balance</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($cash_blance as $key => $value) {

                                    $tblpx = Yii::app()->db->tablePrefix;
                                    $where_bank = '';
                                    $where_hand = '';

                                    $opening_hand = '';
                                    $opening_bank = '';
                                    if (!empty($date_from) && !empty($date_to)) {
                                        $opening_date = date('Y-m-d', strtotime('-1 day', strtotime($date_from)));

                                        $where_bank .= " AND reconciliation_date between '" . $date_from . "' and '" . $date_to . "'";
                                        $where_hand .= " `date` between '" . $date_from . "' and '" . $date_to . "' AND ";
                                        $opening_hand .= " AND date <= '" . $opening_date . "'";
                                        $opening_bank .= " AND reconciliation_date <= '" . $opening_date . "'";
                                    } else {
                                        if (!empty($_REQUEST['date_from'])) {
                                            $opening_date = date('Y-m-d', strtotime('-1 day', strtotime($_REQUEST['date_from'])));
                                            $date_from = date('Y-m-d', strtotime($_REQUEST['date_from']));

                                            $where_bank .= " AND reconciliation_date between '" . $date_from . "' and '" . $current_date . "'";
                                            $where_hand .= " `date` between '" . $date_from . "' and '" . $current_date . "' AND ";
                                            $opening_hand .= " AND date <= '" . $opening_date . "'";
                                            $opening_bank .= " AND reconciliation_date <= '" . $opening_date . "'";
                                        }
                                        if (!empty($_REQUEST['date_to'])) {
                                            $opening_date = $value['cashbalance_date'];
                                            $date_to = date('Y-m-d', strtotime($_REQUEST['date_to']));

                                            $where_bank .= " AND reconciliation_date between '" . $value['cashbalance_date'] . "' and '" . $date_to . "'";
                                            $where_hand .= " `date` between '" . $value['cashbalance_date'] . "' and '" . $date_to . "' AND ";
                                            $opening_hand .= " AND date <= '" . $opening_date . "'";
                                            $opening_bank .= " AND reconciliation_date <= '" . $opening_date . "'";
                                        }
                                    }



                                    if ($value['bank_id'] != '') {
                                        // deposit
                                        $reconcil = "reconciliation_status = 1 " . $where_bank;
                                        $unreconcil = "reconciliation_status = 0  "
                                            . " AND reconciliation_date IS NULL";

                                        $sql = "SELECT SUM(receipt) as total_amount "
                                            . " FROM {$tblpx}expenses "
                                            . " WHERE " . $reconcil . " AND payment_type=88 "
                                            . " AND bank_id=" . $value['bank_id'] . " AND type=72 "
                                            . " AND company_id=" . $value['company_id'];


                                        $daybook = Yii::app()->db->createCommand($sql)->queryRow();

                                        $sql = "SELECT SUM(dailyexpense_receipt) as total_amount "
                                            . " FROM {$tblpx}dailyexpense "
                                            . " WHERE (" . $reconcil
                                            . " AND dailyexpense_receipt_type=88 "
                                            . " AND bank_id=" . $value['bank_id'] . ")"
                                            . " OR (" . $unreconcil
                                            . "  AND dailyexpense_receipt_type=89 "
                                            . " AND bank_id=" . $value['bank_id'] . ""
                                            . " AND expensehead_type=5) AND  parent_status='1' "
                                            . " AND exp_type=72"
                                            . " AND company_id=" . $value['company_id'];


                                        $dailyexpense = Yii::app()->db->createCommand($sql)->queryRow();

                                        $daybook_check = $daybook['total_amount'];
                                        $dailyexpense_check = $dailyexpense['total_amount'];
                                        $deposit = $daybook_check + $dailyexpense_check;


                                        //withdrawal
                    
                                        $sql = "SELECT SUM((IFNULL(paid, 0))  - IFNULL(expense_tds, 0)) as total_amount "
                                            . " FROM {$tblpx}expenses "
                                            . " WHERE " . $reconcil . " AND expense_type=88 "
                                            . " AND bank_id=" . $value['bank_id'] . " AND type=73  "
                                            . " AND company_id=" . $value['company_id'];

                                        $daybook_with = Yii::app()->db->createCommand($sql)->queryRow();

                                        $sql = "SELECT SUM(dailyexpense_paidamount) as total_amount "
                                            . " FROM {$tblpx}dailyexpense "
                                            . " WHERE company_id=" . $value['company_id']
                                            . " AND parent_status='1' AND exp_type=73 AND "
                                            . " (" . $reconcil
                                            . " AND (expense_type=88 ) AND bank_id=" . $value['bank_id'] . ") "
                                            . " AND dailyexpense_chequeno IS NOT NULL "
                                        ;


                                        $dailyexpense_with = Yii::app()->db->createCommand($sql)->queryRow();

                                        $daybook_with_amount = $daybook_with['total_amount'];
                                        $dailyexpense_with_amount = $dailyexpense_with['total_amount'];

                                        $sql = "SELECT SUM( (IFNULL(amount, 0) + IFNULL(tax_amount, 0))- IFNULL(tds_amount,0) ) "
                                            . " as total_amount FROM {$tblpx}dailyvendors "
                                            . " WHERE " . $reconcil . " AND payment_type=88  AND "
                                            . " bank=" . $value['bank_id']
                                            . " AND company_id=" . $value['company_id'];
                                        $dailyvendors = Yii::app()->db->createCommand($sql)->queryRow();

                                        $sql = "SELECT SUM( (IFNULL(amount, 0) + IFNULL(tax_amount, 0))- IFNULL(tds_amount,0)) "
                                            . " as total_amount FROM {$tblpx}subcontractor_payment "
                                            . " WHERE " . $reconcil . " AND payment_type=88  "
                                            . " AND bank=" . $value['bank_id']
                                            . " AND company_id=" . $value['company_id'];
                                        $subcontractor = Yii::app()->db->createCommand($sql)->queryRow();

                                        $dailyvendors_check = $dailyvendors['total_amount'];
                                        $subcontractor_check = $subcontractor['total_amount'];

                                        $withdrawal = $dailyvendors_check + $daybook_with_amount + $dailyexpense_with_amount;


                                        // opening balance calculation
                                        // deposit
                                        $sql = "SELECT SUM(receipt) as total_amount "
                                            . " FROM {$tblpx}expenses "
                                            . " WHERE reconciliation_status = 1 " . $opening_bank
                                            . " AND payment_type=88 AND bank_id=" . $value['bank_id']
                                            . " AND type=72 AND company_id=" . $value['company_id'];
                                        $daybook_opening = Yii::app()->db->createCommand($sql)->queryRow();

                                        $sql = "SELECT SUM(dailyexpense_receipt) as total_amount "
                                            . " FROM {$tblpx}dailyexpense "
                                            . " WHERE reconciliation_status = 1 " . $opening_bank
                                            . " AND dailyexpense_receipt_type=88 "
                                            . " AND bank_id=" . $value['bank_id']
                                            . " AND exp_type=72 AND company_id=" . $value['company_id'];
                                        $dailyexpense_opening = Yii::app()->db->createCommand($sql)->queryRow();

                                        $daybook_check_opening = $daybook_opening['total_amount'];
                                        $dailyexpense_check_opening = $dailyexpense_opening['total_amount'];
                                        $deposit_opening = $daybook_check_opening + $dailyexpense_check_opening;


                                        //withdrawal
                                        $sql = "SELECT SUM((IFNULL(paid, 0))  - IFNULL(expense_tds, 0)) as total_amount "
                                            . " FROM {$tblpx}expenses "
                                            . " WHERE reconciliation_status = 1 " . $opening_bank
                                            . " AND expense_type=88 AND bank_id=" . $value['bank_id']
                                            . " AND type=73 AND company_id=" . $value['company_id'];
                                        $daybook_with_opening = Yii::app()->db->createCommand($sql)->queryRow();

                                        $sql = "SELECT SUM(dailyexpense_paidamount) as total_amount "
                                            . " FROM {$tblpx}dailyexpense "
                                            . " WHERE reconciliation_status = 1 " . $opening_bank
                                            . " AND expense_type=88 AND bank_id=" . $value['bank_id']
                                            . " AND exp_type=73 AND company_id=" . $value['company_id'];
                                        $dailyexpense_with_opening = Yii::app()->db->createCommand($sql)->queryRow();

                                        $daybook_with_amount_opening = $daybook_with_opening['total_amount'];
                                        $dailyexpense_with_amount_opening = $dailyexpense_with_opening['total_amount'];

                                        $sql = "SELECT SUM( (IFNULL(amount, 0) + IFNULL(tax_amount, 0))- IFNULL(tds_amount,0) ) "
                                            . " as total_amount FROM {$tblpx}dailyvendors "
                                            . " WHERE reconciliation_status = 1 " . $opening_bank
                                            . " AND payment_type=88  AND bank=" . $value['bank_id']
                                            . " AND company_id=" . $value['company_id'];
                                        $dailyvendors_opening = Yii::app()->db->createCommand($sql)->queryRow();

                                        $dailyvendors_check_opening = $dailyvendors_opening['total_amount'];

                                        $withdrawal_opening = $dailyvendors_check_opening + $daybook_with_amount_opening + $dailyexpense_with_amount_opening;

                                        // $opening_balance = ($deposit_opening + $value['cashbalance_opening_balance']) - $withdrawal_opening;
                                        $opening_date = date('Y-m-d', strtotime('-1 day', strtotime($current_date)));
                                        $opening_bank = " AND reconciliation_date <= '" . $opening_date . "'";
                                        //deposit opening               
                                        $bopendeposit = CashBalanceHelper::depositOpening($opening_bank, $value['bank_id'], $value['company_id']);
                                        $deposit_opening = $bopendeposit['daybook_check_opening'] + $bopendeposit['dailyexpense_check_opening'];

                                        //withdraw opening
                                        $bopenwithdraw = CashBalanceHelper::withdrawOpening($opening_bank, $value['bank_id'], $value['company_id']);
                                        $withdrawal_opening = $bopenwithdraw['dailyvendors_check_opening'] + $bopenwithdraw['daybook_with_amount_opening'] +
                                            $bopenwithdraw['dailyexpense_with_amount_opening'];

                                        // $opening_balance = ($deposit_opening + floatval($value['cashbalance_opening_balance'])) - $withdrawal_opening;
                                        $opening_balance = floatval($value['cashbalance_opening_balance']);
                                    } else {

                                        //deposit
                                        $sql = "SELECT SUM(receipt) as total_amount "
                                            . " FROM {$tblpx}expenses WHERE $where_hand payment_type=89 AND "
                                            . " type=72 AND company_id=" . $value['company_id'];
                                        $daybook_cah = Yii::app()->db->createCommand($sql)->queryRow();
                                        $daybook_cash = $daybook_cah['total_amount'];

                                        $sql = "SELECT SUM(dailyexpense_receipt) as total_amount "
                                            . " FROM {$tblpx}dailyexpense "
                                            . " WHERE  $where_hand (dailyexpense_receipt_type=89 AND "
                                            . " dailyexpense_chequeno IS NULL "
                                            . " AND reconciliation_status IS NULL "
                                            . " AND company_id=" . $value['company_id'] . ")  "
                                            . " OR (dailyexpense_receipt_type=88 "
                                            . " AND expensehead_type=4  AND "
                                            . " company_id=" . $value['company_id'] . ") AND "
                                            . " parent_status='1' AND "
                                            . " exp_type=72 ";
                                        $dailyexpense_cah = Yii::app()->db->createCommand($sql)->queryRow();
                                        $dailyexpense_cash = $dailyexpense_cah['total_amount'];

                                        $deposit = $daybook_cash + $dailyexpense_cash;
                                        // withdrawal
                    
                                        $sql = "SELECT SUM( (IFNULL(paid, 0))  - IFNULL(expense_tds, 0)) as total_amount "
                                            . " FROM {$tblpx}expenses "
                                            . " WHERE $where_hand (expense_type=89) "
                                            . " AND type=73 "
                                            . " AND company_id=" . $value['company_id'];
                                        $daybook_with = Yii::app()->db->createCommand($sql)->queryRow();
                                        $daybook_with_amount = $daybook_with['total_amount'];

                                        $sql = "SELECT SUM(dailyexpense_paidamount) "
                                            . " as total_amount "
                                            . " FROM {$tblpx}dailyexpense "
                                            . " WHERE $where_hand (expense_type=89) AND exp_type=73 "
                                            . " AND parent_status='1' "
                                            . " AND dailyexpense_chequeno IS NULL "
                                            . " AND reconciliation_status IS NULL "
                                            . " AND company_id=" . $value['company_id'];
                                        $dailyexpense_with = Yii::app()->db->createCommand($sql)->queryRow();
                                        $dailyexpense_with_amount = $dailyexpense_with['total_amount'];

                                        $sql = "SELECT SUM( (IFNULL(amount, 0) + IFNULL(tax_amount, 0))-IFNULL(tds_amount, 0) ) "
                                            . " as total_amount FROM {$tblpx}dailyvendors"
                                            . " WHERE  $where_hand payment_type=89 "
                                            . " AND company_id=" . $value['company_id'];

                                        $dailyvendors_cah = Yii::app()->db->createCommand($sql)->queryRow();

                                        $dailyvendors_cash = $dailyvendors_cah['total_amount'];

                                        $sql = "SELECT SUM( IFNULL(paidamount, 0) - IFNULL(tds_amount, 0) ) "
                                            . " as total_amount FROM {$tblpx}subcontractor_payment "
                                            . " WHERE $where_hand payment_type=89 "
                                            . " AND company_id=" . $value['company_id'];

                                        $subcontractor_cah = Yii::app()->db->createCommand($sql)->queryRow();
                                        $subcontractor_cash = $subcontractor_cah['total_amount'];

                                        $withdrawal = $dailyvendors_cash + $daybook_with_amount + $dailyexpense_with_amount;
                                        $buyer_module = GeneralSettings::model()->checkBuyerModule();
                                        if ($buyer_module) {
                                            foreach ($buyer_transactions_cash as $buyer_transaction) {
                                                if ($buyer_transaction['transaction_for'] == 3) {
                                                    $withdrawal += $buyer_transaction['total_amount'];
                                                } elseif ($buyer_transaction['transaction_for'] == 1) {
                                                    $deposit += $buyer_transaction['total_amount'];
                                                }
                                            }
                                            foreach ($buyer_transactions_bank as $buyer_transaction) {
                                                if ($buyer_transaction['transaction_for'] == 3) {
                                                    $withdrawal += $buyer_transaction['total_amount'];
                                                } elseif ($buyer_transaction['transaction_for'] == 1) {
                                                    $deposit += $buyer_transaction['total_amount'];
                                                }
                                            }
                                        }




                                        // opening balance calculation
                                        // deposit
                                        $sql = "SELECT SUM(receipt) as total_amount "
                                            . " FROM {$tblpx}expenses "
                                            . " WHERE payment_type=89 AND type=72 " . $opening_hand
                                            . " AND company_id=" . $value['company_id'];
                                        $daybook_cah_opening = Yii::app()->db->createCommand($sql)->queryRow();
                                        $daybook_cash_opening = $daybook_cah_opening['total_amount'];

                                        $sql = "SELECT SUM(dailyexpense_receipt) as total_amount "
                                            . " FROM {$tblpx}dailyexpense "
                                            . " WHERE (dailyexpense_receipt_type=89) "
                                            . " AND exp_type=72 " . $opening_hand
                                            . " AND company_id=" . $value['company_id'];
                                        $dailyexpense_cah_opening = Yii::app()->db->createCommand($sql)->queryRow();
                                        $dailyexpense_cash_opening = $dailyexpense_cah_opening['total_amount'];
                                        $deposit_opening = $daybook_cash_opening + $dailyexpense_cash_opening;

                                        $sql = "SELECT SUM((IFNULL(paid, 0))  - IFNULL(expense_tds, 0)) as total_amount FROM {$tblpx}expenses "
                                            . " WHERE expense_type=89 " // OR expense_type=103)
                                            . " AND type=73 " . $opening_hand
                                            . " AND company_id=" . $value['company_id'];
                                        $daybook_with_opening = Yii::app()->db->createCommand($sql)->queryRow();
                                        $daybook_with_amount_opening = $daybook_with_opening['total_amount'];

                                        $sql = "SELECT SUM(dailyexpense_paidamount) as total_amount "
                                            . " FROM {$tblpx}dailyexpense "
                                            . " WHERE (expense_type=89) AND exp_type=73 " . $opening_hand
                                            . " AND company_id=" . $value['company_id'];
                                        $dailyexpense_with_opening = Yii::app()->db->createCommand($sql)->queryRow();
                                        $dailyexpense_with_amount_opening = $dailyexpense_with_opening['total_amount'];

                                        $sql = "SELECT SUM( (IFNULL(amount, 0) + IFNULL(tax_amount, 0))- IFNULL(tds_amount,0) ) "
                                            . " as total_amount FROM {$tblpx}dailyvendors "
                                            . " WHERE payment_type=89 " . $opening_hand
                                            . " AND company_id=" . $value['company_id'];
                                        $dailyvendors_cah_opening = Yii::app()->db->createCommand($sql)->queryRow();
                                        $dailyvendors_cash_opening = $dailyvendors_cah_opening['total_amount'];
                                        $withdrawal_opening = $dailyvendors_cash_opening + $daybook_with_amount_opening + $dailyexpense_with_amount_opening;

                                        // $opening_balance = ($deposit_opening + $value['cashbalance_opening_balance']) - $withdrawal_opening;
                                        $opening_hand = " AND date <= '" . $opening_date . "'";
                                        //deposit opening               
                                        $copendeposit = CashBalanceHelper::cdepositOpening($opening_hand, $value['company_id']);
                                        $deposit_opening = $copendeposit['daybook_cash_opening'] + $copendeposit['dailyexpense_cash_opening'];

                                        //withdrwa opening               
                                        $copenwithdraw = CashBalanceHelper::cwithdrawOpening($opening_hand, $value['company_id']);
                                        $withdrawal_opening = floatval($copenwithdraw['dailyvendors_cash_opening']) + floatval($copenwithdraw['dailyexpense_with_amount_opening']) + floatval($copenwithdraw['daybook_with_amount_opening']);

                                        // $opening_balance = ($deposit_opening + floatval($value['cashbalance_opening_balance'])) - $withdrawal_opening;
                                        $opening_balance = floatval($value['cashbalance_opening_balance']);
                                    }
                                    ?>

                                    <tr>
                                        <td>
                                            <?php echo $key + 1; ?>
                                        </td>
                                        <td>
                                            <?php
                                            $company = Company::model()->findByPk($value['company_id']);
                                            echo $company->name;
                                            ?>
                                        </td>
                                        <td>
                                            <?php echo $value['caption']; ?>
                                        </td>
                                        <td>
                                            <?php echo $value['bank_name']; ?>
                                        </td>
                                        <td>
                                            <?php echo $value['cashbalance_date']; ?>
                                        </td>
                                        <td align="right">
                                            <?php echo Controller::money_format_inr($opening_balance, 2); ?>
                                        </td>
                                        <td align="right">
                                            <?php echo Controller::money_format_inr($deposit, 2); ?>
                                        </td>
                                        <td align="right">
                                            <?php echo Controller::money_format_inr($withdrawal, 2); ?>
                                        </td>

                                        <td align="right">
                                            <?php echo Controller::money_format_inr((($opening_balance + $deposit) - $withdrawal), 2); ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>

                        </table>
                    </div>

                    <?php
                }
            } else {

                echo "No data found";
            }
        }
        ?>


    </div>



</div>
<style>
    #parent {
        max-height: 300px;
        margin-bottom: 20px;
    }

    #parent2 {
        max-height: 300px;
    }

    table thead th,
    table tfoot th {
        background-color: #eee;
    }

    .table {
        margin-bottom: 0px;
    }

    .contentdiv {
        position: relative;
    }

    .summarytext {
        position: absolute;
        right: 3px;
        top: 15px;
    }
</style>
<script>
    $(document).ready(function () {
        $("#fixTable").tableHeadFixer({
            'left': false,
            'foot': true,
            'head': true
        });
        $("#fixTable2").tableHeadFixer({
            'left': false,
            'foot': true,
            'head': true
        });
    });
    $(function () {
        $("#date_from").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: $("#date_to").val()
        });
        $("#date_to").datepicker({
            dateFormat: 'yy-mm-dd',
        });
        $("#date_from").change(function () {
            $("#date_to").datepicker('option', 'minDate', $(this).val());
        });
        $("#date_to").change(function () {
            $("#date_from").datepicker('option', 'maxDate', $(this).val());
        });
    });
</script>