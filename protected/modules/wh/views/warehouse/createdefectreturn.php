<?php
/* @var $this BillsController */
/* @var $model Bills */

$this->breadcrumbs = array(
    'Bills' => array('index'),
    'Create',
);

$this->menu = array(
    //array('label'=>'List Bills', 'url'=>array('index')),
    //array('label'=>'Manage Bills', 'url'=>array('admin')),
);
?>

<div class="container" id="expense">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>

    <div class="header-container">
        <h3>Defect Return</h3>
        <a href="<?php echo Yii::app()->createAbsoluteUrl('wh/warehouse/defectreturn'); ?>" class="btn btn-info">
            List Defect Return</a>
    </div>
    <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
    <?php $this->renderPartial('_returnform', array('model' => $model)); ?>

</div>
<script>
    $(document).ready(function () {
        $('#loading').hide();
    });
</script>