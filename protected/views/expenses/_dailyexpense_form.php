<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<?php
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
$newQuery1 = "";
foreach ($arrVal as $arr) {
    if ($newQuery) $newQuery .= ' OR';
    if ($newQuery1) $newQuery1 .= ' OR';
    $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
    $newQuery1 .= " FIND_IN_SET('" . $arr . "', company_id)";
}
?>
<style type="text/css">
    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .btn.addentries:focus,
    .btn.addentries:hover {
        border: 1px solid transparent;
        box-shadow: none;
    }

    .btn.addentriestransfer:focus,
    .btn.addentriestransfer:hover {
        border: 1px solid transparent;
        box-shadow: none;
    }

    .btn-info.addentriestransfer {
        background-image: none;
        background-color: #6a8ec7;
    }

    .addentriestransfer {
        margin-top: 15px;
    }

    button.addentriestransfer.collapsed:before {
        content: 'Fund Transfer';
        display: block;
    }

    button.addentriestransfer:before {
        content: 'Close';
        display: block;
    }

    .deleteclass {
        background-color: #efca92;
    }

    .filter_elem.links_hold {
        margin-top: 6px;
    }
</style>
<div class="container" id="expense">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="expenses-heading">
        
        <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
        
    </div>
    <div class="daybook form">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'dailyexpense-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true,
                'validateOnType' => false,),
        )); ?>
        <div class="clearfix custom-form-style">
            <div class="datepicker">
                <div class="page_filter clearfix">
                    <div class="row">
                        <div class="col-md-3 filter_elem">
                            <div class="display-flex">
                                <label class="nowrap-white-space margin-top-5">Entry Date:</label>
                                <?php echo CHtml::activeTextField($model, 'date', array('class' => 'form-control', 'size' => 10, 'autocomplete' => 'off', 'id' => 'Dailyexpense_date', 'value' => date('d-m-Y'), 'onchange' => 'changedate()')); ?>
                            </div>
                        </div>
                        <div class="col-md-3 filter_elem links_hold">
                            <a href="#" id="previous" class="link">Previous</a> |
                            <a href="#" id="current" class="link">Current</a> |
                            <a href="#" id="next" class="link">Next</a>
                        </div>
                        <?php
                        $tblpx        = Yii::app()->db->tablePrefix;
                        $expenseData  = Yii::app()->db->createCommand("select MAX(date) as entry_date FROM " . $tblpx . "dailyexpense")->queryRow();
                        ?>
                        <div class="col-md-3 pull-right last-entry">
                            <span id="lastentry">Last Entry date :
                                <span id="entrydate">
                                    <?php echo (isset($lastdate['date']) ? date("d-M-Y", strtotime($lastdate['date'])) : ''); ?>
                                </span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
        }
        $companyInfo = Company::model()->findAll(array('condition' => $newQuery));
        ?>
        

        <div>
            <div id="errormessage"></div>
        </div>
        <div class="block_hold shdow_box clearfix  custom-form-style" id="dailyform">
            <h4 class="section_title">Transaction Details</h4>
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="form-group">
                        <label for="expenseType">Company</label>
                        <select class="form-control js-example-basic-single" name="Dailyexpense[company_id]" id="Dailyexpense_company_id" style="width:100%">
                            <option value="">-Select Company-</option>
                            <?php
                            foreach ($companyInfo as $key => $value) {
                            ?>
                                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="form-group">
                        <label for="billNumber">Bill No</label>
                        <input type="text" class="form-control" name="Dailyexpense[bill_id]" id="Dailyexpense_bill_id" />
                        <div class="errorMessage nowrap"></div>
                    </div>
                </div>
                <?php
                $tblpx           = Yii::app()->db->tablePrefix;
                $cExpType        = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "company_expense_type WHERE type IN(2,1) ORDER BY name ASC")->queryAll();
                $cReceiptType    = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "company_expense_type WHERE type IN(0,2) ORDER BY name ASC")->queryAll();
                $deposit         = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "deposit ORDER BY deposit_name ASC")->queryAll();
                ?>
                <div class="col-md-3 col-sm-6">
                    <div class="form-group">
                        <label for="expenseHead">Transaction Head</label>
                        <select class="form-control js-example-basic-single" name="Dailyexpense[expensehead_id]" id="Dailyexpense_expensehead_id" style="width:100%">
                            <option value="">-Select Transaction Head-</option>
                            <optgroup label="Expense Head">
                                <?php
                                foreach ($cExpType as $company) {
                                    if(strtoupper($company["name"]) != 'PETTY CASH ISSUED'){ ?>
                                    <option data-id="1" value="1,<?php echo $company["company_exp_id"]; ?>"><?php echo $company["name"]; ?></option>
                                    
                                    <?php } } ?>
                            </optgroup>
                            <optgroup label="Receipt Head">
                                <?php
                                foreach ($cReceiptType as $receipt) {
                                    if(strtoupper($receipt["name"]) != 'PETTY CASH REFUND'){ ?>
                                    <option data-id="2" value="2,<?php echo $receipt["company_exp_id"]; ?>"><?php echo $receipt["name"]; ?></option>
                                <?php } } ?>
                            </optgroup>
                            <optgroup label="Deposit">
                                <?php
                                foreach ($deposit as $deposits) { ?>
                                    <option data-id="3" value="3,<?php echo $deposits["deposit_id"]; ?>"><?php echo $deposits["deposit_name"]; ?></option>
                                <?php } ?>
                            </optgroup>
                            <optgroup label="Withdrawal">
                                <option data-id="4" value="4,Withdrawal From Bank">Withdrawal From Bank</option>
                            </optgroup>
                            <optgroup label="Deposit to bank">
                                <option data-id="5" value="5,Cash Deposit to Bank">Cash Deposit To Bank</option>
                            </optgroup>
                        </select>
                    </div>
                </div>


                <div class="col-md-3 col-sm-6">
                    <div class="form-group">
                        <label for="expenseType">Transaction Type</label>
                        <select class="form-control js-example-basic-single" name="Dailyexpense[expense_type]" id="Dailyexpense_expense_type" style="width:100%">
                            <option value="">-Select Transaction Type-</option>                            
                            <option value="103" selected>Petty Cash</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 hiden_box" style="display:none;">
                    <div class="form-group">
                        <label for="bank">Bank</label>
                        <?php
                        echo $form->dropDownList($model, 'bank_id', CHtml::listData(Bank::model()->findAll(array(
                            'select' => array('bank_id, bank_name'),
                            'order' => 'bank_name ASC',
                            'condition' => '(' . $newQuery1 . ')',
                            'distinct' => true,
                        )), 'bank_id', 'bank_name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Bank-', 'style' => 'width:100%'));
                        ?>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 hiden_box" style="display:none;">
                    <div class="form-group">
                        <label for="cheque">Cheque No / Transaction ID</label>
                        <input type="text" class="form-control numbersOnly w-100" id="txtChequeno" name="Dailyexpense[dailyexpense_chequeno]" />
                        <div class="chq_error text-danger"></div>
                        <div class="errorMessage text-danger"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="form-group">
                        <label for="bank">Petty Cash User</label>
                        <?php
                        echo $form->dropDownList($model, 'employee_id', CHtml::listData(Users::model()->findAll(array(
                            'select' => array('userid, concat_ws(" ",first_name,last_name) as first_name'),
                            'order' => 'first_name ASC',
                            'condition' => '(' . $newQuery1 . ') AND user_type NOT IN (1)',
                            'distinct' => true,
                        )), 'userid', 'first_name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Employee-', 'style' => 'width:100%'));
                        ?>
                    </div>
                </div>
            </div>


            <h4 class="section_title">Amount Details</h4>
            <div class="row daybook-inner">
                <div class="col-md-3 col-sm-3">
                    <div class="form-group">
                        <label for="amount">Amount</label>
                        <input type="text" class="form-control check" id="DtxtAmount" name="Dailyexpense[dailyexpense_amount]" />
                    </div>
                </div>
                <div class="col-md-2 col-sm-3 gsts">
                    <div class="form-group">
                        <label for="sgstp">SGST(%)</label>
                        <input type="text" class="form-control check percentage" id="DtxtSgstp" name="Dailyexpense[dailyexpense_sgstp]" />
                        <span class="gstvalue" id="DtxtSgst1" name="Dailyexpense[dailyexpense_sgst]"></span>
                    </div>
                </div>
                <div class="col-md-2 col-sm-3 amntblock">
                    <div class="form-group">
                        <label for="sgst"></label>
                        <input type="hidden" class="form-control" id="DtxtSgst" name="Dailyexpense[dailyexpense_sgst]" readonly="true" />

                    </div>
                </div>
                <div class="col-md-2 col-sm-3 gsts">
                    <div class="form-group">
                        <label for="cgstp">CGST(%)</label>
                        <input type="text" class="form-control check percentage" id="DtxtCgstp" name="Dailyexpense[dailyexpense_cgstp]" />
                        <span class="gstvalue" id="DtxtCgst1" name="Dailyexpense[dailyexpense_cgst]"></span>

                    </div>
                </div>
                <div class="col-md-2 col-sm-3 amntblock">
                    <div class="form-group">
                        <label for="cgst"></label>
                        <input type="hidden" class="form-control" id="DtxtCgst" name="Dailyexpense[dailyexpense_cgst]" readonly="true" />

                    </div>
                </div>
                <div class="col-md-2 col-sm-3 gsts">
                    <div class="form-group">
                        <label for="igstp">IGST(%)</label>
                        <input type="text" class="form-control check percentage" id="DtxtIgstp" name="Dailyexpense[dailyexpense_igstp]" />
                        <span class="gstvalue" id="DtxtIgst1" name="Dailyexpense[dailyexpense_igst]"></span>

                    </div>
                </div>
                <div class="col-md-2 col-sm-3 amntblock">
                    <div class="form-group">
                        <label for="igst"></label>
                        <input type="hidden" class="form-control" id="DtxtIgst" name="Dailyexpense[dailyexpense_igst]" readonly="true" />

                    </div>
                </div>

                <div class="col-md-3 col-sm-3 totalamnt">
                    <div class="form-group">
                        <label for="total"></label>
                        <input type="hidden" class="form-control" id="DtxtTotal" name="Dailyexpense[amount]" readonly="true" />
                        <span class="gstvalue" id="DtxtgstTotal"></span>
                        <span class="gstvalue" id="DtxtTotal1" name="Dailyexpense[amount]"></span>
                    </div>
                </div>

            </div>


            <h4 class="section_title">Payment Details</h4>
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="form-group">
                        <label for="purchaseType">Purchase Type</label>
                        <select class="form-control js-example-basic-single" id="DtxtPurchaseType" name="Dailyexpense[dailyexpense_purchase_type]" style="width:100%">
                            <option value="">-Select Purchase Type-</option>
                            <option value="1">Credit</option>
                            <option value="2">Full Paid</option>
                            <option value="3">Partially Paid</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="form-group">
                        <label for="paid">Paid</label>
                        <input type="text" class="form-control check w-100" id="DtxtPaid" name="Dailyexpense[dailyexpense_paidamount]" />
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="DtxtDescription" name="Dailyexpense[description]" rows="1" /></textarea>
                    </div>
                </div>

            </div>
            <input type="hidden" name="Dailyexpense[petty_type]" value="0"/>
            <input type="hidden" name="Dailyexpense[txtExpensesId]" value="" id="txtExpensesId" />
            <div class="form-group submit-button text-right">
                <button type="button" class="btn btn-info" id="buttonsubmit1">ADD</button>
                <button type="button" class="btn" id="btnReset">RESET</button>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>
    
</div>
<?php $vendorUrl            = Yii::app()->createAbsoluteUrl("dailyexpense/dynamicVendor"); ?>
<?php $getUrl               = Yii::app()->createAbsoluteUrl("dailyexpense/getDataByDate"); ?>
<?php $getBillUrl           = Yii::app()->createAbsoluteUrl("dailyexpense/GetBillDetails"); ?>
<?php $getDaybook           = Yii::app()->createAbsoluteUrl("dailyexpense/getDaybookDetails"); ?>
<?php $addCashtransfer      = Yii::app()->createAbsoluteUrl("dailyexpense/addCashtransfer"); ?>
<?php $updateCashtransfer   = Yii::app()->createAbsoluteUrl("dailyexpense/updateCashtransfer"); ?>
<?php $getBankByCompany     = Yii::app()->createAbsoluteUrl("dailyexpense/getBankByCompany"); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-dateFormat/1.0/jquery.dateFormat.js"></script>
<script>
    $(document).ready(function() {
        $('#loading').hide();
        if ($("#Dailyexpense_expense_type").val() == 88) {
            $(".hiden_box").show();
            $("#txtChequeno").prop("readonly", false);
            $("#Dailyexpense_bank_id").prop("readonly", false);
        }



        $('#dailyform').on('shown.bs.collapse', function() {
            $("#Dailyexpense_company_id").select2("focus");
        });
    });
    $(function() {
        $("#Dailyexpense_date").datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: new Date()
        });
    });

    jQuery.extend(jQuery.expr[':'], {
        focusable: function(el, index, selector) {
            return $(el).is('button, :input, [tabindex]');
        }
    });

    $(document).on('keydown', 'input,select', function(e) {
        if (e.which == 13) {
            e.preventDefault();
            var $canfocus = $(':focusable');
            var index = $canfocus.index(document.activeElement) + 1;
            if (index >= $canfocus.length) index = 0;
            $canfocus.eq(index).focus();
        }
    });

    $(document).on('keydown', '.check,select', function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            var index = $('.check').index(this) + 1;
            $('.check').eq(index).focus();
        }
    });

    $("#DtxtIgstp").keydown(function(e) {
        $('#loading').show();
        var expensehead = $("#Dailyexpense_expensehead_id").find(':selected').data('id');
        if (e.keyCode == 13) {
            if (expensehead == 2) {
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                    success: function(result) {
                        $("#DtxtDescription").focus();
                    }
                });
            } else {
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                    success: function(result) {
                        $("#DtxtPurchaseType").focus();
                    }
                });
            }
        }
    });

    $(".js-example-basic-single").select2();
    $("#Dailyexpense_expensehead_id").change(function() {
        $('#loading').show();
        if ($(this).val() != '') {
            $.ajax({
                method: "POST",
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                success: function(result) {
                    $("#Dailyexpense_expense_type").focus();
                }
            });
        }
    });

    $("#Dailyexpense_company_id").change(function() {
        if ($(this).val() != '') {
            $.ajax({
                method: "POST",
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                success: function(result) {
                    $("#Dailyexpense_bill_id").focus();
                }
            });
        }
    })


    $("#Dailyexpense_expense_type").change(function() {
        var typeId = $("#Dailyexpense_expensehead_id").find(':selected').data('id');
        if (typeId == 4 || typeId == 5) {
            $(".hiden_box").show();
        } else {
            if ($(this).val() == 88) {
                $(".hiden_box").show();
            } else {
                $(".hiden_box").hide();
            }
        }


        if ($(this).val() != '') {
            $('#loading').show();
            if (typeId == 4 || typeId == 5) {
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                    success: function(result) {
                        $("#Dailyexpense_bank_id").focus();
                    }
                });
            } else {
                if ($(this).val() == 88) {
                    $.ajax({
                        method: "POST",
                        dataType: "json",
                        url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                        success: function(result) {
                            $("#Dailyexpense_bank_id").focus();
                        }
                    });
                } else {
                    $.ajax({
                        method: "POST",
                        dataType: "json",
                        url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                        success: function(result) {
                            $("#Dailyexpense_employee_id").focus();
                        }
                    });
                }
            }

        }


    });

    $("#Dailyexpense_employee_id").change(function() {

        if ($(this).val() != '') {
            $('#loading').show();
            $.ajax({
                method: "POST",
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                success: function(result) {
                    $("#DtxtAmount").focus();
                }
            });
        }
    });

    $("#DtxtPurchaseType").change(function() {

        var val = $(this).val();
        var trns_type = $("#Dailyexpense_expense_type").val();
        if (val != '') {
            $('#loading').show();
            if (val == 1) {
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                    success: function(result) {
                        $("#DtxtDescription").focus();
                    }
                });
            } else if (val == 2 && trns_type == 88) {
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                    success: function(result) {
                        $("#DtxtDescription").focus();
                    }
                });
            } else if ((val == 3 && trns_type == 88) || (val == 3 && trns_type == 89)) {
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                    success: function(result) {
                        $("#DtxtPaid").focus();
                    }
                });
            } else if (val == 2 && trns_type == 89) {
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                    success: function(result) {
                        $("#DtxtDescription").focus();
                    }
                });
            } else if (val == 2 && trns_type == 103) {
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                    success: function(result) {
                        $("#DtxtDescription").focus();
                    }
                });
            } else if (val == 3 && trns_type == 103) {
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                    success: function(result) {
                        $("#DtxtPaid").focus();
                    }
                });
            }

        }
    });

    $("#Dailyexpense_bank_id").change(function() {

        if ($(this).val() != '') {
            $('#loading').show();
            $.ajax({
                method: "POST",
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                success: function(result) {
                    $("#txtChequeno").focus();
                }
            });
        }
    });
    $("#DtxtPurchaseType").change(function(e) {
        if ($(this).val() == 3) {
            if (e.keyCode == 13) {
                $("#DtxtPaid").focus();
            }
        }
    });

    $("#txtChequeno").keyup(function(e) {
        var expensehead = $("#Dailyexpense_expensehead_id").find(':selected').data('id');
        if ($(this).val() != '') {
            if (e.keyCode == 13) {
                if (expensehead == 2) {
                    $("#DtxtDescription").focus();
                }
            }
        }
    });
    $("#Dailyexpense_expense_type").change(function(e) {

        var expensehead = $("#Dailyexpense_expensehead_id").find(':selected').data('id');
        if ($(this).val() != '') {
            if (($(this).val() == 89) && expensehead == 2) {
                $('#loading').show();
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                    success: function(result) {
                        $("#DtxtAmount").focus();
                    }

                });

            }
        }
    });

    $("#DtxtPaid").keyup(function(e) {
        var trns_type = $("#Dailyexpense_expense_type").val();
        var purchase_type = $("#DtxtPurchaseType").val();
        if ($(this).val() != '') {
            if (e.keyCode == 13) {
                if (trns_type == 89 && purchase_type == 3) {}
            }
        }
    });

    $("#DtxtDescription").keyup(function(e) {

        var expensehead = $("#Dailyexpense_expensehead_id").find(':selected').data('id');
        var trns_type = $("#Dailyexpense_expense_type").val();
        if ($(this).val() != '') {
            if (e.keyCode == 13) {
                if (expensehead == 2 && trns_type == 88) {
                    $('#loading').show();
                    $.ajax({
                        method: "POST",
                        dataType: "json",
                        url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                        success: function(result) {
                            $("#buttonsubmit1").focus();
                        }
                    });
                }
                if (expensehead == 2 && trns_type == 89) {
                    $("#buttonsubmit1").focus();
                }
            }
        }
    });
    $("#DtxtDescription").keyup(function(e) {
        if ($(this).val() != '') {
            if (e.keyCode == 13) {
                $("#buttonsubmit1").focus();
            }
        }
    });
</script>


<script type="text/javascript">
    function getBankByCompany(company_id, status) {
        $('#loading').show();
        $.ajax({
            url: "<?php echo $getBankByCompany; ?>",
            data: {
                "company_id": company_id
            },
            type: "POST",
            success: function(data) {
                if (status == 1) {
                    $("#Dailyexpense_bank_id").html(data);
                } else if (status == 2) {
                    $("#Cashtransfer_sender_bank").html(data);
                } else if (status == 3) {
                    $("#Cashtransfer_beneficiary_bank").html(data);
                }
            }
        });
    }

    function getFullData(newDate) {
        $('#loading').show();
        $.ajax({
            url: "<?php echo $getUrl; ?>",
            data: {
                "date": newDate
            },
            type: "POST",
            success: function(data) {
                $("#newlist").html(data);
            }
        });
    }

    function changedate() {
        var cDate = $("#Dailyexpense_date").val();
        getFullData(cDate);
        validateDateSelection(cDate);
    }
    $(document).ready(function() {
        $("#Dailyexpense_company_id").select2("focus");
        $("a#next").css("pointer-events", "none").css("color", "#ccccc");
        $("#txtSPaymentType").change(function(event) {
            event.preventDefault();
            var type = $(this).val();
            if (type == 88) {
                $(".hiden_box").css("display", "inline-block");
            } else {
                $(".hiden_box").css("display", "none");
            }
        });
        $("#txtBPaymentType").change(function(event) {
            event.preventDefault();
            var type = $(this).val();
            if (type == 88) {
                $(".hiden_box1").css("display", "block");
            } else {
                $(".hiden_box1").css("display", "none");
            }
        });
        $("#buttontransfersubmit").click(function(event) {
            $('.loading-overlay').addClass('is-active');
            event.preventDefault();
            var crDate = $("#Dailyexpense_date").val();
            var scompany = $("#txtSCompany").val();
            var sptype = $("#txtSPaymentType").val();
            var sbank = $("#Cashtransfer_sender_bank").val();
            var scheque = $("#txtSChequeno").val();
            var bcompany = $("#txtBCompany").val();
            var bptype = $("#txtBPaymentType").val();
            var bbank = $("#Cashtransfer_beneficiary_bank").val();
            var bcheque = $("#txtBChequeno").val();
            var amount = $("#txtTAmount").val();
            var description = $("#txtDescription1").val();
            var transferId = $("#txtTransferId").val();
            if (crDate == "" || scompany == "" || sptype == "" || bcompany == "" || bptype == "" || amount == "" || description == "") {
                $("#errormessage").show()
                    .html('<div class="alert alert-danger">Enter all mandatory fields1.</div>')
                    .fadeOut(10000);
                return false;
            }
            if (sptype == 88) {
                if (sbank == "" || scheque == "") {
                    $("#errormessage").show()
                        .html('<div class="alert alert-danger">Enter all mandatory fields2.</div>')
                        .fadeOut(10000);
                    return false;
                }
            }
            if (bptype == 88) {
                if (bbank == "" || bcheque == "") {
                    $("#errormessage").show()
                        .html('<div class="alert alert-danger">Enter all mandatory fields3.</div>')
                        .fadeOut(10000);
                    return false;
                }
            }
            var transferId = $("#txtTransferId").val();
            if (transferId == "") {
                var dataUrl = "<?php echo Yii::app()->createAbsoluteUrl("dailyexpense/addCashtransfer"); ?>";
            } else {
                var dataUrl = "<?php echo Yii::app()->createAbsoluteUrl("dailyexpense/updateCashtransfer"); ?>";
            }
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                url: dataUrl,
                data: {
                    "crDate": crDate,
                    "scompany": scompany,
                    "sptype": sptype,
                    "sbank": sbank,
                    "scheque": scheque,
                    "bcompany": bcompany,
                    "bptype": bptype,
                    "bbank": bbank,
                    "bcheque": bcheque,
                    "amount": amount,
                    "description": description,
                    "transferId": transferId
                },
                type: "POST",
                success: function(data) {
                    //  $('.loading-overlay').removeClass('is-active');
                    if (data == 2) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-danger">Failed.</div>')
                            .fadeOut(10000);
                        return false;
                    } else {
                        if (transferId == "") {
                            $("#errormessage").show()
                                .html('<div class="alert alert-success">Cash transfer details added successfully.</div>')
                                .fadeOut(10000);
                        } else {
                            $("#errormessage").show()
                                .html('<div class="alert alert-success">Cash transfer details updated successfully.</div>')
                                .fadeOut(10000);
                        }
                        $("#txtSCompany").val("").trigger('change.select2');;
                        $("#txtSPaymentType").val("").trigger('change.select2');;
                        $("#Cashtransfer_sender_bank").val("").trigger('change.select2');;
                        $("#txtSChequeno").val("");
                        $("#txtBCompany").val("").trigger('change.select2');;
                        $("#txtBPaymentType").val("").trigger('change.select2');;
                        $("#Cashtransfer_beneficiary_bank").val("").trigger('change.select2');
                        $("#txtBChequeno").val("");
                        $("#txtTAmount").val("");
                        $("#txtDescription1").val("");
                        $("#txtTransferId").val("");
                        $(".hiden_box").css("display", "none");
                        $(".hiden_box1").css("display", "none");
                        $("#Dailyexpense_date").val(crDate);
                        $("#newlist").html(data);
                    }
                }
            });
        });
        $(document).on("click","#buttonsubmit1",function(event) {
            event.preventDefault();
            $("#buttonsubmit1").attr('disabled', true);
            var crDate = $("#Dailyexpense_date").val();
            var bill = $("#Dailyexpense_bill_id").val();
            var trHeadType = parseInt($("#Dailyexpense_expensehead_id").find(':selected').data('id'));
            var trHead = parseInt($("#Dailyexpense_expensehead_id").val());
            var amount = parseFloat($("#DtxtAmount").val());
            var sgstp = parseFloat($("#DtxtSgstp").val());
            var sgst = parseFloat($("#DtxtSgst").val());
            var cgstp = parseFloat($("#DtxtCgstp").val());
            var cgst = parseFloat($("#DtxtCgst").val());
            var igstp = parseFloat($("#DtxtIgstp").val());
            var igst = parseFloat($("#DtxtIgst").val());
            var total = parseFloat($("#DtxtTotal").val());
            var trType = $("#Dailyexpense_expense_type").val();
            var desc = $("#DtxtDescription").val();
            var purchasetype = parseInt($("#DtxtPurchaseType").val());
            var paid = parseFloat($("#DtxtPaid").val());
            var bank = $("#Dailyexpense_bank_id").val();
            var cheque = $("#txtChequeno").val();
            var purchase_type = $("#DtxtPurchaseType").val();
            var employee_id = $("#Dailyexpense_employee_id").val();
            var company = $("#Dailyexpense_company_id").val();
            if (company == "") {
                $("#errormessage").show()
                    .html('<div class="alert alert-danger">Please select company.</div>')
                    .fadeOut(5000);
                $("#buttonsubmit1").attr('disabled', false);
                return false;
            }
            if (trHeadType == 2 || trHeadType == 5) {
                if (trHead == "" || amount == "" || total == "" || trType == "" || desc == "") {
                    $("#errormessage").show()
                        .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                        .fadeOut(5000);
                    $("#buttonsubmit1").attr('disabled', false);
                    return false;
                }
            } else {
                if (trHead == "" || amount == "" || total == "" || trType == "" || desc == "" || purchase_type == "") {
                    $("#errormessage").show()
                        .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                        .fadeOut(5000);
                    $("#buttonsubmit1").attr('disabled', false);
                    return false;
                }
            }

            if (trHeadType == 1 || trHeadType == 3) {
                if ((purchase_type == "" || paid == "") && trType != 1) {
                    $("#errormessage").show()
                        .html('<div class="alert alert-danger">The paid amount must be greater than 0.</div>')
                        .fadeOut(5000);
                    $("#buttonsubmit1").attr('disabled', false);
                    return false;
                }
            }
            if (trType == 88) {
                if (bank == "" || cheque == "") {
                    $("#errormessage").show()
                        .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                        .fadeOut(5000);
                    $("#buttonsubmit1").attr('disabled', false);
                    return false;
                }
            }
            if (trType == 103) {
                if (employee_id == "") {
                    $("#errormessage").show()
                        .html('<div class="alert alert-danger">Please fill all mandatory fields.</div>')
                        .fadeOut(5000);
                    $("#buttonsubmit1").attr('disabled', false);
                    return false;
                }
            }
            if (purchasetype == 3) {
                if (paid >= total) {
                    $("#errormessage").show()
                        .html('<div class="alert alert-warning">You are seleted the purchase type as Partially Paid.<br/>Paid amount should be less than total amount.</div>')
                        .fadeOut(10000);
                    $("#buttonsubmit1").attr('disabled', false);
                    return false;
                }
            }
            var dayBookId = $("#txtExpensesId").val();
            var actionUrl;
            if (dayBookId == "") {
                actionUrl = "<?php echo Yii::app()->createAbsoluteUrl("dailyexpense/addDailyexpense&headtype="); ?>" + trHeadType;
                localStorage.setItem("action", "add");
            } else {
                actionUrl = "<?php echo Yii::app()->createAbsoluteUrl("dailyexpense/updateDailyexpense&headtype="); ?>" + trHeadType;
                localStorage.setItem("action", "update");
            }
            var data = $("#dailyexpense-form").serialize();
            console.log(data);
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                type: 'POST',
                url: actionUrl,
                data: data,
                dataType: "JSON",
                success: function(data) {
                    if (data.status == "Edit") {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Request Send !</strong> Update request send to admin for approval.#ID:' + data.id + '</div>')
                            .fadeOut(5000);
                        $.ajax({
                            type: "POST",
                            data: {
                                date: crDate
                            },
                            url: "<?php echo Yii::app()->createUrl("dailyexpense/getAllData") ?>",
                            success: function(data) {
                                document.getElementById("dailyexpense-form").reset();
                                $("#Dailyexpense_date").val(crDate);
                                $("#newlist").html(data);
                                $("#Dailyexpense_bill_id").focus();
                                $("#buttonsubmit1").attr('disabled', false);
                                $('#Dailyexpense_company_id').val('').trigger('change.select2');
                                $('#Dailyexpense_expensehead_id').val('').trigger('change.select2');
                                $('#Dailyexpense_expense_type').val('').trigger('change.select2');
                                $('#DtxtPurchaseType').val('').trigger('change.select2');
                                $('#Dailyexpense_bank_id').val('').trigger('change.select2');
                                $('#Dailyexpense_employee_id').val('').trigger('change.select2');

                                $("#DtxtSgst1").text("");
                                $("#DtxtCgst1").text("");
                                $("#DtxtIgst1").text("");
                                $("#DtxtTotal1").html("");
                                $("#DtxtgstTotal").text("");
                            }
                        });
                    } else if (data.status == 1 || data.status == 5) {
                        if (data.status == 1) {
                            var message = data.error_message;
                        } else if (data.status == 5) {
                            $('html, body').animate({
                                scrollTop: $("#dailyexpense-form").offset().top
                            }, 200);
                            var message = "Transaction Unsuccessfull. User has insufficient balance!";
                            // var message = "Insufficient balance!! Please Add " + data.negative_amount.toFixed(2) + " for further proceedings.";
                        }
                        $("#errormessage").show()
                            .html('<div class="alert alert-warning font-weight-bold"><i class="fa fa-exclamation-circle pr-2 fa-lg"></i>' + message + '</div>')
                            .fadeOut(15000);

                        $("#buttonsubmit1").attr('disabled', false);
                        return false;
                    } else {
                        var currAction = localStorage.getItem("action");
                        if (currAction == "add") {
                            $("#errormessage").show()
                                .html('<div class="alert alert-success"><strong>Successfully !</strong> record added. ID is #' + data.id + '</div>')
                                .fadeOut(5000);
                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth() + 1; //January is 0!
                            var yyyy = today.getFullYear();
                            if (dd < 10)
                                dd = '0' + dd
                            if (mm < 10)
                                mm = '0' + mm
                            today = dd + '-' + mm + '-' + yyyy;
                            $.ajax({
                                type: "POST",
                                data: {
                                    date: crDate
                                },
                                url: "<?php echo Yii::app()->createUrl("dailyexpense/getAllData") ?>",
                                success: function(data) {
                                    document.getElementById("dailyexpense-form").reset();
                                    $("#Dailyexpense_date").val(crDate);
                                    $("#newlist").html(data);
                                    $("#Dailyexpense_bill_id").focus();
                                    $("#buttonsubmit1").attr('disabled', false);
                                    $('#Dailyexpense_company_id').val('').trigger('change.select2');
                                    $('#Dailyexpense_expensehead_id').val('').trigger('change.select2');
                                    $('#Dailyexpense_expense_type').val('').trigger('change.select2');
                                    $('#DtxtPurchaseType').val('').trigger('change.select2');
                                    $('#Dailyexpense_bank_id').val('').trigger('change.select2');
                                    $('#Dailyexpense_employee_id').val('').trigger('change.select2');

                                    $("#DtxtSgst1").text("");
                                    $("#DtxtCgst1").text("");
                                    $("#DtxtIgst1").text("");
                                    $("#DtxtTotal1").html("");
                                    $("#DtxtgstTotal").text("");
                                }
                            });
                            $("#entrydate").text(today);
                        } else if (currAction == "update") {
                            $("#errormessage").show()
                                .html('<div class="alert alert-success"><strong>Successfully !</strong> record updated.#ID.' + data.id + '</div>')
                                .fadeOut(5000);
                        }
                        document.getElementById("dailyexpense-form").reset();
                        $("#Dailyexpense_date").val(crDate);
                        $("#newlist").html(data);
                        $("#Dailyexpense_bill_id").focus();
                        $("#buttonsubmit1").attr('disabled', false);
                        $('#Dailyexpense_company_id').val('').trigger('change.select2');
                        $('#Dailyexpense_expensehead_id').val('').trigger('change.select2');
                        $('#Dailyexpense_expense_type').val('').trigger('change.select2');
                        $('#DtxtPurchaseType').val('').trigger('change.select2');
                        $('#Dailyexpense_bank_id').val('').trigger('change.select2');
                        $('#Dailyexpense_employee_id').val('').trigger('change.select2');

                        $("#DtxtSgst1").text("");
                        $("#DtxtCgst1").text("");
                        $("#DtxtIgst1").text("");
                        $("#DtxtTotal1").html("");
                        $("#DtxtgstTotal").text("");
                    }
                    //   $('.loading-overlay').removeClass('is-active');
                },
                error: function(data) {

                }
            });
        });
        $("#DtxtPaid").blur(function() {
            var totalamount = parseFloat($("#DtxtTotal").val());
            var purchasetype = $("#DtxtPurchaseType").val();
            var paid = parseFloat($("#DtxtPaid").val());
            if (purchasetype == 3) {
                if (paid >= totalamount) {
                    $("#errormessage").show()
                        .html('<div class="alert alert-warning">You have selected the purchase type as Partially Paid.<br/>The paid amount must be less than the total amount.</div>')
                        .fadeOut(10000);
                    return false;
                }
            }
        });
        $("body").on("click", ".row-daybook", function(e) {
            var reconStatus = $(this).parents("td").attr("data-status");
            if(reconStatus==1){
                alert("Can't update !Reconciled Entry");
                return;
            }
            $('.loading-overlay').addClass('is-active');
            $('html, body').animate({
                scrollTop: 0
            }, 'slow');

            $(".popover").removeClass("in");
            var rowId = $(this).attr("id");
            var expenseId = $(this).parents("tr").attr('data-id');
            var expId = $(this).parents("tr").attr('id');
            var expStatus = $(this).parents("tr").attr('data-status');
            //var expId = $("#expenseid" + rowId).val();
            var transferId = $("#transferid" + rowId).val();

            if (transferId == 0) {
                $("#buttonsubmit1").text("UPDATE");
                $("#txtExpensesId").val(expenseId);
                $("#dailyform.collapse").addClass("in");
                $("#dailytransferform.collapse").removeClass("in");
                $(".addentries").removeClass("collapsed");

                var id = $(".addentries").attr("id");
                if (id === undefined) {
                    $(".addentries").attr("id", "addentries1");
                    $(".addentriestransfer").attr("disabled", true);
                } else {
                    $(".addentriestransfer").attr("disabled", false);
                    $(".addentries").removeAttr("id");
                }
                var dataUrl = "<?php echo Yii::app()->createAbsoluteUrl("dailyexpense/getDaybookDetails"); ?>";
            } else if (transferId > 0) {
                $(".addentriestransfer").removeClass("collapsed");
                $("#dailyform.collapse").removeClass("in");
                $("#dailytransferform.collapse").addClass("in");
                $("#buttontransfersubmit").text("UPDATE");
                $("#txtTransferId").val(transferId);

                var id = $(".addentriestransfer").attr("id");
                if (id === undefined) {
                    $(".addentriestransfer").attr("id", "addentries1");
                    $(".addentries").attr("disabled", true);
                } else {
                    $(".addentries").attr("disabled", false);
                    $(".addentriestransfer").removeAttr("id");
                }
                var dataUrl = "<?php echo Yii::app()->createAbsoluteUrl("dailyexpense/getTransferDetails"); ?>";
            }

            if ($(".daybook .collapse").css('display') == 'visible') {
                $(".daybook .collapse").css({
                    "height": "auto"
                });
            } else {
                $(".daybook .collapse").css({
                    "height": ""
                });
            }
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                url: dataUrl,
                data: {
                    "expenseid": expId,
                    "transferId": transferId,
                    "expstatus": expStatus
                },
                type: "POST",
                success: function(data) {
                    var result = JSON.parse(data);
                    if (transferId == 0) {
                        $(".gstvalue").css({
                            "display": "inline-block"
                        });
                        var dailyexp = result["dexptype"];
                        if (result["exptypeid"])
                            var exphead = result["exptypeid"];
                        else
                            var exphead = result["exprhead"];
                        $("#Dailyexpense_bill_id").val(result["expbill"]);
                        $("#Dailyexpense_company_id").select2("focus");
                        $('#Dailyexpense_company_id').val(result["company"]).trigger('change.select2');
                        var headtype = result["headtype"];
                        if (dailyexp == "expense") { //Expense
                            if (headtype == 4) {
                                $("#Dailyexpense_expensehead_id").val('4,Withdrawal From Bank');
                                $('#Dailyexpense_expensehead_id').val('4,Withdrawal From Bank').trigger('change.select2');
                            } else {
                                $("#Dailyexpense_expensehead_id").val('1,' + exphead);
                                $('#Dailyexpense_expensehead_id').val('1,' + exphead).trigger('change.select2');
                            }
                        } else if (dailyexp == "deposit") { //Deposit
                            $("#Dailyexpense_expensehead_id").val('3,' + exphead);
                            $('#Dailyexpense_expensehead_id').val('3,' + exphead).trigger('change.select2');
                        } else if (dailyexp == "receipt") { //Receipt
                            if (headtype == 5) {
                                $("#Dailyexpense_expensehead_id").val('5,Cash Deposit to Bank');
                                $('#Dailyexpense_expensehead_id').val('5,Cash Deposit to Bank').trigger('change.select2');
                            } else {
                                $("#Dailyexpense_expensehead_id").val('2,' + exphead);
                                $('#Dailyexpense_expensehead_id').val('2,' + exphead).trigger('change.select2');
                            }
                        }
                        if (dailyexp == "expense" || dailyexp == "deposit") {
                            var trType = result["exptypecc"];
                            $("#DtxtPurchaseType").attr("disabled", false);
                            $("#DtxtPaid").attr("readonly", false);
                            $("#DtxtPurchaseType").val(result["expptype"]);
                            $('#DtxtPurchaseType').val(result["expptype"]).trigger('change.select2');
                            $("#DtxtPaid").val(result["exppaid"]);
                        } else if (dailyexp == "receipt") {
                            var trType = result["exprtype"];
                            $("#DtxtPurchaseType").val("");
                            $("#DtxtPaid").val("");
                            $("#DtxtPurchaseType").attr("disabled", true);
                            $("#DtxtPaid").attr("readonly", true);
                            $("#Dailyexpense_expense_type").html('<option value="">-Select Expense Type-</option><option value="88">Cheque / Online Payment</option><option value="89">Cash</option><option value="103">Petty Cash</option>');
                            if (headtype == 5) {
                                $("#DtxtPurchaseType").attr("disabled", false);
                                $("#DtxtPaid").attr("readonly", false);
                                $('#DtxtPurchaseType').val(result["expptype"]).trigger('change.select2');
                                $("#DtxtPaid").val(result["exppaid"]);
                            }
                        }

                        $("#DtxtAmount").val(result["expamount"]);
                        $("#DtxtSgstp").val(result["expsgstp"]);
                        $("#DtxtSgst").val(result["expsgst"]);
                        $("#DtxtSgst1").text(result["expsgst"]);
                        $("#DtxtCgstp").val(result["expcgstp"]);
                        $("#DtxtCgst").val(result["expcgst"]);
                        $("#DtxtCgst").val(result["expcgst"]);
                        $("#DtxtIgstp").val(result["expigstp"]);
                        $("#DtxtIgst").val(result["expigst"]);
                        $("#DtxtIgst1").text(result["expigst"]);
                        $("#DtxtTotal").val(result["exptotal"]);

                        $("#DtxtTotal1").html("<b>Total:</b> " + result["exptotal"] + " ");

                        var sgst = parseFloat(result["expsgst"]);
                        var cgst = parseFloat(result["expcgst"]);
                        var igst = parseFloat(result["expigst"]);

                        var gsttotal = sgst + cgst + igst;
                        $("#DtxtgstTotal").html("<b>Total Tax: </b>" + gsttotal + " ");

                        $("#Dailyexpense_expense_type").val(trType);
                        $('#Dailyexpense_expense_type').val(trType).trigger('change.select2');
                        $("#DtxtDescription").val(result["expdesc"]);
                        $('#Dailyexpense_employee_id').val(result["expemployee"]).trigger('change.select2');
                        if (trType == 88) {
                            $(".hiden_box").show();
                            $("#Dailyexpense_bank_id").attr("disabled", false);
                            $("#txtChequeno").attr("readonly", false);
                            $("#Dailyexpense_bank_id").val(result["bank"]);
                            $('#Dailyexpense_bank_id').val(result["bank"]).trigger('change.select2');
                            $("#txtChequeno").val(result["chequeno"]);
                        } else {
                            if (trType == 103) {
                                $(".hiden_box").hide();
                                $("#Dailyexpense_bank_id").val("");
                                $("#txtChequeno").val("");
                                $("#Dailyexpense_bank_id").attr("disabled", true);
                                $("#txtChequeno").attr("readonly", true);
                            } else if (trType == 89) {
                                console.log(result["bank"]);
                                if (result["bank"] != null) {
                                    $(".hiden_box").show();
                                    $("#Dailyexpense_bank_id").attr("disabled", false);
                                    $("#txtChequeno").attr("readonly", false);
                                    $("#Dailyexpense_bank_id").val(result["bank"]);
                                    $('#Dailyexpense_bank_id').val(result["bank"]).trigger('change.select2');
                                    $("#txtChequeno").val(result["chequeno"]);
                                } else {
                                    $(".hiden_box").hide();
                                    $("#Dailyexpense_bank_id").val("");
                                    $("#txtChequeno").val("");
                                    $("#Dailyexpense_bank_id").attr("disabled", true);
                                    $("#txtChequeno").attr("readonly", true);
                                }
                            } else {
                                $(".hiden_box").hide();
                                $("#Dailyexpense_bank_id").val("");
                                $("#txtChequeno").val("");
                                $("#Dailyexpense_bank_id").attr("disabled", true);
                                $("#txtChequeno").attr("readonly", true);
                            }
                        }
                        if (result["expptype"] == 3) {
                            $("#DtxtPaid").attr("readonly", false);
                        } else {
                            $("#DtxtPaid").attr("readonly", true);
                        }
                    } else if (transferId > 0) {
                        $("#txtSCompany").val(result["scompany"]).trigger('change.select2');
                        $("#txtSPaymentType").val(result["sptype"]).trigger('change.select2');
                        if (result["sptype"] == 88) {
                            $(".hiden_box").css("display", "block");
                            $("#Cashtransfer_sender_bank").val(result["sbank"]).trigger('change.select2');
                            $("#txtSChequeno").val(result["scheque"]);
                        } else {
                            $(".hiden_box").css("display", "none");
                        }
                        $("#txtBCompany").val(result["bcompany"]).trigger('change.select2');
                        $("#txtBPaymentType").val(result["bptype"]).trigger('change.select2');
                        if (result["bptype"] == 88) {
                            $(".hiden_box1").css("display", "block");
                            $("#Cashtransfer_beneficiary_bank").val(result["bbank"]).trigger('change.select2');
                            $("#txtBChequeno").val(result["bcheque"]);
                        } else {
                            $(".hiden_box1").css("display", "none");
                        }
                        $("#txtTAmount").val(result["amount"]);
                        $("#txtDescription1").val(result["desc"]);
                    }
                }
            });
        });
        $("#previous").click(function(e) {
            var cDate = $("#Dailyexpense_date").val();
            cDate = cDate.split("-").reverse().join("-");
            var newDate = new Date(cDate);
            var prDate = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate() - 1);
            var newPrDate = $.format.date(prDate, "dd-MM-yyyy");
            $("#Dailyexpense_date").val(newPrDate);
            getFullData(newPrDate);
            validateDateSelection(newPrDate);
        });
        $("#current").click(function() {
            var cDate = new Date();
            var newDate = $.format.date(cDate, "dd-MM-yyyy");
            $("#Dailyexpense_date").val(newDate);
            getFullData(newDate);
            validateDateSelection(newDate);
        });
        $("#next").click(function() {
            var cDate = $("#Dailyexpense_date").val();
            cDate = cDate.split("-").reverse().join("-");
            var newDate = new Date(cDate);
            var nxtDate = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate() + 1);
            var newNxtDate = $.format.date(nxtDate, "dd-MM-yyyy");
            $("#Dailyexpense_date").val(newNxtDate);
            getFullData(newNxtDate);
            validateDateSelection(newNxtDate);
        });

        $("#DtxtPurchaseType").change(function() {
            var purchaseType = $(this).val();
            var amount = $("#DtxtTotal").val();
            if (purchaseType == 1) {
                $("#DtxtPaid").val(0);
                $("#DtxtPaid").attr("readOnly", true);
            } else if (purchaseType == 2) {
                $("#DtxtPaid").val(amount);
                $("#DtxtPaid").attr("readOnly", true);
            } else if (purchaseType == 3) {
                $("#DtxtPaid").val(0);
                $("#DtxtPaid").attr("readOnly", false);
            } else {
                $("#DtxtPaid").val("0");
                $("#DtxtPaid").attr("readOnly", false);
            }
        });
        $("#btnReset, [data-toggle='collapse']").click(function() {
            $("#txtExpensesId").val("");
            $("#txtTransferId").val("");
            $("#buttonsubmit1").text("ADD");
            $("#buttontransfersubmit").text("ADD");
            $('#dailyexpense-form').find('input, select, textarea').not("#Dailyexpense_date").val('');
            $("#Dailyexpense_bill_id").val("");
            $("#Dailyexpense_expensehead_id").val("");

            $('#Dailyexpense_expensehead_id').val('').trigger('change.select2');
            $('#Dailyexpense_expense_type').val('').trigger('change.select2');
            $('#DtxtPurchaseType').val('').trigger('change.select2');
            $('#Dailyexpense_bank_id').val('').trigger('change.select2');
            $('#Dailyexpense_company_id').val('').trigger('change.select2');
            $('#Dailyexpense_employee_id').val('').trigger('change.select2');
            $("#DtxtSgst1").text("");
            $("#DtxtCgst1").text("");
            $("#DtxtIgst1").text("");
            $("#DtxtTotal1").html("");
            $("#DtxtgstTotal").text("");
            $('#txtSCompany').val('').trigger('change.select2');
            $('#txtSPaymentType').val('').trigger('change.select2');
            $('#Cashtransfer_sender_bank').val('').trigger('change.select2');
            $('#txtBCompany').val('').trigger('change.select2');
            $('#txtBPaymentType').val('').trigger('change.select2');
            $('#Cashtransfer_beneficiary_bank').val('').trigger('change.select2');
            $(".hiden_box").css("display", "none");
            $(".hiden_box1").css("display", "none");
        });
        $(".addentriestransfer").click(function() {
            var id = $(this).attr("id");
            if (id === undefined) {
                $(this).attr("id", "addentries1");
                $(".addentries").attr("disabled", true);
            } else {
                $(".addentries").attr("disabled", false);
                $(this).removeAttr("id");
            }
        });
        $(".addentries").click(function() {
            var id = $(this).attr("id");
            if (id === undefined) {
                $(this).attr("id", "addentries2");
                $(".addentriestransfer").attr("disabled", true);
            } else {
                $(".addentriestransfer").attr("disabled", false);
                $(this).removeAttr("id");
            }
        });
        $("#Dailyexpense_expensehead_id").change(function() {
            var typeId = $(this).find(':selected').data('id');
            var typeVal = $(this).find(':selected').text();
            $("#Dailyexpense_expense_type").val("103");
            if (typeId == 1 || typeId == 3) {
                $("#DtxtPurchaseType").attr("disabled", false);
                $("#DtxtPaid").attr("readonly", false);
                

            } else if (typeId == 2) {
                $("#DtxtPurchaseType").attr("disabled", true);
                $("#DtxtPaid").val("");
                $("#DtxtPaid").attr("readonly", true);
                

            } else if (typeId == 4 || typeId == 5) {
                $("#DtxtPurchaseType").attr("disabled", false);
                $("#DtxtPaid").attr("readonly", false);                
            }
        });
        $("#Dailyexpense_expense_type").change(function() {
            var transactionType = $("#Dailyexpense_expense_type").val();
            var typeId = $("#Dailyexpense_expensehead_id").find(':selected').data('id');
            var bank_id = $("#Dailyexpense_bank_id").val();
            if (transactionType == 88) {
                $("#Dailyexpense_bank_id").attr("disabled", false);
                $("#txtChequeno").attr("readonly", false);
            } else {
                if (typeId == 4 || typeId == 5) {
                    if (bank_id == null) {
                        $("#Dailyexpense_bank_id").val("");
                        $("#Dailyexpense_bank_id").attr("disabled", false);
                        $("#txtChequeno").val("");
                        $("#txtChequeno").attr("readonly", false);
                    }
                } else {
                    $("#Dailyexpense_bank_id").val("");
                    $("#Dailyexpense_bank_id").attr("disabled", true);
                    $("#txtChequeno").val("");
                    $("#txtChequeno").attr("readonly", true);
                }

            }
            if (transactionType == 1)
                $("#DtxtPurchaseType").html('<option value="">-Select Purchase Type-</option><option value="1">Credit</option>');
            else
                $("#DtxtPurchaseType").html('<option value="">-Select Purchase Type-</option><option value="2">Full Paid</option><option value="3">Partially Paid</option>');
        });
        $("#Dailyexpense_company_id").change(function() {
            var company_id = $(this).val();
            getBankByCompany(company_id, 1);

        });
        $("#txtSCompany").change(function() {
            var company_id = $(this).val();
            var txtBCompany = $("#txtBCompany").val();
            
            if (txtBCompany !=""){
                if(txtBCompany == company_id){
                    $("#errormessage").show()
                            .html('<div class="alert alert-danger"> Same Sender and Beneficiary Company Not Allowed.</div>')
                            .fadeOut(5000);                    
                    $("#buttontransfersubmit").attr('disabled',true);
                }else{                    
                    $("#buttontransfersubmit").attr('disabled',false);
                }
            }
            getBankByCompany(company_id, 2);
        });
        $("#txtBCompany").change(function() {
            var company_id = $(this).val();
            var txtSCompany = $("#txtSCompany").val();
            if (txtSCompany !=""){
                if(txtSCompany == company_id){
                    $("#errormessage").show()
                            .html('<div class="alert alert-danger"> Same Sender and Beneficiary Company Not Allowed.</div>')
                            .fadeOut(5000); 
                    $("#buttontransfersubmit").attr('disabled',true);
                }else{                    
                    $("#buttontransfersubmit").attr('disabled',false);
                }
            }
            getBankByCompany(company_id, 3);
        });
        $("#DtxtAmount, #DtxtSgstp, #DtxtCgstp, #DtxtIgstp").blur(function() {

            var transactionType = $("#Dailyexpense_expense_type").val();
            var purchaseType = $("#DtxtPurchaseType").val();
            var amount = parseFloat($("#DtxtAmount").val());
            var sgstp = parseFloat($("#DtxtSgstp").val());
            var cgstp = parseFloat($("#DtxtCgstp").val());
            var igstp = parseFloat($("#DtxtIgstp").val());

            var sgst = (sgstp / 100) * amount;
            var cgst = (cgstp / 100) * amount;
            var igst = (igstp / 100) * amount;

            if (isNaN(sgst)) sgst = 0;
            if (isNaN(cgst)) cgst = 0;
            if (isNaN(igst)) igst = 0;
            if (isNaN(amount)) amount = 0;

            var total = amount + sgst + cgst + igst;

            var gsttotal = (parseFloat(sgst) + parseFloat(cgst) + parseFloat(igst)).toFixed(2);

            if (purchaseType != "" && purchaseType == 2) {
                $("#DtxtPaid").val(total.toFixed(2));
            }

            $("#DtxtSgst").val(sgst.toFixed(2));
            $("#DtxtCgst").val(cgst.toFixed(2));
            $("#DtxtIgst").val(igst.toFixed(2));
            $("#DtxtTotal").val(total.toFixed(2));

            $(".gstvalue").css({
                "display": "inline-block"
            });
            $("#DtxtSgst1").text(sgst.toFixed(2));
            $("#DtxtCgst1").text(cgst.toFixed(2));
            $("#DtxtIgst1").text(igst.toFixed(2));
            $("#DtxtTotal1").html("<b>Total: </b>" + total.toFixed(2) + "");
            $("#DtxtgstTotal").html("<b>Total Tax: </b>" + gsttotal + " ");
        });
    });

    function validateDateSelection(cDate) {
        var selDate = cDate;
        var currDate = new Date();
        currDate = $.format.date(currDate, "dd-MM-yyyy");
        if (selDate == currDate) {
            $("a#next").css("pointer-events", "none").css("color", "#ccccc");
        } else {
            $("a#next").css("pointer-events", "visible").css("color", "#337ab7");
        }
        $("#btnReset").click();
        $("#txtExpensesId").val("");
        $("#txtTransferId").val("");
        $("#Dailyexpense_date").val(selDate);
    }

    function validateData() {
        var billNumber = $("#Dailyexpense_bill_id").val();
        var expenseType = $("#Dailyexpense_expensehead_id").val();
        var amount = $("#DtxtAmount").val();
        var sgstP = $("#DtxtSgstp").val();
        var sgst = $("#DtxtSgst").val();
        var cgstP = $("#DtxtCgstp").val();
        var cgst = $("#DtxtCgst").val();
        var total = $("#DtxtTotal").val();
        var receiptType = $("#Dailyexpense_dailyexpense_receipt_type").val();
        var receipt = $("#txtReceipt").val();
        var purchaseType = $("#DtxtPurchaseType").val();
        var paid = $("#DtxtPaid").val();
        var currId = $(this).attr(id);
        if (expenseType != "" || expenseType != 0) {
            $("#Dailyexpense_dailyexpense_receipt_type").attr("disabled", true);
            $("#txtReceipt").attr("readonly", true);
            $("#DtxtAmount").attr("readonly", false);
            $("#DtxtSgstp").attr("readonly", false);
            $("#DtxtSgst").attr("readonly", false);
            $("#DtxtCgstp").attr("readonly", false);
            $("#DtxtCgst").attr("readonly", false);
            $("#DtxtTotal").attr("readonly", false);
        }
        if (currId == "Dailyexpense_expensehead_id" && (expenseType == "" || expenseType == 0)) {
            $("#Dailyexpense_dailyexpense_receipt_type").attr("disabled", false);
            $("#txtReceipt").attr("readonly", false);
        }
    }

    $(".check").keydown(function(event) {
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
            (event.keyCode >= 96 && event.keyCode <= 105) ||
            event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
            event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
            var splitfield = $(this).val().split(".");
            if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
                event.preventDefault();
            }
        } else {
            event.preventDefault();
        }
        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();
    });

    $('.percentage').keyup(function() {
        if ($(this).val() > 100) {
            $(this).val('');
        }
    });

    $(document).on('click', '.delete_confirmation', function(e) {
        var rowId = $(this).attr("id");
        var expId = $("#expenseid" + rowId).val();
        var date = $("#Dailyexpense_date").val();
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                type: "POST",
                data: {
                    expId: expId,
                    expense_date: date,
                    status: 1,
                    delId: "",
                    action: 2
                },
                url: "<?php echo Yii::app()->createUrl("dailyexpense/deleteconfirmation") ?>",
                success: function(data) {
                    //   $('.loading-overlay').removeClass('is-active');
                    if (data == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-danger"><strong>Failed!</strong> This request unable to process now. Please try again later.</div>')
                            .fadeOut(5000);
                        return false;
                    } else {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Success!</strong> Request for deleting this row is send to admin. Please wait for response.</div>')
                            .fadeOut(5000);
                        $("#newlist").html(data);
                    }
                }
            });
        } else {
            return false;
        }
    });

    $(document).on('click', '.delete_restore', function(e) {
        var rowId = $(this).attr("id");
        var delId = $(this).attr("data-id");
        var expId = $("#expenseid" + rowId).val();
        var date = $("#Dailyexpense_date").val();
        var answer = confirm("Are you sure you want to restore?");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                type: "POST",
                data: {
                    expId: expId,
                    expense_date: date,
                    status: 2,
                    delId: delId,
                    action: 2
                },
                url: "<?php echo Yii::app()->createUrl("dailyexpense/deleteconfirmation") ?>",
                success: function(data) {
                    $('.loading-overlay').removeClass('is-active');
                    if (data == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-danger"><strong>Failed!</strong> This request unable to process now. Please try again later.</div>')
                            .fadeOut(5000);
                        return false;
                    } else {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Success!</strong> Restored data successfully.</div>')
                            .fadeOut(5000);
                        $("#newlist").html(data);
                    }
                }
            });
        } else {
            return false;
        }
    });

    $(document).on('click', '.delete_row', function(e) {
        var reconStatus = $(this).parents("td").attr("data-status");
            if(reconStatus==1){
                alert("Can't delete !Reconciled Entry");
                return;
            }
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            var rowId = $(this).attr("id");
            var expId = $("#expenseid" + rowId).val();
            var transferId = $("#transferid" + rowId).val();
            var date = $("#Dailyexpense_date").val();
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                type: "GET",
                data: {
                    expId: expId,
                    expense_date: date,
                    transferId: transferId
                },
                url: "<?php echo Yii::app()->createUrl("dailyexpense/deletexpense") ?>",
                success: function(data) {
                    $('.loading-overlay').removeClass('is-active');
                    if (data.response == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-warning"><strong>Failed !</strong> please try again.</div>')
                            .fadeOut(5000);
                        $("#buttonsubmit1").attr('disabled', false);
                        return false;
                    } else if (data.response == 2) {
                        $("#errormessage").show()
                                .html('<div class="alert alert-warning"><strong>'+data.data+'</div>')
                                .fadeOut(5000);
                    } else {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Success!</strong> Row Deleted.</div>')
                            .fadeOut(5000);
                        $("#newlist").html(data.data);
                    }
                    $("#txtExpensesId").val("");
                    $("#txtTransferId").val("");
                    $("#buttonsubmit1").text("ADD");
                    $('#dailyexpense-form').find('input, select, textarea').not("#Dailyexpense_date").val('');
                    $("#Dailyexpense_bill_id").val("");
                    $("#Dailyexpense_expensehead_id").val("");
                    $('#Dailyexpense_company_id').val('').trigger('change.select2');
                    $('#Dailyexpense_expensehead_id').val('').trigger('change.select2');
                    $('#Dailyexpense_expense_type').val('').trigger('change.select2');
                    $('#DtxtPurchaseType').val('').trigger('change.select2');
                    $('#Dailyexpense_bank_id').val('').trigger('change.select2');
                    $('#Dailyexpense_employee_id').val('').trigger('change.select2');

                    $("#DtxtSgst1").text("");
                    $("#DtxtCgst1").text("");
                    $("#DtxtIgst1").text("");
                    $("#DtxtTotal1").html("");
                    $("#DtxtgstTotal").text("");
                }
            });
        }
    });

    $("#txtSCompany").change(function() {
        if ($(this).val() != '') {
            $('#loading').show();
            $.ajax({
                method: "POST",
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                success: function(result) {
                    $("#txtSPaymentType").focus();
                }
            });
        }
    });

    $("#txtSPaymentType").change(function() {
        var val = $(this).val();
        if ($(this).val() != '') {
            $('#loading').show();
            if (val == 89) {
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                    success: function(result) {
                        $("#txtBCompany").focus();
                    }
                });
            } else {
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                    success: function(result) {
                        $("#Cashtransfer_sender_bank").focus();
                    }
                });
            }
        }
    });


    $("#Cashtransfer_sender_bank").change(function() {
        if ($(this).val() != '') {
            $('#loading').show();
            $.ajax({
                method: "POST",
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                success: function(result) {
                    $("#txtSChequeno").focus();
                }
            });
        }
    });

    $("#Cashtransfer_sender_bank").change(function() {
        if ($(this).val() != '') {
            $('#loading').show();
            $.ajax({
                method: "POST",
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                success: function(result) {
                    $("#txtSChequeno").focus();
                }
            });
        }
    });

    $("#txtSChequeno").keyup(function(e) {
        if ($(this).val() != '') {
            if (e.keyCode == 13) {
                $("#txtBCompany").focus();
            }
        }
    });

    $("#txtBCompany").change(function() {
        if ($(this).val() != '') {
            $('#loading').show();
            $.ajax({
                method: "POST",
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                success: function(result) {
                    $("#txtBPaymentType").focus();
                }
            });
        }
    });

    $("#txtBPaymentType").change(function() {
        var val = $(this).val();
        if ($(this).val() != '') {
            $('#loading').show();
            if (val == 89) {
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                    success: function(result) {
                        $("#txtTAmount").focus();
                    }
                });
            } else {
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                    success: function(result) {
                        $("#Cashtransfer_beneficiary_bank").focus();
                    }
                });
            }
        }
    });

    $("#Cashtransfer_beneficiary_bank").change(function() {
        if ($(this).val() != '') {
            $('#loading').show();
            $.ajax({
                method: "POST",
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('dailyexpense/ajaxcall'); ?>',
                success: function(result) {
                    $("#txtBChequeno").focus();
                }
            });
        }
    });

    $("#txtDescription1").keyup(function(e) {
        if ($(this).val() != '') {
            if (e.keyCode == 13) {
                $("#buttontransfersubmit").focus();
            }
        }
    });

    $(document).on('change', '#txtBPaymentType', function() {
        var val = $(this).val();
        var id = $("#Cashtransfer_sender_bank").val();
        var company = $("#txtBCompany").val();
        var scompany = $("#txtSCompany").val();
        var bcompany = $("#txtBCompany").val();
        if (val == 88 && scompany == bcompany) {
            $('#loading').show();
            if (val != '' && company != '') {
                $.ajax({
                    method: "POST",
                    data: {
                        id: id,
                        company: company
                    },
                    url: '<?php echo Yii::app()->createUrl('dailyexpense/getbank'); ?>',
                    success: function(result) {
                        $("#Cashtransfer_beneficiary_bank").html(result);
                    }
                });
            }
        }
    })

    $(document).on('change', '#txtSPaymentType', function() {
        var val = $(this).val();
        var id = $("#Cashtransfer_beneficiary_bank").val();
        var company = $("#txtSCompany").val();
        var scompany = $("#txtSCompany").val();
        var bcompany = $("#txtBCompany").val();
        if (val == 88 && scompany == bcompany) {
            if (val != '' && company != '') {
                $('#loading').show();
                $.ajax({
                    method: "POST",
                    data: {
                        id: id,
                        company: company
                    },
                    url: '<?php echo Yii::app()->createUrl('dailyexpense/getbank'); ?>',
                    success: function(result) {
                        $("#Cashtransfer_sender_bank").html(result);
                    }
                });
            }
        }
    })
    $(document).ajaxComplete(function() {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
    
    $("#txtSChequeno,#txtBChequeno ,#txtChequeno").change(function (e) {        
        var cheque_no = $(this).val();
        var bank = $("#Dailyexpense_bank_id").val();        
        $.ajax({
            type:'POST',               
            data: {
                cheque_no: cheque_no, 
                bank:bank                   
            },
            url: "<?php echo Yii::app()->createUrl("expenses/testChequeNumber") ?>",
            success: function (data) {
                $('.chq_error').html(data);
                return;                
            }
        })
    });
    
    function onlySpecialchars(str)
    {        
        var regex = /^[^a-zA-Z0-9]+$/;
        var message ="";      
        var matchedAuthors = regex.test(str);
         
        if (matchedAuthors) {
            var message ="Special characters not allowed";            
        } 
        return message;        
    }
      

    $("#Dailyexpense_bill_id").keyup(function () {      
        var message =  onlySpecialchars(this.value);   
        $(this).siblings(".errorMessage").show().html(message).addClass('d-block');
        var numItems = $('.errorMessage').not(':empty').length;        
        if(numItems >0){
            $("#buttonsubmit1").attr('disabled',true);
        }else{
            $("#buttonsubmit1").attr('disabled',false);
        } 
    });

    $("#txtChequeno").keyup(function(){
        var str = $(this).val();        
        var regex =  /^[a-zA-Z0-9]{2,}$/;
        var message ="";      
        var matchedAuthors = regex.test(str);
         
        if (!matchedAuthors) {
            var message ="Invalid cheque number";            
        } 

        $(this).siblings(".errorMessage").show().html(message).addClass('d-block');
        var numItems = $('.errorMessage').not(':empty').length;        
        if(numItems >0){
            $("#buttonsubmit1").attr('disabled',true);
        }else{
            $("#buttonsubmit1").attr('disabled',false);
        }
    })

    
</script>