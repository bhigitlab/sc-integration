<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/tablestyles.css" />
<h1>CASH BLANCE REPORT</h1>
<div class="container-fluid">
            
            <table class="table table-bordered table-striped" border="1">

                <thead>
                    <tr>
                        <th>Sl No.</th>
                        <th>Cash Balance Type</th>
                        <th>Bank</th>
                        <th>Date</th>
                        <th>Opening Balance</th>
                        <th>Deposit</th>
                        <th>Withdrawal</th>
                        <th>Closing Balance</th>
                    </tr>
                </thead>
                 <tbody>
					 <?php
					  foreach($cash_blance as $key=> $value){
					 ?>
					 
					 <tr>
                         <td><?php echo $key+1; ?></td>
                         <td><?php echo $value['caption']; ?></td>
                         <td><?php echo $value['bank_name'];?></td>
                         <td><?php echo $value['cashbalance_date'];?></td>
                         <td align="right"><?php echo $value['cashbalance_opening_balance'];?></td>
                         <td align="right"><?php echo $value['cashbalance_deposit'];?></td>
                         <td align="right"><?php echo $value['cashbalance_withdrawal'];?></td>
                         <td align="right"><?php echo $value['cashbalance_closing_balance'];?></td>
                     </tr>
                     <?php } ?>
                 </tbody>

            </table>

        </div>
