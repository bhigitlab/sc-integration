<?php

class PurchaseSubCategoryNameController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','Newlist'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','CheckSubCategory'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','removesubcategory'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionNewlist()
	{
		$model = new Brand();
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Brand']))
			$model->attributes=$_GET['Brand'];
			
		$this->render('newlist', array(
			'model' => $model,
		   'dataProvider' => $model->search(),
		));
	}
	
	public function actionCheckSubCategory()
	{
		$tblpx = Yii::app()->db->tablePrefix;
	   $specification = Yii::app()->db->createCommand("SELECT id FROM {$tblpx}purchase_sub_category WHERE sub_category_name ='".$_REQUEST['sub_category_name']."'")->queryRow();
	   if($specification) {
		echo 'false';
			} else {
				echo 'true';
			}
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
        if (isset($_POST['PurchaseSubCategory'])) {
			if ($_POST['sub_category_id'] == '' || $_POST['sub_category_id'] == 0) {
				$model = new PurchaseSubCategory;
				$this->performAjaxValidation($model);
				$model->unsetAttributes();
				$model->sub_category_name 	 = $_POST['PurchaseSubCategory']['sub_category_name'];
				if (isset($_POST['PurchaseSubCategory']['company_id']) || !empty($_POST['PurchaseSubCategory']['company_id'])) {
					$company = implode(",", $_POST['PurchaseSubCategory']['company_id']);
					$model->company_id = $company;
				} else {
					$model->company_id = NULL;
				}
				$model->created_by = Yii::app()->user->id;
				$model->created_date =  date('Y-m-d');
				if ($model->save()) {
					Yii::app()->user->setFlash('success', "Added Successfully");
					$this->redirect(array('purchaseCategory/newList'));
				}
			} else {
				$id = $_POST['sub_category_id'];
				$model = $this->loadModel($id);
				$this->performAjaxValidation($model);
				//$model->unsetAttributes();
				$model->sub_category_name 	 = $_POST['PurchaseSubCategory']['sub_category_name'];
				if (isset($_POST['PurchaseSubCategory']['company_id']) || !empty($_POST['PurchaseSubCategory']['company_id'])) {
					$company = implode(",", $_POST['PurchaseSubCategory']['company_id']);
					$model->company_id = $company;
				} else {
					$model->company_id = NULL;
				}
				if ($model->save()) {
					Yii::app()->user->setFlash('success', "Updated Successfully");
					$this->redirect(array('purchaseCategory/newList'));
				}
			}
		}

	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{
        $categoryId         = $_REQUEST["sub_category_id"];
		$categoryRs         = PurchaseSubCategory::model()->findByPk($categoryId);
		$category["sub_category_name"]   = $categoryRs["sub_category_name"];
		$category["company_id"]   = $categoryRs["company_id"];
		$categoryDet        = json_encode($category);
		echo $categoryDet;
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	public function actionremovesubcategory(){
		$id = $_POST["catId"];
		$model = $this->loadModel($id);
		$transaction = Yii::app()->db->beginTransaction();
		try{
			if( !$model->delete() ){
				$success_status = 0;
				throw new Exception( json_encode($model->getErrors()));
			} else {
				$success_status = 1;
			}
			$transaction->commit();
		}catch (Exception $error){
			$transaction->rollBack();
			$success_status = 0;
		}finally {
			if ($success_status == 1){
				echo json_encode( array('response' => 'success', 'msg'=>' Data Deleted Successfully '));
			}else{
				if ($error->errorInfo[1] == 1451){
					echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
				} else {
					echo json_encode( array('response' => 'error','msg' => 'Some error Occured'));
				}
			}

		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Brand');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Brand('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Brand']))
			$model->attributes=$_GET['Brand'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Brand the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PurchaseSubCategory::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Brand $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='purchase-sub-category-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
