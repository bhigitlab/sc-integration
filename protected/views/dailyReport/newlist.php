<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs = array(
    'performa invoice',
)
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<?php
$total_amount    = 0;
foreach ($newmodel as $expenses) {
    $total_amount = $total_amount + $expenses["amount"];
}

?>

<?php
$sort = new CSort;
$sort->defaultOrder = 'dr_id DESC';
$sort->attributes = array('dr_id' => 'Expense Id');
$dataProvider = new CArrayDataProvider($newmodel, array(
    'id' => 'dr_id', //this is an identifier for the array data provider
    'sort' => $sort,
    'keyField' => 'dr_id', //this is what will be considered your key field
    'pagination' => false
));
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'viewData' => array('total_amount' => $total_amount),
    'itemView' => '_newview',
    'id' => 'daybooklist',
    'enableSorting' => 1,
    'enablePagination' => true,
    'template' => '<div>{summary}{sorter}</div><div id="parent" class="table-wrapper margin-top-25"><table width="100" class="table total-table" id="fixTable">{items}</table></div>{pager}',
    'enablePagination' => false
)); ?>

<script>
    $(document).ready(function() {
        $("#fixTable").tableHeadFixer({
            'left': 1,
            'foot': true,
            'head': true
        });
    });
    $(document).ready(function() {
        $(".popover-test").popover({
            html: true,
            content: function() {
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function(e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function(e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function(e) {
            $('[data-toggle=popover]').each(function() {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
    });
</script>
<style>
    #parent {
        max-height: 400px;
    }

    th {
        background: #eee;
    }

    .pro_back {
        background: #fafafa;
    }

    .total-table {
        margin-bottom: 0px;
    }

    th,
    td {
        border-right: 1px solid #ccc !important;
    }
</style>