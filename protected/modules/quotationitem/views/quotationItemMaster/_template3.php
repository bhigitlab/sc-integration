<div  id="container_anotherwork" >
    <div class="row addRow panel" style="padding:10px">
        <div class="col-md-3">
            <?php echo $form->textField($model, 'worktype_label', array('name' => 'worktype_label_other[]', 'class' => 'form-control worktypelabel_other common-another', 'id' => 'worktypelabel_other_1', 'placeholder' => 'Work Type Label')); ?>
            <?php echo $form->error($model, 'worktype_label'); ?>
        </div>
        <div class="col-md-3">
            <?php echo $form->textArea($model, 'description', array('class' => 'form-control description', 'placeholder' => 'Description', 'name' => 'description[]', 'id' => 'description_1')); ?>
            <?php echo $form->error($model, 'description'); ?>
        </div>
        <div class="col-md-2">
            <?php echo $form->textField($model, 'rate', array('class' => 'form-control allownumericdecimal other_rate common-another', 'id' => 'other_rate_1', 'placeholder' => 'Rate', 'name' => 'other_rate[]')); ?>
            <?php echo $form->error($model, 'rate'); ?>
        </div>
        <div class="col-md-2">
            <?php echo $form->textField($model, 'profit', array('class' => 'form-control allownumericdecimal other_profit common-another', 'id' => 'other_profit_1', 'placeholder' => 'Profit %', 'name' => 'other_profit[]')); ?>
            <?php echo $form->error($model, 'profit'); ?>
        </div>
        <div class="col-md-2">
            <?php echo $form->textField($model, 'discount', array('class' => 'form-control allownumericdecimal other_discount common-another', 'id' => 'other_discount_1', 'placeholder' => 'Discount %', 'name' => 'other_discount[]')); ?>
            <?php echo $form->error($model, 'discount'); ?>
        </div>
    </div>
</div>
<div class="btn-holder-another">
    <p id="add_field_another"><a><span>&raquo; Add More</span></a></p>
    <p id="add_field_another_remove"><a><span>&raquo; Remove</span></a></p>
</div>