<?php

/**
 * This is the model class for table "{{quotation_gen_worktype}}".
 *
 * The followings are the available columns in table '{{quotation_gen_worktype}}':
 * @property integer $id
 * @property integer $qid
 * @property integer $section_id
 * @property integer $category_label_id
 * @property integer $worktype_label
 * @property integer $unit
 * @property integer $quantity
 * @property integer $quantity_nos
 * @property double $mrp
 * @property double $amount_after_discount
 * @property string $description
 * @property integer $master_cat_id
 */
class QuotationGenWorktype extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{quotation_gen_worktype}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('qid, section_id, category_label_id,worktype_label', 'required'),
            array('qid, section_id, category_label_id, unit, quantity,master_cat_id', 'numerical', 'integerOnly'=>true),
            array('mrp, amount_after_discount', 'numerical'),
            array('description', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, qid, section_id, category_label_id, unit,hsn_code, quantity, quantity_nos, mrp, amount_after_discount, description', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'qid' => 'Qid',
            'section_id' => 'Section',
            'category_label_id' => 'Category Gen',
            'worktype_label'=>'Worktype Label',
            'unit' => 'Unit',
            'quantity' => 'Quantity',
            'quantity_nos' => 'Quantity Nos',
            'mrp' => 'Mrp',
            'amount_after_discount' => 'Amount After Discount',
            'description' => 'Description',
            'hsn_code' => 'HSN Code',
            'master_cat_id'=>'Category'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('qid',$this->qid);
        $criteria->compare('section_id',$this->section_id);
        $criteria->compare('category_label_id',$this->category_label_id);
        $criteria->compare('worktype_label',$this->worktype_label);
        $criteria->compare('unit',$this->unit);
        $criteria->compare('quantity',$this->quantity);
        $criteria->compare('quantity_nos',$this->quantity_nos);
        $criteria->compare('mrp',$this->mrp);
        $criteria->compare('amount_after_discount',$this->amount_after_discount);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('hsn_code',$this->hsn_code,true);
        $criteria->compare('master_cat_id',$this->master_cat_id,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return QuotationGenWorktype the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}