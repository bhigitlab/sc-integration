<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<div class="page_filter clearfix custom-form-style">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>
    <div class="row">
        <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
            <label for="executive">Executive</label>
            <?php
            $listdata = CHtml::listData($sales_executive, "id", "name");
            echo CHtml::dropDownList('executive', (isset($_GET['executive']) ? $_GET['executive'] : ''), $listdata, array('empty' => 'Select a Executive', 'class' => 'form-control'));
            ?>
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
            <label for="location">Location</label>
            <?php
            $listlocation = CHtml::listData($location, "id", "name");
            echo CHtml::dropDownList('location', (isset($_GET['location']) ? $_GET['location'] : ''), $listlocation, array('empty' => 'Select a Location', 'class' => 'form-control'));
            ?>
        </div>

        <?php
        if (!isset($_GET['date_from']) || $_GET['date_from'] == '') {
            $datefrom = "";
        } else {
            $datefrom = $_GET['date_from'];
        }
        ?>
        <div class="form-group col-xs-12 col-sm-2 col-md-2 ">
            <!-- <div class="display-flex"> -->
            <label>From </label>
            <?php echo CHtml::textField('date_from', $datefrom, array('value' => $datefrom, 'readonly' => true, 'class' => 'form-control')); ?>
            <!-- </div> -->
        </div>
        <?php
        if (!isset($_GET['date_to']) || $_GET['date_to'] == '') {
            $date_to = "";
        } else {
            $date_to = $_GET['date_to'];
        }
        ?>
        <div class="form-group col-xs-12 col-sm-2 col-md-2 ">
            <!-- <div class="display-flex"> -->
            <label>To </label>
            <?php echo CHtml::textField('date_to', $date_to, array('value' => $date_to, 'readonly' => true, 'class' => 'form-control')); ?>
            <!-- </div> -->
        </div>
        <div class="form-group col-xs-12 col-sm-2 col-md-2 text-sm-left text-right">
            <label class="d-sm-block d-none">&nbsp;</label>
            <div>
                <input name="yt0" value="Go" type="submit" class="btn btn-sm btn-primary">
                <input id="reset" name="yt1" value="Clear" type="reset" class="btn btn-sm btn-default ">
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script>
    $('#reset').click(function () {
        location.href = '<?php echo $this->createUrl('report'); ?>';
    })
    $(document).ready(function () {
        $(".select_box").select2();
        $(function () {
            $("#date_from").datepicker({ dateFormat: 'dd-mm-yy' });

            $("#date_to").datepicker({ dateFormat: 'dd-mm-yy' });
        });
        $("#date_from").change(function () {
            $("#date_to").datepicker('option', 'minDate', $(this).val());
        });
        $("#date_to").change(function () {
            $("#date_from").datepicker('option', 'maxDate', $(this).val());
        });
    });
</script>
