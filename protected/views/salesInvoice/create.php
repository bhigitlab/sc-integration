<?php
/* @var $this SalesInvoiceController */
/* @var $model SalesInvoice */

$this->breadcrumbs = array(
	'Sales Invoices' => array('index'),
	'Create',
);
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<div class="container">
	<div class="loading-overlay">
		<span class="fa fa-spinner fa-3x fa-spin"></span>
	</div>
	<div class="clearfix">
		<div class="pull-left">
			<?php if ($model->isNewRecord) { ?>
				<h2>Create Sales Invoice</h2>
			<?php } else { ?>
				<h2>Update Sales Invoice</h2>
			<?php } ?>
		</div>
		<div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
		<?php echo CHtml::link('Invoices', array('salesInvoice/admin'), array('class' => 'btn btn-primary pull-right mt')); ?>

	</div>

	<?php if (Yii::app()->user->hasFlash('success')) : ?>
		<div class="flash-holder flash-success">
			<?php echo Yii::app()->user->getFlash('success'); ?>
		</div>
	<?php endif; ?>
	<div id="myflashwrapper" style="display: none;"></div>

	<?php if (Yii::app()->user->hasFlash('error')) : ?>
		<div class="alert alert-danger  alert-dismissible">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<?php echo Yii::app()->user->getFlash('error'); ?>
		</div>
	<?php endif; ?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="mt-0">Add Entry</h4>
		</div>

		<div class="panel-body" id="main_div">
			<?php $this->renderPartial('_form', array('model' => $model, 'user_companies' => $user_companies, 'main_item_model' => $main_item_model, 'sub_item_model' => $sub_item_model)); ?>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		$('#loading').hide();
	});
</script>