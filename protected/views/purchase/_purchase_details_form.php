<?php
if (!empty($mrId)) {
    $materialreq = MaterialRequisition::model()->findByPk($mrId);


    if (!empty($materialreq)) {
        //$purchasemodel = Purchase::model()->findByPk($materialreq->purchase_id);
        if (!empty($purchasemodel)) {
            ?>

        <?php }
    } ?>

<?php
} ?>
<?php
$tblpx = Yii::app()->db->tablePrefix;
$purchase_details = Yii::app()->db->createCommand("SELECT purchase_no FROM jp_purchase WHERE purchase_no IS NOT NULL AND company_id=" . Yii::app()->user->company_id . " ORDER BY p_id DESC LIMIT 0,1")->queryRow();
$lastpurchase_no = $purchase_details['purchase_no'];
if (is_numeric($lastpurchase_no)) {
    $lastpurchase_no = $lastpurchase_no + 1;
} else {
    $p_no = substr($lastpurchase_no, 0, -1);
    $last = substr($lastpurchase_no, -1);
    if (is_numeric($last)) {
        $last_no = $last + 1;
    } else {
        $last_no = $last . '1';
    }
    $lastpurchase_no = $p_no . $last_no;
}
$readonly = "";
if (!empty($model)) {
    $company_details = Company::model()->findByPk($model->company_id);
    if ($company_details->auto_purchaseno == 1) {
        $readonly = "readonly=true";
    } else {
        $readonly = "";
    }
}

?>


<div class="modal fade" id="myModal1" role="dialog">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body p-0"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal2" role="dialog">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body p-0"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal3" role="dialog">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body p-0"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal4" role="dialog">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body p-0"></div>
        </div>
    </div>
</div>

<div class="loading-overlay">
    <span class="fa fa-spinner fa-3x fa-spin"></span>
</div>

<input type="hidden" id="mrId" value="<?php echo isset($mrId) ? $mrId : '' ?>">
<input type="hidden" id="selectedMaterialItems" value="<?php echo isset($selectedItems) ? $selectedItems : '' ?>">


<div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
<div id="msg_box"></div>
<!-- <div class="entries-wrapper"> -->
<form id="pdfvals1" method="post" action="">

    <input type="hidden" name="remove" id="remove" value="">
    <input type="hidden" name="project_id" id="project_id"
        value="<?php echo isset($materialreq->project_id) ? $materialreq->project_id : '' ?>" ;>

    <input type="hidden" name="purchase_id" id="purchase_id" value="<?php if (isset($material_req)) {
        echo '0';
    } else {
        echo isset($purchasemodel->p_id) ? $purchasemodel->p_id : '0';
    } ?>" ;>
    <div class="row">
        <div class="col-xs-12">
            <div class="heading-title">Add Details</div>
            <div class="dotted-line"></div>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-xs-12 col-md-2 select2-width">

            <label>COMPANY :
                <a data-toggle="modal" href="<?php echo Yii::app()->createAbsoluteUrl('company/create&layout=1'); ?>"
                    data-target="#myModal1">Add New</a></label>
            <?php
            if (!empty($mrId)) {
                $newQuery = "";
                $materialreq = MaterialRequisition::model()->findByPk($mrId);
                if (!empty($materialreq)) {
                    $project = Projects::model()->findByPk($materialreq->project_id);
                    if (!empty($project)) {
                        $company_arr = explode(',', $project->company_id);
                        if (!empty($company_arr)) {
                            $newQuery = "";
                            foreach ($company_arr as $arr) {
                                if ($newQuery)
                                    $newQuery .= ' OR';
                                $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
                            }
                        }
                    }
                }

            } else {
                $user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                foreach ($arrVal as $arr) {
                    if ($newQuery)
                        $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
                }
            }

            $companyInfo = Company::model()->findAll(array('condition' => $newQuery));
            ?>
            <select name="company" class="form-control target company change_val" id="company_id">
                <option value="">Choose Company</option>
                <?php
                foreach ($companyInfo as $key => $value) {
                    if (!empty($model)) { ?>
                        <option value="<?php echo $value['id'] ?>" <?php echo ($value['id'] == $model->company_id) ? 'selected' : ''; ?>>
                            <?php echo $value['name']; ?>
                        </option>
                    <?php } else {
                        ?>
                        <option value="<?php echo $value['id'] ?>">
                            <?php echo $value['name']; ?>
                        </option>
                        <?php
                    }
                }
                ?>

            </select>

        </div>

        <div class="form-group col-xs-12 col-md-2 select2-width">

            <label>PROJECT :
                <a data-toggle="modal" href="<?php echo Yii::app()->createAbsoluteUrl('projects/create&layout=1'); ?>"
                    data-target="#myModal2">Add New</a></label>
            <select name="project" class=" form-control target project change_val" id="project">
                <?php if (!empty($model)) {
                    if (!empty($project)) { ?>
                        <option value="<?php echo $project->pid; ?>" selected>
                            <?php echo $project['name']; ?>
                        </option>
                    <?php }
                } else { ?>
                    <option value="">Choose Project</option>
                <?php } ?>
            </select>

        </div>
        <div class="form-group col-xs-12 col-md-2 select2-width">

            <label>EXPENSE HEAD :
                <a data-toggle="modal"
                    href="<?php echo Yii::app()->createAbsoluteUrl('expensetype/create&layout=1'); ?>"
                    data-target="#myModal3" class="myModal3">Add New</a></label>
            <select name="expense_head" class="form-control target expense_head change_val" id="expense_head">
                <?php if (!empty($model)) {
                    if (!empty($expense_head)) {
                        foreach ($expense_head as $value) {
                            ?>
                            <option value="<?php echo $value['type_id']; ?>" <?php echo ($value['type_id'] == $model->expensehead_id) ? 'selected' : ''; ?>>
                                <?php echo $value['type_name']; ?>
                            </option>
                        <?php }
                    }
                } else { ?>
                    <option value="">Choose Expense Head</option>
                <?php } ?>
            </select>

        </div>

        <div class="form-group col-xs-12 col-md-2 select2-width">

            <label>VENDOR :
                <a data-toggle="modal" href="<?php echo Yii::app()->createAbsoluteUrl('vendors/create&layout=1'); ?>"
                    data-target="#myModal4">Add New</a></label>
            <select name="vendor" class="form-control target vendor change_val" id="vendor">
                <?php if (!empty($model)) {
                    if (!empty($vendor)) {
                        foreach ($vendor as $value) {
                            ?>
                            <option value="<?php echo $value['vendorid']; ?>" <?php echo ($value['vendorid'] == $model->vendor_id) ? 'selected' : ''; ?>>
                                <?php echo $value['vendorname']; ?>
                            </option>
                        <?php }
                    }
                } else { ?>
                    <option value="">Choose Vendor</option>
                <?php } ?>
            </select>

        </div>
        <div class="form-group col-xs-12 col-md-2">

            <label class="">DATE : </label>
            <input type="text"
                value="<?php echo ((isset($model->purchase_date) && $model->purchase_date != '') ? date("d-m-Y", strtotime($model->purchase_date)) : date("d-m-Y")); ?>"
                id="datepicker" class="txtBox date inputs target form-control change_val" name="date"
                placeholder="Please click to edit">

        </div>
        <?php if (!empty($model)) {
            $purchaseno = $model->purchase_no;
        }
        ?>

        <div class="form-group col-xs-12 col-md-2">

            <label class="">PURCHASE NO : </label>
            <input type="text" required
                value="<?php echo ((isset($purchaseno) && $purchaseno != '') ? $purchaseno : ''); ?>"
                class="txtBox inputs target check_type purchaseno form-control change_val" name="purchaseno"
                id="purchaseno">


        </div>


        <div class="form-group col-xs-12 col-md-2">
            <label class="">CONTACT NO : </label>
            <?php
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $contact_no = $user->phonenumber;
            if (!empty($model)) {
                $contact_no = $model->contact_no;
            }
            ;
            ?>




            <input type="text" required
                value="<?php echo ((isset($contact_no) && $contact_no != '') ? $contact_no : ''); ?>"
                class="txtBox inputs target check_type purchaseno form-control change_val" name="contact_no"
                id="contact_no" placeholder="">
            <div class="errorMessage"></div>
        </div>

        <div class="form-group col-xs-12 col-md-2">

            <label>EXPECTED DELIVERY DATE : </label>
            <input type="text"
                value="<?php echo ((isset($model->expected_delivery_date) && $model->expected_delivery_date != '') ? date("d-m-Y", strtotime($model->expected_delivery_date)) : date("d-m-Y")); ?>"
                class="txtBox inputs target check_type delivery_date form-control" name="delivery_date"
                id="delivery_date" placeholder="">

        </div>
        <?php
        $shipping_address = '';
        if (!empty($model)) {

            $shipping_address = $model->shipping_address;
        } ?>
        <div class="form-group col-xs-12 col-md-3">

            <label>SHIPPING ADDRESS : </label>
            <textarea name="shipping_address" id="shipping_address" rows="2" cols="20" class="form-control change_val"
                autocomplete="off"><?= $shipping_address ?></textarea>
        </div>
        <?php
        $purchase_description = '';
        if (!empty($model)) {
            $purchase_description = $model->purchase_description;
        }
        ?>

        <div class="form-group col-xs-12 col-md-5">

            <label>DESCRIPTION : </label>
            <textarea name="po_description" id="po_description" rows="2" cols="20" autocomplete="off"
                class="form-control change_val"><?= $purchase_description ?></textarea>

        </div>

        <div class="form-group col-xs-12 col-md-3 margin-top-0 margin-bottom-0">
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="inclusive" name="inclusive" <?php echo (isset($model->inclusive_gst) && $model->inclusive_gst == 1) ? 'checked="checked"' : '' ?>>
                <label class="form-check-label" for="inclusive">Inclusive of GST
                </label>
            </div>
        </div>


    </div>



</form>
<!-- </div> -->

<div class="toastmessage" style="top: 60px;right: 6px;width: 350px;position: fixed;"></div>

<script>

    $(document).ready(function () {
        $('#loading').hide();
        $(".js-example-basic-single").select2();
        $(".project").select2();
        $(".vendor").select2();
        $(".company").select2();
        $(".expense_head").select2();
    });
    $(document).find("#expense_head").change(function () {
        $('#loading').show();
        var val = $(this).val();
        $("#vendor").html('<option value="">Select Vendor</option>');
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('purchase/dynamicvendor'); ?>',
            method: 'POST',
            data: {
                exp_id: val
            },
            dataType: "json",
            success: function (response) {
                $("#vendor").html(response.html);
                var erroCount = $(".block_purchase").find(".errorMessage").not(":empty").length;
                if (erroCount == 0) {
                    savePurchaseOrder();
                }
            }
        })
    })
    $(document).find("#project").change(function () {
        $('#loading').show();
        var val = $(this).val();
        $("#vendor").html('<option value="">Select Vendor</option>');
        $("#expense_head").html('<option value="">Select Expense Head</option>');
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('purchase/dynamicexpensehead'); ?>',
            method: 'POST',
            data: {
                project_id: val
            },
            dataType: "json",
            success: function (response) {
                if (response.msg != "") {
                    alert(response.msg);
                }
                $("#shipping_address").val(response.address);

                if (response.status == 'success') {
                    $("#expense_head").html(response.html);
                    $("#vendor").html('<option value="">Select Vendor</option>');
                }
                var erroCount = $(".block_purchase").find(".errorMessage").not(":empty").length;
                if (erroCount == 0) {
                    savePurchaseOrder();
                }
            }
        })
    })
    $(document).on("change", "#company_id", function () {
        var val = $(this).val();
        $('#loading').show();
        if (val != '') {

            var mrId = $('#mrId').val(); // Get the MR ID
            var companyId = $(this).val(); // Get the selected company ID
            if (mrId && companyId) {
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('purchase/getProjectByMrId'); ?>',
                    method: 'POST',
                    data: {
                        mrId: mrId,
                        companyId: companyId
                    },
                    dataType: "json",
                    success: function (response) {
                        if (response && response.html_arr) {
                            // Update the project dropdown with HTML options
                            $('#project').html(response.html);
                            $('#project').trigger('change');
                            $('#vendor').focus();
                            // Set purchase order number based on auto_pono and purchase_no
                            if (response.html_arr.auto_pono == 1 || response.html_arr.auto_pono == 2) {
                                $('#purchaseno').attr('readonly', true);
                                $('#purchaseno').val(response.html_arr.purchase_no);
                                $('#purchaseno').attr('title', response.html_arr.purchase_no);
                                $('#purchaseno').trigger('change');

                            } else {
                                $('#purchaseno').attr('readonly', false);
                                $('#purchaseno').val('');
                            }
                        }

                    }
                });
            } else {
                $("#project").html('<option value="">Choose Project</option>');
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('purchase/dynamicproject'); ?>',
                    method: 'POST',
                    data: {
                        company_id: val
                    },
                    dataType: "json",
                    success: function (response) {
                        //alert(response);
                        if (response.auto_pono == 1) {
                            $('#purchaseno').attr('readonly', true);
                            $('#purchaseno').val(response.purchase_no);
                            $('#purchaseno').attr('title', response.purchase_no);
                        } else if (response.auto_pono == 2) {
                            $('#purchaseno').attr('readonly', true);
                            $('#purchaseno').val(response.purchase_no);
                            $('#purchaseno').attr('title', response.purchase_no);
                        } else {
                            $('#purchaseno').attr('readonly', false);
                            $('#purchaseno').val('');
                        }
                        if (response.status == 'success') {
                            $("#project").html(response.html);
                        } else {
                            $("#project").html(response.html);
                        }
                        var erroCount = $(".block_purchase").find(".errorMessage").not(":empty").length;
                        if (erroCount == 0) {
                            savePurchaseOrder();
                        }
                    }
                })
            }
        } else {
            $('#purchaseno').attr('readonly', false);
            $('#purchaseno').val('');
        }
    });


    $(document).ready(function () {
        $('select').first().focus();
    });
    $(document).on("blur", ".delivery_date", function () {
        var erroCount = $(".block_purchase").find(".errorMessage").not(":empty").length;
        if (erroCount == 0) {
            savePurchaseOrder();
        }
    });
    $(function () {
        $(document).on("blur", ".date", function () {
            $("#contact_no").focus();
            var erroCount = $(".block_purchase").find(".errorMessage").not(":empty").length;
            if (erroCount == 0) {
                savePurchaseOrder();
            }
        });


        function onlySpecialchars(str) {
            var regex = /^\d{10}$/;
            var message = "";

            if (!str.match(regex)) {
                var message = "Not a valid Phone Number";
            }
            return message;
        }

        $(document).on("blur", "#contact_no", function () {
            var message = onlySpecialchars(this.value);
            $(this).siblings(".errorMessage").show().html(message);

            $("#delivery_date").focus();
            var erroCount = $(".block_purchase").find(".errorMessage").not(":empty").length;
            if (erroCount == 0) {
                savePurchaseOrder();
            }

        });
        $(document).on("blur", "#shipping_address", function () {
            var erroCount = $(".block_purchase").find(".errorMessage").not(":empty").length;
            if (erroCount == 0) {
                savePurchaseOrder();
            }
        });

        $(document).on("change", "#vendor", function () {
            var erroCount = $(".block_purchase").find(".errorMessage").not(":empty").length;
            $(".date").focus();
            if (erroCount == 0) {
                savePurchaseOrder();
            }
        });
    })

    function savePurchaseOrder() {
        var mrId = '';
        var selected_items = '';
        mrId = $("#mrId").val();
        selected_items = $("#selectedMaterialItems").val();

        var purchase_id = $("#purchase_id").val();
        //alert(purchase_id);
        var default_date = $(".date").val();
        var delivery_date = $(".delivery_date").val();
        var purchaseno = $("#purchaseno").val();
        var expense_head = $('#expense_head').val();
        var contact_no = $("#contact_no").val();
        var shipping_address = $("#shipping_address").val();
        var description_po = CKEDITOR.instances.po_description.getData();
        if ($("#inclusive").prop('checked') == true) {
            var inclusive_gst = 1;
        } else {
            var inclusive_gst = 0;
        }

        if ($('#inclusive').is(':checked')) {
            var inclusive = $('#inclusive').next("label").text();
        } else {
            var inclusive = '';
        }
        if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
            if (purchaseno === '') {
                event.preventDefault();
            } else {
                var project = $('#project').val();
                var vendor = $('#vendor').val();
                var company = $('#company_id').val();
                var date = $('.date').val();
                if (project === '' || vendor === '' || default_date === '' ||
                    purchaseno === '' || expense_head === '' ||
                    company === '' || delivery_date === '') {

                } else {
                    $('.loading-overlay').addClass('is-active');
                    $.ajax({
                        method: "GET",
                        data: {
                            purchase_id: purchase_id,
                            purchaseno: purchaseno,
                            default_date: default_date,
                            project: project,
                            vendor: vendor,
                            expense_head: expense_head,
                            company: company,
                            shipping_address: shipping_address,
                            contact_no: contact_no,
                            description_po: description_po,
                            inclusive_gst: inclusive_gst,
                            delivery_date: delivery_date,
                            mrId: mrId,
                            selected_items: selected_items,
                        },
                        dataType: "json",
                        url: '<?php echo Yii::app()->createUrl('purchase/createnewpurchase'); ?>',
                        success: function (result) {
                            if (result.response === 'success') {
                                $(".purchase_items").removeClass('checkek_edit');
                                $("#purchase_id").val(result.p_id);
                                $(".toastmessage").addClass("alert alert-success").html("" + result.msg + "").fadeIn().delay(4000).fadeOut();

                            } else if (result.response === 'error') {
                                $(".toastmessage").addClass("alert alert-danger").html("" + result.msg + "").fadeIn().delay(4000).fadeOut();

                            } else {

                            }


                            $('.loading-overlay').removeClass('is-active');
                        }
                    });
                }
            }

        } else {
            $(this).focus();
            $(".toastmessage").addClass("alert alert-danger").html("Please enter valid date DD-MM-YYYY format").fadeIn().delay(4000).fadeOut();
            // $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
            $(this).focus();
        }
    }
    $("#purchaseno").keypress(function (event) {
        if (event.keyCode == 13) {
            $("#purchaseno").blur();
        }
    });

    $("#contact_no").keypress(function (event) {
        if (event.keyCode == 13) {
            $(this).removeClass("focus_input");
            $("#shipping_address").addClass("focus_input");
            event.stopPropagation();
        }
    });

    $("#date").keypress(function (event) {
        if (event.keyCode == 13) {
            if ($(this).val()) {
                $("#purchaseno").focus();
            }
        }
    });

    $(".date").keyup(function (event) {
        if (event.keyCode == 13) {
            $(".date").click();
        }
    });

    $("#vendor").keyup(function (event) {
        if (event.keyCode == 13) {
            $("#vendor").click();
        }
    });

    $("#project").keyup(function (event) {
        if (event.keyCode == 13) {
            $("#project").click();
        }
    });

    $("#expense_head").keyup(function (event) {
        if (event.keyCode == 13) {
            $("#expense_head").click();
        }
    });
    $("#purchaseno").keyup(function () {
        if (this.value.match(/[^a-zA-Z0-9]/g)) {
            this.value = this.value.replace(/[^a-zA-Z0-9\-/]/g, '');
        }
    });
    var $ = jQuery.noConflict();
    $(document).ready(function () {

        CKEDITOR.replace('po_description');
        CKEDITOR.instances['po_description'].on("blur", function () {
            $("#cke_po_description").removeClass('focus_input');
            // $('#description').select2('focus');
            var erroCount = $(".block_purchase").find(".errorMessage").not(":empty").length;
            if (erroCount == 0) {
                savePurchaseOrder();
            }
        });
        CKEDITOR.instances['po_description'].on("focus", function (e) {
            $("#cke_po_description").addClass('focus_input');
        });
        // $("#main_table").tableHeadFixer({
        //     'left': false,
        //     'foot': true,
        //     'head': true
        // });
        $(".popover-test").popover({
            html: true,
            content: function () {
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function (e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function (e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function (e) {
            $('[data-toggle=popover]').each(function () {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
        $(document).ajaxComplete(function () {
            // $(".popover-test").popover({
            //     html: true,
            //     content: function() {
            //         return $(this).next('.popover-content').html();
            //     }
            // });
        });
    });
    function closeaction() {
        $(".modal").trigger("click");
    }


    // Form submissions
    $('#myModal1').on('hidden.bs.modal', function (e) {
        $(this).find(".panel-body")
            .find("input[type=text],textarea,select")
            .val('')
            .end()
            .find("input[type=checkbox], input[type=radio]")
            .prop("checked", "")
            .end();
    })

    $('#myModal2').on('hidden.bs.modal', function (e) {
        $(this).find(".panel-body")
            .find("input[type=text],textarea,select")
            .val('')
            .end()
            .find("input[type=checkbox], input[type=radio]")
            .prop("checked", "")
            .end();
    })
    $('#myModal3').on('hidden.bs.modal', function (e) {
        $(this).find(".panel-body")
            .find("input[type=text],textarea,select")
            .val('')
            .end()
            .find("input[type=checkbox], input[type=radio]")
            .prop("checked", "")
            .end();
    })
    $('#myModal4').on('hidden.bs.modal', function (e) {
        $(this).find(".panel-body")
            .find("input[type=text],textarea,select")
            .val('')
            .end()
            .find("input[type=checkbox], input[type=radio]")
            .prop("checked", "")
            .end();
    })
    // 1. COmpany
    $(document).on("submit", "#company-form", function () {
        var data = $("#company-form").serialize();
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('company/create&layout=1&popup=1'); ?>',
            method: 'POST',
            data: data,
            success: function (response) {
                console.log("Company added successfully");
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('purchase/dynamiccompany&popup=1'); ?>',
                    method: 'POST',
                    dataType: "json",
                    success: function (response) {
                        $("#company_id").html(response.html);
                        $(".modal").trigger("click");
                        $(".toastmessage").addClass("alert alert-success").html("New company added").fadeIn().delay(4000).fadeOut();

                    }
                })
            }
        })
        return false;
    })

    // 2. Project
    $(document).on("submit", "#projects-form", function () {
        var company = $("#company_id").val();
        if (company == "") {
            alert('Please choose a company');
            return false;
        }

        var data = $("#projects-form").serialize();
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('projects/create&layout=1'); ?>',
            method: 'POST',
            data: data,
            success: function (response) {
                console.log("Project added successfully");
                if (company != '') {
                    $("#project").html('<option value="">Select rty</option>');
                    $.ajax({
                        url: '<?php echo Yii::app()->createUrl('purchase/dynamicproject&popup=1'); ?>',
                        method: 'POST',
                        data: {
                            company_id: company
                        },
                        dataType: "json",
                        success: function (response) {
                            if (response.auto_pono == 1) {
                                $('#purchaseno').attr('readonly', true);
                                $('#purchaseno').val(response.purchase_no);
                            } else {
                                $('#purchaseno').attr('readonly', false);
                                $('#purchaseno').val('');
                            }
                            if (response.status == 'success') {
                                $("#project").html(response.html);
                                $(".modal").trigger("click");
                            } else {
                                $("#project").html(response.html);
                                $(".modal").trigger("click");
                            }
                            $(".toastmessage").addClass("alert alert-success").html("New project added").fadeIn().delay(4000).fadeOut();
                        }
                    })
                }
            }
        })

        return false;
    })

    // 3. Expense head
    $(document).on("submit", "#expense-type-form", function () {
        var company = $("#company_id").val();
        var project = $("#project").val();
        if (company == "") {
            alert('Please choose a company');
            return false;
        }
        if (project == "") {
            alert('Please choose a project');
            return false;
        }
        var data = $("#expense-type-form").serialize();
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('expensetype/create&layout=1&popup=1&projectid='); ?>' + project,
            method: 'POST',
            data: data,
            success: function (response) {
                console.log("Expense head added successfully");

                $("#vendor").html('<option value="">Select Vendor</option>');
                $("#expense_head").html('<option value="">Select Expense Head</option>');
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('purchase/dynamicexpensehead&popup=1'); ?>',
                    method: 'POST',
                    data: {
                        project_id: project
                    },
                    dataType: "json",
                    success: function (response) {
                        if (response.msg != "") {
                            alert(response.msg);
                        }
                        $("#shipping_address").val(response.address);

                        if (response.status == 'success') {
                            $("#expense_head").html(response.html);
                            $("#vendor").html('<option value="">Select Vendor</option>');
                            $(".modal").trigger("click");
                        }
                        $(".toastmessage").addClass("alert alert-success").html("New Expense head added").fadeIn().delay(4000).fadeOut();

                    }
                })
            }
        })
        return false;
    })

    // 4. Vendor
    $(document).on("submit", "#vendors-form", function () {
        var company = $("#company_id").val();
        var project = $("#project").val();
        var expense_head = $("#expense_head").val();
        if (company == "") {
            alert('Please choose a company');
            return false;
        }
        if (project == "") {
            alert('Please choose a project');
            return false;
        }
        if (expense_head == "") {
            alert('Please choose an Expense head');
            return false;
        }
        var data = $("#vendors-form").serialize();
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('vendors/create&layout=1'); ?>',
            method: 'POST',
            data: data,
            success: function (response) {
                console.log("Vendor added successfully");
                $("body").trigger("click");

                $("#vendor").html('<option value="">Select Vendor</option>');
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('purchase/dynamicvendor&popup=1'); ?>',
                    method: 'POST',
                    data: {
                        exp_id: expense_head
                    },
                    dataType: "json",
                    success: function (response) {
                        $("#vendor").html(response.html);
                        $(".modal").trigger("click");
                        $(".toastmessage").addClass("alert alert-success").html("New Vendor added").fadeIn().delay(4000).fadeOut();
                        var erroCount = $(".block_purchase").find(".errorMessage").not(":empty").length;
                        if (erroCount == 0) {
                            savePurchaseOrder();
                        }
                    }
                })
            }
        })
        return false;
    })
    function validate() {
        if (document.getElementById('inclusive').checked) {
            var inclusive = $(this).next("label").text();
            var erroCount = $(".block_purchase").find(".errorMessage").not(":empty").length;
            if (erroCount == 0) {
                savePurchaseOrder();
            }

        } else {
            var inclusive = "";
            var erroCount = $(".block_purchase").find(".errorMessage").not(":empty").length;
            if (erroCount == 0) {
                savePurchaseOrder();
            }
        }
    }
    document.getElementById('inclusive').addEventListener('change', validate);


</script>