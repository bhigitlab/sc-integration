<?php

class WarehousestockController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('@'),
            ),
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'getItemCategory', 'getParent', 'getunit', 'savedata', 'updatedata', 'delete', 'addwarehousestock', 'savewarehousestock', 'saveitem', 'ajax', 'StockValueReport', 'StockOpeningBalance'),
                'users' => array('@'),
                'expression' => 'Yii::app()->user->role <= 2 || Yii::app()->user->role == 5',
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
                'expression' => 'Yii::app()->user->role <= 2',
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $this->layout = false;
        $model = $this->loadModel($id);
        $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
        $this->render('update', array(
            'model' => $model,
            'specification' => $specification
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if ($this->loadModel($id)->delete()) {
            $model = new Warehousestock;
            $client = $this->renderPartial('newlist', array('dataProvider' => $model->search()));
            return $client;
        } else {
            echo 1;
        }

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $model = new Warehousestock;
        $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
        if (isset($_GET['warehouse_id']) || isset($_GET['date_from']) || isset($_GET['date_to'])) {
            $model->warehousestock_warehouseid = $_GET['warehouse_id'];
            $model->date_from = $_GET['date_from'];
            $model->date_to = $_GET['date_to'];
        }

        $this->render('create', array(
            'model' => $model,
            'dataProvider' => $model->searchStockStatusReport(Yii::app()->user->id),
            'specification' => $specification
        ));
    }
    public function actionStockOpeningBalance()
    {
        $model = new Warehousestock;
        $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
        if (isset($_GET['warehouse_id']) || isset($_GET['date_from']) || isset($_GET['date_to'])) {
            $model->warehousestock_warehouseid = $_GET['warehouse_id'];
            $model->date_from = $_GET['date_from'];
            $model->date_to = $_GET['date_to'];
        }
        $criteria = new CDbCriteria;
        //$criteria->addCondition('warehousestock_date=:warehousestock_date');
        //$criteria->params = array(':warehousestock_date' => date('Y-m-d'));
        $stock_list_today = Warehousestock::model()->findAll($criteria);
        $this->render('StockOpningBalance', array(
            'model' => $model,
            'dataProvider' => $model->search(Yii::app()->user->id),
            'specification' => $specification, 'list' => $stock_list_today
        ));
    }
    public function actionIndex1()
    {
        $model = new Warehousestock;
        $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
        if (isset($_GET['warehouse_id']) || isset($_GET['date_from']) || isset($_GET['date_to'])) {
            $model2->warehouse_id = $_GET['warehouse_id'];
            $model2->date_from = $_GET['date_from'];
            $model2->date_to = $_GET['date_to'];
        }
        $this->render('create', array(
            'model' => $model,
            'model2' => $model2,
            'dataProvider' => $model->searchStockStatusReport(Yii::app()->user->id),
            'dataProvider' => $model->search(Yii::app()->user->id),
            'specification' => $specification
        ));
    }
    public function actionStockValueReport()
    {
        $model = new Warehousestock;
        $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
        if (isset($_GET['warehouse_id']) || isset($_GET['date_from']) || isset($_GET['date_to']) || isset($_GET['item_id'])) {
            $model->warehousestock_warehouseid = $_GET['warehouse_id'];
            $model->date_from = $_GET['date_from'];
            $model->date_to = $_GET['date_to'];
            $model->warehousestock_itemid =  $_GET['item_id'];
        }
        $this->render('stock_value_report', array(
            'model' => $model,
            'dataProvider' => $model->search(Yii::app()->user->id),
            'specification' => $specification
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Warehousestock('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Warehousestock']))
            $model->attributes = $_GET['Warehousestock'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '')
    {
        if (!is_array($user_tree_array))
            $user_tree_array = array();
        $data = array();
        $final = array();
        $tblpx = Yii::app()->db->tablePrefix;
        
        $sql = "SELECT id, specification, brand_id,cat_id"
            . " FROM {$tblpx}specification "
            . " WHERE  company_id=" . Yii::app()->user->company_id;
        $specification = Yii::app()->db->createCommand($sql)->queryAll();


        foreach ($specification as $key => $value) {
            $brand = '';
            if ($value['brand_id'] != NULL) {
                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $value['brand_id'] . "")->queryRow();
                $brand = '-' . ucfirst($brand_details['brand_name']);
            }

            if ($value['cat_id'] != '') {
                $result = $this->actionGetParent($value['cat_id']);
                $final[$key]['data'] = ucfirst($result['category_name']) . $brand . '-' . ucfirst($value['specification']);
            } else {
                $final[$key]['data'] = $brand . '-' . $value['specification'];
            }



            $final[$key]['id'] = $value['id'];
        }
        return $final;
    }

    public function actionGetParent($id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $category = Yii::app()->db->createCommand("SELECT parent_id, id , category_name FROM {$tblpx}purchase_category WHERE id=" . $id)->queryRow();

        return $category;
    }

    public function actiongetunit()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $html['unit'] = '';
        if ($_REQUEST['data'] != '' && $_REQUEST['data'] != 'other') {
            $sql = "SELECT id, cat_id, brand_id, specification, unit "
            . " FROM {$tblpx}specification WHERE id=" . $_REQUEST['data'];
            $specification = Yii::app()->db->createCommand($sql)->queryRow();

            if (!empty($specification['unit'])) {
                $html['unit'] .= $specification['unit'];
            } else {
                $html['unit'] .= '';
            }
        } else {
            $html['unit'] .= '';
        }
        echo json_encode($html);
    }

    public function actionsavedata()
    {
        $model = new Warehousestock;
        $item_model = new WarehousestockItems;
        $item_batch = ($data['batch'] != "") ? trim($data['batch']) : NULL;
        //print_r($_POST);
        //exit();

        if (isset($_POST['Warehousestock'])) {
            $model->attributes = $_POST['Warehousestock'];
            $model->warehousestock_status = 1;
            $model->warehousestock_date = date('Y-m-d',  strtotime($_POST['Warehousestock']['warehousestock_date']));
            $model->batch = $item_batch;
            $model->rate = $data['rate'];
            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->company_id = Yii::app()->user->company_id;
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');

            if ($model->save()) {
                $stock_id = $model->warehousestock_id;
                if (isset($_POST['WarehousestockItems'])) {
                    $item_model->attributes = $_POST['WarehousestockItems'];
                    $item_model->warehousestock_id = $stock_id;
                    $item_model->created_by = yii::app()->user->id;
                    $item_model->created_date = date('Y-m-d');
                    $item_model->updated_by = yii::app()->user->id;
                    $item_model->updated_date = date('Y-m-d');
                    $item_model->save();
                }
                //$model=new Warehousestock;
                //$client = $this->renderPartial('newlist', array('dataProvider' => $model->search()));
                //return $client; 
            }
        }
    }

    public function actionupdatedata()
    {
        $id   = $_POST['Warehousestock']['warehousestock_id'];
        $model = $this->loadModel($id);
        if (isset($_POST['Warehousestock'])) {
            $model->attributes = $_POST['Warehousestock'];
            $model->warehousestock_status = 1;
            $model->company_id = Yii::app()->user->company_id;
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            if ($model->save()) {
                $model = new Warehousestock;
                $client = $this->renderPartial('newlist', array('dataProvider' => $model->search()));
                return $client;
            } else {
                echo 1;
            }
        }
    }


    public function actionsavewarehousestock()
    {
        $data = $_REQUEST['data'];
        $stock_id = $data['stock_id'];
        $item_batch = ($data['batch'] != "") ? trim($data['batch']) : NULL;
        $rate = floatval($data['rate']);
        if (isset($data['warehouse']) && isset($data['date'])) {
            if ($stock_id == 0) {
                $tblpx = Yii::app()->db->tablePrefix;

                if ($item_batch == null || $item_batch == "") {
                    $res_sql = "SELECT * FROM {$tblpx}warehousestock WHERE warehousestock_warehouseid=" . $data['warehouse'] . " AND warehousestock_itemid=" . $data['stock_item'] . " AND (batch IS Null OR batch ='" . $item_batch . "') AND rate = $rate ";
                    $res = Yii::app()->db->createCommand($res_sql)->queryRow();
                } else {
                    $res_sql = "SELECT * FROM {$tblpx}warehousestock WHERE warehousestock_warehouseid=" . $data['warehouse'] . " AND warehousestock_itemid=" . $data['stock_item'] . " AND  batch ='" . $item_batch . "' AND rate  = $rate ";
                    $res = Yii::app()->db->createCommand($res_sql)->queryRow();
                }
                if (empty($res)) {
                    $model = new Warehousestock;
                    $model->warehousestock_warehouseid = $data['warehouse'];
                    $model->warehousestock_date = date('Y-m-d',  strtotime($data['date']));
                    $model->batch = $item_batch;
                    $model->rate = $rate;
                    $model->warehousestock_itemid = $data['stock_item'];
                    $model->warehousestock_stock_quantity = $data['quantity'];
                    $model->warehousestock_initial_quantity = $data['quantity'];
                    $model->warehousestock_unit = $data['unit'];
                    $model->warehousestock_status = 1;
                    $model->created_by = yii::app()->user->id;
                    $model->created_date = date('Y-m-d');
                    $model->company_id = Yii::app()->user->company_id;
                    $model->updated_by = yii::app()->user->id;
                    $model->updated_date = date('Y-m-d');
                    if ($model->save(false)) {
                        $criteria = new CDbCriteria;
                        $criteria->addCondition('warehousestock_date=:warehousestock_date');
                        $criteria->params = array(':warehousestock_date' => date('Y-m-d'));
                        $stock_list_today = Warehousestock::model()->findAll($criteria);
                        $result =   $this->renderPartial('_list_data', array('list' => $stock_list_today));

                        return $result;
                        //echo json_encode(array('response' => 'success', 'msg' => 'Warehouse stock added successfully', 'result' => $result));
                    } else {
                        echo 1;
                        //echo json_encode(array('response' => 'error', 'msg' => 'Sorry some problem occured'));
                    }
                } else {
                    if ($res['warehousestock_initial_quantity'] == NULL || $res['warehousestock_initial_quantity'] == "") {
                        $warehousestock_stock_quantity = $data['quantity'] + $res['warehousestock_stock_quantity'];
                        $model = $this->loadModel($res['warehousestock_id']);
                        $model->warehousestock_stock_quantity = $warehousestock_stock_quantity;
                        $model->warehousestock_initial_quantity = $data['quantity'];
                        $model->updated_by = yii::app()->user->id;
                        $model->updated_date = date('Y-m-d');
                        if ($model->save(false)) {
                            $criteria = new CDbCriteria;
                            $criteria->addCondition('warehousestock_date=:warehousestock_date');
                            $criteria->params = array(':warehousestock_date' => date('Y-m-d'));
                            $stock_list_today = Warehousestock::model()->findAll($criteria);
                            $result =   $this->renderPartial('_list_data', array('list' => $stock_list_today));

                            return $result;
                            //echo json_encode(array('response' => 'success', 'msg' => 'Warehouse stock added successfully', 'result' => $result));
                        } else {
                            echo 1;
                            //echo json_encode(array('response' => 'error', 'msg' => 'Sorry some problem occured'));
                        }
                    } else {
                        echo 2;
                    }
                }
            } else {
                $tblpx = Yii::app()->db->tablePrefix;
                $res = Warehousestock::model()->findAll(array('condition' => 'warehousestock_warehouseid = "' . $data['warehouse'] . '" AND warehousestock_itemid="' . $data['stock_item'] . '" AND warehousestock_id!=' . $stock_id . ''));
                if (empty($res)) {
                    $model = Warehousestock::model()->findByPk($stock_id);
                    $model->warehousestock_warehouseid = $data['warehouse'];
                    $model->warehousestock_date = date('Y-m-d',  strtotime($data['date']));
                    $model->batch = $item_batch;
                    $model->rate = $rate;
                    $model->warehousestock_itemid = $data['stock_item'];
                    $model->warehousestock_stock_quantity = $data['quantity'];
                    $model->warehousestock_initial_quantity = $data['quantity'];
                    $model->warehousestock_unit = $data['unit'];
                    $model->company_id = Yii::app()->user->company_id;
                    $model->updated_by = yii::app()->user->id;
                    $model->updated_date = date('Y-m-d');
                    if ($model->save(false)) {
                        $model = new Warehousestock;
                        $result = $this->renderPartial('newlist', array('dataProvider' => $model->search()));
                        return $result;
                    } else {
                        echo 1;
                    }
                } else {
                    echo 2;
                }
            }
        }
    }

    public  function actionajax()
    {
        echo json_encode('success');
    }


    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Warehousestock the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Warehousestock::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Warehousestock $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'warehousestock-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
