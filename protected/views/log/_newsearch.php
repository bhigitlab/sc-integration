<?php
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
foreach($arrVal as $arr) {
    if ($newQuery) $newQuery .= ' OR';
    $newQuery .= " FIND_IN_SET('".$arr."', company_id)";
}
?>
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">-->

<script>
    $( function() {
            $( "#JpLog_fromdate" ).datepicker({
                dateFormat: 'yy-mm-dd'
            });
            $( "#JpLog_todate" ).datepicker({
                dateFormat: 'yy-mm-dd'
            });
            $("#JpLog_fromdate").change(function() {
                $("#JpLog_todate").datepicker('option', 'minDate', $(this).val());
            });
            $("#JpLog_todate").change(function() {
                $("#JpLog_fromdate").datepicker('option', 'maxDate', $(this).val());
            });
    } );

</script>

<div class="page_filter clearfix custom-form-style">
  <div class="row">
    <?php $form=$this->beginWidget('CActiveForm', array(
      'action'=>Yii::app()->createUrl($this->route),
      'method'=>'get',
    )); ?>
      <div class="filter_elem col-md-3 col-sm-6 col-xs-12 padding-y-5">
        <?php echo $form->textField($model,'fromdate',array('class'=>'date','placeholder'=>'Date From', "readonly" => true,'class'=>'form-control')); ?>
      </div>
        <div class="filter_elem col-md-3 col-sm-6 col-xs-12 padding-y-5">
        <?php echo $form->textField($model,'todate',array('class'=>'date','placeholder'=>'Date To', "readonly" => true,'class'=>'form-control')); ?>
      </div>
        <div class="filter_elem col-md-3 col-sm-6 col-xs-12 padding-y-5">
        <?php echo $form->textField($model,'log_table',array('placeholder'=>'Log Table','class'=>'form-control')); ?>
      </div>
        <div class="filter_elem col-md-3 col-sm-6 col-xs-12 padding-y-5">
        <?php echo $form->textField($model,'log_action',array('placeholder'=>'Log Action','class'=>'form-control')); ?>
      </div>
        <div class="filter_elem col-md-3 col-sm-6 col-xs-12 padding-y-5">
        <?php echo $form->dropDownList($model,'log_action_by', CHtml::listData(Users::model()->findAll(array('order' => 'userid ASC')), 'userid', 'username'), array('empty'=>'Select a user','class'=>'form-control')); ?>
      </div>
      <div class="filter_elem col-md-3 col-sm-6 col-xs-12 padding-y-5 filter_btns">
            <?php echo CHtml::submitButton('Go'); ?>
            <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="'. $this->createUrl('historylog').'"')); ?>
      </div>
  </div>
  <?php $this->endWidget(); ?>

</div><!-- search-form -->
