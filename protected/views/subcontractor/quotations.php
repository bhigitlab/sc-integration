<?php
$this->breadcrumbs = array(
    'Subcontractor',
);

?>
<?php
$tblpx = Yii::app()->db->tablePrefix;
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<div class="container" id="expense">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="expenses-heading sub-heading mb-10">
        <h3>SUBCONTRACTORS QUOTATIONS</h3>
        <?php if ((isset(Yii::app()->user->role) && (in_array('/subcontractor/addquotation', Yii::app()->user->menuauthlist)))) { ?>
            <a href="index.php?r=subcontractor/addquotation" class="button btn btn-primary">Add Quotation</a>
        <?php } ?>
    </div>
    <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
    <?php $this->renderPartial('_newsearch', array('model' => $model)) ?>


    <?php echo $this->renderPartial('_newform', array('model' => $model)); ?>
    <input type="hidden" name="txtId" id="txtId" value="" />

    <?php
    foreach (Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
    }
    ?>
    <div class="row">
        <div class="col-sm-12">
            <div id="errorMsg"></div>
        </div>
    </div>

    <div class="" id="allcontentview">
        <div class="row">

            <div class="col-sm-12 text-right">
                <b>Total Quotation Amount:
                    <?php echo Controller::money_format_inr($total_quot_amountsum, 2); ?>
                </b>
            </div>
        </div><br>

        <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'viewData' => array('amountsum' => $amountsum),
            'itemView' => '_newview',
            'template' => '<div class="clearfix"><div class="pull-right">{summary}</div><div class="pull-left">{sorter}</div></div><div id="parent" class="subq_parent1"><table cellpadding="10" class="table tab1" id="subqtntbl">{items}</table></div><div class="pager-container">{pager}</div>',
            'enableSorting' => true,
            'sortableAttributes' => array(
                'project' => 'Project',
            ),
            'afterAjaxUpdate' => 'reinitializeDataTable'  // Reinitialize after AJAX
        )); ?>

        <script type="text/javascript">
            function reinitializeDataTable() {
                $('#subq_parent1').dataTable({
                    "scrollY": "300px",
                    "scrollCollapse": true,
                    "paging": true,  // Ensure pagination is working here
                    "autoWidth": false,
                    "columnDefs": [
                        { "searchable": false, "targets": [0, 2] },
                    ],
                    "fixedHeader": true,

                });
            }
        </script>

        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close closeWriteoff">&times;</button>
                        <h4 class="modal-title">Subcontractor Write off</h4>
                    </div>
                    <?php $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'writeoff-form',
                        'action' => Yii::app()->createUrl('subcontractor/writeoff'),
                        'enableAjaxValidation' => false,
                    )); ?>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <?php echo $form->labelEx($writeoff, 'amount'); ?>
                                    <?php echo $form->textField($writeoff, 'amount', array('class' => 'form-control')); ?>
                                    <?php echo $form->error($writeoff, 'amount'); ?>
                                    <div class="error text-danger"></div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <?php echo $form->labelEx($writeoff, 'description'); ?>
                                    <?php echo $form->textField($writeoff, 'description', array('class' => 'form-control')); ?>
                                    <?php echo $form->error($writeoff, 'description'); ?>
                                </div>
                            </div>
                            <input type="hidden" id="sc_quotation_amount">
                            <?php echo $form->hiddenField($writeoff, 'id', array('type' => 'hidden')); ?>
                            <?php echo $form->hiddenField($writeoff, 'subcontractor_id', array('type' => 'hidden')); ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info" id="actionWriteoff">ADD</button>
                        <button type="button" class="btn btn-default closeWriteoff">Close</button>
                    </div>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
        <script>
            function writeoff(quotation_id) {
                $('#loading').show();
                $("#Writeoff_subcontractor_id").val(quotation_id);
                $.ajax({
                    url: '<?php echo Yii::app()->createAbsoluteUrl('subcontractor/checkwriteoff'); ?>',
                    type: 'POST',
                    data: {
                        quotation_id: quotation_id
                    },
                    success: function (data) {
                        var result = JSON.parse(data);
                        $("#Writeoff_amount").val(result["amount"]);
                        $("#Writeoff_description").val(result["description"]);
                        $("#Writeoff_id").val(result["id"]);
                        $("#sc_quotation_amount").val(result["quotation_amount"]);
                    }
                });
            }
            $(document).ready(function () {
                $('#loading').hide();
                $('.closeWriteoff').click(function () {
                    $('#myModal').modal('hide');
                    $("#Writeoff_amount").val("");
                    $("#Writeoff_description").val("");
                    $("#Writeoff_id").val("");
                    $("#Writeoff_subcontractor_id").val("");
                });

                $("#clear").click(function () {
                    window.location.href = "<?php echo Yii::app()->createAbsoluteUrl('subcontractor/quotations'); ?>";
                });
                $(".quotation_status").click(function () {
                    $('#loading').show();
                    var quotation_id = $(this).attr("id");
                    $.ajax({
                        url: '<?php echo Yii::app()->createAbsoluteUrl('subcontractor/quotationpermission'); ?>',
                        type: 'GET',
                        data: {
                            quotation_id: quotation_id
                        },
                        success: function (data) {
                            if (data == 1) {
                                $("#errorMsg").show()
                                    .html('<div class="alert alert-success"><b>Success!</b> Status is changed to saved.</div>')
                                    .fadeOut(5000);
                                $("#quotation_status" + quotation_id).text("Saved");
                            } else {
                                $("#errorMsg").show()
                                    .html('<div class="alert alert-danger"><b>Failed!</b> Unable to change the status. Please try again.</div>')
                                    .fadeOut(5000);
                            }
                        }
                    });
                });
            });

            function edit(id) {
                window.location.href = '<?php echo Yii::app()->createAbsoluteUrl('subcontractor/addquotation&scquotation_id='); ?>' + id;
            }

            function view(id) {
                window.location.href = '<?php echo Yii::app()->createAbsoluteUrl('subcontractor/viewquotation&scquotation_id='); ?>' + id;
            }
            function clone(id) {
                window.location.href = '<?php echo Yii::app()->createAbsoluteUrl('subcontractor/clonequotation&scquotation_id='); ?>' + id;
            }
            function approve(id, elem) {
                $.ajax({
                    url: '<?php echo Yii::app()->createAbsoluteUrl('subcontractor/approveScItems'); ?>',
                    type: 'POST',
                    data: {
                        quotation_id: id
                    },
                    success: function (data) {
                        if (data == 1) {
                            $("#errorMsg").show()
                                .html('<div class="alert alert-success"><b>Success!</b> Items Approved Successfully.</div>')
                                .fadeOut(5000);
                            setTimeout(function () {
                                location.reload();
                            }, 3000);

                        } else {
                            $("#errorMsg").show()
                                .html('<div class="alert alert-danger"><b>Error Occured.</b></div>')
                                .fadeOut(5000);
                        }
                    }
                });
            }
            function approvelabourrate(quotation_id) {

                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    url: '<?php echo Yii::app()->createAbsoluteUrl('subcontractor/approveScQuotLabourRateChanges'); ?>',
                    type: 'POST',
                    data: {
                        quotation_id: quotation_id
                    },
                    success: function (data) {
                        if (data == 1) {
                            $("#errorMsg").show()
                                .html('<div class="alert alert-success"><b>Success!</b> Labour Rate Difference Approved Successfully.</div>')
                                .fadeOut(5000);
                            setTimeout(function () {
                                location.reload();
                            }, 3000);

                        } else {
                            $("#errorMsg").show()
                                .html('<div class="alert alert-danger"><b>Error Occured.</b></div>')
                                .fadeOut(5000);
                        }
                    }
                })
            };


            function actdelete(id, elem) {
                var item_count = $(elem).attr('data-item-id');
                if (item_count > 0) {
                    var answer = confirm("This Quotation contains " + item_count + " items.Are you sure you want to delete?");
                } else {
                    var answer = confirm("Are you sure you want to delete?");
                }

                if (answer) {
                    $.ajax({
                        url: '<?php echo Yii::app()->createAbsoluteUrl('subcontractor/DeleteQuotations'); ?>',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            id: id
                        },
                        success: function (data) {
                            $("#errorMsg").show()
                                .html('<div class="alert alert-' + data.response + '"><b>' + data.msg + '</b></div>')
                                .fadeOut(5000);

                            setTimeout(function () {
                                location.reload();
                            }, 3000)
                        }
                    })
                }
            }
        </script>



    </div>
</div>


<style>
    .btn-info.addentries {
        background-color: #698ec8;
    }

    @media (min-width: 1024px) {}

    .navbar-right {
        float: right !important;
        margin-right: -20px;
    }

    .table {
        margin-bottom: 0px;
    }

    table tfoot th,
    table thead th {
        background: #eee;
    }

    #subqtntbl {
        overflow-y: auto;
        max-height: 400px;
        /* Adjust as needed */
        /* display: block; */
    }

    .pager-container {
        clear: both;
    }

    table {
        width: 100% !important;
        /* Ensure table uses full width of container */
    }

    td {
        white-space: nowrap !important;
        /* Prevent text wrapping if necessary */
    }
</style>

<script>
    $(document).ready(function () {
        $("#subqtntbl").tableHeadFixer({
            'left': false,
            'foot': true,
            'head': true
        });
    });
    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });

    $(".popover-test").popover({
        html: true,
        content: function () {
            return $(this).next('.popover-content').html();
        }
    });
    $('[data-toggle=popover]').on('click', function (e) {
        $('[data-toggle=popover]').not(this).popover('hide');
    });
    $('body').on('hidden.bs.popover', function (e) {
        $(e.target).data("bs.popover").inState.click = false;
    });
    $('body').on('click', function (e) {
        $('[data-toggle=popover]').each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
    $(document).ajaxComplete(function () {
        $(".popover-test").popover({
            html: true,
            content: function () {
                return $(this).next('.popover-content').html();
            }
        });

    });

    $("#Writeoff_amount").on("keyup", function () {

        var writeoff_amount = parseFloat($("#Writeoff_amount").val());
        var quotation_amount = parseFloat($("#sc_quotation_amount").val());

        $("#Writeoff_amount").siblings(".error").text("");
        $("#actionWriteoff").attr('disabled', false);
        if (quotation_amount < writeoff_amount) {
            $("#Writeoff_amount").siblings(".error").text("Write off amount must be less than quotation amount")
            $("#actionWriteoff").attr('disabled', true);
        }
    })
    $(".alert").fadeOut(1000);
</script>