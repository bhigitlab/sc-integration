<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Expense Type',
);
$page = Yii::app()->request->getParam('ExpenseType_page');
if ($page != '') {
    Yii::app()->user->setReturnUrl($page);
} else {
    Yii::app()->user->setReturnUrl(0);
}
?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container" id="expensetype">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="expenses-heading">
        <div class="clearfix">
                  <!-- remove addentries class -->
            <button type="button" id="btn1" href="<?php echo Yii::app()->createUrl('expensetype/savetopdf') ?>" class="btn btn-info pull-right mt-0 mb-10  margin-right-5">SAVE AS PDF</button>
            <button type="button" id="btn2" href="<?php echo Yii::app()->createUrl('expensetype/savetoexcel') ?>" class="btn btn-info pull-right mt-0 mb-10  margin-right-5">SAVE AS EXCEL</button>
             <?php
            if (isset(Yii::app()->user->role) && (in_array('/expensetype/createexpensetype', Yii::app()->user->menuauthlist))) {
            ?>
                <button class=" addexpense btn btn-info pull-right mt-0 mb-10 margin-right-5">Add Expense Head</button>
            <?php } ?>
            <h3>Expense Head</h3>
        </div>
                
    </div>
    
    <div id="addexpense" style="display:none;"></div>
    <div id="errMsg"></div>
    <div class="row">
        <div class="col-md-12">
            <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_newview',
                'template' => '<div class=" table-wrapper margin-top-10"><div id="table-wrapper"><table cellpadding="10" id="exptypetbl" class="table total-table fixtable">{items}</table></div></div>',
                'ajaxUpdate' => false,
            ));
            ?>
        </div>

        <?php
        Yii::app()->clientScript->registerScript('myjavascript', '
             $(document).ready(function(){
               $("#exptypetbl").dataTable( {
                    "scrollY": "300px",
                    "scrollX": true,
                    "scrollCollapse": true,
                   
                     "columnDefs"        : [       
                        { 
                            "searchable"    : false, 
                            "targets"       : [0,2] 
                        },
                        { "bSortable": false, "aTargets": [-1]  }
                    ],
                } );
                });

            ');
        ?>

        <script>
            function closeaction() {
                $('#addexpense').slideUp(500);
            }

            $(document).ready(function() {
                $('.addexpense').click(function() {
                    $('.loading-overlay').addClass('is-active');
                    var id = $(this).attr('data-id');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $this->createUrl('expensetype/create&layout=1') ?>",
                        success: function(response) {
                            $('#addexpense').html(response).slideDown();
                        },
                    });
                });
                $('.editProject').click(function() {
                    $('.loading-overlay').addClass('is-active');
                    var id = $(this).attr('data-id');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $this->createUrl('expensetype/update&layout=1&id=') ?>" + id,
                        success: function(response) {
                            $('.loading-overlay').removeClass('is-active');
                            $('#addexpense').html(response).slideDown();
                        },
                    });
                });
                $('.deleteProject').click(function() {
                    $('.loading-overlay').addClass('is-active');
                    var id = $(this).attr('data-id');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $this->createUrl('expensetype/delete&layout=1&id=') ?>" + id,
                        dataType: 'json',
                        success: function(response) {
                            $('html, body').animate({ scrollTop: 0 }, 'slow');
                            $("#errMsg").show()
                                .html('<div class="alert alert-'+response.success+'">'+response.message+'</div>')
                                .fadeOut(10000);
                            setTimeout(function () {                        
                                location.reload(true);
                            }, 3000);
                                
                           
                        },
                    });
                });


                jQuery(function($) {
                    $('#addExpensetype').on('keydown', function(event) {
                        if (event.keyCode == 13) {
                            $("#expensetypesearch").submit();
                        }
                    });
                });

            });
            $(document).ajaxComplete(function() {
                $('.loading-overlay').removeClass('is-active');
                $('#loading').hide();
            });
            $("#btn1").click(function(){
                let redirecturl = $(this).attr('href');
                window.location.href=redirecturl;
            });
            $("#btn2").click(function(){
                let redirecturl = $(this).attr('href');
                window.location.href=redirecturl;
            });
        </script>
    </div>
</div>
<style>
    .savepdf {
        background-color: #6a8ec7;
        border: 1px solid #6a8ec8;
        color: #fff;
        padding: 5px;
    }

    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    .panel {
        border: 1px solid #ddd;
    }

    .panel-heading {
        background-color: #eee;
        height: 40px;
    }
</style>