<?php

/**
 * This is the model class for table "{{brand}}".
 *
 * The followings are the available columns in table '{{brand}}':
 * @property integer $id
 * @property string $branch_name
 * @property integer $created_by
 * @property string $created_date
 */
class Brand extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{brand}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('brand_name, created_by, created_date, company_id', 'required'),
			array('created_by', 'numerical', 'integerOnly'=>true),
			array('brand_name', 'length', 'max'=>100),
                         //array('brand_name', 'unique'),
                        array('brand_name', 'checkexist'),
                        array('company_id', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, brand_name, created_by, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'brand_name' => 'Brand Name',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
                        'company_id'=> 'Company',
		);
	}
        
        public function checkexists($attribute, $params) {
		
		    $tblpx = Yii::app()->db->tablePrefix;


            if($this->id!='' ){

            	$data1 = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id ='".$this->id."'")->queryRow();

            	if($data1['brand_name'] != $this->brand_name ){

            		$data = Yii::app()->db->createCommand("SELECT id FROM {$tblpx}brand WHERE brand_name ='".$this->brand_name."'")->queryRow();
			
					if (!empty($data)) {

		                 $this->addError("brand", ("Brand is already taken"));
		             }
            	}

            	
            }
    }
    
    public function checkexist(){
        $id = Brand::model()->findByAttributes(array('id' => $this->id));
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('".$arr."', company_id)"; 
        }
            if(!empty($id)){
                $name = Brand::model()->findByAttributes(array('brand_name' => $this->brand_name),array('condition'=>'id != '.$this->id.' AND ('.$newQuery.')'));
                if(!empty($name)) {
                    $this->addError("brand_name", ("Brand name already exists"));
               }
            } else{
                $name = Brand::model()->findByAttributes(array('brand_name' => $this->brand_name),array('condition'=>$newQuery));
                if(!empty($name)) {
                    $this->addError("brand_name", ("Brand name already exists"));
                }
            }
    }


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('brand_name',$this->brand_name,true);
		$criteria->compare('created_by',$this->created_by);
        $criteria->compare('company_id',$this->company_id); 

		$user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                foreach($arrVal as $arr) {
                    if ($newQuery) $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET('".$arr."', company_id)";
                }
                $criteria->addCondition($newQuery);
		$criteria->order = 'id DESC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>false,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Brand the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
