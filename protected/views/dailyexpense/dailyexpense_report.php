<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">

<?php
    $company_address = Company::model()->findBypk(($model->company_id)?$model->company_id:Yii::app()->user->company_id);	
?>
<div class="pdf_spacing">
<h3 class="text-center">Daily Expense Report</h3>
<p class="text-center" style="font-size:12px"><?= $model->fromdate ?> To <?= $model->todate ?></p> 
<?php $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        'viewData' => array( 'paidsum' => $model->getSum($model->newsearch(), 'dailyexpense_paidamount'), 'receiptsum' => $model->getSum($model->newsearch(), 'dailyexpense_receipt')),
        'itemView' => '_newdailyexpensereportview', 'template' => '<div>{summary}{sorter}</div><div id="parent"><table cellpadding="10" class="table  list-view sorter" id="fixTable" border="1">{items}</table></div>',
)); ?>
</div>