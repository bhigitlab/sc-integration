<?php
/* @var $this QuotationCategoryMasterController */
/* @var $model QuotationCategoryMaster */

$this->breadcrumbs=array(
	'Quotation Finish Masters'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List QuotationFinishMaster', 'url'=>array('index')),
	array('label'=>'Manage QuotationFinishMaster', 'url'=>array('admin')),
);
?>

<h1>Create QuotationFinishMaster</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>