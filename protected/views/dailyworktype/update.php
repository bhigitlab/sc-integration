<div class="panel panel-gray">
    <div class="panel-heading form-head">
        <h3 class="panel-title">Update Daily Work Type</h3>
    </div>
    <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
