<?php
/* @var $this UnitConversionController */
/* @var $data UnitConversion */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('item_id')); ?>:</b>
	<?php echo CHtml::encode($data->item_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('base_unit')); ?>:</b>
	<?php echo CHtml::encode($data->base_unit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('conversion_factor')); ?>:</b>
	<?php echo CHtml::encode($data->conversion_factor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('conversion_unit')); ?>:</b>
	<?php echo CHtml::encode($data->conversion_unit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('priority')); ?>:</b>
	<?php echo CHtml::encode($data->priority); ?>
	<br />


</div>