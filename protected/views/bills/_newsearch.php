<?php
/* @var $this BillsController */
/* @var $model Users */
/* @var $form CActiveForm */
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>

<?php $form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
)); ?>
<div class="page_filter clearfix">
    <?php
    $tblpx = Yii::app()->db->tablePrefix;
    $user = Users::model()->findByPk(Yii::app()->user->id);
    $arrVal = explode(',', $user->company_id);
    $newQuery = "";
    $newQuery1 = "";
    foreach ($arrVal as $arr) {
        if ($newQuery)
            $newQuery .= ' OR';
        if ($newQuery1)
            $newQuery1 .= ' OR';
        $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
        $newQuery1 .= " FIND_IN_SET('" . $arr . "', company_id)";
    }
    $userId = Yii::app()->user->id;
    $bills_data = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "bills WHERE (" . $newQuery1 . ") ORDER BY bill_id desc")->queryAll();
    $purchase_data = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "purchase WHERE (" . $newQuery1 . ") AND purchase_no IS NOT NULL ORDER BY p_id desc")->queryAll();
    $sql = "SELECT * FROM " . $tblpx . "projects pr LEFT JOIN " . $tblpx . "project_assign pa ON pr.pid = pa.projectid WHERE (" . $newQuery1 . ") AND pa.userid = '$userId' ORDER BY pid desc";
    $project_data = Yii::app()->db->createCommand($sql)->queryAll();
    $vendor_data = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "vendors WHERE (" . $newQuery1 . ") ORDER BY vendor_id desc")->queryAll();
    $companyInfo = Company::model()->findAll(array('condition' => $newQuery));
    ?>
    <div class="row">
        <div class="form-group col-xs-12 col-md-2">
            <label>Purchase No </label>
            <select id="purchase_no" class="select_box form-control" name="purchase_no">
                <option value="">Select Purchase no</option>
                <?php
                if (!empty($purchase_data)) {
                    foreach ($purchase_data as $key => $value) {
                        ?>
                        <option value="<?php echo $value['purchase_no'] ?>" <?php echo ($value['purchase_no'] == $model->purchase_no) ? 'selected' : ''; ?>>
                            <?php echo $value['purchase_no']; ?>
                        </option>
                    <?php }
                } ?>
            </select>
        </div>

        <div class="form-group col-xs-12 col-md-2">
            <label>Bill No </label>
            <select id="Bills_bill_number" class="select_box form-control" name="Bills[bill_number]">
                <option value="">Select Bill no</option>
                <?php
                if (!empty($bills_data)) {
                    foreach ($bills_data as $key => $value) {
                        ?>
                        <option value="<?php echo $value['bill_number'] ?>" <?php echo ($value['bill_number'] == $model->bill_number) ? 'selected' : ''; ?>>
                            <?php echo $value['bill_number']; ?>
                        </option>
                    <?php }
                } ?>
            </select>
        </div>
        <div class="form-group col-xs-12 col-md-2">
            <label>Company </label>
            <select id="Bills_company_id" class="select_box form-control" name="Bills[company_id]">
                <option value="">Select Company</option>
                <?php
                if (!empty($companyInfo)) {
                    foreach ($companyInfo as $key => $value) {
                        ?>
                        <option value="<?php echo $value['id'] ?>" <?php echo ($value['id'] == $model->company_id) ? 'selected' : ''; ?>>
                            <?php echo $value['name']; ?>
                        </option>
                    <?php }
                } ?>
            </select>
        </div>

        <div class="form-group col-xs-12 col-md-2">
        <label>Project </label>
            <select id="project_id" class="select_box form-control" name="project_id">
                <option value="">Select Project</option>
                <?php
                if (!empty($project_data)) {
                    foreach ($project_data as $key => $value) {
                        ?>
                        <option value="<?php echo $value['pid'] ?>" <?php echo ($value['pid'] == $model->project_id) ? 'selected' : ''; ?>>
                            <?php echo $value['name']; ?>
                        </option>
                    <?php }
                } ?>
            </select>
        </div>
        <div class="form-group col-xs-12 col-md-2">
            <label>Expense Head </label>
            <select style="padding: 2.5px 0px;" id="expense_head" class="select_box" name="expensehead_id" class="sel">
                <option value="">Select Expense Head</option>
                <?php
                if (!empty($expensehead)) {
                    foreach ($expensehead as $value) {
                        ?>
                        <option value="<?php echo $value['type_id']; ?>" <?php echo ($value['type_id'] == $model->expensehead_id) ? 'selected' : ''; ?>>
                            <?php echo $value['type_name']; ?>
                        </option>

                        <?php
                    }
                }
                ?>
            </select>
        </div>

        <div class="form-group col-xs-12 col-md-2">
            <label>Vendor </label>
            <select id="vendor_id" class="select_box form-control" name="vendor_id">
                <option value="">Select Vendor</option>
                <?php
                if (!empty($vendor_data)) {
                    foreach ($vendor_data as $key => $value) {
                        ?>
                        <option value="<?php echo $value['vendor_id'] ?>" <?php echo ($value['vendor_id'] == $model->vendor_id) ? 'selected' : ''; ?>>
                            <?php echo $value['name']; ?>
                        </option>
                    <?php }
                } ?>
            </select>
        </div>

        <?php
        if (!isset($_GET['date_from']) || $_GET['date_from'] == '') {
            // $datefrom = date("Y-m-") . "01";
            //$datefrom = date("Y-")."01-" . "01";
            //$datefrom = date('d-m-Y', strtotime('today - 30 days'));
            $datefrom = "";
        } else {
            $datefrom = $_GET['date_from'];
        }
        ?>
        <div class="form-group col-xs-12 col-md-2">
            <label class="text-nowrap small-label">Bill Date From :</label>
            <?php echo CHtml::textField('date_from', $datefrom, array('value' => $datefrom, 'readonly' => true, 'class' => 'form-control w-100')); ?>
        </div>
        <?php
        if (!isset($_GET['date_to']) || $_GET['date_to'] == '') {
            //$date_to = date("Y-m-d");
            //$date_to = date("d-m-Y");
            $date_to = "";
        } else {
            $date_to = $_GET['date_to'];
        }
        ?>
        <div class="form-group col-xs-12 col-md-2">
            <label class="text-nowrap small-label">Bill Date To :</label>
            <?php echo CHtml::textField('date_to', $date_to, array('value' => $date_to, 'readonly' => true, 'class' => 'form-control w-100')); ?>
        </div>

        <?php
        if (!isset($_GET['created_date_from']) || $_GET['created_date_from'] == '') {
            // $datefrom = date("Y-m-") . "01";
            //$datefrom = date("Y-")."01-" . "01";
            //$datefrom = date('d-m-Y', strtotime('today - 30 days'));
            $createddatefrom = "";
        } else {
            $createddatefrom = $_GET['created_date_from'];
        }
        ?>
        <div class="form-group col-xs-12 col-md-2">
            <label class="text-nowrap small-label">Created Date From :</label>
            <?php echo CHtml::textField('created_date_from', $createddatefrom, array('value' => $createddatefrom, 'readonly' => true, 'class' => 'form-control w-100')); ?>
        </div>
        <?php
        if (!isset($_GET['created_date_to']) || $_GET['created_date_to'] == '') {
            //$date_to = date("Y-m-d");
            //$date_to = date("d-m-Y");
            $createddate_to = "";
        } else {
            $createddate_to = $_GET['created_date_to'];
        }
        ?>
        <div class="form-group col-xs-12 col-md-2">
            <label class="text-nowrap small-label">Created Date To :</label>
            <?php echo CHtml::textField('created_date_to', $createddate_to, array('value' => $createddate_to, 'readonly' => true, 'class' => 'form-control w-100')); ?>
        </div>

        <div class="form-group col-xs-12 col-md-2 margin-top-20">
           <button type="submit" class="btn btn-sm btn-primary" name="yt0">Go</button>
            <button type="reset" class="btn btn-sm btn-default" id="reset">Reset</button>
        </div>
    </div>
</div>


    <?php $this->endWidget(); ?>

    <script>
        $('#reset').click(function () {
            location.href = '<?php echo $this->createUrl('admin'); ?>';
        })
        $(document).ready(function () {
            $(".select_box").select2();
            $(function () {
                $("#date_from").datepicker({
                    dateFormat: 'dd-mm-yy'
                });

                $("#date_to").datepicker({
                    dateFormat: 'dd-mm-yy'
                });

                $("#created_date_from").datepicker({
                    dateFormat: 'dd-mm-yy'
                });

                $("#created_date_to").datepicker({
                    dateFormat: 'dd-mm-yy'
                });

            });
            $("#date_from").change(function () {
                $("#date_to").datepicker('option', 'minDate', $(this).val());
            });
            $("#date_to").change(function () {
                $("#date_from").datepicker('option', 'maxDate', $(this).val());
            });
            $("#created_date_from").change(function () {
                $("#created_date_to").datepicker('option', 'minDate', $(this).val());
            });
            $("#created_date_to").change(function () {
                $("#created_date_from").datepicker('option', 'maxDate', $(this).val());
            });

        });
    </script>