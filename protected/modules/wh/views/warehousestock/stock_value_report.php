
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Clients',
);
$page = Yii::app()->request->getParam('Clients_page');
if($page != '') {
	Yii::app()->user->setReturnUrl($page);
} else {
	Yii::app()->user->setReturnUrl(0);
}
?>


<div class="container" id="project">
    <div class="clearfix">
        <!-- <div class="add-btn pull-right ">
            <?php /*if (Yii::app()->user->role == 1 ||  Yii::app()->user->role == 2) { ?>
             <a <?php echo Yii::app()->createAbsoluteurl('warehousestock/create') ?> class="button">Add Warehouse Stock</a>                
            <?php } */?>
        </div> -->
        <h3>Warehouse Stock Value Report</h3>
    </div> 
    <div id="addclient" style="display:none;">
    
     </div>
    <?php
//    $this->widget('zii.widgets.CListView', array(
//        'dataProvider' => $dataProvider,
//        'itemView' => '_view', 
//        'template' => '<div>{sorter}</div><div class=""><table cellpadding="10" id="myTable" class="table">{items}</table></div>',
//
//        //'template' => '<div>{sorter}</div><div class=""><table cellpadding="10" class="table" id="myTable">{items}</table></div>'
//        //'ajaxUpdate' => false,
//    ));
    ?>    
    <?php $this->renderPartial('_searchStockValueReport', array('model' => $model,'specification'=>$specification)); ?>
     <div class="warehousestock">
            <div id="warehousestock-entry">
                <?php $this->renderPartial('newlistStockValueReport',array('dataProvider'=>$dataProvider,)); ?>
            </div>
    </div>  
    
</div>




<script>
function closeaction(){
                 $('#addclient').slideUp('slow');
            }
$(document).ready(function () {
 
       
    $('.addclient').click(function () {
        // alert('hi');
        $.ajax({
        type: "POST",
        url: "<?php echo $this->createUrl('warehousestock/create&layout=1') ?>",
        success: function (response)
        {
        $("#addclient").html(response).slideDown();
        }
        });
    });
    $(document).delegate('.editstock','click', function() {        
        var id = $(this).attr('data-id');        
        $.ajax({
        type: "POST",
        url: "<?php echo $this->createUrl('warehousestock/update&id=') ?>" + id,
        success: function (response)
        {
            $("#addclient").html(response).slideDown();
            $("#warehouseStockId").val(id);
        }
        });
    });
    
    $(document).on('click','.deletestock',function(){
        var id = $(this).attr('data-id');        
        $.ajax({
        type: "POST",
        url: "<?php echo $this->createUrl('warehousestock/delete&id=') ?>" + id,
        success: function (result)
        {
            $("#addclient").slideUp();
            if(result == 1){
                
            }else{
                 $("#newlist").html(result);
                 $("#myTable").dataTable( {
                            "scrollY": "300px",
                            "scrollCollapse": true,
                            "paging": false,
                            "columnDefs": [ {
                                "targets": [0],
                                "searchable": false
                              },
                            ]
                        } );
            }
        }
        });
    });
    
    jQuery(function ($) {
        $('#client').on('keydown', function (event) {
            if (event.keyCode == 13) {
                $("#clientsearch").submit();
            }
        });
    });
});


</script>
        

<?php
Yii::app()->clientScript->registerScript('myjavascript', '
     $(document).ready(function(){
     ajaxdatatable();
    function ajaxdatatable(){
    
       $("#myTable").dataTable( {
       
            "scrollY": "300px",
            "scrollCollapse": true,
            "paging": false,
            
             "columnDefs": [ {
      "targets": [0],
      "searchable": false
    },
    //{ "bSortable": false, "aTargets": [-1]  }
    ]
                  
	} );
	
	
     }
        
	});
      
        
    ');
?>

<style>
    .panel-gray{box-shadow: 0px 3px 8px 0px rgba(0,0,0,0.25);}
    .panel{border:1px solid #ddd;}
    .panel-heading{background-color:#eee;height:40px; }
     .addRow label {
    display: inline-block;}
    input[type="radio"]{
        margin: 4px 4px 0;
    }
    .page-body h3{
        margin:4px 0px;
        color:inherit;
        text-align: left;
    }
    a.button {
        background-color: #6a8ec7;
        display: block;
        padding: 5px;
        color: #fff;
        cursor: pointer;
        float: right;
        border: 1px solid #6a8ec8;
    }
    .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover{
        color: #fff !important;
    }
    .dataTables_wrapper .dataTables_paginate .paginate_button:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active  {
        color:#fff !important;
    }
    .list-view .sorter {margin: 0px;}
    h4{
        background-color: #fafafa;
        padding: 8px;

    }
        .dataTables_wrapper.no-footer .dataTables_scrollBody {
    border-bottom: none;
}

table.dataTable{border-collapse: collapse;}

</style>

