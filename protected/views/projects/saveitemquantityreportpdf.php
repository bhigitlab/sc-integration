<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap-theme.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" />
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/tablestyles.css" />
<?php
Yii::app()->clientScript->registerScript('search', "");
?>
<div class="container" id="project">
    <div id="contenthtml" class="contentdiv">
        <h2 class="text-center">Item Estimation Report of <?php echo $model["name"]; ?></h2>
        <div class="container-fluid">
            <table class="table table-bordered ">
                <tr>
                    <td><b>Client : </b><?php echo $model["clientname"]; ?></td>
                </tr>
                <tr>
                    <td><b>Project Name : </b><?php echo $model["name"]; ?></td>
                </tr>
                <tr>
                    <td><b>Site : </b><?php echo $model["site"]; ?></td>
                </tr>
            </table>
            <table class="table table-bordered ">
                <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>Item Name</th>
                        <th>Unit</th>
                        <th>Type</th>
                        <th>Estimation Quantity</th>
                        <th>Estimation Amount</th>
                        <th>Actual Quantity</th>
                        <th>Actual Amount</th>
                        <th>Quantity Variation</th>
                        <th>Amount Variation</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $i                  = 1;
                $billItemGrandTotal = 0;
                foreach($itemmodelcat as $modelcat) {
                    $billitemTotal      = ($modelcat["billitemamount"] - $modelcat["billitemdiscountamount"]) + $modelcat["billitemtaxamount"];
                    $billItemGrandTotal = $billItemGrandTotal + $billitemTotal;
                    if($modelcat["purchase_type"] == "A") {
                        $itemType = "By Length";
                    } else if($modelcat["purchase_type"] == "G") {
                        $itemType = "By Width and height";
                    } else if($modelcat["purchase_type"] == "O") {
                        $itemType = "By Quantity";
                    } else {
                        $itemType = "";
                    }
                    $quantityVariation  = $modelcat["itemestimation_quantity"] - $modelcat["billitemquantity"];
                    $amountVariation    =  $modelcat["itemestimation_amount"] - $billitemTotal;
                    if($modelcat["itemestimation_quantity"] < $modelcat["billitemquantity"]) {
                        $style = " style='background:#DD1035; color:#FFFFFF'";
                    } else if($modelcat["itemestimation_quantity"] > $modelcat["billitemquantity"]) {
                        $style = " style='background:#006600; color:#FFFFFF'";
                    } else {
                        $style = "";
                    }
                ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $modelcat["category_name"]." - ".$modelcat["brand_name"]." - ".$modelcat["specification"]; ?></td>
                        <td><?php echo $modelcat["itemestimation_unit"]; ?></td>
                        <td><?php echo $itemType; ?></td>
                        <td><?php echo $modelcat["itemestimation_quantity"]; ?></td>
                        <td class="text-right"><?php echo $modelcat["itemestimation_amount"]; ?></td>
                        <td<?php echo $style; ?>><?php echo Controller::money_format_inr($modelcat["billitemquantity"],2); ?></td>
                        <td class="text-right"><?php echo Controller::money_format_inr($billitemTotal,2); ?></td>
                        <td><?php echo Controller::money_format_inr($quantityVariation,2); ?></td>
                        <td class="text-right"><?php echo Controller::money_format_inr($amountVariation,2); ?></td>
                    </tr>
                <?php
                    $i = $i + 1;
                }
                $totalAmountVariation = $itemmodelcat[0]["itemestimationsum"] - $billItemGrandTotal;
                ?>
                <tbody>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th class="text-right"><?php echo $itemmodelcat[0]["itemestimationsum"]; ?></th>
                        <th></th>
                        <th class="text-right"><?php echo Controller::money_format_inr($billItemGrandTotal,2); ?></th>
                        <th></th>
                        <th class="text-right"><?php echo Controller::money_format_inr($totalAmountVariation,2); ?></th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>