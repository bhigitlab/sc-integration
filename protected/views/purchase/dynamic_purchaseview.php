<?php
$base_url = Yii::app()->theme->baseUrl;
$terms = Yii::app()->db->createCommand("SELECT * FROM `jp_terms_conditions` WHERE `template_id` = 1")->queryRow();
$terms_and_conditions = $terms['terms_and_conditions'];
$company_details = Company::model()->findByPk($model->company_id);
$logo = '<img src="' . Yii::app()->request->baseUrl . '/uploads/image/' . $company_details['logo'] . '" alt="" class="pop" modal-src="' . Yii::app()->request->baseUrl . '/uploads/image/' . $company_details['logo'] . '" style="max-height:45px;">';
$phone=$company_details['phone'];
$company = $company_details['name'];
$company_email = $company_details['email_id'];
$company_address = $company_details['address'];
$company_gst = $company_details['company_gstnum'];
$company_bank = $company_details['bank_details'];
$company_bank = str_replace('.','<br>',$company_bank);
$tax_in_words = Yii::app()->Controller->getDisplaywords($item['tot_tax']);
$project = isset($project->name)?$project->name:'';
$expense_head=isset($expense_head->type_name)?$expense_head->type_name : '';
$vendor=isset($vendor->name) ? $vendor->name : ''; 
$delivery_address=isset($model->shipping_address) ? nl2br($model->shipping_address) : '-';
$description=isset($model->purchase_description) ? nl2br($model->purchase_description) : '-'; 
$client_address = isset($client->address)?$client->address:'';
$client_gst = isset($client->gst_no)?$client->gst_no:'';
$client = isset($client->name)?$client->name:'';
$date = isset($model->expected_delivery_date)?date('d-m-Y', strtotime($model->expected_delivery_date)):'' ;
$purchase_number = isset($model->purchase_no)?$model->purchase_no:'';
$discount_total = $tax_total_amount = $item_total = 0;
// $payment_stage = PaymentStage::model()->findByPk($model->stage_id)['payment_stage'];
// $amount = Controller::money_format_inr(($model->amount + $item['tot_tax']),2,1);
$app_name = Yii::app()->name;
$item_list = '';
$total_sgst = $total_cgst = $total_igst = 0;

if (!empty($itemmodel)) {
   
    foreach ($itemmodel as $category_id => $category) {
        $category_title = $category['category_title'];
        foreach ($category as $index => $item) {
            if (!is_array($item)) {
                continue;
            }
            $item_list .= '<tr class="tr_class">';
            $item_list .= '<td>' . ($index + 1) . '</td>';
            $item_list .= '<td>' . $item['specification'] . '</td>';
            $item_list .= '<td>' . $item['quantity'] . '</td>';
            $item_list .= '<td>' . $item['unit'] . '</td>';
            $item_list .= '<td>' . $item['rate'] . '</td>';
            $item_list .= '<td>' . $item['hsn_code'] . '</td>';
            $item_list .= '<td>' . number_format($item['sgst'], 2) . '</td>';
            $total_sgst += $item['sgst'];
            $item_list .= '<td>' . number_format($item['cgst'], 2) . '</td>';
            $total_cgst += $item['cgst'];
            $item_list .= '<td>' . number_format($item['igst'], 2) . '</td>';
            $total_igst += $item['igst'];
            $item_list .= '<td>' . number_format($item['amount'], 2) . '</td>';
            $item_list .= '<td>' . number_format($item['tot_tax'], 2) . '</td>';
            $item_list .= '</tr>';
            $discount_total += $item['disc'];
            $tax_total_amount += $item['tot_tax'];
            $total_amt =  ($item['amount'] + $item['tot_tax']) - $item['disc'];
            $item_total += $total_amt;
        }
    }
}

$total_sgst = isset($total_sgst) ? number_format($total_sgst, 2) : 0;
$total_cgst = isset($total_cgst) ? number_format($total_cgst, 2) : 0;
$total_igst = isset($total_igst) ? number_format($total_igst, 2) : 0;

$grand_total=isset($model->total_amount) ? Yii::app()->Controller->money_format_inr($model->getPurchaseTotalAmount($model['p_id']), 2, 1) : '';
$total_amount=  Yii::app()->Controller->money_format_inr($item_total, 2, 1);
// $total_amount = Controller::money_format_inr($item['amount']+$item['tot_tax'], 2,1);
$total_tax = Controller::money_format_inr($item['tot_tax'], 2,1);
// $grand_total = Controller::money_format_inr($item['tot_tax']+$item['amount'],2,1);
$modifiedTemplate = preg_replace('/\[\[ITEMTEMPLATE(.*?)ENDITEMTEMPLATE\]\]/s', '', $pdf_data);
$pattern = '/\[\[ITEMTEMPLATE(.*?)ENDITEMTEMPLATE\]\]/s';
            if (preg_match($pattern, $pdf_data, $matches)) {
                $extractedContent = $matches[1];
            } else {
                $extractedContent = '';
            }
$replacements = array();
$resultItem = '';
           
if (!empty($itemmodel)) {
    $i = 0;
    foreach ($itemmodel as $category_id => $category) {
        $category_title = $category['category_title'];

        // Iterate through each item in the category
        foreach ($category as $index => $item) {
            if (!is_array($item)) {
                continue;
            }
            $i++;
            $tax_amount=$item['amount']*$item['tax_slab']/100;
            $amount_with_tax=$item['tot_tax'] + $item['amount'];
            $replacements = array(
                '{slno}' => $i,
                '{specification}' => !empty($item['specification']) ? $item['specification'] : '',
                '{quantity}' => !empty($item['quantity']) ? $item['quantity'] : '',
                '{unit}' => !empty($item['unit']) ? $item['unit'] : '',
                '{hsn_code}' => !empty($item['hsn_code']) ? $item['hsn_code'] : '',
                '{rate}' => !empty($item['rate']) ? $item['rate'] : '',
                '{tax_slab}' => !empty($item['tax_slab']) ? $item['tax_slab'] : '',
                // '{sgst}' => !empty($item['sgstp']) ? number_format($item['sgstp'], 2) : '',
                // '{sgst_amount}' => !empty($item['sgst']) ? Controller::money_format_inr($item['sgst'], 2, 1) : '',
                // '{cgst}' => !empty($item['cgstp']) ? number_format($item['cgstp'], 2) : '',
                // '{cgst_amount}' => !empty($item['cgst']) ? Controller::money_format_inr($item['cgst'], 2, 1) : '',
                // '{igst}' => !empty($item['igstp']) ? number_format($item['igstp'], 2) : '',
                // '{igst_amount}' => !empty($item['igst']) ? Controller::money_format_inr($item['igst'], 2, 1) : '',
                '{discount}' => !empty($item['discp']) ? number_format($item['discp'], 2 ,1) : '',
                '{discount_amount}' => !empty($item['disc']) ? Controller::money_format_inr($item['disc'], 2, 1) : '',
                '{amount}' => !empty($item['amount']) ? Controller::money_format_inr($item['amount']) : '',
                '{tax_amount}' => !empty($item['tot_tax']) ? Controller::money_format_inr($item['tot_tax'], 2, 1) : '',
                '{amount_with_tax}' => !empty($item['amount']) ? Controller::money_format_inr($amount_with_tax,2,1) : '','{category_title}' => $category_title,'{date}'=>$date,
                '{taxable_amount}' => !empty($tax_amount) ? Controller::money_format_inr($tax_amount, 2, 1) : '',
            );

            if ($model->type == 'quantity_rate') {
                $replacements = array_merge($replacements, array(
                    '{quantity}' => !empty($item['quantity']) ? number_format($item['quantity'], 2) : '',
                    '{unit}' => !empty($item['unit']) ? $item['unit'] : '',
                    '{rate}' => !empty($item['rate']) ? number_format($item['rate'], 2) : '',
                ));
            }
           
            $html_string = $this->replaceItemlistWithCondition($extractedContent, $replacements);
           
            $itemRow = str_replace(array_keys($replacements), array_values($replacements), $html_string);
            $resultItem .= $itemRow;
        }
    }
}

$pattern = '/{image::\s*([^|]+)\|\|width:\'(\d+px;)\'}/';

//$path = Yii::app()->request->baseUrl . '/uploads/company_assets/media/';
$path = Yii::app()->basePath . '/../uploads/company_assets/media/';
$modifiedPdfData = preg_replace_callback($pattern, function($matches) use ($path) {
    $image_name = $matches[1];
    $width_value = $matches[2];

    $replacement_image = "<img src='" . $path . "$image_name' alt='$image_name' class='dynamic_media_image' style='width:$width_value;'>";
    return $replacement_image;
}, $modifiedTemplate);
$pdf_data = str_replace(
    array('{base_url}','{category_title}','{date}','{purchase_no}','{vendor}','{expense_head}','{phone}','{company_gst}','{delivery_address}','{description}','{terms_and_conditions}','{logo}','{item_list}','{dynamic_item_list}','{company}','{project}','{client}','{client_address}','{tax_in_words}','{company_email}','{company_address}','{sgst_amount}','{cgst_amount}','{igst_amount}','{total_amount}','{grand_total}',
   ),
    array($base_url,$category_title,$date,$purchase_number,$vendor,$expense_head,$phone,$company_gst,$delivery_address,$description,$terms_and_conditions,$logo,$item_list,$resultItem,$company,$project,$client,$client_address,$tax_in_words,$company_email,$company_address,$total_sgst,$total_cgst,$total_igst,$total_amount,$grand_total
), $modifiedPdfData);
// echo '<pre>';print_r($pdf_data);exit;
$str = html_entity_decode($pdf_data, ENT_QUOTES, 'UTF-8');
eval('?>'.$str.'');
?>

