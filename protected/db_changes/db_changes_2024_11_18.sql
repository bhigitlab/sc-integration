ALTER TABLE `jp_clients` ADD client_mapping_id INT(11) NOT NULL DEFAULT '0' AFTER `pms_client_id`;

ALTER TABLE jp_clients CHANGE project_type project_type SMALLINT(6) NULL;
ALTER TABLE jp_projects CHANGE start_date start_date DATE NULL;

ALTER TABLE jp_projects ADD created_from INT NOT NULL DEFAULT '1' COMMENT '1=>accounts,2=>pms,3=>crm' AFTER pms_project_id;
ALTER TABLE jp_clients ADD created_from INT NOT NULL DEFAULT '1' COMMENT '1=>accounts,2=>pms,3=>crm' AFTER pms_client_id;

ALTER TABLE jp_api_settings ADD active_modules VARCHAR(200)  NULL AFTER api_integration_settings;