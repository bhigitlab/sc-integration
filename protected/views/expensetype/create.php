<?php
/* @var $this ExpenseTypeController */
/* @var $model ExpenseType */

$this->breadcrumbs=array(
	'Expense Types'=>array('index'),
	'Create',
);

?>

<div class="entries-wrapper">
    <div class="row">
                <div class="col-xs-12">
                    <div class="heading-title">Add Expense Head </div>
                    <div class="dotted-line"></div>
                </div>
    </div>

        <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
