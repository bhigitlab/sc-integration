<?php
/* @var $this QuotationCategoryMasterController */
/* @var $model QuotationCategoryMaster */

$this->breadcrumbs = array(
    'Location' => array('index'),
    'Manage',
);
?>
<style>
    .greytable {
        width: 100%;
        cellpadding: 0;
        border-collapse: collapse;
    }

    .greytable thead tr:first-child {
        background: #6e6f72;
        color: #fff;
    }

    .greytable thead tr:first-child a:link {
        color: #fff;
    }

    .greytable>tbody tr {
        background: #fff;
        color: #828282;
    }

    .greytable>tbody tr:nth-child(2n) {
        background: #ededed;
    }

    .greytable td,
    th {
        padding: 8px 6px;
    }

    .greytable th {
        color: #fff;
    }

    .greytable tbody th {
        color: #828282;
    }

    .greytable,
    .greytable th,
    .greytable td {
        border: 1px solid #d8d8d8;
    }

    .greytable td.highlight {
        background: #555;
        color: #fff;
    }

    div.flash-success
    {
        background:#E6EFC2;
        color:#264409;
        border-color:#C6D880;
    }
</style>
<div class="alert alert-success" role="alert" style="display:none;">
</div>
<div class="alert alert-danger" role="alert" style="display:none;">
</div>
<div class="alert alert-warning" role="alert" style="display:none;"></div>
<div class="container">
    <?php if (Yii::app()->user->hasFlash('success')) : ?>
        <div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: green;font-size: 20px;font-weight: bold;">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>

    <?php if (Yii::app()->user->hasFlash('error')) : ?>
        <div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: red;font-size: 20px;font-weight: bold;">
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-4">
            <h4><?php echo $formmodel->isNewRecord ? 'Add' : 'Update' ?> Location</h4>
            <?php echo $this->renderPartial('_form', array('model' => $formmodel)); ?>
        </div>
        <div class="col-md-8">
            <h4>Location List</h4>
            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'location-master-grid',
                'dataProvider' => $model->search(),
                'itemsCssClass' => 'greytable',
                'afterAjaxUpdate' => 'true',
                'pager' => array(
                    'id' => 'dataTables-example_paginate', 'header' => '', 'prevPageLabel' => '<<',
                    'nextPageLabel' => '>>'
                ),
                'filter' => $model,
                'columns' => array(
                    array(
                        'value' => '$data->id',
                        'headerHtmlOptions' => array('style' => 'display:none'),
                        'htmlOptions' => array('style' => 'display:none', 'class' => 'rowId'),
                        'filterHtmlOptions' => array('style' => 'display:none'),
                    ),
                    array('class' => 'IndexColumn', 'header' => 'Sl.No.', 'htmlOptions' => array('class' => 'snocol')),
                    'name',
                    array(
                        'class' => 'CButtonColumn',
                        'template' => '{update}<span class="fa fa-trash deleteSpecification" onclick="deleteaction(this)"></span>',
                        'htmlOptions' => array('width' => '100px', 'style' => 'font-weight: bold;text-align:center', 'class' => 'indexactionbtncolmn'),
                        'buttons' => array(
                            'update' => array(
                                'url' => 'Yii::app()->createUrl("location/location/index", array("id"=>$data->id))',
                            ),
                        ),
                    ),
                ),
            ));
            ?>
        </div>
    </div>
</div>
<script type="text/javascript">
  function deleteaction(elem) {
      var rowid = $(elem).closest("tr").find(".rowId").text();
      var url = "<?php echo $this->createUrl('location/delete&id=') ?>" + rowid;
      var answer = confirm("Are you sure you want to delete?");
      if (answer) {
            $.ajax({
		type: "POST",
		dataType: "json",
		url: url,
		success: function(response) {
		$('html, body').animate({
                scrollTop: $("#content").position().top - 100
                }, 500);
                $.fn.yiiGridView.update('location-master-grid');
                if (response.response == "success") {
                        $(".alert-success").show().html(response.msg).delay(3000).fadeOut();
                } else if (response.response == "warning") {
                        $(".alert-warning").show().html(response.msg).delay(3000).fadeOut();
                } else if(response.response == "error"){
                        $(".alert-danger").show().html(response.msg).delay(3000).fadeOut();
		}
		}
                });
		}
	return false;
	};
 </script>
