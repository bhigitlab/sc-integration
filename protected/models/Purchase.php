<?php

/**
 * This is the model class for table "{{purchase}}".
 *
 * The followings are the available columns in table '{{purchase}}':
 * @property integer $p_id
 * @property integer $purchase_no
 * @property integer $vendor_id
 * @property integer $project_id
 * @property double $sub_total
 * @property double $total_amount
 * @property double $unbilled_amount
 * @property integer $created_by
 * @property string $created_date
 */
class Purchase extends CActiveRecord
{
	public $date_from;
	public $date_to;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{purchase}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('purchase_no, vendor_id, project_id, sub_total, '
				. 'total_amount, purchase_date,'
				. ', created_by, created_date', 'required'),
			array('created_by', 'numerical', 'integerOnly' => true),
			array('contact_no','match', 'pattern' => '{^\+?[0-9-]+$}',
            'message' => 'Invalid phone number'),
			array('sub_total, total_amount', 'numerical'),
			array('purchase_no', 'unique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('p_id, purchase_no, vendor_id, project_id, expensehead_id, '
				. 'sub_total, total_amount, po_companyid, created_by, created_date, '
				. 'permission_status, unbilled_amount', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
			'vendor' => array(self::BELONGS_TO, 'Vendors', 'vendor_id'),
			'expensehead' => array(self::BELONGS_TO, 'ExpenseType', 'expensehead_id'),
			'status' => array(self::BELONGS_TO, 'Status', 'purchase_billing_status'),
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'p_id' => 'P',
			'purchase_no' => 'Purchase No',
			'vendor_id' => 'Vendor',
			'project_id' => 'Project',
			'sub_total' => 'Sub Total',
			'total_amount' => 'Total Amount',
			'purchase_date' => 'Date',
			'contact_no' => 'Contact No',
			'shipping_address' => 'Shipping address',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'expensehead_id' => 'Expense Head',
			'unbilled_amount' => 'Unbilled Amount',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched. 

		$criteria = new CDbCriteria;

		$criteria->compare('p_id', $this->p_id);
		$criteria->compare('purchase_no', $this->purchase_no);
		$criteria->compare('vendor_id', $this->vendor_id);
		$criteria->compare('project_id', $this->project_id);
		$criteria->compare('expensehead_id', $this->expensehead_id);
		$criteria->compare('purchase_status', $this->purchase_status);
		$criteria->compare('purchase_billing_status', $this->purchase_billing_status);
		$criteria->compare('sub_total', $this->sub_total);
		$criteria->compare('purchase_date', $this->purchase_date);
		$criteria->compare('total_amount', $this->total_amount);
		$criteria->compare('po_companyid', $this->po_companyid);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('company_id', $this->company_id);
		$criteria->addCondition("type ='po'");
		$criteria->compare('permission_status', $this->permission_status);
		$criteria->compare('unbilled_amount', $this->unbilled_amount);
		$criteria->addCondition("purchase_no IS NOT NULL");

		if (!empty($this->date_from) && empty($this->date_to)) {
			$criteria->addCondition("purchase_date >= '" . date('Y-m-d', strtotime($this->date_from)) . "'");  // date is database date column field
		} elseif (!empty($this->date_to) && empty($this->date_from)) {
			$criteria->addCondition("purchase_date <= '" . date('Y-m-d', strtotime($this->date_to)) . "'");
		} elseif (!empty($this->date_to) && !empty($this->date_from)) {
			$criteria->addCondition("purchase_date between '" . date('Y-m-d', strtotime($this->date_from)) . "' and  '" . date('Y-m-d', strtotime($this->date_to)) . "'");
		}

		// if(isset($this->date_from) && isset($this->date_to) && $this->date_to != '' && $this->date_from != '') {
		//             $criteria->addCondition("purchase_date >= '".date('Y-m-d',strtotime($this->date_from))."'"); 
		//             $criteria->addCondition("purchase_date <= '".date('Y-m-d',strtotime($this->date_to))."'"); 
		//         }else{
		//             $datefrom = date("Y-")."01-" . "01";
		//             $date_to = date("Y-m-d");
		//             $criteria->addCondition("purchase_date >= '".date('Y-m-d',strtotime($datefrom))."'"); 
		//             $criteria->addCondition("purchase_date <= '".date('Y-m-d',strtotime($date_to))."'"); 
		//         }
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		foreach ($arrVal as $arr) {
			if ($newQuery) $newQuery .= ' OR';
			$newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
		}
		$newQuery1 = "project_id IN (SELECT projectid FROM jp_project_assign WHERE userid = " . Yii::app()->user->id . ")";
		$criteria->addCondition($newQuery);
		$criteria->addCondition($newQuery1);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'sort' => array('defaultOrder' => 'p_id DESC',),
			'pagination' => array('pageSize' => 20,),
			// 'pagination'=>false,

		));
	}
	public function searchtest()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('p_id', $this->p_id);
		$criteria->compare('purchase_no', $this->purchase_no);
		$criteria->compare('vendor_id', $this->vendor_id);
		$criteria->compare('project_id', $this->project_id);
		$criteria->compare('expensehead_id', $this->expensehead_id);
		$criteria->compare('purchase_status', $this->purchase_status);
		$criteria->compare('purchase_billing_status', $this->purchase_billing_status);
		$criteria->compare('sub_total', $this->sub_total);
		$criteria->compare('purchase_date', $this->purchase_date);
		$criteria->compare('total_amount', $this->total_amount);
		$criteria->compare('po_companyid', $this->po_companyid);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('company_id', $this->company_id);
		$criteria->addCondition("type ='po'");
		$criteria->compare('permission_status', $this->permission_status);
		$criteria->compare('unbilled_amount', $this->unbilled_amount);
		$criteria->addCondition("purchase_no IS NOT NULL");
		if (isset($this->date_from) && isset($this->date_to) && $this->date_to != '' && $this->date_from != '') {
			$criteria->addCondition("purchase_date >= '" . date('Y-m-d', strtotime($this->date_from)) . "'");
			$criteria->addCondition("purchase_date <= '" . date('Y-m-d', strtotime($this->date_to)) . "'");
		} else {
			$datefrom = date('Y-m-d', strtotime('today - 30 days'));
			$date_to = date("Y-m-d");
			$criteria->addCondition("purchase_date >= '" . date('Y-m-d', strtotime($datefrom)) . "'");
			$criteria->addCondition("purchase_date <= '" . date('Y-m-d', strtotime($date_to)) . "'");
		}
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		foreach ($arrVal as $arr) {
			if ($newQuery) $newQuery .= ' OR';
			$newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
		}
		$newQuery1 = "project_id IN (SELECT projectid FROM jp_project_assign WHERE userid = " . Yii::app()->user->id . ")";
		$criteria->addCondition($newQuery);
		$criteria->addCondition($newQuery1);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'sort' => array('defaultOrder' => 'p_id DESC',),
			'pagination' => array('pageSize' => 20,),
			//'pagination'=>false,

		));
		//return new SqlDataProvider( array('sql' => $sql));
	}

	public function getapprovepo()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('p_id', $this->p_id);
		$criteria->compare('purchase_no', $this->purchase_no);
		$criteria->compare('vendor_id', $this->vendor_id);
		$criteria->compare('project_id', $this->project_id);
		$criteria->compare('expensehead_id', $this->expensehead_id);
		$criteria->compare('purchase_status', $this->purchase_status);
		$criteria->compare('purchase_billing_status', $this->purchase_billing_status);
		$criteria->compare('sub_total', $this->sub_total);
		$criteria->compare('purchase_date', $this->purchase_date);
		$criteria->compare('total_amount', $this->total_amount);
		$criteria->compare('po_companyid', $this->po_companyid);
		$criteria->compare('created_by', $this->created_by);
		// $criteria->compare('created_date',$this->created_date);
		$criteria->compare('company_id', $this->company_id);
		// $criteria->compare('unbilled_amount', $this->unbilled_amount);
		$criteria->addCondition("type ='po'");
		
		$criteria->addCondition("purchase_no IS NOT NULL");
		$newQuery1 = "permission_status ='No' ";
		
		$criteria->addCondition($newQuery1);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'sort' => array('defaultOrder' => 'p_id DESC',),
			'pagination' => array('pageSize' => 20,),
			//'pagination'=>false,

		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Purchase the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getloopbalanceamount($pid, $total_amount, $loop_billed_amount)
	{
		$tblpx 	 = Yii::app()->db->tablePrefix;
		$value = Yii::app()->db->createCommand("SELECT item_id, quantity, rate FROM {$tblpx}purchase_items WHERE purchase_id=" . $pid)->queryAll();
		$max = array();
		$blanc_amount = '';
		$loop_blance_amount = 'dd';
		$tvalue = 0;
		if (!empty($value)) {
			foreach ($value as $values) {
				$bill_item = Yii::app()->db->createCommand("SELECT sum(billitem_quantity) as billitem_quantity FROM {$tblpx}billitem WHERE purchaseitem_id=" . $values['item_id'])->queryRow();


				if (!empty($bill_item)) {
					$blance_quantity = $values['quantity'] - $bill_item['billitem_quantity'];
					$max = $blance_quantity * $values['rate'];
					$blanc_amount += $max;
				} else {
					$blanc_amount += $values['quantity'] * $values['rate'];
				}
			}


			if ($loop_billed_amount == $total_amount) {
				$loop_blance_amount = '0.00';
			} else {
				$loop_blance_amount = $blanc_amount;
			}
		} else {
			$loop_blance_amount = '0.00';
		}
		return $loop_blance_amount;
	}

	public function getTotalBalanceAmountDataprovider($datas)
	{
		$balance_amount_to_bill = 0;
		foreach ($datas as $data) {
			$tax_value = $discount_value = 0;
			$purchase_items = PurchaseItems::model()->findAll(array('condition' => 'purchase_id = ' . $data['p_id']));
			$balance_amount = 0;
			foreach ($purchase_items as $p_items) {
				if (!empty($p_items['tax_amount'])) {
					$tax_value += ($p_items['cgst_amount'] + $p_items['igst_amount'] + $p_items['sgst_amount']);
				}
				if (!empty($p_items['discount_amount'])) {
					$discount_value += $p_items['discount_amount'];
				}
				$billed_quanity = Billitem::model()->find(array(
					'select' => 'sum(billitem_quantity) as billitem_quantity',
					'condition' => 'purchaseitem_id=:purchaseitem_id',
					'params' => array(':purchaseitem_id' => $p_items['item_id'])
				));
				if (!empty($billed_quanity['billitem_quantity'])) {
					$balance_quantity = $p_items['quantity'] - $billed_quanity['billitem_quantity'];
					$balance_amount +=  $balance_quantity * $p_items['rate'];
				} else {
					$balance_amount += $p_items['quantity'] * $p_items['rate'];
				}
			}
			$data->total_amount = ($data->total_amount + $tax_value) - $discount_value;
			$bill_amount = Bills::model()->find(array(
				'select' => 'SUM(bill_totalamount) as bill_totalamount,sum(bill_additionalcharge)  as bill_additionalcharge',
				'condition' => 'purchase_id=:purchase_id',
				'params' => array(':purchase_id' => $data['p_id'])
			));
			$bill_total_amount = $bill_amount['bill_totalamount'] + $bill_amount['bill_additionalcharge'];
			if ($data->status->caption == "Pending to be Billed" || $data->status->caption == 'PO not issued') {

				$balance_amount = $data->total_amount;
			} elseif (($bill_total_amount == $data->total_amount) || ($bill_total_amount > $data->total_amount)) {
				$balance_amount = 0;
			} else {
				$balance_amount = $balance_amount;
			}

			$balance_amount_to_bill +=	$balance_amount;
		}
		return $balance_amount_to_bill;
	}

	public function getPurchaseTotalAmount($purchase_id, $type = false, $type_name = null) //$type =1 =>only exclue tax and discount amount
	{
		$items = PurchaseItems::model()->findAll(array('condition' => 'purchase_id = ' . $purchase_id));
		$item_amount = $tax_amount = $discount_amount = $total_amount = 0;
		foreach ($items as $item) {
			if ($type_name == "A") {

				$item_amount += ($item['item_length'] * $item['quantity'] * $item['rate']);
				$tax_amount += $item['cgst_amount'] + $item['sgst_amount'] + $item['igst_amount'];
				$discount_amount += $item['discount_amount'];
			} else if ($type_name == "G") {
				$item_amount += ($item['item_width'] * $item['item_height'] * $item['quantity'] * $item['rate']);
				$tax_amount += $item['cgst_amount'] + $item['sgst_amount'] + $item['igst_amount'];
				$discount_amount += $item['discount_amount'];
			} else {
				$item_amount += ($item['quantity'] * $item['rate']);
				$tax_amount += $item['cgst_amount'] + $item['sgst_amount'] + $item['igst_amount'];
				$discount_amount += $item['discount_amount'];
			}
		}
		if (isset($type) && $type == 1) {
			$total_amount = $item_amount;
		} else {
			$total_amount = $item_amount + $tax_amount - $discount_amount;
		}
		return $total_amount;
	}
}
