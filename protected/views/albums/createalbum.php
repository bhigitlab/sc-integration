<div class="modal-dialog modal-md" id="modal-album">
    <div class="modal-content">
       <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
			<h4 class="modal-title">Add Album</h4>
		</div> 
        

<?php
/* @var $this AlbumsController */
/* @var $model Albums */
/* @var $form CActiveForm */
?>

<div class="modal-body">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
       'id'=>'albums-form',
        'enableAjaxValidation' => true,'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,'validateOnClick' => true,
            'validateOnType' => true,),
    ));
    ?>
   

<div class="clearfix">
    <div class="row addRow">
        <div class="col-md-12">
            <?php echo $form->labelEx($model, 'title'); ?>
            <?php echo $form->textField($model, 'title', array('size' => 66, 'maxlength' => 100)); ?>
            <?php echo $form->error($model, 'title'); ?>
        </div>
        <input type="hidden" id="Albums_projectid" name="Albums[projectid]" value="<?php echo isset($_POST['project']) ? $_POST['project'] : ''; ?>"/>
        <div class="col-md-12">
            <?php echo $form->labelEx($model, 'description'); ?>
            <?php echo $form->textArea($model, 'description'); ?>
            <?php echo $form->error($model, 'description'); ?>
        </div>
        
        
 </div>
   <br />
    <div class="modal-footer save-btnHold">
        <?php echo CHtml::Button($model->isNewRecord ? 'Create' : 'Save',array('id' => 'album','name' => 'createalbum')); ?> 
       
        <button data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
    </div>

    <?php $this->endWidget(); ?>
</div>
</div><!-- form -->

    </div>

</div>
<script>
$(document).ready(function() {
    $('#album').click(function () {
        var title = $('#Albums_title').val();
        var desc = $('#Albums_description').val();
        var project = $('#Albums_projectid').val();
        $.ajax({
           type: "POST",
           data: 'project='+project+'&title='+title+'&desc='+desc,
           url: "<?php echo $this->createUrl('albums/savealbum') ?>",
           success: function (response)
           {
               if(response == 'success'){
                   $('.modal-backdrop').remove();
                   $('#addAlbum').html('');
                   //alert(title);
                    $.ajax({
                       type: 'POST',
                       dataType :'json',
                       data:'id='+ project+'&name='+title,
                       //url: "<?php echo $this->createUrl('/ImageGallery/getalbumbyprojectid') ?>",
                       url: "<?php echo $this->createUrl('/ImageGallery/getalbumname') ?>",
                        success: function (data)
                       {
                           $("#ImageGallery_albumid").html("");
                           $("#ImageGallery_albumid").html(data.albums);
                       }
                   });
               }else{
                   $("#modal-album").html(response);
               }
            }
       });
   });
});
</script>

