<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>

<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'pms-acc-wpr-item-consumed-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <div class="panel panel-gray">
        <div class="panel-heading form-head">
            <h3 class="panel-title">Add Consumption Request</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="project">Project</label>
                        <?php
                        echo $form->dropDownList($model, 'coms_project_id', CHtml::listData(Projects::model()->findAll(array(
                            'select' => array('pid, name'),
                            'order' => 'name ASC',
                        )), 'pid', 'name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Project-', 'style' => 'width:100%;'));
                        ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="warehouse">Warehouse</label>
                        <?php
                        echo $form->dropDownList($model, 'warehouse_id', CHtml::listData(Warehouse::model()->findAll(array(
                            'select' => array('warehouse_id, warehouse_name'),
                            'order' => 'warehouse_name ASC',
                        )), 'warehouse_id', 'warehouse_name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Warehouse-', 'style' => 'width:100%;'));
                        ?>
                    </div>
                </div>
            </div>

            <!-- Second Row (with Add and Remove buttons) -->
            <div id="dynamic-rows">
                <div class="row dynamic-row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'coms_material_id'); ?>
                            <?php
                            echo $form->dropDownList($model, 'coms_material_id[]', CHtml::listData(Materials::model()->findAll(array(
                                'select' => array('id, material_name'),
                                'order' => 'material_name ASC',
                            )), 'id', 'material_name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Material-', 'style' => 'width:100%;'));
                            ?>
                            <?php echo $form->error($model, 'coms_material_id'); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'item_id'); ?>
                            <?php
                            echo $form->dropDownList($model, 'item_id[]', CHtml::listData(Specification::model()->findAll(array(
                                'select' => array('id, specification'),
                                'order' => 'specification ASC',
                            )), 'id', 'specification'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Specification-', 'style' => 'width:100%;'));
                            ?>
                            <?php echo $form->error($model, 'item_id'); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'item_rate'); ?>
                            <?php 
                            $itemRates = array('' => 'Choose Rate');
                            if (!empty($rates)) {
                                foreach ($rates as $rate) {
                                    $formattedRate = number_format((float)$rate['rate'], 2, '.', '');
                                    $itemRates[$formattedRate] = $formattedRate;
                                }
                            }
                            echo $form->dropDownList($model, 'item_rate[]', $itemRates, array(
                                'class' => 'form-control'
                            ));
                            ?>
                            <?php echo $form->error($model, 'item_rate'); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'item_qty'); ?>
                            <?php echo $form->textField($model, 'item_qty[]', array('class' => 'form-control')); ?>
                            <?php echo $form->error($model, 'item_qty'); ?>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <label>&nbsp;</label><br>
                        <button type="button" class="btn btn-primary btn-sm add-row">+</button>
                        <button type="button" class="btn btn-danger btn-sm remove-row" style="display: none;">-</button>
                    </div>
                </div>
            </div>

            <!-- Save Button -->
            <div class="row">
                <div class="col-md-4 save-btnHold">
                    <label style="display:block;">&nbsp;</label>
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info btn-sm submit', 'id' => 'submitButton')); ?>
                    <?php
                    if (!$model->isNewRecord) {
                        echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-sm'));
                    } else {
                        echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;', 'class' => 'btn btn-default btn-sm'));
                        echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-sm'));
                    }
                    ?>
                </div>
            </div>

        </div>
    </div>

    <?php $this->endWidget(); ?>
</div>



<script>
$(document).ready(function() {
    // Add new row
    $(document).on('click', '.add-row', function() {
        var $row = $(this).closest('.dynamic-row');
        var $clone = $row.clone();
        $clone.find('input, select').val('');
        $clone.find('.remove-row').show();
        $clone.find('.add-row').remove(); 
        $('#dynamic-rows').append($clone);
    });

    // Remove row
    $(document).on('click', '.remove-row', function() {
        $(this).closest('.dynamic-row').remove(); // Remove the row
    });
});
    $(".js-example-basic-single").select2();
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2({
            dropdownParent: $('#myModal'),
        });
    });
    $(document).ready(function(){
        $('#PmsAccWprItemConsumed_coms_project_id').change(function(){
            var projectId = $(this).val();
            $.ajax({
            type: 'POST',
            url: '<?php echo $this->createUrl('warehouse/GetWarehouses'); ?>',
            data: {projectId: projectId},
            dataType: 'json',
            success: function(response){
                if (response.status === 'success') {
                    var warehouses = response.warehouses;
                        var dropdown = $('#PmsAccWprItemConsumed_warehouse_id');
                        dropdown.empty();
                        dropdown.append('<option value="">-Select Warehouse-</option>');
                        warehouses.forEach(function(warehouse) {
                            dropdown.append('<option value="' + warehouse.id + '">' + warehouse.name + '</option>');
                        });
                } else {
                    // Handle error if needed
                }
            },
            error: function(){
                // Handle error
            }
            });
        });
        $('#PmsAccWprItemConsumed_coms_material_id').change(function(){
            var material_id = $(this).val();
            $.ajax({
            type: 'POST',
            url: '<?php echo $this->createUrl('warehouse/getSpecifications'); ?>',
            data: {material_id: material_id},
            dataType: 'json',
            success: function(response){
                if (response.status === 'success') {
                    var specifications = response.specifications;
                        var dropdown = $('#PmsAccWprItemConsumed_item_id');
                        dropdown.empty();
                        dropdown.append('<option value="">-Select Specification-</option>');
                        specifications.forEach(function(spec) {
                            dropdown.append('<option value="' + spec.id + '">' + spec.specification + '</option>');
                        });
                } else {
                    // Handle error if needed
                }
            },
            error: function(){
                // Handle error
            }
            });
        });
    });

    $('#PmsAccWprItemConsumed_item_id').change(function() {
        updateRateBasedOnSelection();
    });

    $('#PmsAccWprItemConsumed_warehouse_id').change(function() {
        updateRateBasedOnSelection();
    });

    function updateRateBasedOnSelection() {
        var specificationId = $('#PmsAccWprItemConsumed_item_id').val();
        var warehouseId = $('#PmsAccWprItemConsumed_warehouse_id').val();
    
        $.ajax({
            type: 'POST',
            url: '<?php echo $this->createUrl('warehouse/getRate'); ?>',
            data: {
                specification_id: specificationId,
                warehouse_id: warehouseId,
            },
            dataType: 'json',
            success: function(response) {
                var rateDropdown = $('#PmsAccWprItemConsumed_item_rate');
                var rateErrorDiv = $('#rateError');
                
                rateDropdown.empty();
                rateErrorDiv.text('');
                $('#saveChanges').prop('disabled', false); 
                if (response.status === 'success') {
                    response.rates.forEach(function(rate) {
                        rateDropdown.append('<option value="' + rate.rate + '">' + rate.rate + '</option>');
                    });
                } else {
                    rateErrorDiv.text(response.message);
                    $('#saveChanges').prop('disabled', true); 
                }
            },
            error: function(xhr, status, error) {
                console.error('AJAX Error:', status, error);
                $('#rateField').val('');
                alert('An error occurred while fetching the rate.');
            }
        });
    }
</script>
