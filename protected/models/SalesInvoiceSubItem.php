<?php

/**
 * This is the model class for table "{{sales_invoice_sub_item}}".
 *
 * The followings are the available columns in table '{{sales_invoice_sub_item}}':
 * @property integer $id
 * @property integer $invoice_item_id
 * @property string $item_description
 * @property integer $item_type
 * @property double $quantity
 * @property string $unit
 * @property double $rate
 * @property double $length
 * @property double $width
 * @property double $height
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property Users $createdBy
 * @property SalesInvoiceItem $invoiceItem
 * @property Users $updatedBy
 */
class SalesInvoiceSubItem extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_invoice_sub_item}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// array('unit', 'required'),
			array('invoice_item_id, item_type, created_by, updated_by', 'numerical', 'integerOnly' => true),
			array('quantity, rate, length, width, height', 'numerical'),
			array('unit', 'length', 'max' => 100),
			array('item_description', 'required'),
			array('item_description, created_date, updated_date,invoice_id,amount,id', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, invoice_item_id, item_description, item_type, quantity, unit, rate, length, width, height, created_by, created_date, updated_by, updated_date', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'invoiceItem' => array(self::BELONGS_TO, 'SalesInvoiceItem', 'invoice_item_id'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'invoice_item_id' => 'Invoice Item',
			'item_description' => 'Item Description',
			'item_type' => 'Item Type',
			'quantity' => 'Quantity',
			'unit' => 'Unit',
			'rate' => 'Rate',
			'length' => 'Length',
			'width' => 'Width',
			'height' => 'Height',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('invoice_item_id', $this->invoice_item_id);
		$criteria->compare('item_description', $this->item_description, true);
		$criteria->compare('item_type', $this->item_type);
		$criteria->compare('quantity', $this->quantity);
		$criteria->compare('unit', $this->unit, true);
		$criteria->compare('rate', $this->rate);
		$criteria->compare('length', $this->length);
		$criteria->compare('width', $this->width);
		$criteria->compare('height', $this->height);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SalesInvoiceSubItem the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getType($type)
	{
		if ($type == '1') {
			$value = 'Quantity * Rate';
		} elseif ($type  == '2') {
			$value = "Lumpsum";
		} elseif ($type == '3') {
			$value = "Length * Width";
		} elseif ($type == '4') {
			$value = "Length * Width * Height";
		} else {
			$value = '';
		}
		return $value;
	}
	public function getDimension($model)
	{
		if ($model['item_type'] == '3') {
			$value = $model['length'] . " * " . $model['width'];
		} elseif ($model['item_type'] == '4') {
			$value = $model['length'] . " * " . $model['width'] . " * " . $model['height'];
		} else {
			$value = 'N/A';
		}
		return $value;
	}
	public function getUnit($model)
	{
		if ($model['item_type'] == '1') {
			$value = $model['unit'];
		} elseif ($model['item_type'] == '3') {
			$value = 'sq.ft';
		} elseif ($model['item_type'] == '4') {
			$value = 'CUFT';
		} else {
			$value = 'N/A';
		}
		return $value;
	}
}
