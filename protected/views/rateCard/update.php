<?php
/* @var $this WorkTypeController */
/* @var $model WorkType */

$this->breadcrumbs=array(
	'Work Types'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
?>
<div class="panel panel-gray">
    <div class="panel-heading form-head">
        <h3 class="panel-title">Edit Rate Card</h3>
    </div>
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
