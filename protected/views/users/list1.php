
			<div class="container" id="user">
				<h4>Filter By :</h4>
				<div class=" filter">
					<form id="usersearch" method="post" action="<?php echo $this->createAbsoluteUrl('users/userlist'); ?>">
					<ul class="list-inline">
						
						<li class="">
					        <label>User:</label>
							<input type="text" name="user" id="user" value="<?php echo (isset($user) ? $user : ""); ?>">
						</li>
						<li class="">
							<label>Profile Type :</label>
							<input type="text" name="type" id="type">
						</li>
						<li class="">
							<label>Status :</label>
							<input type="text" name="status" id="status">
						</li>
						
						<li class="">
							<label>Reporting Person :</label>
							<input type="text" name="person" id="person">
						</li>
					</ul>
					
					</form>
				</div>
				<div class="add-btn">
					<button data-toggle="modal" data-target="#addUser" class="createUser">+</button>
				</div>
				
				<div class="exp-list">
					<?php
					$tblpx = Yii::app()->db->tablePrefix;
					foreach($model as $mod) 
					{
			$id= $mod['userid'];			
			$sql= Yii::app()->db->createCommand('select role from '.$tblpx.'users
			inner join '.$tblpx.'user_roles on '.$tblpx.'users.user_type = '.$tblpx.'user_roles.id
			where userid='.$id
		   )->queryAll();
			//print_r($sql); die();	
					?>
					<div class="row">
							<div class="col-md-5 col-sm-5 col-xs-4">
							
								<div><?php echo $mod['username']?></div>
								<div><?php echo $sql[0]['role']?></div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-5">
								<div><?php echo $mod['email']?></div>
								<div>Ph:<?php echo $mod['phonenumber']?></div>
								<div>Reporting Person:<?php echo $mod['parent_name']?></div>
								<!--div>Status:<?php echo ($mod['status']==1 ? "Active":"Inactive") ;?></div-->
							</div>
							<div class="col-md-1 col-sm-1 col-xs-2">
								<span class="fa fa-edit editUser" data-toggle="modal" data-target=".edit" data-id="<?php echo $id; ?>"></span>
							</div>
					</div>
					<?php
					}
					?>
					
				</div>
			

		<!-- Add User Popup -->
		<div id="addUser" class="modal fade" role="dialog">
			<div class="modal-dialog modal-lg">

		</div>
	</div>

		<!--Edit Popup -->
		<div class="modal fade edit" role="dialog">
			<div class="modal-dialog modal-lg">
		
		</div>
	</div>
	
<script>
 $(document).ready(function () {
	 $('.createUser').click(function () {
		// alert('hi');
		 $.ajax({
               type: "GET",
               
               url: "<?php echo $this->createUrl('users/create') ?>",
               success: function (response)
               {
                   $("#addUser").html(response);

               }
           });
	 });
	 
	  $('.editUser').click(function () {
          // alert("hai");
           var id=$(this).attr('data-id');
       //   alert(id);
          
           $.ajax({
               type: "GET",
               url: "<?php echo $this->createUrl('users/update&id=') ?>" + id,
               success: function (response)
               {
                   $(".edit").html(response);

               }
           });

       });
	 
	 jQuery(function($) {
		$('#user').on('keydown', function(event) {
			 if(event.keyCode == 13){
				 $("#usersearch").submit();
				 
			 }
		
           
		});
});
	
      
});	 
</script>				
