<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: green;font-size: 20px;font-weight: bold;">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>


<?php if (Yii::app()->user->hasFlash('error')) : ?>
    <div class="info" style="width: 500px;margin-left: 400px;padding: 8px;color: red;font-size: 20px;font-weight: bold;">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php endif; ?>
<style type="text/css">
    table.total-table {
        font-size: 14px;
    }

    table.total-table div {
        text-align: right;
    }

    table.table .form-control {
        padding: 1px 1px;
        font-size: inherit;
        min-width: 70px;
    }

    table.table tr.pitems td div {
        padding-top: 8px;
    }

    table.table tr.pitems td div .fa {
        color: #060;
    }

    .formError .formErrorArrow div {
        display: none !important;
    }

    .formErrorContent {
        background-color: #333333 !important;
        border: 1px solid #ddd !important;
    }

    .bold {
        font-weight: bold;
    }


    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .addRow,
    .table-responsive {
        margin-bottom: 10px;
    }

    .extracharge {
        float: left;
    }

    .extracharge h4 {
        position: relative;
    }

    .addcharge {
        position: absolute;
        top: 2px;
        right: -25px;
        cursor: pointer;
    }

    .extracharge div {
        margin-bottom: 10px;
    }

    .addRow input.textbox {
        width: 200px;
        display: inline-block;
        margin-right: 10px;
    }

    .delbil,
    .deleteitem {

        color: #000;
        background-color: #eee;
        padding: 3px;
        cursor: pointer;
    }

    .addRow label {
        display: inline-block;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css" />
<?php if (Yii::app()->user->hasFlash('success')) : ?><br>
    <div class="example1consolesucces">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>.select();
<?php endif; ?>
<div class="container" id="expense">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix">
		<div class="add-btn pull-right">
			
			<a href="<?php echo $this->createUrl('subcontractorbill/admin') ?>" class="button">View Bills</a>
			
		</div>
		<h2>Subcontractor Bill</h2>
		
	</div>
    <div id="errormessage"></div>
    <div class="panel panel-gray">
        <div class="panel-heading">
            <h3 class="panel-title">Subcontractor Bill</h3>
        </div>
        <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>

        <div class="panel-body">
            <div class="form">
                <div id="alert"></div>
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'subcontractorbill-form',            
                    'enableAjaxValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                ));
                ?>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'company_id'); ?>
                            <?php
                            $model->company_id = $quotation_model['company_id'];
                            $company_model = Company::model()->findByPk($model->company_id);
                            ?>
                            <input type="text" name="company_id" value="<?= $company_model->name; ?>" class="form-control" readonly="readonly">
                            <input type="hidden" name="Subcontractorbill[company_id]" value="<?= $model->company_id; ?>" id="company_id" class="form-control">

                            <?php echo $form->error($model, 'company_id'); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="Subcontractorbill_project" class="required projectlabel">Project <span class="required">*</span></label>

                            <input type="text" name="project_id" value="<?= $quotation_model->projects->name; ?>" class="form-control" readonly="readonly">


                            <div class="errorMessage" id="projectErrorMsg"></div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="Subcontractorbill_subcontractor" class="required subcontractorlabel">Subcontractor <span class="required">*</span></label>

                            <input type="text" name="subcontractor_id" value="<?= $quotation_model->subcontractor->subcontractor_name; ?>" class="form-control" readonly="readonly">


                            <div class="errorMessage" id="subcontractorErrorMsg"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'scquotation_id'); ?>

                            <input type="text" name="scquotation_id" value="<?= $quotation_model->scquotation_no; ?>" class="form-control" readonly="readonly">
                            <input type="hidden" name="Subcontractorbill[scquotation_id]" value="<?= $quotation_model->scquotation_id; ?>" class="form-control" id="Subcontractorbill_scquotation_id">


                            <?php echo $form->error($model, 'scquotation_id'); ?>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <?php echo $form->labelEx($model,'stage_id'); ?>                
                            <input type="text" name="stage_id"  value="<?= $quotation_stages->payment_stage; ?>" class="form-control" readonly="readonly">
                            <input type="hidden" name="Subcontractorbill[stage_id]"  value="<?= $quotation_stages->id; ?>" class="form-control" >                                    
                            <?php echo $form->error($model,'stage_id'); ?>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <?php $model->bill_number = $bill_number; ?>
                            <?php echo $form->labelEx($model, 'bill_number'); ?>
                            <?php echo $form->textField($model, 'bill_number', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control validate[required]')); ?>
                            <?php echo $form->error($model, 'bill_number'); ?>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'date'); ?>
                            <?php echo $form->textField($model, 'date', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                            <?php echo $form->error($model, 'date'); ?>
                            
                        </div>
                    </div>        
                    <div class="col-sm-4">
                        <div class="form-group">
                            
                            <?php 
                            $quotation_amount =$quotation_model->getTotalQuotationAmount($quotation_model->scquotation_id) ;
                            $stage_amount = $quotation_amount * ($quotation_stages['stage_percent'] / 100);
                            echo $form->labelEx($model,'total_amount'); ?>                
                            <input type="text" name="total_amount"  value="<?= $stage_amount; ?>" class="form-control" readonly="readonly">                    
                        </div>
                    </div>
                </div>
                <div class="row addRow">
                    <div class="col-sm-12 text-center">
                        <?php echo CHtml::Button($model->isNewRecord ? 'Create' : 'Save', array('class' => 'buttonsubmit btn btn-default', 'id' => 'buttonsubmit')); ?>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
            </div>  
        </div>
    </div> 
</div>

<script type="text/javascript">
    $(document).on('keypress', 'input,select', function(e) {
        if (e.which == 13) {
            e.preventDefault();           
            var $canfocus = $(':focusable');
            var index = $canfocus.index(document.activeElement) + 1;
            if (index >= $canfocus.length) index = 0;
            $canfocus.eq(index).focus();
        }
    });
    $("body").on('change', '.table tr.pitems input, .table tr.pitems select.tax_slab', function(e) {       
        var rowId = $(this).parent().attr("id");
        var qty = $(this).parents('tr').find("#quantity").val();
        var rate = $(this).parents('tr').find("#rate").val();
        var item_amount = qty * rate;        
        var aStat;
        var billNo = $("#Subcontractorbill_bill_number").val(); 
        if(qty != "" && rate != ""){           
            var amount = parseFloat(item_amount);  
            $(this).parent('td').siblings(".amount_td").find(".amount").text(amount);
            $(this).parent('td').siblings(".tot_amount_td").find(".tot_amount").text(amount);            
        }else{
            var amount = parseFloat($("#biamount" + rowId).val());
        }    
        if (amount == "")
            amount = 0;
        var sgstp = parseFloat($("#sgstpercent" + rowId).val());
        if (sgstp == "")
            sgstp = 0;
        var cgstp = parseFloat($("#cgstpercent" + rowId).val());
        if (cgstp == "")
            cgstp = 0;
        var igstp = parseFloat($("#igstpercent" + rowId).val());
        if (igstp == "")
            igstp = 0;
        var sgst = (sgstp / 100) * amount;
        if (isNaN(sgst)) sgst = 0;
        $("#sgst" + rowId).val(sgst.toFixed(2));
        var cgst = (cgstp / 100) * amount;
        if (isNaN(cgst)) cgst = 0;
        $("#cgst" + rowId).val(cgst.toFixed(2));
        var igst = (igstp / 100) * amount;
        if (isNaN(igst)) igst = 0;
        $("#igst" + rowId).val(igst.toFixed(2));
        var total_tax = sgst + cgst + igst;
        var total_amount = amount + total_tax;
        var crId = $(this).attr("id");
        $("#tot_tax" + rowId).text(total_tax.toFixed(2));
        var tot_tax_value = parseFloat($("#total_tax").val());
        var tot_total_value = parseFloat($("#total_amount").val());
        if (isNaN(tot_tax_value)) tot_tax_value = 0;              
        tot_tax_value = tot_tax_value + total_tax;
        tot_total_value = tot_total_value + tot_tax_value;
        $("#total_tax").val(tot_tax_value.toFixed(2));
        $("#totalamount" + rowId).text(total_amount.toFixed(2));
        $("#biamount" + rowId).text(total_amount.toFixed(2));
        $("#amount_total" + rowId).val(total_amount.toFixed(2));
        $("#tax_total" + rowId).val(total_tax.toFixed(2));        
        if (billNo == "") {
            $("#alert").addClass("alert alert-danger").html("<strong>Please enter bill number<strong>");
            this.checked = false;
            return false;
        } else {
            $("#alert").removeClass("alert alert-danger").html("");            
        }
    });

    <?php $addUrl = Yii::app()->createAbsoluteUrl("bills/addItemsForBills"); ?>
    <?php $valUrl = Yii::app()->createAbsoluteUrl("subcontractorbill/validateBillNumber"); ?>
    <?php $reloadUrl = Yii::app()->createAbsoluteUrl("bills/updateBillsOnReload"); ?>
    <?php $addbillurl = Yii::app()->createAbsoluteUrl("bills/additionalbill"); ?>
    <?php $delbillurl = Yii::app()->createAbsoluteUrl("bills/deletebill"); ?>
    $(".js-example-basic-single").select2();
    $(".tax_slab").select2();

    $(document).ready(function() {
        $('#loading').hide();
        $('select').first().focus();
    });


    $(document).ready(function() {
        $(function() {
            $("#Subcontractorbill_date").datepicker({
                dateFormat: 'dd-mm-yy'
            }).datepicker("setDate", new Date());
        });

        $(".buttonsubmit").click(function() {
            $("#subcontractorbill-form").submit();
            
        })

        $(document).ajaxComplete(function() {
            $(".tax_slab").select2();
        });
        $("#bills-form").validationEngine({
            'custom_error_messages': {
                'custom[number]': {
                    'message': 'Invalid number'
                }
            }
        });
        $(".purchasechange").change(function() {
            $(".data-amnts, .extracharge").show();
        });
        $('.addcharge').click(function() {
            $(".chargeadd").toggle();
            $(this).toggleClass("icon-minus hidecharge");

        });

        $("#Bills_company_id").focus();

        $(document).on("change", "#Bills_company_id", function() {
            $('#loading').show();
            var val = $(this).val()
            if (val != '') {
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: '<?php echo Yii::app()->createUrl('bills/ajax'); ?>',
                    success: function(result) {
                        $("#Bills_purchase_id").select2("focus");
                    }
                });
            }
        });

        $('#bills-form').on('keypress', ':input', function(event) {
            if (event.keyCode == 13) {
                /* FOCUS ELEMENT */
                var thisId = $(this).attr("id");
                var thisVal = $(this).val();

                if (thisId == "Bills_purchase_id" && thisVal == "") {

                } else {
                    var inputs = $(this).parents("form").eq(0).find(":input");
                    var idx = inputs.index(this);

                    if (idx == inputs.length - 1) {

                    } else {
                        inputs[idx + 1].focus(); //  handles submit buttons
                        inputs[idx + 1].select();
                    }
                    return false;
                }
            }
        });
        $("#Bills_bill_date").keypress(function(e) {
            if (e.keyCode == 13) {
                $("#biquantity0").focus();
                $("#biquantity0").select();
            }
        });
        $('#bills-form').on('keypress', '.lnext', function(e) {
            //$( "#sgst2" ).keypress(function(e) {
            if (e.keyCode == 13) {
                var rowId = parseInt($(this).parent().attr("id"));
                var totrows = $("#totrows").val();
                var nextId = rowId + 1;
                if (nextId == totrows) {
                    var billNo = $("#Bills_bill_number").val();
                    if (billNo == "")
                        $("#Bills_bill_number").focus()
                    else
                        $("#buttonsubmit").focus();
                } else {
                    $("#biquantity" + nextId).focus();
                    $("#biquantity" + nextId).select();
                }
            }
        });


        $("#buttonsubmit").keypress(function(e) {
            if (e.keyCode == 13) {
                $('.buttonsubmit').click();
            }
        });

        $(".buttonsubmit").click(function() {
            $("#bills-form").submit();
        })


        $("#Subcontractorbill_bill_number").change(function() {
            $('#loading').show();
            var billNo = $("#Subcontractorbill_bill_number").val();
            var company_id = $('#company_id').val();
            var crId = "";
            $.ajax({
                url: "<?php echo $valUrl; ?>",
                data: {
                    "billno": billNo,
                    "company_id": company_id
                },
                type: "POST",
                success: function(data) {
                    if (data == 1) {
                        $("#alert").addClass("alert alert-danger").html("<strong>Bill number already exist. Please enter a new bill number.<strong>");
                        //$("#chkitem"+rowId).checked = false;
                        $('#buttonsubmit').prop('disabled', true);
                        return false;
                    } else {
                        $("#alert").removeClass("alert alert-danger").html("");
                        $('#buttonsubmit').prop('disabled', false);

                    }
                }
            });
        });

    });


    //document.onkeydown = convertEnterToTab;


    $("#Bills_company_id").change(function() {
        $('#loading').show();
        var val = $(this).val();
        $("#Bills_purchase_id").html('<option value="">Select Project</option>');
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('bills/dynamicpurchase'); ?>',
            method: 'POST',
            data: {
                company_id: val
            },
            dataType: "json",
            success: function(response) {
                if (response.status == 'success') {
                    $("#Bills_purchase_id").html(response.html);
                } else {
                    $("#Bills_purchase_id").html(response.html);
                }
            }

        })
    })
    $(document).ajaxComplete(function() {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });

    $("#Subcontractorbill_date").change(function(){
        var entry_date =$(this).val();
        var id = $("#Subcontractorbill_scquotation_id").val();
		
        $.ajax({
            type:'POST',
            dataType:'json',
            data:{
                scquotation_id:id,
                entry_date:entry_date
            },
            url:"<?php echo Yii::app()->createUrl("subcontractorpayment/checkQuotationDate") ?>",
            success:function(response){
                
                if(response.response=="error"){  
                    $("#buttonsubmit").attr('disabled', true);                     
                    $("#errormessage").show()
                    .html('<div class="alert alert-danger">'+response.msg+'</div>');
                                       
                }else{                    
                    $("#errormessage").hide()
                    $("#buttonsubmit").attr('disabled', false);
                    
                }
            }
        }) 
    })
</script>