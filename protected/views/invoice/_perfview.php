

<div class="row">
		<div class="col-md-1 col-sm-1">
			<div class="list_item"><span>Invoice No</span></div>
			<div class="list_item"><?php echo $data->inv_no; ?></div>
<!--			<h4>Details</h4>
			<div class="list_item"><span>Quantity</span></div>-->
			<?php
			//foreach($query as $q){
			?>
<!--			<div class="list_item"><?php //echo ($q['quantity'] ? $q['quantity'] : '---'); ?></div>-->
			<?php
			//}
			?>
		</div>
		<div class="col-md-2 col-sm-2">
			<div class="list_item"><span>Client</span></div>
			<div class="list_item"><?php echo $data->client_name; ?></div>
<!--			<h4>&nbsp;&nbsp;</h4>
			<div class="list_item"><span>Unit</span></div>-->
			<?php
			//foreach($query as $q){
			?>
<!--			<div class="list_item"><?php //echo ($q['unit'] ? $q['unit'] : '---'); ?></div>-->
			<?php
			//}
			?>
			
		</div>
		<div class="col-md-2 col-sm-2">
			<div class="list_item"><span>Date</span></div>
			<div class="list_item"><?php if($data->date!= ""){ echo date("d-m-Y", strtotime($data->date)); } ?></div>
<!--			<h4>&nbsp;&nbsp;</h4>
			<div class="list_item"><span>Rate</span></div>-->
			<?php
			//foreach($query as $q){
			?>
<!--			<div class="list_item"><?php //echo ($q['rate'] ? $q['rate'] : '---'); ?></div>-->
			<?php
			//}
			?>
		</div>
		<div class="col-md-2 col-sm-2">
			<div class="list_item"><span>Project</span></div>
			<div class="list_item"><?php echo $data->project_name; ?></div>
<!--			<h4>&nbsp;&nbsp;</h4>
			<div class="list_item"><span>Amount</span></div>-->
			<?php
			//foreach($query as $q){
			?>
<!--			<div class="list_item"><?php //echo ($q['amount'] ? $q['amount'] : '---'); ?></div>-->
			<?php
			//}
			?>
			
		</div>
		<div class="col-md-1 col-sm-1">
			<div class="list_item"><span>Fees </span></div>
			<div class="list_item"><?php echo $data->fees; ?></div>
<!--			<h4>&nbsp;&nbsp;</h4>
			<div class="list_item"><span>Description</span></div>-->
			<?php
			//foreach($query as $q){
			?>
<!--			<div class="list_item"><?php //echo ($q['description'] ? $q['description'] : '---'); ?></div>-->
			<?php
			//}
			?>	
		</div>
		<div class="col-md-1 col-sm-1">
			<div class="list_item"><span>Sub Total</span></div>
			<div class="list_item"><?php echo $data->subtotal; ?></div>
		</div>
		<div class="col-md-2 col-sm-2">
			<div class="list_item"><span>Total Amount </span></div>
			<div class="list_item"><?php echo $data->amount; ?></div>
		</div>
		
         <div class="col-md-1 col-sm-1">
                <a href="index.php?r=invoice/viewperforma&pid=<?php echo $data->id; ?>"><span class="fa fa-eye editExpense" data-toggle="modal" data-target=".edit" data-id="<?php echo $data->id; ?>"></span></a>   
                <a href="index.php?r=invoice/updateperforma&pid=<?php echo $data->id; ?>"><span class="fa fa-edit editExpense" data-toggle="modal" data-target=".edit" data-id="<?php echo $data->id; ?>"></span></a>   
         </div>          
</div>
