<?php

/**
 * This is the model class for table "{{quotation_list}}".
 *
 * The followings are the available columns in table '{{quotation_list}}':
 * @property integer $id
 * @property integer $qt_id
 * @property double $quantity
 * @property string $unit
 * @property double $rate
 * @property double $amount
 * @property double $cgst
 * @property double $cgst_amount
 * @property double $sgst
 * @property double $sgst_amount
 * @property double $igst
 * @property double $igst_amount
 * @property double $tax_amount
 * @property string $description
 * @property string $created_date
 */
class QuotationList extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{quotation_list}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('quantity, rate, amount, description', 'required'),
			array('qt_id', 'numerical', 'integerOnly'=>true),
			array('quantity, rate, amount, cgst, cgst_amount, sgst, sgst_amount, igst, igst_amount, tax_amount', 'numerical'),
			array('unit', 'length', 'max'=>10),
			array('created_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, qt_id, quantity, unit, rate, amount, cgst, cgst_amount, sgst, sgst_amount, igst, igst_amount, tax_amount, description, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'qt_id' => 'Qt',
			'quantity' => 'Quantity',
			'unit' => 'Unit',
			'rate' => 'Rate',
			'amount' => 'Amount',
			'cgst' => 'Cgst',
			'cgst_amount' => 'Cgst Amount',
			'sgst' => 'Sgst',
			'sgst_amount' => 'Sgst Amount',
			'igst' => 'Igst',
			'igst_amount' => 'Igst Amount',
			'tax_amount' => 'Tax Amount',
			'description' => 'Description',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('qt_id',$this->qt_id);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('unit',$this->unit,true);
		$criteria->compare('rate',$this->rate);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('cgst',$this->cgst);
		$criteria->compare('cgst_amount',$this->cgst_amount);
		$criteria->compare('sgst',$this->sgst);
		$criteria->compare('sgst_amount',$this->sgst_amount);
		$criteria->compare('igst',$this->igst);
		$criteria->compare('igst_amount',$this->igst_amount);
		$criteria->compare('tax_amount',$this->tax_amount);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QuotationList the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
