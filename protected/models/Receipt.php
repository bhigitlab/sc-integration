<?php

/**
 * This is the model class for table "{{receipt}}".
 *
 * The followings are the available columns in table '{{receipt}}':
 * @property integer $rec_id
 * @property integer $project_id
 * @property double $receipt_amount
 * @property string $date
 * @property string $description
 * @property integer $payment_type
 *
 * The followings are the available model relations:
 * @property Status $paymentType
 * @property Projects $project
 */
class Receipt extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{receipt}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_id, receipt_amount, date, payment_type', 'required'),
			array('project_id, payment_type', 'numerical', 'integerOnly'=>true),
			array('receipt_amount', 'numerical'),
			array('description', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('rec_id, project_id, receipt_amount, date, description, payment_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'paymentType' => array(self::BELONGS_TO, 'Status', 'payment_type'),
			'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'rec_id' => 'Rec',
			'project_id' => 'Project',
			'receipt_amount' => 'Receipt Amount',
			'date' => 'Date',
			'description' => 'Description',
			'payment_type' => 'Payment Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('rec_id',$this->rec_id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('receipt_amount',$this->receipt_amount);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('payment_type',$this->payment_type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort' => array("defaultOrder" => "rec_id DESC")
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Receipt the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
