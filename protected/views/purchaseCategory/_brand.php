<?php
if ($index == 0) {
?>
    <div class="filter_sec pull-right col-sm-6 col-md-4 padding-0">
        <label>Search:</label>
        <input id="brand_search" type="text" class="form-control">
    </div>
    <thead class="entry-table">
        <tr>

            <th style="width: 20px;">No</th>
            <th>Brand</th>
            <th>Company</th>
            <th>Created Date</th>
            <?php
            if (isset(Yii::app()->user->role) && (in_array('/purchaseCategory/manageitem', Yii::app()->user->menuauthlist))) {
            ?>
                <th></th>
            <?php } ?>

        </tr>
    </thead>
<?php } ?>

<tr>
    <td style="width: 20px;"><?php echo $index + 1; ?></td>
    <td><?php echo $data->brand_name; ?></td>
    <td>
        <?php
        $companyname = array();
        $arrVal = explode(',', $data['company_id']);
        foreach ($arrVal as $arr) {
            $value = Company::model()->findByPk($arr);
            if ($value !== NULL) {
                array_push($companyname, $value['name']);
            }
        }
        echo rtrim(trim(implode(', ', $companyname)), ",");
        ?>
    </td>
    <td><?php echo date('d-m-Y', strtotime($data->created_date));  ?></td>
    <?php
    if (isset(Yii::app()->user->role) && (in_array('/purchaseCategory/manageitem', Yii::app()->user->menuauthlist))) {
    ?>
        <td>
            <div class="edit_purchase edit_option_<?php echo $data->id; ?>" id="<?php echo $data->id; ?>" data-toggle="tooltip" title="Edit"><span class="fa fa-edit editBrands" data-toggle="modal" data-target=".edit" data-id="<?php echo $data->id; ?>"></span></div>
        </td>
    <?php } ?>


</tr>