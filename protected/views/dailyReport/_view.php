<?php
$tblpx = Yii::app()->db->tablePrefix;
$pms_api_integration_model = ApiSettings::model()->findByPk(1);
$pms_api_integration = $pms_api_integration_model->api_integration_settings;

$tblpx = Yii::app()->db->tablePrefix;
if ($index == 0) {
    ?>

    <thead class="entry-table">
        <tr>
            <?php
            if ($unapprovedCount > 1) {
                if ($status == 2 || $status == 3 || $status == '') {
                    ?>
                    <th>
                        <input type="checkbox" name="allbulkapprovecheck[]" class="checkallbulkapprove clearcheck_all"
                            id="checkallbulkapprove<?php $index; ?>" data-id="" value="" style="vertical-align: text-bottom;" />
                    </th>
                <?php }
            } ?>
            <th></th>
            <th>Sl No</th>
            <th>Company</th>
            <th>Project</th>
            <th>Subcontractor</th>
            <th>Expense Head</th>
            <th>Date</th>
            <th>Description</th>
            <th>Requested From</th>
            <th>status</th>
            <th>Total Amount</th>
            <th></th>
        </tr>
    </thead>
<?php }
$bgcolor = '';
$approval_status = $data["approval_status"];
$id = 'dr_id';
$tbl = $this->tableNameAcc('pre_dailyreport', 0);
$modelName = 'PreDailyreport';
$newbgclass = '';
$final = Yii::app()->controller->actiongetDailyReportFinalData($data, $approval_status, $id, $tbl, $modelName);

$data = $final['data'];
$approval_status = $final['approval_status'];
$count = $final['count'];
if ($data['approve_status'] == 2) {
    $newbgclass = 'row_class';
}
$expenseId = $data["dr_id"];
$deleteconfirm = Deletepending::model()->find(array("condition" => "deletepending_parentid = " . $expenseId . " AND deletepending_status = 0 AND deletepending_table = '{$tblpx}dailyreport'"));
$updateconfirm = Editrequest::model()->find(array("condition" => "parent_id = " . $expenseId . " AND editrequest_status = 0 AND editrequest_table = '{$tblpx}dailyreport'"));
$confirmcount = !empty($deleteconfirm) ? count($deleteconfirm->attributes) : 0;
$updatecount = !empty($updateconfirm) ? count($updateconfirm->attributes) : 0;
if ($confirmcount > 0 || $updatecount > 0) {
    $deleteclass = "deleteclass";
    $deletetitle = "Waiting for delete confirmation from admin.";
} else {
    $deleteclass = "";
    $deletetitle = "";
}

?>
<?php
if ($pms_api_integration == 1) {
    if ($data['request_from'] == 2) {
        if ($data['pms_project_id'] == 0 || $data['pms_labour_template'] == 0) {
            $newbgclass = 'row_class_mapped';
        }
    }

}
?>
<tr id="<?php echo $data['dr_id'] ?>"
    class="<?php echo $deleteclass . " " . $newbgclass . " " . (($approval_status == 0) ? 'bg_pending' : ''); ?>"
    title="<?php echo $deletetitle; ?>" data-status="<?php echo $data["approval_status"] ?>"
    data-id="<?php echo ($data["approval_status"] == 0) ? $data['ref_id'] : $data['dr_id']; ?>">
    <td>
        <?php
        if ($unapprovedCount > 1) {
            if ($data['approve_status'] == 2) { ?>
                <input type="checkbox" name="bulkapprovecheck[]" class="bulkapprovecheck clearcheck_all"
                    id="bulkapprove<?php $index; ?>" data-id="<?php echo $data['dr_id']; ?>"
                    value="<?php echo $data['dr_id']; ?>" style="vertical-align: text-bottom;" />
            <?php }
        } ?>
    </td>
    <td>
        <?php

        if ($updatecount > 0) {
            $requestmessage = "Already an update request is pending";
            ?>
            <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button"
                data-html="true" style="cursor: pointer;"></span>
            <div class="popover-content hide">
                <ul class="tooltip-hiden">
                    <li><span id="<?php echo $index; ?>" class="pending-icon fa fa-question"
                            title="<?php echo $requestmessage; ?>"></span></li>
                </ul>
            </div>
            <?php
        } else {
            $tblpx = Yii::app()->db->tablePrefix;
            $pms_api_integration_model = ApiSettings::model()->findByPk(1);
            $pms_api_integration = $pms_api_integration_model->api_integration_settings;
            $payDate = date("Y-m-d", strtotime($data["date"]));
            if ($pms_api_integration != '1') {
                $company = Company::model()->findByPk($data["company_id"]);
                $updateDays = ($company->company_updateduration) - 1;
                $editDate = date('Y-m-d ', strtotime($payDate . ' + ' . $updateDays . ' days'));
            }

            $addedDate = date("Y-m-d 23:59:59", strtotime($data["date"]));
            $currentDate = date("Y-m-d");
            if (Yii::app()->user->role == 1) {

                ?>
                <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button"
                    data-html="true" style="cursor: pointer;"></span>
                <div class="popover-content hide">
                    <ul class="tooltip-hiden">
                        <?php
                        if ((in_array('/dailyReport/updateDailyentry', Yii::app()->user->menuauthlist))) {
                            if ($data['approve_status'] != 1) {
                                ?>
                                <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default row-daybook">Edit</button></li>
                            <?php }
                        } ?>
                        <?php if ($data['approve_status'] == 1) { ?>
                            <li><a class="click btn btn-xs btn-default" id="<?php echo $data['dr_id']; ?>" href="#"
                                    onClick="view('<?php echo $data['dr_id']; ?>')" title="Update">View</a></li>
                        <?php } ?>
                        <?php
                        if ((in_array('/dailyReport/deletereport', Yii::app()->user->menuauthlist))) {
                            if ($data['request_from'] != 2) {
                                ?>
                                <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default delete_row"
                                        entry-date="<?php echo $data["date"] ?>">Delete</button></li>
                            <?php }
                        } ?>

                        <?php
                        if ($data['approve_status'] == 2) {
                            if ((in_array('/dailyReport/approve', Yii::app()->user->menuauthlist))) {
                                ?>
                                <li><button id="<?php echo $data['dr_id']; ?>"
                                        class="btn btn-xs btn-default approve approveoption_<?php echo $data['dr_id']; ?>">Approve</button>
                                </li>
                                <li><a class="click btn btn-xs btn-default" id="<?php echo $data['dr_id']; ?>" href="#"
                                        onClick="openRejectModal('<?php echo $data['dr_id']; ?>')" title="Reject">Reject</a></li>
                            <?php }
                        } ?>
                    </ul>
                </div>
                <?php
                // }
            } else {
                if (isset($company->company_updateduration) && !empty($company->company_updateduration)) {
                    if ($editDate >= $currentDate) {
                        ?>
                        <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button"
                            data-html="true" style="cursor: pointer;"></span>
                        <div class="popover-content hide">
                            <ul class="tooltip-hiden">
                                <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default row-daybook">Edit</button></li>
                                <?php if ($confirmcount == 0) { ?>
                                    <li><button id="<?php echo $index; ?>"
                                            class="btn btn-xs btn-default delete_confirmation">Delete</button></li>
                                <?php } else { ?>
                                    <li><button id="<?php echo $index; ?>" data-id="<?php echo $deleteconfirm["deletepending_id"]; ?>"
                                            class="btn btn-xs btn-default delete_restore">Restore</button></li>
                                <?php }
                                if ($data['approve_status'] == 2) {
                                    if ((in_array('/dailyReport/approve', Yii::app()->user->menuauthlist))) {
                                        ?>
                                        <li><button id="<?php echo $data['dr_id']; ?>"
                                                class="btn btn-xs btn-default approve approveoption_<?php echo $data['dr_id']; ?>">Approve</button>
                                        </li>
                                    <?php }
                                } ?>
                            </ul>
                        </div>
                        <?php
                    }
                } else {
                    if ($currentDate <= $addedDate) {
                        ?>
                        <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button"
                            data-html="true" style="cursor: pointer;"></span>
                        <div class="popover-content hide">
                            <ul class="tooltip-hiden">
                                <?php
                                if ((in_array('/dailyReport/updateDailyentry', Yii::app()->user->menuauthlist))) {
                                    ?>
                                    <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default row-daybook">Edit</button></li>
                                <?php } ?>
                                <?php
                                if ((in_array('/dailyReport/deletereport', Yii::app()->user->menuauthlist))) {
                                    ?>
                                    <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default delete_row"
                                            entry-date="<?php echo $data["date"] ?>">Delete</button></li>
                                <?php }
                                if ($data['approve_status'] == 2) {
                                    if ((in_array('/dailyReport/approve', Yii::app()->user->menuauthlist))) {
                                        ?>
                                        <li><button id="<?php echo $data['dr_id']; ?>"
                                                class="btn btn-xs btn-default approve approveoption_<?php echo $data['dr_id']; ?>">Approve</button>
                                        </li>
                                    <?php }
                                } ?>
                            </ul>
                        </div>
                        <?php
                    }
                }
            }
        } ?>
    </td>
    <td>
        <?php echo $index + 1; ?>
    </td>
    <td>
        <?php
        if (isset($data["company_id"])) {
            $company = Company::model()->findByPk($data["company_id"]);
            echo $company['name'];
        } else {
            echo "";
        }
        ?>
    </td>
    <td>
        <?php
        if (isset($data["projectid"])) {
            $project = Projects::model()->findByPk($data["projectid"]);
            echo $project['name'];
        }

        ?>
    </td>
    <td>
        <?php
        if (isset($data["subcontractor_id"])) {
            $subcontractor = Subcontractor::model()->findByPk($data["subcontractor_id"]);
            echo $subcontractor['subcontractor_name'];
        } else {
            echo 'No Subcontractor';
        }
        ?>
    </td>
    <td>
        <?php
        if (isset($data["expensehead_id"])) {
            $expensehead = ExpenseType::model()->findByPk($data["expensehead_id"]);
            echo $expensehead['type_name'];
        }
        ?>
    </td>
    <td class="nowrap">
        <?php echo date('d-m-Y', strtotime($data['date'])); ?>
    </td>
    <td>
        <?php echo $data['description']; ?>
    </td>
    <td style="white-space:nowrap;">
        <?php
        if ($data['request_from'] == 1) {
            echo CHtml::encode('ACCOUNTS');
        } else {
            echo CHtml::encode('PMS');
        } ?>
    </td>
    <td style="white-space:nowrap;">
        <?php
        if ($data['approve_status'] == 2) {
            echo CHtml::encode('Pending');
        } else if ($data['approve_status'] == 1) {
            echo CHtml::encode('Approved');
        } else if ($data['approve_status'] == 4) {
            echo CHtml::encode('Pending');
        } else if ($data['approve_status'] == 5) {
            echo CHtml::encode('Rejected');
        }
        ?>
    </td>
    <td class="text-right"><span class="popover-test tax_amnt" data-toggle="popover" data-trigger="hover"
            data-placement="left" data-trigger="hover" type="button" data-html="true">
            <?php echo $data['amount'] ? Controller::money_format_inr($data['amount'], 2) : ""; ?>
        </span>
        <div class="popover-content hide">
            <?php
            $model2 = ExpenseType::model()->findByPk($data['expensehead_id']);

            ?>
            <?php
            if (isset($model2->labour_status) && $model2->labour_label != '' && $model2->labour_status == 1) {
                $head_array[] = $model2->labour_label;
                echo "<div><span>" . $model2->labour_label . ": " . $data['labour'] . "</span></div>";
            }
            if (isset($model2->wage_label) && $model2->wage_label != '' && $model2->wage_status == 1) {
                echo "<div><span>" . $model2->wage_label . ": " . $data['wage'] . "</span></div>";
            }

            if (isset($model2->wage_rate_label) && $model2->wage_rate_label != '' && $model2->wagerate_status == 1) {
                echo "<div><span>" . $model2->wage_rate_label . ": " . $data['wage_rate'] . "</span></div>";
            }
            if (isset($model2->helper_label) && $model2->helper_label != '' && $model2->helper_status == 1) {
                echo "<div><span>" . $model2->helper_label . ": " . $data['helper'] . "</span></div>";
            }
            if (isset($model2->helper_labour_label) && $model2->helper_labour_label != '' && $model2->helperlabour_status == 1) {
                echo "<div><span>" . $model2->helper_labour_label . ": " . $data['helper_labour'] . "</span></div>";
            }
            if (isset($model2->lump_sum_label) && $model2->lump_sum_label != '' && $model2->lump_sum_status == 1) {
                echo "<div><span>" . $model2->lump_sum_label . ": " . $data['lump_sum'] . "</span></div>";
            }

            ?>
        </div>
    </td>
    <input type="hidden" name="reportid[]" id="dlyreportid<?php echo $index; ?>"
        value="<?php echo $data["dr_id"]; ?>" />
</tr>
<style>
    .approve_section {
        background: #f8cbcb !important;
    }

    .row_class {
        background-color: #f8cbcb !important;
    }

    .row_class_mapped {
        background-color: #A9E9EC !important;
    }
</style>