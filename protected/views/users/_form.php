<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<div class="">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'users-form',
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    $readonly=(defined('LOGIN_USER_TABLE') && LOGIN_USER_TABLE != '')?true:false;
    ?>
    <!-- Popup content-->

    <div class="panel-body clearfix">
        <div class="userform">

            <div class="row addRow">					
                <div class="col-md-6">
                    <?php echo $form->labelEx($model, 'first_name'); ?>
                    <?php echo $form->textField($model, 'first_name', array('size' => 30, 'class' => 'form-control first_name','readonly'=>$readonly)); ?>
                    <?php echo $form->error($model, 'first_name'); ?>
                </div>
                <div class="col-md-6">
                    <?php echo $form->labelEx($model, 'last_name'); ?>
                    <?php echo $form->textField($model, 'last_name', array('size' => 30, 'class' => 'form-control last_name','readonly'=>$readonly)); ?>
                    <?php echo $form->error($model, 'last_name'); ?>
                </div>
            </div>
            <div class="row addRow">	
                <div class="col-md-6">
                    <?php echo $form->labelEx($model, 'username'); ?>
                    <?php echo $form->textField($model, 'username', array('size' => 30, 'class' => 'form-control username')); ?>
                    <?php echo $form->error($model, 'username'); ?>
                </div>
                <div class="col-md-6">						
                    <?php echo $form->labelEx($model, 'password'); ?>
                    <?php echo $form->passwordField($model, 'password', array('autocomplete' => 'off', 'size' => 30, 'value' => '', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'password'); ?>
                </div>
            </div>
            <div class="row addRow">	
                <div class="col-md-6">
                    <?php echo $form->labelEx($model, 'email'); ?>
                    <?php echo $form->textField($model, 'email', array('size' => 30, 'class' => 'form-control','readonly'=>$readonly)); ?>
                    <?php echo $form->error($model, 'email'); ?>
                </div>
                <div class="col-md-6">						
                    <?php echo $form->labelEx($model, 'phonenumber'); ?>
                    <?php echo $form->textField($model, 'phonenumber', array('size' => 30, 'class' => 'form-control')); ?>

                    <?php echo $form->error($model, 'phonenumber'); ?>
                </div>
            </div>
            <div class="row addRow">
                <div class="col-md-6">						
                    <?php echo $form->labelEx($model, 'user_type'); ?>
                    <?php echo $form->dropDownList($model, 'user_type', CHtml::listData(UserRoles::model()->findAll(array('order' => 'role ASC')), 'id', 'role'), array('class' => 'form-control', 'empty' => '-Choose a role-')); ?>
                    <?php echo $form->error($model, 'user_type'); ?>
                </div>
                <div class="col-md-6">
                    <?php echo $form->labelEx($model, 'reporting_person'); ?>
                    <?php
                    $where = '';
                    if (Yii::app()->user->role != 1) {
                        $where = "AND t.company_id=" . Yii::app()->user->company_id . "";
                    }
                    $tblpx = Yii::app()->db->tablePrefix;
                    $sql = "SELECT `t`.`userid`, concat_ws(' ',`t`.`first_name`, `t`.`last_name`) as full_name,"
                            . "`{$tblpx}user_roles`.`role` "
                            . "FROM `{$tblpx}users` `t` JOIN {$tblpx}user_roles ON {$tblpx}user_roles.id= t.user_type "
                            . "WHERE status=0 and user_type in (1,2) " . $where . " ORDER BY user_type,full_name ASC";
                    $result = Yii::app()->db->createCommand($sql)->queryAll();
                    $listdata = CHtml::listData($result, 'userid', 'full_name', 'role');
                    echo $form->dropDownList($model, 'reporting_person', $listdata, array('class' => 'form-control', 'empty' => '-----------','disabled'=>$readonly));
                    ?>

                    <?php echo $form->error($model, 'reporting_person'); ?>
                </div>

            </div>
            <div class="row addrow">
                <div class="col-md-6">	
                    <?php echo $form->labelEx($model, 'company_id'); ?>
                    <ul class="checkboxList">
                        <?php
                        $disabled = '';

                        $type_list = CHtml::listData(Company::model()->findAll(), 'id', 'name');
                        $typelist = Company::model()->findAll(array());

                        if (count($typelist) == 0) {
                            $disabled = 'disabled';
                        }

                        $assigned_company_array = array();

                        if (!$model->isNewRecord) {
                            $assigned_types = Users::model()->find(array('condition' => 'userid=' . $model->userid));
                            $assigned_types_array = explode(",", $assigned_types->company_id);
                        } else {
                            $assigned_types_array = '';
                        }

                        echo CHtml::checkBoxList('Users[company_id]', $assigned_types_array,
                                CHtml::listData($typelist, 'id', 'name'),
                                array(
                                    'checkAll' => 'Check all',
                                    'template' => '<li class="checkboxtype">{input}{label}</li>',
                                    'separator' => '', 'disabled' => $disabled)
                        );
                        ?>				
                    </ul>
                    <?php echo $form->error($model, 'company_id'); ?>

                </div>
                <?php
                if (Yii::app()->user->role == 1 && ($model->userid != Yii::app()->user->id)) {
                    ?>
                    <div class="col-md-6 form-group">
                        <?php echo $form->labelEx($model, 'status'); ?>
                        <div class="radio_btn1">
                            <?php
                            echo $form->radioButtonList($model, 'status', array('Active', 'Inactive'), array('separator' => '',));
                            ?>
                        </div>
                    </div>
                <?php } ?>
            </div>



        </div>
    </div>
    <div class="save-btnHold panel-footer text-center">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info submit')); ?>
        
        <?php
        if (!$model->isNewRecord) {
            echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn'));
        } else {
            echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;', 'class' => 'btn btn-default'));
            echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn'));
        }
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<style>
    
    .addRow label {
        display: inline-block;}
    input[type="radio"]{
        display: inline-block;
        margin: 4px 4px 0;
    }

</style>
<script>
    $(document).ready(function () {

        $("#select_all").change(function () {  //"select all" change
            var status = this.checked; // "select all" checked status
            $('.checkbox').each(function () { //iterate all listed checkbox items
                this.checked = status; //change ".checkbox" checked status
            });
        });

        $('.checkbox').change(function () { //".checkbox" change
            //uncheck "select all", if one of the listed checkbox item is unchecked
            if (this.checked == false) { //if this item is unchecked
                $("#select_all")[0].checked = false; //change "select all" checked status to false
            }

            //check "select all" if all checkbox items are checked
            if ($('.checkbox:checked').length == $('.checkbox').length) {
                $("#select_all")[0].checked = true; //change "select all" checked status to true
            }
        });


        $("#select_alltype").change(function () {  //"select all" change
            var status = this.checked; // "select all" checked status
            $('.checkboxtype').each(function () { //iterate all listed checkbox items
                this.checked = status; //change ".checkbox" checked status
            });
        });


        $('.checkboxtype').change(function () { //".checkbox" change
            //uncheck "select all", if one of the listed checkbox item is unchecked
            if (this.checked == false) { //if this item is unchecked
                $("#select_alltype")[0].checked = false; //change "select all" checked status to false
            }

            //check "select all" if all checkbox items are checked
            if ($('.checkboxtype:checked').length == $('.checkboxtype').length) {
                $("#select_alltype")[0].checked = true; //change "select all" checked status to true
            }
        });


    });

    function countOfLetters(str) {
        var letter = 0;
        for (i = 0; i < str.length; i++) {
            if ((str[i] >= 'A' && str[i] <= 'Z')
                || (str[i] >= 'a' && str[i] <= 'z'))
            letter++;
        }
        
        return letter;
    } 
      
    $(".first_name,.last_name,.username").keyup(function () {         
        var alphabetCount =  countOfLetters(this.value);  
        var message ="";      
        var disabled=false;
        if(alphabetCount < 1){
            var message = "Invalid User Name";
            var disabled=true;           
        }             
        $(this).siblings(".errorMessage").show().html(message).addClass('d-block');                            
        var numItems = $('.errorMessage').not(':empty').length;        
        if(numItems >0){
            $(".submit ").attr('disabled',true);
        }else{
            $(".submit ").attr('disabled',false);
        }            
    });        

</script>
