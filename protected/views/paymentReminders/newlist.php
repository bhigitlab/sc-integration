

<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Projects',
);
?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/additional-methods.js"></script>
<div class="container" id="project">
    <div class="clearfix">
        <div class="add-btn pull-right">
            <?php
                if((in_array('/paymentReminders/create', Yii::app()->user->menuauthlist))){
                ?>
<!--               <button data-toggle="modal" data-target="#addProject"  class="createProject">Add Project</button>-->
                <button class="btn btn-info btn-sm createpro">Add payment reminders</button>
           <?php } ?>
       </div>
       <h2>Payment Reminders</h2>
       <div class="alertmsg alert " style="display: none;"></div>
    </div>
    <div id="projectform" style="display:none;"></div>
<!--    <input type="text" id="proname" placeholder="Filter By Name" />
    <input type="text" id="prostatus" placeholder="Filter By Project Status" />-->

    <?php $this->renderPartial('_newsearch', array('model' => $model)) ?>


      <div  style="">
        <div id="addremark" class="panel panel-gray" style="display:none;position: fixed;top: 15%; bottom: 15%;right: 0px; z-index: 6;width:500px; margin:0 auto;background-color: #fff;">
            <div class="panel-heading form-head">
                <h3 class="panel-title">Add Reminder</h3>
            </div>
            <div class="panel-body">
                <form id="reminder_form" class="show_class">
                <button type="reset"  class="close " data-dismiss="alert" aria-label="Close" onclick="closeremark('this,event');"  ><span aria-hidden="true">X</span></button>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                        <label>Date</label>
                        <input type="text" name="date" id="date" class="form-control">
                    </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                        <label>Status</label>
                        <select id="status" name="status" class="form-control status">
                            <option value="">select</option>
                            <option value="Extended">Extended</option>
                            <option value="Completed">Completed</option>
                        </select>
                    </div>
                    </div>
                </div>
                    <div class="form-group">
                        <label>Comment</label>
                        <textarea type="text" name="comment" comment="remark" id="comment" class="form-control"></textarea>
                    </div>
                    <div class="text-right">
                        <input type="button" value="Save" id="remarkSubmit" class="btn btn-sm btn-info"/>
                    </div>
            </form>
                <input type="hidden" name="reminder_id" id="reminder_id" value=""/>
                <h4 class="head_remarks">Remarks</h4>
                <div class="all_remarks">
                    <div id="remarkList">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="">
        <?php
        //echo "<pre>";  print_r($dataProvider);exit;
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_newview', 'template' => '<div class="clearfix"><table cellpadding="10" class="table" id="protable">{items}</table></div>',
            //'sortableAttributes' => array('ex_percent' => '% of Expense'),
            'ajaxUpdate' => false,
        ));
        ?>

<?php
        Yii::app()->clientScript->registerScript('myjavascript', '
             $(document).ready(function(){
                $("#protable").dataTable( {
//                    "ordering": false,
                    "scrollY": "300px",
                    "scrollCollapse": true,
                    "paging": false,
                    "columnDefs"        : [
                        {
                            "searchable"    : false,
                            "targets"       : [0]
                        },


//                       { "bSortable": false, "aTargets": [-1]  }
                    ],

                } );
                /*var table = $("#protable").DataTable();
                $("#Projects_name").on( "keyup", function () {
                var proname = $(this).val();
                    table
                        .columns( 1 )
                        .search( proname )
                        .draw();
                } );
                $("#Projects_project_status").on( "change", function () {
                var selectedText=" ";
                if($(this).val()!=""){
                var selectedText = $(this).find("option:selected").text();
                }
                    table
                        .columns( 8 )
                        .search( selectedText )
                        .draw();
                } );
                $("#Projects_company_id").on( "change", function () {
                var selectedText=" ";
                if($(this).val()!=""){
                var selectedText = $(this).find("option:selected").text();
                }
                    table
                        .columns( 2 )
                        .search( selectedText )
                        .draw();
                } );
                $("#clear").on( "click", function () {
                var selectedText=" ";
                    table
                        .columns([ 8, 1, 2 ] )
                        .search( selectedText )
                        .draw();
                } );*/



            });

            ');
        ?>
        <script>
       function addremark(pur_order,event){
        var id  = pur_order;
        $(".popover").removeClass( "in" );
        $('#addremark').show("slide", { direction: "right" }, 1000);
        $('#projectform').slideUp();
        $("#txtPurchaseId").val(id);
//        event.preventDefault();
        $.ajax({
            type: "GET",
            data: {reminder_id: id},
            dataType:'json',
            url: '<?php echo Yii::app()->createUrl('paymentReminders/viewcomment'); ?>',
            success: function (response) {
                $("#remarkList").html(response.result);
                $("#reminder_id").val(id);
                if(response.status =='n'){
                   $(".show_class").show();
                }else{
                    $(".show_class").hide();
                }
           }
        });
    }

    function ignore(id,event){
       var reminder_id=id;
       $.ajax({
            type:"GET",
            data: {reminder_id:reminder_id},
            url: '<?php echo Yii::app()->createUrl('paymentReminders/ignore'); ?>',
            dataType:"json",
            success:function(response){
                if(response.status==1){
                    $('.alertmsg').removeClass(' alert-danger ').addClass(' alert-success ').html('Reminder Ignored Successfully').show();
                    setTimeout(function(){
                        location.reload();
                    },2000);
                    
                }else{
                    $('.alertmsg').removeClass('alert-success').addClass('alert-danger').html('Some Error Occured').show();
                    setTimeout(function(){
                        location.reload();
                    },2000);
                }
               

            }

       });
    }
    function deleteRemainder(id, event){
        let reminder_id = id;
        $.ajax({
            type: "GET",
            data: {reminder_id: reminder_id},
            dataType:"json",
            url: '<?php echo Yii::app()->createUrl('paymentReminders/deleteRemainder'); ?>',
            success:function(response){
                if(response.status==1){
                    $('.alertmsg').removeClass(' alert-danger ').addClass(' alert-success ').html('Reminder Deleted Successfully').show();
                    setTimeout(function(){
                        location.reload();
                    },2000);
                    
                }else{
                    $('.alertmsg').removeClass('alert-success').addClass('alert-danger').html('Some Error Occured').show();
                    setTimeout(function(){
                        location.reload();
                    },2000);
                }
               

            }


        });

    }



  $('#date').click(function (){
            $( this ).datepicker();
            $(this).datepicker('option', 'minDate', new Date());
            $( this ).datepicker({dateFormat: 'dd-mm-yy'}).datepicker("show");


         });
$('#reminder_form').validate({
            rules: {
                date: {
                    required: true,
                },
                status: {
                    required: true,
                },
                comment: {
                    required: true,
                }
            },
            messages: {
                date: {
                    required: "Please select date",
                },
                status: {
                    required: "Please select status",
                },
                comment: {
                    required: "Please enter comment",
                }
            },

        });
    $("#remarkSubmit").click(function() {
    var reminder_id  = $("#reminder_id").val();
    var comment      = $("#comment").val();
    var status       = $('.status').val();
    var date         = $('#date').val();

    if( $('#reminder_form').valid()) {

    var element = $(this);
    $.ajax({
        method: "GET",
        dataType:'json',
        data: {reminder_id: reminder_id, comment: comment, status: status, date:date},
        url: '<?php echo Yii::app()->createUrl('paymentReminders/addcomment'); ?>',
        success: function (response) {
            $("#remarkList").html(response.html);
            $('#addremark').show("slide", { direction: "right" }, 1000);
            $("#reminder_id").val("");
            $("#comment").val("");
            $("#status").val("");
            $("#date").val("");
            element.closest('tr ').removeClass('permission_style');
            location.reload();
        }
    });
    }

});

function myFunction() {
  var checkBox = document.getElementById("myCheck");
  if (checkBox.checked == true){
    $("#status").append("<option value='Completed'>Completed</option>");
  } else {
    $("#status").html("<option value=''>Select status</option><option value='Open'>Open</option> <option value='Extended'>Extended</option>");
  }
}

function editremark(remind_id){
        $('html,body').animate({
            scrollTop: $("body").offset().top
        },'slow');
        var id = remind_id;
        $.ajax({
            type: "GET",
            url: "<?php echo $this->createUrl('paymentReminders/update&layout=1&id=') ?>" + id,
            success: function (response)
            {
                $("#projectform").html(response).slideDown();

            }
        });

    };


function closeaction (event){

     $('#projectform').slideUp();
 }

function closeremark(event){
        $('#addremark').hide("slide", { direction: "right" }, 1000);
        $("#reminder_id").val("");
        $("#comment").val("");
        $("#status").val("");
        $("#date").val("");
        $('#reminder_form').find('input').next('label').css('display','none');
        $('#reminder_form').find('select').next('label').css('display','none');
        $('#reminder_form').find('textarea').next('label').css('display','none');
        $('#reminder_form').find('select').removeClass('error');
}


    function editpro(pro_id){
        $('html,body').animate({
            scrollTop: $("body").offset().top
        },'slow');
        var id = pro_id;
        $.ajax({
            type: "GET",
            url: "<?php echo $this->createUrl('paymentReminders/update&layout=1&id=') ?>" + id,
            success: function (response)
            {
                $("#projectform").html(response).slideDown();

            }
        });

    };

            $(document).ready(function () {
                $('.createpro').click(function () {
                    $.ajax({
                        type: "GET",
                        url: "<?php echo $this->createUrl('paymentReminders/create&layout=1') ?>",
                        success: function (response)
                        {
                            $("#projectform").html(response).slideDown();
                            $('#addremark').hide();
                        }
                    });
                });

            });

            $(".popover-test").popover({
                html: true,
                content: function() {
                  return $(this).next('.popover-content').html();
                }
            });
            $('[data-toggle=popover]').on('click', function (e) {
               $('[data-toggle=popover]').not(this).popover('hide');
            });
            $('body').on('hidden.bs.popover', function (e) {
                $(e.target).data("bs.popover").inState.click = false;
            });
            $('body').on('click', function (e) {
                $('[data-toggle=popover]').each(function () {
                    // hide any open popovers when the anywhere else in the body is clicked
                    if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                        $(this).popover('hide');
                    }
                });
            });



            // remark






</script>
</div>
</div>
<style>

.page-body h3{
    margin:4px 0px;
    color:inherit;
    text-align: left;
}
.panel{border:1px solid #ddd;}
.panel-heading{background-color:#eee;height:40px; }
.error {
            color: #ff0000;
        }
 .close{
    position: absolute;
    top: 7px;
    right: 6px;
}
#addremark .all_remarks{
    overflow: auto;
    margin-top: 5px;
    max-height: 150px;

}
.pur_remark span{
    float:right;
}
.pur_remark span:first-child{
    float:left;
}
.tooltip-hiden{
    width:auto;
}
.empty{display:none;}
@media(min-width: 1400px){
   #addremark .all_remarks{
        max-height: 300px;
   }
}

.popover-content {
    padding: 5px 14px;
}

</style>
