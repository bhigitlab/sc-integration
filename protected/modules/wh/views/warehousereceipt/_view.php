<?php
if($index == 0) {
?>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Warehouse</th>
                            <th>Vendor</th>
                            <th>Clerk</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                    </thead>
<?php } ?>
                    <tr id="<?php echo $index; ?>" class="">
                        <td><?php echo $index+1; ?></td>
                        <td><?php echo $data->warehouse['warehouse_name']; ?></td>
                        <td><?php echo $data->vendor['name']; ?></td>
                        <td><?php echo $data->clerk['first_name']; ?></td>
                        <td><?php echo $data->warehousereceipt_date; ?></td>
                        <td>
                            <a data-id="<?php echo $data->warehousereceipt_id; ?>" data-id="<?php echo $data->warehousereceipt_id; ?>" data-target=".edit" class="fa fa-edit edit_item"></a>
                        </td>
                            
                    </tr>