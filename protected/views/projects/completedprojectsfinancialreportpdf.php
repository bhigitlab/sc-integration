<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
<style>
.text-right{text-align:right}
.text-center{text-align:center}
.grandtotal {background-color: #eee;border:1px solid #000;}
.companyhead{font-size: 18px;font-weight:bold;margin-top:0px;color: #789AD1;}
.img-hold{width: 10%;}
</style>

<div class="container" style="margin:0px 30px">
<?php
    $company_address = Company::model()->findBypk(Yii::app()->user->company_id);
?>
<br><br>
<h2 class="text-center mt-3">Completed Project Report  : <?php echo date('d-M-Y');?></h2>
<h3>Fixed Rate Contracts</h3>
        <table class="table" border="1">
            <thead>
                <tr>
                    <th>Sl No.</th>
                    <th>Project Name</th>
                    <th>SQ. FT.</th>
                    <th>SQ.FT.Rate or Lumpsum</th>
                    <th>Receipts</th>
                    <th>Expenses</th>
                    <th>Invoice</th>
                    <th>Deficit</th>
                    <th>Surplus</th>
                    <th>Remarks</th>
                </tr>
            </thead>
            <tbody>

                    <?php echo $entries; ?>
            </tbody>
        </table>

<h3>Fixed Percentage Contracts</h3>
<table class="table" border="1">
            <thead>
                <tr>
                    <th>Sl No.</th>
                    <th>Project Name</th>
                    <th>SQ. FT.</th>
                    <th>Fixed Percentage</th>
                    <th>Receipts</th>
                    <th>Expenses</th>
                    <th>Service Charge</th>
                    <th>Invoice</th>
                    <th>Deficit</th>
                    <th>Surplus</th>
                    <th>Remarks</th>

                </tr>


            </thead>
            <tbody>

                <?php echo $entries2; ?>
            </tbody>
        </table>
