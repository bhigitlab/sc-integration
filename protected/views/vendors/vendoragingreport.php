<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<?php
//$aging_array = Controller::getVendorAgingSummary();
//echo "<pre>";print_r($aging_array);exit;
?>
<div class="container">
    <div class="clearfix">
        <h2 class="pull-left">Aging Summary Reports</h2>
        <a href="<?php echo Yii::app()->createUrl("vendors/paymentReport")?>" class="pull-right save_btn  save_btn btn btn-info pull-right mt-0 mb-10 margin-right-5" style ="margin-left:5px;">Vendor Payment Report</a>
        <a href="<?php echo Yii::app()->createUrl("subcontractorpayment/report")?>" class="pull-right save_btn save_btn btn btn-info pull-right mt-0 mb-10 margin-right-5">Subcontractor Payment Report</a>
    </div>
    <?php $this->renderPartial('_vendor_ageing_search', array(
                    'model' => $model,
                    'dataProvider' => $dataProvider1,
                    'date_from' => $date_from,
                    'date_to' => $date_to,
                    
                    'vendor_id' => $vendor_id,
                    
                    'day_interval'=>$day_interval,
                    'day_range'=>$day_range
                )) ?>

    <br>
    <?php if(count($aging_array) > 0){ ?>
        <div class="clearfix">
            <div class="pull-right">Total <?php echo count($aging_array) ?> records</div>
        </div>
    <?php } ?>
        <div id="parent">                    
            <table class="table" id="fixTable">
                <thead>
                    <tr>
                        <th>Bill Number</th>
                        <th>Date</th>
                        <th>Party Name</th>
                        <th>Vendor/Subcontractor</th>
                        <th>Date Range</th>
                        <th>No of days since billed</th>
                        <th>No of days Delayed</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $i=1;
                    foreach ($aging_array as $value) { ?>
                        <?php
                            $now = time(); 
                            $your_date = strtotime($value['bill_date']);
                            $datediff = ceil(($now - $your_date)/86400);
                            $delayed = $datediff -$value['payment_date_range']; 
                            if ($datediff <= 15) {
                                $value['payment_date_range'] = '1-15 days';
                            } elseif ($datediff <= 30) {
                                $value['payment_date_range'] = '16-30 days';
                            } elseif ($datediff <= 45) {
                                $value['payment_date_range'] = '31-45 days';
                            } else {
                                $value['payment_date_range'] = '45+ days';
                            }  
                            if($delayed >=0){                 
                            ?>
                            <tr class="pos_row">
                                <td><?php echo $value['bill_number']; ?></td>
                                <td><?php echo date("d-m-Y", strtotime($value['bill_date'])); ?></td>
                                <td><?php echo $value['name']; ?></td>
                                <td><?php echo ucfirst($value['type']); ?></td>
                                <td><?php echo $value['payment_date_range']; ?></td>                    
                                <td><?php echo $datediff ; ?></td>
                                <td class="diff">
                                    <?php echo $delayed ; ?>
                                </td>
                            </tr>
                        <?php } else{ ?>
                            <tr class="neg_row">
                                <td><?php echo $value['bill_number']; ?></td>
                                <td><?php echo date("d-m-Y", strtotime($value['bill_date'])); ?></td>
                                <td><?php echo $value['name']; ?></td>
                                <td><?php echo ucfirst($value['type']); ?></td>
                            <td><?php echo $value['payment_date_range']; ?></td>                    
                                <td><?php echo $datediff ; ?></td>
                                <td class="diff">
                                    <?php echo $delayed ; ?>
                                </td>
                            </tr>
                        <?php }
                        $i++; 
                    }?>
                    
                </tbody>
            </table>
        
        </div>
</div>

<script>
    $(function(){
        var sorted = $('table tbody tr.pos_row').sort(function(a, b) {
            var a = $(a).find('td.diff').text(), b = $(b).find('td.diff').text();
            return b.localeCompare(a, false, {numeric: true})
        });
        

        var sorted1 = $('table tbody tr.neg_row').sort(function(a, b) {
            var a = $(a).find('td.diff').text(), b = $(b).find('td.diff').text();
            return a.localeCompare(b, false, {numeric: true})
        })
        $('table tbody').append(sorted);
        $(sorted1).insertAfter('table tbody tr.pos_row:last');
        // $('table tbody tr.pos_row:last').append(sorted1);

        
    })
    
    $("#fixTable").tableHeadFixer({
        'foot': true,
        'head': true
    });
    $("#submitsearchForm").click(function(){
        var redirectUrl = $(this).attr("href");
        window.location.href=redirectUrl;
    });
    // $(document).ready(function(){
    //     $('.js-example-basic-single').select2();
    // })
</script>

<style>
    #parent {
        max-height: 400px;
        overflow:auto;
    }
</style>
