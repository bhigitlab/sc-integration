<?php
/* @var $this UnitConversionController */
/* @var $model UnitConversion */

$this->breadcrumbs=array(
	'Unit Conversions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UnitConversion', 'url'=>array('index')),
	array('label'=>'Create UnitConversion', 'url'=>array('create')),
	array('label'=>'View UnitConversion', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage UnitConversion', 'url'=>array('admin')),
);
?>

<h1>Update UnitConversion <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>