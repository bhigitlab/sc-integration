<?php
/* @var $this TaxSlabsController */
/* @var $data TaxSlabs */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tax_slab_value')); ?>:</b>
	<?php echo CHtml::encode($data->tax_slab_value); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cgst')); ?>:</b>
	<?php echo CHtml::encode($data->cgst); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sgst')); ?>:</b>
	<?php echo CHtml::encode($data->sgst); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('igst')); ?>:</b>
	<?php echo CHtml::encode($data->igst); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>