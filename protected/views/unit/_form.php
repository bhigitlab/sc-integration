<?php
/* @var $this CompanyExpenseTypeController */
/* @var $model CompanyExpenseType */
/* @var $form CActiveForm */
$pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
?>

<div class="form">

<?php  $form = $this->beginWidget('CActiveForm', array(
        'id'=>'unit-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    )); ?>

	
    <!-- Popup content-->



    <div class="clearfix">
            <div class="row addRow">
            <input type="hidden" id="pms_integration" value="<?php echo $pms_api_integration ;?>">
                <div class="col-md-12">
                    <?php echo $form->labelEx($model,'unit_name'); ?>
                    <?php echo $form->textField($model,'unit_name',array('size'=>50,'maxlength'=>50)); ?>
                    <?php echo $form->error($model,'unit_name'); ?>
                </div>
                
            </div>
            <div class="modal-footer save-btnHold">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?> 
              
                <button data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
                <?php echo CHtml::hiddenField('execute_api', 0);?>
            </div>
    </div><!-- form -->

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function() {
        document.getElementById('').onsubmit = function() {
        var pms_integration = $("#pms_integration").val();
        if(pms_integration =='1'){
            var confirmApiCall = confirm("Do you want to save the unit in Pms?");
            if (confirmApiCall) {
                document.getElementById('execute_api').value = 1;
            }
       }
    };
    });
</script>
