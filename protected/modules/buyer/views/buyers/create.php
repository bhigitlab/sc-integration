<?php
$this->breadcrumbs = array(
	'Buyers' => array('index'),
	'Create',
);
?>
<div class="panel panel-gray">
	<div class="panel-heading form-head">
		<h3 class="panel-title">Add Buyer</h3>
	</div>
	<?php echo $this->renderPartial('_form', array('model' => $model, 'projects' => $projects,'flat_numbers'=>$flat_numbers)); ?>
</div>