<?php
/* @var $this ExpenseTypeController */
/* @var $model ExpenseType */



?>


<div class="panel panel-gray">
	<div class="panel-heading form-head">
		<h3 class="panel-title">Update Tax Slab</h3>
	</div>
	<?php echo $this->renderPartial('_form', array('model' => $model, 'status_values' => $status_values)); ?>
</div>