<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
<div class="container" style="margin:0px  30px;">
<?php
    $company_address = Company::model()->findBypk(Yii::app()->user->company_id);	
?>

<br>
<h4>Deposit List</h4>
<table border="1" class="table table-bordered table-striped tab1" style="width:100%;border:1px solid gray;">
    <thead>
        <tr>
            <th>Sl No.</th>
            <th>Deposit Name</th>            
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 0;
        if ($model == NULL) {
            echo '<tr><td colspan="2">No records Found</td></tr>';
        } else {
            foreach ($model as $bank) {
                $i++;
                ?>

                <tr>	
                    <td><?= $i; ?></td>
                    <td><?php echo $bank['deposit_name']; ?></td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>

