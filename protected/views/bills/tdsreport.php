<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<style>
    .grandtotal th {
        background-color: rgb(247, 240, 240) !important;
    }
</style>
<div class="container" id="project">
    <?php
    $tblpx = Yii::app()->db->tablePrefix;

    $user = Users::model()->findByPk(Yii::app()->user->id);
    $arrVal = explode(',', $user->company_id);
    $query_company = "";
    foreach ($arrVal as $arr) {
        if ($query_company)
            $query_company .= ' OR';
        $query_company .= " FIND_IN_SET('" . $arr . "', id)";
    }
    $query = "";
    foreach ($arrVal as $arr) {
        if ($query)
            $query .= ' OR';
        $query .= " FIND_IN_SET('" . $arr . "', company_id)";
    }
    ?>
    <div class="header-container">
        <h3>TDS Report</h3>
        <?php if (count($tdsdata) > 0) { ?>
            <div class="btn-container">
                <?php if (
                    (isset(
                        Yii::app()->
                            user->role
                    ) && (in_array("/bills/tdstopdf", Yii::app()->user->menuauthlist)))
                ) { ?>
                    <!-- PDF Button -->
                    <button type="button" class="btn btn-info" id="pdf-btn" title="SAVE AS PDF" style="cursor:pointer;"
                        href="<?php echo Yii::app()->createAbsoluteUrl("bills/tdstopdf", array('fromdate' => $date_from, 'date_to' => $date_to, 'company_id' => $company_id, 'vendor_id' => $vendor_id, 'subcontractor_id' => $subcontractor_id)); ?>">
                        <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
                    </button>
                <?php } ?>
                <?php if ((isset(Yii::app()->user->role) && (in_array("/bills/tdstoexcel", Yii::app()->user->menuauthlist)))) { ?>
                    <!-- Excel Button -->
                    <button type="button" class="btn btn-info" style="cursor:pointer;" id="excel-btn" title="Excel"
                        href="<?php echo Yii::app()->createAbsoluteUrl("bills/tdstoexcel", array('fromdate' => $date_from, 'date_to' => $date_to, 'company_id' => $company_id, 'vendor_id' => $vendor_id, 'subcontractor_id' => $subcontractor_id)); ?>">
                        <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>
                    </button>
                <?php } ?>
            </div>
        <?php } ?>
    </div>

    <div id="contenthtml" class="">
        <div class="page_filter clearfix ">
            <form id="projectform" action="<?php $url = Yii::app()->createAbsoluteUrl("bills/tdsreport"); ?>"
                method="POST">
                <div class="search-form">
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-3 col-md-2">
                            <?php echo CHtml::label('From ', ''); ?>
                            <?php echo CHtml::textField('date_from', (!empty($date_from) ? date('Y-m-d', strtotime($date_from)) : ""), array("id" => "date_from", "class" => "form-control", "readonly" => true)); ?>
                        </div>
                        <div class="form-group col-xs-12 col-sm-3 col-md-2">
                            <?php echo CHtml::label('To ', ''); ?>
                            <?php echo CHtml::textField('date_to', (!empty($date_to) ? date('Y-m-d', strtotime($date_to)) : ""), array("id" => "date_to", "class" => "form-control", "readonly" => true)); ?>
                        </div>
                        <div class="form-group col-xs-12 col-sm-3 col-md-2">
                            <label for="vendor">Vendor </label>
                            <?php
                            echo CHtml::dropDownList('vendor_id', 'vendor_id', CHtml::listData(Vendors::model()->findAll(array(
                                'select' => array('vendor_id, name'),
                                'order' => 'name',
                                'condition' => '(' . $query . ')',
                                'distinct' => true
                            )), 'vendor_id', 'name'), array('empty' => 'Select Vendor', 'id' => 'vendor_id', 'class' => 'form-control', 'options' => array($vendor_id => array('selected' => true))));
                            ?>
                        </div>
                        <div class="form-group col-xs-12 col-sm-3 col-md-2">
                            <label for="sub">Subcontractor</label>
                            <?php
                            echo CHtml::dropDownList('subcontractor_id', 'subcontractor_id', CHtml::listData(Subcontractor::model()->findAll(array(
                                'select' => array('subcontractor_id, subcontractor_name'),
                                'order' => 'subcontractor_name',
                                'condition' => '(' . $query . ')',
                                'distinct' => true
                            )), 'subcontractor_id', 'subcontractor_name'), array('empty' => 'Select Subcontractor', 'id' => 'subcontractor_id', 'class' => 'form-control', 'options' => array($subcontractor_id => array('selected' => true))));
                            ?>
                        </div>
                        <div class="form-group col-xs-12 col-sm-3 col-md-2">
                            <label for="comapny">Company</label>
                            <?php
                            echo CHtml::dropDownList('company_id', 'company_id', CHtml::listData(Company::model()->findAll(array(
                                'select' => array('id, name'),
                                'order' => 'id DESC',
                                'condition' => '(' . $query_company . ')',
                                'distinct' => true
                            )), 'id', 'name'), array('empty' => 'Select Company', 'id' => 'company_id', 'class' => 'form-control', 'options' => array($company_id => array('selected' => true))));
                            ?>
                        </div>
                        <div class="form-group col-xs-12 col-sm-3 col-md-2 text-sm-left text-right">
                            <label class="d-sm-block d-none">&nbsp;</label>
                            <div>
                                <?php echo CHtml::submitButton('GO', array('id' => 'btSubmit', 'class' => 'btn btn-sm btn-primary')); ?>
                                <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('bills/tdsreport') . '"', 'class' => ' btn btn-sm btn-default')); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="mt">
        <?php if (count($tdsdata) > 0) { ?>
            <div class="text-right">
                Total
                <?php echo count($tdsdata); ?> results
            </div>
            <div id="table-wrapper">

                <table class="table  total-table ">
                    <thead class="entry-table">
                        <tr class="first-thead-row">
                            <th>Sl No</th>
                            <th>Project</th>
                            <th>Date</th>
                            <th>Vendor</th>
                            <th>Subcontractor</th>
                            <th>Amount Paid</th>
                            <th>TDS (%)</th>
                            <th>TDS Amount</th>
                            <th>Balance</th>
                        </tr>
                        <?php
                        $totalpaid = 0;
                        $totaltdsp = 0;
                        $totaltdsamount = 0;
                        $totalpaidamount = 0;
                        $i = 1;
                        foreach ($tdsdata as $data) {
                            $totalpaid = $totalpaid + $data["paid"];
                            $totaltdsp = $totaltdsp + $data["tdsp"];
                            $totaltdsamount = $totaltdsamount + $data["tdsamount"];
                            $totalpaidamount = $totalpaidamount + $data["balance"];
                        }
                        $averagetdsp = $totaltdsp / count($tdsdata);
                        $averagetdsp = $totaltdsp / count($tdsdata);

                        ?>
                        <tr class="grandtotal second-thead-row">
                            <th colspan="5" class="text-right">Total</th>
                            <th class="text-right">
                                <?php echo Controller::money_format_inr($totalpaid, 2); ?>
                            </th>
                            <th class="text-right">
                                <?php echo Controller::money_format_inr($averagetdsp, 2); ?>
                            </th>
                            <th class="text-right">
                                <?php echo Controller::money_format_inr($totaltdsamount, 2); ?>
                            </th>
                            <th class="text-right">
                                <?php echo Controller::money_format_inr($totalpaidamount, 2); ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $totalpaid = 0;
                        $totaltdsp = 0;
                        $totaltdsamount = 0;
                        $totalpaidamount = 0;
                        $i = 1;
                        foreach ($tdsdata as $data) {
                            $totalpaid = $totalpaid + $data["paid"];
                            $totaltdsp = $totaltdsp + $data["tdsp"];
                            $totaltdsamount = $totaltdsamount + $data["tdsamount"];
                            $totalpaidamount = $totalpaidamount + $data["balance"];
                            ?>
                            <tr>
                                <td>
                                    <?php echo $i; ?>
                                </td>
                                <td>
                                    <?php echo $data["project"]; ?>
                                </td>
                                <td>
                                    <?php echo date("d-m-Y", strtotime($data["date"])); ?>
                                </td>
                                <td>
                                    <?php echo $data["vendor"]; ?>
                                </td>
                                <td>
                                    <?php echo $data["subcontractor"]; ?>
                                </td>
                                <td class="text-right">
                                    <?php echo Controller::money_format_inr(floatval(round($data["paid"], 2)), 2); ?>
                                </td>
                                <td class="text-right">
                                    <?php echo Controller::money_format_inr(floatval(round($data["tdsp"], 2)), 2); ?>
                                </td>
                                <td class="text-right">
                                    <?php echo Controller::money_format_inr(floatval(round($data["tdsamount"], 2)), 2); ?>
                                </td>
                                <td class="text-right">
                                    <?php echo Controller::money_format_inr(floatval(round($data["balance"], 2)), 2); ?>
                                </td>
                            </tr>
                            <?php
                            $i = $i + 1;
                        }
                        $averagetdsp = $totaltdsp / count($tdsdata);
                        ?>
                    </tbody>
                </table>
            </div>

        <?php } else {
            echo "No records found.";
        } ?>
    </div>

</div>
<script>
    $(document).ready(function () {
        $("#fixTable").tableHeadFixer({ 'left': false, 'foot': true, 'head': true });
        $("#fixTable2").tableHeadFixer({ 'left': false, 'foot': true, 'head': true });

        $("#date_from").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: $("#date_to").val()
        });
        $("#date_to").datepicker({
            dateFormat: 'yy-mm-dd',
        });
        $("#date_from").change(function () {
            $("#date_to").datepicker('option', 'minDate', $(this).val());
        });
        $("#date_to").change(function () {
            $("#date_from").datepicker('option', 'maxDate', $(this).val());
        });
    });
    $("#excel-btn").click(function () {
        var redirectUrl = $(this).attr("href");
        window.location.href = redirectUrl;
    });
    $("#pdf-btn").click(function () {
        var redirectUrl = $(this).attr("href");
        window.location.href = redirectUrl;
    });
</script>
<style>
    #parent,
    #parent2 {
        max-height: 400px;
    }

    #parent table th,
    #parent2 table th {
        background-color: #eee;
    }

    /* .table-bordered>thead>tr>th{background-color: rgb(238, 238, 238) !important; */
    }

    .list-view .sorter {
        margin-bottom: 0px;
    }

    .table {
        margin-bottom: 0px;
    }
</style>