<?php
/* @var $this ExpensesController */
/* @var $model Expenses */

$this->breadcrumbs = array(
    'Expenses' => array('index'),
    'Create',
);

/* $this->menu=array(
    array('label'=>'List Expenses', 'url'=>array('index')),
    array('label'=>'Manage Expenses', 'url'=>array('admin')),
); */
?>
<div class="container" id="expense">
    <div class="expenses-heading">
        <div class="clearfix">
            <h2>DUPLICATE SUBCONTRACTOR PAYMENT ENTRIES</h2>
        </div>
    </div>
    <div class="popwindow" style="display:none;">
        <div id="details">

        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Company</th>
                            <th>Subcontractor</th>
                            <th>Project</th>
                            <th>Description</th>
                            <th>Transaction Type</th>
                            <th>Bank</th>
                            <th>Cheque No</th>
                            <th>Amount</th>
                            <th>Tax Amount</th>
                            <th>TDS Amount</th>
                            <th>Paid Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($duplicateentry as $entry) {
                            ?>
                            <tr class="duplicaterow" id="duplicaterow<?php echo $i; ?>" data-id="<?php echo $i; ?>"
                                title="Click here to view <?php echo $entry["rowcount"]; ?> similar entries.">
                                <td>
                                    <?php echo $i; ?>
                                </td>
                                <td>
                                    <?php
                                    $company = Company::model()->findByPk($entry["company_id"]);
                                    echo $company->name;
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $subcon = Subcontractor::model()->findByPk($entry["subcontractor_id"]);
                                    echo $subcon->subcontractor_name;
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $project = Projects::model()->findByPk($entry["project_id"]);
                                    echo $project->name;
                                    ?>
                                </td>
                                <td>
                                    <?php echo $entry["description"] ? $entry["description"] : ""; ?>
                                </td>
                                <td>
                                    <?php
                                    $ptype = Status::model()->findByPk($entry["payment_type"]);
                                    echo $ptype->caption;
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $bank = Bank::model()->findByPk($entry["bank"]);
                                    if ($bank)
                                        echo $bank->bank_name;
                                    ?>
                                </td>
                                <td>
                                    <?php echo $entry["cheque_no"] ? $entry["cheque_no"] : ""; ?>
                                </td>
                                <td>
                                    <?php echo $entry["amount"] ? Controller::money_format_inr($entry["amount"], 2) : ""; ?>
                                </td>
                                <td><span class="popover-test tax_amnt" data-toggle="popover" data-trigger="hover"
                                        data-placement="left" data-trigger="hover" type="button" data-html="true">
                                        <?php echo $entry["tax_amount"] ? Controller::money_format_inr($entry["tax_amount"], 2) : ""; ?>
                                    </span>
                                <td><span class="popover-test tds_amnt" data-toggle="popover" data-trigger="hover"
                                        data-placement="left" data-trigger="hover" type="button" data-html="true">
                                        <?php echo $entry["tds_amount"] ? Controller::money_format_inr($entry["tds_amount"], 2) : ""; ?>
                                    </span>
                                <td><span class="popover-test paid_amnt" data-toggle="popover" data-trigger="hover"
                                        data-placement="left" data-trigger="hover" type="button" data-html="true">
                                        <?php echo $entry["paidamount"] ? Controller::money_format_inr($entry["paidamount"], 2) : ""; ?>
                                    </span>
                                    <div class="popover-content hide">
                                        <div><span>SGST: (
                                                <?php echo $entry["sgst"] ? $entry["sgst"] : ""; ?>&#37;)
                                                <?php echo $entry["sgst_amount"] ? Controller::money_format_inr($entry["sgst_amount"], 2) : ""; ?>
                                            </span></div>
                                        <div><span>CGST: (
                                                <?php echo $entry["cgst"] ? $entry["cgst"] : ""; ?>&#37;)
                                                <?php echo $entry["cgst_amount"] ? Controller::money_format_inr($entry["cgst_amount"], 2) : ""; ?>
                                            </span></div>
                                        <div><span>IGST: (
                                                <?php echo $entry["igst"] ? $entry["igst"] : ""; ?>&#37;)
                                                <?php echo $entry["igst_amount"] ? Controller::money_format_inr($entry["igst_amount"], 2) : ""; ?>
                                            </span></div>
                                    </div>
                                </td>
                                <input type="hidden" name="subcontractor[]" id="subcontractor<?php echo $i; ?>"
                                    value="<?php echo $entry["subcontractor_id"]; ?>" />
                                <input type="hidden" name="project[]" id="project<?php echo $i; ?>"
                                    value="<?php echo $entry["project_id"]; ?>" />
                                <input type="hidden" name="amount[]" id="amount<?php echo $i; ?>"
                                    value="<?php echo $entry["amount"]; ?>" />
                                <input type="hidden" name="date[]" id="date<?php echo $i; ?>"
                                    value="<?php echo $entry["date"]; ?>" />
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<style>
    .popwindow {
        position: fixed;
        top: 94px;
        left: 70px;
        right: 70px;
        z-index: 2;
        max-width: 1150px;
        margin: 0 auto;
    }

    #details {
        background-color: #fff;
        border-color: #fff;
        box-shadow: 0px 0px 8px 6px rgba(0, 0, 0, 0.25);
        color: #555;
        padding: 10px;
        border-radius: 4px;
    }
</style>
<script>
    $(document).ready(function () {
        $(".duplicaterow").click(function (e) {
            $(".popwindow").show();
            var row = $(this).attr("data-id");
            var subcontractor = $("#subcontractor" + row).val();
            var project = $("#project" + row).val();
            var amount = $("#amount" + row).val();
            var date = $("#date" + row).val();
            $.ajax({
                type: "GET",
                data: { subcontractor: subcontractor, project: project, amount: amount, date: date },
                url: "<?php echo $this->createUrl('subcontractorpayment/getduplicateentries') ?>",
                success: function (data) {
                    $("#details").html(data);
                }
            });
        });
        $(document).on('click', '#details', function (e) {
            $(".popwindow").hide();
            $("#details").html("");
        });
    });
</script>