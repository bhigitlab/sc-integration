<?php
$company_address = Company::model()->findBypk($model->company_id);
?>
<?php
$tblpx = Yii::app()->db->tablePrefix;
?>


<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
<div class="container"   style="margin:0px 30px">

    

    <table class="details">
        <tr>
            <td><label>COMPANY : </label><span><?php echo $company_address['name']; ?></span></td>
            <td><label>PROJECT : </label><span><?php echo $project[0]['name']; ?></span></td>
        </tr>
        <tr>
            <td><label>VENDOR : </label><span><?php echo (isset($sql['vendor'])) ? $sql['vendor'] : ''; ?></span></td>
            <td><label>DATE : </label><span><?php echo date("d-m-Y", strtotime($model['bill_date'])); ?></span></td>

        </tr>

        <tr>
            <td><label>PURCHASE NO : </label><span><?php echo (isset($purchase['purchase_no'])) ? $purchase['purchase_no'] : ''; ?></span></td>
            <td><label>BILL NO : </label><span><?php echo $model['bill_number']; ?></span></td>
        </tr>

    </table>

    <br /><br />

    <table border="1" style="width:100%;">
        <thead>
            <tr>
                <th>Sl.No</th>
                <th>Specifications/Remark</th>
                <th>HSN Code</th>
                <th>Quantity</th>
                <th>Unit</th>
                <th>Rate</th>
                <th>Amount</th>
                <th>Tax Slab (%)</th>
                <th>Discount</th>
                <th>Discount (%)</th>
                <th>CGST</th>
                <th>CGST (%)</th>
                <th>SGST</th>
                <th>SGST (%)</th>
                <th>IGST</th>
                <th>IGST (%)</th>
                <th>Total Tax</th>
                <th>Total Tax (%)</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody class="addrow">
            <?php
            if (!empty($newmodel)) {
                // echo '<pre>';
                // print_r($newmodel);
                // exit;
                $items = $newmodel['items'];
                $additional_items = $newmodel['add_items'];
                $i = 1;
                foreach ($items as $value) {
            ?>
                    <tr>
                        <td colspan="19"><?php echo !empty($value['category_title']) ? $value['category_title'] : '' ?></td>
                    </tr>
                    <?php
                    foreach ($value as $item) {

                        if (isset($item['specification'])) {
                    ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $item['specification']; ?></td>
                                <td><?php echo $item['hsn_code']; ?></td>
                                <td><?php echo $item['quantity']; ?></td>
                                <td><?php echo $item['unit'];; ?></td>
                                <td class="text-right"><?php echo number_format((float) $item['rate'], 2, '.', ''); ?></td>
                                <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($item['amount'], 2, 1); ?></td>
                                <td class="text-right"><?php echo $item['tax_slab']; ?></td>
                                <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($item['disc'], 2, 1); ?></td>
                                <td class="text-right"><?php echo $item['discp']; ?></td>
                                <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($item['cgst'], 2, 1); ?></td>
                                <td class="text-right"><?php echo $item['cgstp']; ?></td>
                                <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($item['sgst'], 2, 1); ?></td>
                                <td class="text-right"><?php echo $item['sgstp']; ?></td>
                                <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($item['igst'], 2, 1); ?></td>
                                <td class="text-right"><?php echo $item['igstp']; ?></td>
                                <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($item['tot_tax'], 2, 1); ?></td>
                                <td class="text-right"><?php echo $item['taxpercent']; ?></td>
                                <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr(($item['amount'] - $item['disc']) + $item['tot_tax'], 2, 1); ?></td>
                            </tr>
                    <?php
                            $i++;
                        }
                    } ?>

                <?php
                }
                if (!empty($additional_items)) {
                ?>
                    <tr>
                        <td colspan="19" class="categ_td text-center">Additional Items</td>
                    </tr>
                    <?php


                    foreach ($additional_items as $value) {
                    ?>
                        <tr>
                            <td colspan="19" class="categ_td"><?php echo !empty($value['category_title']) ? $value['category_title'] : '' ?></td>
                        </tr>
                        <?php
                        foreach ($value as $item) {

                            if (isset($item['specification'])) {
                        ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $item['specification']; ?></td>
                                    <td><?php echo $item['hsn_code']; ?></td>
                                    <td><?php echo $item['quantity']; ?></td>
                                    <td><?php echo $item['unit'];; ?></td>
                                    <td class="text-right"><?php echo number_format((float) $item['rate'], 2, '.', ''); ?></td>
                                    <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($item['amount'], 2, 1); ?></td>
                                    <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($item['tax_slab'], 2, 1); ?></td>
                                    <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($item['disc'], 2, 1); ?></td>
                                    <td class="text-right"><?php echo $item['discp']; ?></td>
                                    <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($item['cgst'], 2, 1); ?></td>
                                    <td class="text-right"><?php echo $item['cgstp']; ?></td>
                                    <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($item['sgst'], 2, 1); ?></td>
                                    <td class="text-right"><?php echo $item['sgstp']; ?></td>
                                    <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($item['igst'], 2, 1); ?></td>
                                    <td class="text-right"><?php echo $item['igstp']; ?></td>
                                    <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($item['tot_tax'], 2, 1); ?></td>
                                    <td class="text-right"><?php echo $item['taxpercent']; ?></td>
                                    <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr(($item['amount'] - $item['disc']) + $item['tot_tax'], 2, 1); ?></td>
                                </tr>
                        <?php
                            }
                        }
                        ?>
            <?php

                    }
                }
            }
            ?>
        </tbody>
    </table>
    <br /><br />
    <?php

    $addchargesall   =  Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $model['bill_id'])->queryAll(); ?>

    <?php if (!empty($addchargesall)) { ?>
        <h4>Additional Charges </h4>

        <?php foreach ($addchargesall as $data) { ?>

            <div> <span style="font-weight: bold;"><?= $data['category'] ?></span> : <span class='totalprice1'><?= Yii::app()->Controller->money_format_inr($data['amount'], 2) ?></span></div>
    <?php }
    } ?>

    <?php
    $addcharges  =  Yii::app()->db->createCommand("SELECT ROUND(SUM(amount), 2) as amount FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $model['bill_id'])->queryRow();
    $tot_amount = $model['bill_totalamount'];
    if (!empty($addcharges['amount'])) {
        $tot_amount = $tot_amount + $addcharges['amount'];
    }
    ?>
    <br>
    <br>

    <table border="1" align="right" style="width:50%; float: right; font-size:14px;">
        <tr>
            <td>Amount</td>
            <td style="text-align:right;"><?php echo ($model['bill_amount'] != '') ? Yii::app()->Controller->money_format_inr($model['bill_amount'], 2, 1)  : '0.00'; ?></td>
        </tr>
        <tr>
            <td>Discount Amount</td>
            <td style="text-align:right;"><?php echo ($model['bill_discountamount'] != '') ?  Yii::app()->Controller->money_format_inr($model['bill_discountamount'], 2, 1) : '0.00'; ?></td>
        </tr>
        <tr>
            <td>Tax Amount</td>
            <td style="text-align:right;"><?php echo ($model['bill_taxamount'] != '') ?  Yii::app()->Controller->money_format_inr($model['bill_taxamount'], 2, 1) : '0.00'; ?></td>
        </tr>
        <?php if ($addcharges['amount'] != '') {  ?>
            <tr>
                <td>Additional Charge</td>
                <td style="text-align:right;"><?php echo $addcharges['amount']; ?></td>
            </tr>
        <?php } ?>
        <tr>
            <td>Grand Total</td>
            <td style="text-align:right;"><?php echo ($tot_amount != '') ? Controller::money_format_inr($tot_amount, 2) : '0.00'; ?></td>
        </tr>
    </table>

    <h4>Authorized Signatory,</h4>
    <h4></h4>
</div>