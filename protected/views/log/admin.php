<?php
/* @var $this LogController */
/* @var $model JpLog */

$this->breadcrumbs=array(
	'Jp Logs'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List JpLog', 'url'=>array('index')),
	array('label'=>'Create JpLog', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#jp-log-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Jp Logs</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'jp-log-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'log_id',
		'log_data',
		'log_table',
		'log_primary_key',
		'log_datetime',
		'log_action',
		/*
		'log_action_by',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
