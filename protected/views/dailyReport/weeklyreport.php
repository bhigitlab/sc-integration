<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs = array(
    'Purchase List',
)
    ?>
<?php
$datefrom = '';
$dateto = '';
// Calculate default date_from (previous Saturday) and date_to (next Friday)
if (!isset($_GET['date_from']) && !isset($_GET['date_to'])) {
    $currentDate = date('d-m-Y');
    $previousSaturday = date('d-m-Y', strtotime('last Saturday', strtotime($currentDate)));
    if (date('l', strtotime($currentDate)) === 'Saturday') {
        $previousSaturday = $currentDate;
    }
    $upcomingFriday = date('d-m-Y', strtotime('next Friday', strtotime($currentDate)));
    if (date('l', strtotime($currentDate)) === 'Friday') {
        $upcomingFriday = $currentDate;
    }

    // Use default values if GET parameters are empty
    $datefrom = isset($_GET['date_from']) && $_GET['date_from'] !== '' ? $_GET['date_from'] : $previousSaturday;
    $dateto = isset($_GET['date_to']) && $_GET['date_to'] !== '' ? $_GET['date_to'] : $upcomingFriday;
}
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<div class="container" id="project">
    <div class="expenses-heading header-container">
        <h3 class="pull-left">Labour Report</h3>
        <div class="btn-container">
            <button type="button" id="excel-btn1" class=" savepdf save_btn  btn btn-info"
                href="<?php echo Yii::app()->createUrl('dailyReport/weeklyexcel', array('project_id' => isset($_GET['project_id']) ? $_GET['project_id'] : "", 'subcontractor_id' => isset($_GET['subcontractor_id']) ? $_GET['subcontractor_id'] : "", 'datefrom' => isset($_GET['date_from']) ? $_GET['date_from'] : $datefrom, 'dateto' => isset($_GET['date_to']) ? $_GET['date_to'] : $dateto)) ?>"><i
                    class="fa fa-file-excel-o button-icon" aria-hidden="true"></i></button>
            <button type="button" id="pdf-btn" class="btn btn-info savepdf save_btn "
                href="<?php echo Yii::app()->createUrl('dailyReport/weeklypdf', array('project_id' => isset($_GET['project_id']) ? $_GET['project_id'] : "", 'subcontractor_id' => isset($_GET['subcontractor_id']) ? $_GET['subcontractor_id'] : "", 'datefrom' => isset($_GET['date_from']) ? $_GET['date_from'] : $datefrom, 'dateto' => isset($_GET['date_to']) ? $_GET['date_to'] : $dateto)) ?>"><i
                    class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i></button>
        </div>
    </div>


    <?php

    $user = Users::model()->findByPk(Yii::app()->user->id);
    $arrVal = explode(',', $user->company_id);
    $newQuery = "";
    foreach ($arrVal as $arr) {
        if ($newQuery)
            $newQuery .= ' OR';
        $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
    }
    ?>

    <?php
    $this->renderPartial('_newsearch', array('model' => $model, 'project' => $project, 'subcontractor' => $subcontractor, 'expense' => $expense, 'dateto' => $dateto, 'datefrom' => $datefrom)) ?>

    <?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="info"
            style="width: 500px;margin-left: 400px;padding: 8px;color: green;font-size: 20px;font-weight: bold; padding-top: 46px;">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>


    <?php if (Yii::app()->user->hasFlash('error')): ?>
        <div class="info"
            style="width: 500px;margin-left: 400px;padding: 8px;color: red;font-size: 20px;font-weight: bold; padding-top: 46px;">
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
    <?php endif; ?>


    <div id="msg_box"></div>
    <div class="table-wrapper margin-top-20">


        <div class="d-flex">
            <b class="margin-bottom-0 " style="color:red" ;>
                **NOTE: REG refers to Regular And OT refers to Overtime
            </b>
            <div class="summary text-right margin-left-auto">Total
                <?php echo count($final_array); ?> results
            </div>
        </div>
        <div id="table-wrapper" class="pdf_spacing">
            <table id="weekly_tbl" class="weekly_tbl">
                <thead class="entry-table">
                    <tr>
                        <th>SI No</th>
                        <th>Subcontractor</th>
                        <th>Project</th>
                        <th>Description</th>

                        <th style="width: 200px;">Approved Head Count</th>

                        <th>REGULAR / OT Expense</th>
                        <th>Total Expense</th>
                        <th>Paid Amount</th>
                        <th>Payment to be done</th>
                        <th style="width: 150px;">Unpproved Head Count</th>
                        <th>REGULAR / OT Expense</th>
                        <th>Total Unapprove Expense</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $total_amount = 0;
                    //echo "<pre>";print_r($final_array);exit;
                    foreach ($final_array as $key => $data) {
                        if ($data['total_advance'] > 0 || $data['total_payment'] > 0 || $data['total_unapprove'] > 0 || $data['total_paid'] > 0) {
                            $total_amount += $data['total_paid'];

                            ?>
                            <tr>
                                <th>
                                    <?php echo $key + 1; ?>
                                </th>
                                <th>
                                    <?php echo isset($data['subcontractor']) ? $data['subcontractor'] : 'No Subcontractor'; ?>
                                </th>
                                <th class="text-right ">Total</th>
                                <th></th>
                                <th></th>


                                <th></th>
                                <th class="text-right ">
                                    <?php echo Yii::app()->Controller->money_format_inr($data['total_payment'], 2); ?>
                                </th>
                                <th class="text-right ">
                                    <?php echo Yii::app()->Controller->money_format_inr($data['total_advance'], 2); ?>
                                </th>
                                <th class="text-right ">
                                    <?php echo Yii::app()->Controller->money_format_inr($data['total_paid'], 2); ?>
                                </th>
                                <th></th>
                                <th></th>
                                <th class="text-right ">
                                    <?php echo Yii::app()->Controller->money_format_inr($data['total_unapprove'], 2); ?>
                                </th>
                            </tr>
                            <?php
                            if (isset($data['payment']) && !empty($data['payment'])) {
                                foreach ($data['payment'] as $key => $values) {
                                    ?>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <?php echo $values['project']; ?>
                                        </td>
                                        <td>
                                            <?php echo $values['description']; ?>
                                        </td>

                                        <td style="width: 50px;  white-space: normal;">

                                            <div>
                                                <?php
                                                $list_counts_arr = array();
                                                if (!empty($values['approved_reg_headcount'])) {
                                                    $list_counts_arr = explode(',', $values['approved_reg_headcount']);

                                                }
                                                if (!empty($values['approved_reg_headcount'])) {
                                                    echo '<b>REG:</b> <ul>';
                                                    foreach ($list_counts_arr as $arr) {
                                                        echo '<li>' . $arr . '</li>';

                                                    }
                                                    echo "</ul>";

                                                } else {
                                                    echo "";
                                                } ?>
                                            </div>
                                            <?php if (!empty($values['approved_reg_headcount']) && !empty($values['approved_ot_headcount'])) {
                                                echo '<div style="border-bottom: 1px solid #ccc; margin: 4px 0;"></div>';
                                            } ?>

                                            <div>
                                                <?php
                                                $list_ot_counts_arr = array();
                                                if (!empty($values['approved_ot_headcount'])) {
                                                    $list_ot_counts_arr = explode(',', $values['approved_ot_headcount']);

                                                }
                                                if (!empty($values['approved_ot_headcount'])) {
                                                    echo '<b class="pink-txt-color">OT:</b> <ul>';
                                                    foreach ($list_ot_counts_arr as $ot_arr) {
                                                        echo "<li>" . $ot_arr . "</li>";
                                                    }

                                                    echo "<ul>";
                                                }
                                                ?>
                                            </div>
                                        </td>

                                        <td>
                                            <div class="text-right ">
                                                <?php echo !empty($values['approved_reg_expense']) ? Yii::app()->Controller->money_format_inr($values['approved_reg_expense'], 2) . ' ' : ''; ?>
                                            </div>
                                            <?php if (!empty($values['approved_reg_expense']) && !empty($values['approved_ot_expense'])) {
                                                echo '<div style="border-bottom: 1px solid #ccc; margin: 4px 0;"></div>';
                                            } ?>

                                            <div class="text-right ">
                                                <?php echo !empty($values['approved_ot_expense']) ? Yii::app()->Controller->money_format_inr($values['approved_ot_expense'], 2) : ''; ?>
                                            </div>
                                        </td>
                                        <td class="text-right ">
                                            <?php echo Yii::app()->Controller->money_format_inr($values['payment'], 2); ?>
                                        </td>
                                        <td class="text-right ">
                                            <?php echo Yii::app()->Controller->money_format_inr($values['advance'], 2); ?>
                                        </td>
                                        <td class="text-right ">
                                            <?php echo Yii::app()->Controller->money_format_inr($values['paid'], 2); ?>
                                        </td>

                                        <td>
                                            <div>
                                                <?php
                                                $unlist_counts_arr = array();
                                                if (!empty($values['unapproved_reg_headcount'])) {
                                                    $unlist_counts_arr = explode(',', $values['unapproved_reg_headcount']);

                                                }
                                                if (!empty($values['unapproved_reg_headcount'])) {
                                                    echo '<b>REG:</b> <ul>';
                                                    foreach ($unlist_counts_arr as $arr) {
                                                        echo '<li>' . $arr . '</li>';

                                                    }
                                                    echo "</ul>";

                                                } else {
                                                    echo "";
                                                } ?>
                                            </div>
                                            <?php if (!empty($values['unapproved_reg_headcount']) && !empty($values['unapproved_ot_headcount'])) {
                                                echo '<div style="border-bottom: 1px solid #ccc; margin: 4px 0;"></div>';
                                            } ?>

                                            <div>
                                                <?php
                                                $list_ot_counts_arr = array();
                                                if (!empty($values['unapproved_ot_headcount'])) {
                                                    $list_ot_counts_arr = explode(',', $values['unapproved_ot_headcount']);

                                                }
                                                if (!empty($values['unapproved_ot_headcount'])) {
                                                    echo '<b class="pink-txt-color">OT:</b> <ul>';
                                                    foreach ($list_ot_counts_arr as $ot_arr) {
                                                        echo "<li>" . $ot_arr . "</li>";
                                                    }

                                                    echo "<ul>";
                                                } else {
                                                    echo "";
                                                }
                                                ?>
                                            </div>


                                        </td>
                                        <td>
                                            <div class="text-right ">
                                                <?php echo !empty($values['unapproved_reg_expense']) ? Yii::app()->Controller->money_format_inr($values['unapproved_reg_expense'], 2) . ' ' : ''; ?>
                                            </div>
                                            <?php if (!empty($values['unapproved_reg_expense']) && !empty($values['unapproved_ot_expense'])) {
                                                echo '<div style="border-bottom: 1px solid #ccc; margin: 4px 0;"></div>';
                                            } ?>

                                            <div class="text-right ">
                                                <?php echo !empty($values['unapproved_ot_expense']) ? Yii::app()->Controller->money_format_inr($values['unapproved_ot_expense'], 2) : ''; ?>
                                            </div>
                                        </td>
                                        <td class="text-right ">
                                            <?php echo Yii::app()->Controller->money_format_inr($values['unapprove'], 2); ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                        }
                    } ?>
                </tbody>
                <tfoot>
                    <th colspan="8" class="text-right"> Total</th>
                    <th class="text-right ">
                        <?php echo Yii::app()->Controller->money_format_inr($total_amount, 2); ?>
                    </th>
                    <th colspan="3"></th>

                </tfoot>
            </table>
        </div>
    </div>
    <!-- Separate Advance Details Table -->
    <div class="row margin-top-10">
        <div class="col-xs-12">
            <div class="heading-title">Advance Payment Details</div>
            <div class="dotted-line"></div>
        </div>
    </div>
    <div class="table-wrapper margin-top-20">
        <div id="table-wrapper" class="pdf_spacing">

            <table id="advance_tbl" class="advance_tbl">
                <thead class="entry-table">
                    <tr>
                        <th>SI No</th>
                        <th>Subcontractor</th>
                        <th>Date</th>
                        <th>Description</th>
                        <th>Project</th>

                        <th>Advance Amount</th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    $advance_count = 1;
                    $i = 0;
                    foreach ($final_array as $data) {
                        if ($data['total_advance'] > 0 || $data['total_payment'] > 0 || $data['total_unapprove'] > 0 || $data['total_paid'] > 0) {
                            ?>
                            <tr>
                                <th>
                                    <?php echo $i + 1; ?>
                                </th>
                                <th>
                                    <?php echo isset($data['subcontractor']) ? $data['subcontractor'] : 'No Subcontractor'; ?>
                                </th>

                                <th></th>
                                <th></th>
                                <th class="text-right ">Total</th>
                                <th class="text-right ">
                                    <?php echo Yii::app()->Controller->money_format_inr($data['total_advance'], 2); ?>
                                </th>
                            </tr>

                            <?php if (isset($data['payment']) && !empty($data['payment'])) {
                                $k = 0;
                                foreach ($data['payment'] as $payment_key => $values) {

                                    if (!empty($values['advance_details'])) {

                                        foreach ($values['advance_details'] as $advance) {
                                            ?>
                                            <tr>
                                                <td class="text-right">
                                                    <?php echo $k + 1; ?>
                                                </td>
                                                <td></td>
                                                <td>
                                                    <?php echo date('d-m-Y', strtotime($advance['date'])); ?>
                                                </td>
                                                <td>
                                                    <?php echo $advance['description']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $values['project']; ?>
                                                </td>

                                                <td class="text-right">
                                                    <?php echo isset($data['subcontractor']) ? Yii::app()->Controller->money_format_inr($advance['amount'], 2) : "0.00"; ?>
                                                </td>

                                            </tr>
                                            <?php $k++;
                                        }

                                    }

                                }
                            }

                            $i++;
                        }
                    } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>





<script>
    $(document).ready(function () {
        $("#weekly_tbl").tableHeadFixer({
            'left': false,
            'foot': true,
            'head': true
        });
        $("#advance_tbl").tableHeadFixer({
            'left': false,
            'foot': true,
            'head': true
        });
    });

    $("#pdf-btn").click(function () {
        var redirectUrl = $(this).attr("href");
        window.location.href = redirectUrl;
    });
    $("#excel-btn1").click(function () {
        var redirectUrl = $(this).attr("href");
        window.location.href = redirectUrl;
    });
</script>