ALTER TABLE `jp_projects` CHANGE `billed_to_client` `billed_to_client` FLOAT(20,2) NULL DEFAULT NULL;
ALTER TABLE `jp_projects` CHANGE `tot_expense` `tot_expense` FLOAT(20,2) NULL DEFAULT NULL;
ALTER TABLE `jp_projects` CHANGE `tot_receipt` `tot_receipt` FLOAT(20,2) NULL DEFAULT NULL;
ALTER TABLE `jp_projects` CHANGE `tot_paid_to_vendor` `tot_paid_to_vendor` FLOAT(20,2) NULL DEFAULT NULL;

ALTER TABLE `jp_warehousereceipt` CHANGE `delete_status` `delete_status` ENUM('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0' COMMENT '0=>receipt not deleted, 1 => deleted';

ALTER TABLE `jp_warehousereceipt` ADD `approve_status` ENUM('0','1') NOT NULL DEFAULT '1' COMMENT '0=>not approved,1=>approved' AFTER `delete_status`;

ALTER TABLE `jp_warehousereceipt_items` CHANGE `warehousereceipt_itemunit` `warehousereceipt_itemunit` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

ALTER TABLE `jp_warehouse_stock_correction` CHANGE `wh_receipt_item_id` `wh_receipt_item_id` INT(11) NULL DEFAULT NULL;
ALTER TABLE `jp_warehouse` ADD `auto_debit` INT(11) NOT NULL DEFAULT '0' COMMENT '0=>No,1=>Yes' AFTER `assigned_to`;