<?php
/* @var $this SubcontractorController */
/* @var $model Subcontractor */

$this->breadcrumbs=array(
	'Subcontractors'=>array('index'),
	'Create',
);

/*$this->menu=array(
	array('label'=>'List Subcontractor', 'url'=>array('index')),
	array('label'=>'Manage Subcontractor', 'url'=>array('admin')),
);*/
?>
<div class="entries-wrapper">
    <div class="row">
        <div class="col-xs-12">
            <div class="heading-title">Add Subcontractor</div>
            <div class="dotted-line"></div>
        </div>
    </div>
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
