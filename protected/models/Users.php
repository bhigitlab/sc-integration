<?php

/**
 * This is the model class for table "{{users}}".
 * @property integer $global_user_id
 */

class Users extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Users the static model class
     */
//for forgot password
    public $myemail_or_username;
    public $ruser_id;
    public $retype_pwd;
    public $new_pwd;
    public $old_password;
    public $rules_array = array();
    public $action_name;
    public $status = 0;
    public $controllername;
    public $action;
    public $condition;
    public $company_id;
    
   /* public function defaultScope()
    {
        $controllername =   Yii::app()->controller->id;
        $action     =   Yii::app()->controller->action->id;
        if(in_array($controllername.'/'.$action,
            array(
                'clients/update','site/login','users/passchange','users/usershift'
            )
        ))
        {   //Return all active records.
            $condition = '';
        }else{
            $condition = 'user_type <= 2';
        }
        
        return array(
            'condition' => $condition,
        );   
 
    }*/
   /* for designer is not listing */
   
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'jp_users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        $this->action_name = trim(strtolower(Yii::app()->controller->action->id));

        if ($this->action_name === 'pwdrecovery') {
            $this->rules_array = array(
                array('myemail_or_username', 'required', 'message' => 'Entera your username or email id'),
                //array('myemail_or_username', 'match', 'pattern' => '/^[A-Za-z0-9@.-\s,]+$/u', 'message' => "Enter Valid username or email id"),
// password needs to be authenticated
                array('myemail_or_username', 'checkexists'),
            );
        } elseif ($this->action_name === 'pwdreset') {
            $this->rules_array = array(
                array('new_pwd, retype_pwd', 'required', 'message' => 'Entera {attribute}'),
                array('new_pwd', 'length', 'max' => 32, 'min' => 4, 'message' => "Incorrect password (minimal length 4 characters)."),
                array('retype_pwd', 'compare', 'compareAttribute' => 'new_pwd', 'message' => "Password mismatch"),
            );
        } elseif ($this->action_name === 'passchange') {
            $this->rules_array = array(
                array('old_password, password ,retype_pwd', 'required', 'message' => 'Enter {attribute}'),
                array('old_password', 'validate_oldpass'),
                array('password', 'length', 'min' => '5'),
                array('retype_pwd', 'compare', 'compareAttribute' => 'password', 'message' => "Password mismatch"),
            );
        } elseif ($this->action_name === 'editprofile') {
            $this->rules_array = array(
                array('first_name ,email', 'required', 'message' => 'Enter {attribute}'),
                array('email', 'email'),
            );
        } elseif ($this->action_name === 'update') {
            $this->rules_array = array(
                array('first_name, username, phonenumber, status, user_type, company_id', 'required', 'message' => 'Enter {attribute}'),
                array('password', 'length', 'min' => 5, 'max' => 32),
                array('email', 'email'),
                array('phonenumber', 'numerical'),
                array('phonenumber', 'length', 'min' => 4, 'max' => 15),                
                array('last_name,reporting_person,company_id', 'safe'),
               array('company_id', 'checkboxlistoptions'),
            );
        } elseif ($this->action_name === 'create') {
            $this->rules_array = array(
                array('first_name, username, password, phonenumber,company_id', 'required', 'message' => 'Enter {attribute}'),
                array('user_type', 'required', 'message' => 'Choose profile type'),
                array('user_type', 'numerical', 'integerOnly' => true),
                array('password', 'length', 'min' => 5, 'max' => 32),
                array('first_name, last_name', 'length', 'max' => 30),
                array('username', 'length', 'max' => 20),
                array('username', 'unique', 'message' => '{attribute} already exists.'),
                array('email', 'length', 'max' => 70),
                array('email', 'email'),
                 array('phonenumber', 'numerical'),
                array('phonenumber', 'length', 'min' => 4, 'max' => 15),               
                array('reporting_person,company_id', 'safe'),
                //array('company_id', 'required', 'requiredValue' => 1, 'message' => 'Tienes que leer y aceptar nuestra política de datos'),
                //array('company_id', 'in','range'=>array(1,2)),
                array('company_id', 'checkboxlistoptions'),
            );
        } else {
            $this->rules_array = array(
                array('first_name, username, password, phonenumber', 'required', 'message' => 'Enter {attribute}'),
                array('user_type', 'required', 'message' => 'Choose profile type'),
                array('user_type', 'numerical', 'integerOnly' => true),
                array('password', 'length', 'min' => 5, 'max' => 32),
                array('first_name, last_name', 'length', 'max' => 30),
                array('username', 'length', 'max' => 20),
                array('username', 'unique', 'message' => '{attribute} already exists.'),
                array('email', 'length', 'max' => 70),
                array('email', 'email'),
                array('phonenumber', 'numerical'),
                array('phonenumber', 'length', 'min' => 4, 'max' => 15),                
                array('reg_ip', 'length', 'max' => 15),
                array('activation_key', 'length', 'max' => 32),
                array('reg_date, reporting_person,client_id', 'safe'),
                // The following rule is used by search().
// Please remove those attributes that should not be searched.
                array('userid, user_type, first_name, last_name, username, password, email, phonenumber, last_modified, reg_date, reg_ip,reporting_person, activation_key, email_activation, status,client_id', 'safe', 'on' => 'search'),
            );
        }

        return $this->rules_array;
    }

    //Custom validation functions
    public function checkexists($attribute, $params) {
        if (!$this->hasErrors()) {  // we only want to authenticate when no input errors
            if (strpos($this->myemail_or_username, "@")) {
                $user = Users::model()->findByAttributes(array('email' => $this->myemail_or_username, 'status' => 0, 'email_activation' => 1));
                if ($user)
                    $this->ruser_id = $user->userid;
            } else {
                $user = Users::model()->findByAttributes(array('username' => $this->myemail_or_username, 'status' => 0, 'email_activation' => 1));
                if ($user)
                    $this->ruser_id = $user->userid;
            }

            if ($user === null)
                if (strpos($this->myemail_or_username, "@")) {
                    $this->addError("myemail_or_username", ("This email address was not found in our records"));
                } else {
                    $this->addError("myemail_or_username", ("This username was not found in our records"));
                }
        }
    }
    
     public function checkboxlistoptions($attribute, $params) {
		/* if(empty($this->company_id)) {
		  $this->addError("company_id", ("Seleact Company"));
	    }*/
	    
	    if(($this->company_id) === null) {
		  $this->addError("company_id", ("Select Company"));
	    }
	 }

    public function validate_oldpass($attribute, $params) {
        if (!$this->hasErrors()) {  // we only want to authenticate when no input errors
            $oldpass = md5(base64_encode($this->old_password));
            //$user = Users::model()->findByAttributes(array('password' => $oldpass, 'userid' => Yii::app()->user->id, 'status' => 0, 'email_activation' => 1));
            $user = Users::model()->findByAttributes(array('password' => $oldpass, 'userid' => Yii::app()->user->id, 'status' => 0));
            if (!$user) {
                $this->addError("old_password", ("Invalid old password"));
            }
        }
    }

//End of custome validation functions
    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'reportingPerson' => array(self::BELONGS_TO, 'Users', 'reporting_person'),
            'users' => array(self::HAS_MANY, 'Users', 'reporting_person'),
            'userType' => array(self::BELONGS_TO, 'UserRoles', 'user_type'),
            'globalUser' => array(self::BELONGS_TO, 'GlobalUsers', 'global_user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'userid' => 'Userid',
            'user_type' => 'User type',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'username' => 'Username',
            'password' => 'Password',
            'email' => 'Email',
            'phonenumber' => 'Phone Number',
            'reporting_person' => 'Reporting Person',
            'last_modified' => 'Last Modified',
            'reg_date' => 'Reg Date',
            'reg_ip' => 'Reg Ip',
            'activation_key' => 'Activation Key',
            'email_activation' => 'Email Activation',
            'status' => 'Status',
            'myemail_or_username' => 'Username or Email id',
            'retype_pwd' => 'Retype password',
            'new_pwd' => 'New password',
            'old_password' => 'Old password',
            'reg_type' => '',
            'company_id' => 'Company'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
// Warning: Please modify the following code to remove attributes that
// should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('userid', $this->userid);
        $criteria->compare('user_type', $this->user_type);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('last_name', $this->last_name, true);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('phonenumber', $this->phonenumber, true);
        $criteria->compare('reporting_person', $this->reporting_person);
        $criteria->compare('last_modified', $this->last_modified, true);
        $criteria->compare('reg_date', $this->reg_date, true);
        $criteria->compare('reg_ip', $this->reg_ip, true);
        $criteria->compare('activation_key', $this->activation_key, true);
        $criteria->compare('email_activation', $this->email_activation);
        $criteria->compare('status', $this->status);
        $criteria->compare('company_id', $this->company_id);
        if (defined('LOGIN_USER_TABLE') && LOGIN_USER_TABLE != '') {                        
            $criteria->compare('global_user_id',$this->global_user_id);
        }
        $criteria->addCondition('user_type != 4');        
        if(Yii::app()->user->role !=1){
			$criteria->addCondition('company_id = '.Yii::app()->user->company_id.'');
		}

        return new CActiveDataProvider($this, array('pagination' => array('pageSize' => 25),
            'sort' => array(
                'defaultOrder' => 'status,userid DESC',
            ),'pagination' => false,
            'criteria' => $criteria,
        ));
    }

}
