<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/moment.js"></script>
<script  type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<script src="<?php echo Yii::app()->baseUrl . '/ckeditor/ckeditor.js'; ?>"></script>

<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="flash-holder flash-success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<?php if (Yii::app()->user->hasFlash('error')) : ?>
    <div class="alert alert-danger  alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php endif; ?>

<div id="previous_details"></div>
<div class="entries-wrapper margin-bottom-20">
            <div class="row">
                    <div class="col-xs-12">
                      <div class="heading-title">Quotation Details</div>
                      <div class="dotted-line"></div>
                      <a data-toggle="collapse" data-parent="#accordion" href="#quotation_form" 
                        class="pull-right text-lg">
                            <i class="fa fa-caret-down"></i>
                        </a>
                    </div>
            </div>
            
        </div>
        <div id="quotation_form" class="panel-collapse collapse in">
            <div class="panel-body p-0">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'quotation-form',
                    'enableClientValidation' => true,
                    'clientOptions' => array('validateOnChange' => true),
                    'enableAjaxValidation' => false,
                ));
                ?>
                <div class="row">

                    <div class="form-group col-xs-12 col-md-2">
                        <div class="form-group">
                            <input type="hidden" name="Quotation[quotation_id]" id="Quotation_quotation_id" value="">
                            <input type="hidden" name="Quotation[total_amount]" id="Quotation_total_amount" value="">
                            <?php echo $form->labelEx($model, 'company_id'); ?>
                            <?php
                            $user = Users::model()->findByPk(Yii::app()->user->id);
                            $arrVal = explode(',', $user->company_id);
                            $newQuery = "";
                            foreach ($arrVal as $arr) {
                                if ($newQuery)
                                    $newQuery .= ' OR';
                                $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
                            }
                            $companyInfo = Company::model()->findAll(array('condition' => $newQuery));
                            ?>
                            <?php
                            echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array('condition' => $newQuery)), 'id', 'name'), array('class' => 'form-control js-example-basic-single field_change invoice_add require', 'empty' => '-Select Company-', 'style' => 'width:100%'));
                            ?>
                            <?php echo $form->error($model, 'company_id'); ?>
                        </div>
                    </div>

                    <div class="form-group col-xs-12 col-md-2">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'client_name'); ?>
                            <?php echo $form->textField($model, 'client_name', array('class' => 'form-control ')); ?>
                            <?php echo $form->error($model, 'client_name'); ?>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-md-2">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'address'); ?>
                            <?php echo $form->textField($model, 'address', array('class' => 'form-control ')); ?>
                            <?php echo $form->error($model, 'address'); ?>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-md-2">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'phone_no'); ?>
                            <?php echo $form->textField($model, 'phone_no', array('class' => 'form-control ', 'value' => $model->phone_no)); ?>
                            <?php echo $form->error($model, 'phone_no'); ?>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-md-2">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'email'); ?>
                            <?php echo $form->textField($model, 'email', array('class' => 'form-control ')); ?>
                            <?php echo $form->error($model, 'email'); ?>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-md-2">
                        <div class="form-group">
                            <label>DATE : </label>
                            <input type="text" value="<?php echo ((isset($date) && $date != '') ? date("d-m-Y", strtotime($date)) : ''); ?>" id="datepicker" class="txtBox date inputs target form-control invoice_add require" name="date" placeholder="Please click to edit" readonly="true">
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-md-2">
                        <div class="form-group">

                            <?php echo $form->labelEx($model, 'location_id'); ?>
                            <?php
                            echo $form->dropDownList($model, 'location_id', CHtml::listData($location_data, 'id', 'name'), array('class' => 'form-control js-example-basic-single field_change invoice_add require', 'empty' => '-Select Location-', 'style' => 'width:100%'));
                            ?>
                            <?php echo $form->error($model, 'location_id'); ?>

                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-md-2">
                        <div class="form-group">
                            <?php
                            echo $form->labelEx($model, 'invoice_no');
                            echo $form->textField($model, 'invoice_no', array('class' => 'form-control require', 'readonly' => true));
                            ?>
                            <?php echo $form->error($model, 'invoice_no'); ?>
                        </div>
                        <input type="hidden" value="<?php echo $model->revision_no ?>">
                    </div>
                
                    <div class="form-group col-xs-12 col-md-2">
                        <div class="form-group">

                            <?php echo $form->labelEx($model, 'sales_executive_id'); ?>
                            <?php
                            echo $form->dropDownList($model, 'sales_executive_id', CHtml::listData(Salesexecutive::model()->findAll(), 'id', 'name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Executive-', 'style' => 'width:100%'));
                            ?>
                            <?php echo $form->error($model, 'location_id'); ?>

                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-md-2">
                        <div class="form-group">                    
                            <label>Ex. Phone</label>
                            <?php
                            $phone ="";
                            if($model->sales_executive_id !=""){
                                $salesExecutive = Salesexecutive::model()->findByPk($model->sales_executive_id);
                                $phone = $salesExecutive['phone'];
                            }
                            
                            ?>
                            <input type="text" class="ex_phone form-control" value="<?php echo $phone ?>" readonly />
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-md-2">
                        <div class="form-group">                    
                            <label>Ex. Designation</label>
                            <?php
                            $designation ="";
                            if($model->sales_executive_id !=""){
                                $salesExecutive = Salesexecutive::model()->findByPk($model->sales_executive_id);
                                $designation = $salesExecutive['designation'];
                            }
                            
                            ?>
                            <input type="text" class="ex_designation form-control" value="<?php echo $designation ?>" readonly />
                        </div>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>
</div>
<!-- Main form ends -->

<div class="alert alert-success  alert-dismissible alert-msg-box"  style="display:none;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <span class="alert-msg"></span>
</div>
<?php
if (isset($_GET['qid']) && $_GET['qid'] != "") {
    ?>
    <div class="row">
         <div class="col-xs-12">
            <div class="heading-title">Quotation Item Details</div>
                <div class="dotted-line"></div>
                    <button type="button" class=" btn-preview" style="visibility:hidden"  
                    data-qtid="<?php echo $_GET['qid'] ?>"
                    data-rev="<?php echo $_GET['rev'] ?>" data-status="<?php echo $_GET['status'] ?>" 
                    data-inter-rev="<?php echo (isset($_GET['selected_revision'])?$_GET['selected_revision']:"") ?>">
                        Preview
                </button> 
            </div>
    </div>
    <!-- <div class="clearfix border-bottom">
        <h4 class="pull-left">Quotation Item Details</h4>   
        <button type="button" class=" btn-preview" style="visibility:hidden"  
         data-qtid="<?php echo $_GET['qid'] ?>"
         data-rev="<?php echo $_GET['rev'] ?>" data-status="<?php echo $_GET['status'] ?>" 
         data-inter-rev="<?php echo (isset($_GET['selected_revision'])?$_GET['selected_revision']:"") ?>">
            Preview
        </button>     
    </div> -->

    


    <div id="item_details">

        <div>
            <div class="add_sec">                
                <div class="sections_block">
                    <div class="quotation_sec">
                        <!-- start main sec -->
                        <!-- <div class="panel panel shdow_box border-0" style="position:relative">                             -->
                            <div class="panel-body">                                
                                <?php
                                $form = $this->beginWidget('CActiveForm', array(
                                    'id' => 'section-form',
                                    'enableAjaxValidation' => true,
                                    'enableClientValidation' => true,
                                    //'action' => Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/addquotationitems"),
                                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                                    'clientOptions' => array(
                                        'validateOnSubmit' => true,
                                        'validateOnChange' => true,
                                        'validateOnType' => false,
                                    ),
                                ));
                                ?>
                                <!-- <b>Section Details</b>  -->
                                
                                <div class="row section_holder"> 
                                         
                                    <?php if(isset($_GET['rev']) && $_GET['status']==1) {?>
                                    <input id="row_revision" type="hidden" value="<?php echo 'REV-'.$_GET['rev']?>" name=""> 
                                    <?php } ?>      
                                    <div class="form-group col-xs-12 col-md-2">
                                        <?php echo $form->hiddenField($main_sec_model, 'id', array('id' => 'section_id')); ?>
                                        <label>Section&nbsp;</label>
                                        <label class="pull-right add_new_sec" style="font-size: 10px;color: #1218db;">Add New</label>
                                        <?php echo $form->textField($main_sec_model, 'section_name', array('class' => 'form-control  section_name', 'autocomplete' => 'off', 'placeholder' => 'Section', 'name' => 'section_name', 'id' => 'section_name','required' => 'required')); ?>
                                        <div class="errorMessage"></div>
                                    </div>
                                    <div class="form-group col-xs-12 col-md-2">                                                                                  
                                        <?php echo $form->hiddenField($main_cat_model, 'id', array('id' => 'sub_section_id')); ?>  
                                        <label>Sub Section &nbsp;</label>                  
                                        <label class="pull-right add_new_subsec" style="font-size: 10px;color: #1218db;">Add New</label>
                                        <?php
                                        echo $form->textField($main_cat_model, 'category_label', array('class' => 'form-control require sub_section', 'id' => 'sub_section', 'placeholder' => 'Sub Section' ,'required' => 'required'));
                                        ?>
                                        <div class="errorMessage"></div>
                                    </div>
                                    <?php
                                    $visible = "";
                                     if(!isset($_REQUEST['sec_id'])){  
                                        $visible = "style=display:none";
                                        ?>
                                        <div class="add_subitem col-md-2" title="Add Sub Section"> 
                                        <label class="d-block mb-0" style="margin-bottom: 0;">&nbsp;</label>                                                       
                                        <button class="btn btn-info btn-sm">Add Items</button>
                                        </div>
                                        <?php } ?>
                                    
                                </div>
                                <?php $this->endWidget(); ?>
                                <div class="" id="quotation_item"></div>

                                
                                <br>
                                <div class="worktype_label_sec mt-2" <?php echo $visible ?> >
                                    <div class="">
                                        <div class="panel-heading position-relative" style="background: #eee;border: none;">
                                        <div class="row">
                                            <div class="col-xs-12">
                                            <h5>Add Sub Items</h5>
                                            <div class="dotted-line"></div>
                                            </div>
                                            <div class="pull-right">
                                                    <i class="fa fa-refresh reset_subitem"></i>                                                    
                                                </div>
                                        </div>
                                            <!-- <div class="clearfix">
                                                <div class="pull-left">
                                                    <h5>Add Sub Items</h5>                                                    
                                                </div>
                                                
                                            </div> -->
                                        </div>
                                        <div class="panel-body">
                                            <?php
                                            echo $this->renderPartial('_worktype_label_sec1',
                                                array('main_wtype_model' => $main_wtype_model, 
                                                'qid' => $qid, 'rev' => $rev, 'status' => $status))
                                            ?>
                                            <p style="color:red;padding-left:15px;"><i>* Nos will be the unit displayed if Qty in Nos field is entered</i></p>
                                                                                    
                                        </div>
                                    </div>
                                </div>
                                <div class="" id="quotation_item_edit"></div>                                        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- sum sec starts -->

        <?php
        

        if (isset($_GET['qid'])) {
            $revision_no = 'REV-' . $rev;

            $sql = 'SELECT * FROM jp_quotation_revision '
                    . ' WHERE qid=' . $_GET['qid']
                    . ' AND revision_no="' . $revision_no . '"';
            $revision_model = Yii::app()->db->createCommand($sql)->queryRow();
            if (empty($revision_model) && $_GET['status'] == 1) {
                $revision_no = $_GET['selected_revision'];
                $sql = 'SELECT * FROM jp_quotation_revision '
                        . ' WHERE qid=' . $_GET['qid']
                        . ' AND revision_no="' . $_GET['selected_revision'] . '"';
                $revision_model = Yii::app()->db->createCommand($sql)->queryRow();
            }
            ?>

            
        <?php } ?> 

        <!-- sum sec ends -->
        <?php
    } ?>
    <div class="Item_details_table">
        
    </div>
    <div class="text-center">
        <br>
        <?php
       
        if(isset($_GET['status'])&& $_GET['status'] == 1){
            $inter_rev = $_GET['selected_revision'].$_GET['rev'];
            $sql = 'SELECT count(*) FROM `jp_sales_quotation` '
                    . ' WHERE master_id =' . $_GET['qid']
                    . ' AND revision_no="' . $inter_rev . '" AND `deleted_status` = 1';
                    
            $inter_rev_count = Yii::app()->db->createCOmmand($sql)->queryScalar();
    
            $disabled = '';
            if($inter_rev_count == 0){
                $disabled = 'disabled';
            }
            
            echo CHtml::button('Update',array('class'=>'btn btn-primary  updateRevision',
                            'data-rev'=>$inter_rev,'data-qid'=>$_GET['qid'],'disabled'=>$disabled));
        }
        
        ?>
    </div>



