<?php
/* @var $this PmsAccWprItemConsumedController */
/* @var $model PmsAccWprItemConsumed */

$this->breadcrumbs=array(
	'Pms Acc Wpr Item Consumeds'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List PmsAccWprItemConsumed', 'url'=>array('index')),
	array('label'=>'Create PmsAccWprItemConsumed', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pms-acc-wpr-item-consumed-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Pms Acc Wpr Item Consumeds</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pms-acc-wpr-item-consumed-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'warehouse_id',
		'item_id',
		'item_rate',
		'item_qty',
		'status',
		/*
		'item_added_by',
		'item_approved_by',
		'wpr_id',
		'pms_project_id',
		'coms_project_id',
		'coms_material_id',
		'pms_material_id',
		'pms_template_material_id',
		'type_from',
		'created_date',
		'updated_date',
		'pms_task_name',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
