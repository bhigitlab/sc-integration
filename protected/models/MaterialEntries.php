<?php

/**
 * This is the model class for table "{{material_entries}}".
 *
 * The followings are the available columns in table '{{material_entries}}':
 * @property integer $id
 * @property integer $project_id
 * @property integer $company_id
 * @property integer $subcontractor_id
 * @property integer $expensehead_id
 * @property string $description
 * @property integer $status
 * @property string $Date
 *
 * The followings are the available model relations:
 * @property Projects $project
 * @property Subcontractor $subcontractor
 * @property MaterialItems[] $materialItems
 */
class MaterialEntries extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{material_entries}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_id, company_id, subcontractor_id, expensehead_id, status', 'numerical', 'integerOnly'=>true),
			array('description', 'length', 'max'=>255),
			array('Date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, project_id, company_id, subcontractor_id, expensehead_id, description, status, Date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
			'subcontractor' => array(self::BELONGS_TO, 'Subcontractor', 'subcontractor_id'),
			'materialItems' => array(self::HAS_MANY, 'MaterialItems', 'material_entry_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'project_id' => 'Project',
			'company_id' => 'Company',
			'subcontractor_id' => 'Subcontractor',
			'expensehead_id' => 'Expensehead',
			'description' => 'Description',
			'status' => 'Status',
			'Date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('subcontractor_id',$this->subcontractor_id);
		$criteria->compare('expensehead_id',$this->expensehead_id);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('Date',$this->Date,true);

		return new CActiveDataProvider(
			$this,
			array(
				'criteria' => $criteria,
			)
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MaterialEntries the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
 
}
