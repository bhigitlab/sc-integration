INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES ('785', 'Add Warehouse', '704', '0', 'wh/warehouse', 'create', '', '2', '1', NULL);
ALTER TABLE `jp_quotation_section` CHANGE `quantity_nos` `quantity_nos` VARCHAR(100) NULL DEFAULT NULL;
ALTER TABLE `jp_quotation_gen_category` CHANGE `quantity_nos` `quantity_nos` VARCHAR(100) NULL DEFAULT NULL;
ALTER TABLE `jp_quotation_gen_worktype` CHANGE `quantity_nos` `quantity_nos` VARCHAR(100) NULL DEFAULT NULL;
ALTER TABLE `jp_sales_quotation` CHANGE `quantity_nos` `quantity_nos` VARCHAR(100) NOT NULL;

