<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script>
    var shim = (function(){document.createElement('datalist');})();
</script>
<style>
    .lefttdiv{
            float: left;
    }
</style>
<script>
    $( function() {
        $( "#datepicker" ).datepicker({dateFormat: 'dd-mm-yy'}).datepicker("setDate", new Date());
    } );

</script>
<style>
    .dropdown{
	position: relative;
	display: inline-block;
    }
    .dropdown::before{
	position: absolute;
	content: " \2193";
	top: 0px;
	right: -8px;
	height: 20px;
	width: 20px;
    }

    button#caret {
	border : none;
	background : none;
	position: absolute;
	top: 0px;
	right: 0px;
    }
    .invoicemaindiv th, .invoicemaindiv td{
        padding: 10px;
        text-align: left;
    }
    .addcolumn {
        cursor: pointer;
        background-color: black;
        border: none;
        color: white;
        text-align: center;
        text-decoration: none;
        margin: 2px;
        font-size: 30px;
        width: 40px;
      }
      
            
      .checkek_edit{
		pointer-events: none; 
	 }
	 	 
	 .add_selection {
		 background: #000;
	  }
	  
	.toast-container {
	  width:350px;
	}
	.toast-position-top-right {
	  top: 57px;
	  right: 6px;
	}
	.toast-item-close { 
	  background-image: none;
	  cursor: pointer;
	  width: 12px;
	  height: 12px;
	  text-align: center;
	  border-radius: 2px;
	}
	.toast-item-image {
	  font-size: 24px;
	}
	.toast-item-close:hover {
	  color: red;
	}
	.toast-item {
	  border: transparent;
	  border-radius: 3px;
	  font-size: 10px;
	  opacity: 1;
	  background-color: rgba(34,45,50,0.8);
	}
	.toast-item-wrapper p {
	  margin: 0px 5px 0px 42px;
	  font-size: 14px;
	  text-align: justify;
	}	
	  
	.toast-type-success {
	  background-color: #00A65A;
	  border-color: #00A65A;
	}
	.toast-type-error {
	  background-color: #DD4B39;
	  border-color: #DD4B39;
	}
	.toast-type-notice {
	  background-color: #00C0EF;
	  border-color: #00C0EF;
	}
	.toast-type-warning {
	  background-color: #F39C12;
	  border-color: #F39C12;
	}
	
	
        
	.remark{display:none;}
	.padding-box{padding:3px 0px; min-height:17px; display:inline-block;}
	th{height:auto;}
	.quantity, .rate, .small_class{max-width:80px;}
        .p_block{margin-bottom: 10px;padding: 0px 15px;}
        .gsts input{width: 50px;}
        .amt_sec:after,.padding-box:after{
              display:block;
              content:'';
              clear:both;
        }
        .client_data{margin-top: 6px;}
	*:focus{
		border:1px solid #333;
		box-shadow:0 0 6px 1px rgba(0,0,0,0.25);
	}
        .toottip-hiden{width:auto;}
        
        @media(max-width: 767px){
        .quantity, .rate, .small_class {
             width: auto !important; 
          }
        .p_block {
          margin-bottom: 0px;
        }
        .padding-box{
              float: right;
          }
          #tax_amount,#item_amount{display:inline-block;float:none;text-align:right;}
        span.select2.select2-container.select2-container--default.select2-container--focus{width:auto !important;}
         span.select2.select2-container.select2-container--default.select2-container--below{width:auto !important;}
        }
        .tooltip-hiden{width:auto;}
	
</style>

<div class="container">
    
<div class="invoicemaindiv" >
    <h2 class="purchase-title">Sales Book</h2>
    <div id="msg_box"></div>
	<form id="pdfvals1" method="post" action="<?php echo $this->createAbsoluteUrl('purchase/addpurchase'); ?>"  >
	<input type="hidden" name="remove" id="remove" value="">
	<input type="hidden" name="invoice_id" id="invoice_id" value="0">
	<div class="block_sec">
		<div class="row">
			<div class="col-md-2 col-sm-6">
				<div class="form-group">
					<label>COMPANY : </label>
					<select name="company_id" class="inputs target company_id form-control"  id="company_id" style="width:190px">
					<option value="">Choose Company</option>
					<?php
					$user = Users::model()->findByPk(Yii::app()->user->id);
					$arrVal = explode(',', $user->company_id);
					$newQuery = "";
					foreach($arrVal as $arr) {
					if ($newQuery) $newQuery .= ' OR';
					$newQuery .= " FIND_IN_SET('".$arr."', id)";
					}
					$companyInfo = Company::model()->findAll(array('condition' => $newQuery));
					foreach($companyInfo as $key => $value){
					?>	
					<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
					<?php		
					}
					?>
					</select>
				</div>
			</div>
			<div class="col-md-2 col-sm-6">
				<div class="form-group">
					<label>PROJECT : </label>
					<select name="project" class="inputs target project form-control"  id="project" style="width:190px">
					<option value="">Choose Project</option>
					<?php
					foreach($project as $key => $value){
					?>	
					<option value="<?php echo $value['pid']; ?>"><?php echo $value['name']; ?></option>
					<?php		
					}
					?>
					</select>
				</div>
			</div>
			<div class="col-md-2 col-sm-6 client_data">     
				<div class="form-group">               
					<label>CLIENT :  <div id="client_label"></div></label>
					<input type="hidden" name="client" class="inputs target" value='0' id="client">	
				</div>				
			</div>
			<div class="col-md-2  col-sm-6">
				<div class="form-group">
					<label>DATE : </label>
					<input type="text" value="<?php  echo ((isset($date) && $date != '') ? date("d-m-Y", strtotime($date)) : ''); ?>" id="datepicker" class="txtBox date inputs target form-control" name="date"  placeholder="Please click to edit" >
				</div>
			</div>
			<div class="col-md-2 col-sm-6">	
				<div class="form-group">			
					<label>Invoice No : </label>
					<input type="text" required value="<?php  echo ((isset($invoice_no) && $invoice_no != '') ? $invoice_no : ''); ?>" class="txtBox inputs target check_type inv_no form-control" name="inv_no" id="inv_no" placeholder="" readonly='true' >
				</div>
			</div>
		</div>
	</div>
	       
	<div class="clearfix"></div>
	<div id="msg"></div>  
	<div id="previous_details" style="position: fixed;top: 94px; left: 70px; right: 70px; z-index: 2; max-width:1150px; margin:0 auto;"></div>

	<div class="purchase_items panel panel-default">
		<div class="panel-heading">
		<h3>Sales Book Entries</h3>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-2">
					<?php				
					echo CHtml::activeDropDownList($newmodel,
                    'type',
                    array('1'=>'By Lumpsum','2'=>'By Quantity * Rate'),
                    array('empty'=>'Select Type')
                	);
					?>
				</div>
				<div class="col-md-8">
					<div class="row">
						<div class="purchaseitem col-md-4">
							<div class="form-group">
							<label>Description:</label>
							<input type="text" class="inputs target txtBox description form-control"  id="description" name="description[]" placeholder=""/></td>
							</div>
						</div>
						<div class="purchaseitem col-md-2">
							<div class="form-group">
							<label>HSN Code:</label>
							<input type="text" class="inputs target txtBox hsn_code form-control"  id="hsn_code" name="hsn_code[]" placeholder=""/></td>
							</div>
						</div>
						<div class="purchaseitem quantity_div col-md-2">
							<div class="form-group">
							<label>Quantity:</label>
							<input type="text" class="inputs target txtBox quantity allownumericdecimal form-control"  id="quantity" name="quantity[]" placeholder=""/></td>
							</div>
						</div>
						<div class="purchaseitem col-md-2">
							<div class="form-group">
							<label>Units:</label>
							<input type="text" class="inputs target txtBox unit form-control"  id="unit" name="unit[]" placeholder=""/></td>
							</div>
						</div>
						<div class="purchaseitem col-md-2">
							<div class="form-group">
							<label>Rate:</label>
							<input type="text" class="inputs target txtBox rate allownegativenumericdecimal form-control" id="rate" name="rate[]" placeholder=""/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="purchaseitem gsts col-md-2">
							<label>SGST (%):</label>
							<input type="text" class="inputs target calculation small_class txtBox sgst allownumericdecimal form-control" id="sgst" name="sgst[]" placeholder=""/>
							<div id="sgst_amount" class="padding-box">0.00</div>
						</div>
						<div class="purchaseitem gsts col-md-2">
							<label>CGST (%):</label>
							<input type="text" class="inputs target calculation txtBox cgst small_class allownumericdecimal form-control" id="cgst" name="cgst[]" placeholder=""/>
							<div id="cgst_amount" class="padding-box">0.00</div>
						</div>
						<div class="purchaseitem gsts col-md-2">
							<label>IGST (%):</label>
							<input type="text" class="inputs target calculation txtBox igst  small_class allownumericdecimal form-control" id="igst" name="igst[]" placeholder=""/>
							<div id="igst_amount" class="padding-box">0.00</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-right">
					<div class="purchaseitem amt_sec">
						<label>&nbsp;</label>
						<b>Amount: </b><div id="item_amount" class="padding-box">0.00</div>
					</div>
					<div class="purchaseitem amt_sec">
						<label>&nbsp;</label>
						<b>Tax Amount: </b><div id="tax_amount" class="padding-box">0.00</div>
					</div>
				</div>
			</div>
			<div class="text-right">
				<input type="button" class="item_save btn btn-info" id="0" value="Save">
			</div>
		</div>
		
	</div>
</div>
	
	
    <div id="table-scroll" class="table-scroll">
        <div id="faux-table" class="faux-table" aria="hidden"></div>
            <div id="table-wrap" class="table-wrap">
            <div class="table-responsive">
                <table border="1" class="table">
                    	<thead>
				<tr>
				<th>Sl.No</th>
				<th>Description</th>
				<th>HSN Code</th>
				<th>Quantity</th>
				<th>Unit</th>
				<th>Rate</th>
				<th>SGST (%)</th>
				<th>SGST Amount</th>
				<th>CGST (%)</th>
				<th>CGST Amount</th>
				<th>IGST (%)</th>
				<th>IGST Amount</th>
				<th>Amount</th>
				<th>Tax Amount</th>                                                        
				<th></th>
				</tr>
                    	</thead>
                        <tbody class="addrow">           
				</tr>
                        </tbody>
                        <tfoot>
				<tr>
					<th colspan="12" class="text-right">TOTAL: </th>
					<th class="text-right"><div id="grand_total">0.00</div></th>
					<th class="text-right"><div id="grand_taxamount">0.00</th>
					<th></th>
				</tr>
				<tr>
					<th colspan="15" class="text-right">GRAND TOTAL: <span id="net_amount">0.00</span></th>
				</tr>
                        </tfoot>
                        
                    </table>
                </div>                                
                </div>
                </div>
					
		<div style="padding-right: 0px;" class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6">
						
		<input type="hidden" value="<?php  echo ((isset($subtot) && $subtot != '') ? $subtot : ''); ?>"  class="txtBox pastweek" readonly=ture name="subtot"  /></td>
         	<input type="hidden" value="<?php  echo ((isset($grand) && $grand != '') ? $grand : ''); ?>"  class="txtBox pastweek grand" name="grand"  readonly=true/>                                            
            </div>
                    
               </form>
	</div>
  
	<input type="hidden" name="final_amount" id="final_amount" value="0">

	<input type="hidden" name="item_amount_temp" id="item_amount_temp" value="0">

	<input type="hidden" name="final_tax" id="final_tax" value="0">

	<input type="hidden" name="item_tax_temp" id="item_tax_temp" value="0">


<style>
    .error_message{
            color:red;
    }
    a.pdf_excel {
            background-color: #6a8ec7;
            display:inline-block;
            padding:8px;
            color: #fff;
            border: 1px solid #6a8ec8;
    }
</style>



<script>

jQuery.extend(jQuery.expr[':'], {
	focusable: function (el, index, selector) {
	return $(el).is('button, :input, [tabindex]');
	}
});

$(document).on('keypress', 'input,select', function (e) {
	if (e.which == 13) {
	e.preventDefault();
	var $canfocus = $(':focusable');
	var index = $canfocus.index(document.activeElement) + 1;
	if (index >= $canfocus.length) index = 0;
	$canfocus.eq(index).focus();
	}
});

$(document).ready(function() {
	$(".popover-test").popover({
		html: true,
		content: function() {
		return $(this).next('.popover-content').html();
		}
	});
	$('[data-toggle=popover]').on('click', function (e) {
		$('[data-toggle=popover]').not(this).popover('hide');
	});
	$('body').on('hidden.bs.popover', function (e) {
		$(e.target).data("bs.popover").inState.click = false;
	});
	$('body').on('click', function (e) {
		$('[data-toggle=popover]').each(function () {
			if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
			$(this).popover('hide');
			}
		});
	});
	$(document).ajaxComplete(function() {
		$(".popover-test").popover({
			html: true,
			content: function() {
			return $(this).next('.popover-content').html();
			}
		});
	});

	$(".js-example-basic-single").select2();
	$(".project").select2();
	$(".client").select2();
	$(".company_id").select2();

});


$('.project').change(function(){
	var val =$(this).val();
	$.ajax({
		type: "POST",
		url: '<?php echo Yii::app()->createUrl('invoice/getClient'); ?>',
		data:'project_id='+val,
		dataType:'json',
		success: function(data){
		if(data.status == 'success') {
		$('#client').val(data.id);
		$('#client_label').text(data.name);
		} else {
		$('#client').val('');
		$('#client_label').text('');
		}
		},
	})
})

$(document).ready(function () {
	$('select').first().focus();
	$( "#inv_no").keyup(function() {
		if (this.value.match(/[^a-zA-Z0-9]/g)) {
		this.value = this.value.replace(/[^a-zA-Z0-9\-/]/g, '');
		}
	});
});

</script>



<script>
	$(document).ready(function() {
		$().toastmessage({
			sticky: false,
			position: 'top-right',
			inEffectDuration: 1000,   
			stayTime: 3000,
			closeText: '<i class="icon ion-close-round"></i>',
		});
		
		$(".purchase_items").addClass('checkek_edit'); 	
		
	});
	
	$(document).on('click', '.removebtn', function(e){
	e.preventDefault();
	element = $(this);
	 var item_id = $(this).attr('id');
   		var answer = confirm("Are you sure you want to delete?");
		if (answer)
		{
		var item_id = $(this).attr('id');
		var invoice_id = $("#invoice_id").val();
		var subtot = $('input[name="subtot"]').val();
		var grand = $('input[name="grand"]').val();
		var tax_grand = $('#grand_taxamount').text();
		var data = {'invoice_id':invoice_id,'item_id':item_id,'grand' :grand, 'subtot' : subtot,'tax_grand' : tax_grand};
		 $.ajax({
				method: "GET",
				async: false,
				data: {data: data},
				dataType:"json",
				url: '<?php echo Yii::app()->createUrl('invoice/removeinvoiceitem'); ?>',
				success: function(response) {
					if(response.response == 'success')
					 {
                                            $('#grand_total').html(response.final_amount);
                                            $('#grand_taxamount').html(response.final_tax);
                                            $('#net_amount').html(response.grand_total);
                                            $().toastmessage('showSuccessToast', ""+response.msg+"");
                                            $('.addrow').html(response.html);
					 } else {
                                             $().toastmessage('showErrorToast', ""+response.msg+"");
					 }
				}
			}); 
			
		}
	   else
	   {
		  
		   return false;
	   }	
			

	});



	$(document).on("click", ".addcolumn, .removebtn", function(){
		$("table.table  input[name='sl_No[]']").each(function(index,element) {               
			$(element).val(index + 1); 
			$('.sl_No').html(index + 1);
		});
	});


	$(".inputSwitch span").on("click", function() {
		var $this = $(this);
		$this.hide().siblings("input").val($this.text()).show();
	});

	$(".inputSwitch input").bind('blur', function() {
		var $this = $(this);
		$(this).attr('value', $(this).val());
		$this.hide().siblings("span").text($this.val()).show();

	}).hide();

	$(".allownumericdecimal").keydown(function (event) {						
		if (event.shiftKey == true) {
			event.preventDefault();
		}
		if ((event.keyCode >= 48 && event.keyCode <= 57) || 
			(event.keyCode >= 96 && event.keyCode <= 105) || 
			event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
			event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 ||  event.keyCode == 13) {
			var splitfield = $(this).val().split(".");
			if((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13){
				event.preventDefault();
			}
		} else {
			event.preventDefault();
		}
		if($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110 ))
			event.preventDefault(); 			

	});								
	$(".allownegativenumericdecimal").keydown(function (event) {
			if (event.shiftKey == true) {
				event.preventDefault();
			}

			if (
				(event.keyCode >= 48 && event.keyCode <= 57) ||
				(event.keyCode >= 96 && event.keyCode <= 105) ||
				event.keyCode == 8 ||
				event.keyCode == 9 ||
				event.keyCode == 37 ||
				event.keyCode == 39 ||
				event.keyCode == 46 ||
				event.keyCode == 190 ||
				event.keyCode == 110 ||
				event.keyCode == 13 ||
				event.keyCode == 189 // Allow the '-' key
			) {
				var splitfield = $(this).val().split(".");
				if (
					(splitfield[1] !== undefined &&
						splitfield[1].length >= 2) &&
					event.keyCode != 8 &&
					event.keyCode != 0 &&
					event.keyCode != 13
				) {
					event.preventDefault();
				}
			} else {
				event.preventDefault();
			}

			if (
				$(this).val().indexOf('.') !== -1 &&
				(event.keyCode == 190 || event.keyCode == 110)
			)
				event.preventDefault();
			//if a decimal has been added, disable the "."-button
	});	
		
	$('.other').click(function(){
		if(this.checked){
			$('#autoUpdate').fadeIn('slow');
			$('#select_div').hide();
		}else {
			
			$('#autoUpdate').fadeOut('slow');
			$('#select_div').show();
		}
		
	})
				
	$(document).on("change", ".other", function(){
		if(this.checked){
			$(this).closest('tr').find('#autoUpdate').fadeIn('slow');
			$(this).closest('tr').find('#select_div').hide();
		}else {
			$(this).closest('tr').find('#autoUpdate').fadeOut('slow');
			$(this).closest('tr').find('#select_div').show();
		}
	});										
		
</script>


<script>

/* Neethu  */

        $(document).on("change", "#company_id", function () {
	  var element = $(this);
	  var invoice_id = $("#invoice_id").val();
	  var default_date = $(".date").val();
	  var company = $(this).val();
	  var client = $('#client').val();
          var project = $('#project').val();
	  var inv_no = $('#inv_no').val(); 
		 if((moment(default_date, 'DD-MM-YYYY',true).isValid()))
		{	
		if(project == '' || client == '' || default_date == '' || inv_no == '' || company ==''){
			$.ajax({
				method: "GET",
				data: {invoice_id:'test'},
				dataType:"json",
				url: '<?php echo Yii::app()->createUrl('invoice/ajaxdate'); ?>',
				success: function(result) {
					 $('#project').focus();
				}
			});
		} else{
		$.ajax({
				method: "GET",
				async: false,
				data: {invoice_id:invoice_id,inv_no: inv_no, default_date: default_date, project: project, client: client, company:company},
				dataType:"json",
				url: '<?php echo Yii::app()->createUrl('invoice/createnewinvoice'); ?>',
				success: function(result) {
					if(result.response == 'success')
					 {
						$(".purchase_items").removeClass('checkek_edit');
						$('.js-example-basic-single').select2('focus');
						 $("#invoice_id").val(result.invoice_id);
						 $().toastmessage('showSuccessToast', ""+result.msg+"");
                                                } else {
						 $().toastmessage('showErrorToast', ""+result.msg+"");
						 element.val('');
					 }
					 
				}
			});
		}
		} else {
		$(this).focus();
		  $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
		  $(this).focus();
		}
	});

	$(document).on("change", "#project", function () {
	  var element = $(this);
	  var invoice_id = $("#invoice_id").val();
	  var default_date = $(".date").val();
	  var project = $(this).val();
	  var client = $('#client').val(); 
	  var inv_no = $('#inv_no').val();
          var company = $('#company_id').val();
		 if((moment(default_date, 'DD-MM-YYYY',true).isValid()))
		{	
		if(project == '' || client == '' || default_date == '' || inv_no == '' || company ==''){
			$.ajax({
				method: "GET",
				data: {invoice_id:'test'},
				dataType:"json",
				url: '<?php echo Yii::app()->createUrl('invoice/ajaxdate'); ?>',
				success: function(result) {
					 $('.date').focus();
				}
			});
		} else{
		$.ajax({
				method: "GET",
				async: false,
				data: {invoice_id:invoice_id,inv_no: inv_no, default_date: default_date, project: project, client: client, company:company},
				dataType:"json",
				url: '<?php echo Yii::app()->createUrl('invoice/createnewinvoice'); ?>',
				success: function(result) {
					if(result.response == 'success')
					 {
						$(".purchase_items").removeClass('checkek_edit');
						$('.js-example-basic-single').select2('focus');
						 $("#invoice_id").val(result.invoice_id);
						 $().toastmessage('showSuccessToast', ""+result.msg+"");
                                                } else {
						 $().toastmessage('showErrorToast', ""+result.msg+"");
						 element.val('');
					 }
					 
				}
			});
		}
		} else {
		$(this).focus();
		  $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
		  $(this).focus();
		}
	});
	
	$(document).on("change", "#client", function () {
	  var element = $(this);
	  var invoice_id = $("#invoice_id").val();
	  var default_date = $(".date").val();
	  var client = $(this).val();
	  var company = $('#company_id').val();
	  var project = $('#project').val();
	  var inv_no = $('#inv_no').val(); 
	  
	  if((moment(default_date, 'DD-MM-YYYY',true).isValid()))
		{	
		 
		if(project == '' || client == '' || default_date == '' || inv_no == '' || company ==''){
			$.ajax({
				method: "GET",
				data: {invoice_id:'test'},
				dataType:"json",
				url: '<?php echo Yii::app()->createUrl('invoice/ajaxdate'); ?>',
				success: function(result) {
					 $('.date').focus();
				}
			});
			
		} else{
		$.ajax({
				method: "GET",
				data: {invoice_id:invoice_id,inv_no: inv_no, default_date: default_date, project: project, client: client, company : company},
				dataType:"json",
				url: '<?php echo Yii::app()->createUrl('invoice/createnewinvoice'); ?>',
				success: function(result) {
					if(result.response == 'success')
					 {
						 $(".purchase_items").removeClass('checkek_edit');						
						$('.js-example-basic-single').select2('focus');
						 $("#invoice_id").val(result.invoice_id);
						 $().toastmessage('showSuccessToast', ""+result.msg+"");
                                            } else {
						 $().toastmessage('showErrorToast', ""+result.msg+"");
					 }
					 
				}
			});
			
		}
		
		} else {
		$(this).focus();
		  $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
		  $(this).focus();
		}
		
	});
	
	
	
	$(document).on("change", ".date", function () {
	  var element = $(this);
	  var invoice_id = $("#invoice_id").val();
	  var default_date = $(this).val();
	  var project = $('#project').val();
	  var client = $('#client').val(); 
	  var inv_no = $('#inv_no').val(); 
	  var company = $('#company_id').val();
	if((moment(default_date, 'DD-MM-YYYY',true).isValid()))
		{
		  if(project == '' || client == '' || default_date == '' || inv_no == '' || company ==''){
			
		} else{
		$.ajax({
				method: "GET",
				async: false,
				data: {invoice_id:invoice_id,inv_no: inv_no, default_date: default_date, project: project, client: client, company:company},
				dataType:"json",
				url: '<?php echo Yii::app()->createUrl('invoice/createnewinvoice'); ?>',
				success: function(result) {
					if(result.response == 'success')
					 {
						 $(".purchase_items").removeClass('checkek_edit');
						 $('.js-example-basic-single').select2('focus');
						 $("#invoice_id").val(result.invoice_id);
						 $().toastmessage('showSuccessToast', ""+result.msg+"");
                                            } else {
						 $().toastmessage('showErrorToast', ""+result.msg+"");
					 }
				}
				
			});
			
		}
		} else {
		$(this).focus();
		  $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
		  $(this).focus();
		}
	
	$('#inv_no').focus();
	
});
	
	
	$(document).on("blur", "#inv_no", function (event) {
		var element = $(this);
		  var invoice_id = $("#invoice_id").val();
		  var default_date = $(".date").val();
		  var inv_no = $(this).val();	
		  
		  if((moment(default_date, 'DD-MM-YYYY',true).isValid()))
		{		
		  if(inv_no == '') {
			event.preventDefault();
            
			} else {
			  var project = $('#project').val();
			  var client = $('#client').val(); 
			  var date = $('.date').val();
                          var company = $('#company_id').val();
				if(project == '' || client == '' || default_date == '' || company == ''){
				} else{
				$.ajax({
						method: "GET",
						data: {invoice_id:invoice_id,inv_no: inv_no, default_date: default_date, project: project, client: client,company:company},
						dataType:"json",
						url: '<?php echo Yii::app()->createUrl('invoice/createnewinvoice'); ?>',
						success: function(result) {
							if(result.response == 'success')
							 {
								 $(".purchase_items").removeClass('checkek_edit');
								 $("#invoice_id").val(result.invoice_id);
								 $().toastmessage('showSuccessToast', ""+result.msg+"");
							 } else {
								 $().toastmessage('showErrorToast', ""+result.msg+"");
							 }
							 
							 $('#description').focus();						}
						
					});
					
					
					
				}
		    }
		    
		    } else {
		$(this).focus();
		  $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
		  $(this).focus();
		}
	   
	}); 
	
	
	
	
	$("#purchaseno").keypress(function(event){
		if(event.keyCode == 13){
			$("#purchaseno").blur();
		}
	});
	
	$("#date").keypress(function(event){
        if(event.keyCode == 13){
            if($(this).val()){
				$("#inv_no").focus();
			}
        }
    });
	
	$(".date").keyup(function(event){
		if(event.keyCode == 13){
			$(".date").click();
		}
	});
	
	$("#vendor").keyup(function(event){
		if(event.keyCode == 13){
			$("#vendor").click();
		}
	});
	
	$("#project").keyup(function(event){
		if(event.keyCode == 13){
			$("#project").click();
		}
	});


		var sl_no =1;
		var howMany = 0;
		$('.item_save').click(function()
		{
		 $("#previous_details").hide();	
		 var element = $(this);
		  var item_id = $(this).attr('id');
		 
		  if(item_id == 0) {
		 
		 
		  var description = $('#description').val();
          var hsn_code = $('#hsn_code').val();	
		  var quantity = $('#quantity').val();
		  var unit = $('#unit').val();
		  var rate = $('#rate').val();
		  
		  var cgst = $('#cgst').val();
		  var cgst_amount = $('#cgst_amount').html();
		  var sgst = $('#sgst').val();
		  var sgst_amount = $('#sgst_amount').html();
		  var igst = $('#igst').val();
		  var igst_amount = $('#igst_amount').html();
		  var tax_amount = $('#tax_amount').html();
		  var company = $('#company').val();
		  var amount = $('#item_amount').html();
		  var project = $('input[name="project"]').val();
		  var client = $('input[name="client"]').val(); 
		  var date = $('input[name="date"]').val(); 
		  var inv_no = $('input[name="inv_no"]').val(); 
		  var rowCount = $('.table .addrow tr').length;
		 
		  
		  if(project == '' || client == '' || date == '' || inv_no == '' || company_id ==''){
				 $().toastmessage('showErrorToast', "Please enter sales details");
			} else{
				if((moment(date, 'DD-MM-YYYY',true).isValid()))
				{
					
				if(description != '' && rate != '' && hsn_code !='')
				{
					howMany += 1;
						if(howMany ==1)
						{
							if(quantity == ''){
								quantity = 1;
							}
							var invoice_id = $('input[name="invoice_id"]').val();
							var subtot = $('#grand_total').text(); 
							var grand = $('#grand_total').text();
							var grand_tax = $('#grand_taxamount').text(); 
							var data = {'sl_no':rowCount,'quantity':quantity,'description':description,'unit':unit,'rate':rate,'amount':amount, 'sgst' :sgst, 'sgst_amount' : sgst_amount, 'cgst' : cgst, 'cgst_amount' : cgst_amount, 'igst' : igst, 'igst_amount' : igst_amount, 'tax_amount' : tax_amount, 'invoice_id' : invoice_id, 'grand' :grand, 'subtot' : subtot, 'grand_tax' :grand_tax,'hsn_code' :hsn_code};
							$.ajax({
								url: '<?php echo  Yii::app()->createAbsoluteUrl('invoice/invoiceitem'); ?>',
								type:'GET',
								dataType:'json',
								data:{data:data},
								success: function(response) {
									if(response.response == 'success')
									{
										$('#grand_total').html(response.final_amount);
										$('#grand_taxamount').html(response.final_tax);
                                                                                $('#net_amount').html(response.grand_total);
										$().toastmessage('showSuccessToast', ""+response.msg+"");
                                                                                $('.addrow').html(response.html);
									} else {
										 $().toastmessage('showErrorToast', ""+response.msg+"");	
									}
									howMany =0;
									$('#description').val('').trigger('change');
									$('#remarks').val('');
									$('#quantity').val('');
                                                                        $('#hsn_code').val('');
									$('#unit').val('');
									$('#rate').val('');
									$('#item_amount').html('');
									$('#tax_amount').html('0.00');
									$('#sgst').val('');
									$('#sgst_amount').html('0.00');
									$('#cgst').val('');
									$('#cgst_amount').html('0.00');
									$('#igst').val('');
									$('#igst_amount').html('0.00');
									$('#description').focus();
								}
							}); 
						
						}
				} else {
						$().toastmessage('showErrorToast', "Please fill the mandatory fields");
					}
					
				} else {
					
				  $(this).focus();
				  $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
				  $(this).focus();	
				}
					
					
					
					
			}
			
			
			
		} else { 						
		  var description = $('#description').val();
                  var hsn_code = $('#hsn_code').val();		
		  var quantity = $('#quantity').val();
		  var unit = $('#unit').val();
		  var rate = $('#rate').val();
		  var amount = $('#item_amount').html();
		  var project = $('input[name="project"]').val();
		  var client = $('input[name="client"]').val(); 
		  var date = $('input[name="date"]').val(); 
		  var inv_no = $('input[name="inv_no"]').val();		  
		  var cgst = $('#cgst').val();
		  var cgst_amount = $('#cgst_amount').html();
		  var sgst = $('#sgst').val();
		  var sgst_amount = $('#sgst_amount').html();
		  var igst = $('#igst').val();
		  var igst_amount = $('#igst_amount').html();
		  var tax_amount = $('#tax_amount').html();
		  var company = $('#company').val();
		  if(project == '' || client == '' || date == '' || inv_no == '' || company ==''){
			   $().toastmessage('showErrorToast', "Please enter sales details");
		  } else {
			  
			  if((moment(date, 'DD-MM-YYYY',true).isValid()))
				{	  
		  if(description != '' && rate != '' && hsn_code !='')
				{
					 howMany += 1;
						if(howMany ==1)
						{
							if(quantity == ''){
								quantity = 1;
							}
							var invoice_id = $('input[name="invoice_id"]').val();
							var subtot = $('#grand_total').text(); 
							var grand = $('#grand_total').text();
							var grand_tax = $('#grand_taxamount').text();  
							var data = {'item_id': item_id, 'sl_no':sl_no,'quantity':quantity,'description':description,'unit':unit,'rate':rate,'amount':amount, 'sgst' :sgst, 'sgst_amount' : sgst_amount, 'cgst' : cgst, 'cgst_amount' : cgst_amount, 'igst' : igst, 'igst_amount' : igst_amount, 'tax_amount' : tax_amount, 'invoice_id' : invoice_id, 'grand' :grand, 'subtot' : subtot, 'grand_tax' : grand_tax,'hsn_code' :hsn_code};
							$.ajax({
								url: '<?php echo  Yii::app()->createAbsoluteUrl('invoice/updatesinvoiceitem'); ?>',
								type:'GET',
								dataType:'json',
								data:{data:data},
								success: function(response) {									
									if(response.response == 'success')
									{
										$('#grand_total').html(response.final_amount);
										$('#grand_taxamount').html(response.final_tax);
                                                                                $('#net_amount').html(response.grand_total);
										$().toastmessage('showSuccessToast', ""+response.msg+"");
                                                                                $('.addrow').html(response.html);
									} else {
										 $().toastmessage('showErrorToast', ""+response.msg+"");
									}
									$('#description').val('').trigger('change');
									$('#remarks').val('');
                                                                        $('#hsn_code').val('');
									$('#quantity').val('');
									$('#unit').val('');
									$('#rate').val('');
									$('#item_amount').html('');
									$('#tax_amount').html('0.00');
									$('#sgst').val('');
									$('#sgst_amount').html('0.00');
									$('#cgst').val('');
									$('#cgst_amount').html('0.00');
									$('#igst').val('');
									$('#igst_amount').html('0.00');
									$('#description').focus();
									$('#item_amount_temp').val(0);
									$('#item_tax_temp').val(0);
									
									$(".item_save").attr('value', 'Save');
									$(".item_save").attr('id', 0);
									$('#description').focus();
									howMany =0;
								}
							}); 
						
					}
				} else {
					
						$().toastmessage('showErrorToast', "Please fill the mandatory fields");
					}
					
				}  else {
					$(this).focus();
					  $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
					  $(this).focus();
					}
					
					
					
				}
		   
			
		 }
			
			
			
			
		});





$(document).on('click', '.edit_item', function(e){
	e.preventDefault();
	
	
	$('.remark').css("display", "inline-block" );
        
	$('#remark').focus();  
	
	var item_id = $(this).attr('id');
	var $tds = $(this).closest('tr').find('td');
    var description = $tds.eq(1).text();
    var hsn_code = $tds.eq(2).text();
    var quantity = $tds.eq(3).text();
    var unit = $tds.eq(4).text();
    var rate = $tds.eq(5).text();
    var sgst = $tds.eq(6).text();
    var sgst_amount = $tds.eq(7).text();
    var cgst = $tds.eq(8).text();
    var cgst_amount = $tds.eq(9).text();
    
    var igst = $tds.eq(10).text();
    var igst_amount = $tds.eq(11).text();
    var amount = $tds.eq(12).text();
    var tax_amount = $tds.eq(13).text();
    
     
	$('#description').val(description.trim())
    $('#hsn_code').val(hsn_code.trim())
	$('#quantity').val(parseFloat(quantity))
	$('#unit').val(unit.trim())
	$('#item_amount').html(parseFloat(amount))
	$('#item_amount_temp').val(parseFloat(amount))
	$('#rate').val(parseFloat(rate))
	
	$('#tax_amount').html(parseFloat(tax_amount))
	$('#item_tax_temp').val(parseFloat(tax_amount))
	$('#cgst').val(cgst.trim())
	$('#cgst_amount').text(cgst_amount)
	$('#sgst').val(sgst.trim())
	$('#sgst_amount').text(sgst_amount)
	$('#igst').val(igst.trim())
	$('#igst_amount').text(igst_amount)
	
	$(".item_save").attr('value', 'Update');
	$(".item_save").attr('id', item_id);
	$(".item_save").attr('id', item_id);
	$('#description').focus();
  

 })
 
 
 


 $('.item_save').keypress(function(e) {
      if (e.keyCode == 13) {
          $('.item_save').click();
      }
 });
 
 
function filterDigits(eventInstance) {
                eventInstance = eventInstance || window.event;
                    key = eventInstance.keyCode || eventInstance.which;
                if ((47 < key) && (key < 58) || key == 8) {
                    return true;
                } else {
                        if (eventInstance.preventDefault) 
                             eventInstance.preventDefault();
                        eventInstance.returnValue = false;
                        return false;
                    } 
}


 $("#quantity, #rate, #sgst, #cgst, #igst").blur(function() {
        var quantity = parseFloat($("#quantity").val());
        var rate = parseFloat($("#rate").val());
        var sgst  = parseFloat($("#sgst").val());
        var cgst  = parseFloat($("#cgst").val());
        var igst  = parseFloat($("#igst").val());
        var amount = quantity * rate ;
        var new_amount = 0;
        
        var sgst_amount   = (sgst / 100) * amount;
        var cgst_amount   = (cgst / 100) * amount;
        var igst_amount   = (igst / 100) * amount;
        
        if (isNaN(sgst_amount)) sgst_amount = 0;
        if (isNaN(cgst_amount)) cgst_amount = 0;
        if (isNaN(igst_amount)) igst_amount = 0;
        if (isNaN(amount)) amount = 0;
        
        var tax_amount = sgst_amount+cgst_amount+igst_amount;
        
        var total_amount = amount+tax_amount;
        
        $("#sgst_amount").html(sgst_amount.toFixed(2));
        $("#cgst_amount").html(cgst_amount.toFixed(2));
        $("#igst_amount").html(igst_amount.toFixed(2));
        $("#item_amount").html(amount.toFixed(2));
        $("#tax_amount").html(tax_amount.toFixed(2));
        $("#total_amount").html(total_amount.toFixed(2));
        
    });

    $('.calculation').keyup(function(event){
        var val = $(this).val();
        if(val>100){
            $().toastmessage('showErrorToast',"This percentage must be less than 100%");
            $(this).val('');
        } 
    })
    
    $("#company_id").change(function(){
	var val =$(this).val();
        $("#project").html('<option value="">Select Project</option>');
        $.ajax({
        url:'<?php echo Yii::app()->createUrl('invoice/dynamicproject'); ?>',
        method:'POST',
        data:{company_id:val},
        dataType:"json",
        success:function(response){
                if(response.status == 'success'){
                        $("#project").html(response.html);
                } else{
                        $("#project").html(response.html);
                }
        }

        })
    })

</script>

<script>
(function() {
   var mainTable = document.getElementById("main-table"); 
   if(mainTable !== null){
   var tableHeight = mainTable.offsetHeight; 
   if (tableHeight > 380) { 
  		var fauxTable = document.getElementById("faux-table");
		document.getElementById("table-wrap").className += ' ' + 'fixedON';
  		var clonedElement = mainTable.cloneNode(true); 
  		clonedElement.id = "";
  		fauxTable.appendChild(clonedElement);
   }
   }
})();
</script>

