<?php

class ExportdataController extends Controller
{
    public $layout='//layouts/column2';

	/**
	 * @return array action filters 
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	public function accessRules()
	{

					 $accessArr = array();
					 $accessauthArr = array();
					 $controller = Yii::app()->controller->id;
					 if(isset(Yii::app()->user->menuall)){
							 if (array_key_exists($controller, Yii::app()->user->menuall)) {
									 $accessArr = Yii::app()->user->menuall[$controller];
							 }
					 }

					 if(isset(Yii::app()->user->menuauth)){
							 if (array_key_exists($controller, Yii::app()->user->menuauth)) {
									 $accessauthArr = Yii::app()->user->menuauth[$controller];
							 }
					 }
					 $access_privlg = count($accessauthArr);
					 return array(
							 array('allow', // allow all users to perform 'index' and 'view' actions
									 'actions' => $accessArr,
									 'users' => array('@'),
									 'expression' => "$access_privlg > 0",
							 ),

							 array('allow', // allow admin user to perform 'admin' and 'delete' actions
									 'actions' => $accessauthArr,
									 'users' => array('@'),
									 'expression' => "$access_privlg > 0",
							 ),
							 array('deny',  // deny all users
									 'users'=>array('*'),
							 ),
					 );
	}
	public function actionIndex()
	{
			$tblpx = Yii::app()->db->tablePrefix;
			$sales_count =0;
			$contra_count=0;
			$payment_count=0;
			$count_purchase=0;
			$count_expense=0;
			$count_return=0;
	//	print_r($_GET);die;
		if(isset($_GET['fromdate']) || isset($_GET['todate'])){
			$fromdate   = date('Y-m-d', strtotime($_GET['fromdate']));
			$todate     = date('Y-m-d', strtotime($_GET['todate']));
			$where      = '';
			$wherec='';
			$where1      = '';
			$where2 ='';
			if (!empty($fromdate) && !empty($todate)) {
					$where .= " inv.date between '" . $fromdate ."' and'" . $todate . "'";
					$wherec .= " date between '" . $fromdate ."' and'" . $todate . "'";
					$where1 .= " {$tblpx}bills.bill_date between '" . $fromdate ."' and'" . $todate . "'";
					$where2 .= " preturn.return_date between '" . $fromdate ."' and'" . $todate . "'";
			} else {
					if (!empty($date_from)) {
							$where .= "inv.date >= '" . $fromdate ."'";
							$wherec .= "date >= '" . $fromdate ."'";
							$where1 .= " {$tblpx}bills.bill_date between >= '" . $fromdate ."'";
							$where2 .= " preturn.return_date between >= '" . $fromdate ."'";
					}
					if (!empty($todate)) {
							$date_to = date('Y-m-d', strtotime($todate));
							$where .= "inv.date <= '" . $todate . "'";
							$wherec .= "date <= '" . $todate . "'";
							$where1 .= "{$tblpx}bills.bill_date <= '" . $todate . "'";
							$where2 .= "preturn.return_date <= '" . $todate . "'";
					}
			}
			$sql1=	Yii::app()->db->createCommand(" SELECT * FROM " . $tblpx ."invoice as inv WHERE ". $where ." ")->queryAll();
			$sales_count=count($sql1);
			$sql2=	Yii::app()->db->createCommand(" SELECT * FROM " . $tblpx ."dailyexpense WHERE ". $wherec ." AND exp_type_id =0 AND transaction_parent IS NULL")->queryAll();
			$contra_count=count($sql2);
			$sql3=	Yii::app()->db->createCommand(" SELECT * FROM " . $tblpx ."subcontractor_payment WHERE ". $wherec ." ")->queryAll();
			$sql4=	Yii::app()->db->createCommand(" SELECT * FROM " . $tblpx ."dailyvendors WHERE ". $wherec ." ")->queryAll();
			$payment_count=count($sql3)+count($sql4);
			$sql_purchase=	Yii::app()->db->createCommand("SELECT {$tblpx}bills.bill_id,{$tblpx}bills.bill_date,{$tblpx}purchase.project_id FROM {$tblpx}bills LEFT JOIN {$tblpx}purchase ON {$tblpx}bills.purchase_id= {$tblpx}purchase.p_id LEFT JOIN {$tblpx}vendors ON {$tblpx}purchase.vendor_id={$tblpx}vendors.vendor_id WHERE (".$where1.") ORDER BY {$tblpx}bills.bill_date desc")->queryAll();
			$count_purchase=count($sql_purchase);
			$sql_expense=	Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses WHERE ".$wherec." AND bill_id IS NULl AND invoice_id IS NULL AND subcontractor_id is NULL ORDER BY date DESC")->queryAll();
			 $count_expense=count($sql_expense);
			 $sql_return=	Yii::app()->db->createCommand("SELECT preturn.* FROM {$tblpx}purchase_return preturn LEFT JOIN {$tblpx}bills bills ON preturn.bill_id=bills.bill_id WHERE ".$where2."")->queryAll();
			$count_return = count($sql_return);

		}
		$this->render('index',array('sales_count' => $sales_count,'contra_count'=>$contra_count,'payment_count'=>$payment_count,'count_purchase' => $count_purchase,'count_expense' => $count_expense,'count_return' => $count_return));
	}


	public function actionSavetoExcelSales($fromdate,$todate) {
		$tblpx = Yii::app()->db->tablePrefix;
		$fromdate   = date('Y-m-d', strtotime($fromdate));
		$todate     = date('Y-m-d', strtotime($todate));
		$where      = '';
		if (!empty($fromdate) && !empty($todate)) {
				$where .= " inv.date between '" . $fromdate ."' and'" . $todate . "'";

		} else {
				if (!empty($date_from)) {
						$where .= "inv.date >= '" . $fromdate ."'";
				}
				if (!empty($todate)) {
						$date_to = date('Y-m-d', strtotime($todate));
						$where .= "inv.date <= '" . $todate . "'";
				}
		}


	$sql=	Yii::app()->db->createCommand("SELECT inv.invoice_id,inv.client_id,inv.amount,inv.date,(((sum(inl.cgst_amount)+sum(inl.sgst_amount)+sum(inl.igst_amount))/inv.amount)*100) as salesledger,sum(inl.cgst_amount) as cgst,sum(inl.sgst_amount) as sgst,sum(inl.igst_amount) as igst FROM " . $tblpx ."invoice as inv join " . $tblpx ."inv_list as inl on  inv.invoice_id =inl.inv_id WHERE ". $where ."	group by inv.invoice_id")->queryAll();

 // echo '<pre>';
 // print_r($sql);die;

		  $arraylabel = array('Voucher Number', 'Voucher Date', 'Customer Name','Sales Ledger','Sales Amount','CGST','SGST','IGST','Additional Charges','Round Off','Total Amount');
			$finaldata = array();
			if(!empty($sql)){
			foreach ($sql as $key => $datavalue) {

					$finaldata[$key][] = $datavalue['invoice_id'];
					$finaldata[$key][] = $datavalue['date'];
					$client     = Clients::model()->findByPK($datavalue['client_id']);
					$finaldata[$key][] = $client->name;
					$finaldata[$key][] = 'Sales '.Controller::money_format_inr($datavalue['salesledger'],2)."%";
					$finaldata[$key][] = ($datavalue['amount'] == 0)?'':Controller::money_format_inr($datavalue['amount'],2,1);
					$finaldata[$key][] = ($datavalue['cgst'] == 0)?'':Controller::money_format_inr($datavalue['cgst'],2,1);
					$finaldata[$key][] = ($datavalue['sgst'] == 0)?'':Controller::money_format_inr($datavalue['sgst'],2,1);
					$finaldata[$key][] = ($datavalue['igst'] == 0)?'':Controller::money_format_inr($datavalue['igst'],2,1);
					$finaldata[$key][] = "";
				  $finaldata[$key][] = "Nil";
					$total = $datavalue['amount']+$datavalue['cgst']+$datavalue['sgst']+$datavalue['igst'];
					$finaldata[$key][] =  Controller::money_format_inr($total,2);


			}
		}
		else{
					$finaldata[0][] = '';
					$finaldata[0][] = '';
					$finaldata[0][] = '';
					$finaldata[0][] = '';
					$finaldata[0][] = '';
					$finaldata[0][] = '';
					$finaldata[0][] = '';
					$finaldata[0][] = '';
					$finaldata[0][] = '';
					$finaldata[0][] = '';
					$finaldata[0][] = '';
		}


			Yii::import('ext.ECSVExport');

			$csv = new ECSVExport($finaldata);

			$csv->setHeaders($arraylabel);
			$csv->setToAppend();
			$contentdata = $csv->toCSV();
			$csvfilename = 'Exportdata Report-sales' . date('d_M_Y') . '.csv';
			Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
			exit();

		}
		public function actionSavetoExcelContra($fromdate,$todate){
				$tblpx = Yii::app()->db->tablePrefix;
				$fromdate   = date('Y-m-d', strtotime($fromdate));
				$todate     = date('Y-m-d', strtotime($todate));
				$wherec      = '';
				if (!empty($fromdate) && !empty($todate)) {
						$wherec .= " date between '" . $fromdate ."' and'" . $todate . "'";

				} else {
						if (!empty($date_from)) {
								$wherec .= "date >= '" . $fromdate ."'";
						}
						if (!empty($todate)) {
								$date_to = date('Y-m-d', strtotime($todate));
								$wherec .= "date <= '" . $todate . "'";
						}
				}

			$sqlcc=	"SELECT  * FROM " . $tblpx ."dailyexpense as de  WHERE ". $wherec ." AND de.exp_type_id =0 AND de.transaction_parent IS NULL ";
			//de.exp_type_id =0 =>refers to withdrawal from Bank expense head
		$sqlc=	Yii::app()->db->createCommand($sqlcc)->queryAll();

	$arraylabel = array('Date', 'Voucher No', 'To','By','Amount','Narration','Instrument Type','Instrument No','Instrument Date','Bank Name');
 	$finaldata = array();
 	if(!empty($sqlc)){
 	foreach ($sqlc as $key => $datavalue) {
		$trans_type;
		$value;
		if(($datavalue['dailyexpense_receipt_type'] != NULL) && $datavalue['expense_type'] == NULL){
			$trans_type=$datavalue['dailyexpense_receipt_type'];
		}
		else{
			$trans_type=$datavalue['expense_type'];
		}
		if($trans_type == 1){
			$value="Credit";
		}
		else if($trans_type ==88){

			$value="Cheque";
		}
		else if($trans_type ==89){

			$value="Cash";
		}
		else if($trans_type ==103){

			$value="Petty Cash";
		}

 			$finaldata[$key][] = $datavalue['date'];
			$finaldata[$key][] = $datavalue['dailyexp_id'];
			if($datavalue['bank_id'] != ''){
				$bank   = Bank::model()->findByPK($datavalue['bank_id']);
 				$finaldata[$key][] = $bank->bank_name;
			}
			else{
				$finaldata[$key][] = '';
			}
 			$finaldata[$key][] = $value;
			$finaldata[$key][] = Controller::money_format_inr($datavalue['amount'],2);
 			$finaldata[$key][] = $datavalue['description'];
 			$finaldata[$key][] = $value;
 			$finaldata[$key][] = $datavalue['dailyexpense_chequeno'];
 			$finaldata[$key][] = $datavalue['created_date'];
			if($datavalue['bank_id'] != ''){
				$bank   = Bank::model()->findByPK($datavalue['bank_id']);
 				$finaldata[$key][] = $bank->bank_name;
			}
			else{
				$finaldata[$key][] = '';
			}


 	}
 }
 else{
 			$finaldata[0][] = '';
 			$finaldata[0][] = '';
 			$finaldata[0][] = '';
 			$finaldata[0][] = '';
 			$finaldata[0][] = '';
 			$finaldata[0][] = '';
 			$finaldata[0][] = '';
 			$finaldata[0][] = '';
 			$finaldata[0][] = '';
 			$finaldata[0][] = '';
 			$finaldata[0][] = '';
 }


 	Yii::import('ext.ECSVExport');

 	$csv = new ECSVExport($finaldata);

 	$csv->setHeaders($arraylabel);
 	$csv->setToAppend();
 	$contentdata = $csv->toCSV();
 	$csvfilename = 'Exportdata Report-contra' . date('d_M_Y') . '.csv';
 	Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
 	exit();
 }

 public function actionSavetoExcelPayment($fromdate,$todate){
				 $subpay=array();
			 	 $tblpx = Yii::app()->db->tablePrefix;
				 $fromdate   = date('Y-m-d', strtotime($fromdate));
 				$todate     = date('Y-m-d', strtotime($todate));
 				$wherep      = '';
 				if (!empty($fromdate) && !empty($todate)) {
 						$wherep .= " date between '" . $fromdate ."' and'" . $todate . "'";

 				} else {
 						if (!empty($date_from)) {
 								$wherep .= "date >= '" . $fromdate ."'";
 						}
 						if (!empty($todate)) {
 								$date_to = date('Y-m-d', strtotime($todate));
 								$wherep .= "date <= '" . $todate . "'";
 						}
 				}
				$sql="SELECT sp.date as date,sp.payment_id as voucherno,sp.subcontractor_id as name,sp.amount as amount,sp.description as description,sp.payment_type as transactiontype,sp.cheque_no as instrumentno,sp.date as instrumentdate,sp.bank as bank_name FROM " . $tblpx ."subcontractor_payment as sp  WHERE  sp.approve_status='yes' AND ". $wherep ." ";
				
				$sqlsp=	Yii::app()->db->createCommand($sql)->queryAll();

				foreach($sqlsp as $s){
					$s['payment_name'] ="subcontractor";
					array_push($subpay,$s);
				}
				
			   $sqlv=	Yii::app()->db->createCommand("SELECT v.date as date,v.daily_v_id as voucherno,v.vendor_id as name,v.amount as amount,v.description as description,v.payment_type as transactiontype,v.cheque_no as instrumentno,v.date as instrumentdate,v.bank as bank_name  FROM " . $tblpx ."dailyvendors as v WHERE ". $wherep ."  ")->queryAll();
				foreach($sqlv as $s){
					$s['payment_name'] ="vendor";
					array_push($subpay,$s);
				}
				
				$sqld=	Yii::app()->db->createCommand("SELECT v.date as date,v.exp_id as voucherno,v.vendor_id as name,v.amount as amount,v.description as description,v.payment_type as transactiontype,v.cheque_no as instrumentno,v.date as instrumentdate,v.bank_id as bank_name  FROM " . $tblpx ."expenses as v WHERE ". $wherep ." AND v.type=73 ")->queryAll();
				foreach($sqld as $s){
					$s['payment_name'] ="Daybook";
					array_push($subpay,$s);
				}

				$sqldx=	Yii::app()->db->createCommand("SELECT v.date as date,v.dailyexp_id as voucherno,v.vendor_id as name,v.amount as amount,v.description as description,v.expense_type as transactiontype,v.dailyexpense_chequeno as instrumentno,v.date as instrumentdate,v.bank_id as bank_name  FROM " . $tblpx ."dailyexpense as v WHERE ". $wherep ." AND v.exp_type=73 AND parent_status='1'")->queryAll();

				foreach($sqldx as $s){
					$s['payment_name'] ="Daily Expense";
					array_push($subpay,$s);
				}
				
					 $arraylabel = array('Date', 'Voucher No', 'Type','To','Amount','Description','Instrument Type','Instrument No','Instrument Date','Bank Name');
					 $finaldata = array();
					 if(!empty($subpay)){
					 foreach ($subpay as $key => $datavalue) {
						 $value="";
						 $ttype="";
							 $trans_type=$datavalue['transactiontype'];
						 if($trans_type == 1 || $trans_type == 107){
							 $value="Credit";
							 $ttype="Credit";
						 }
						 else if($trans_type ==88){
							 $value="Cheque";
							 $ttype="Bank";
						 }
						 else if($trans_type ==89){
							 $value="Cash";
							 $ttype="Cash";
						 }
						 else if($trans_type ==103){
							 $value="Petty Cash";
							 $ttype="Petty Cash";
						 }
							 $finaldata[$key][] = $datavalue['date'];
							 $finaldata[$key][] = $datavalue['voucherno'];
							 $finaldata[$key][] = $ttype;
							 if($datavalue['payment_name'] == "subcontractor" )
							 {
								$name = "";
								if($datavalue['name'] != ""){
								 	$sname= Subcontractor::model()->findByPK($datavalue['name']);
								 	$name=$sname->subcontractor_name;
								}
							 }
							 else{
								$name = "";
								if($datavalue['name'] != ""){
									$vname= Vendors::model()->findByPK($datavalue['name']);
									$name=$vname->name;
								}								  
							 }
							 $finaldata[$key][] = $name;
							 $finaldata[$key][] = Controller::money_format_inr($datavalue['amount'],2);
							 $finaldata[$key][] = $datavalue['description'];
							 $finaldata[$key][] = $value;
							 $finaldata[$key][] = $datavalue['instrumentno'];
							 $finaldata[$key][] = $datavalue['instrumentdate'];
							 if($datavalue['bank_name'] != ''){
							 $bank   = Bank::model()->findByPK($datavalue['bank_name']);
							 $finaldata[$key][] = $bank->bank_name;
						 }
						 else{
							 	 $finaldata[$key][] = '';
						 }

					 }
					}
					else{
							 $finaldata[0][] = '';
							 $finaldata[0][] = '';
							 $finaldata[0][] = '';
							 $finaldata[0][] = '';
							 $finaldata[0][] = '';
							 $finaldata[0][] = '';
							 $finaldata[0][] = '';
							 $finaldata[0][] = '';
							 $finaldata[0][] = '';
							 $finaldata[0][] = '';
					}
					

					 Yii::import('ext.ECSVExport');

					 $csv = new ECSVExport($finaldata);

					 $csv->setHeaders($arraylabel);
					 $csv->setToAppend();
					 $contentdata = $csv->toCSV();
					 $csvfilename = 'Exportdata Report-payment' . date('d_M_Y') . '.csv';
					 Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
					 exit();
 }
 public function actionsavetoexcelpurchase($fromdate,$todate){
	$tblpx = Yii::app()->db->tablePrefix;
	$fromdate   = date('Y-m-d', strtotime($fromdate));
	$todate     = date('Y-m-d', strtotime($todate));
	$where      = '';
	$purchase_head = array();
	$purchase = array();
	if (!empty($fromdate) && !empty($todate)) {
			$where .= " {$tblpx}bills.bill_date between '" . $fromdate ."' and'" . $todate . "'";

	} else {
			if (!empty($date_from)) {
					$where .= " {$tblpx}bills.bill_date between >= '" . $fromdate ."'";
			}
			if (!empty($todate)) {
					$date_to = date('Y-m-d', strtotime($todate));
					$where .= "{$tblpx}bills.bill_date <= '" . $todate . "'";
			}
	}
	$sql_purchase=	Yii::app()->db->createCommand("SELECT {$tblpx}bills.bill_id,{$tblpx}bills.bill_date,{$tblpx}purchase.project_id,{$tblpx}bills.bill_totalamount FROM {$tblpx}bills LEFT JOIN {$tblpx}purchase ON {$tblpx}bills.purchase_id= {$tblpx}purchase.p_id LEFT JOIN {$tblpx}vendors ON {$tblpx}purchase.vendor_id={$tblpx}vendors.vendor_id WHERE (".$where.") ORDER BY {$tblpx}bills.bill_date desc")->queryAll();
	$data1 = Yii::app()->db->createCommand("SELECT DISTINCT billitem_cgstpercent,billitem_igstpercent,billitem_sgstpercent FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE ".$where." GROUP BY billitem_cgstpercent, billitem_sgstpercent, billitem_igstpercent")->queryAll();
	$data2 = Yii::app()->db->createCommand("SELECT DISTINCT billitem_taxpercent FROM {$tblpx}billitem  LEFT JOIN {$tblpx}bills ON {$tblpx}bills.bill_id ={$tblpx}billitem.bill_id WHERE ".$where." AND billitem_taxpercent > 0  GROUP BY billitem_taxpercent")->queryAll();

	foreach($data1 as $key=> $value){
		$purchase_head[$key]['cgstpercent'] = round($value['billitem_cgstpercent'],0);
		$purchase_head[$key]['sgstpercent'] = round($value['billitem_sgstpercent'],0);
		$purchase_head[$key]['igstpercent'] = round($value['billitem_igstpercent'],0);
	}

	foreach($data2 as $key=> $value){
		$purchase[$key]['taxpercent'] = round($value['billitem_taxpercent'],0);
	}
			$arraylabel = array('','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','');
			$finaldata = array();
			$i=1;
			if(!empty($sql_purchase)){
					$finaldata[0][] = 'Voucher Number';
					$finaldata[0][] = 'Voucher Date';
					$finaldata[0][] = 'Customer Name';
					foreach($purchase as $key_h => $value_h){
						$finaldata[0][] ='Purchase '.$value_h['taxpercent'].' %';
					}
					foreach($purchase_head as $key_h => $value_h){
						$finaldata[0][] ='CGST '.$value_h['cgstpercent'].' %';
						$finaldata[0][] ='SGST '.$value_h['sgstpercent'].' %';
						$finaldata[0][] ='IGST '.$value_h['igstpercent'].' %';
					}
					$finaldata[0][] = 'Round off';
					$finaldata[0][] = 'Total amount';
					foreach($sql_purchase as $key=> $value){
						if($value['project_id']!=''){
						$finaldata[$i][] =$value['bill_id'];
						$finaldata[$i][] =date('Y-m-d',strtotime($value['bill_date']));
						$row_client = Yii::app()->db->createCommand("SELECT {$tblpx}clients.name as name FROM {$tblpx}projects inner Join {$tblpx}clients ON {$tblpx}projects.client_id={$tblpx}clients.cid where {$tblpx}projects.pid =".$value['project_id']." ")->queryRow();
 						$finaldata[$i][] =$row_client['name'];
						foreach($purchase as $key_h => $value_h){
							$row_taxpercent = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as billitem_amount, SUM(billitem_discountamount) as billitem_discountamount FROM {$tblpx}billitem WHERE bill_id=".$value['bill_id']." AND billitem_taxpercent=".$value_h['taxpercent']."")->queryRow();
							$amount = $row_taxpercent['billitem_amount']-$row_taxpercent['billitem_discountamount'];
							$finaldata[$i][] = ($amount ==0)?'':Controller::money_format_inr($amount,2,1);
						}
						foreach($purchase_head as $key1 => $value1){
							$row_cgst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_cgst) as cgst FROM {$tblpx}billitem WHERE bill_id=".$value['bill_id']." AND FORMAT(billitem_cgstpercent,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(".$value1['sgstpercent'].",2) AND FORMAT(billitem_igstpercent,2) = FORMAT(".$value1['igstpercent'].",2)")->queryRow();
							$row_sgst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_sgst) as sgst FROM {$tblpx}billitem WHERE bill_id=".$value['bill_id']." AND FORMAT(billitem_cgstpercent,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(".$value1['sgstpercent'].",2) AND FORMAT(billitem_igstpercent,2) = FORMAT(".$value1['igstpercent'].",2)")->queryRow();
							$row_igst1 = Yii::app()->db->createCommand("SELECT SUM(billitem_igst) as igst FROM {$tblpx}billitem WHERE bill_id=".$value['bill_id']." AND FORMAT(billitem_cgstpercent,2) = FORMAT(".$value1['cgstpercent'].",2) AND FORMAT(billitem_sgstpercent,2) = FORMAT(".$value1['sgstpercent'].",2) AND FORMAT(billitem_igstpercent,2) = FORMAT(".$value1['igstpercent'].",2)")->queryRow();
							$purchase_cgst =   $row_cgst1['cgst'];
							$purchase_sgst = $row_sgst1['sgst'];
							$purchase_igst = $row_igst1['igst'];
							$finaldata[$i][] = ($purchase_cgst == 0)?'':Controller::money_format_inr($purchase_cgst,2,1);
							$finaldata[$i][] = ($purchase_sgst == 0)?'':Controller::money_format_inr($purchase_sgst,2,1);
							$finaldata[$i][] = ($purchase_igst == 0)?'':Controller::money_format_inr($purchase_igst,2,1);
						}
						$finaldata[$i][] = 'Nil';
						$finaldata[$i][] = $value['bill_totalamount'];
						$i++;
					}
				}
			}
		else{
					$finaldata[0][] = '';
					$finaldata[0][] = '';
					$finaldata[0][] = '';
		}


			Yii::import('ext.ECSVExport');

			$csv = new ECSVExport($finaldata);

			$csv->setHeaders($arraylabel);
			$csv->setToAppend();
			$contentdata = $csv->toCSV();
			$csvfilename = 'Exportdata Report-purchase' . date('d_M_Y') . '.csv';
			Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
			exit();
}
 		public function actionsavetoexcelexpense($fromdate,$todate){

 				$tblpx = Yii::app()->db->tablePrefix;
 				$fromdate   = date('Y-m-d', strtotime($fromdate));
 				$todate     = date('Y-m-d', strtotime($todate));
 				$where      = '';
 				if (!empty($fromdate) && !empty($todate)) {
 						$where .= " date between '" . $fromdate ."' and'" . $todate . "'";
 				} else {
 						if (!empty($date_from)) {
 								$where .= " date between >= '" . $fromdate ."'";
 						}
 						if (!empty($todate)) {
 								$date_to = date('Y-m-d', strtotime($todate));
 								$where .= "date <= '" . $todate . "'";
 						}
 				}
				$sql="SELECT * FROM {$tblpx}expenses WHERE ".$where." AND bill_id IS NULL AND  subcontractor_id is NULL AND vendor_id IS NULL ORDER BY date DESC";
 				$sql1=	Yii::app()->db->createCommand($sql)->queryAll();
				
				$sql_2="SELECT *,dailyexp_id as exp_id,dailyexpense_receipt_type as payment_type,dailyexpense_receipt as receipt,dailyexpense_chequeno as cheque_no  FROM {$tblpx}dailyexpense WHERE ".$where." AND exp_type=72 AND parent_status='1' ORDER BY date DESC";	
				$sql2=	Yii::app()->db->createCommand($sql_2)->queryAll();
				//die($sql_2);
				$sql = array_merge($sql1,$sql2);
				//echo "<pre>";print_r($sql);exit;
 			  	$arraylabel = array('Voucher Number', 'Voucher Date','Transaction type', 'Customer Name','Amount','Description','Instrument Type','Instrument No','Instrument Date','Bank Name');
 				 $finaldata = array();
 				if(!empty($sql)){
 				foreach ($sql as $key => $datavalue) {
 						$finaldata[$key][] = $datavalue['exp_id'];
 						$finaldata[$key][] = $datavalue['date'];
 						$transaction 			 = $datavalue['payment_type'];
 						$transaction_type= '';
 						if($transaction == 0){
 							$transaction_type = 'Credit';
 						}else if($transaction ==88){
 							$transaction_type = 'Bank';
 						}else if($transaction ==89){
 							$transaction_type = 'Cash';
 						}else{
 							$transaction_type = 'Petty Cash';
 						}
 						$finaldata[$key][] = $transaction_type;
 						$project_id = isset($datavalue['projectid'])?$datavalue['projectid']:"";
 						$clients = Yii::app()->db->createCommand("SELECT {$tblpx}clients.name FROM {$tblpx}projects LEFT JOIN {$tblpx}clients ON {$tblpx}projects.client_id ={$tblpx}clients.cid")->queryRow();
						if(!empty($datavalue['invoice_id'])){
							$inv_det = Invoice::model()->findByPk($datavalue['invoice_id']);
							//echo "<pre>";print_r($inv_det);exit;
							if(!empty($inv_det)){
								$clientId=$inv_det["client_id"];
								$sql = "SELECT {$tblpx}clients.name FROM {$tblpx}clients WHERE {$tblpx}clients.cid = :client_id";
								$clients = Yii::app()->db->createCommand($sql)
									->bindParam(':client_id', $clientId, PDO::PARAM_INT)
									->queryRow();
							}

						}
 						$finaldata[$key][] = $clients['name'];
 						$finaldata[$key][] = Controller::money_format_inr($datavalue['receipt'],2);
 						$finaldata[$key][] = $datavalue['description'];
 						$paymenttype 			 = $datavalue['payment_type'];
 						$type= '';
 						if($paymenttype == 0){
 							$type = 'Credit';
 						}else if($paymenttype ==88){
 							$type = 'Cheque';
 						}else if($paymenttype ==89){
 							$type = 'Cash';
 						}else{
 							$type = 'Petty Cash';
 						}
 						$finaldata[$key][] = $type;
 						$finaldata[$key][] = $datavalue['cheque_no'];
 						$finaldata[$key][] = $datavalue['date'];
 						$bank = Bank::model()->findByPk($datavalue['bank_id']);
 						$finaldata[$key][] = ($datavalue['bank_id'] !='')?$bank->bank_name:'';
 				}
 			}
 			else{
 						$finaldata[0][] = '';
 						$finaldata[0][] = '';
 						$finaldata[0][] = '';
 						$finaldata[0][] = '';
 						$finaldata[0][] = '';
 						$finaldata[0][] = '';
 						$finaldata[0][] = '';
 						$finaldata[0][] = '';
 						$finaldata[0][] = '';
 						$finaldata[0][] = '';
 						$finaldata[0][] = '';
 			}
 				Yii::import('ext.ECSVExport');

 				$csv = new ECSVExport($finaldata);

 				$csv->setHeaders($arraylabel);
 				$csv->setToAppend();
 				$contentdata = $csv->toCSV();
 				$csvfilename = 'Exportdata Report-expense' . date('d_M_Y') . '.csv';
 				Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
 				exit();
 		}
		public function actionsavetoexcelreturn($fromdate,$todate){
			$tblpx = Yii::app()->db->tablePrefix;
			$fromdate   = date('Y-m-d', strtotime($fromdate));
			$todate     = date('Y-m-d', strtotime($todate));
			$where      = '';
			if (!empty($fromdate) && !empty($todate)) {
					$where .= " preturn.return_date between '" . $fromdate ."' and'" . $todate . "'";
			} else {
					if (!empty($date_from)) {
							$where .= " preturn.return_date between >= '" . $fromdate ."'";
					}
					if (!empty($todate)) {
							$date_to = date('Y-m-d', strtotime($todate));
							$where .= "preturn.return_date <= '" . $todate . "'";
					}
			}
			$sql=	Yii::app()->db->createCommand("SELECT preturn.* FROM {$tblpx}purchase_return preturn LEFT JOIN {$tblpx}bills bills ON preturn.bill_id=bills.bill_id WHERE ".$where."")->queryAll();
			$data1 = Yii::app()->db->createCommand("SELECT DISTINCT item.returnitem_taxpercent,item.returnitem_id FROM jp_purchase_returnitem item LEFT JOIN jp_purchase_return preturn ON preturn.return_id=item.return_id WHERE ".$where." GROUP BY item.returnitem_taxpercent ")->queryAll();
			$data2 = Yii::app()->db->createCommand("SELECT DISTINCT item.returnitem_cgstpercent,item.returnitem_sgstpercent,item.returnitem_igstpercent,item.returnitem_id FROM jp_purchase_returnitem item LEFT JOIN jp_purchase_return preturn ON preturn.return_id=item.return_id WHERE ".$where." GROUP BY item.returnitem_cgstpercent,item.returnitem_sgstpercent,item.returnitem_igstpercent ")->queryAll();

			$arraylabel = array('','','','','','','','','','','','','','','','','','');
			$finaldata = array();
			if(!empty($sql)){
				$finaldata[0][] = 'Sl No.';
				$finaldata[0][] = 'Party';
				foreach($data1 as $key=>$data){
					$finaldata[0][] = "Purchase return ".$data['returnitem_taxpercent']." %";
				}
				foreach($data2 as $key=>$data){
					$finaldata[0][] = "CGST ".round($data['returnitem_cgstpercent'],0)." %";
					$finaldata[0][] = "SGST ".round($data['returnitem_sgstpercent'],0)." %";
					$finaldata[0][] = "IGST ".round($data['returnitem_igstpercent'],0)." %";
				}
				$finaldata[0][] = 'Round off';
				$finaldata[0][] = 'Total';
				$i=1;
				foreach($sql as $key1=> $data){
					$finaldata[$i][] = $i;
					$vendor = Yii::app()->db->createCommand("SELECT {$tblpx}vendors.name FROM {$tblpx}purchase LEFT JOIN {$tblpx}bills ON {$tblpx}purchase.p_id={$tblpx}bills.purchase_id LEFT JOIN {$tblpx}vendors ON {$tblpx}purchase.vendor_id={$tblpx}vendors.vendor_id WHERE {$tblpx}bills.bill_id=".$data['bill_id']."")->queryRow();
					$finaldata[$i][] = $vendor['name'];
					foreach($data1 as $key2=>$value){
							$purchase_amount = Yii::app()->db->createCommand("SELECT sum(returnitem_amount)as returnitem_amount, sum(returnitem_discountamount) as returnitem_discountamount FROM {$tblpx}purchase_returnitem WHERE returnitem_taxpercent=".$value['returnitem_taxpercent']." AND return_id=".$data['return_id']."")->queryRow();
							$purchaseamt = $purchase_amount['returnitem_amount']-$purchase_amount['returnitem_discountamount'];
							$finaldata[$i][] = ($purchaseamt == 0)?'':Controller::money_format_inr($purchaseamt,2,1);
					}
					foreach($data2 as $key3=>$value1){
							//$tax_com = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}purchase_returnitem WHERE returnitem_id=".$value1['returnitem_id']." AND return_id=".$data['return_id']."")->queryRow();
							$row_cgst1 = Yii::app()->db->createCommand("SELECT SUM(returnitem_cgst) as cgst FROM {$tblpx}purchase_returnitem WHERE return_id=".$data['return_id']." AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['returnitem_cgstpercent'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['returnitem_sgstpercent'].",2) AND FORMAT(returnitem_igstpercent,2) = FORMAT(".$value1['returnitem_igstpercent'].",2)")->queryRow();
							$row_cgst2 = Yii::app()->db->createCommand("SELECT SUM(returnitem_sgst) as sgst FROM {$tblpx}purchase_returnitem WHERE return_id=".$data['return_id']." AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['returnitem_cgstpercent'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['returnitem_sgstpercent'].",2) AND FORMAT(returnitem_igstpercent,2) = FORMAT(".$value1['returnitem_igstpercent'].",2)")->queryRow();
							$row_cgst3 = Yii::app()->db->createCommand("SELECT SUM(returnitem_igst) as igst FROM {$tblpx}purchase_returnitem WHERE return_id=".$data['return_id']." AND FORMAT(returnitem_cgstpercent,2) = FORMAT(".$value1['returnitem_cgstpercent'].",2) AND FORMAT(returnitem_sgstpercent,2) = FORMAT(".$value1['returnitem_sgstpercent'].",2) AND FORMAT(returnitem_igstpercent,2) = FORMAT(".$value1['returnitem_igstpercent'].",2)")->queryRow();

							// $finaldata[$i][] = $tax_com['returnitem_cgst'];
							// $finaldata[$i][] = $tax_com['returnitem_sgst'];
							// $finaldata[$i][] = $tax_com['returnitem_igst'];

							$finaldata[$i][] = ($row_cgst1['cgst'] == 0)?'':Controller::money_format_inr($row_cgst1['cgst'],2,1);
							$finaldata[$i][] = ($row_cgst2['sgst'] == 0)?'':Controller::money_format_inr($row_cgst2['sgst'],2,1);
							$finaldata[$i][] = ($row_cgst3['igst'] == 0)?'':Controller::money_format_inr($row_cgst3['igst'],2,1);
					}
					$finaldata[$i][] = 'Nil';
					$finaldata[$i][] = ($data['return_totalamount'] == 0)?'':Controller::money_format_inr($data['return_totalamount'],2,1);
					$i++;
				}
			}

			Yii::import('ext.ECSVExport');

			$csv = new ECSVExport($finaldata);

			$csv->setHeaders($arraylabel);
			$csv->setToAppend();
			$contentdata = $csv->toCSV();
			$csvfilename = 'Exportdata Report-Purchase Return' . date('d_M_Y') . '.csv';
			Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
			exit();
		}

	}
