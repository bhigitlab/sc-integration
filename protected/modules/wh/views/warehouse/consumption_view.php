<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/additional-methods.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

    <?php
/* @var $this Controller */
/* @var $model ConsumptionRequest */
/* @var $modelItems PmsAccWprItemConsumed[] */
$pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
                        
?>

<div class="container">
     <div class="expenses-heading header-container">
        <h3>CONSUMPTION REQUEST</h3>
        <?php if(($model['status'] != 1) ){ 
            $bgcolor_status=""; 
            ?>
        <div class="btn-container ">
        <button id="<?php echo $model['id']; ?>" data-id="<?php echo $model['id']; ?>" style="" class="btn btn-xs btn-info btn-approve approveoption_<?php echo $model['id']; ?>">Approve</button>
        </div>
           
    <?php } ?>

    </div>
    
    <br><br>
     <div id="msg"></div>
    <div class="row">
        <div class="col-md-6">
            <p><strong>PROJECT :</strong> <?php 
                $pmodel  = Projects::model()->findBypk($model->coms_project_id);
                echo isset($pmodel->name)?$pmodel->name:'';
                ?>
            </p>
            <p><strong>WAREHOUSE :</strong> <?php 
                $wmodel = Warehouse::model()->findByPk($model->warehouse_id);
                echo isset($wmodel->warehouse_name) ? CHtml::encode($wmodel->warehouse_name) : '';
            ?></p>

            <?php 
            if($pms_api_integration ==1){ ?>
                 <p><strong>TASK NAME :</strong> <?= CHtml::encode($model->task_name); ?></p>
            <?php }
            ?>
           
        </div>
        <div class="col-md-6">
            <p><strong>DATE :</strong> <?= CHtml::encode(date('d-m-Y', strtotime($model->date))); ?></p>
            <p><strong>STATUS :</strong> <?= CHtml::encode($model->status ? 'Approved' : 'Pending'); ?></p>
            <p><strong>REMARKS :</strong> <?= CHtml::encode($model->remarks); ?></p>
        </div>
    </div>
    
        <ul class="legend ">
            <li><span style="background-color:yellow;"></span> Material Not Estimated For This Project in Estimation</li>
        </ul>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Sl.No</th>
                <th>Item Name</th>
                <th>Quantity</th>
                <th>Unit</th>
                <th>Rate</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $bgcolor_status='';
            foreach ($modelItems as $index => $item): 
               $material=$item["coms_material_id"];
                $sql = "SELECT m.id, m.material, m.quantity, m.amount, m.used_quantity, m.used_amount 
                    FROM `jp_material_estimation` m 
                    LEFT JOIN `jp_itemestimation` e ON e.itemestimation_id = m.estimation_id 
                    WHERE e.project_id = {$model->coms_project_id} 
                    AND e.itemestimation_status = 2  
                    AND m.project_id = {$model->coms_project_id} 
                    AND m.material = {$material}";
                $estimated_det = Yii::app()->db->createCommand($sql)->queryRow();
            //echo "<pre>";print_r($sql);exit;
                    
            $material_det = Materials::model()->findByPk($material);
           
            $estimated_det = Yii::app()->db->createCommand($sql)->queryRow();

            if (!$estimated_det) {
                $bgcolor_status = 'style ="background-color:yellow"';
            }?>
                <tr>
                    <td><?= $index + 1; ?></td>
                    <td <?php echo $bgcolor_status;?>>
                        <?php 
                        $specModel = Specification::model()->findByPk($item->item_id);
                        echo isset($specModel->specification) ? CHtml::encode($specModel->specification) : CHtml::encode($item->item_id);
                        ?>
                    </td>
                    <td><?= CHtml::encode($item->item_qty); ?></td>
                    <td>Nos</td> <!-- Replace with actual unit if available -->
                    <td><?= CHtml::encode(number_format($item->item_rate, 2)); ?></td>
                    <td><?= CHtml::encode(number_format($item->item_rate * $item->item_qty, 2)); ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div class="total">
        <p><strong>TOTAL AMOUNT: </strong>
            <?php
            $totalAmount = 0;
            foreach ($modelItems as $item) {
                $totalAmount += $item->item_rate * $item->item_qty;
            }
            echo CHtml::encode(number_format($totalAmount, 2));
            ?>
        </p>
    </div>
</div>

<style>
    .container {
        width: 80%;
        margin: auto;
    }
    .table {
        width: 100%;
        margin-top: 20px;
        border-collapse: collapse;
    }
    .table, .table th, .table td {
        border: 1px solid #000;
        text-align: center;
    }
    .total {
        margin-top: 20px;
        text-align: right;
    }
</style>
<script>
    $(document).on("click", ".btn-approve", function () {
        var id = $(this).attr('data-id');
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "<?php echo $this->createUrl('warehouse/approveConsumedQuantity') ?>",
            data: {
                id: id
            },
            success: function (response) {
                if (response.response === 'warning') {
                    var errorAlert = '<div class="alert alert-warning alert-dismissible" role="alert">' +
                                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                        '<span aria-hidden="true">&times;</span>' +
                                    '</button>' +
                                    response.msg +
                                '</div>';
                $("#msg").html(errorAlert);
                } else {
                    $("#msg").html('<div class="alert alert-' + response.response + '">' + response.msg + '</div>');

                    setTimeout(function () {
                        window.location.reload(1);
                    }, 2000);
                }
            }
        })
    })
</script>