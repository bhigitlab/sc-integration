<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">

<?php
    $company_address = Company::model()->findBypk(($company_id)?$company_id:Yii::app()->user->company_id);	
?>
    <div style="margin:0px 30px">
        <br>
        <h3 class="text-center">Receipt Report</h3>
        <h4>Cash In Hand <?= $duration; ?> </h4>
        <table border="1" class="table">
            <thead>
                <tr>
                    <th>Sl No.</th>
                    <th>Project</th>
                    <th>Party Name</th>
                    <th>Site</th>
                    <th>Date</th>
                    <th>Receipt Amount</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="5" align="right"><b>Total</b></td>
                    <td align="right"><b><?= Controller::money_format_inr($cash_total, 2)?></b></td>
                </tr>
                
                <?php echo $entries; ?>
            </tbody>
        </table>

        <h4>Bank Balance <?= $duration; ?> </h4>
        <table border="1" class="table">
            <thead>
                <tr>
                    <th>Sl No.</th>
                    <th>Project</th>
                    <th>Party Name</th>
                    <th>Site</th>
                    <th>Date</th>
                    <th>Receipt Amount</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="5" align="right"><b>Total</b></td>
                    <td align="right"><b><?= Controller::money_format_inr($bank_total, 2)?></b></td>
                </tr>
                
                <?php echo $entries2; ?>
            </tbody>
        </table>
    </div>
