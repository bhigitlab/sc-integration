<?php
/* @var $this QuotationCategoryMasterController */
/* @var $model QuotationCategoryMaster */
/* @var $form CActiveForm */
?>
<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'location-master-data-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnChange' => true,
            'validateOnType' => false,
        )
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <div class="col-xs-12">
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textField($model, 'name', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
        </div>
        <?php echo $form->error($model, 'name'); ?>
        <div class="col-md-6 save-btnHold">
            <label style="display:block;">&nbsp;</label>
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-sm btn-info')); ?>
            <?php
            if (!$model->isNewRecord) {
                echo CHtml::Button('Close', array('class' => 'btn', 'onclick' => 'javascript:location.href="' . $this->createUrl('index') . '"'));
            } else {
                echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;', 'class' => 'btn btn-sm btn-default'));
            }
            ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>