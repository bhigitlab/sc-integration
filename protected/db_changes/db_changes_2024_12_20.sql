ALTER TABLE `pms_acc_wpr_item_consumed` ADD `item_amount` DOUBLE(11,2) NULL AFTER `pms_task_name`;
ALTER TABLE `jp_scquotation_items` CHANGE `item_amount` `item_amount` DOUBLE(13,2) NOT NULL;