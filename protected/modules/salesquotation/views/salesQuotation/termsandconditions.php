<script src="<?php echo Yii::app()->baseUrl . '/ckeditor/ckeditor.js'; ?>"></script>
<style>
    .cke_wysiwyg_frame{
        height:300px !important;
    }
    
</style>
<div class="container">
    <h2>Terms and Conditions</h2>
    <div class="row">
        <div class="col-md-8">            
            <input type="hidden" value="<?php echo $terms['id'] ?>" class="term_id">
            <textarea id="editor1" name="editor1"><?php echo $terms['terms_and_conditions'] ?></textarea>
        </div>        
        <div class="col-md-8 text-right">
            <label class="d-block">&nbsp;</label>
            <button class="btn btn-primary save_terms">Save</button>
        </div>
    </div>                                   
</div>
<script type="text/javascript">    
    CKEDITOR.replace( 'editor1' );
   
    $(".save_terms").click(function(){
        var term_id =$(".term_id").val();    
        var terms = $("#cke_editor1").find("iframe").contents().find("body").html();
        $.ajax({
            url: '<?php echo  Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/saveterms'); ?>',
            type: 'POST',            
            data: {
                term_id: term_id,
                terms:terms
            },
            success: function(response) {
                alert(response);
            }
        })        
   })
   

</script>
