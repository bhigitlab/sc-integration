ALTER TABLE `jp_sales_quotation` DROP FOREIGN KEY `jp_sales_quotation_ibfk_2`;
ALTER TABLE `jp_sales_quotation` DROP FOREIGN KEY `jp_sales_quotation_ibfk_3`;
ALTER TABLE `jp_sales_quotation` DROP FOREIGN KEY `jp_sales_quotation_ibfk_4`;
ALTER TABLE `jp_sales_quotation` DROP FOREIGN KEY `jp_sales_quotation_ibfk_5`;
ALTER TABLE `jp_sales_quotation` DROP FOREIGN KEY `jp_sales_quotation_ibfk_6`; 

ALTER TABLE `jp_quotation_item_master` DROP FOREIGN KEY `jp_quotation_item_master_ibfk_1`;
ALTER TABLE `jp_quotation_item_master` DROP FOREIGN KEY `jp_quotation_item_master_ibfk_10`;
ALTER TABLE `jp_quotation_item_master` DROP FOREIGN KEY `jp_quotation_item_master_ibfk_11`;
ALTER TABLE `jp_quotation_item_master` DROP FOREIGN KEY `jp_quotation_item_master_ibfk_2`;
ALTER TABLE `jp_quotation_item_master` DROP FOREIGN KEY `jp_quotation_item_master_ibfk_3`;
ALTER TABLE `jp_quotation_item_master` DROP FOREIGN KEY `jp_quotation_item_master_ibfk_6`;
ALTER TABLE `jp_quotation_item_master` DROP FOREIGN KEY `jp_quotation_item_master_ibfk_7`;
ALTER TABLE `jp_quotation_item_master` DROP FOREIGN KEY `jp_quotation_item_master_ibfk_8`;
ALTER TABLE `jp_quotation_item_master` DROP FOREIGN KEY `jp_quotation_item_master_ibfk_9`;

TRUNCATE `jp_sales_quotation`;
TRUNCATE `jp_quotation_gen_worktype`;
TRUNCATE `jp_quotation_gen_category`;
TRUNCATE `jp_quotation_section`; 
TRUNCATE `jp_sales_quotation_master`;
TRUNCATE `jp_quotation_item_master`; 
TRUNCATE `jp_quotation_worktype_master` ;
TRUNCATE `jp_quotation_category_master`;

ALTER TABLE `jp_sales_quotation`
ADD CONSTRAINT `jp_sales_quotation_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `jp_quotation_category_master` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `jp_sales_quotation_ibfk_3` FOREIGN KEY (`work_type`) REFERENCES `jp_quotation_worktype_master` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `jp_sales_quotation_ibfk_4` FOREIGN KEY (`unit`) REFERENCES `jp_unit` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `jp_sales_quotation_ibfk_5` FOREIGN KEY (`created_by`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE,
ADD CONSTRAINT `jp_sales_quotation_ibfk_6` FOREIGN KEY (`item_id`) REFERENCES `jp_quotation_item_master` (`id`) ON UPDATE CASCADE;


ALTER TABLE `jp_quotation_item_master`
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_1` FOREIGN KEY (`quotation_category_id`) REFERENCES `jp_quotation_category_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_10` FOREIGN KEY (`finish_id`) REFERENCES `jp_quotation_finish_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_11` FOREIGN KEY (`updated_by`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_2` FOREIGN KEY (`work_type_id`) REFERENCES `jp_quotation_worktype_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_3` FOREIGN KEY (`shutter_finish_id`) REFERENCES `jp_quotation_finish_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_6` FOREIGN KEY (`carcass_finish_id`) REFERENCES `jp_quotation_finish_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_7` FOREIGN KEY (`created_by`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_8` FOREIGN KEY (`quotation_category_id`) REFERENCES `jp_quotation_category_master` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jp_quotation_item_master_ibfk_9` FOREIGN KEY (`work_type_id`) REFERENCES `jp_quotation_worktype_master` (`id`) ON UPDATE CASCADE;