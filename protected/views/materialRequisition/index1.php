<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo $this->customAssets('https://code.jquery.com/jquery-1.12.4.js', '/js/jquery-1.12.4.js'); ?>"></script>
<script src="<?php echo $this->customAssets('https://code.jquery.com/ui/1.12.1/jquery-ui.js', '/js/jquery-ui.js'); ?>"></script> -->
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<style>
    .alert {
        padding: 9px;
        background-color: #d9edf7;
        color: #31708f;
    }
    .select2-container.select2-container--default.select2-container--open{
        z-index:9999999 !important;
    }
    .select2.select2-container{
        width:100% !important;
    }
</style>
<?php
/* @var $this MaterialRequisitionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Material Requisitions',
);

// $this->menu = array(
//     array('label' => 'Create MaterialRequisition', 'url' => array('create')),
//     array('label' => 'Manage MaterialRequisition', 'url' => array('admin')),
// );
?>

<div class="container">
    <h2>Material Requisitions</h2>
    <div id="alert"></div>
    <div class="alert <?= Yii::app()->user->hasFlash('error') ? "" : "hide" ?>">
        <?php
        if (Yii::app()->user->hasFlash('error')) {
            echo Yii::app()->user->getFlash('error');
        }
        ?>
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    </div>
    <div class="clearfix">
        <div class="add-btn pull-right" style="margin-left:10px;">
                <?php 
                    echo CHtml::link('Material Requisition from pms', array('MaterialRequisition/index'), array('class' => 'btn btn-info', 'id' => 'materialrequest')); 
                ?>
        </div>
        <div class="add-btn pull-right">
                <?php 
                    echo CHtml::link('Add Material Requisition', array('MaterialRequisition/create', 'type' => 1), array('class' => 'btn btn-info', 'id' => 'materialrequest')); 
                ?>
        </div>
    </div>

    <div id="reject_form" data-toggle="modal" data-target="#reviewLoginModal" class="modal">

    </div>
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'material-requisition-grid',
        'dataProvider' => $model->searchaccounts(),
        // 'ajaxUpdate' => false,
        'itemsCssClass' => 'table table-bordered',
        'filter' => $model,
        'pager' => array(
            'id' => 'dataTables-example_paginate',  'header' => '', 'prevPageLabel' => 'Previous ',
            'nextPageLabel' => 'Next '
        ),
        'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
        'dataProvider' => $model->searchaccounts(),
        'filter' => $model,
        'columns' => array(

            array('class' => 'IndexColumn', 'header' => 'S.No.', 'htmlOptions' => array('style' => 'width:20px;'),),

            array(
                'name' => 'requisition_no',
                'value' => '$data->requisition_no',
                'type' => 'raw',
            ),
            array(
                'name' => 'project_id',
                'type' => 'raw',
                'value' => 'isset($data->project->name)?$data->project->name:"Nil"',
                'filter' => CHtml::listData(Projects::model()->findAll(
                    array(
                        'select' => array('pid,name'),
                        'order' => 'name',
                        'distinct' => true
                    )
                ), "pid", "name"),
            ),
            array(
                'name' => 'task_description',
                'value' => 'isset($data->task_description)?$data->task_description:"Nil"',
                'type' => 'raw',
            ),
            array(
                'name' => 'purchase_id',
                'type' => 'raw',
                'value' => function($data, $row) {
                    $purchase_id = $data->purchase_id;
                    return (isset($purchase_id) ? CHtml::link($data->getPurchaseName($purchase_id),array('purchase/viewpurchase', 'pid'=>$purchase_id),array('target'=>'_blank')) : "Nil");                                        
                },               
            ),
            array(
                'name' => 'purchase_id',
                'type' => 'raw',
                'value' => function($data, $row) {
                    $purchase_id = $data->purchase_id;
                 
                    return (isset($purchase_id) ? $data->getBillingStatus($purchase_id):"Nil");
                    
                },  
                'header'=>'Billing Status'
                
            ),
            array(
                'htmlOptions' => array('class' => 'nowrap'),
                'class' => 'ButtonColumn',
                'template' => '{view}{add_po}',
                'evaluateID' => true,
                
                'buttons' => array(
                    'view' => array(
                        'label' => '<button class="btn btn-default btn-xs">View</button>',
                        'imageUrl' => false,
                        'options' => array(
                            'id' => '$data->requisition_id',
                            // 'title' => '$data->requisition_id',
                        ),
                    ),
                    'add_po' => array(
                        'label' => '<button class="btn btn-default btn-xs ml-1 btn_po" data-toggle="modal" data-target="#exampleModal">Add PO</button>',
                        'visible'=>'$data->purchase_id==""',
                        'imageUrl' => false,
                        'options' => array(
                            'id' => '$data->requisition_no',
                            'title' => '$data->requisition_id',
                            
                        ),
                        'htmlOptions' => array('class' => 'nowrap'),
                        
                    ),
                    


                )

            ),


        ),
    ));
    ?>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header clearfix">
                    <h5 class="modal-title pull-left mt-2" id="exampleModalLabel">ADD PO</h5>
                    <button type="button" class="close  pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                        <input type="hidden" id="req_id" value="">
                        <input type="text" id="req_no" value=""  class="form-control" readonly>
                        </div>
                        <div class="col-md-6">
                        <?php
                        $command = Yii::app()->db->createCommand();
                        $command->select('p.p_id,p.purchase_no');
                        $command->from($this->tableNameAcc('purchase', 0) . ' as p');
                        
                        $command->where(' p.purchase_status = "saved" '
                                . 'AND p.purchase_no IS NOT NULL ');
                        $command->order('p.p_id');
                        $purchase = $command->queryAll();
                        $data = CHtml::listData($purchase, 'p_id', 'purchase_no');
                        
                        echo  CHtml::dropDownList('', 'purchase_id', $data,
                                array('empty' => '-Select Purchase-','class'=>'select_box','id'=>'po_number')
                            );                                        
                        ?>
                        </div>
                    </div>
                   
                </div>
                <div class="modal-footer">
                    
                    <button type="button" class="btn btn-primary add_po">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>




<script>
    $(".select_box").select2();
    $(document).on('click', ".approve", function() {
        if (!confirm("Do you want to Approve ?")) {} else {

            var id = this.id;
            $.ajax({
                url: '<?php echo Yii::app()->createUrl('materialRequisition/approveentry'); ?>',
                data: {
                    id: id
                },
                method: "POST",
                dataType: 'json',
                success: function(result) {

                    if (result.status == 1) {
                        $('.alert').removeClass('hide');
                        $('.alert').css('display', 'block');
                        $('.alert').append(result.message);
                        setTimeout(location.reload.bind(location), 1000);
                    } else {
                        $('.alert').removeClass('hide');
                        $('.alert').css('display', 'block');
                        $('.alert').append(result.message);
                    }
                }
            })
        }
    });

    $(document).on('click', ".reject", function() {
        var id = this.id;
        $.ajax({
            "type": "POST",

            "url": "<?php echo Yii::app()->createUrl('materialRequisition/reject_mr'); ?>",
            "dataType": "json",
            "data": {
                id: id
            },
            "success": function(data) {
                $("#reject_form").html(data).dialog({
                    title: "Reject MR"
                });
            }
        });
    });
    $(document).on('click', ".ui-dialog-titlebar-close", function() {

        window.location.reload();
    });
    $(document).on("click", ".btn_po", function () {
        var req_no = $(this).parent().attr('id');
        var req_id = $(this).parent().siblings().attr('id');
        $('#req_id').val( req_id );
        $('#req_no').val( req_no );
        
    });

    $(document).on("click", ".add_po", function () {
        var po_number = $('#po_number').val();
        var req_id =$('#req_id').val();
    
        $.ajax({
            "type": "POST",
            "url": "<?php echo Yii::app()->createUrl('materialRequisition/addPurchaseData'); ?>",
            "dataType": "json",
            "data": {
                req_id: req_id,
                po_number:po_number
            },
            "success": function(data) {
                
                setTimeout(function(){
                window.location.reload(1);
                }, 1000);
            }
        });
    });
</script>