<style>
.row_class{
    
    background-color:#f8cbcb !important;
}
</style>
<?php
if ($type == 2) {
$template = Yii::app()->db->createCommand("SELECT * FROM `jp_labour_template` "
. " WHERE `id` = ".$template_id)->queryRow();

if($template){
 $arrVal = explode(',', $template['labour_type']); ?>
 <table class="table" style="margin-bottom:0px;">
            <thead>
            <tr>
                <th>Labour label</th>
                <th>Labour Rate</th>
                </tr>
            </thead>
            <tbody>                    
                <?php
    foreach ($arrVal as $val) {
    
        $labour_data = Yii::app()->db->createCommand("SELECT * FROM `jp_labour_worktype` WHERE `type_id` = ".$val)->queryRow();
        ?>
        <tr>
        <td><?php echo $labour_data['worktype']; ?></td>
        <td class="text-right">
            <input type="text" name="Scquotation[lab_templates][<?php echo $val; ?>][rate]" value="<?php echo Controller::money_format_inr($labour_data['rate'], 2); ?>" class="form-control w-100p pull-right" />
            <input type="hidden" name="Scquotation[lab_templates][<?php echo $val; ?>][type_id]" value="<?php echo $labour_data["type_id"]; ?>" />
        </td>
    </tr>
            
        <?php
    } ?>
    </tbody>
        </table>
        <?php
}
}elseif($type==1){
    $arrVal = Yii::app()->db->createCommand("SELECT * FROM `jp_sc_quot_labour_template` WHERE `template_id` = " . $template_id . " AND `sc_quot_id` =" . $project)->queryAll();
   
if($arrVal){
 ?>
 <table class="table" style="margin-bottom:0px;margin-top: 21px;">
            <thead>
            <tr>
                <th>Labour label</th>
                <th>Labour Rate</th>
                </tr>
            </thead>
            <tbody>                    
                <?php
    foreach ($arrVal as $val) {
        //die($val);
        $row_class = ($val['rate_value_differ'] == '1') ? 'row_class' : '';
        $labour_data = Yii::app()->db->createCommand("SELECT * FROM `jp_labour_worktype` WHERE `type_id` = " . $val['labour_id'])->queryRow();
        
         ?>
        <tr class="<?php echo $row_class; ?>">
                <td><?php echo $labour_data['worktype']; ?></td>
              
        <td class="text-right">
            <input type="text" name="Scquotation[lab_templates][<?php echo $val['labour_id']; ?>][rate]" value="<?php echo Controller::money_format_inr($val['labour_rate'], 2); ?>" class="form-control w-100p pull-right" />
            <input type="hidden" name="Scquotation[lab_templates][<?php echo $val['labour_id']; ?>][type_id]" value="<?php echo  $val['labour_id'] ?>" />
        </td>
        </tr>
            
        <?php
    } ?>
    </tbody>
        </table>
        <?php
}
}
?>