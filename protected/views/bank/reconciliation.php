<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Users',
);

?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<link rel="mask-icon" href="../favicons/safari-pinned-tab.svg" color="#ee3342">
<div class="container" id="user">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix">

        <h2>Bank Reconciliation</h2>
    </div>

    <?php $this->renderPartial('_reconnewsearch', array('model' => $model)) ?>



    <?php if (Yii::app()->user->hasFlash('success')) : ?>
        <div class="info d-block mb-2 alert alert-success">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>


    <?php
if (Yii::app()->user->hasFlash('error')) :
    $errorMessages = Yii::app()->user->getFlash('error');
    // Decode the JSON string to convert it into an array
    $errorMessagesArray = json_decode($errorMessages, true);

    // Check if decoding was successful and the array is not null
    if ($errorMessagesArray !== null && isset($errorMessagesArray['reconciliation_amount'])) {
        // Remove duplicate error messages
        $uniqueErrorMessages = array_unique($errorMessagesArray['reconciliation_amount']);
        foreach ($uniqueErrorMessages as $errorMessage) :
?>
            <div class="info mb-2 alert alert-danger">
                <?php echo $errorMessage; ?>
            </div>
<?php
        endforeach;
    }else{ ?>
        <div class="info mb-2 alert alert-danger">
                <?php echo $errorMessages; ?>
            </div>
  <?php  }
endif;
?>




    <div class="reconciliation-data margin-top-25">
        <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'viewData' => array('itemcount' => $dataProvider->getTotalItemCount()),
            //'itemView'=>'_newview',
            'itemView' => '_reconnewview', 'template' => '<div class="clearfix " id="table-wrapper"><div class="pull-right">{summary}</div><div class="pull-left">{sorter}</div></div><div id="parent"><table cellpadding="10" class="table total-table   list-view sorter" id="fixTable">{items}</table></div>',
            'enableSorting' => true,
            'sortableAttributes' => array(
                'reconciliation_id' => 'ID',
                'reconciliation_paymentdate' => 'Payment Date',
                'created_date' => 'Created Date',
                'reconciliation_chequeno' => 'Cheque Number',
            ),
            'afterAjaxUpdate' => 'function() {
            $(".reconciliation_date").datepicker({
                dateFormat: "dd-mm-yy"
            });
            }',
        )); ?>

        <!-- Add User Popup -->

    </div>
    <?php
    if ($dataProvider->getTotalItemCount() != 0) {
    ?>
        <div class="add-btn pull-right">
            <button type="button" class="button btn btn-primary" id="submitdate">Save</button>
        </div>
    <?php } ?>
</div>
<script>
    $(document).ready(function() {

        $(".changeStatus").click(function(){

            var id = $(this).attr('data-id');
            var table = $(this).attr('data-table');
            var answer = confirm("Are you sure you want to change the status?");
            if (answer) {
                $.ajax({
                    type: "POST",
                    data: {
                        id: id,
                        table: table,                        
                    },
                    url: "<?php echo $this->createUrl('bank/changestatus') ?>",
                    success: function(response) {                        
                       $('#yw0')[0].reset();
                        location.reload();                        
                    }
                });
            }           
        })
        setTimeout(function() {
            $('.info').fadeOut('slow');
        }, 3000);
        // register jQuery extension
        $('.date-entry').mask("99-99-9999", {
            placeholder: 'DD-MM-YYYY'
        });
        $("#fixTable").tableHeadFixer({
            'head': true
        });
        $(document).ajaxComplete(function() {
            $('.date-entry').mask("99-99-9999", {
                placeholder: 'DD-MM-YYYY'
            });
            $("#fixTable").tableHeadFixer({
                'head': true
            });
        });
        $(document).on('keypress', '.date-entry', function(e) {
            if (e.which == 13) {
                e.preventDefault();
                // Get all focusable elements on the page
                var $canfocus = $(':focusable');
                var index = $canfocus.index(this) + 1;
                if (index >= $canfocus.length) index = 0;
                $canfocus.eq(index).focus();
            }
        });
        
        $( ".reconciliation_date" ).datepicker({
            dateFormat: 'dd-mm-yy'
        });

        $('.container').on('click', '#submitdate', function() {
            var recDate = new Array();
            var recId = new Array();
            var nextRow = new Array();
            var recCreds = new Array();
            var newCreds = new Array();
            var recAction = new Array();
            var itemCount = $("#txtCount").val();
            var j = 0;
            var maxCount = itemCount;
            for (var i = 0; i < maxCount; i++) {
                if ($("#txtDate" + i).val() != "") {
                    recDate[i] = $("#txtDate" + i).val();
                    recId[i] = $("#txtDate" + i).attr("data-id");
                    nextRow[i] = parseInt(recId[i]) + 1;
                    recCreds[i] = $("#creds" + recId[i]).val();
                    newCreds[i] = recCreds[i].split(",");
                    recAction[i] = newCreds[i][2];
                    j = j + 1;
                }
            }
            if (j > 0) {
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    type: "POST",
                    data: {
                        recDate: recDate,
                        recCreds: recCreds,
                        nums: j
                    },
                    url: "<?php echo $this->createUrl('bank/reconciliationentry') ?>",
                    success: function(data) {
                        $('#yw0')[0].reset();
                        location.reload();
                        $("#txtDate0").focus();
                    }
                });
            } else {
                $("#txtDate0").focus();
            }
        });
        $(document).on("keypress", '.date-entry', function(e) {

        });
    });
    $(document).ajaxComplete(function() {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
</script>





<style>
    #parent {
        max-height: 512px;
    }

    table {
        margin-bottom: 0px;
    }

    table thead th {
        background-color: #eee;
    }
</style>