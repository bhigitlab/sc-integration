<!-- views/quotationitem/_quotationItemRate.php -->
 <?php 
 $serialNumber = $widget->dataProvider->pagination->currentPage * $widget->dataProvider->pagination->pageSize + $index + 1;

  ?>
<tr>
    <td><?php echo $serialNumber++; ?></td>
    <td>
        <?php 
        $category = QuotationCategoryMaster::model()->findByPk($data['master_category_id']);
        echo $category['name'];
        ?>
    </td>
    <td>
        <?php 
        $spec = Specification::model()->findByPk($data['material_id']);
        $cat = PurchaseCategory::model()->findByPk($spec['cat_id']);
        $brand = Brand::model()->findByPk($spec['brand_id']);
        echo $cat['category_name'].'-'.$brand['brand_name'].'-'.$spec['specification'];
        ?>
    </td>
    <td class="text-right">
        <?php echo Controller::money_format_inr($data['rate'], 2); ?>
    </td>
    <td class="text-right">
        <?php echo CHtml::link('Edit', Yii::app()->createUrl('quotationitem/quotationItemMaster/rateindex&id='.$data['id']), array('class' => 'btn btn-sm btn-default')); ?> 
        <?php echo CHtml::link('Delete', '#', array('class' => 'btn btn-sm btn-default btn_ratedelete', 'id' => $data['id'])); ?>
    </td>
</tr>
<?php $row++?>
