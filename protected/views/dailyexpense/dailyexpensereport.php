<?php setlocale(LC_MONETARY, 'en_IN');
if (!empty($model->employee_id)) {
    $paid_to = $model->employee_id;
} else {
    $paid_to = '';
}
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<div class="container" id="project">
    <div id="financialreport1">
        <div class="expenses-heading">
            <div class="clearfix">
                <button type="button" class="btn btn-info pull-right mt-0 mb-10">
                    <a href="<?php echo Yii::app()->createUrl('dailyexpense/savetoexceldailyexpense', array(
                        'fromdate' => $model->fromdate,
                        'todate' => $model->todate,
                        'company_id' => $model->company_id,
                        'exp_type' => $model->exp_type,
                        'dailyexpense_purchase_type' => $model->dailyexpense_purchase_type,
                        'expense_type' => $model->expense_type,
                        'expHead' => $expHead
                    )); ?>" style="text-decoration: none; color: white;">
                        <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>
                    </a>
                </button>
                <button type="button" class="btn btn-info pull-right mt-0 mb-10 margin-right-5">
                    <a href="<?php echo Yii::app()->createUrl('dailyexpense/savetopdfdailyexpense', array(
                        'fromdate' => $model->fromdate,
                        'todate' => $model->todate,
                        'company_id' => $model->company_id,
                        'exp_type' => $model->exp_type,
                        'dailyexpense_purchase_type' => $model->dailyexpense_purchase_type,
                        'expense_type' => $model->expense_type,
                        'expHead' => $expHead
                    )); ?>" style="text-decoration: none; color: white;">
                        <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
                    </a>
                </button>
                <h2 class="pull-left">DAILY EXPENSE REPORT</h2>
            </div>
        </div>


        <div class="page_filter filtercls clearfix ">
            <form name="dailyexpense-search" id="dailyexpense-search" method="post"
                action="<?php echo Yii::app()->createUrl('dailyexpense/Dailyexpensereport'); ?>">
                <div class="row">
                    <div class="form-group  col-xs-12 col-sm-3 col-md-2 currentmonth">
                        <label>Comapny </label>
                        <?php
                        $user = Users::model()->findByPk(Yii::app()->user->id);
                        $arrVal = explode(',', $user->company_id);
                        $newQuery = "";
                        $newQuery1 = "";
                        foreach ($arrVal as $arr) {
                            if ($newQuery)
                                $newQuery .= ' OR';
                            if ($newQuery1)
                                $newQuery1 .= ' OR';
                            $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
                            $newQuery1 .= " FIND_IN_SET('" . $arr . "', company_id)";

                        }
                        echo CHtml::dropDownList('Dailyexpense[company_id]', 'company_id', CHtml::listData(Company::model()->findAll(array(
                            'select' => array('id, name'),
                            'order' => 'name',
                            'condition' => '(' . $newQuery . ')',
                            'distinct' => true
                        )), 'id', 'name'), array('empty' => 'Select Company', 'class' => 'form-control', 'id' => 'company_id', 'options' => array($model->company_id => array('selected' => true))));
                        ?>
                    </div>
                    <div class="form-group  col-xs-12 col-sm-3 col-md-2 currentyear">
                        <label  >From</label>
                        <input type="text" name="Dailyexpense[date_from]" id="Dailyexpense_date_from"
                            class="form-control" value="<?php echo date("d-m-Y", strtotime($model->fromdate)); ?>"
                            placeholder="From" autocomplete="off" style="display:inline-block">
                    </div>
                    <div class="form-group  col-xs-12 col-sm-3 col-md-2 currentmonth">
                        <label  >To</label>
                        <input type="text" name="Dailyexpense[date_to]" id="Dailyexpense_date_to" class="form-control"
                            value="<?php echo date("d-m-Y", strtotime($model->todate)); ?>" placeholder="To"
                            autocomplete="off" style="display:inline-block">
                    </div>

                    <div class="form-group  col-xs-12 col-sm-3 col-md-2 currentmonth">
                        <label>Expense Type</label>
                        <?php
                        echo CHtml::dropDownList('Dailyexpense[exp_type]', 'exp_type', CHtml::listData(Status::model()->findAll(array(
                            'select' => array('sid,caption'),
                            'condition' => 'status_type="expense"',
                            'order' => 'caption',
                            'distinct' => true
                        )), 'sid', 'caption'), array('empty' => 'Choose a type', 'class' => 'form-control', 'options' => array($model->exp_type => array('selected' => true))));
                        ?>
                    </div>
                    <div class="form-group  col-xs-12 col-sm-3 col-md-2 currentmonth">
                        <label>Purchase Type</label>
                        <select name="Dailyexpense[purchase_type]" id="dailyexpense_purchase_type" class="form-control">
                            <option value="">Choose purchase type</option>
                            <option value="1" <?php if ($model->dailyexpense_purchase_type == 1) {
                                echo "selected";
                            } ?>>
                                Credit</option>
                            <option value="2" <?php if ($model->dailyexpense_purchase_type == 2) {
                                echo "selected";
                            } ?>>
                                Full Paid</option>
                            <option value="3" <?php if ($model->dailyexpense_purchase_type == 3) {
                                echo "selected";
                            } ?>>
                                Partially Paid</option>
                        </select>
                    </div>
                    <?php
                    if ($model->exp_type == 72)
                        $expense_type = $model->dailyexpense_receipt_type;
                    else if ($model->exp_type == 73)
                        $expense_type = $model->expense_type;
                    else
                        $expense_type = "";
                    ?>

                    <div class="form-group  col-xs-12 col-sm-3 col-md-2 currentmonth">
                        <label>Payment Type</label>
                        <?php
                        echo CHtml::dropDownList('Dailyexpense[expense_type]', 'expense_type', CHtml::listData(Status::model()->findAll(array(
                            'select' => array('sid,caption'),
                            'condition' => 'status_type="payment_type"',
                            'order' => 'caption',
                            'distinct' => true
                        )), 'sid', 'caption'), array('empty' => 'Choose a payment type', 'class' => 'form-control', 'options' => array($expense_type => array('selected' => true))));
                        ?>
                    </div>
                    <div class="form-group  col-xs-12 col-sm-3 col-md-2 currentmonth">
                        <label>Transaction Type</label>
                        <select class="form-control" name="Dailyexpense[expensehead_type]"
                            id="Dailyexpense_expensehead_type">
                            <option value="">Choose Transaction Type</option>
                            <option value="1" <?php echo ($model->expensehead_type == 1) ? "selected" : ""; ?>>Expense
                            </option>
                            <option value="2" <?php echo ($model->expensehead_type == 2) ? "selected" : ""; ?>>Receipt
                            </option>
                            <option value="3" <?php echo ($model->expensehead_type == 3) ? "selected" : ""; ?>>Deposit
                            </option>
                            <option value="4" <?php echo ($model->expensehead_type == 4) ? "selected" : ""; ?>>Withdrawal
                                From
                                Bank</option>
                            <option value="5" <?php echo ($model->expensehead_type == 5) ? "selected" : ""; ?>>Deposit to
                                Bank
                            </option>
                        </select>
                    </div>

                    <?php
                    $tblpx = Yii::app()->db->tablePrefix;
                    $cExpType = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "company_expense_type WHERE type IN(2,1) ORDER BY name ASC")->queryAll();
                    $cReceiptType = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "company_expense_type WHERE type IN(0,2) ORDER BY name ASC")->queryAll();
                    $deposit = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "deposit ORDER BY deposit_name ASC")->queryAll();
                    $expensetype_name1 = '';
                    $expensetype_name2 = '';
                    if ($expensehead1 != '' && $expensehead2 != '') {
                        if ($expensehead1 != '') {
                            $expensetype = Companyexpensetype::model()->findByPK($expensehead1);
                            $expensetype_name1 = $expensetype->name;
                        } else {
                            $expensetype = Companyexpensetype::model()->findByPK($expensehead2);
                            $expensetype_name2 = $expensetype->name;
                        }
                    }
                    ?>
                    <div class="form-group  col-xs-12 col-sm-3 col-md-2 currentmonth">
                        <label>Transaction Head</label>
                        <select class="form-control js-example-basic-single" name="Dailyexpense[expensehead_id]"
                            id="Dailyexpense_expensehead_id">
                            <option value="">-Select Transaction Head-</option>
                            <optgroup label="Expense Head">
                                <?php
                                foreach ($cExpType as $company) { ?>
                                    <option data-id="1" value="1,<?php echo $company["company_exp_id"]; ?>" <?php echo (($company["company_exp_id"] == $expensehead1) && $type == 1) ? 'selected' : "" ?>>
                                        <?php echo $company["name"]; ?>
                                    </option>
                                <?php } ?>
                            </optgroup>
                            <optgroup label="Receipt Head">
                                <?php
                                foreach ($cReceiptType as $receipt) { ?>
                                    <option data-id="2" value="2,<?php echo $receipt["company_exp_id"]; ?>" <?php echo (($receipt["company_exp_id"] == $expensehead2) && $type == 2) ? 'selected' : "" ?>>
                                        <?php echo $receipt["name"]; ?>
                                    </option>
                                <?php } ?>
                            </optgroup>
                            <optgroup label="Deposit">
                                <?php
                                foreach ($deposit as $deposits) { ?>
                                    <option data-id="3" value="3,<?php echo $deposits["deposit_id"]; ?>" <?php echo (($deposits["deposit_id"] == $expensehead1) && $type == 3) ? 'selected' : "" ?>>
                                        <?php echo $deposits["deposit_name"]; ?>
                                    </option>
                                <?php } ?>
                            </optgroup>
                        </select>
                    </div>
                    <div class="form-group  col-xs-12 col-sm-3 col-md-2 currentmonth">
                        <label>Paid To</label>
                        <?php
                        echo CHtml::dropDownList('Dailyexpense[paid_to]', 'paid_to', CHtml::listData(Users::model()->findAll(array(
                            'select' => array('userid, concat_ws(" ",first_name,last_name) as first_name'),
                            'condition' => '(' . $newQuery1 . ') AND user_type NOT IN (1)',
                            'order' => 'first_name ASC',
                            'distinct' => true
                        )), 'userid', 'first_name'), array('empty' => 'Select Paid To', 'class' => 'form-control', 'options' => array($paid_to => array('selected' => true))));
                        ?>
                    </div>

                    <div class="form-group col-xs-12 col-sm-9 col-md-6  text-right">
                        <label>&nbsp;</label>
                        <div>
                            <?php echo CHtml::submitButton('Go', array('buttonType' => 'submit', 'name' => 'monthlyreport', 'class' => 'btn btn-sm btn-primary')); ?>
                            <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('Dailyexpense/Dailyexpensereport') . '"', 'class' => 'btn btn-sm btn-default')); ?>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div id="parent">
            <?php $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'viewData' => array('paidsum' => $model->getSum($model->newsearch(), 'dailyexpense_paidamount'), 'receiptsum' => $model->getSum($model->newsearch(), 'dailyexpense_receipt')),
                'itemView' => '_newdailyexpensereportview',
                'template' => '<div class="sub-heading">{sorter}{summary}</div><div id="parent"><table cellpadding="10" class="table  list-view sorter" id="fixTable">{items}</table></div>',
                'enableSorting' => true,
                'sortableAttributes' => array(
                    'Expense' => 'Expense',
                    'Receipt' => 'Receipt',
                    'Type' => 'Type',
                ),
            )); ?>
        </div>
    </div>
</div>
<style type="text/css">
    #parent {
        max-height: 300px;
    }
</style>

<script>
    $(document).ready(function () {
        $("#fixTable").tableHeadFixer({ 'left': false, 'foot': true, 'head': true });
        $(document).ajaxComplete(function () {
            $("#fixTable").tableHeadFixer({ 'left': false, 'foot': true, 'head': true });
        });
        $(function () {
            $("#Dailyexpense_date_from").datepicker({ dateFormat: 'dd-mm-yy' });
        });
        $(function () {
            $("#Dailyexpense_date_to").datepicker({ dateFormat: 'dd-mm-yy' });
        });
    });
</script>