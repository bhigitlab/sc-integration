

<?php
/* @var $this WorkTypeController */
/* @var $model WorkType */
/* @var $form CActiveForm */
?>

<div class="">
    <?php
       $form = $this->beginWidget('CActiveForm', array(
        'id'=>'work-type-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>

    <!-- Popup content-->



    <div class="panel-body">

            <div class="row">

                <div class="col-md-6">
                    <div class="form-group">
                   <?php echo $form->labelEx($model,'work_type'); ?>
                    <?php echo $form->textField($model,'work_type',array('class'=>'form-control','size'=>30,'maxlength'=>30)); ?>
                    <?php echo $form->error($model,'work_type'); ?>
                    </div>
                    </div>
                <div class="col-md-6">
                    <div class="save-btnHold">
                        <label style="display:block;">&nbsp;</label>
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-info btn-sm')); ?>
                        <?php echo CHtml::ResetButton('Reset', array('class'=>'btn btn-default btn-sm')); ?>
                        <?php echo CHtml::ResetButton('Close', array('onclick'=>'closeaction(this,event);','class'=>'btn btn-sm'));  ?>

                    </div>
                </div>
            </div>
        </div>
<?php $this->endWidget(); ?>
</div><!-- form -->
<script>
    function countOfLetters(str) {
        var letter = 0;
        
        for (i = 0; i < str.length; i++) {        
            if ((str[i] >= 'A' && str[i] <= 'Z')
                    || (str[i] >= 'a' && str[i] <= 'z'))
                letter++;
        }        
        return letter;
    }
      
    $("#WorkType_work_type").keyup(function () {              
        var alphabetCount =  countOfLetters(this.value);  
        var message ="";      
        var disabled=false;
        if(alphabetCount < 1){
            var message = "Invalid Work Type";
            var disabled=true;                
        }       
        $(this).siblings(".errorMessage").show().html(message).addClass('d-block');    
        $(".btn-info").attr('disabled',disabled);
              
    });
</script>
    
    
