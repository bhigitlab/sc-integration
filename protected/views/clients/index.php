)
<?php
/* @var $this ClientsController */
/* @var $model Clients */

$this->breadcrumbs = array(
    'Clients' => array('index'),
    'Index',
);

$this->menu = array(
    array('label' => 'List Clients', 'url' => array('index')),
    array('label' => 'Create Clients', 'url' => array('create')),
    array('label' => 'Manage Clients', 'url' => array('admin')),
    array('label' => 'Manage Project types', 'url' => array('/projectType/admin')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('clients-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div style="float:left;"><h2>List Clients</h2></div>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'clients-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'Sl.No.',),
        array('name'=>'cid', 'htmlOptions' => array('width' => '40px','style'=>'font-weight: bold;text-align:center')),       
          array(
        'name'  => 'name',
        'value' => 'CHtml::link($data->name, Yii::app()
 ->createUrl("clients/view",array("id"=>$data->cid)))',
        'type'  => 'raw',
    ),
        array(
            'name' => 'project_type',
            'value' => '$data->projectType->project_type',
            'type' => 'raw',
            'filter' => CHtml::listData(ProjectType::model()->findAll(
                            array(
                                'select' => array('ptid,project_type'),
                                'order' => 'project_type',
                                'distinct' => true
                    )), "ptid", "project_type")
        ),
        'description',
        array(
            'name' => 'status',
            'value' => '$data->status0->caption',
            'type' => 'raw',
            'filter' => CHtml::listData(Status::model()->findAll(
                            array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="active_status"',
                                'order' => 'status_type',
                                'distinct' => true
                    )), "sid", "caption")
        ),
        'created_date',
              array(
        'class'=>'CButtonColumn',
         'header'=>'Toggle Details',
          'template'=>'{view}',
  
          ),
    ),
));
?>
