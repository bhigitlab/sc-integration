<?php

class ImageGalleryController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view','upload','Getalbumname','image','deleteimage','getalbumbyprojectid','viewimages','getalbum'),
                'users' => array('@'), 'expression' => 'yii::app()->user->role<=1',
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'), 'expression' => 'yii::app()->user->role<=1',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {

        $model = new ImageGallery;
        $this->performAjaxValidation($model);
        if (isset($_POST['ImageGallery'])) {
            $uploadedFile = CUploadedFile::getInstance($model, 'image');
            $newName = date('Y-m-d H:i:s') . "_" . $uploadedFile;
            if (!empty($uploadedFile)) {
                $uploadedFile->saveAs(Yii::getPathOfAlias('webroot') . '/uploads/image/' . $newName);
                $model->attributes = $_POST['ImageGallery'];
                $model->image = $newName;
                $model->created_date = date('Y-m-d');
                $model->created_by = Yii::app()->user->id;
                $model->updated_date = date('Y-m-d');
                $model->updated_by = Yii::app()->user->id;
                if ($model->save()) {
                    $this->redirect(array('admin'));
                }
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $prevIMage = $model->image;
        if (isset($_POST['ImageGallery'])) {
            $model->attributes = $_POST['ImageGallery'];
            $uploadedFile = CUploadedFile::getInstance($model, 'image');
            $newName = date('Y-m-d H:i:s') . "_" . $uploadedFile;
            if (!empty($uploadedFile)) {
                if (file_exists(Yii::getPathOfAlias('webroot') . '/uploads/image/' . $prevIMage)) {
                    unlink(Yii::getPathOfAlias('webroot') . '/uploads/image/' . $prevIMage);
                }
                $uploadedFile->saveAs(Yii::getPathOfAlias('webroot') . '/uploads/image/' . $newName);
                $model->image = $newName;
            } else {
                $model->image = $prevIMage;
            }
            $model->updated_date = date('Y-m-d');
            $model->updated_by = Yii::app()->user->id;
            if ($model->save()) {
                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
     public function actionDelete($id,$albumid) {
		
		$model = $this->loadModel($id);
		$imagename = $model['image'];
		$tblpx = Yii::app()->db->tablePrefix;
		$this->loadModel($id)->delete();
		 
		$output_dir1 = Yii::getPathOfAlias('webroot') . '/uploads/image/thumbnail/';
		$output_dir2 = Yii::getPathOfAlias('webroot') . '/uploads/image/';
		
		$filePath1 = $output_dir1. $imagename;
		$filePath2 = $output_dir2. $imagename;
		if (file_exists($filePath1)) 
		{
			unlink($filePath1);
		}
		if (file_exists($filePath2)) 
		{
			unlink($filePath2);
		}
		
		$query = Yii::app()->db->createCommand()
                ->select(['*'])
                ->from($tblpx.'image_gallery im')
                ->where('im.albumid='.$albumid)
                ->queryAll();
        if($query == null)
        {
			echo json_encode(array('response' => 'error'));
			
		}
		else
		{ 
        echo json_encode(array('response' => 'success'));
		}

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
       /* if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('viewimage')); */
    }
       
       
    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('ImageGallery');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new ImageGallery('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ImageGallery']))
            $model->attributes = $_GET['ImageGallery'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ImageGallery the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = ImageGallery::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param ImageGallery $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'image-gallery-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    /*------------------Mod by Rajisha--------------------*/
    public function actionUpload(){
        $final_width_of_image = 100;
        //$path_to_image_directory = 'images/fullsized/';
        $path_to_thumbs_directory = Yii::getPathOfAlias('webroot') . '/uploads/image/thumbnail/';
        $path_to_image_directory = Yii::getPathOfAlias('webroot') . '/uploads/image/';
        if(isset($_FILES["myfile"]))
        {
                $ret = array();

        //	This is for custom errors;	
        /*	$custom_error= array();
                $custom_error['jquery-upload-file-error']="File already exists";
                echo json_encode($custom_error);
                die();
        */
                $error =$_FILES["myfile"]["error"];
                //You need to handle  both cases
                //If Any browser does not support serializing of multiple files using FormData() 
                if(!is_array($_FILES["myfile"]["name"])) //single file
                {
                        $fileName = $_FILES["myfile"]["name"];
                        move_uploaded_file($_FILES["myfile"]["tmp_name"],$path_to_image_directory.str_replace(' ','_',date('Y-m-d H:i:s')) . "_" .$fileName);
                        $this->createThumbnail(str_replace(' ','_',date('Y-m-d H:i:s')) . "_" .$fileName); 
                        $ret[]= str_replace(' ','_',date('Y-m-d H:i:s')) . "_" .$fileName;
                }
                else  //Multiple files, file[]
                {
                  $fileCount = count($_FILES["myfile"]["name"]);
                  for($i=0; $i < $fileCount; $i++)
                  {
                        $fileName = $_FILES["myfile"]["name"][$i];
                        move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$path_to_image_directory.str_replace(' ','_',date('Y-m-d H:i:s')) . "_" .$fileName);
                         $this->createThumbnail(str_replace(' ','_',date('Y-m-d H:i:s')) . "_" .$fileName); 
                        $ret[]= str_replace(' ','_',date('Y-m-d H:i:s')) . "_" .$fileName;
                  }

                }
            echo json_encode($ret);
         }
         
    }


    
    
    public function actionDeleteimage(){
        $output_dir = Yii::getPathOfAlias('webroot') . '/uploads/image/';
        if(isset($_POST["op"]) && $_POST["op"] == "delete" && isset($_POST['name']))
        {
                $fileName =$_POST['name'];
                $fileName=str_replace("..",".",$fileName); //required. if somebody is trying parent folder files	
                $filePath = $output_dir. $fileName;
                if (file_exists($filePath)) 
                {
                    unlink($filePath);
                }
                echo $fileName;
        }
    }
    public function actionImage(){//$this->layout = false;
        $model = new ImageGallery;
        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($_POST['ImageGallery'])) {
             if($_POST['ImageGallery']['albumid']!=0){
            if(isset($_POST['hidfiles'])){
                $files = $_POST['hidfiles'];
                $explode = explode(',', $files);
                foreach($explode as $value){
                    $project = ($_POST['ImageGallery']['projectid'] != 0 || !empty($_POST['ImageGallery']['projectid'])) ? $_POST['ImageGallery']['projectid'] : 'NULL';
                    $album   = ($_POST['ImageGallery']['albumid'] != 0 || !empty($_POST['ImageGallery']['albumid'])) ? $_POST['ImageGallery']['albumid'] : 'NULL';
                    $query[] = '(null, ' . $project . ', ' . $album . ',"' . $value . '","'.Yii::app()->user->id.'","'.date('Y-m-d').'","'.Yii::app()->user->id.'","'.date('Y-m-d').'")';
                }
               $insert = 'insert into '.$tblpx.'image_gallery values ' . implode(',', $query);
                Yii::app()->db->createCommand($insert)->query();
            }
            
            //if ($model->save()) {
                $this->redirect(array('admin'));
           // }
             }
        }
        $this->render('image',array('model' => $model));
    }
    public function actionGetalbumbyprojectid(){
        $id = $_REQUEST['id'];
//        if(isset($_REQUEST['name']))
//        {
//        $name=$_REQUEST['name'];
//        }
        $tblpx = Yii::app()->db->tablePrefix;
        $query = Yii::app()->db->createCommand("SELECT album_id,title FROM {$tblpx}albums WHERE projectid = '".$id."'  ORDER BY title")->queryAll();
        $res = CHtml::listData($query, 'album_id', 'title');
        
       
        $albums = "<option value=''>Select Albums</option><option value='0'>Create New Album</option>";
        //$selected = '';
// each $row is an array representing a row of data
           foreach ($res as $value => $resv){
//               if($resv == $name){
//                   $selected = 'selected';
//               }
               $albums .= CHtml::tag('option', array('value' => $value), CHtml::encode($resv), true);
           }
           echo CJSON::encode(array('albums' => $albums,));
//      
        
    }
    /*------------------Mod by Rajisha--------------------*/
    
    
    
     
    /*----------------------Ranjitha--------------------*/
    public function createThumbnail($filename) {
     
        $final_width_of_image = 100;
        //$path_to_image_directory = 'images/fullsized/';
        $path_to_thumbs_directory = Yii::getPathOfAlias('webroot') . '/uploads/image/thumbnail/';
        $path_to_image_directory = Yii::getPathOfAlias('webroot') . '/uploads/image/';
     
    if(preg_match('/[.](jpg)$/', $filename)) {
        $im = imagecreatefromjpeg($path_to_image_directory . $filename);
    } else if (preg_match('/[.](gif)$/', $filename)) {
        $im = imagecreatefromgif($path_to_image_directory . $filename);
    } else if (preg_match('/[.](png)$/', $filename)) {
        $im = imagecreatefrompng($path_to_image_directory . $filename);
    }
     
    $ox = imagesx($im);
    $oy = imagesy($im);
     
    $nx = $final_width_of_image;
    $ny = floor($oy * ($final_width_of_image / $ox));
     
    $nm = imagecreatetruecolor($nx, $ny);
     
    imagecopyresized($nm, $im, 0,0,0,0,$nx,$ny,$ox,$oy);
     
    if(!file_exists($path_to_thumbs_directory)) {
      if(!mkdir($path_to_thumbs_directory)) {
           die("There was a problem. Please try again!");
      } 
       }
 
    imagejpeg($nm, $path_to_thumbs_directory . $filename);
//    $tn = '<img src="' . $path_to_thumbs_directory . $filename . '" alt="image" />';
//    $tn .= '<br />Congratulations. Your file has been successfully uploaded, and a      thumbnail has been created.';
//    echo $tn;
    return ;
}
/*----------------------Ranjitha--------------------*/




 /*------------------Mod by Sikha--------------------*/
	   public function actionViewimages($albumid) {
		$tblpx = Yii::app()->db->tablePrefix;
		$model = Yii::app()->db->createCommand()
                ->select(['im.id','im.image','al.title','al.album_id','al.description','p.name'])
                ->from($tblpx.'image_gallery im')
                ->join($tblpx.'albums al', 'al.album_id=im.albumid')
                ->join($tblpx.'projects p', 'p.pid=im.projectid')
                ->where('im.albumid='.$albumid)
                ->queryAll();

        $this->render('viewimage', array(
            'model' => $model,
        ));
    }
    
     public function actionGetalbum(){
		 
        $id = $_REQUEST['id'];
        $albums = CHtml::listData(Albums::model()->findAll(array(
                            'select' => array('album_id, title'),
                            "condition"=> 'projectid='.$id,
                            'order' => 'title',
                            'distinct' => true
                        )), 'album_id', 'title');
                        
				if(count($albums)){
									$albumlist= '<option value="">Select Album</option>';
									foreach($albums as $albumid=>$title){
									   $albumlist.='<option value="'.$albumid.'">'.$title.'</option>';
									}
								}
				else{
				  $albumlist= '<option value="">No Album is created</option>';
				}
		$albumarray = array('albums'=>$albumlist);
		echo json_encode($albumarray);
    
        
    }

 /*------------------Mod by Sikha--------------------*/

public function actionGetalbumname(){
       $id = $_REQUEST['id'];
        if(isset($_REQUEST['name']))
        {
        $name=$_REQUEST['name'];
        }
        $tblpx = Yii::app()->db->tablePrefix;
        $query = Yii::app()->db->createCommand("SELECT album_id,title FROM {$tblpx}albums WHERE projectid = '".$id."'  ORDER BY title")->queryAll();
        $res = CHtml::listData($query, 'album_id', 'title');
        $albums = "<option value=''>Select Albums</option><option value='0'>Create New Album</option>";
      
        $selected = '';
// each $row is an array representing a row of data
           foreach ($res as $value => $resv){
               if($resv == $name){
                   $selected = 'selected';
               }
               $albums .= CHtml::tag('option', array('value' => $value,'selected' => $selected), CHtml::encode($resv), true);
           }
           echo CJSON::encode(array('albums' => $albums));
//      
        
//      
        
    }
}
