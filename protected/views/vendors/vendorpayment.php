<?php
if ($company_id != NULL) {
    $company = Company::model()->findbyPk($company_id);
    $company_name = ' - ' . $company->name;
} else {
    $company_name = "";
}
?>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/tablestyles.css" />
<link rel="stylesheet" type="text/css" href="uploads/new_template/css/newstyle.css" />
<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }

    /*.container{max-width: 1050px;margin:0 auto;padding: 15px;border: 1px solid #ddd;}*/
    .text-right {
        text-align: right
    }

    .details,
    .info-table {
        border: 1px solid #212121;
        margin-top: 20px;
    }

    .details td,
    .info-table td,
    .info-table th {
        padding: 6px 8px;
    }

    .info-table {
        margin-bottom: 10px;
    }

    .text-center {
        text-align: center;
    }

    .img-hold {
        width: 10%;
    }

    .companyhead {
        font-size: 18px;
        font-weight: bold;
        margin-top: 0px;
        color: #789AD1;
    }
</style>
<?php
$company_address = Company::model()->findBypk(($company_id) ? $company_id : Yii::app()->user->company_id);
?>
<table border=0>
    <tbody>
        <tr>
            <td class=text-right>GST NO: <?php echo isset($company_address["company_gstnum"]) ? $company_address["company_gstnum"] : ''; ?></td>
        </tr>
    </tbody>
</table>
<table border=0>
    <tbody>
        <tr>
            <td class="img-hold"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" alt="img" style='margin-top:-45px;' /></td>
            <td class="text-center">
                <p class='companyhead'><?php echo isset($company_address['name']) ? $company_address['name'] : ''; ?></p><br>
                <p><?php echo ((isset($company_address['address']) && !empty($company_address['address'])) ? ($company_address['address']) : ""); ?><br>
                    PIN : <?php echo ((isset($company_address['pincode']) && !empty($company_address['pincode'])) ? ($company_address['pincode']) : ""); ?>,&nbsp;
                    PHONE : <?php echo ((isset($company_address['phone']) && !empty($company_address['phone'])) ? ($company_address['phone']) : ""); ?><br>
                    EMAIL : <?php echo ((isset($company_address['email_id']) && !empty($company_address['email_id'])) ? ($company_address['email_id']) : ""); ?></p><br>
                <h4>Vendor Payment Report</h4>
            </td>
        </tr>
    </tbody>
</table>
<div id="contenthtml">
    <div style="margin-left:30px;">
        <table class="table table-bordered" border="1">
            <tbody>
                <thead>
                    <tr>
                        <th>SL No</th>
                        <th>Date</th>
                        <th>Bill Number</th>
                        <th>Invoice amount / Vendor amount</th>
                        <th>Paid</th>
                        <th>TDS Amount</th>
                        <th>Paid To Vendor</th>
                        <th>Balance without TDS</th>
                    </tr>
                </thead>
            <tbody>
                <?php
                $totalAmount        = 0;
                $totalPaid          = 0;
                $totalTds           = 0;
                $totalPaidVendor    = 0;
                /**************************** */
                if (!empty($daybookPayment)) {
                    $totalAmount        = $totalAmount + $daybookPayment[count($daybookPayment) - 1]["sumtotalamount"];
                    $totalPaid          = $totalPaid + $daybookPayment[count($daybookPayment) - 1]["sumtotalpaid"];
                    $totalTds           = $totalTds + $daybookPayment[count($daybookPayment) - 1]["sumtotaltds"];
                    $totalPaidVendor    = $totalPaidVendor + $daybookPayment[count($daybookPayment) - 1]["sumtotalpaidtovendor"];
                }
                if (!empty($dailyvPayment)) {
                    // $totalAmount        = $totalAmount + $dailyvPayment[0]["sumtotalamount"];
                    $totalPaid          = $totalPaid + $dailyvPayment[0]["sumtotalamount"];
                    $totalTds           = $totalTds + $dailyvPayment[0]["sumtdsamount"];
                    $totalPaidVendor    = $totalPaidVendor + $dailyvPayment[0]["sumpaidamount"];
                }

                if (!empty($paymentData)) {
                    foreach ($paymentData as $payments) {
                        $totalAmount += $payments['total_amount'];
                    }
                }
                ?>
                <tr>
                    <th colspan="3" class="text-right"><b>Grand Total</b></th>
                    <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($totalAmount), 2); ?></b></th>
                    <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($totalPaid), 2); ?></b></th>
                    <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($totalTds), 2); ?></b></th>
                    <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($totalPaidVendor), 2); ?></b></th>
                    <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($totalAmount - $totalPaid), 2); ?></b></th>
                </tr>
                <?php
                /*************************** */
                $totalAmount        = 0;
                $totalPaid          = 0;
                $totalTds           = 0;
                $totalPaidVendor    = 0;
                if (!empty($daybookPayment)) {

                    $c  = 1;
                    foreach ($daybookPayment as $vendor) {
                        if ($c != $vendor["projectcount"]) {
                            $c = $c + 1;
                        } else {
                            $vendorCount[] = $vendor["projectcount"];
                            $c = 1;
                        }
                    }
                    $vendorCount    = array_values($vendorCount);
                    $vendorNum      = count($vendorCount);
                    $totalV = 0;
                    for ($i = 0; $i < $vendorNum; $i++) {
                        $vendorC    = $vendorCount[$i];
                        for ($j = 0; $j < $vendorC; $j++) {
                            $vCount = $daybookPayment[$j]["projectcount"];
                            if ($j == 0) {
                ?>
                                <tr>
                                    <td colspan="6" width="100%"><b><?php echo $daybookPayment[$totalV]["projectname"]; ?></b></td>
                                </tr>
                            <?php
                            }
                            ?>
                            <tr>
                                <td><?php echo $j + 1; ?></td>
                                <td><?php echo date("d-m-Y", strtotime($daybookPayment[$totalV]["date"])); ?></td>
                                <td><?php echo $daybookPayment[$totalV]["billnumber"] . " - " . $daybookPayment[$totalV]["description"]; ?></td>
                                <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($daybookPayment[$totalV]["amount"], 2); ?></td>
                                <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($daybookPayment[$totalV]["paid"], 2); ?></td>
                                <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($daybookPayment[$totalV]["expense_tds"], 2); ?></td>
                                <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($daybookPayment[$totalV]["paidamount"], 2); ?></td>
                                <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr(($daybookPayment[$totalV]["amount"] - $daybookPayment[$totalV]["paid"]), 2); ?></td>
                            </tr>
                            <?php
                            if ($vendorC == $j + 1) {
                            ?>
                                <tr>
                                    <th colspan="3" class="text-right"><b>Total</b></th>
                                    <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($daybookPayment[$totalV]["totalamount"]), 2); ?></b></th>
                                    <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($daybookPayment[$totalV]["totalpaid"]), 2); ?></b></th>
                                    <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($daybookPayment[$totalV]["totaltds"]), 2); ?></b></th>
                                    <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($daybookPayment[$totalV]["totalpaidtovendor"]), 2); ?></b></th>
                                    <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($daybookPayment[$totalV]["totalamount"] - $daybookPayment[$totalV]["totalpaid"]), 2); ?></b></th>
                                </tr>
                    <?php
                            }
                            $totalV = $totalV + 1;
                        }
                    }
                    ?>
                    <tr>
                        <th colspan="3" class="text-right"><b>Daybook Total</b></th>
                        <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($daybookPayment[$totalV - 1]["sumtotalamount"]), 2); ?></b></th>
                        <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($daybookPayment[$totalV - 1]["sumtotalpaid"]), 2); ?></b></th>
                        <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($daybookPayment[$totalV - 1]["sumtotaltds"]), 2); ?></b></th>
                        <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($daybookPayment[$totalV - 1]["sumtotalpaidtovendor"]), 2); ?></b></th>
                        <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($daybookPayment[$totalV - 1]["sumtotalamount"] - $daybookPayment[$totalV - 1]["sumtotalpaid"]), 2); ?></b></th>
                    </tr>
                <?php
                    $totalAmount        = $totalAmount + $daybookPayment[$totalV - 1]["sumtotalamount"];
                    $totalPaid          = $totalPaid + $daybookPayment[$totalV - 1]["sumtotalpaid"];
                    $totalTds           = $totalTds + $daybookPayment[$totalV - 1]["sumtotaltds"];
                    $totalPaidVendor    = $totalPaidVendor + $daybookPayment[$totalV - 1]["sumtotalpaidtovendor"];
                } ?>
                <?php if (!empty($dailyvPayment)) { ?>
                    <tr>
                        <th colspan="7"><b>Direct Vendor Payment</b></th>
                    </tr>
                    <?php
                    $k = 1;
                    foreach ($dailyvPayment as $dailyvendor) {
                    ?>
                        <tr>
                            <td><?php echo $k; ?></td>
                            <td><?php echo date("d-m-Y", strtotime($dailyvendor["date"])); ?></td>
                            <td><?php echo $dailyvendor["description"]; ?></td>
                            <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr(0, 2); ?></td>
                            <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($dailyvendor["amount"] + $dailyvendor["tax_amount"], 2); ?></td>
                            <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($dailyvendor["tds_amount"], 2); ?></td>
                            <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($dailyvendor["paidamount"], 2); ?></td>
                            <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr(0, 2); ?></td>
                        </tr>
                    <?php
                        $k = $k + 1;
                    } ?>
                    <tr>
                        <th colspan="3" class="text-right"><b>Total</b></th>
                        <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(0, 2); ?></b></th>
                        <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($dailyvPayment[0]["sumtotalamount"]), 2); ?></b></th>
                        <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($dailyvPayment[0]["sumtdsamount"]), 2); ?></b></th>
                        <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($dailyvPayment[0]["sumpaidamount"]), 2); ?></b></th>
                        <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round(0), 2); ?></b></th>
                    </tr>
                <?php
                    $totalAmount        = $totalAmount + $dailyvPayment[0]["sumtotalamount"];
                    $totalPaid          = $totalPaid + $dailyvPayment[0]["sumtotalamount"];
                    $totalTds           = $totalTds + $dailyvPayment[0]["sumtdsamount"];
                    $totalPaidVendor    = $totalPaidVendor + $dailyvPayment[0]["sumpaidamount"];
                } ?>
                <?php if (!empty($paymentData)) {
                ?>
                    <tr>
                        <th colspan="8"><b>Pending Bills</b></th>
                    </tr>
                    <?php
                    foreach ($paymentData as $payments) {
                        $bills = $payments['datas'];
                    ?>
                        <tr>
                            <td colspan="8" width="100%"><b><?php echo $payments['project_name']; ?></b></td>
                        </tr>
                        <?php
                        foreach ($bills as $bill) {
                        ?>
                            <tr>
                                <td><?php echo '1'; ?></td>
                                <td><?php echo date("d-m-Y", strtotime($bill["bill_date"])); ?></td>
                                <td><?php echo $bill["bill_number"]; ?></td>
                                <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($bill["bill_totalamount"], 2); ?></td>
                                <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr(0, 2); ?></td>
                                <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr(0, 2); ?></td>
                                <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr(0, 2); ?></td>
                                <td class="text-right"><?php echo Yii::app()->Controller->money_format_inr($bill["bill_totalamount"], 2); ?></td>
                            </tr>
                        <?php
                        }
                        ?>
                        <tr>
                            <th colspan="3" class="text-right"><b>Total</b></th>
                            <th class="text-right total_1"><b><?php echo Yii::app()->Controller->money_format_inr($payments['total_amount'], 2); ?></b></th>
                            <th class="text-right total_2"><b><?php echo Yii::app()->Controller->money_format_inr(0, 2); ?></b></th>
                            <th class="text-right total_3"><b><?php echo Yii::app()->Controller->money_format_inr(0, 2); ?></b></th>
                            <th class="text-right total_4"><b><?php echo Yii::app()->Controller->money_format_inr(0, 2); ?></b></th>
                            <th class="text-right total_5"><b><?php echo Yii::app()->Controller->money_format_inr(($payments['total_amount']), 2); ?></b></th>
                        </tr>
                <?php
                    }
                } ?>
            </tbody>
            <!-- <tfoot>
                    <tr>
                        <th colspan="7"></th> 
                    </tr>
                    <tr>
                        <th colspan="3" class="text-right"><b>Grand Total</b></th>
                        <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($totalAmount), 2); ?></b></th>
                        <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($totalPaid), 2); ?></b></th>
                        <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($totalTds), 2); ?></b></th>
                        <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($totalPaidVendor), 2); ?></b></th>
                        <th class="text-right"><b><?php echo Yii::app()->Controller->money_format_inr(round($totalAmount - $totalPaid), 2); ?></b></th>
                    </tr>
                </tfoot> -->
            </tbody>
        </table>
    </div>
</div>