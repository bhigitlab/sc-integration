<?php
$tblpx = Yii::app()->db->tablePrefix;
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
$newQuery1 = "";
$newQuery2 = "";
$newQuery3 = "";
$newQuery4 = "";
foreach ($arrVal as $arr) {
    if ($newQuery)
        $newQuery .= ' OR';
    if ($newQuery1)
        $newQuery1 .= ' OR';
    if ($newQuery2)
        $newQuery2 .= ' OR';
    if ($newQuery3)
        $newQuery3 .= ' OR';
    if ($newQuery4)
        $newQuery4 .= ' OR';
    $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
    $newQuery1 .= " FIND_IN_SET('" . $arr . "', id)";
    $newQuery2 .= " FIND_IN_SET('" . $arr . "', b.company_id)";
    $newQuery3 .= " FIND_IN_SET('" . $arr . "', i.company_id)";
    $newQuery4 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "purchase_return.company_id)";
}

$billsDatasql = "SELECT b.bill_id as billid, b.bill_number as billnumber FROM " . $tblpx . "bills b LEFT JOIN " . $tblpx . "purchase p ON b.purchase_id = p.p_id WHERE (" . $newQuery2 . ") AND b.bill_totalamount IS NOT NULL AND b.purchase_id IS NOT NULL AND b.bill_id NOT IN (Select bill_id from " . $tblpx . "expenses WHERE bill_id IS NOT NULL) GROUP BY b.bill_id  ORDER BY b.bill_id DESC";
$billsData = Yii::app()->db->createCommand($billsDatasql)->queryAll();

$invoicesDatasql = "SELECT i.invoice_id, i.inv_no as invoice_no FROM " . $tblpx . "invoice i LEFT JOIN " . $tblpx . "projects p ON i.project_id = p.pid WHERE (" . $newQuery3 . ") AND i.amount >0 AND i.invoice_id NOT IN(Select invoice_id from " . $tblpx . "expenses WHERE invoice_id IS NOT NULL) GROUP BY i.invoice_id ORDER BY i.invoice_id DESC";
$invoicesData = Yii::app()->db->createCommand($invoicesDatasql)->queryAll();

$purchasereturnDatasql = "SELECT * FROM " . $tblpx . "purchase_return LEFT JOIN " . $tblpx . "bills ON " . $tblpx . "purchase_return.bill_id=" . $tblpx . "bills.bill_id WHERE " . $tblpx . "purchase_return.return_totalamount >0 AND (" . $newQuery4 . ") AND  " . $tblpx . "purchase_return.return_id NOT IN(Select return_id from " . $tblpx . "expenses WHERE return_id IS NOT NULL) GROUP BY " . $tblpx . "purchase_return.return_id ORDER BY " . $tblpx . "purchase_return.return_id";
$purchasereturnData = Yii::app()->db->createCommand($purchasereturnDatasql)->queryAll();

$worktype = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "daily_work_type")->queryAll();
?>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<div class="container" id="expense">

    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="expenses-heading header-container">
        <h3>Labour Entries</h3>
        <div class="btn-container">
            <?php
            $pms_api_integration_model = ApiSettings::model()->findByPk(1);
            $pms_api_integration = $pms_api_integration_model->api_integration_settings;
            if ($pms_api_integration == 1) {
                echo CHtml::link(CHtml::encode("Refresh"), array('DailyReport/RefreshButton', 'type' => 1), array('class' => 'btn btn-info addentries'));
            }
            ?>
            <?php echo CHtml::button('Entries', array(
                'class' => 'btn btn-info addentries',
                'onclick' => 'window.open("' . Yii::app()->createUrl("dailyReport/admin", array("status" => 3)) . '", "_blank")'
            )); ?>
            <?php if (isset(Yii::app()->user->role) && (in_array('/dailyReport/createdailyreport', Yii::app()->user->menuauthlist))) {?>
                <button type="button" class="btn btn-info addentries collapsed" data-toggle="collapse"
                    data-target="#daybookform" id="dailyformbutton"></button>
            <?php } ?>
           
           
             <?php echo CHtml::button('Estimation', array(
                'class' => 'btn btn-info  addentries ',
                'onclick' => 'window.open("' . Yii::app()->createUrl("purchase/itemestimation") . '", "_blank")'
            )); ?>

            
        </div>
    </div>
    <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
    <div class="daybook form">
        <div id="error-message-form"></div>
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'dailyreport-form',
            'enableAjaxValidation' => false,
        ));
        ?>
        <input type="hidden" name="integrationStatus" id="integrationStatus" value="<?= $pms_api_integration == 1 ?>">
        <div class="clearfix">
            <div class="datepicker">
                <div class="page_filter clearfix  filter-wrapper">
                    <div class="filter_elem">
                        <label>Entry Date:</label>
                        <?php echo CHtml::activeTextField($model, 'date', array('class' => 'form-control', 'readonly' => true, 'autocomplete' => 'off', 'id' => 'Dailyreport_expense_date', 'value' => date('d-m-Y'), 'onchange' => 'changedate()')); ?>
                    </div>
                    <div class="filter_elem links_hold">
                        <a href="#" id="previous" class="link">Previous</a> |
                        <a href="#" id="current" class="link">Current</a> |
                        <a href="#" id="next" class="link">Next</a>
                    </div>
                    <?php
                    $tblpx = Yii::app()->db->tablePrefix;
                    $expenseData = Yii::app()->db->createCommand("select MAX(date) as entry_date FROM " . $tblpx . "expenses")->queryRow();
                    ?>
                    <div class="ml-auto">
                        <span id="lastentry">
                            Last Entry date :
                            <span id="entrydate">
                                <?php echo (isset($lastdate['date']) ? date("d-M-Y", strtotime($lastdate['date'])) : ''); ?>
                            </span>
                        </span>
                    </div>
                </div>
            </div>
        </div>


        <div class="entries-wrapper collapse " id="daybookform">
            <div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="heading-title">Main Details</div>
                        <div class="dotted-line"></div>
                    </div>
                </div>


                <div class="row">
                    <div class="form-group col-xs-12 col-md-3">

                        <label for="project">Company</label>
                        <?php
                        echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array(
                            'select' => array('id, name'),
                            'order' => 'name ASC',
                            'condition' => '(' . $newQuery1 . ')',
                            'distinct' => true,
                        )), 'id', 'name'), array('class' => 'form-control js-example-basic-single company', 'empty' => '-Select Company-', 'style' => 'width:100%;'));
                        ?>

                    </div>
                    <div class="form-group col-xs-12 col-md-3">

                        <label for="project">Project</label>
                        <?php
                        echo $form->dropDownList($model, 'projectid', CHtml::listData(Projects::model()->findAll(array(
                            'select' => array('pid, name'),
                            'order' => 'name ASC',
                            'condition' => '(' . $newQuery . ')',
                            'distinct' => true,
                        )), 'pid', 'name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Project-', 'style' => 'width:100%;'));
                        ?>

                    </div>
                    <div class="form-group col-xs-12 col-md-3">



                        <label for="vendor">Subcontractor <span class="icon icon-exclamation"
                                title="For company entry, select 'No subcontractor' option in the drop down"></span></label>
                        <select class="form-control js-example-basic-single" name="Dailyreport[subcontractor_id]"
                            id="Dailyreport_subcontractor_id" style="width:100%;">
                            <option value="">-Select Subcontractor-</option>
                        </select>
                        <input type="hidden" name="bill_vendor" id="bill_vendor" value="">

                    </div>
                    <div class="form-group col-xs-12 col-md-3">

                        <label for="vendor">Expense Head</label>
                        <select class="form-control js-example-basic-single" name="Dailyreport[expensehead_id]"
                            id="Dailyreport_expensehead_id" style="width:100%;">
                            <option value="">-Select Expense Head-</option>
                        </select>
                        <input type="hidden" name="bill_vendor" id="bill_vendor" value="">

                    </div>
                    <div class="form-group col-xs-12 col-md-4 description_cls">

                        <label for="Description">Description</label>
                        <?php echo $form->textArea($model, 'description', array(
                            'value' => $model['description'],
                            'rows' => 4,
                            'cols' => 50,
                            'class' => "form-control description",
                            'style' => "height:auto; width:auto;"
                        )); ?>

                    </div>
                    <div class=" form-group col-xs-12 col-md-1 ">
                        <div class="form-group">
                            <label for="project">Over Time</label>
                            <div class="form-check">

                                <div class="form-check">
                                    <input type="hidden" name="Dailyreport[overtime]" value="0">
                                    <!-- Hidden input for unchecked state -->
                                    <input type="checkbox" class="form-check-input" id="overtime"
                                        name="Dailyreport[overtime]" value="1" <?php echo (isset($model->overtime) && $model->overtime == '1') ? 'checked="checked"' : '' ?>>
                                    <label class="form-check-label" for="overtime">OT</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-md-3 scquotation_no_class" style="display:none;">

                        <label for="sc_quot_no">Sc Quotation No</label>
                        <select class="form-control js-example-basic-single" name="Dailyreport[scquotation_id]"
                            id="Dailyreport_scquotation_id" style="width:100%;">
                            <option value="">-Select sc quotation no-</option>
                        </select>
                        <input type="hidden" name="bill_vendor" id="bill_vendor" value="">

                    </div>
                    <div class="form-group col-xs-12 col-md-3 project_template_class" style="display:none;">

                        <label for="project_template">Project Template</label>
                        <select class="form-control js-example-basic-single" name="Dailyreport[labour_template_id]"
                            id="Dailyreport_template_id" style="width:100%;">

                        </select>
                        <input type="hidden" name="bill_vendor" id="bill_vendor" value="">

                    </div>
                    <div class="form-group col-xs-12 col-md-4">
                        <label>&nbsp;</label>
                        <div id="dialog-modal" class="shdow_box first-thead-row" style="
                                    max-height: 150px;
                                    overflow: auto;
                                    "></div>
                    </div>
                </div>
                <div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="heading-title">Amount Details</div>
                            <div class="dotted-line"></div>
                        </div>
                    </div>
                    <div class="row daybook-inner">
                        <div class="field_details"></div>
                        <div id="dynamicInputs" style="width: 100%;"></div>
                        <div class="form-group col-xs-12 col-md-3">

                            <label for="description">Amount</label>
                            <input class="form-control" readonly="readonly" id="Dailyreport_amount"
                                name="Dailyreport[amount]" value=<?php echo $model->amount ?>>

                        </div>
                        <input type="hidden" name="Dailyreport[dr_id]" value="" id="txtDailyReportId" />

                        <div class="form-group col-xs-12 text-right">
                            <button type="button" class="btn btn-primary" id="buttonsubmit">ADD</button>
                            <button type="button" class="btn btn-default" id="btnReset">RESET</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>
        <div class="">
            <div class="">
                <div id="errormessage"></div>
            </div>
        </div>
        <div id="daybook-entry">
            <div class="" id="newlist">
                <?php $this->renderPartial('newlist', array('newmodel' => $newmodel, )); ?>
            </div>
            <div class="mt note_sec" style="display:none;">
                <h5 href="#demo" data-toggle="collapse"><i class="fa fa-edit mr-2"></i><b>Notes</b></h5>
                <div id="demo" class="collapse">
                    <h6 class="mt" style="color: #dc3232;
                font-weight: 600;">Daily labour report is only for without quotation.</h6>
                    <h6 class="mt">There is a total of 4 templates in daily report which satisfy the following
                        calculation :</h6>
                    <div class="list_sec">
                        <ul>
                            <li>Labour * Corresponding wage * Wage rate = Amount</li>
                            <li>( Mason * Mason Labour * Mason Wage) + (Helper * Helper labour * Helper Wage) = Amount
                            </li>
                            <li>( Labour + Helper ) * Corresponding wage * wage rate =Amount</li>
                            <li>Lumpsum</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="rejectModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="rejectModalLabel">Reject Labour</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="rejectForm">
                        <input type="hidden" id="rejectId" name="id">
                        <div class="form-group">
                            <label for="remarks">Remarks</label>
                            <textarea class="form-control" id="remarks" name="remarks" rows="3" required></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" onClick="submitReject()">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php //echo '<pre>';print_r($model);exit; ?>
<?php $dropdownUrl = Yii::app()->createAbsoluteUrl("dailyReport/dynamicDropdown"); ?>
<?php $vendorUrl = Yii::app()->createAbsoluteUrl("dailyReport/dynamicVendor"); ?>
<?php $invoiceUrl = Yii::app()->createAbsoluteUrl("dailyReport/GetInvoiceDetails"); ?>
<?php $returnUrl = Yii::app()->createAbsoluteUrl("dailyReport/GetPurchaseReturnDetails"); ?>
<?php $getUrl = Yii::app()->createAbsoluteUrl("dailyReport/getDataByDate"); ?>
<?php $getBillUrl = Yii::app()->createAbsoluteUrl("dailyReport/GetBillDetails"); ?>
<?php $getDailyreport = Yii::app()->createAbsoluteUrl("dailyReport/getDailyReportDetails"); ?>
<?php $bill_invoice = Yii::app()->createAbsoluteUrl("dailyReport/DynamicBillorInvoice"); ?>
<?php $projectUrl = Yii::app()->createAbsoluteUrl("dailyReport/DynamicProject"); ?>
<?php $getScQuotationUrl = Yii::app()->createAbsoluteUrl("dailyReport/getDynamicScQuotation"); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-dateFormat/1.0/jquery.dateFormat.js"></script>
<script>
    $(".js-example-basic-single").select2();
    $(function () {
        $("#Dailyreport_expense_date").datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: new Date()
        });
    });
    $(document).ready(function () {
        var urlParams = new URLSearchParams(window.location.search);
        if (urlParams.has('id')) {
            editRowClick('', urlParams.get('id'));
        }

        $('#loading').hide();
        $('#daybookform').on('shown.bs.collapse', function () {
            $('select').first().focus();
        });

        var model_company_id = '<?php echo $model->company_id; ?>';
        var model_project_id = '<?php echo $model->projectid; ?>';
        var model_subid = '<?php echo $model->subcontractor_id; ?>';
        if (model_subid == null) {
            model_subid = '-1';
        }
        var model_expensehead_id = '<?php echo $model->expensehead_id; ?>';

        getProjectList(model_company_id, 0, model_project_id, model_subid);
        getExpenseHead(model_subid, model_expensehead_id);




    });
</script>
<script type="text/javascript">


    function openRejectModal(id) {
        $('#rejectId').val(id);
        $('#rejectModal').modal('show');
    }

    function submitReject() {
        var id = $('#rejectId').val();
        var remarks = $('#remarks').val();

        if (remarks === "") {
            alert("Remarks field cannot be empty.");
            return;
        }

        $.ajax({
            type: "POST",
            url: "index.php?r=DailyReport/rejectLabour",
            data: {
                id: id,
                remarks: remarks,
            },
            success: function (response) {
                var result = JSON.parse(response);
                if (result.response === 'success') {
                    alert('Labour rejected successfully');
                    $('#rejectModal').modal('hide');
                    location.reload();
                } else {
                    alert('Error: ' + result.msg);
                }
            },
            error: function () {
                alert('An error occurred. Please try again.');
            }
        });
    }

    function getFullData(newDate) {
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            url: "<?php echo $getUrl; ?>",
            data: {
                "date": newDate
            },
            type: "POST",
            success: function (data) {
                $("#newlist").html(data);
            }
        });
    }

    function changedate() {
        var cDate = $("#Dailyreport_expense_date").val();
        getFullData(cDate);
        validateDateSelection(cDate);
    }

    function view(id) {
        window.location.href = '<?php echo Yii::app()->createAbsoluteUrl('DailyReport/viewlabour&id='); ?>' + id;
    }

    function getDropdownList(project_id, bill_id, exptypeid, edit, expId) {
        $('#loading').show();
        if (edit == undefined || edit == '') {
            edit = '';
        }
        $.ajax({
            url: "<?php echo $dropdownUrl; ?>",
            data: {
                "project": project_id,
                "bill": bill_id,
                "expId": expId
            },
            type: "POST",
            success: function (data) {
                var result = JSON.parse(data);
                $("#Dailyreport_bill_id").html(result["bills"]);
                if (edit == 'edit') {
                    $("#Dailyreport_projectid").select2("focus");
                } else {
                    $("#Dailyreport_bill_id").select2("focus");
                }

                if (exptypeid == 0) {
                    $("#Dailyreport_bill_id").val(0);
                    $("#Dailyreport_vendor_id").html('<option value="">-Select Vendor-</option>');
                } else {
                    if (bill_id === null) {
                        $("#Dailyreport_bill_id").val(0);
                    } else {
                        if (result['type'] == 'bill') {
                            $("#Dailyreport_bill_id").val('1,' + bill_id);
                        } else if (result['type'] == 'invoice') {
                            $("#Dailyreport_bill_id").val('2,' + bill_id);
                        } else {
                            $("#Dailyreport_bill_id").val('3,' + bill_id);
                        }
                    }

                }

                $("#Dailyreport_expensetype_id").html(result["expenses"]);
                if (exptypeid != 0)
                    $("#Dailyreport_expensetype_id").val(exptypeid);
            },
        });
    }

    function getDynamicBillorInvoice() {
        $('#loading').show();
        $.ajax({
            url: "<?php echo $bill_invoice; ?>",
            type: "POST",
            success: function (data) {
                $("#Dailyreport_bill_id").html(data);
            }
        });
    }
    var integration = $("#integrationStatus").val();

    function getProjectList(comId, opt, pro, sub) {
        $('#loading').show();

        $.ajax({
            url: "<?php echo $projectUrl; ?>",
            data: {
                "comId": comId
            },
            type: "POST",
            dataType: 'json',
            success: function (data) {
                if (comId == 0) {
                    $("#Dailyreport_projectid").val('').trigger('change.select2');
                    $("#Dailyreport_subcontractor_id").val('').trigger('change.select2');
                } else {
                    if (integration != 1) {
                        $("#Dailyreport_projectid").html(data.project);
                    }
                    $("#Dailyreport_subcontractor_id").html(data.subcontractor);
                }
                $('#Dailyreport_bill_id').val('0').trigger('change.select2');
                $("#Dailyreport_subcontractor_id").val(sub).trigger('change.select2');
                if (integration != 1) {
                    $("#Dailyreport_projectid").val(pro).trigger('change.select2');
                }
                // $("#Dailyreport_expensehead_id").val('0').trigger('change.select2');
                // if (sub != 0) {
                //     $("#Dailyreport_subcontractor_id").val(sub).trigger('change.select2');
                // }
                // if (pro != 0) {
                //     $("#Dailyreport_projectid").val(pro).trigger('change.select2');
                // }

            }
        });
    }



    $("#Dailyreport_company_id").change(function () {
        var comId = $("#Dailyreport_company_id").val();
        var comId = comId ? comId : 0;
        if (integration != 1) {
            getProjectList(comId, 0, 0, 0);
        }
        else {
            getExpenseHead(-1, 0);
            getProjectList(comId, 0, 0, 0);
            $(".labour_count").each(function () {
                $(this).trigger('blur');
                // calculateLabourAmount(this);
            });
        }
    });

    $("#Dailyreport_projectid").change(function () {
        var prId = $("#Dailyreport_projectid").val();
        var prId = prId ? prId : 0;
        getVendorList(prId, 0);
    });
    $(document).ready(function () {
        $("#Dailyreport_subcontractor_id,#Dailyreport_projectid").change(function () {
            if (integration != 1) {
                let company_id = $("#Dailyreport_company_id").val();
                let subcontractor_id = $("#Dailyreport_subcontractor_id").val();
                let project_id = $("#Dailyreport_projectid").val();
                $('#dialog-modal').html("");
                $(".field_details").html('');
                if (company_id != '' && project_id != '' && subcontractor_id != '') {
                    if (subcontractor_id != '-1' && subcontractor_id != null) {
                        $('#dynamicInputs').html('');
                        $.ajax({
                            url: '<?php echo Yii::app()->createAbsoluteUrl('dailyReport/getDynamicScQuotation'); ?>',
                            data: {
                                "company_id": company_id,
                                "project_id": project_id,
                                "subcontractor_id": subcontractor_id,
                            },
                            type: "POST",
                            dataType: 'json',
                            success: function (data) {
                                $('#dialog-modal').html('');

                                if (data.response_no == 1) {
                                    $('#dialog-modal').html('');
                                    // $(".description_cls").css("display","none");
                                    $(".project_template_class").css("display", 'none');
                                    $(".scquotation_no_class").show();
                                    $("#Dailyreport_scquotation_id").html(data.scQuotNo);
                                    $("#Dailyreport_expensehead_id").select2("focus");



                                } else if (data.response_no == 0) {
                                    $(".project_template_class").css("display", 'none');
                                    $("#error-message-form").show()
                                        .html('<div class="alert alert-danger">No Quoation is Created against the Subcontractor.So please Create any Quotation  </div>')
                                        .fadeOut(8000);

                                }

                            }

                        });
                    } else if (subcontractor_id == '-1') {
                        let type = 2;
                        $(".description_cls").css("display", "");
                        $(".project_template_class").css("display", '');
                        $(".scquotation_no_class").hide();
                        $.ajax({
                            url: '<?php echo Yii::app()->createUrl('dailyReport/getProjectTemplate'); ?>',
                            method: 'GET',
                            data: {
                                scquotation_id: project_id,
                                type: type,

                            },
                            success: function (response) {
                                const data = JSON.parse(response);
                                if (data.response_no == 1) {
                                    $('#dialog-modal').html('');
                                    // $(".description_cls").css("display","none");

                                    $("#Dailyreport_template_id").html(data.templateOptions);
                                    $(".project_template_class").show();
                                    $("#Dailyreport_template_id").select2();


                                } else if (data.response_no == 0) {

                                    $("#error-message-form").show()
                                        .html('<div class="alert alert-danger">No Template is Selected against the Project.So please select template in project settings</div>')
                                        .fadeOut(8000);

                                }


                            }
                        })

                    }


                }
            }

        });
        $("#Dailyreport_scquotation_id").change(function () {
            var scquotation_id = $(this).val();
            // alert(scquotation_id);
            var type = '';
            type = 1;
            if ($(this).val() != 0) {
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('dailyReport/getTemplateDetails'); ?>',
                    method: 'GET',
                    data: {
                        scquotation_id: scquotation_id,
                        type: type,

                    },
                    success: function (response) {
                        if (response == '0') {
                            $("#error-message-form").show()
                                .html('<div class="alert alert-danger">No Template is selected! Please Select Labour Template against the selected Quotation </div>')
                                .fadeOut(8000);
                        } else {
                            console.log(response);
                            $('#dialog-modal').html(response);
                            getExpenseFields(scquotation_id, 1);
                        }


                    }
                })

            } else {
                $('#dialog-modal').html("");
            }
        });

        $("#Dailyreport_template_id").change(function () {
            var template_id = $(this).val();
            let project_id = $("#Dailyreport_projectid").val();
            // alert(scquotation_id);
            var type = '';
            type = 2;
            if ($(this).val() != 0) {
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('dailyReport/getTemplateDetails'); ?>',
                    method: 'GET',
                    data: {
                        template: template_id,
                        type: type,
                        scquotation_id: project_id,

                    },
                    success: function (response) {
                        if (response == '0') {
                            $("#error-message-form").show()
                                .html('<div class="alert alert-danger">No Template is selected! Please Select Labour Template against the selected Quotation </div>')
                                .fadeOut(8000);
                        } else {
                            let project_id = $("#Dailyreport_projectid").val();
                            console.log(response);
                            $('#dialog-modal').html(response);
                            getExpenseFields(project_id, 2, template_id);
                        }


                    }
                })

            } else {
                $('#dialog-modal').html("");
            }
        });



    });

    function getVendorList(prId, expvendor, edit) {
        $('#loading').show();
        if (edit == undefined) {
            edit = '';
        }
        if (prId === null) {
            prId = 0;
        } else {
            prId = prId;
        }

        $.ajax({
            url: "<?php echo $vendorUrl; ?>",
            data: {
                "prId": prId
            },
            type: "POST",
            success: function (data) {
                $("#Dailyreport_vendor_id").html(data);
                if (edit == 'edit') {
                    $("#Dailyreport_company_id").select2("focus");
                } else {
                    $("#Dailyreport_vendor_id").select2("focus");
                }
                if (expvendor != 0)
                    $("#Dailyreport_vendor_id").val(expvendor);
            }
        });
    }

    $(document).ready(function () {
        $("#Dailyreport_projectid").focus();
        $("#Dailyreport_bank_id").attr("disabled", true);
        $("#txtChequeno").attr("readonly", true);
        $("#txtPurchaseType").attr("disabled", true);
        $("#txtPaid").attr("readonly", true);
        $("a#next").css("pointer-events", "none").css("color", "#ccccc");
        $("#buttonsubmit").click(function (event) {
            event.preventDefault()
            $("#buttonsubmit").attr('disabled', true);
            var crDate = $("#Dailyreport_expense_date").val();
            var project = $("#Dailyreport_projectid").val();
            var company = $("#Dailyreport_company_id").val();
            var subcontractor_id = $("#Dailyreport_subcontractor_id").val();
            var expensehead_id = $("#Dailyreport_expensehead_id").val();
            var amount = $("#Dailyreport_amount").val();
            var overtime = 0;
            if ($("#overtime").is(":checked")) {
                overtime = 1;
            }

            var labour = $("#Dailyreport_labour").val();
            var wage = $("#Dailyreport_wage").val();
            var wage_rate = $("#Dailyreport_wage_rate").val();
            var helper = $("#Dailyreport_helper").val();
            var helper_labour = $("#Dailyreport_helper_labour").val();
            var lump_sum = $("#Dailyreport_lump_sum").val();

            if (company == "") {
                $("#errormessage").show()
                    .html('<div class="alert alert-danger">Please select a company from the dropdown.</div>')
                    .fadeOut(5000);
                $("#buttonsubmit").attr('disabled', false);
                return false;
            }
            if (project == "") {
                $("#errormessage").show()
                    .html('<div class="alert alert-danger">Please select a project from the dropdown.</div>')
                    .fadeOut(5000);
                $("#buttonsubmit").attr('disabled', false);
                return false;
            }
            if (subcontractor_id == "") {
                $("#errormessage").show()
                    .html('<div class="alert alert-danger">Please select a subcontractor from the dropdown.</div>')
                    .fadeOut(5000);
                $("#buttonsubmit").attr('disabled', false);
                return false;
            }

            if (amount == "" || amount < 1) {
                $("#error-message-form").show()
                    .html('<div class="alert alert-danger">Amount cannot blank and 0 .If labour template is not selected then please select template</div>')
                    .fadeOut(5000);
                $("#buttonsubmit").attr('disabled', false);
                return false;
            }
            var dlyReportId = $("#txtDailyReportId").val();
            var actionUrl;
            if (dlyReportId == "") {
                actionUrl = "<?php echo Yii::app()->createAbsoluteUrl("dailyReport/addDailyentry"); ?>";
                localStorage.setItem("action", "add");
            } else {
                actionUrl = "<?php echo Yii::app()->createAbsoluteUrl("dailyReport/updateDailyentry"); ?>";
                localStorage.setItem("action", "update");
            }
            var data = $("#dailyreport-form").serialize();
            $('.loading-overlay').addClass('is-active');

            $.ajax({
                type: 'GET',
                url: actionUrl,
                data: data,
                success: function (data) {
                    $("#buttonsubmit").attr('disabled', false);
                    $('.loading-overlay').removeClass('is-active');
                    if (data == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-warning"><strong>Failed !</strong> please try again.</div>')
                            .fadeOut(8000);
                        $("#buttonsubmit").attr('disabled', false);
                        return false;
                    } else {
                        var urlParams = new URLSearchParams(window.location.search);
                        if (urlParams.has('id')) {
                            var url_ = "<?php echo Yii::app()->createAbsoluteUrl("dailyReport/create"); ?>";
                            window.location.replace(url_);

                        }
                        var currAction = localStorage.getItem("action");
                        if (currAction == "add") {
                            $("#errormessage").show()
                                .html('<div class="alert alert-success"><strong>Successfully !</strong> record added.</div>')
                                .fadeOut(10000);
                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth() + 1;
                            var yyyy = today.getFullYear();
                            if (dd < 10)
                                dd = '0' + dd
                            if (mm < 10)
                                mm = '0' + mm
                            today = dd + '-' + mm + '-' + yyyy;
                            $("#entrydate").text(today);
                            $('#Dailyreport_subcontractor_id').val('').trigger('change.select2');
                            $('#Dailyreport_expensehead_id').val('').trigger('change.select2');
                            $('#Dailyreport_projectid').val('').trigger('change.select2');
                            $('#Dailyreport_company_id').val('').trigger('change.select2');
                            $('#Dailyreport_bill_id').val('0').trigger('change.select2');
                            $('#Dailyreport_expensetype_id').html('<option value="">-Select Expense Head-</option>');
                            $('#txtPurchaseType').val("").trigger('change.select2');
                            $('#Dailyreport_expense_type').val('').trigger('change.select2');
                            $('#Dailyreport_employee_id').val('').trigger('change.select2');
                            $('#Dailyreport_bank_id').val('').trigger('change.select2');
                            $('#Dailyreport_vendor_id').html('<option value="">-Select Vendor-</option>');
                        } else if (currAction == "update") {

                            $("#txtDaybookId").val('');
                            $("#buttonsubmit").text("ADD");
                            if (data == 2) {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-success"><strong>Successfully !</strong> Update request send succesfully. Please wait for approval</div>')
                                    .fadeOut(10000);
                                $('.loading-overlay').addClass('is-active');
                                $.ajax({
                                    type: "POST",
                                    data: {
                                        date: crDate
                                    },
                                    url: "<?php echo Yii::app()->createUrl("dailyReport/getAllData") ?>",
                                    success: function (response) {
                                        document.getElementById("dailyreport-form").reset();
                                        $("#Dailyreport_expense_date").val(crDate);
                                        $("#newlist").html(response);
                                        $("#Dailyreport_projectid").focus();
                                        $("#Dailyreport_bill_id").attr("disabled", false);
                                        $("#Dailyreport_expensetype_id").attr("disabled", false);
                                        $("#Dailyreport_payment_type").attr("disabled", false);
                                        $("#txtReceipt").attr("readonly", false);
                                        $("#buttonsubmit").attr("disabled", false);


                                        $('#Dailyreport_projectid').val('').trigger('change.select2');
                                        $('#Dailyreport_company_id').val('').trigger('change.select2');
                                        $('#Dailyreport_bill_id').val('0').trigger('change.select2');
                                        $('#Dailyreport_expensetype_id').html('<option value="">-Select Expense Head-</option>');
                                        $('#txtPurchaseType').val("").trigger('change.select2');
                                        $('#Dailyreport_expense_type').val('').trigger('change.select2');
                                        $('#Dailyreport_employee_id').val('').trigger('change.select2');
                                        $('#Dailyreport_bank_id').val('').trigger('change.select2');
                                        $('#Dailyreport_vendor_id').html('<option value="">-Select Vendor-</option>');

                                        $("#txtAmount").attr("readonly", true);
                                        $("#txtSgstp").attr("readonly", true);
                                        $("#txtCgstp").attr("readonly", true);
                                        $("#txtIgstp").attr("readonly", true);
                                        $("#txtPurchaseType").attr("disabled", true);
                                        $("#txtPaid").attr("readonly", true);
                                        $("#Dailyreport_bank_id").attr("disabled", true);
                                        $("#txtChequeno").attr("readonly", true);

                                        $("#txtSgst1").text("");
                                        $("#txtCgst1").text("");
                                        $("#txtIgst1").text("");
                                        $("#txtTotal1").html("");
                                        $("#txtgstTotal").text("");
                                    }
                                });
                            } else if (data == 3) {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-danger"><strong>Failed !</strong> Cannot send update request.</div>')
                                    .fadeOut(10000);
                            } else if (data == 4) {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-warning"><strong>Failed !</strong> Cannot send update request. Already have a request for this.</div>')
                                    .fadeOut(10000);
                            } else {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-success"><strong>Successfully !</strong> record updated.</div>')
                                    .fadeOut(10000);
                            }
                        }
                        document.getElementById("dailyreport-form").reset();
                        $("#Dailyreport_expense_date").val(crDate);
                        if (data == 2 || data == 3 || data == 4) {

                        } else {
                            $("#newlist").html(data);
                        }
                        $('#Dailyreport_bill_id').val('0').trigger('change.select2');
                        $('#Dailyreport_expensetype_id').html('<option value="">-Select Expense Head-</option>');
                        $('#txtPurchaseType').val("").trigger('change.select2');
                        $('#Dailyreport_expense_type').val('').trigger('change.select2');
                        $('#Dailyreport_employee_id').val('').trigger('change.select2');
                        $('#Dailyreport_bank_id').val('').trigger('change.select2');
                        $('#Dailyreport_vendor_id').html('<option value="">-Select Vendor-</option>');
                    }
                    location.reload();
                },
                error: function (data) {
                    alert("Error occured.please try again");
                    alert(data);
                }
            });
        });
        $("#txtPaid").keyup(function () {
            var totalamount = parseFloat($("#txtTotal").val());
            var purchasetype = $("#txtPurchaseType").val();
            var paid = parseFloat($("#txtPaid").val());
            if (purchasetype == 3) {
                if (paid >= totalamount) {
                    $("#errormessage").show()
                        .html('<div class="alert alert-warning">You have selected the purchase type as Partially Paid.<br/>The paid amount must be less than the total amount.</div>')
                        .fadeOut(10000);
                    $("#buttonsubmit").attr('disabled', true);
                    return false;
                } else if (paid <= 0) {
                    $("#errormessage").show()
                        .html('<div class="alert alert-warning">You have selected the purchase type as Partially Paid.<br/>The paid amount must be greater than 0.</div>')
                        .fadeOut(10000);
                    $("#buttonsubmit").attr('disabled', true);
                    return false;
                } else {

                    $("#buttonsubmit").attr('disabled', false);
                }
            }
            if (paid == "") {
                if (totalamount == "") {
                    getTdsCalculations(0);
                } else {
                    getTdsCalculations(totalamount);
                }
            } else {
                getTdsCalculations(paid);
            }
        });

        $("body").on("click", ".row-daybook", function (e) {
            var rowId = $(this).attr("id");
            var expId = $("#dlyreportid" + rowId).val();
            editRowClick(rowId, expId);
        });



        $("#previous").click(function (e) {
            var cDate = $("#Dailyreport_expense_date").val();
            cDate = cDate.split("-").reverse().join("-");
            var newDate = new Date(cDate);
            var prDate = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate() - 1);
            var newPrDate = $.format.date(prDate, "dd-MM-yyyy");
            $("#Dailyreport_expense_date").val(newPrDate);
            getFullData(newPrDate);
            validateDateSelection(newPrDate);
        });
        $("#current").click(function () {
            var cDate = new Date();
            var newDate = $.format.date(cDate, "dd-MM-yyyy");
            $("#Dailyreport_expense_date").val(newDate);
            getFullData(newDate);
            validateDateSelection(newDate);
        });
        $("#next").click(function () {
            var cDate = $("#Dailyreport_expense_date").val();
            cDate = cDate.split("-").reverse().join("-");
            var newDate = new Date(cDate);
            var nxtDate = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate() + 1);
            var newNxtDate = $.format.date(nxtDate, "dd-MM-yyyy");
            $("#Dailyreport_expense_date").val(newNxtDate);
            getFullData(newNxtDate);
            validateDateSelection(newNxtDate);
        });


        $('.numbersOnly').keyup(function () {
            this.value = this.value.replace(/[^0-9\.]/g, '');
        });


        $("#btnReset, [data-toggle='collapse']").click(function () {
            $("#txtDaybookId").val("");
            $("#buttonsubmit").text("ADD");
            $('#Dailyreport-form').find('input, select, textarea').not("#Dailyreport_expense_date").val('');
            $("#Dailyreport_expensetype_id").html('<option value="">-Select Expense Head-</option>');
            $("#Dailyreport_vendor_id").html('<option value="">-Select Vendor-</option>');
            $("#Dailyreport_expensetype_id").attr("disabled", false);
            $("#txtPurchaseType").attr("disabled", true);
            $('#Dailyreport_projectid').val('').trigger('change.select2');
            $('#Dailyreport_bill_id').val('0').trigger('change.select2');
            $('#Dailyreport_expensetype_id').val('').trigger('change.select2');
            $('#txtPurchaseType').val('').trigger('change.select2');
            $('#Dailyreport_expense_type').val('').trigger('change.select2');
            $('#Dailyreport_bank_id').val('').trigger('change.select2');
            $('#Dailyreport_vendor_id').val('').trigger('change.select2');
            $('#Dailyreport_employee_id').val('').trigger('change.select2');
            $('#Dailyreport_company_id').val('').trigger('change.select2');
            $('.employee_box').hide();
            $("#txtSgst1").text("");
            $("#txtCgst1").text("");
            $("#txtIgst1").text("");
            $("#txtTotal1").html("");
            $("#txtgstTotal").text("");
            $("#txtTdsp").val("");
            $("#txtTdsp").attr("readonly", false);
            $("#txtTds1").text("");
            $("#txtTds").val("");
            $("#txtTdsPaid").html("");
            $("#Dailyreport_paidamount").val("");
        });
    });


    $("#txtSgstp").keyup(function (e) {
        if (e.keyCode == 13) {
            $("#txtCgstp").focus();
        }
    });

    $("#txtCgstp").keyup(function (e) {
        if (e.keyCode == 13) {
            $("#txtIgstp").focus();
        }
    });

    $("#txtIgstp").keyup(function (e) {
        if (e.keyCode == 13) {
            $("#txtDescription").focus();
        }
    });

    $("#Dailyreport_company_id").change(function () {
        $('#loading').show();
        var company_id = $("#Dailyreport_company_id").val();
        if (company_id != "")
        //alert(company_id);
            $.ajax({
                method: "GET",
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('dailyReport/ajaxcall'); ?>',
                data:{
                    company_id:company_id,
                },
                success: function (result) {
                   console.log(result);
                    $("#Dailyreport_projectid").html(result['html']);
                    $("#Dailyreport_projectid").select2("focus");
                }
            });
    });

    // decimal point check
    $(document).on('keydown', '.check', function (event) {
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
            (event.keyCode >= 96 && event.keyCode <= 105) ||
            event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
            event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
            var splitfield = $(this).val().split(".");
            if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
                event.preventDefault();
            }
        } else {
            event.preventDefault();
        }
        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();
    });

    $('.percentage').keyup(function () {
        if ($(this).val() > 100) {
            $(this).val('');
        }
    });
    // delete confirmation
    $(document).on('click', '.delete_confirmation', function (e) {
        var rowId = $(this).attr("id");
        var expId = $("#dlyreportid" + rowId).val();
        var date = $("#Dailyreport_expense_date").val();
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                type: "POST",
                data: {
                    expId: expId,
                    expense_date: date,
                    status: 1,
                    delId: "",
                    action: 1
                },
                url: "<?php echo Yii::app()->createUrl("dailyReport/deleteconfirmation") ?>",
                success: function (data) {
                    $('.loading-overlay').removeClass('is-active');
                    if (data == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-danger"><strong>Failed!</strong> This request unable to process now. Please try again later.</div>')
                            .fadeOut(5000);
                        return false;
                    } else {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Success!</strong> Request for deleting this row is send to admin. Please wait for response.</div>')
                            .fadeOut(5000);
                        $("#newlist").html(data);
                    }
                }
            });
        } else {
            return false;
        }
    });
    // delete restore
    $(document).on('click', '.delete_restore', function (e) {
        var rowId = $(this).attr("id");
        var delId = $(this).attr("data-id");
        var expId = $("#dlyreportid" + rowId).val();
        var date = $("#Dailyreport_expense_date").val();
        var answer = confirm("Are you sure you want to restore?");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                type: "POST",
                data: {
                    expId: expId,
                    expense_date: date,
                    status: 2,
                    delId: delId,
                    action: 1
                },
                url: "<?php echo Yii::app()->createUrl("dailyReport/deleteconfirmation") ?>",
                success: function (data) {
                    $('.loading-overlay').removeClass('is-active');
                    if (data == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-danger"><strong>Failed!</strong> This request unable to process now. Please try again later.</div>')
                            .fadeOut(5000);
                        return false;
                    } else {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Success!</strong> Restored data successfully.</div>')
                            .fadeOut(5000);
                        $("#newlist").html(data);
                    }
                }
            });
        } else {
            return false;
        }
    });
    // delete row
    $(document).on('click', '.delete_row', function (e) {
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            var rowId = $(this).attr("id");
            var expId = $("#dlyreportid" + rowId).val();
            var date = $("#Dailyreport_expense_date").val();
            $.ajax({
                type: "POST",
                data: {
                    expId: expId,
                    expense_date: date
                },
                url: "<?php echo Yii::app()->createUrl("dailyReport/deletereport") ?>",
                success: function (data) {
                    $('.loading-overlay').removeClass('is-active');
                    if (data == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-warning"><strong>Failed !</strong> please try again.</div>')
                            .fadeOut(5000);
                        $("#buttonsubmit").attr('disabled', false);
                        return false;
                    } else {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Success!</strong> Row Deleted.</div>')
                            .fadeOut(5000);
                        $("#newlist").html(data);
                    }

                    $('#Dailyreport_projectid').val('').trigger('change.select2');
                    $('#Dailyreport_bill_id').val('0').trigger('change.select2');
                    $('#Dailyreport_expensetype_id').html('<option value="">-Select Expense Head-</option>');
                    $('#txtPurchaseType').val("").trigger('change.select2');
                    $('#Dailyreport_expense_type').val('').trigger('change.select2');
                    $('#Dailyreport_employee_id').val('').trigger('change.select2');
                    $('#Dailyreport_bank_id').val('').trigger('change.select2');
                    $('#Dailyreport_vendor_id').html('<option value="">-Select Vendor-</option>');
                    $('#Dailyreport_company_id').val('').trigger('change.select2');
                    $("#txtAmount").attr("readonly", true);
                    $("#txtSgstp").attr("readonly", true);
                    $("#txtCgstp").attr("readonly", true);
                    $("#txtIgstp").attr("readonly", true);
                    $("#txtPurchaseType").attr("disabled", true);
                    $("#txtPaid").attr("readonly", true);
                    $("#Dailyreport_bank_id").attr("disabled", true);
                    $("#txtChequeno").attr("readonly", true);
                    $("#txtSgst1").text("");
                    $("#txtCgst1").text("");
                    $("#txtIgst1").text("");
                    $("#txtTotal1").html("");
                    $("#txtgstTotal").text("");
                }
            })
        }

    })

    function getTdsCalculations(total) {
        if ($("#txtTdsp").val() != "") {
            var tdsp = parseFloat($("#txtTdsp").val());
        } else {
            var tdsp = 0;
        }

        var tdsamount = (tdsp / 100) * total;
        if (isNaN(tdsamount))
            tdsamount = 0;
        var paidamount = total - parseFloat(tdsamount);
        if (isNaN(paidamount))
            paidamount = 0;

        $("#txtTds1").text(tdsamount.toFixed(2));
        $("#txtTds").val(tdsamount.toFixed(2));
        $("#txtTdsPaid").html("<b>Amount To Be Paid: </b>" + paidamount.toFixed(2) + "");
        $("#Dailyreport_paidamount").val(paidamount.toFixed(2));
    }
    function validateInput(input) {

        // Remove any non-numeric characters except for period (.)
        const regex = /^\d*\.?\d*$/;
        const value = input.value;

        // If the value doesn't match the regex, remove the last entered character
        if (!regex.test(value)) {
            input.value = value.slice(0, -1);
        }

        // Ensure the value is not negative
        if (input.value < 0) {
            input.value = 0;
            displayError(input, "Number of labours cannot be negative.");
        } else {
            clearError(input);
        }
    }

    function displayError(input, message) {
        var errorDiv = input.nextElementSibling;
        if (errorDiv) {
            errorDiv.textContent = message;
            errorDiv.style.color = "red";
        }
    }

    function clearError(input) {
        var errorDiv = input.nextElementSibling;
        if (errorDiv) {
            errorDiv.textContent = "";
        }
    }


    function editRowClick(rowId, expId) {
        $('html, body').animate({
            scrollTop: 0
        }, 'slow');
        $(".daybook .collapse").addClass("in");
        $(".addentries").removeClass("collapsed");
        if ($(".daybook .collapse").css('display') == 'visible') {
            $(".daybook .collapse").css({
                "height": "auto"
            });
        } else {
            $(".daybook .collapse").css({
                "height": ""
            });
        }
        $(".popover").removeClass("in");
        $("#buttonsubmit").text("UPDATE");
        $("#txtDailyReportId").val(expId);
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            url: "<?php echo $getDailyreport; ?>",
            data: {
                "dailyreport_id": expId
            },
            type: "POST",
            success: function (data) {
                $('.loading-overlay').removeClass('is-active');
                var result = JSON.parse(data);

                $("#Dailyreport_company_id").select2("focus");
                $('#Dailyreport_company_id').val(result["company_id"]).trigger('change.select2');
                $('#Dailyreport_projectid').val(result["projectid"]).trigger('change.select2');
                $subcontractor_id = 0;
                if (result["subcontractor_id"] == null) {
                    $subcontractor_id = '-1';
                } else {
                    $subcontractor_id = result["subcontractor_id"];
                }
                //console.log(overtime);
                if (result["overtime"] == 1) {
                    $("#overtime").prop("checked", true); // Check the checkbox
                } else {
                    $("#overtime").prop("checked", false); // Uncheck the checkbox
                }
                $("#Dailyreport_projectid").val(result["projectid"]);
                getProjectList(result["company_id"], 0, result["projectid"], $subcontractor_id);
                getExpenseHead($subcontractor_id, result["expensehead_id"]);
                // $('#Dailyreport_subcontractor_id').val(result["subcontractor_id"]).trigger('change.select2');

                $('#Dailyreport_amount').val(result["amount"]).trigger('change.select2');
                $(".field_details").html(result["html"]);
                $('#Dailyreport_expense_date').val(result['date']);
                $('#Dailyreport_description').val(result['description']);
                if (result['sc_quot_id'] != '' && result['sc_quot_id'] != 0) {
                    $(".description_cls").css("display", "");
                    $(".scquotation_no_class").show();
                    console.log(result['sc_quot_no']);
                    $("#Dailyreport_scquotation_id").html(result['scQuotOptions']);
                    getExpenseFieldsForEdit(expId, 2);
                    $('#Dailyreport_expensehead_id').val(result["expensehead_id"]);
                    let type = 1;
                    $.ajax({
                        url: '<?php echo Yii::app()->createUrl('dailyReport/getTemplateDetails'); ?>',
                        method: 'GET',
                        data: {
                            scquotation_id: result['sc_quot_id'],
                            type: type,

                        },
                        success: function (response) {
                            console.log(response);
                            $('#dialog-modal').html(response);

                        }
                    })

                } else {
                    getExpenseFieldsForEdit(expId, 2);
                    $('#Dailyreport_expensehead_id').html(result["expenseHeadOptions"]);
                    let type = 2;
                    let project_id = result["projectid"];

                    $('#Dailyreport_expensehead_id').html(result["expenseHeadOptions"]);
                    $(".project_template_class").css("display", '');
                    $(".scquotation_no_class").hide();



                    $.ajax({
                        url: '<?php echo Yii::app()->createUrl('dailyReport/getProjectTemplate'); ?>',
                        method: 'GET',
                        data: {
                            scquotation_id: project_id,
                            type: type,

                        },
                        success: function (response) {
                            const data = JSON.parse(response);
                            if (data.response_no == 1) {
                                $('#dialog-modal').html('');
                                // $(".description_cls").css("display","none");

                                $("#Dailyreport_template_id").html(data.templateOptions);
                                $(".project_template_class").show();
                                $("#Dailyreport_template_id").select2();
                                $("#Dailyreport_template_id").html(result["templateOptions"]);
                                type = 2;
                                project_id = result["projectid"];
                                template_id = result["labour_template"];
                                $.ajax({
                                    url: '<?php echo Yii::app()->createUrl('dailyReport/getTemplateDetails'); ?>',
                                    method: 'GET',
                                    data: {
                                        template: template_id,
                                        type: type,
                                        scquotation_id: project_id,

                                    },
                                    success: function (response) {

                                        let project_id = $("#Dailyreport_projectid").val();
                                        console.log(response);
                                        $('#dialog-modal').html(response);




                                    }
                                })


                            } else if (data.response_no == 0) {

                                $("#error-message-form").show()
                                    .html('<div class="alert alert-danger">No Template is Selected against the Project.So please select template in project settings</div>')
                                    .fadeOut(8000);

                            }


                        }
                    })
                    $('#Dailyreport_expensehead_id').html(result["expenseHeadOptions"]);


                }
                $('#Dailyreport_expensehead_id').html(result["expenseHeadOptions"]);

                if (result["subcontractor_id"] == null && result["subcontractor_id"] == '') {

                    //getExpenseFieldsForEdit(expId,2);
                    // $("#Dailyreport_subcontractor_id").val('-1').trigger('change');



                    $('#Dailyreport_expensehead_id').html(result["expenseHeadOptions"]);
                    $("#Dailyreport_template_id").html(result["templateOptions"]);
                    $("#Dailyreport_template_id").val(result["labour_template"]).trigger('change');
                }
                if ($("#integrationStatus").val() == 1) {
                    var $select = $('#Dailyreport_projectid');
                    var $select2Container = $select.next('.select2-container').find('.select2-selection--single');
                    $select2Container.css({
                        'background-color': '#e9ecef',
                        'pointer-events': 'none',
                        'cursor': 'not-allowed'
                    });
                    $select.on('select2:opening select2:unselecting', function (e) {
                        e.preventDefault();
                    });
                    var project_id = $('#Dailyreport_projectid').val();
                    var integration = $("#integrationStatus").val();
                    var company_id = $("#Dailyreport_company_id").val();
                    if (project_id && integration == 1) {
                        $.ajax({
                            url: '<?php echo Yii::app()->createAbsoluteUrl('DailyReport/GetCompany'); ?>',
                            type: 'GET',
                            data: { project_id: project_id },
                            success: function (response) {
                                var jsonResponse = JSON.parse(response);
                                if (jsonResponse.status == 1) {
                                    var companies = jsonResponse.companies;
                                    var $dropdown = $('#Dailyreport_company_id');
                                    $dropdown.empty();
                                    $dropdown.append('<option value="">-Select Company-</option>');
                                    $.each(companies, function (id, name) {
                                        $dropdown.append($('<option></option>').attr('value', id).text(name));
                                    });
                                    if (company_id) {
                                        $dropdown.val(company_id);
                                    }
                                    // $("#Dailyreport_subcontractor_id").append('<option value="-1">No subcontractor</option>');
                                    // $("#Dailyreport_subcontractor_id").val('-1').trigger('change');

                                } else {
                                    console.log(jsonResponse.message);
                                }
                            },
                            error: function () {
                                console.log('An error occurred while fetching company data.');
                            }
                        });
                    }
                    var templateId = result["labour_template"];
                    // getExpenseFields(project_id,2,templateId); 
                    getExpenseFieldsForEdit(expId, 2);



                }
            }

        });
    }


    $("#Dailyreport_subcontractor_id").change(function () {
        var id = $(this).val();
        if (id != '') {
            id = id ? id : 0;
            // if(integration!=1){
            getExpenseHead(id, 0);
            // }
            if (integration == 1) {
                let company_id = $("#Dailyreport_company_id").val();
                let subcontractor_id = $("#Dailyreport_subcontractor_id").val();
                let project_id = $("#Dailyreport_projectid").val();
                $('#dialog-modal').html("");
                $(".field_details").html('');
                if (company_id != '' && project_id != '' && subcontractor_id != '') {
                    if (subcontractor_id != '-1' && subcontractor_id != null) {
                        $('#dynamicInputs').html('');
                        $.ajax({
                            url: '<?php echo Yii::app()->createAbsoluteUrl('dailyReport/getDynamicScQuotation'); ?>',
                            data: {
                                "company_id": company_id,
                                "project_id": project_id,
                                "subcontractor_id": subcontractor_id,
                            },
                            type: "POST",
                            dataType: 'json',
                            success: function (data) {
                                $('#dialog-modal').html('');

                                if (data.response_no == 1) {
                                    $('#dialog-modal').html('');
                                    // $(".description_cls").css("display","none");
                                    $(".project_template_class").css("display", 'none');
                                    $(".scquotation_no_class").show();
                                    $("#Dailyreport_scquotation_id").html(data.scQuotNo);
                                    $("#Dailyreport_expensehead_id").select2("focus");



                                } else if (data.response_no == 0) {
                                    $(".project_template_class").css("display", 'none');
                                    $("#error-message-form").show()
                                        .html('<div class="alert alert-danger">No Quoation is Created against the Subcontractor.So please Create any Quotation  </div>')
                                        .fadeOut(8000);

                                }

                            }

                        });
                    } else if (subcontractor_id == '-1') {
                        let type = 2;
                        $(".description_cls").css("display", "");
                        $(".project_template_class").css("display", '');
                        $(".scquotation_no_class").hide();
                        $.ajax({
                            url: '<?php echo Yii::app()->createUrl('dailyReport/getProjectTemplate'); ?>',
                            method: 'GET',
                            data: {
                                scquotation_id: project_id,
                                type: type,

                            },
                            success: function (response) {
                                const data = JSON.parse(response);
                                if (data.response_no == 1) {
                                    $('#dialog-modal').html('');
                                    // $(".description_cls").css("display","none");

                                    $("#Dailyreport_template_id").html(data.templateOptions);
                                    $(".project_template_class").show();
                                    $("#Dailyreport_template_id").select2();


                                } else if (data.response_no == 0) {

                                    $("#error-message-form").show()
                                        .html('<div class="alert alert-danger">No Template is Selected against the Project.So please select template in project settings</div>')
                                        .fadeOut(8000);

                                }


                            }
                        })

                    }


                }
            }
        }
    });

    function getExpenseHead(id, exp) {
        $('#loading').show();

        let company_id = $("#Dailyreport_company_id").val();
        $.ajax({
            url: "<?php echo $this->createUrl('dailyReport/getexpensehead'); ?>",
            data: {
                "id": id,
                "company_id": company_id
            },
            type: "GET",
            success: function (response) {
                if (id == 0) {
                    $("#Dailyreport_expensehead_id").val('-Select Expense Head-').trigger('change.select2');
                } else {
                    $("#Dailyreport_expensehead_id").html(response);
                }
                $("#Dailyreport_expensehead_id").val(exp).trigger('change.select2');

            }
        });
    }

    function getExpenseFields(project_or_scquot_id, type, template = '') {
        $(".field_details").html('');
        $('#loading').show();
        $.ajax({
            type: "GET",
            data: {
                scquotation_id: project_or_scquot_id,
                type: type,
                template: template
            },
            url: "<?php echo $this->createUrl('dailyReport/getfields'); ?>",
            success: function (response) {

                if (response == '2') {
                    $(".field_details").html(response);
                    $('#dynamicInputs').html('');

                } else {
                    $(".field_details").html(response);
                    $('#dynamicInputs').html('');
                }

            }
        });
    }
    function getExpenseFieldsForEdit(dr_id, type) {
        $(".field_details").html('');
        var scquotation_id = $("#Dailyreport_scquotation_id").val();
        $(".field_details").html('');
        $('#loading').show();
        $.ajax({
            type: "GET",
            data: {
                dr_id: dr_id,
                type: type,
            },
            url: "<?php echo $this->createUrl('dailyReport/getfieldsForEdit'); ?>",
            success: function (response) {
                $(".field_details").html(response);

            }
        });
    }
    function addNewInputRow(isFirstRow) {

        let index = $('#dynamicInputs .input-group').length;
        if (index == '') {
            index = 0;
        }
        $.ajax({
            url: '<?php echo $this->createUrl("dailyReport/getNewInputRow"); ?>',
            type: 'POST',
            data: { index: index },
            success: function (response) {
                $('#dynamicInputs').append(response);
                $('.labour_worktype').select2();

                // Add 'Add' button div to first input row
                let addButtonDiv = '<div class="col-md-2 ">' +
                    '<button type="button" class="add-btn btn btn-info btn-sm">+</button>' +
                    '<button type="button" class="remove-btn btn btn-danger btn-sm" >-</button>' +
                    '</div>';
                $('#dynamicInputs .input-group:last').append(addButtonDiv);


            }
        });
    }
    function handleRemoveButtonClick(button) {

        console.log('Remove button clicked for button:', button);

        $(button).closest('.input-group').remove();

        updateTotalAmount();
    }
    function addDynamicRow() {
        addNewInputRow(false);
    }
    $('#dynamicInputs').on('click', '.add-btn', function () {
        addNewInputRow(false);
    });
    $('#dynamicInputs').on('click', '.remove-btn', function () {
        $(this).closest('.input-group').remove();
        calculateAmount(this);
    });
    $(document).ready(function () {
        // Event delegation for dynamic elements
        $('#dynamicInputs').on('input change', '.labourcount, .labour_worktype', function () {
            calculateAmount(this);
        });


    });



    function setLabourId(element) {
        let labourId = $(element).find('option:selected').val();
        $(element).closest('.input-group').find('.labour_worktype').val(labourId);
        calculateAmount(element); // Use element instead of this
    }

    function calculateAmount(element) {
        let count = parseFloat($(element).closest('.input-group').find('.labourcount').val());
        console.log("Count: ", count);

        let rate = parseFloat($(element).closest('.input-group').find('.labour_worktype option:selected').data('rate'));
        console.log("Rate: ", rate);

        if (isNaN(count) || isNaN(rate)) {
            alert("Invalid input. Please enter valid numbers.");
            return;
        }

        let amount = count * rate;
        console.log("Amount: ", amount);

        $(element).closest('.input-group').find('.labour_total_amount').val(amount);
        updateTotalAmount();
    }


    $(document).on('blur', '.labour_count', function () {
        var count = $(this).val();
        var rate = $(this).data('rate');
        var amount = rate * count;
        $(this).closest('.row').find('.labour_total_amount').val(amount.toFixed(2));
        updateTotalAmount();
    });
    $(document).on('blur', '.labour_count', function () {
        var worktype = $(this).closest('.row').find('.labour_type').val();
        let $this = $(this);
        let project_id = $("#Dailyreport_projectid").val();
        
        var postData={
            worktype:worktype,
            project:project_id
        }
        $.ajax({
             url: '<?php echo Yii::app()->createAbsoluteUrl('dailyReport/estimateBal'); ?>',
             method:"POST",
             data:postData,
             dataType:"json",
             "success":function(response){
                 $this.closest('.row').find('.labour_count').siblings('.error_count').html(response.msg);

             },
             error: function(xhr, status, error) {
                console.error("AJAX error:", error);
                $this.closest('.row').find('.error_count').html("An error occurred.");
            }

        });
       
       
        
    });
    function updateTotalAmount() {
        var totalAmount = 0;
        $('.labour_total_amount').each(function () {
            var amount = $(this).val() || 0;
            totalAmount += parseFloat(amount);
            $("#Dailyreport_amount").val(totalAmount.toFixed(2));
        })
    }
    $(document).on('click', '.approve', function (e) {
        e.preventDefault();

        var element = $(this);
        var item_id = $(this).attr('id');
        var includeInReport = confirm("Whether this entry should be added to project cost? ");
        var includeInReportValue = includeInReport ? 1 : 0;
        // alert(includeInReportValue);
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('dailyReport/approve'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                item_id: item_id,
                includeInReportValue: includeInReportValue,
            },
            success: function (response) {
                // alert('hi');
                if (response.response == 'success') {
                    $(".approveoption_" + item_id).hide();
                    element.closest("tr").removeClass("approve_section");
                    element.closest("tr").addClass("tobe_approve");
                    element.closest("tr").removeClass("row_class");
                    $("#errormessage").show()
                        .html('<div class="alert alert-success"><strong>Success!</strong>' + response.msg + '</div>')
                        .fadeOut(5000, function () {
                            location.reload();
                        });
                } else if (response.response == 'warning') {

                    var warningAlert = '<div class="alert alert-warning alert-dismissible" role="alert">' +
                           '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                               '<span aria-hidden="true">&times;</span>' +
                           '</button>' +
                           '<strong>Sorry!</strong> ' + response.msg +
                       '</div>';
                    $("#errormessage").html(warningAlert).show();
                } else {
                    $("#errormessage").show()
                        .html('<div class="alert alert-danger"><strong>Sorry!</strong> ' + response.msg + '</div>')
                        .fadeOut(5000);
                }
            }
        });
    });

    function validateDateSelection(cDate) {
        var selDate = cDate;
        var currDate = new Date();
        currDate = $.format.date(currDate, "dd-MM-yyyy");
        if (selDate == currDate) {
            $("a#next").css("pointer-events", "none").css("color", "#ccccc");
        } else {
            $("a#next").css("pointer-events", "visible").css("color", "#337ab7");
        }
        $("#btnReset").click();
    }

    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
    $("#overtime").on("change", function () {
        if ($(this).is(":checked")) {
            $(this).val("1"); // Set value to 1 if checked
        } else {
            $(this).val("0"); // Set value to 0 if unchecked
        }
    });
</script>