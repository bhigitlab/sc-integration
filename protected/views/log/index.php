<?php
/* @var $this LogController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Jp Logs',
);

$this->menu=array(
	array('label'=>'Create JpLog', 'url'=>array('create')),
	array('label'=>'Manage JpLog', 'url'=>array('admin')),
);
?>

<h1>Jp Logs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
