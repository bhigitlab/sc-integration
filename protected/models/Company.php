<?php

/**
 * This is the model class for table "{{company}}".
 *
 * The followings are the available columns in table '{{company}}':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $address
 * @property string $pincode
 * @property string $phone
 * @property string $email_id
 * @property string $company_gstnum
 * @property double $company_tolerance
 * @property integer $company_popermission
 * @property integer $created_by
 * @property string $created_date
 * @property integer $company_updateduration
 * @property double $purchaseorder_limit
 */
class Company extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jp_company';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
            return array(
                array('name, description, address, pincode, phone, email_id, company_gstnum, company_tolerance,po_email_userid, subco_email_userid, subcontractor_limit, created_by, created_date,expense_calculation', 'required'),
                array('company_popermission, subcontractor_limit, auto_purchaseno, company_updateduration, created_by,sc_bill_format', 'numerical', 'integerOnly' => true),
                array('company_tolerance, purchase_amount, pincode, phone, purchaseorder_limit', 'numerical'),
                array('name', 'length', 'max' => 500),
                array('pincode', 'length', 'max' => 30),
                array('email_id', 'length', 'max' => 100),
                array('email_id', 'email'),
				array('name', 'unique'),
                array('company_gstnum', 'match', 'pattern' => '{^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$}',
                    'message' => 'Invalid GST number'),
                array('phone', 'match', 'pattern' => '{^\+?[0-9-]+$}',
                    'message' => 'Invalid phone number'),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array('id, name, description, address, pincode, phone, email_id, company_gstnum, company_tolerance, company_popermission,po_email_userid, subco_email_userid, subcontractor_limit, auto_purchaseno, created_by, created_date, purchase_amount, expenses_email, expenses_percentage, invoice_email_userid, purchaseorder_limit,invoice_format,sc_quotation_format,purchase_order_format,material_requisition_fomat,warehouse_receipt_format,expense_calculation', 'safe', 'on' => 'search'),
            );
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'description' => 'Description',
			'address' => 'Address',
			'pincode' => 'Pincode',
			'phone' => 'Phone',
			'email_id' => 'Email',
			'company_gstnum' => 'Company GST No',
			'company_tolerance' => 'Company Tolerance (%)',
			'company_popermission' => 'Company PO Permission',
			'po_email_userid' => 'PO Email Notification',
			'subco_email_userid' => 'Subcontractor Email Notification',
			'subcontractor_limit' => 'Subcontractor Payment Limit (%)',
                        'auto_purchaseno' => 'Auto Purchase NO',
                        'purchase_amount' => 'Purchase Amount',
                        'company_updateduration' => 'Payment Update Duration',
                        'purchaseorder_limit' => 'PO Limit without Permission',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
                        'invoice_email_userid' => 'Invoice Email Notification',
						'logo' => 'Logo',
						'bank_details' => 'Bank Details',
						'invoice_format'=> 'Invoice Format',
						'expense_calculation'=>'Expense Calculation'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('pincode',$this->pincode,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email_id',$this->email_id,true);
		$criteria->compare('company_gstnum',$this->company_gstnum,true);
		$criteria->compare('company_tolerance',$this->company_tolerance);
		$criteria->compare('company_popermission',$this->company_popermission);
		$criteria->compare('po_email_userid',$this->po_email_userid,true);
		$criteria->compare('subco_email_userid',$this->subco_email_userid,true);
		$criteria->compare('subcontractor_limit',$this->subcontractor_limit,true);
                $criteria->compare('auto_purchaseno',$this->auto_purchaseno,true);
                $criteria->compare('purchase_amount',$this->purchase_amount,true);
                $criteria->compare('expenses_email',$this->expenses_email,true);
                $criteria->compare('expenses_percentage',$this->expenses_percentage,true);
                $criteria->compare('invoice_email_userid',$this->invoice_email_userid);
                $criteria->compare('company_updateduration',$this->company_updateduration);
                $criteria->compare('purchaseorder_limit',$this->purchaseorder_limit);
				$criteria->compare('invoice_format',$this->invoice_format);
				$criteria->compare('sc_quotation_format',$this->sc_quotation_format);
				$criteria->compare('purchase_order_format',$this->purchase_order_format);
				$criteria->compare('material_requisition_fomat',$this->material_requisition_fomat);
				$criteria->compare('warehouse_receipt_format',$this->warehouse_receipt_format);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>false,
		));
	}
	
	public function checkboxlistoptions($attribute, $params) {
	    if(($this->po_email_userid) === null) {
		  $this->addError("po_email_userid", ("Select User"));
	    }
	    if(($this->subco_email_userid) === null) {
		  $this->addError("subco_email_userid", ("Select User"));
	    }
	 }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Company the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
