<?php
/* @var $this ExpenseTypeController */
/* @var $model ExpenseType */

$this->breadcrumbs=array(
	'Expense Types'=>array('index'),
	$model->type_id=>array('view','id'=>$model->type_id),
	'Update',
);
?>
<div class="entries-wrapper">
    <div class="row">
                <div class="col-xs-12">
                    <div class="heading-title">Update Expense Head</div>
                    <div class="dotted-line"></div>
                </div>
    </div>
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
