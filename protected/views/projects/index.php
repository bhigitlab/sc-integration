<?php
/* @var $this ProjectsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Projects',
);

if(yii::app()->user->role<=2){
	$this->menu = array(
	   // array('label' => 'Create Projects', 'url' => array('create')),
	   // array('label' => 'Manage Projects', 'url' => array('admin')),
	);
}
?>

<h1>Projects</h1>



<div style="width: 40%;float:left;margin-right: 30px;">
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'projects-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'Sl.No.','htmlOptions' => array('width' => '40px','style'=>'font-weight: bold;text-align:center'),),
       // array('name'=>'pid', 'htmlOptions' => array('width' => '40px','style'=>'font-weight: bold;text-align:center')),  
      array(
        'name'  => 'name',
        'value' => 'CHtml::link($data->name, Yii::app()
 ->createUrl("projects/view",array("id"=>$data->pid)))',
        'type'  => 'raw',
    ),
      
        array(
            'name' => 'status',
            'value' => '$data->status0->caption',
            'type' => 'raw',
            'filter' => CHtml::listData(Status::model()->findAll(
                            array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="active_status"',
                                'order' => 'status_type',
                                'distinct' => true
                    )), "sid", "caption")
        ),
		//////created by arun on 14-10-16 starting from here
		array(            
            'header' => 'Assigned To',
            'type' => 'html',
            'filter' => false,			
            'value' => function($data, $row) {
                $pid = $data->pid;
                return $data->getAssignedusers($pid);
            },
        ),
		//////created by arun on 14-10-16 ends here
        array(
            'header' => 'Credit',
            'value' => '"&#2352; ".floatval(Expenses::model()->find(
                            array(
                                "select" => array("sum(amount) as amount"),
                                "condition" => "type=\"72\" AND projectid=$data->pid"
                    ))->amount)',
            'type' => 'raw',
            'htmlOptions' => array('width' => '40px', 'style' => 'border:1px solid #c2c2c2;font-weight: bold;text-align:left')
        ),
        array(
            'header' => 'Debit',
            'value' => '"&#2352; ".floatval(Expenses::model()->find(
                            array(
                                "select" => array("sum(amount) as amount"),
                                "condition" => "type=\"73\" AND projectid=$data->pid"
                    ))->amount)',
            'type' => 'raw',
            'htmlOptions' => array('width' => '40px', 'style' => 'border:1px solid #c2c2c2;font-weight: bold;text-align:left')
        ),
       
        /*
          'description',
          'created_date',
          'created_by',
          'updated_date',
          'updated_by',
         */
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
</div>

<div style="border:1px solid #c2c2c2;padding:10px 20px;float:left;width:700px">
<h3>Add New Project</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model2)); ?>

</div>