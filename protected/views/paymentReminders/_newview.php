<?php
/* @var $this ProjectsController */
/* @var $data Projects */
?>


<?php 
if ($index == 0) {
    ?>

    <thead>
        <tr>
            <th style="width:40px;">Sl No.</th>
            <th>Company</th>
            <th>Client</th>
            <th>Project</th>
            <th>Amount</th>
            <th>Date</th>            
            <th>Status</th>
            <th>Description</th>
            <th></th>
            
        </tr>   
    </thead>
            <?php
        }
        ?>
        
        <tr>
            <td><?php echo $index+1; ?></td>
            <td>
                <?php 
                $company = Company::model()->findByPk($data->company_id);
                echo $company['name'];
                ?>
            </td>
            <td>
                <?php 
                $client = Clients::model()->findByPk($data->client_id);
                echo $client['name'];
                ?>
            </td>
            <td>
                <?php
                $project = Projects::model()->findByPk($data->project_id);
                echo $project['name'];
                ?>
            </td>
            <td><?php echo $data->amount ?></td>
            <td><?php echo date('d-m-Y',strtotime($data->date)); ?></td>
            <td><?php echo $data->status ?></td>
            <td><?php echo $data->description ?></td>
            <td> 
                <span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                    <div class="popover-content hide">
                        <ul class="tooltip-hiden">
                <?php
                if((in_array('/paymentReminders/addcomment', Yii::app()->user->menuauthlist))){
                    $current_date = date('Y-m-d');
                    if($data->status == 'Open') {
                        if($data->date <= $current_date) {
                ?>
                  <li>
                   <?php if($data->viewed_status == 0 && ($data->date == $current_date)){ ?>
                  <button  id="<?php echo $data->id; ?>" class="add_remark btn btn-xs btn-default" onclick="ignore(<?php echo $data->id; ?>,event)" title="Ignore">Ignore</button>
                  <?php }?>
                       
                </li>
                <li>
                <button  id="<?php echo $data->id; ?>" class="add_remark btn btn-xs btn-default" onclick="addremark(<?php echo $data->id; ?>,event)" title="View">Reminder</button>
                 </li> 
                 
                    <?php } } else{ ?>
                      
                            <li>  
                <button  id="<?php echo $data->id; ?>" class="add_remark btn btn-xs btn-default" onclick="addremark(<?php echo $data->id; ?>,event)" title="View">Reminder</button>        
                </li>
                    <?php }
                    } ?>
                             
                             
                <?php
                if((in_array('/paymentReminders/update', Yii::app()->user->menuauthlist))){
                    if($data->status == 'Open' && $data->date >= $current_date) {
                ?>
                <li>
                <button  id="<?php echo $data->id; ?>" class="edit_remark btn btn-xs btn-default" onclick="editremark(<?php echo $data->id; ?>,event)" title="Edit">Edit</button>
               </li>
                <?php } } ?>
                <li>
                <button  id="<?php echo $data->id; ?>" class="delete_remark btn btn-xs btn-default" onclick="deleteRemainder(<?php echo $data->id; ?>,event)" title="Delete">Delete</button>
                 </li> 
                             
                             </ul>
                    </div>
           </td>            
        </tr>
        
		

