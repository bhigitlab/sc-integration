<!--<link rel="stylesheet" type="text/css" href="uploads/new_template/css/newstyle.css" />-->

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript"
    src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<?php if (isset($_GET['exportpdf'])) { ?>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
<?php } ?>

<script>
    $(function () {
        $("#datepicker").datepicker({ dateFormat: 'dd-mm-yy' }).datepicker("setDate", new Date());
    });
</script>
<div class="container">
    <div class="invoicemaindiv">
        <div class="header-container">
            <h3 class="<?php echo isset($_GET['exportpdf']) ? "text-center" : "purchase-title" ?>">Quotation</h3>
            <div class="btn-container">
                <?php
                if (!isset($_GET['exportpdf'])) { ?>
                    <a id="download" class="excelbtn1 pdf_excel btn btn-info" title="SAVE AS EXCEL">
                        <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>
                    </a>
                    <?php echo CHtml::link('<i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>', Yii::app()->request->requestUri . "&exportpdf=1", array('class' => 'pdf_excel btn btn-info', 'title' => 'SAVE AS PDF'));
                } ?>
            </div>
        </div>
        <form id="pdfvals1" method="post" action="">
            <input type="hidden" name="purchaseview">
            <?php if (!isset($_GET['exportpdf'])) { ?>
                <div class="page_filter">
                    <div class="row">
                        <div class="col-md-3">
                            <label>COMPANY : </label>
                            <?php
                            $company = Company::model()->findByPk($model->company_id);
                            echo $company['name']; ?>
                        </div>
                        <div class="col-md-3">
                            <label>CLIENT : </label>
                            <?php echo isset($client->name) ? $client->name : ''; ?>
                        </div>
                        <div class="col-md-3">
                            <label>PROJECT : </label>
                            <?php echo isset($project->name) ? $project->name : ''; ?>
                        </div>
                        <div class="col-md-3">
                            <label>DATE : </label>
                            <?php echo isset($model->date) ? date('d-m-Y', strtotime($model->date)) : ''; ?>
                        </div>
                        <div class="col-md-3">
                            <label>Quotation No : </label>
                            <?php echo isset($model->inv_no) ? $model->inv_no : ''; ?>
                        </div>
                        <div class="col-md-2">
                            <label>Total Project Cost : </label>
                            <?php echo isset($model->total_project_cost) ? $model->total_project_cost : '0.00'; ?>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <table border="0" class="details" style="margin:0px 30px">
                    <thead>
                        <tr>
                            <td>COMPANY :
                                <b>
                                    <?php
                                    $company = Company::model()->findByPk($model->company_id);
                                    echo $company['name']; ?>
                                </b>
                            </td>

                            <td>CLIENT :
                                <b>
                                    <?php echo isset($client->name) ? $client->name : ''; ?>
                                </b>
                            </td>

                            <td>PROJECT :
                                <b>
                                    <?php echo isset($project->name) ? $project->name : ''; ?>
                                </b>
                            </td>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <td>DATE :
                                <b>
                                    <?php echo isset($model->date) ? date('d-m-Y', strtotime($model->date)) : ''; ?>
                                </b>
                            </td>

                            <td>Quotation No :
                                <b>
                                    <?php echo isset($model->inv_no) ? $model->inv_no : ''; ?>
                                </b>
                            </td>

                            <td>Total Project Cost :
                                <b>
                                    <?php echo isset($model->total_project_cost) ? $model->total_project_cost : '0.00'; ?>
                                </b>
                            </td>
                        </tr>
                    </thead>
                </table>

            <?php } ?>
            <h4>Payment Stages</h4>
            <div class="table-responsive">
                <table border="1" class="table" <?php echo isset($_GET['exportpdf']) ? 'style="margin:0px 30px"' : '' ?>>
                    <thead>
                        <tr>
                            <th>Sl Number</th>
                            <th>Stage</th>
                            <th>Stage Percentage</th>
                            <th>Amount</th>
                            <th>Invoice Status </th>
                            <?php
                            if (!isset($_GET['exportpdf'])) { ?>
                                <th>Action </th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $payment_stage = Yii::app()->db->createCommand("SELECT * FROM `jp_payment_stage` WHERE `quotation_id` = " . $_GET['qtid'] . "  ORDER BY `serial_number` ASC ")->queryAll();
                        foreach ($payment_stage as $key => $value) {

                            ?>
                            <tr>
                                <td>
                                    <?php echo $value['serial_number']; ?>
                                </td>
                                <td>
                                    <?php echo $value['payment_stage']; ?>
                                </td>
                                <td>
                                    <?php echo number_format($value['stage_percent'], 2, '.', ''); ?>
                                </td>
                                <td class="text-right">
                                    <?php echo $value['stage_amount']; ?>
                                </td>
                                <td class="text-center">
                                    <?php echo ($value['invoice_status'] == 0) ? 'Pending' : 'Invoice Generated'; ?>
                                </td>
                                <?php
                                if (!isset($_GET['exportpdf'])) { ?>
                                    <td>
                                        <?php if ($value['invoice_status'] == 0) { ?>
                                            <span class="icon icon-options-vertical popover-test" data-toggle="popover"
                                                data-placement="left" type="button" data-html="true"
                                                style="cursor: pointer;"></span>
                                            <div class="popover-content hide">
                                                <ul class="tooltip-hiden">



                                                    <?php
                                                    if ((isset(Yii::app()->user->role) && (in_array('/quotation/converttoinvoice', Yii::app()->user->menuauthlist)))) {
                                                        ?>
                                                        <li>
                                                            <a href="index.php?r=invoice/invoicelumpsum&stageId=<?php echo $value['id'] ?>&quotationId=<?php echo $value['quotation_id'] ?>"
                                                                class=" btn btn-xs btn-default convert_invoice"
                                                                qtid="<?php echo $value['quotation_id'] ?>" target="_blank">Convert To
                                                                Invoice</a>
                                                        </li>
                                                    <?php } ?>
                                                    <?php if ($value['delete_approve_status'] == 0) { ?>
                                                        <li>
                                                            <button class="btn btn-xs btn-default deletePaymentStage"
                                                                data-id="<?php echo $value['id'] ?>">Delete</button>

                                                        </li>
                                                    <?php } ?>


                                                </ul>
                                            </div>
                                        <?php } ?>
                                    </td>
                                <?php } ?>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
</div>
<script>
    $(function () {

        $('.pdfbtn1').click(function () {
            $("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('quotation/SaveQuotation', array('qid' => $quotation_id)); ?>");
            $("form#pdfvals1").submit();

        });

        $('.excelbtn1').click(function () {
            $("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('quotation/ExportQuotation1', array('qid' => $quotation_id)); ?>");
            $("form#pdfvals1").submit();

        });


    });
    $(document).ready(function () {

        $(".popover-test").popover({
            html: true,
            content: function () {
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function (e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function (e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function (e) {
            $('[data-toggle=popover]').each(function () {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
        $(document).ajaxComplete(function () {
            $(".popover-test").popover({
                html: true,
                content: function () {
                    return $(this).next('.popover-content').html();
                }
            });

        });
        $(".js-example-basic-single").select2();
        $(".project").select2();
        $(".client").select2();

    });

</script>



<script>
    (function () {
        var mainTable = document.getElementById("main-table");
        if (mainTable !== null) {
            var tableHeight = mainTable.offsetHeight;
            if (tableHeight > 380) {
                var fauxTable = document.getElementById("faux-table");
                document.getElementById("table-wrap").className += ' ' + 'fixedON';
                var clonedElement = mainTable.cloneNode(true);
                clonedElement.id = "";
                fauxTable.appendChild(clonedElement);
            }
        }
    })();
    $(document).on('click', '.deletePaymentStage', function (e) {

        var element = $(this);
        var stage_id = $(this).attr('data-id');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('quotation/deletePaymentStage'); ?>',
            type: 'POST',
            dataType: 'json',
            data: { id: stage_id },
            success: function (response) {
                $("#msg_box").html('<div class="alert alert-' + response.response + ' alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>' + response.msg + '</div>');
                setTimeout(function () {
                    window.location.reload(1);
                }, 1000);

            }
        });
    });

</script>