<?php
/* @var $this EquipmentsEntriesController */
/* @var $model EquipmentsEntries */

$this->breadcrumbs=array(
	'Equipments Entries'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List EquipmentsEntries', 'url'=>array('index')),
	array('label'=>'Create EquipmentsEntries', 'url'=>array('create')),
	array('label'=>'Update EquipmentsEntries', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EquipmentsEntries', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EquipmentsEntries', 'url'=>array('admin')),
);
?>

<h1>View EquipmentsEntries #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'project_id',
		'company_id',
		'subcontractor_id',
		'expensehead_id',
		'description',
		'status',
		'Date',
	),
)); ?>
