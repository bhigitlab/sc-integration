<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <link rel="icon" type="image/png" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png" />
        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style1.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />




        <title><?php echo CHtml::encode($this->pageTitle); ?> </title>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>



    </head>

    <body>
        <div class="container" id="page">
            <div id="header">
                <div id="logo">
                    <div style="float:left;"><?php echo CHtml::encode(Yii::app()->name); ?> (PMS)</div>

                    <?php
                    //Change user login from Direct admin user
                    if (isset(Yii::app()->user->mainuser_id) && isset(Yii::app()->user->mainuser_role) && (Yii::app()->user->mainuser_role == 1)) {
                        ?>
                        <div style="float:left;font-size:12px;"><div style="clear:both;font-size: 100% !important;margin-left:20px; padding:6px 10px;background-color:#E5F1F4; width:250px;border-radius:20px;">Change user session:
                                <?php
                                echo CHtml::dropDownList('shift_userid', '', CHtml::listData(Users::model()->findAll(array('condition' => 'status=0', 'order' => 'first_name ASC')), 'userid', 'first_name'), array('options' => array(Yii::app()->user->id => array('selected' => true)),
                                    'ajax' => array(
                                        'type' => 'POST', //request type
                                        'url' => CController::createUrl('users/usershift'), //url to call.
                                        'success' => 'js:function(data){
                                        location.reload();
                                    }',
                                        'data' => array('shift_userid' => 'js:this.value'),
                                )));
                                ?></div>
                            <?php if (Yii::app()->user->mainuser_id != Yii::app()->user->id) { ?>
                                <span style="color:red;font-style: italic;text-decoration: blink;margin-left:20px;">(Note: You are working on other user's session)</span>
                            <?php } ?></div>
                        <?php
                    }
                    if (isset(yii::app()->user->role)) {
                        ?>

                    <?php } ?>

                    <div style="float:right;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png" border="0" align='right' height="50" /></div>
                </div>

            </div><!-- header -->
            <br clear="all"/><br clear="all"/>
            <div id="mainmenu">
                <?php
                if (isset(Yii::app()->user->role)) {
                    ?>

                    <?php
                    $this->widget('zii.widgets.CMenu', array(
                        'items' => array(
                            array('label' => 'Home', 'url' => array('/expenses/list')),
                            array('label' => 'Entry Log', 'url' => array('/expenses/entrylog'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 2 OR Yii::app()->user->role == 3))),
                            array('label' => 'Manage All Expenses', 'url' => array('/expenses/admin'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role <= 2 OR Yii::app()->user->role == 2))),
                            array('label' => 'Manage All Withdrawals', 'url' => array('/withdrawals/admin'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 2))),
                            array('label' => 'Projects', 'url' => array('/projects/index'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role <= 2 OR Yii::app()->user->role == 2))),
                            array('label' => 'Users', 'url' => array('/users/index'), 'visible' => (isset(Yii::app()->user->role) && (Yii::app()->user->role == 1))),
                            array('label' => 'Login', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                            array('label' => 'Logout (' . (isset(Yii::app()->user->mainuser_username) ? Yii::app()->user->mainuser_username : '') . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
                        ),
                    ));
                    ?>

                    <?php
                }
                ?>

            </div><!-- mainmenu -->
            <div class="span-5 last">
                <div id="sidebar">
<?php
$this->beginWidget('zii.widgets.CPortlet', array(
    'title' => '',
));
$this->widget('zii.widgets.CMenu', array(
    'items' => $this->menu,
    'htmlOptions' => array('class' => 'operations'),
));
$this->endWidget();
?>
                </div><!-- sidebar -->
            </div>

<?php echo $content; ?>

            <div class="clear"></div>

            <div id="footer">
                Copyright &copy; <?php echo date('Y'); ?> by <a href="http://www.bluehorizoninfotech.com">Blue Horizon Infotech</a>, Cochin.<br/>
                All Rights Reserved.<br/>
            </div><!-- footer -->

        </div><!-- page -->
    </body>
</html>
