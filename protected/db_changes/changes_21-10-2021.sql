ALTER TABLE `jp_quotation_item_master` ADD `additional_image` VARCHAR(250) NULL AFTER `image`;

ALTER TABLE `jp_sales_quotation` ADD `image` VARCHAR(500) NULL AFTER `mrp`;