<?php
/* @var $this DailyWorkTypeController */
/* @var $model DailyWorkType */

$this->breadcrumbs=array(
	'Daily Work Types'=>array('index'),
	$model->wtid,
);

$this->menu=array(
	array('label'=>'List DailyWorkType', 'url'=>array('index')),
	array('label'=>'Create DailyWorkType', 'url'=>array('create')),
	array('label'=>'Update DailyWorkType', 'url'=>array('update', 'id'=>$model->wtid)),
	array('label'=>'Delete DailyWorkType', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->wtid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DailyWorkType', 'url'=>array('admin')),
);
?>

<h1>View DailyWorkType #<?php echo $model->wtid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'wtid',
		'work_type',
		'company_id',
		'no_of_labour_label',
		'wage_label',
		'wage_rate_label',
		'helper_label',
		'helper_labour_label',
	),
)); ?>
