<div>
    <?php
   
    $subcontractor = Subcontractor::model()->findByPk($model['subcontractor_id']);
    $project = Projects::model()->findByPk($model['project_id']);
    $expense_head =ExpenseType::model()->findByPk($model['expensehead_id']);
    
    ?>
    <div class="text-center">
        <h2>WORK CONTRACT ORDER</h2>
        <h2><?php echo $model["scquotation_no"]?></h2>
    </div>
    
    <table>                    
        <tr>
            <td width="30%">CONTRACTOR NAME</td>
            <td width="2%">: </td>
            <td><?php echo $subcontractor["subcontractor_name"]; ?></td>
        </tr>
        <tr>
            <td width="30%">ADHAR NUMBER</td>
            <td width="2%">: </td>
            <td><?php echo $subcontractor["adhar_no"]; ?></td>
        </tr>
        <tr>
            <td width="30%">ADDRESS</td>
            <td width="2%">: </td>
            <td><?php echo $subcontractor["address"]; ?></td>
        </tr>
        <tr>
            <td width="30%">PAN NUMBER</td>
            <td width="2%">: </td>
            <td><?php echo $subcontractor["pan_no"]; ?></td>
        </tr>
        <tr>
            <td width="30%">ACCOUNT DETAILS </td>
            <td width="2%">: </td>
            <td><?php echo $subcontractor["account_details"]; ?></td>
        </tr>
        
        <tr>
            <td width="30%">CONTACT NUMBER </td>
            <td width="2%">: </td>
            <td><?php echo $subcontractor["subcontractor_phone"]; ?></td>
        </tr>
        <tr>
            <td width="30%">GST NO </td>
            <td width="2%">: </td>
            <td><?php echo $subcontractor["gst_no"]; ?></td>
        </tr>
        <tr>
            <td width="30%">REF </td>
            <td width="2%">: </td>
            <td><?php echo $model["scquotation_no"]?></td>
        </tr>
        <tr>
            <td width="30%">JOB NUMBER </td>
            <td width="2%">: </td>
            <td><?php echo $project["name"]; ?></td>
        </tr>
        <tr>
            <td width="30%">PROJECT IN CHARGE </td>
            <td width="2%">: </td>
            <td> <?php echo $project["incharge"]; ?></td>
        </tr>
        <tr>
            <td width="30%">SCOPE OF WORK </td>
            <td width="2%">: </td>
            <td><?php echo $expense_head["type_name"]; ?></td>
        </tr>
    </table>
</div>

<style>
td{
    font-size:18px;
}
</style>