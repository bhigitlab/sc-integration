<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Project Templates',
);
$page = Yii::app()->request->getParam('Project Templates_page');
if ($page != '') {
    Yii::app()->user->setReturnUrl($page);
} else {
    Yii::app()->user->setReturnUrl(0);
}
?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container" id="project">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix">
        <div class="add-btn pull-right">
            <?php
            if (isset(Yii::app()->user->role) && (in_array('/projectTemplate/create', Yii::app()->user->menuauthlist))) {
            ?>
                <button class="createtemplate btn btn-primary">Add PDF Layout</a>                    
            <?php } ?>
        </div>
        <h2 class="pull-left">PDF Layouts</h2>
    </div>
    <div id="errMsg"></div>
    <?php 
        foreach (Yii::app()->user->getFlashes() as $key => $message) {
            $alertType = ($key == 'error') ? 'danger' : 'success';
            echo '<div class="flash-' . $key . ' alert alert-' . $alertType . ' flash_msg" style="width:335px;">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>' . $message . "</div>\n";
        }
        ?>
    <div id="projecttemplateform" style="display:none;"></div>
    <div class="">
        <?php
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_newview', 'template' => '<div class=""><table cellpadding="10" class="table" id="clientab">{items}</table></div>',
            'ajaxUpdate' => false,
        ));
        ?>
        <?php
        Yii::app()->Clientscript->registerScript('myjavascript', '
             $(document).ready(function(){
               $("#clientab").dataTable( {
                    "scrollY": "300px",
                    "scrollCollapse": true,
                    
                     "columnDefs"        : [       
                        { 
                            "searchable"    : false, 
                            "targets"       : [0,3] 
                        },
                        { "bSortable": false, "aTargets": [-1]  }
                    ],
                } );
                });

            ');
        ?>

        <script>
            jQuery(function($) {
                $('#projecttemplate').on('keydown', function(event) {
                    if (event.keyCode == 13) {
                        $("#ProjectTemplatesearch").submit();
                    }
                });
            });

            function closeaction() {
                $('#projecttemplateform').slideUp();
            }

            function editTemplate(projectTemplate_id,ptype='quotation') {
                $('.loading-overlay').addClass('is-active');
                var id = projectTemplate_id;
                $.ajax({
                    type: "GET",
                    url: "<?php echo $this->createUrl('ProjectTemplate/update&layout=1&id=') ?>" + id + "&type=" + ptype,
                    success: function(response) {
                        $('.loading-overlay').removeClass('is-active');
                        $("#projecttemplateform").html(response).slideDown();

                    },
                }); 

            };
            $(document).ready(function() {
                $('.createtemplate').click(function() {
                    $('.loading-overlay').addClass('is-active');
                    $.ajax({
                        type: "GET",
                        url: "<?php echo $this->createUrl('ProjectTemplate/create&layout=1') ?>",
                        success: function(response) {
                            $("#projecttemplateform").html(response).slideDown();
                        
                        }
                    });
                });
            });
            $(document).ajaxComplete(function() {
                console.log('ss');
                $('.loading-overlay').removeClass('is-active');
                $('#loading').hide();
            });

            $(".project_template_check").click(function () {
            var id = $(this).attr('data-id');
            //$(this).attr('checked', true);
           // $(this).parents(".template_row").siblings().find('.template_check').attr('checked', false);
            $.ajax({
                type: "GET",
                url: "<?php echo $this->createUrl('dashboard/changeProjectTemplate') ?>",
                data: {
                    "id": id,
                },
                success: function (response)
                {
                    location.reload();
                }
            });
        })

        </script>
    </div>
</div>



<style>
    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    .panel {
        border: 1px solid #ddd;
    }

    .panel-heading {
        background-color: #eee;
        height: 40px;
    }

    input[type="checkbox"] {
        margin: 11px 3px 0;
    }
</style>