<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<?php
if(isset($_GET['status']) && $_GET['status']==1){
    $inter_rev = $_GET['selected_revision'].$_GET['rev'];
    $sql = 'SELECT count(*) FROM `jp_quotation_section` '
            . ' WHERE qtn_id =' . $_GET['qid']
            . ' AND revision_no="' . $inter_rev . '"';
    $inter_rev_count = Yii::app()->db->createCOmmand($sql)->queryScalar();

    $latest_rev = 'REV-'.$_GET['rev'];
    $sql = 'SELECT count(*) FROM `jp_quotation_section` '
            . ' WHERE qtn_id =' . $_GET['qid']
            . ' AND revision_no="' . $latest_rev . '"';
    $latest_rev_count = Yii::app()->db->createCOmmand($sql)->queryScalar();

    
    ?>
    <input type="hidden" value="<?php echo $inter_rev ?>" data-count = "<?php echo $inter_rev_count ?>" class="inter-rev">
    <input type="hidden" value="<?php echo $latest_rev ?>" data-count = "<?php echo $latest_rev_count ?>" class="latest-rev">
<?php } ?>
<style>
    
    .table.qitem_table thead {
        background-color: transparent !important;
    }
    .table.qitem_table thead tr:first-child{
        background-color: #eee !important;
    }
    .table.qitem_table thead tr:nth-child(2) th{
        padding-bottom:0px !important;
        padding: 15px;
        border:none !important;
        background-color:transparent !important;
    }
    .table.qitem_table tbody tr td{
        border:none !important;
        padding: 20px 5px;
        padding-top: 10px;
    }
    .table.qitem_table tfoot tr th{
        background:none !important;
    }
    .fa:focus{
        border:none !important;
        box-shadow:none !important;
    }
    .subitem_detais{
        padding: 5px 10px;    
        box-shadow: 3px 3px 11px 7px #eee;
    }
    .position-relative{
        position:relative;
    }
    .toggle_btn{
        position: absolute;
        right: 0;
        top: 8px;
        width:120px;
        text-align:center;
        cursor: pointer;
    }
    .toggle_btn1{
        position: absolute;
        right: 40px;
        top: 12px;
        /* width:120px; */
        text-align:center;
        cursor: pointer;
    }
    .toggle-caret{
        font-size: 4rem;
        font-weight: bold;
        color: #3383ca;
    }
    
    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown::before {
        position: absolute;
        content: " \2193";
        top: 0px;
        right: -8px;
        height: 20px;
        width: 20px;
    }

    button#caret {
        border: none;
        background: none;
        position: absolute;
        top: 0px;
        right: 0px;
    }

    .invoicemaindiv th,
    .invoicemaindiv td {
        padding: 10px;
    }

    .addcolumn {
        cursor: pointer;
        background-color: black;
        border: none;
        color: white;
        text-align: center;
        text-decoration: none;
        margin: 2px;
        font-size: 30px;
        width: 40px;
    }

    .invoicemaindiv .pull-right,
    .invoicemaindiv .pull-left {
        float: none !important;
    }
    .checkek_edit {
        pointer-events: none;
    }
    .add_selection {
        background: #000;
    }
    .toast-container {
        width: 350px;
    }

    .toast-position-top-right {
        top: 57px;
        right: 6px;
    }

    .toast-item-close {
        background-image: none;
        cursor: pointer;
        width: 12px;
        height: 12px;
        text-align: center;
        border-radius: 2px;
    }

    .toast-item-image {
        font-size: 24px;
    }
    .toast-item-close:hover {
        color: red;
    }
    .toast-item {
        border: transparent;
        border-radius: 3px;
        font-size: 10px;
        opacity: 1;
        background-color: rgba(34, 45, 50, 0.8);
    }
    .toast-item-wrapper p {
        margin: 0px 5px 0px 42px;
        font-size: 14px;
        text-align: justify;
    }
    .toast-type-success {
        background-color: #00A65A;
        border-color: #00A65A;
    }
    .toast-type-error {
        background-color: #DD4B39;
        border-color: #DD4B39;
    }
    .toast-type-notice {
        background-color: #00C0EF;
        border-color: #00C0EF;
    }

    .toast-type-warning {
        background-color: #F39C12;
        border-color: #F39C12;
    }
    .span_class {
        min-width: 70px;
        display: inline-block;
    }
    .purchase-title {
        border-bottom: 1px solid #ddd;
    }
    .purchase_items h3 {
        font-size: 18px;
        color: #333;
        margin: 0px;
        padding: 0px;
        text-align: left;
        padding-bottom: 10px;
    }
    .purchase_items h4 {
        margin: 0px 15px 10px 15px !important;
        line-height: 24px;
        background-color: #fafafa;
        font-size: 16px;
    }
    .purchase_items {
        padding: 15px;
        /* box-shadow: 0 0 13px 1px rgba(0, 0, 0, 0.25); */
        margin-bottom: 15px;
    }

    .purchaseitem {
        display: inline-block;
        margin-right: 20px;
    }

    .purchaseitem .form-control {
        height: 28px;
        display: inline-block;
        padding: 6px 3px;
    }

    .purchaseitem label {
        display: block;
    }

    .purchaseitem last-child {
        margin-right: 0px;
    }

    .purchaseitem label {
        font-size: 12px;
    }

    .remark {
        display: none;
    }

    .padding-box {
        padding: 3px 0px;
        min-height: 17px;
        display: inline-block;
    }

    th {
        height: auto;
    }

    .quantity,
    .rate,#qtynos {
        max-width: 80px;
    }

    .p_block {
        margin-bottom: 10px;
        padding: 0px 15px;
    }

    .gsts input {
        width: 50px;
    }

    .amt_sec {
        float: right;
    }

    .amt_sec:after,
    .padding-box:after {
        display: block;
        content: '';
        clear: both;
    }

    .client_data {
        margin-top: 6px;
    }

    #previous_details {
        position: fixed;
        top: 94px;
        left: 70px;
        right: 70px;
        z-index: 2;
        max-width: 1150px;
        margin: 0 auto;
    }

    .text_align {
        text-align: center;
    }

    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .tooltip-hiden {
        width: auto;
    }

    .form-fields {
        margin-bottom: 10px;
    }

    .row_field {
        margin-bottom: 15px;
    }

    .mr-3{
        margin-right:15px;
    }
    .qitem_table label{
        white-space:nowrap;
        font-size:10px;
    }
    .qitem_table input.form-control{
        font-size:10px;
    }
    @media(max-width: 767px) {
        .purchaseitem {
            display: block;
            padding-bottom: 5px;
            margin-right: 0px;
        }

        .quantity,
        .rate,
        .small_class {
            width: auto !important;
        }

        .p_block {
            margin-bottom: 0px;
        }

        .padding-box {
            float: right;
        }

        #tax_amount,
        #item_amount {
            display: inline-block;
            float: none;
            text-align: right;
        }
    }


    @media(min-width: 767px) {
        .invoicemaindiv .pull-right {
            float: right !important;
        }

        .invoicemaindiv .pull-left {
            float: left !important;
        }
    }

    .error_message {
        color: red;
    }

    a.pdf_excel {
        background-color: #6a8ec7;
        display: inline-block;
        padding: 8px;
        color: #fff;
        border: 1px solid #6a8ec8;
    }
    .bold{
        font-weight:bold;
    }
    .alert-msg-box {            
        width: 30%;
    }
</style>
<?php
/* @var $this SalesQuotationController */
/* @var $model SalesQuotation */

$this->breadcrumbs = array(
    'Sales Quotations' => array('index'),
    'Create',
);
?>
<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="flash-holder flash-success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<?php if (Yii::app()->user->hasFlash('error')) : ?>
    <div class="alert alert-danger  alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php endif; ?>
<?php
$main_items = $model->getCategories($model->id);
$qid = '';
if (isset($_GET['qid'])) {
    $qid = $_GET['qid'];
}
$rev = '';
if (isset($_GET['rev'])) {
    $rev = $_GET['rev'];
}
$status = '';
if (isset($_GET['status'])) {
    $status = $_GET['status'];
}
$activeProjectTemplate = $this->getActiveTemplate();
?>
<input type="hidden" value="<?php echo $activeProjectTemplate ?>" class="temp_type">
<div class="container">
    
    <!-- <div class="clearfix purchase-title">
        <h2 class="pull-left ">
            <?php //echo $model->isNewRecord ? 'Create' : 'Update' ?>  
             Quotation 
        </h2>
        <?php //echo CHtml::link('Quotations', array('salesQuotation/index'), array('class' => 'btn btn-primary pull-right mt')); ?>
    </div> -->
    <br>
    <div id="msg_box"></div>
    <div class="">
        
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
        <div id="main_div">
        
            <?php
           
            
            $file = 'qgeneratorform';
            $this->renderPartial($file, array(
                'model' => $model,
                'modelsales' => $modelsales,
                'user_companies' => $user_companies,
                'main_item_model' => $main_item_model,
                'main_item_master' => $main_item_master,
                'location_data' => $location_data,
                'salesquotation_edit' => $salesquotation_edit,
                'salesquotation_extra_edit' => $salesquotation_extra_edit,
                'salesquotation' => $salesquotation,
                'salesquotation_extra' => $salesquotation_extra,
                'main_sec_model' => $main_sec_model,
                'main_cat_model' => $main_cat_model,
                'main_wtype_model' => $main_wtype_model,
                'qid' => $qid,
                'rev' => $rev,
                'status' => $status
            ));
            ?>
        </div>
    </div>
</div>
<?php echo CHtml::hiddenField('master_id', $qid, array('id' => 'master_id')); ?>
<?php echo CHtml::hiddenField('revision_no', $rev, array('id' => 'revision_no')); ?>
<?php echo CHtml::hiddenField('status', $status, array('id' => 'status')); 
$item_count = 0;
if(isset($_GET['sub_item_id'])){
    $revisionNo = "REV-".$rev;
    $sql = "SELECT count(*) FROM `jp_sales_quotation` "
        . " WHERE `parent_id` = ".$_GET['sub_item_id'];
    $item_count = Yii::app()->db->createCommand($sql)->queryScalar();
    
}

?>

<input type="hidden" class="item_count" value="<?php echo $item_count?>">
<script>

    function countOfLetters(str) {
        var letter = 0;
        for (i = 0; i < str.length; i++) {
            if ((str[i] >= 'A' && str[i] <= 'Z')
                || (str[i] >= 'a' && str[i] <= 'z'))
                letter++;
        }            
        return letter;
    }

    $(document).find(".alert-msg-box").fadeOut( "slow" );

    //add section
    $(document).on('change', '.section_name', function () {               
        var $t = $(this).parents(".row");
        var section = $($t).find('.section_name').val(); 
        var sub_section = $($t).find('.section_name').val();        
        var revision_no = $("#revision_no").val();        
        var qid = $("#master_id").val();      
        var sec_id = $($t).find("#section_id").val();  
        if(sub_section == ''){
         var $subsection = $($t).find('.sub_section'); 
         $($subsection).siblings(".errorMessage").html("This field is mandatory");
            
        }          
        var status = "<?php echo isset($_GET['status'])?$_GET['status']:""?>";           
        var url = "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/createsection'); ?>";

        if (section != "") {                                
            $.ajax({
                url: url,
                data: {
                    section_name: section,
                    qtn_id: qid,                        
                    sec_id: sec_id,
                    revision_no: revision_no,
                    status:status,                       
                },
                type: "POST",
                dataType: 'json',
                success: function (result) {
                    $t.find("#section_id").val(result.id);
                    $(".add_new_sec").show();
                    $(".btn-preview").trigger("click");
                }
            })
        }else{
            $(this).siblings(".errorMessage").html("This field is mandatory");
        }        
    })

    $(document).on('change', '#sub_section', function () {         
        var $t = $(this).parents(".row");
        var sub_section_id = $($t).find('#sub_section_id').val();        
        var sub_section = $($t).find('#sub_section').val();   
        $(this).siblings(".errorMessage").html("");      
        var revision_no = $("#revision_no").val();        
        var status = "<?php echo isset($_GET['status'])?$_GET['status']:""?>";            
        var qid = $("#master_id").val();        
        var sec_id = $($t).find("#section_id").val(); 
                        
        var url = "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/createcategorylabel'); ?>";

        if (sub_section != "") {
            // alert(url);  return;                   
            
            $.ajax({
                url: url,
                data: {
                    category_label: sub_section,
                    qid: qid,
                    section_id: sec_id,                        
                    sec_id: sec_id,                    
                    sub_section_id: sub_section_id,
                    revision_no: revision_no,
                    status:status
                },
                type: "POST",
                dataType: 'json',
                success: function (result) {
                    $t.find("#sub_section_id").val(result.id);
                    $(".add_new_subsec").show();
                    $(".btn-preview").trigger("click"); 

                }
            })
        }else{
            $(this).siblings(".errorMessage").html("This field is mandatory");
        }        
    })

    $(document).on('click', '.add_subitem ', function () {       
        $(this).parents(".quotation_sec").find(".worktype_label_sec").slideToggle();
        // $(this).children("i").toggleClass("fa-plus fa-minus");
    })

//add worktype label

    $(document).on('change', '#worktype_label,#wrktype_unit,#wrktype_quantity,#wrktype_quantity_nos,#wrktype_mrp,#wrktype_amount_after_discount,#wrktype_desc,.QuotationGenWorktype_master_cat_id', function () {            
            var $t = $(this);
            var revision_no = $("#revision_no").val();
            var qid = $("#master_id").val();
            var sec_id = $(this).parents(".quotation_sec").find("#section_id").val();
            var cat_label_id = $(this).parents(".quotation_sec").find('#sub_section_id').val();
            var worktype_label = $(this).parents('.worktype_label_row').find("#worktype_label").val();            
            var master_cat_id = $(this).parents('.worktype_label_row').find(".QuotationGenWorktype_master_cat_id").val();            
            var unit = $(this).parents('.worktype_label_row').find("#wrktype_unit").val();
            var quantity = $(this).parents('.worktype_label_row').find("#wrktype_quantity").val();
            var quantity_nos = $(this).parents('.worktype_label_row').find("#wrktype_quantity_nos").val();
            var mrp = $(this).parents('.worktype_label_row').find("#wrktype_mrp").val();
            var amount_after_discount = $(this).parents('.worktype_label_row').find("#wrktype_amount_after_discount").val();            
            var worktype_label_id = $(this).parents('.worktype_label_row').find('#worktype_label_id').val();
            var description = $(this).parents('.worktype_label_row').find('#wrktype_desc').val();
            var url = "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/createworktypelabel'); ?>";


            if (worktype_label != "") {                
                $(this).siblings('.errorMessage').html("");                
                $.ajax({
                    url: url,
                    data: {
                        qid: qid,
                        section_id: sec_id,
                        category_label_id: cat_label_id,
                        worktype_label: worktype_label,
                        unit: unit,
                        quantity: quantity,
                        quantity_nos: quantity_nos,
                        mrp: mrp,
                        amount_after_discount: amount_after_discount,
                        master_cat_id: master_cat_id,
                        worktype_label_id: worktype_label_id,
                        description: description,
                        revision_no: revision_no

                    },
                    type: "POST",
                    dataType: 'json',
                    success: function (result) {
                        $t.siblings("#worktype_label_id").val(result.id);
                        var id = 'QuotationGenWorktype_master_cat_id' + result.id;
                        $t.parents('.worktype_label_row').find(".QuotationGenWorktype_master_cat_id").attr("id", id);                        
                        console.log("Sub items  Saved");
                        $(".btn-preview").trigger("click");
                    }
                })
            }else{
                $(this).siblings('.errorMessage').html("WorkType is Mandatory"); 
            }        
    })       
   
    $(document).on('change', '.QuotationGenWorktype_master_cat_id', function () {
        
        var workType = $(this).parents(".worktype_label_row").find("#worktype_label").val();
        if(workType !="" && ($(this).val() !=null && $(this).val() !== "")){            
            $t = $(this);
            var category_id = $(this).val();
            var category_label_id = $(this).parents(".row").find("#worktype_label_id").val();
            var qid = "<?php echo $qid ?>";
            var rev = "<?php echo $rev ?>";
            var status = "<?php echo $status ?>";
            var parent_type = '1';
            var parent_id = $(this).parents(".row").find("#worktype_label_id").val();
            var url = "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getquotationitem') ?>";           
            var item_count =  $(".item_count").val();
            
            var sec_id = '<?php echo isset($_REQUEST['sec_id'])?$_REQUEST['sec_id']:'' ?>'; 
            
            if(sec_id !="" && item_count >0){
               
                var url = "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getquotationitemforedit') ?>"; 
            }
            $.ajax({
                url: url,
                data: {
                    category_id: category_id,
                    category_label_id: category_label_id,
                    qid: qid,
                    rev: rev,
                    status: status,
                    parent_type: parent_type,
                    parent_id: parent_id

                },
                type: "POST",

                success: function (data) {
                    $t.parents('.worktype_label_row').find("#quotation_item").html(data);                    
                }
            })
        }else{ 
           if($(this).val() !=null && $(this).val() !== ""){         
                $(this).parents(".worktype_label_row").find("#worktype_label").siblings('.errorMessage').html("WorkType is Mandatory");            
            }
            $(this).val('');
        }
    })    

    $(document).on('click', '.additem', function () {
    var elem = $(this);        
    var type = 0;
            
    var cat_id =  $(this).parents("#section-form").find("#cat_id").val();            
    var master_id =  $(this).parents("#section-form").find("#master_id").val();
    var revision_no =  $(this).parents("#section-form").find("#revision_no").val();             
    var parent_type =  $(this).parents("#section-form").find("#parent_type").val();  
    var parent_id =  $(this).parents("#section-form").find("#parent_id").val();
    var subsec_id = $(this).parents(".quotation_sec").find("#sub_section_id").val();             
    var temp_type = $('.temp_type').val();        
    var status = "<?php echo isset($_GET['status']) ? $_GET['status'] : "" ?>";
    
    var submitData = $(this).parents("#section-form").find(".clients-form");
                    
    $(submitData).each(function(){
        var length = $(".length").val();
        var width = $(".width").val();
        var wrktype_quantity= $(".quantity").val();
        if(length !='' && width ==''){
            alert('Width is required when length is entered')
        }else if(length =='' && width !=''){
            alert('Length is required when width is entered');
        }
        if(wrktype_quantity == ''){
            alert('Quantity is required');
        }
        var serializedData = $(this).serialize();
        var params = new URLSearchParams(serializedData);

        $(".subitem-save").attr('disabled',true);
        var data = $(this).serializeArray();
        // console.log(data);return;
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/addquotationitemsDetails'); ?>',
            data: data,
            dataType: 'json',
            success: function (result) {
                $(".subitem-save").attr('disabled',false);
                $(".final_amount_after_additional").val(''); //reset additional discount if item changed                   
                $('html, body').animate({
                    scrollTop: $(document).find(".alert-msg-box").offset().top - 80
                }, 'slow'); 
                
                if(result.response == 'Error') {
                    $(document).find(".alert-msg-box").removeClass('alert-success').addClass('alert-danger').fadeIn(300).delay(5000).fadeOut(1000); 
                    $(document).find(".alert-msg").html(result.message); 
                } else if(result.response == 'Success') { 
                    if(result.partial_errors && result.partial_errors.length > 0) {
                        alert('Some of the items not got saved due to the missing of required fields');
                        $(document).find(".alert-msg-box").removeClass('alert-success').addClass('alert-warning').fadeIn(300).delay(5000).fadeOut(3000); 
                        $(document).find(".alert-msg").html('Some items were not saved due to missing fields: ' + result.partial_errors.join(', ')); 
                    } else {
                        $(document).find(".alert-msg-box").addClass('alert-success').removeClass('alert-danger').fadeIn(300).delay(5000).fadeOut(1000); 
                        $(document).find(".alert-msg").html('Items saved successfully'); 
                    }
                }
                $(elem).parents('.worktype_label_row').find("#quotation_item").html(""); 
                $('.QuotationGenWorktype_master_cat_id').val('')  
                $(elem).parents(".worktype_label_row").find("input").val("");
                $(elem).parents(".worktype_label_row").find("textarea").val("");
                $(elem).parents(".quotation_sec").find("#section_name,#sub_section").attr("readonly", true);
                $("#sgst_percent").change();
                $("#cgst_percent").change();
                $("#igst_percent").change();
                $(".btn-preview").trigger("click");
                setTimeout(() => {
                    $(".reset_subitem").trigger("click");
                }, 2000);
            }
        })    
    })                            
});


    function remoeOldData(master_id,revision_no,parent_type,parent_id,cat_id,subsec_id,type){
        
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/removeOldQuotationData'); ?>',
            data:{
                master_id:master_id,
                revision_no:revision_no,
                parent_type:parent_type,
                parent_id:parent_id,
                cat_id:cat_id ,
                subsec_id:subsec_id,
                type :type
            },
                
            success: function (data) {
                console.log("deleted successfully");                    
                    
            }
        })
         
    }

    $(document).on('click', '.btn_close', function () {
        $(this).parents("#section-form").find(".worktype_label_row").slideUp();
        $(this).parents("#section-form").siblings(".clearfix").find(".add_more_wtype").trigger("click");        
        $(this).parents(".panel-footer").hide();

    });

    $(document).on("click",".btn-preview",function(){
        var qtid = $(this).attr("data-qtid");
        var rev = $(this).attr("data-rev");
        var status = $(this).attr('data-status');
        var interRev = $(this).attr('data-inter-rev');
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/quotationPreview'); ?>',
            data: {
                qtid:qtid,
                rev:rev,
                status:status,
                interRev:interRev
            },            
            success: function (result) {
                console.log(result);
                $(".Item_details_table").html(result);
                $('.loading-overlay').removeClass('is-active');
                // $(document).find(".item_table tfoot tr:nth-child(2)").find("#sgst_percent,#cgst_percent,#igst_percent").trigger("change");
            }
        })
       
    })

    $(".panel-footer").hide();

    $(function(){        
        var status = "<?php echo isset($_GET['status']) ? $_GET['status'] : "" ?>";
        var qtid = "<?php echo isset($_GET['qid']) ? $_GET['qid'] : "" ?>";
        var rev = "<?php echo isset($_GET['rev']) ? $_GET['rev'] : "" ?>";
        if(status ==1){            
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/updateRevisionStatus'); ?>',
                data: {
                    qtid:qtid,
                    rev:rev
                },            
                success: function (result) {
                    console.log(result);                
                }
            })
        }
    })

    $(document).on("click",".updateRevision",function(){
        
        var rev = $(this).attr('data-rev');
        var qtid = $(this).attr('data-qid');
        var new_rev = "<?php echo isset($_GET['rev']) ? $_GET['rev'] : "" ?>";
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/updateInterRevision'); ?>',
            data: {
                qtid:qtid,
                rev:rev,
                new_rev:new_rev
            },            
            success: function (result) {
                console.log(result);                
            },
            complete: function (result) { 
                setTimeout(function () {
                    location.reload(true);
                },2000);               
            }
        })
    })

    $(document).on("click",".removeQItem",function(){
        
        var id = $(this).parent().attr('id');
        
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/removeQItem'); ?>',
            data: {
                id:id,                
            },            
            success: function (result) {
                console.log(result);                
            },
            complete: function (result) {
                $(".btn-preview").trigger("click");
                $(".updateRevision").trigger("click");
                setTimeout(function () {
                    location.reload(true);
                },1000);               
            }
        })
    })

    $(document).on("click",".restoreQItem",function(){
        
        var id = $(this).parent().attr('id');
        
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/restoreQItem'); ?>',
            data: {
                id:id,                
            },            
            success: function (result) {
                console.log(result);                
            },
            complete: function (result) {
                $(".btn-preview").trigger("click");
                $(".updateRevision").trigger("click");
                setTimeout(function () {
                    location.reload(true);
                },1000);               
            }
        })
    })

    $(document).on("click",".delete_sub",function(){
        var elem = $(this);
        var subsec_id = $(this).attr('data-id');
        var rev_no =$(this).attr('rev-no');
        var qtid = '<?php echo isset($_REQUEST['qid'])?$_REQUEST['qid']:'' ?>';
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/deleteSubSection'); ?>',
            data: {
                subsec_id:subsec_id,
                rev_no:rev_no  ,
                qtid:qtid              
            },            
            success: function (result) {
                console.log(result);                
            },
            complete: function (result) {
                setTimeout(function () {
                    $(elem).parents(".items_holder").remove();
                },1000);               
            }
        })
        // alert(subsec_id);alert(rev_no);
    })
    $(function () {
        var sec_id = '<?php echo isset($_REQUEST['sec_id'])?$_REQUEST['sec_id']:'' ?>'; 
        if(sec_id !=""){            
            $(".QuotationGenWorktype_master_cat_id").trigger("change");
            
           
        }

        
        

            $("#datepicker").datepicker({
                dateFormat: 'dd-mm-yy',
                maxDate: '0'
            }).datepicker("setDate", new Date());
        });
        $(document).on("click",".edit_sec",function(){
            var section_id = $(this).parent("th").attr("data-sec-id");
            var sub_section_id = $(this).parent("th").attr("data-subsec-id");
            var sub_item_id = $(this).parent("th").attr("data-id");
            var qtid = '<?php echo isset($_REQUEST['qid'])?$_REQUEST['qid']:'' ?>';
            var rev = '<?php echo isset($_REQUEST['rev'])?$_REQUEST['rev']:'' ?>';             
            var status = '<?php echo isset($_REQUEST['status'])?$_REQUEST['status']:'' ?>';
            var url = '&sub_section_id='+ sub_section_id+'&sub_item_id='+ sub_item_id;

            if(sub_section_id==""){
                var url = '';
            }else if(sub_section_id !="" && sub_item_id==""){
                var url = '&sub_section_id='+ sub_section_id;
            }


            location.href = '<?php echo $this->createUrl('create'); ?>&qid='+qtid+'&rev='+rev+'&status=0&sec_id='+ section_id+url;
            if(status==1) {
                var selected_revision = '<?php echo isset($_REQUEST['selected_revision'])?$_REQUEST['selected_revision']:'' ?>'
                location.href = '<?php echo $this->createUrl('create'); ?>&qid='+qtid+'&rev='+rev+'&status='+status+'&selected_revision='+selected_revision+'&sec_id='+ section_id+url;
            }
            $(".add_more_items").trigger("click");
            
        })

        $(document).on('change', '.worktype_label', function () {
        var element = $(this);
        var worktypelabel = $('option:selected', this).text();
        if ($(this).parents("#quotation_item").hasClass("sub_worktype_selector")) {
            var cat_id = element.parents(".worktype_label_row").find(".QuotationGenWorktype_master_cat_id").val();
        } else {
            var cat_id = element.parents("#items_holder").find(".QuotationGenCategory_master_cat_id").val();
        }

        var worktypeid = element.parent().siblings('.worktypedata').attr('data-id');
        var template_id = element.parent().siblings('.worktypedata').attr('data-template');
        if (worktypelabel != '') {
            element.parent().siblings().find('input[type=text],select').addClass('commonadd-val');
        } else {
            element.parent().siblings().find('input[type=text],select').removeClass('commonadd-val');
        }
        var id = element.parent().siblings('.worktypedata').attr('data-incremnt');

        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getdatasofworktype'); ?>',
            type: 'GET',
            dataType: 'json',
            data: {
                cat_id: cat_id,
                worktypelabel: worktypelabel,
                worktypeid: worktypeid,
                template_id: template_id
            },
            success: function (result) {

                if (result.status == 1) {
                    $(element).parents("tr").find('#hiddenitem' + id).val(result.item_id);
                    $('#previous_details').html(result.html);
                    $("#previous_details").fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
                    // $("#pre_fixtable").tableHeadFixer();
                    getamount(element, id);
                } else {
                    $('#previous_details').html(result.html);
                    $("#previous_details").fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
                }

                if (template_id == 1) {
                   // $(element).parents("tr").find('#hiddenitem' + id).val(result.item_id);
                    $(element).parents("tr").find('#shutterwork_description' + id).val(result.shutterdesc);
                    $(element).parents("tr").find('#caracoss_description' + id).val(result.carcassdesc);
                    $(element).parents("tr").find('#shutterwork_material' + id).val(result.shuttermaterial);
                    $(element).parents("tr").find('#shutterwork_finish' + id).val(result.shutterfinish);
                    $(element).parents("tr").find('#carcass_material' + id).val(result.carcasmaterial);
                    $(element).parents("tr").find('#carcass_finish' + id).val(result.carcassfinish);
                    $(element).parents("tr").find('#description' + id).val(result.description);
                } else {
                    if (template_id == 3) {
                       // $(element).parents("tr").find('#hiddenitem' + id).val(result.item_id);
                        $(element).parents("tr").find('#material' + id).val(result.material);
                        $(element).parents("tr").find('#finish' + id).val(result.finish);
                    }
                    element.parents('tr').find('.description').val(result.description);

                }
            }
        })
    })

    $(document).on("click",".reset_subitem",function(){
        
        var section_id =$(this).parents(".sections_block").find("#section_id").val();
        console.log(section_id);
        var sub_section_id = $(this).parents(".sections_block").find("#sub_section_id").val();;
        var qtid = '<?php echo isset($_REQUEST['qid'])?$_REQUEST['qid']:'' ?>';
        var rev = '<?php echo isset($_REQUEST['rev'])?$_REQUEST['rev']:'' ?>'; 
        
        var status = '<?php echo isset($_REQUEST['status'])?$_REQUEST['status']:'' ?>';
        location.href = '<?php echo $this->createUrl('create'); ?>&qid='+qtid+'&rev='+rev+'&status=0&sec_id='+ section_id+'&sub_section_id='+ sub_section_id;

        if(status==1) {
            var selected_revision = '<?php echo isset($_REQUEST['selected_revision'])?$_REQUEST['selected_revision']:'' ?>'
            location.href = '<?php echo $this->createUrl('create'); ?>&qid='+qtid+'&rev='+rev+'&status='+status+'&selected_revision='+selected_revision+'&sec_id='+ section_id+'&sub_section_id='+ sub_section_id;
        }


         
            
    })

    $(document).on("click",".add_new_subsec",function(){       
        var section_id =$(this).parents(".row").find("#section_id").val();
        if(section_id==""){
            $(this).parents(".row").find("#section_id").siblings(".errorMessage").val("This field is required!");
        }else{
            $(this).parents(".row").find("#section_id").siblings(".errorMessage").val("");
            var qtid = '<?php echo isset($_REQUEST['qid'])?$_REQUEST['qid']:'' ?>';
            var rev = '<?php echo isset($_REQUEST['rev'])?$_REQUEST['rev']:'' ?>';
            var status = '<?php echo isset($_REQUEST['status'])?$_REQUEST['status']:'' ?>';            
            location.href = '<?php echo $this->createUrl('create'); ?>&qid='+qtid+'&rev='+rev+'&status=0&sec_id='+ section_id; 

            if(status==1) {
                var selected_revision = '<?php echo isset($_REQUEST['selected_revision'])?$_REQUEST['selected_revision']:'' ?>'
                location.href = '<?php echo $this->createUrl('create'); ?>&qid='+qtid+'&rev='+rev+'&status='+status+'&selected_revision='+selected_revision+'&sec_id='+ section_id;
            }
         
        }
         
    })
    $(document).on("click",".add_new_sec",function(){               
            var qtid = '<?php echo isset($_REQUEST['qid'])?$_REQUEST['qid']:'' ?>';
            var rev = '<?php echo isset($_REQUEST['rev'])?$_REQUEST['rev']:'' ?>'; 
            var status = '<?php echo isset($_REQUEST['status'])?$_REQUEST['status']:'' ?>';          
            location.href = '<?php echo $this->createUrl('create'); ?>&qid='+qtid+'&rev='+rev+'&status=0'; 

            if(status==1) {
                var selected_revision = '<?php echo isset($_REQUEST['selected_revision'])?$_REQUEST['selected_revision']:'' ?>'
                location.href = '<?php echo $this->createUrl('create'); ?>&qid='+qtid+'&rev='+rev+'&status='+status+'&selected_revision='+selected_revision;
            }
  
    })

$(function(){
    var inter_count = $(".inter-rev").attr("data-count");
    var latest_count = $(".latest-rev").attr("data-count");
    var is_quotationdata = <?php echo count($salesquotation); ?>;

    if(inter_count == 0 && latest_count ==0 ){                   
        var selected_revision = "<?php echo isset($_GET['selected_revision'])?$_GET['selected_revision']:""; ?>";
        var qid = "<?php echo isset($_GET['qid'])?$_GET['qid']:""; ?>";   
        var inter_rev = $(".inter-rev").val(); 
        var url = "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/saveInterRevision'); ?>";

        $.ajax({
            url: url,
            data: {            
                qtn_id: qid,
                selected_revision: selected_revision, 
                inter_rev:inter_rev           
            },
            type: "POST",
            dataType: 'json',
            success: function (result) {
                console.log("success");
                
            },
            complete: function(){
                setTimeout(function () {
                    if(is_quotationdata){
                        location.reload(true);
                    }
                },1000);
            }
        })
    }
})

$(document).on("click",".delete_row",function(){
    // if status 1 , save new revision and delete
    if(confirm("Are you sure you want to delete? This will also delete all sub-items if any")){
        var type = $(this).attr("data-type");
        var id = $(this).attr("data-id");
        $.ajax({
            url: "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/deleteQuotationData'); ?>",
            data: {            
                type: type,
                id: id,                       
            },
            type: "POST",
            dataType: 'json',
            success: function (result) {
                $(".final_amount_after_additional").val('');//reset additional discount if item changed
                console.log("success");                
            },
            complete: function(){
                $(document).find(".updateRevision").trigger("click");
                $(".add_new_sec").trigger("click");
                $(".btn-preview").trigger("click");
            }
        })
    }
})


        var count = 1;
        $(function () {
            $(".btn-preview").trigger("click");
            $('p#add_field').click(function () {
                count += 1;
                $('#container').append(
                        '<div class="row addRow row-margin-append">' +
                        '<div class="col-md-1">' +
                        '<label for="Clients_Shutter_Work">Extra Work</label>                </div>' +
                        '<div class="col-md-2">' +
                        '<input class="form-control" type="file" name="img" id="img_' + count + '" ><option value=""></input>      ' +
                        '           </div>' +
                        '<div class="col-md-2">' +
                        '<input class="form-control" id="descrition_' + count + '" name="description[]' + '" type="text" placeholder="description"></input>      ' +
                        '           </div>' +
                        '<div class="col-md-1">' +
                        '<select class="form-control" id="sq_feet_' + count + '" name="sq_feet[]' + '" type="text"><option value="">Sq Ft.</option></select>      ' +
                        '          </div>' +
                        '<div class="col-md-1">' +
                        '<input class="form-control" id="qty_' + count + '" name="qty[]' + '" type="text" placeholder="qty"></input>      ' +
                        '           </div>' +
                        '<div class="col-md-1">' +
                        '<input class="form-control" id="qtynos_' + count + '" name="qtynos[]' + '" type="text" placeholder="qty nos"></input>      ' +
                        '           </div>' +
                        '<div class="col-md-2">' +
                        '<input class="form-control" id="mrp_' + count + '" name="mrp[]' + '" type="text" placeholder="MRP"></input>      ' +
                        '           </div>' +
                        '<div class="col-md-2">' +
                        '<input class="form-control" id="amt_dis_' + count + '" name="amt_dis[]' + '" type="text" placeholder="amt after disc"></input>      ' +
                        '           </div>' +
                        '           </div>' +
                        '<br/>');
            });
        });

        $("#quotation-form :input").change(function () {
            var allRequired = true;
            var $inputs = $('#quotation-form :input').filter('.require');
            var values = {};
            $inputs.each(function () {
                if ($(this).val() == '') {
                    allRequired = false;
                }
                values[this.name] = $(this).val();
            });
            if (allRequired) {
                $("#quotation-form").submit();
            }
        });
        $("#category_label").change(function () {
            $category_label = $("#category_label").val();
            $("#cat_name").val($category_label);
        });

        $("#SalesQuotation_id").change(function () {
            $category_label = $("#category_label").val();
            $("#cat_name").val($category_label);
        });


        $(".company_id").select2();
        $(function () {
            $("#datepicker").datepicker({
                dateFormat: 'dd-mm-yy',
                maxDate: '0'
            }).datepicker("setDate", new Date());
        });

        function saveRevisionData() {
            var revision_no = "<?php echo isset($_GET['rev']) ? $_GET['rev'] : '' ?>";
            var qid = "<?php echo isset($_GET['qid']) ? $_GET['qid'] : '' ?>";

            var tax_slab = $('#add_tax_slab').val();
            var sgst_percent = $('#sgst_percent').val();
            var sgst_amount = $('#sgst_amount').text();


            var cgst_percent = $('#cgst_percent').val();
            var cgst_amount = $('#cgst_amount').text();

            var igst_percent = $('#igst_percent').val();
            var igst_amount = $('#igst_amount').text();
            var status = "<?php echo isset($_GET['status']) ? $_GET['status'] : "" ?>";

            var discount_percent = $('#discount_precent').val();
            var discount_amount = $('#discount_amount').text();
            var special_discount = $('.special_discount').val();

            var total_after_additionl_discount = $('.final_amount_after_additional').val().replace(/,/g, '');

            $.ajax({
                url: "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/saveRevisionData') ?>",
                data: {
                    qid: qid,
                    rev: revision_no,
                    tax_slab: tax_slab,
                    sgst_percent: sgst_percent,
                    sgst_amount: sgst_amount,
                    cgst_percent: cgst_percent,
                    cgst_amount: cgst_amount,
                    igst_percent: igst_percent,
                    igst_amount: igst_amount,
                    discount_percent:special_discount,
                    discount_amount:discount_amount,
                    total_after_additionl_discount:total_after_additionl_discount
                },
                type: "POST",
                success: function (data) {
                    console.log('Amount saved');
                }
            })
        }
        $(document).on("change",'#add_tax_slab',function () { 
            var tax_slab_value = $("#add_tax_slab").val();

            $("#sgst_percent").val(tax_slab_value/2);
            $("#cgst_percent").val(tax_slab_value/2);
            $('#sgst_percent,#cgst_percent').trigger('change');
            alert('Tax values updated successfully');
            location.reload(true);
        });
        $(document).on("change",'#sgst_percent',function () {    

            var sgst_percent = parseFloat($('#sgst_percent').val() ) || 0;
            var total_amount = parseFloat( $('#total_after_discount').val() ) || 0;            
            var totaltax = $('#total-tax').val();
            var cgst_amount = $('#cgst_amount').text();
            var igst_amount = $('#igst_amount').text();
            var discount_amount = isNaN($('#discount_amount').text()) ? 0.00 : $('#discount_amount').text();
            if (total_amount > 0) {
                var sgst_amount = (total_amount * sgst_percent) / 100;
                $('#sgst_amount').text(sgst_amount.toFixed(2)); 
                $('#cgst_amount').text(sgst_amount.toFixed(2));               
                saveRevisionData();
                // $(".item_table").load(location.href + " .item_table");
                $( ".updateRevision" ).trigger( "click" );
                // $(".btn-preview").trigger("click");
            } else {
                $('#sgst_amount').text('0.00');
            }
        });

        $(document).on("change",'#cgst_percent',function () {          
            var cgst_percent = parseFloat($('#cgst_percent').val()) || 0;
            var total_amount = parseFloat($('#total_after_discount').val() ) || 0; 
            var totaltax = $('#total-tax').val();
            var sgst_amount = $('#sgst_amount').text();
            var igst_amount = $('#igst_amount').text();
            var discount_amount = isNaN($('#discount_amount').text()) ? 0.00 : $('#discount_amount').text();
            if (total_amount > 0) {

                var cgst_amount = (total_amount * cgst_percent) / 100;
                $('#cgst_amount_hidden').val(cgst_amount.toFixed(2));
                $('#cgst_amount').text(cgst_amount.toFixed(2));
                
                saveRevisionData();
                $( ".updateRevision" ).trigger( "click" );
                // $(".btn-preview").trigger("click");
            } else {
                $('#cgst_amount').text('0.00');

            }
        });

        $(document).on("change",'#igst_percent',function () {          
            var igst_percent = $('#igst_percent').val();
            var total_amount = $('#total_after_discount').val(); 
            var totaltax = $('#total-tax').val();
            var sgst_amount = $('#sgst_amount').text();
            var cgst_amount = $('#cgst_amount').text();
            var discount_amount = isNaN($('#discount_amount').text()) ? 0.00 : $('#discount_amount').text();

            if (total_amount > 0) {
                var igst_amount = (total_amount * igst_percent) / 100;
                $('#igst_amount').text(igst_amount.toFixed(2));
                $('#igst_amount_hidden').val(igst_amount.toFixed(2));
                
                saveRevisionData();
                $( ".updateRevision" ).trigger( "click" );
                // $(".btn-preview").trigger("click");
            } else {
                $('#igst_amount').text('0.00');
            }
        });

        

        $("#total_after_additionl_discount").change(function(){
            saveRevisionData();
        })

        $(function(){
            $(".add_new_sec").hide();
            $(".add_new_subsec").hide();
            var sec_name = $(".section_name").val();
            if(sec_name !=""){
                $(".add_new_sec").show();
            }
            var sub_section = $("#sub_section").val();
            if(sub_section !=""){
                $(".add_new_subsec").show();
            }
        })
        
        $(document).on("blur",".special_discount",function(){
            saveRevisionData();
            $( ".updateRevision" ).trigger( "click" );
            $(".btn-preview").trigger("click");
        })

        $(document).on("blur",".final_amount_after_additional",function(){
            var total_after_additionl_discount = $(this).val();            
            var total_before_additional_discount = $(".total_before_additional_discount").text().replace(/,/g, '');
            if(total_before_additional_discount < total_after_additionl_discount){
                $(this).val(0)
                return;
            }
            saveRevisionData();
            $( ".updateRevision" ).trigger( "click" );
           // $(".btn-preview").trigger("click");
        })

        
        
    </script> 