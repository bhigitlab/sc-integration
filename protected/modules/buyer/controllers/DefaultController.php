<?php

class DefaultController extends Controller
{
	public function actiongetBuyers()
	{
		$buyer_list = array();
		if (isset($_POST['company_id']) && !empty($_POST['company_id'])) {
			$company_id = $_POST['company_id'];
			$buyer_datas = Buyers::model()->findAll(['condition' => 'FIND_IN_SET(' . $company_id . ',company)']);
			$buyer_list =   CHtml::listData($buyer_datas, 'id', 'name');
		} else {
			$buyer_datas = Buyers::model()->findAll(['condition' => 'status = 1']);
			$buyer_list =   CHtml::listData($buyer_datas, 'id', 'name');
		}
		echo json_encode(['buyer_list' => $buyer_list]);
	}

	public function actiongetProjectAndFlats()
	{
		$project_id = '';
		$flat_list = array();
		$flat_count = '';
		if (isset($_POST['buyer_id']) && isset($_POST['company_id'])) {
			$buyer_id = $_POST['buyer_id'];
			$buyer_projects = Buyers::model()->findByPk($buyer_id);
			$project_id = $buyer_projects['project'];
			$project_model = Projects::model()->findByAttributes(array('pid' => $project_id, 'company_id' => $_POST['company_id']));
			if (!empty($project_id)) {
				$flat_datas = ProjectFlatNumbers::model()->findAll(['condition' => 'id IN (' . $buyer_projects['flat_numbers'] . ')']);
				$flat_list =   CHtml::listData($flat_datas, 'id', 'flat_number');
				$flat_count = count($flat_datas);
			} else {
				$project_id = '';
			}
		}
		echo json_encode(['project_id' => $project_id, 'flat_list' => $flat_list, 'flat_count' => $flat_count]);
	}

	public function actiongetProjects()
	{
		$company_id = '';
		if (isset($_POST['company_id'])) {
			$company_id = $_POST['company_id'];
		}
		if (!empty($company_id)) {
			$projects = Projects::model()->findAll(['condition' => 'FIND_IN_SET(' . $company_id . ',company_id)']);
		} else {
			$projects = Projects::model()->findAll();
		}
		$project_list =   CHtml::listData($projects, 'pid', 'name');
		echo json_encode(['project_list' => $project_list]);
	}

	public function actiongetTransactionHeads()
	{
		$tblpx        = Yii::app()->db->tablePrefix;
		$expOptions   = "<option value=''>-Select Transaction Type-</option>";
		$transaction_heads = Status::model()->findAll(['condition' => 'status_type = "payment_type" AND sid IN(88,89)', 'order' => 'caption ASC']);
		$transaction_head_list =   CHtml::listData($transaction_heads, 'sid', 'caption');
		$transaction_for = filter_input(INPUT_POST, 'transaction_for');
		if ($transaction_for == 3) {
			if (filter_input(INPUT_POST, 'project_id')) {
				$project_id = filter_input(INPUT_POST, 'project_id');
				$expData      = Yii::app()->db->createCommand("SELECT et.type_id as typeid, et.type_name as typename FROM " . $tblpx . "expense_type et LEFT JOIN " . $tblpx . "project_exptype pet ON et.type_id = pet.type_id WHERE pet.project_id = " . $project_id . " ORDER BY et.type_name ASC")->queryAll();

				foreach ($expData as $eData) {
					$expOptions  .= "<option value='" . $eData["typeid"] . "'>" . $eData["typename"] . "</option>";
				}
			}
		}
		echo json_encode(['transaction_head_list' => $transaction_head_list, 'expOptions' => $expOptions]);
	}

	public function actiongenerateInvoice()
	{

		/* INV/VH/19-20(financial year)/E(exempted; zero rated. If it is taxable, it will be 'T')001(3 digit auto increment) */
		$invoiceDatas = BuyerInvoice::model()->findAll();
		$invoiceDataCount = count($invoiceDatas);
		$invoiceDataCount++;
		$invoice_pref = 	$this->invoiceGenerate($invoiceDataCount);
		echo json_encode(['invoice_id' => $invoice_pref]);
	}
	public function invoiceGenerate($invoiceDataCount)
	{

		$date = strtotime(date('Y-m-d'));
		$financial_year_date_start = strtotime(date('Y-04-01'));
		if ($date >= $financial_year_date_start) {
			$end = date('y', strtotime('+1 years'));
			$year_data =  date('y') . '-' . $end;
		} else {
			$end = date('y', strtotime('-1 years'));
			$year_data = $end . '-' . date('y');
		}

		$invID = str_pad($invoiceDataCount, 3, '0', STR_PAD_LEFT);
		$invoice_pref = 'INV/VH/' . $year_data . '/E/' . $invID;
		$invoiceCheck = BuyerInvoice::model()->findAll(['condition' => 'invoice_no = "' . $invoice_pref . '"']);
		if (!empty($invoiceCheck)) {
			$invoiceDataCount++;
			$invoice_pref = 	$this->invoiceGenerate($invoiceDataCount);
		}
		return $invoice_pref;
	}
	public function actiongetInvoices()
	{
		if (isset($_POST['buyer_id']) && isset($_POST['project_id']) && isset($_POST['flat_id'])) {
			$invoices 		=  BuyerInvoice::model()->findAll(array("condition" => "buyer_id = " . $_POST['buyer_id'] . " AND project_id =" . $_POST['project_id'] . " AND flat_id =" . $_POST['flat_id']));
			foreach ($invoices as $key => $invoice) {
				$tblpx = Yii::app()->db->tablePrefix;
				$command = Yii::app()->db->createCommand();
				$command->select('SUM(amount) AS sum');
				$command->from($tblpx . 'buyer_transactions');
				$command->where('invoice_no=:invoice_no', array(':invoice_no' => $invoice['buyer_invoice_id']));
				$payed_amount = $command->queryScalar();
				if ($payed_amount == $invoice['total_amount']) {
					unset($invoices[$key]);
				}
			}
			$invoice_list =   CHtml::listData($invoices, 'buyer_invoice_id', 'invoice_no');
			echo json_encode(['invoice_list' => $invoice_list]);
		}
	}
	public function actiongetInvoicesDetails()
	{
		if (isset($_POST['invoice_id'])) {
			$tblpx = Yii::app()->db->tablePrefix;
			$data1 = Yii::app()->db->createCommand("SELECT SUM(amount) as qt_amount FROM {$tblpx}buyer_invoice_list WHERE buyer_inv_id=" . $_POST['invoice_id'] . "")->queryRow();

			$data2 = Yii::app()->db->createCommand("SELECT SUM(tax_total) as qt_taxamount FROM {$tblpx}buyer_invoice_list WHERE buyer_inv_id=" . $_POST['invoice_id'] . "")->queryRow();
			$tblpx = Yii::app()->db->tablePrefix;
			$command = Yii::app()->db->createCommand();
			$command->select('SUM(amount) AS sum');
			$command->from($tblpx . 'buyer_transactions');
			$command->where('invoice_no=:invoice_no', array(':invoice_no' => $_POST['invoice_id']));
			$prev_amount = $command->queryScalar();
			$total_sum = $data2['qt_taxamount'] + $data1['qt_amount'];
			$amount_to_be_billed = $total_sum - $prev_amount;
			echo json_encode(['total_sum' => $amount_to_be_billed]);
		}
	}
	public function actiongetTransactionNo()
	{
		$transaction_no = '';
		$transaction_id = $_POST['transaction_id'];
		if (!empty($_POST['transaction_type'])) {
			$trasaction_type = $_POST['transaction_type'];
			if ($trasaction_type == 1) {
				if (!empty($transaction_id)) {
					$transaction_details = BuyerTransactions::model()->findByPk($transaction_id);
					if ($transaction_details->transaction_for != $trasaction_type) {
						$transactions  = BuyerTransactions::model()->findAll(array('condition' => 'transaction_for = ' . $trasaction_type));
						$transactions_count = count($transactions);
						$transactions_count++;
						$transaction_digit = str_pad($transactions_count, 3, '0', STR_PAD_LEFT);
						$transaction_no = 'ADV/' . date("y", strtotime("-1 year")) . '-' . date('y') . '/' . $transaction_digit;
					} else {
						$transaction_no = $transaction_details['transaction_no'];
					}
				} else {
					$transactions  = BuyerTransactions::model()->findAll(array('condition' => 'transaction_for = ' . $trasaction_type));
					$transactions_count = count($transactions);
					$transactions_count++;
					$transaction_digit = str_pad($transactions_count, 3, '0', STR_PAD_LEFT);
					$transaction_no = 'ADV/' . date("y", strtotime("-1 year")) . '-' . date('y') . '/' . $transaction_digit;
				}
			} elseif ($trasaction_type == 2) {
				if (!empty($transaction_id)) {
					$transaction_details = BuyerTransactions::model()->findByPk($transaction_id);
					if ($transaction_details->transaction_for != $trasaction_type) {
						$transactions  = BuyerTransactions::model()->findAll(array('condition' => 'transaction_for = ' . $trasaction_type));
						$transactions_count = count($transactions);
						$transactions_count++;
						$transaction_digit = str_pad($transactions_count, 3, '0', STR_PAD_LEFT);
						$transaction_no = 'ADV/' . date("y", strtotime("-1 year")) . '-' . date('y') . '/' . $transaction_digit;
					} else {
						$transaction_no = $transaction_details['transaction_no'];
					}
				} else {
					$transactions  = BuyerTransactions::model()->findAll(array('condition' => 'transaction_for = ' . $trasaction_type));
					$transactions_count = count($transactions);
					$transactions_count++;
					$transaction_digit = str_pad($transactions_count, 3, '0', STR_PAD_LEFT);
					$transaction_no = 'RPT/' . date("y", strtotime("-1 year")) . '-' . date('y') . '/' . $transaction_digit;
				}
			}
		}

		echo json_encode(['transaction_no' => $transaction_no]);
	}
	public function actioncheckInvoiceAmount()
	{
		$amount = 0;
		$result = array();
		if (isset($_POST['amount']) && !empty($_POST['amount'])) {
			$amount = $_POST['amount'];
		}
		if (isset($_POST['invoice_no']) && !empty($_POST['invoice_no'])) {
			$invoice_model = BuyerInvoice::model()->findByPk($_POST['invoice_no']);
			$invoice_amount = $invoice_model['total_amount'];
			$tblpx = Yii::app()->db->tablePrefix;
			$command = Yii::app()->db->createCommand();
			$command->select('SUM(amount) AS sum');
			$command->from($tblpx . 'buyer_transactions');
			$command->where('invoice_no=:invoice_no', array(':invoice_no' => $_POST['invoice_no']));
			$prev_amount = $command->queryScalar();
			$billed_transaction = $prev_amount + $amount;
			if ($billed_transaction > $invoice_amount) {
				$result = ['flag' => 2, 'msg' => 'Amount exceeds invoice amount'];
			} else {
				$result = ['flag' => 1];
			}
			echo json_encode($result);
		}
	}
}
