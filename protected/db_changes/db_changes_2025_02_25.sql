
ALTER TABLE `jp_labour_estimation` ADD `used_quantity` INT NULL DEFAULT '0' COMMENT 'defines used quantity in labour entries' AFTER `created_at`;

ALTER TABLE `jp_labour_estimation` ADD `used_amount` INT(11) NULL DEFAULT '0' COMMENT 'used amount in labour entries' AFTER `used_quantity`;

