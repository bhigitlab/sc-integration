<?php

/**
 * This is the model class for table "{{warehousereceipt_items}}".
 *
 * The followings are the available columns in table '{{warehousereceipt_items}}':
 * @property integer $item_id
 * @property integer $warehousereceipt_id
 * @property integer $warehousereceipt_warehouseid
 * @property integer $warehousereceipt_itemid
 * @property integer $warehousereceipt_unit
 * @property double $warehousereceipt_quantity
 * @property double $warehousereceipt_size
 * @property integer $warehousereceipt_jono
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class WarehousereceiptItems extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{warehousereceipt_items}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('warehousereceipt_id, warehousereceipt_warehouseid, warehousereceipt_itemid, warehousereceipt_unit, warehousereceipt_quantity, created_by, created_date, updated_by, updated_date', 'required'),
			array('warehousereceipt_id, warehousereceipt_warehouseid, warehousereceipt_itemid, created_by, updated_by', 'numerical', 'integerOnly' => true),
			array('warehousereceipt_quantity, warehousereceipt_size', 'numerical'),
			array('warehousereceipt_baseunit_accepted_effective_quantity', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('item_id, warehousereceipt_id, warehousereceipt_warehouseid, warehousereceipt_itemid, warehousereceipt_unit,warehousereceipt_batch, warehousereceipt_quantity, warehousereceipt_size, warehousereceipt_jono, created_by, created_date, updated_by, updated_date,warehousereceipt_accepted_quantity,warehousereceipt_rejected_quantity,warehousereceipt_baseunit_accepted_quantity', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'item_id' => 'Item',
			'warehousereceipt_id' => 'Warehousereceipt',
			'warehousereceipt_warehouseid' => 'Warehousereceipt Warehouseid',
			'warehousereceipt_itemid' => 'Warehousereceipt Itemid',
			'warehousereceipt_unit' => 'Warehousereceipt Unit',
			'warehousereceipt_batch' => 'Warehousereceipt Batch',
			'warehousereceipt_quantity' => 'Warehousereceipt Quantity',
			'warehousereceipt_accepted_quantity' => 'Warehousereceipt Accepted Quantity',
			'warehousereceipt_rejected_quantity' => 'Warehousereceipt Rejected Quantity',
			'warehousereceipt_baseunit_accepted_quantity' => 'Warehousereceipt Accepted Quantity(Base)',
			'warehousereceipt_size' => 'Warehousereceipt Size',
			'warehousereceipt_jono' => 'Warehousereceipt Jono',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('item_id', $this->item_id);
		$criteria->compare('warehousereceipt_id', $this->warehousereceipt_id);
		$criteria->compare('warehousereceipt_warehouseid', $this->warehousereceipt_warehouseid);
		$criteria->compare('warehousereceipt_itemid', $this->warehousereceipt_itemid);
		$criteria->compare('warehousereceipt_unit', $this->warehousereceipt_unit);
		$criteria->compare('warehousereceipt_batch', $this->warehousereceipt_batch);
		$criteria->compare('warehousereceipt_quantity', $this->warehousereceipt_quantity);
		$criteria->compare('warehousereceipt_accepted_quantity', $this->warehousereceipt_accepted_quantity);
		$criteria->compare('warehousereceipt_rejected_quantity', $this->warehousereceipt_rejected_quantity);
		$criteria->compare('warehousereceipt_baseunit_accepted_quantity', $this->warehousereceipt_baseunit_accepted_quantity);
		$criteria->compare('warehousereceipt_size', $this->warehousereceipt_size);
		$criteria->compare('warehousereceipt_jono', $this->warehousereceipt_jono);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WarehousereceiptItems the static model class
	 */

	public function getData()
	{

		echo 'hai';
		die('sdhfskjdhsd');
	}
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
