<?php

class MaterialsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$accessArr = array();
        $accessauthArr = array();
        $controller = Yii::app()->controller->id;
        if (isset(Yii::app()->user->menuall)) {
            if (array_key_exists($controller, Yii::app()->user->menuall)) {
                $accessArr = Yii::app()->user->menuall[$controller];
            }
        }

        if (isset(Yii::app()->user->menuauth)) {
            if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                $accessauthArr = Yii::app()->user->menuauth[$controller];
            }
        }
        $access_privlg = count($accessauthArr);
        return array(
            array(
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => $accessArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),

			array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('delete', 'ratedecline','Viewremarks','addremarks','Dynamicexpensehead','Dynamicvendor','additionalCharge','deleteadditionalCharge','Dynamiccompany','loadPoItemsModal','getProjectByMrId'),
                'users' => array('*'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Materials;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Materials'])) {
			$model->attributes = $_POST['Materials'];

			// Check if material_unit is set and is an array
			if (isset($model->material_unit) && is_array($model->material_unit)) {
				// Convert the array to a comma-separated string
				$model->material_unit = implode(',', $model->material_unit);
			}

			if (isset($model->specification) && is_array($model->specification)) {
				$model->specification = implode(',', $model->specification);
			}

			if ($model->save()) {
				//API integration
				if ($_POST['execute_api'] == 1) {
                $pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();

                if($pms_api_integration ==1){
                    $request = [
                        'origin' => 'coms',
                        'material_id' =>$model->id,
                        'name' => $model->material_name,
                        'units' =>  $model->material_unit,
                    ];

                    $slug = "api/create-material";
                    $method="POST";
                    $globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
                }
		     	}
				$this->redirect(array('index'));
			}
		}

		$this->renderPartial('create', array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Materials'])) {

			$old_material_unit = $model->material_unit;
			$model->attributes = $_POST['Materials'];

			// Check if material_unit is set and is an array
			if (isset($model->material_unit) && is_array($model->material_unit)) {
				// Convert the array to a comma-separated string
				$model->material_unit = implode(',', $model->material_unit);
			}

			if (isset($model->specification) && is_array($model->specification)) {
				$model->specification = implode(',', $model->specification);
			}

			if ($model->save()) {
				//API integration
				$pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
				if ($_POST['execute_api'] == 1) {
				if($pms_api_integration ==1){
					$request = [
						'origin' => 'coms',
						'material_id' =>$model->id,
						'name' => $model->material_name,
						'units' => strval($model->material_unit),
						'pms_material_id'=> $model->pms_material_id,
					];
					if (!empty($model->pms_material_id)) {

						$new_material_unit = $model->material_unit; 
						if (!is_array($new_material_unit)) {
							if (is_string($new_material_unit)) {
								$new_material_unit = explode(',', $new_material_unit);
							} else {
								$new_material_unit = [];
							}
						}
						if (!is_array($old_material_unit)) {
							if (is_string($old_material_unit)) {
								$old_material_unit = explode(',', $old_material_unit);
							} else {
								$old_material_unit = [];
							}
						}
						$remove_unit = array_diff($old_material_unit, $new_material_unit);
						$add_unit = array_diff($new_material_unit, $old_material_unit);

						$remove_unit = implode(',', $remove_unit);
						$add_unit = implode(',', $add_unit);
						$removeUnit=explode(',', $remove_unit);
						$addUnit=explode(',', $add_unit);
						$request['remove_units'] = implode(',', $removeUnit);
						$request['add_units'] = implode(',', $addUnit);
						$slug = "api/update-material";
					} else {
						$slug = "api/create-material";
					}
					$method="POST";
					$globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
				}
			    }
				$this->redirect(array('index'));
			}
		} else {
			// Ensure material_unit is a string before using explode
			if (is_string($model->material_unit)) {
				$model->material_unit = explode(',', $model->material_unit);
			} else {
				$model->material_unit = [];
			}

			if (is_string($model->specification)) {
				$model->specification = explode(',', $model->specification);
			} else {
				$model->specification = [];
			}
		}

		$this->renderPartial('update', array(
			'model' => $model,
		));
	}

	public function actionCheckDuplicateMaterialName() {
		if (Yii::app()->request->isPostRequest && isset($_POST['material_name'])) {
			$materialName = $_POST['material_name'];
			$isDuplicate = Materials::model()->exists('material_name=:material_name', array(':material_name' => $materialName));
			
			// Ensure proper JSON response format
			echo CJSON::encode(array('isDuplicate' => $isDuplicate));
			Yii::app()->end();
		}
	}
	
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if ($model->delete()) {
			$response = [
				'success' => 'success',
				'message' => 'Record deleted successfully.'
			];
		} else {
			throw new Exception(json_encode($model->getErrors()));
		
		}
		echo json_encode($response);
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model = new Materials('search');
		$this->render('index',array(
			'dataProvider' => $model->search(),
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Materials('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Materials']))
			$model->attributes=$_GET['Materials'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Materials the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Materials::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Materials $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='materials-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
