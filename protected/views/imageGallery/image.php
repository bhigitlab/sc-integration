<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *///echo Yii::getVersion();

//        $this->widget('application.extensions.fbgallery.fbgallery');
//        $this->widget('application.extensions.fbgallery.fbgallery');
        //$this->widget('application.extensions.fbgallery.fbgallery', array('pid'=>'1'));
        
//        $this->widget('application.extensions.gallery.EGallery',
//        array('path' => '/images/gallery',)
//    );
$url = Yii::app()->createAbsoluteUrl("ImageGallery/upload");
?>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/uploadfile.css" rel="stylesheet">
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.uploadfile.min.js"></script>
<div class="container" id="expense">
    <div class="row">
        <div class="col-md-6">
           <h2>Gallery</h2>
        </div>
        
    </div>
    <div class="container">
        <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'image-gallery-form',
                    'enableAjaxValidation' => true,
                    'method' => 'POST',
                    'htmlOptions' => array(
                        'enctype' => 'multipart/form-data',
                        'enableClientValidation' => true,
                    ),
                ));
                ?>
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <?php echo $form->labelEx($model, 'projectid'); ?>
                <?php
                echo $form->dropDownList($model, 'projectid', CHtml::listData(Projects::model()->findAll(array('order' => 'name')), 'pid', 'name'), array('empty' => '-----------','class' => 'form-control', 'ajax' => array(
                           'type' => 'POST',
                           'dataType' => 'json',
                           'url' => CController::createUrl('/ImageGallery/getalbumbyprojectid'),
                           'data' => array('id' => 'js:this.value'),
                           'success' => 'function(data) {
                       $("#ImageGallery_albumid").html("");
                       $("#ImageGallery_albumid").html(data.albums);
                    }')))
                 
                        ?>
                <?php echo $form->error($model,'projectid'); ?>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <?php echo $form->labelEx($model, 'albumid'); ?>
                <?php
                echo $form->dropDownList($model, 'albumid', array(), array('empty' => '-----------','class' => 'form-control'))
                ?>
                <?php echo $form->error($model, 'albumid'); ?>
                
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12 add-btn" style="display: none">
                <button type="button" style="float: left;margin-top: 25px;" data-toggle="modal" data-target="#addAlbum"  class="createAlbum">Add Album</button>
            </div>
        </div><br>
        <div class="row">
            <input type="hidden" name="hidfiles" id="hidfiles" value=""/>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div id="fileuploader">Upload</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Upload' : 'Save' ,array('class' => 'btn','id'=>'upload')); ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>

    </div>
</div>
<div id="addAlbum" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

            </div>
        </div>
<script>

$(document).ready(function() {
    var upfiles = [];
	$("#fileuploader").uploadFile({
		url:'<?php echo $url; ?>',
		fileName:"myfile",
                returnType: "json",
                acceptFiles:"image/*",
                showPreview:true,
                previewHeight: "100px",
                previewWidth: "100px",
                showDelete: true,
                deleteCallback: function (data, pd) {
                for (var i = 0; i < data.length; i++) {
                    $.post("index.php?r=ImageGallery/deleteimage", {op: "delete",name: data[i]},
                        function (resp,textStatus, jqXHR) {
                            //Show Message

                            upfiles = jQuery.grep(upfiles, function(value) {
                                return value != resp;
                            });
                            
                            $('#hidfiles').val(upfiles);
                            //alert("File Deleted");
                        });
                }
                pd.statusbar.hide(); //You choice.

            },
            onSuccess:function(files,data,xhr,pd)
            {
                upfiles.push(data);
                $('#hidfiles').val(upfiles);
            }
	});
        
$('#ImageGallery_albumid').on('change', function() {
    //alert($('#ImageGallery_albumid').val());
    if(this.value == '')
    {
        $('.add-btn').hide();
    }
    else if(this.value ==0){
        $('.add-btn').css('display','block');
    }
    else if(this.value !=''){
        $('.add-btn').hide();
    }
    
  /*if(this.value == 0){
      
      $('.add-btn').css('display','block');
  }else{
      if(this.value != ''){
            $('.add-btn').hide();
        }
  }*/
});
$('.createAlbum').click(function () {
                    // alert('hi');
                    $.ajax({
                        type: "GET",
                        url: "<?php echo $this->createUrl('albums/createalbum') ?>",
                        success: function (response)
                        {
                            $("#addAlbum").html(response);
                            $('#Albums_projectid').val($('#ImageGallery_projectid').val());
                        }
                    });
                });
});
$('#upload').click(function(){
 var imagedetails=$('#hidfiles').val();
 var project_id=$('#ImageGallery_projectid').val();
 var album=$('#ImageGallery_albumid').val();
 /*if(imagedetails=='' || project_id=='' || album==''){
     alert("All fields are required and Please upload atleast one image.");
      //$('#errorconsole').innerHTML("Please upload atleast one image.");
     return false;
 }
 else{
     return true;
 }*/
 
if(project_id == ''){
       alert("Please select project");
     return false;
 }
 else if(album=='')
 {
     alert("Please select album.");
     return false;
 }
 else if(album==0){
     alert("Please create or select an album then upload image");
     return false;
 }
 else if(imagedetails=='')
 {
     alert("Please upload atleast one image");
     return false;
 }
 
 else if(imagedetails!='' && project_id!='' && album!=''){
     return true;
 }

// else{
//         
//         return true;
//     }


});

</script>
