<?php

Yii::import('zii.widgets.grid.CButtonColumn');

class CustomButtonColumn extends CButtonColumn
{
    protected function renderButton($id, $button, $row, $data)
    {
        if (isset($button['label'])) {
            $label = $button['label'];
            if (is_callable($label)) {
                $button['label'] = call_user_func($label, $data);
            }
        }

        parent::renderButton($id, $button, $row, $data);
    }
}
