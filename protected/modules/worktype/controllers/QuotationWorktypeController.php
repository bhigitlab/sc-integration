<?php

class QuotationWorktypeController extends Controller {

    public function accessRules() {
        $accessArr = array();
        $accessauthArr = array();
        $controller = 'buyer/' . Yii::app()->controller->id;

        if (isset(Yii::app()->user->menuall)) {
            if (array_key_exists($controller, Yii::app()->user->menuall)) {
                $accessArr = Yii::app()->user->menuall[$controller];
            }
        }

        if (isset(Yii::app()->user->menuauth)) {
            if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                $accessauthArr = Yii::app()->user->menuauth[$controller];
            }
        }
        $access_privlg = count($accessauthArr);
        return array(
            array(
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => $accessArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                //'actions' => array(''),
                'users' => array('@'),
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionAdmin() {
        $this->redirect(array('index'));
    }

    public function actionIndex($id = 0) {
        if ($id !== 0 and $id > 0) {
            $formmodel = $this->loadModel($id);
            $actiontype = ' Updated ';
        }

        if ($id == 0) {
            $formmodel = new QuotationWorktype;
            $actiontype = ' Created ';
        }

        if (isset($_POST['QuotationWorktype'])) {
           
            $formmodel->attributes = $_POST['QuotationWorktype'];
            $formmodel->created_by = Yii::app()->user->id;
            $formmodel->created_date = date('Y-m-d');
            $formmodel->template_id = isset($_POST['QuotationWorktype']['template_id'])?$_POST['QuotationWorktype']['template_id']:'';
            if ($formmodel->save()) {
                Yii::app()->user->setFlash('success', "Quotation Work Type{$actiontype} successfully");
                return $this->redirect(array('quotationWorktype/index'));
            } else {
                $error= $formmodel->getErrors();
                Yii::app()->user->setFlash('error', $error['name'][0]);                
                return $this->redirect(array('quotationWorktype/index'));
            }
        }

        $model = new QuotationWorktype('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['QuotationWorktype']))
            $model->attributes = $_GET['QuotationWorktype'];

        $this->render('index', array(
            'model' => $model, 'formmodel' => $formmodel
        ));
    }

    public function actionDelete($id) {

        $model = $this->loadModel($id);
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if (!$model->delete()) {
                $success_status = 0;
                throw new Exception(json_encode($model->getErrors()));
            } else {
                $success_status = 1;
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Data Deleted Successfully'));
            } else {
                if ($error->errorInfo[1] == 1451) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }

    public function loadModel($id) {
        $model = QuotationWorktype::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionSearch() {
        $this->render('search');
    }

    public function actionUpdate() {
        $this->render('update');
    }

    public function actionView() {
        $this->render('view');
    }

    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
}
