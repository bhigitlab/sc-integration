<?php
/* @var $this DailyexpenseController */
/* @var $model Dailyexpense */

$this->breadcrumbs=array(
	'Dailyexpenses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Dailyexpense', 'url'=>array('index')),
	array('label'=>'Manage Dailyexpense', 'url'=>array('admin')),
);
?>

<h1>Create Dailyexpense</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>