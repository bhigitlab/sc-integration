<?php

$project_name = (isset($sql[0]['name']) ? $sql[0]['name'] : '');
$owner = (isset($sql[0]['client_name']) ? $sql[0]['client_name'] : '');
$site = (isset($sql[0]['site']) ? $sql[0]['site'] : '');
//$duration = strftime(" %d/%m ", strtotime($date_from))." --".strftime(" %d/%m/%y ", strtotime($date_to));
$start_date = strftime(" %dth %B '%y, %A", strtotime($sql[0]['created_date']));

$file= 'uploads/blank_template';
foreach (glob($file."/*.html") as $file) {
    $files = $file;
}

$FinishString = file_get_contents($files);
$FinishString=str_replace('{project_name}', $project_name, $FinishString);  
$FinishString=str_replace('{owner}', $owner, $FinishString);  
$FinishString=str_replace('{site}', $site, $FinishString); 
$FinishString=str_replace('{duration}', $duration, $FinishString); 
$FinishString=str_replace('{start_date}', $start_date, $FinishString);
$FinishString=str_replace('{past_week}',nl2br($pastweek), $FinishString);
$FinishString=str_replace('{forecast}', nl2br($nextweek), $FinishString);
$FinishString=str_replace('{open_issues}', nl2br($issues), $FinishString);

$FinishString=str_replace('{content}', $project_entries, $FinishString); 
echo $FinishString;
?>