<?php
/* @var $this AlbumsController */
/* @var $model Albums */

$this->breadcrumbs=array(
	'Albums'=>array('index'),
	'Create',
);

/*$this->menu=array(
	array('label'=>'List Albums', 'url'=>array('index')),
	array('label'=>'Manage Albums', 'url'=>array('admin')),
);*/
?>

<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
            <h4 class="modal-title">Add Albums</h4>
        </div>
        <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
    </div>

</div>
