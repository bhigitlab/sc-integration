<?php
/* @var $this WithdrawalsController */
/* @var $model Withdrawals */

$this->breadcrumbs=array(
	'Withdrawals'=>array('index'),
	$model->exp_id=>array('view','id'=>$model->exp_id),
	'Update',
);
/*
$this->menu=array(
	array('label'=>'List Withdrawals', 'url'=>array('index')),
	array('label'=>'Create Withdrawals', 'url'=>array('create')),
	array('label'=>'View Withdrawals', 'url'=>array('view', 'id'=>$model->exp_id)),
	array('label'=>'Manage Withdrawals', 'url'=>array('admin')),
);
 */
?>
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
			<h4 class="modal-title">Update Withdrawal</h4>
		</div>



<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

</div>
	
</div>