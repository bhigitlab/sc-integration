<?php
/* @var $this BankController */
/* @var $model Bank */

$this->breadcrumbs=array(
	'Deposit'=>array('index'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List Bank', 'url'=>array('index')),
	//array('label'=>'Manage Bank', 'url'=>array('admin')),
);
?>

<!--<div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
            <h4 class="modal-title">Add Deposit</h4>
        </div>
        <?php //echo $this->renderPartial('_form', array('model' => $model)); ?>
    </div>

</div>-->
<div class="panel panel-gray">
<div class="panel-heading form-head">
        <h3 class="panel-title">Add Deposit</h3>
    </div>
        <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
