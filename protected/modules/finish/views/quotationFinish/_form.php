<?php
/* @var $this QuotationCategoryMasterController */
/* @var $model QuotationCategoryMaster */
/* @var $form CActiveForm */
?>
<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'quotation-finish-master-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnChange' => true,
            'validateOnType' => false,
        )
    ));
    ?>

    <div class="row">
        <div class="col-md-12">
            <?php echo $form->labelEx($model, 'name'); ?>
            <?php echo $form->textField($model, 'name', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'name'); ?>
        </div>
        <div class="col-md-6 save-btnHold">
            <label style="display:block;">&nbsp;</label>
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-sm btn-info submit')); ?>
            <?php
            if (!$model->isNewRecord) {
                echo CHtml::Button('Close', array('class' => 'btn', 'onclick' => 'javascript:location.href="' . $this->createUrl('index') . '"'));
            } else {
                echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;', 'class' => 'btn btn-sm btn-default'));
            }
            ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>