<?php
/* @var $this DepositController */
/* @var $model Deposit */

$this->breadcrumbs=array(
	'Deposits'=>array('index'),
	$model->deposit_id,
);

$this->menu=array(
	array('label'=>'List Deposit', 'url'=>array('index')),
	array('label'=>'Create Deposit', 'url'=>array('create')),
	array('label'=>'Update Deposit', 'url'=>array('update', 'id'=>$model->deposit_id)),
	array('label'=>'Delete Deposit', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->deposit_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Deposit', 'url'=>array('admin')),
);
?>

<h1>View Deposit #<?php echo $model->deposit_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'deposit_id',
		'deposit_name',
	),
)); ?>
