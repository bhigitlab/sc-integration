<?php
$tblpx = Yii::app()->db->tablePrefix;
$billId = $model['bill_id'];
$purchaseId = $model['purchase_id'];
$sql = Yii::app()->db->createCommand("select {$tblpx}projects.pid as pid,{$tblpx}projects.name as project,{$tblpx}vendors.name as vendor from {$tblpx}projects
left join {$tblpx}purchase ON {$tblpx}purchase.project_id={$tblpx}projects.pid left join {$tblpx}vendors ON {$tblpx}purchase.vendor_id={$tblpx}vendors.vendor_id
where {$tblpx}purchase.p_id=$purchaseId")->queryRow();
$project[] = array("pid" => $sql["pid"], "name" => $sql["project"]);
$company = Company::model()->findByPk($model->company_id);

$purchasesql = "SELECT  purchase_no,expensehead_id "
    . " FROM `{$tblpx}purchase` "
    . " WHERE p_id =" . $purchaseId;
$purchase = Yii::app()->db->createCommand($purchasesql)->queryRow();
$expense_head = ExpenseType::model()->findByPK($purchase['expensehead_id']);

if (!isset($_GET['exportpdf'])) {
    ?>

    <!-- <style>
        .invoicemaindiv th,
        .invoicemaindiv td {
            padding: 10px;
            text-align: left;
        }


        .form-control[readonly] {
            border: 0;
            background-color: #fff;
        }

        table.total-table tr td {
            font-size: 14px;
        }

        table.table td {
            padding-right: 0px !important;
            font-size: 11px;
            vertical-align: middle !important;
        }

        table.table th {
            font-size: 11px;
        }

        table.table .form-control {
            padding: 1px 1px;
            font-size: 11px;
            box-shadow: none;
        }

        .bill_title {
            border-bottom: 1px solid #ddd;
        }

        .table-inverse thead {
            background-color: #eee;
        }

        .small_box input {
            width: 80px;
        }

        .bill_view input {
            border: 0;
        }

        .amount_values input {
            border: 0;
            text-align: right;
        }

        .amn_data {
            background-color: #fafafa;
            border-bottom: 1px dashed #eee;
            margin-bottom: 2px;
        }
    </style> -->
    <div class="container">
        <div class="loading-overlay">
            <span class="fa fa-spinner fa-3x fa-spin"></span>
        </div>
        <div class="">
            <div class="header-container margin-bottom-5">
                <h3>BILL DETAILS</h3>
                <div class="btn-container">
                    <?php echo CHtml::link('<i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>', Yii::app()->request->requestUri . "&exportpdf=1", array('class' => 'save_btn2 btn btn-primary pdf_excel', 'title' => 'SAVE AS PDF')); ?>
                    <a id="download" class="excelbtn2 pdf_excel btn btn-primary" title="SAVE AS EXCEL">
                        <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <?php
            $id = $model['bill_id'];
            ?>
            <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
            <form id="pdfvals2" method="post"
                action="<?php echo $this->createAbsoluteUrl('bills/updatebills&bid=' . $id) ?>">
                <div class="page_filter">
                    <div class="row">
                        <div class="col-lg-2 col-md-3 col-sm-4 form-group">
                            <label>COMPANY:</label>
                            <div>
                                <?php echo $company['name']; ?>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-4 form-group">
                            <label>VENDOR:</label>
                            <div>
                                <?php echo $sql['vendor']; ?>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-4 form-group">
                            <label>PROJECT:</label>
                            <div>
                                <?php echo $project[0]['name']; ?>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-3 form-group">
                            <label>EXPENSE HEAD:</label>
                            <div>
                                <?php echo $expense_head->type_name; ?>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-3 form-group">
                            <label>PURCHASE No:</label>
                            <div>
                                <?php echo $purchase['purchase_no']; ?>
                            </div>
                        </div>
                        <div class="col-lg-1 col-md-3 col-sm-3 form-group">
                            <label>DATE:</label>
                            <div>
                                <?php echo date("d-m-Y", strtotime($model['bill_date'])); ?>
                            </div>
                        </div>
                        <div class="col-lg-1 col-md-3 col-sm-3 form-group">
                            <label>BILL No:</label>
                            <div>
                                <?php echo $model['bill_number']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="purchase_id" value="<?php echo $purchaseId; ?>">
                <input type="hidden" name="company_id" id="company_id" value="<?php echo $model->company_id; ?>">
                <input type="hidden" name="bill_id" value="<?php echo $billId; ?>">

                <div class="table-responsive">
                    <table class="table table-inverse">
                        <thead class="entry-table">
                            <tr>
                                <th width="10px">Sl.No</th>
                                <th>Specifications/Remark</th>
                                <th>HSN Code</th>
                                <th>Quantity</th>
                                <th>Unit</th>
                                <th>Rate</th>
                                <th>Amount</th>
                                <th>Tax Slab (%)</th>
                                <th>Discount</th>
                                <th>Discount (%)</th>
                                <th>CGST</th>
                                <th>CGST (%)</th>
                                <th>SGST</th>
                                <th>SGST (%)</th>
                                <th>IGST</th>
                                <th>IGST (%)</th>
                                <th>Total Tax</th>
                                <th>Total Tax (%)</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody class="addrow">
                            <?php
                            if (!empty($newmodel)) {
                                $items = $newmodel['items'];
                                $additional_items = $newmodel['add_items'];
                                $i = 1;
                                foreach ($items as $value) {
                                    ?>
                                    <tr>
                                        <td colspan="19" class="categ_td td-highlight">
                                            <?php echo !empty($value['category_title']) ? $value['category_title'] : '' ?>
                                        </td>
                                    </tr>
                                    <?php
                                    foreach ($value as $item) {
                                        if (isset($item['specification'])) {
                                            if ($item['rate_approve'] == 1) {
                                                $rate_title = 'Order Rate: ' . $item["purchase_rate"];
                                            }
                                            if ($item['approve_status'] == 1) {
                                                $qty_title = 'Order Quantity: ' . $item['purchase_qty'];
                                            }
                                            ?>
                                            <tr>
                                                <input type="hidden" class="form-control" name="ids[]"
                                                    value="<?php echo $item['billitem_id']; ?>" />
                                                <td width="10px"><input type="text" class="form-control" name="slNo[]"
                                                        value="<?php echo $i; ?>" readonly="true" /></td>
                                                <td>
                                                    <?php echo $item['specification']; ?><input type="hidden" class="form-control"
                                                        name="category[]" value="<?php echo $item['specification']; ?>" readonly="true" />
                                                </td>
                                                <td class="text-center">
                                                    <?php echo $item['hsn_code']; ?><input type="hidden" class="form-control"
                                                        name="hsn_code[]" value="<?php echo $item['hsn_code']; ?>" readonly="true" />
                                                </td>
                                                <td class="quantity"><input type="text" class="form-control" name="quantity[]"
                                                        value="<?php echo $item['quantity']; ?>" readonly="true"
                                                        title="<?php echo $item['approve_status'] == 1 ? $qty_title : ''; ?>" /></td>
                                                <td><input type="text" class="form-control" name="unit[]"
                                                        value="<?php echo $item['unit']; ?>" readonly="true" /></td>
                                                <td class="rate">
                                                    <input type="text" class="form-control text-right" name="rate[]"
                                                        value="<?php echo number_format((float) $item['rate'], 2, '.', ''); ?>"
                                                        readonly="true"
                                                        title="<?php echo $item['rate_approve'] == 1 ? $rate_title : ''; ?>" />
                                                </td>
                                                <td><input type="text" class="form-control text-right" name="amount[]"
                                                        value="<?php echo Yii::app()->Controller->money_format_inr($item['amount'], 2, 1); ?>"
                                                        readonly="true" /></td>
                                                <td><input type="text" class="form-control text-right" name="tax_slab[]"
                                                        value="<?php echo $item['tax_slab']; ?>" readonly="true" /></td>
                                                <td><input type="text" class="form-control text-right" name="discount[]"
                                                        value="<?php echo Yii::app()->Controller->money_format_inr($item['disc'], 2, 1); ?>"
                                                        readonly="true" /></td>
                                                <td><input type="text" class="form-control text-right" name="discountp[]"
                                                        value="<?php echo $item['discp']; ?>" readonly="true" /></td>
                                                <td><input type="text" class="form-control text-right" name="cgst[]"
                                                        value="<?php echo Yii::app()->Controller->money_format_inr($item['cgst'], 2, 1); ?>"
                                                        readonly="true" /></td>
                                                <td><input type="text" class="form-control text-right" name="cgstp[]"
                                                        value="<?php echo $item['cgstp']; ?>" readonly="true" /></td>
                                                <td><input type="text" class="form-control text-right" name="sgst[]"
                                                        value="<?php echo Yii::app()->Controller->money_format_inr($item['sgst'], 2, 1); ?>"
                                                        readonly="true" /></td>
                                                <td><input type="text" class="form-control text-right" name="sgstp[]"
                                                        value="<?php echo $item['sgstp']; ?>" readonly="true" /></td>
                                                <td><input type="text" class="form-control text-right" name="igst[]"
                                                        value="<?php echo Yii::app()->Controller->money_format_inr($item['igst'], 2, 1); ?>"
                                                        readonly="true" /></td>
                                                <td><input type="text" class="form-control text-right" name="igstp[]"
                                                        value="<?php echo $item['igstp']; ?>" readonly="true" /></td>
                                                <td><input type="text" class="form-control text-right" name="ttax[]"
                                                        value="<?php echo Yii::app()->Controller->money_format_inr($item['tot_tax'], 2, 1); ?>"
                                                        readonly="true" /></td>
                                                <td><input type="text" class="form-control text-right" name="ttaxp[]"
                                                        value="<?php echo $item['taxpercent']; ?>" readonly="true" /></td>
                                                <td><input type="text" class="form-control text-right" name="totalamount[]"
                                                        value="<?php echo Yii::app()->Controller->money_format_inr(($item['amount'] - $item['disc']) + $item['tot_tax'], 2, 1); ?>"
                                                        readonly="true" /></td>
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                    }
                                    ?>

                                    <?php
                                }

                                if (!empty($additional_items)) {

                                    ?>
                                    <tr>
                                        <td colspan="19" class="categ_td text-center">Additional Items</td>
                                    </tr>
                                    <?php


                                    foreach ($additional_items as $value) {
                                        //echo '<pre>';print_r($value);exit;
                                        ?>
                                        <tr>
                                            <td colspan="19" class="categ_td">
                                                <?php echo !empty($value['category_title']) ? $value['category_title'] : '' ?>
                                            </td>
                                        </tr>
                                        <?php
                                        foreach ($value as $item) {
                                            if (isset($item['specification'])) {
                                                ?>
                                                <tr>
                                                    <input type="hidden" class="form-control" name="ids[]"
                                                        value="<?php echo $item['billitem_id']; ?>" />
                                                    <td width="10px"><input type="text" class="form-control" name="slNo[]"
                                                            value="<?php echo $i; ?>" readonly="true" /></td>
                                                    <td>
                                                        <?php echo $item['specification']; ?><input type="hidden" class="form-control"
                                                            name="category[]" value="<?php echo $item['specification']; ?>" readonly="true" />
                                                    </td>
                                                    <td class="text-center">
                                                        <?php echo $item['hsn_code']; ?><input type="hidden" class="form-control"
                                                            name="hsn_code[]" value="<?php echo $item['hsn_code']; ?>" readonly="true" />
                                                    </td>
                                                    <td class="quantity"><input type="text" class="form-control" name="quantity[]"
                                                            value="<?php echo $item['quantity']; ?>" readonly="true" /></td>
                                                    <td><input type="text" class="form-control" name="unit[]" value="<?php echo $item['unit'];
                                                    ; ?>" readonly="true" /></td>
                                                    <td class="rate"><input type="text" class="form-control text-right" name="rate[]"
                                                            value="<?php echo number_format((float) $item['rate'], 2, '.', ''); ?>"
                                                            readonly="true" /></td>
                                                    <td><input type="text" class="form-control text-right" name="amount[]"
                                                            value="<?php echo Controller::money_format_inr($item['amount'], 2, 1); ?>"
                                                            readonly="true" /></td>
                                                    <td><input type="text" class="form-control text-right" name="tax_slab[]"
                                                            value="<?php echo Controller::money_format_inr($item['tax_slab'], 2, 1); ?>"
                                                            readonly="true" /></td>
                                                    <td><input type="text" class="form-control text-right" name="discount[]"
                                                            value="<?php echo Controller::money_format_inr($item['disc'], 2, 1); ?>"
                                                            readonly="true" /></td>
                                                    <td><input type="text" class="form-control text-right" name="discountp[]"
                                                            value="<?php echo $item['discp']; ?>" readonly="true" /></td>
                                                    <td><input type="text" class="form-control text-right" name="cgst[]"
                                                            value="<?php echo Controller::money_format_inr($item['cgst'], 2, 1); ?>"
                                                            readonly="true" /></td>
                                                    <td><input type="text" class="form-control text-right" name="cgstp[]"
                                                            value="<?php echo $item['cgstp']; ?>" readonly="true" /></td>
                                                    <td><input type="text" class="form-control text-right" name="sgst[]"
                                                            value="<?php echo Controller::money_format_inr($item['sgst'], 2, 1); ?>"
                                                            readonly="true" /></td>
                                                    <td><input type="text" class="form-control text-right" name="sgstp[]"
                                                            value="<?php echo $item['sgstp']; ?>" readonly="true" /></td>
                                                    <td><input type="text" class="form-control text-right" name="igst[]"
                                                            value="<?php echo Controller::money_format_inr($item['igst'], 2, 1); ?>"
                                                            readonly="true" /></td>
                                                    <td><input type="text" class="form-control text-right" name="igstp[]"
                                                            value="<?php echo $item['igstp']; ?>" readonly="true" /></td>
                                                    <td><input type="text" class="form-control text-right" name="ttax[]"
                                                            value="<?php echo Controller::money_format_inr($item['tot_tax'], 2, 1); ?>"
                                                            readonly="true" /></td>
                                                    <td><input type="text" class="form-control text-right" name="ttaxp[]"
                                                            value="<?php echo $item['taxpercent']; ?>" readonly="true" /></td>
                                                    <td><input type="text" class="form-control text-right" name="totalamount[]"
                                                            value="<?php echo Controller::money_format_inr(($item['amount'] - $item['disc']) + $item['tot_tax'], 2, 1); ?>"
                                                            readonly="true" /></td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                        }
                                        ?>
                                        <?php
                                    }
                                }
                            }

                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <?php

                    $addchargesall = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $model->bill_id)->queryAll(); ?>

                    <?php if (!empty($addchargesall)) { ?>
                        <h4>Additional Charges </h4>

                        <?php foreach ($addchargesall as $data) { ?>

                            <div> <span style="font-weight: bold;">
                                    <?= $data['category'] ?>
                                </span> : <span class='totalprice1'>
                                    <?= Controller::money_format_inr($data['amount'], 2) ?>
                                </span></div>
                        <?php }
                    } ?>

                    <div
                        class="col-md-offset-8 col-md-4 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6 text-right amount_values margin-top-5">
                        <div><input type="hidden" name="billamount"
                                value="<?php echo Controller::money_format_inr($model['bill_amount'], 2, 1); ?>"
                                readonly="true" /></div>
                        <div><input type="hidden" name="billdiscount"
                                value="<?php echo Controller::money_format_inr($model['bill_discountamount'], 2, 1); ?>"
                                readonly="true" /></div>
                        <div><input type="hidden" name="billtaxamount"
                                value="<?php echo Controller::money_format_inr($model['bill_taxamount'], 2, 1); ?>"
                                readonly="true" /></td>
                        </div>
                        <div><input type="hidden" name="billtotal"
                                value="<?php echo Controller::money_format_inr($model['bill_totalamount'], 2, 1); ?>"
                                readonly="true" /></div>
                        <div class="row-data"><label>Amount:</label> <span>
                                <?php echo Controller::money_format_inr($model['bill_amount'], 2, 1); ?>
                            </span></div>
                        <div class="row-data"><label>Discount Amount:</label> <span>
                                <?php echo Controller::money_format_inr($model['bill_discountamount'], 2, 1); ?>
                            </span></div>
                        <div class="row-data"><label>Tax Amount</label> <span>
                                <?php echo Controller::money_format_inr($model['bill_taxamount'], 2, 1); ?>
                            </span></div>

                        <?php

                        $tot_amount = 0;

                        $addcharges = Yii::app()->db->createCommand("SELECT ROUND(SUM(amount), 2) as amount FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $model['bill_id'])->queryRow();

                        if (!empty($addcharges)) {
                            $tot_amount = $addcharges['amount'] + $model['bill_totalamount'];
                        }
                        ?>


                        <?php if (!empty($addcharges['amount'])) { ?>
                            <div class="row-data"><label>Additional Charges </label> <span>
                                    <?php echo Yii::app()->Controller->money_format_inr($addcharges['amount'], 2, 1); ?>
                                </span></div>
                        <?php } ?>
                        <div class="row-data"><label>Round Off</label> <span>
                                <?php echo isset($model->round_off) ? $model->round_off : '0'; ?>
                            </span></div>

                        <div class="row-data"><label>Total Amount</label> <span>
                                <?php echo Yii::app()->Controller->money_format_inr($tot_amount, 2, 1); ?>
                            </span></div>
                    </div>
                </div>
            </form>
        </div>
        <script>
            $(document).ready(function () {
                $('#loading').hide();
                $(document).on('focus', '.table input[name="amount[]"]', function (e) {
                    var v1 = '';
                    var v2 = '';
                    var v = '';

                    $(this).closest('tr').find("input[name='rate[]']").each(function () {
                        var va1 = this.value;
                        v1 += va1;
                    });

                    $(this).closest('tr').find("input[name='quantity[]']").each(function () {
                        var va2 = this.value;
                        v2 += va2;

                    });

                    if (!(v1 == '' && v2 == '')) {
                        var v = v1 * v2;
                        $(this).closest('tr').find("input[name='amount[]']").val(v);
                    } else {
                        $(this).closest('tr').find("input[name='amount[]']").val('');
                    }

                });


                $(document).on('focus', '.table input[name="amount[]"]', function (e) {
                    var val = 0;
                    $('.table input[name="amount[]"]').each(function () {
                        val += Number($(this).val());
                    });
                    $('input[name="grand"]').val(val);
                });

            });


            $(document).on('change', 'input[name="invoiceno"]', function () {
                $("#loading").show();
                var inv_no = $(this).val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo $this->createUrl('invoice/checkInvoiceno') ?>",
                    data: {
                        "inv_no": inv_no
                    },
                    dataType: "json",
                    success: function (response) {

                        if (response.response == "fail") {

                            alert("Invoice Number is already in use!");

                        }
                    }

                });
            });

            $(function () {

                $('.save_btn1').click(function () {
                    $("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('invoice/saveinvoice'); ?>");
                    $("form#pdfvals1").submit();
                });

                $('.excelbtn1').click(function () {
                    $("form#pdfvals1").attr('action', "<?php echo $this->createAbsoluteUrl('invoice/exportinvoice'); ?>");
                    $("form#pdfvals1").submit();
                });

                $('.save_btn2').click(function () {
                    $("form#pdfvals2").attr('action', "<?php echo $this->createAbsoluteUrl('bills/savebills'); ?>");
                    $("form#pdfvals2").submit();
                });

                $('.excelbtn2').click(function () {
                    $("form#pdfvals2").attr('action', "<?php echo $this->createAbsoluteUrl('bills/exportbills'); ?>");
                    $("form#pdfvals2").submit();
                });

                $('.addcolumn').on('click', function () {
                    $('.addrow').append('<tr><input type="hidden" class="txtBox pastweek" name="ids[]" /><td><input type="text" class="txtBox pastweek" name="sl_No[]" placeholder="Please click to edit"/></td><td><textarea class="txtBox pastweek" name="description[]"/></textarea></td><td><input type="text" class="txtBox pastweek" name="quantity[]" placeholder="Please click to edit"/></td><td><input type="text" class="txtBox pastweek" name="unit[]" placeholder="Please click to edit"/></td><td><input type="text" class="txtBox pastweek" name="rate[]" placeholder="Please click to edit"/></td><td><input type="text" class="txtBox pastweek amt" name="amount[]" placeholder="Please click to edit"/></td></tr>');
                });

                $(".inputSwitch span").on("click", function () {
                    var $this = $(this);
                    $this.hide().siblings("input").val($this.text()).show();
                });

                $(".inputSwitch input").bind('blur', function () {
                    var $this = $(this);
                    $(this).attr('value', $(this).val());
                    $this.hide().siblings("span").text($this.val()).show();
                }).hide();
            });
            $(document).ajaxComplete(function () {
                $('.loading-overlay').removeClass('is-active');
                $('#loading').hide();
            });
        </script>

        <style>
            a.pdf_excel {
                background-color: #6a8ec7;
                display: inline-block;
                padding: 8px;
                color: #fff;
                border: 1px solid #6a8ec8;
            }
        </style>

        <?php $url = Yii::app()->createAbsoluteUrl("invoice/getclientByProject"); ?>
        <?php Yii::app()->clientScript->registerScript('myscript', '
    $(document).ready(function(){
    $("#project").change(function(){
    var pro_id= $("#project").val();
    $.ajax({   
       url: "' . $url . '",
        data: {"id": pro_id},
        //dataType: "json",
        type: "POST",
        success:function(data){
       // alert(data);
           $("#billto").val(data);

        }

});

});
$(".flash-success").animate({opacity: 1.0}, 5000).fadeOut("slow");

    });
        '); ?>


    </div>
<?php } else {
    $this->renderPartial('billsview', array('model' => $model, 'project' => $project, 'sql' => $sql, 'purchase' => $purchase, 'newmodel' => $newmodel));
} ?>