ALTER TABLE `jp_consumption_request` ADD `date` DATETIME NULL AFTER `type_from`;

INSERT INTO jp_consumption_request (coms_project_id, warehouse_id, wpr_id, task_name, pms_project_id, created_at, updated_at, status, item_added_by, type_from) SELECT coms_project_id, warehouse_id, wpr_id, pms_task_name, pms_project_id, created_date, updated_date, status, item_added_by, type_from FROM pms_acc_wpr_item_consumed WHERE status = 0 GROUP BY wpr_id, warehouse_id;

ALTER TABLE `jp_consumption_request` ADD `pms_wpr_status` INT(11) NOT NULL DEFAULT '0' AFTER `type_from`;
