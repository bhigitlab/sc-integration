
<?php

/**
 * This is the model class for table "{{api_settings}}".
 *
 * The followings are the available columns in table '{{api_settings}}':
 * @property integer $id
 * @property string $api_integration_settings
 * @property string $created_at
 * @property string $updated_at
 */
class ApiSettings extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{api_settings}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('api_integration_settings', 'length', 'max'=>1),
            array('created_at, updated_at', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, api_integration_settings, created_at, updated_at', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'api_integration_settings' => 'PMS Api Integration Settings',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('api_integration_settings',$this->api_integration_settings,true);
        $criteria->compare('created_at',$this->created_at,true);
        $criteria->compare('updated_at',$this->updated_at,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ApiSettings the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function activeModules()
    {
        $result = Yii::app()->db->createCommand()
            ->select('active_modules')
            ->from('jp_api_settings')
            ->where("api_integration_settings = '1'")
            ->limit(1)     // Limiting to 1 row to optimize the query
            ->queryScalar(); // Returns a single scalar value

            return array_map('trim', explode(',', $result)); // Trim spaces
    }

    public function pmsIntegrationStatus()
    {
        return in_array('pms',$this->activeModules()) ;
    }
    public function crmIntegrationStatus()
    {
        return in_array('crm',$this->activeModules()) ;
    }
}