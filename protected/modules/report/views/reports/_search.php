<div class="page_filter">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>
    <div class="search-content">
        <div class="row">
            <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
                <label for="warehouse">Warehouse</label>
                <?php
                $view_stocks_in_other_warehouse_status = Controller::generalSettingsForFunctionsActiveOrInactive($settings_id = 2);
                if ($view_stocks_in_other_warehouse_status == 1) {
                    if (yii::app()->user->role == 5 || yii::app()->user->role == 1) {
                        $warehouse_data = Controller::getCategoryOptions();
                        $data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group');
                    } else {
                        $warehouse_data = Controller::getCategoryOptions($type = 1);
                        $data = CHtml::listData(Controller::getCategoryOptions($type = 1), 'id', 'text', 'group');
                    }
                } else {
                    if (yii::app()->user->role == 1) {
                        $warehouse_data = Controller::getCategoryOptions();
                        $data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group');
                    } else {
                        $warehouse_data = Controller::getCategoryOptions($type = 1);
                        $data = CHtml::listData(Controller::getCategoryOptions($type = 1), 'id', 'text', 'group');
                    }
                }


                if (!empty($model->warehousestock_warehouseid)) {
                    $warehouse = $model->warehousestock_warehouseid;
                } else {
                    if (count($warehouse_data) == 1) {
                        $warehouse = $warehouse_data[0]['id'];
                    } else {
                        $warehouse = '';
                    }
                }
                echo CHtml::dropDownList(
                    'warehouse_id',
                    $warehouse,
                    $data,
                    array('empty' => 'Choose Warehouse ', 'id' => 'warehouse', 'class' => ' select_box inputs target warehouse warehouse-from')
                );
                ?>
            </div>
            <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
                <label for="spec">Specification</label>
                <select id="item_id" class="select_box" name="item_id" class="sel">
                    <option value="">Select Item</option>
                    <?php
                    if (!empty($specification)) {
                        foreach ($specification as $key => $value) {
                            ?>
                            <option value="<?php echo $value['id'] ?>" <?php echo ($value['id'] == $model->warehousestock_itemid) ? 'selected' : ''; ?>>
                                <?php echo $value['data']; ?>
                            </option>
                        <?php }
                    } ?>
                </select>
            </div>
            <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
                <label for="unit">unit</label>
                <select id="unit_id" class="select_box" name="unit_id" class="sel">
                    <option value="">Select Unit</option>
                    <?php
                    if (!empty($units)) {
                        foreach ($units as $key => $value) {
                            ?>
                            <option value="<?php echo $value['id'] ?>" <?php echo ($value['unit_name'] == $model->warehousestock_unit) ? 'selected' : ''; ?>>
                                <?php echo $value['unit_name']; ?>
                            </option>
                        <?php }
                    } ?>
                </select>
            </div>
            <div class="form-group col-xs-12 col-sm-3 col-md-2">
                <label for="from">From</label>
                <?php
                if (!isset($_GET['date_from']) || $_GET['date_from'] == '') {
                    $datefrom = "";
                } else {
                    $datefrom = $_GET['date_from'];
                }
                ?>

                <?php echo CHtml::textField('date_from', $datefrom, array("id" => "date_from", "placeholder" => "Date From", "readonly" => true, "class" => "form-control")); ?>
            </div>
            <div class="form-group col-xs-12 col-sm-3 col-md-2">
                <label for="to">To</label>
                <?php
                if (!isset($_GET['date_to']) || $_GET['date_to'] == '') {
                    $date_to = '';
                } else {
                    $date_to = $_GET['date_to'];
                }
                ?>
                <?php echo CHtml::textField('date_to', $date_to, array("id" => "date_till", "placeholder" => "To", "readonly" => true, "class" => "form-control")); ?>
            </div>
            <div class="form-group col-xs-12 col-sm-3 col-md-2 text-sm-left text-right">
                <label class="d-sm-block d-none">&nbsp;</label>
                <div>
                    <input name="yt0" value="Go" type="submit" class="btn btn-sm btn-primary">
                    <input id="reset" name="yt1" value="Clear" type="reset" class=" btn btn-sm btn-default">
                </div>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script>
    $(function () {
        $("#date_from").datepicker({ dateFormat: 'dd-mm-yy' });

        $("#date_till").datepicker({ dateFormat: 'dd-mm-yy' });

        $("#date_from").change(function () {
            $("#date_till").datepicker('option', 'minDate', $(this).val());
        });

        $("#date_till").change(function () {
            $("#date_from").datepicker('option', 'maxDate', $(this).val());
        });

        $(".select_box").select2();

        $('#reset').click(function () {
            location.href = '<?php echo $this->createUrl('stockStatusReport'); ?>';
        })
    });


</script>