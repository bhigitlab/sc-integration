<?php
Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".info").animate({opacity: 1.0}, 2000).fadeOut("slow");',
    CClientScript::POS_READY
);
?>



<div class="container" id="project">
    <div class="clearfix">
        <h2>General Settings</h2>
    </div>
    <?php if (Yii::app()->user->hasFlash('success')) : ?>
        <div class="info alert alert-success alert-dismissible" role="alert" id="success-alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>
    <div class="panel panel-gray" id="generalform">
        <div class="panel-body">
            <span class="error_message"></span>
            <div class="form">
                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'general-settings-_form-form',
                    'enableAjaxValidation' => false,
                )); ?>

                <div class="row">
                    <div class="col-md-4">
                        <?php echo $form->labelEx($model, 'po_email_from'); ?>
                        <?php echo $form->textField($model, 'po_email_from', array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'po_email_from'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo $form->labelEx($model, 'buyer_module'); ?>
                        <?php echo $form->checkBox($model, 'buyer_module', array('class' => 'd-block')); ?>
                        <?php echo $form->error($model, 'buyer_module'); ?>
                    </div>
                    <div class="col-md-4">
                        <label class="d-block">&nbsp;</label>
                        <?php echo CHtml::submitButton('Update', array('class' => 'btn btn-info btn-sm btn_update')); ?>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
            </div><!-- form -->
        </div>
        <div>
        
    </div>
    </div>

    <div>
        <?php echo $this->renderPartial('_uploadfile'); ?>
    </div>

    <div class="container" id="project">
    <div class="clearfix">
        <h4>No Of Days TO Remove Log</h4>
    </div>
    <?php if (Yii::app()->user->hasFlash('success')) : ?>
        <div class="info alert alert-success alert-dismissible" role="alert" id="success-alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>
    <div class="panel panel-gray" id="generalform">
        <div class="panel-body">
            <span class="error_message"></span>
            <div class="form" >
                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'general-settings-_form-form',
                    'enableAjaxValidation' => false,
                )); ?>

                <div class="row">
                    <div class="col-md-4">
                        <?php echo $form->labelEx($new_model, 'no_of_days'); ?>
                        <?php echo $form->numberField($new_model, 'no_of_days', array('class' => 'form-control log_field', 'min' => 0));?>
                        <?php echo $form->error($new_model, 'no_of_days'); ?>
                    </div>
                   
                    <div class="col-md-4">
                        <label class="d-block">&nbsp;</label>
                        <?php echo CHtml::submitButton('Update', array('class' => 'btn btn-info btn-sm btn_update')); ?>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
            </div><!-- form -->

        </div>
        <div>
        
    </div>
    </div>

    
    
</div>
<div class="container" id="project">
    <div class="clearfix">
        <h4>API INTEGRATION SETTINGS</h4>
    </div>
    <?php if (Yii::app()->user->hasFlash('success')) : ?>
        <div class="info alert alert-success alert-dismissible" role="alert" id="success-alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>
    <div class="panel panel-gray" id="generalform">
        <div class="panel-body">
            <span class="error_message"></span>
            <div class="form" >
                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'general-settings-_form-form',
                    'enableAjaxValidation' => false,
                )); ?>

                <div class="row">
               
                    <div class="col-md-4">
                        <?php echo $form->labelEx($api_settings, 'api_integration_settings'); ?>
                        <?php echo $form->checkBox($api_settings, 'api_integration_settings', array('class' => 'd-block', 'onclick' => 'return false;')); ?>
                        <?php echo $form->error($api_settings, 'api_integration_settings'); ?>
                    </div>
                    <?php 
                    if (defined('GLOBAL_API_DOMAIN') && !empty(GLOBAL_API_DOMAIN)) {
                    ?>
                    <div class="col-md-3" >
                        <label> </label><br>
                        <span style="display: inline-flex; align-items: center;">WhiteList Your App
                         <a href="<?= Yii::app()->request->baseUrl ?>/index.php?r=dashboard/IPWhitelist"
                            class="font-16 fa-solid icon-control-play padding-left-10"
                            style="color:var(--edit) !important;padding-left:10px;" title="Run">
                        </a>
                        </span>
                    </div>
                    <?php } ?>
                   
                    <!-- <div class="col-md-4">
                        <label class="d-block">&nbsp;</label>
                        <php echo CHtml::submitButton('Update', array('class' => 'btn btn-info btn-sm btn_update')); ?>
                    </div> -->
                </div>
                   
                   
                </div>
                <?php $this->endWidget(); ?>
            </div><!-- form -->
            
        </div>
        
        <div>
        
    </div>
    
    </div>

    
    
</div>
<script>

    </script>