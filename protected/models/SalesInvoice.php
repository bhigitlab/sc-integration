<?php

/**
 * This is the model class for table "{{sales_invoice}}".
 *
 * The followings are the available columns in table '{{sales_invoice}}':
 * @property integer $id
 * @property integer $company_id
 * @property integer $project_id
 * @property string $invoice_date
 * @property string $invoice_number
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property Company $company
 * @property Users $createdBy
 * @property Projects $project
 * @property Users $updatedBy
 * @property SalesInvoiceItem[] $salesInvoiceItems
 */
class SalesInvoice extends CActiveRecord
{
	public $total_amount;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_invoice}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('company_id, project_id, client_id, created_by, updated_by', 'numerical', 'integerOnly' => true),
			array('invoice_date, company_id, project_id,invoice_number,client_id', 'required'),
			array('invoice_number', 'length', 'max' => 100),
			array('invoice_date, created_date, updated_date, amount, cgst_p, cgst_amount, sgst_p, sgst_amount, igst_p, igst_amount, cess_p, cess_amount, round_off, total_amount', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, company_id, project_id, invoice_date, invoice_number, created_by, created_date, updated_by, updated_date,client_id', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
			'client' => array(self::BELONGS_TO, 'Clients', 'client_id'),
			'salesInvoiceItems' => array(self::HAS_MANY, 'SalesInvoiceItem', 'sales_invoice_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'company_id' => 'Company',
			'project_id' => 'Project',
			'invoice_date' => 'Invoice Date',
			'invoice_number' => 'Invoice Number',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('company_id', $this->company_id);
		$criteria->compare('project_id', $this->project_id);
		$criteria->compare('client_id', $this->client_id);
		$criteria->compare('invoice_date', $this->invoice_date, true);
		$criteria->compare('invoice_number', $this->invoice_number, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);
		$criteria->order = 'id DESC';
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => false
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SalesInvoice the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getMaxId($filter = null)
	{
		$criteria               = new CDbCriteria;
		$criteria->order = 'id DESC';
		$model  = $this->find($criteria);
		return !empty($model) ? ($model->id) + 1 : '1';
	}

	public function getMainItems($id)
	{
		$items = SalesInvoiceItem::model()->findAllByAttributes(array('sales_invoice_id' => $id));
		return $items;
	}

	public function getSubItems($id)
	{
		$items = SalesInvoiceSubItem::model()->findAllByAttributes(array('invoice_item_id' => $id));
		return $items;
	}
	public function getTotal($model)
	{
		$amount_sum = SalesInvoiceItem::model()->find(array(
			'select' => 'sales_invoice_id, SUM(total_amount) as total_amount',
			'condition' => 'sales_invoice_id=:sales_invoice_id',
			'params' => array(':sales_invoice_id' => $model->id)
		));
		$total_amount = $amount_sum['total_amount'] + $model['cgst_amount'] + $model['sgst_amount'] + $model['igst_amount'] + $model['cess_amount'] + ($model['round_off']);
		return isset($total_amount) ?  Yii::app()->controller->money_format_inr($total_amount, 2) : 0;
	}

	public function getItemTotal($model)
	{
		$amount_sum = SalesInvoiceItem::model()->find(array(
			'select' => 'sales_invoice_id, SUM(total_amount) as total_amount',
			'condition' => 'sales_invoice_id=:sales_invoice_id',
			'params' => array(':sales_invoice_id' => $model->id)
		));
		return isset($amount_sum['total_amount']) ?  Yii::app()->controller->money_format_inr($amount_sum['total_amount'], 2) : 0;
	}

	public function getTotalInvoiceAmount($project_id)
	{
		$invoices = SalesInvoice::model()->findAllByAttributes(array('project_id' => $project_id));
		$total_invoice_amount = 0;
		foreach ($invoices as $invoice) {
			$total_invoice_amount += $this->getTotal($invoice);
		}
		return isset($total_invoice_amount) ?  Yii::app()->controller->money_format_inr($total_invoice_amount, 2) : 0;
	}
}
