<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Create',
);
?>

<div class="panel panel-gray">
    <div class="panel-heading form-head">
        <h3 class="panel-title">Add User</h3>
    </div>
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
