<?php
/* @var $this QuotationCategoryMasterController */
/* @var $model QuotationCategoryMaster */

$this->breadcrumbs=array(
	'Quotation Finish Masters'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List QuotationFinishMaster', 'url'=>array('index')),
	array('label'=>'Create QuotationFinishMaster', 'url'=>array('create')),
	array('label'=>'Update QuotationFinishMaster', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete QuotationFinishMaster', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage QuotationFinishMaster', 'url'=>array('admin')),
);
?>

<h1>View QuotationFinishMaster #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'parent_id',
		'name',
		'status',
		'created_by',
		'created_date',
	),
)); ?>
