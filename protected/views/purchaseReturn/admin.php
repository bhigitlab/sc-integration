<?php $this->breadcrumbs = array( 'performa invoice')?>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<div class="container" id="project">
    <div class="sub-heading margin-bottom-10">
        <h3>Purchase Return</h3>
        <?php if ((isset(Yii::app()->user->role) && (in_array('/purchaseReturn/create', Yii::app()->user->menuauthlist)))) {?>
            <a href="<?php echo Yii::app()->createAbsoluteUrl('purchaseReturn/create'); ?>" class="button btn btn-primary">Add Purchase Return</a>
        <?php } ?>
    </div>

    <?php $this->renderPartial('_search', array('model' => $model)) ?>

    <?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="info"
            style="width: 500px;margin-left: 400px;padding: 8px;color: green;font-size: 20px;font-weight: bold;">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>


    <?php if (Yii::app()->user->hasFlash('error')): ?>
        <div class="info"
            style="width: 500px;margin-left: 400px;padding: 8px;color: red;font-size: 20px;font-weight: bold;">
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
    <?php endif; ?>

    <?php
    $amount = 0;
    $total_amount = 0;
    foreach ($dataProvider->getData() as $record) {
        $amount += $record->return_amount;
        $total_amount += $record->return_totalamount;
    }
    ?>

    <div class="table-wrapper">
        <?php
        $dataProvider->sort->defaultOrder = 'bill_id DESC';
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_view',
            'enableSorting' => 1,
            'viewData' => array('amount' => $amount, 'total_amount' => $total_amount),
            'template' => '{summary}<div id="parent"><table cellpadding="10" class="table" id="billtable2">{items}</table></div>{pager}'
        ));
        ?>
    </div>
</div>

<style>
    a.button {
        background-color: #6a8ec7;
        display: block;
        padding: 5px;
        color: #fff;
        cursor: pointer;
        float: right;
        border: 1px solid #6a8ec8;
    }

    table.dataTable tbody td {
        padding: 12px 10px;
    }

    thead th,
    tfoot th {
        background-color: #eee;
    }

    .table {
        margin-bottom: 0px;
    }
</style>
<script>
    $(document).ready(function () {
        $("#billtable2").tableHeadFixer({ 'left': false, 'foot': true, 'head': true });

        $(".popover-test").popover({
            html: true,
            content: function () {
                return $(this).next('.popover-content').html();
            }
        });

        $('[data-toggle=popover]').on('click', function (e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function (e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function (e) {
            $('[data-toggle=popover]').each(function () {

                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
        $(document).ajaxComplete(function () {
            $(".popover-test").popover({
                html: true,
                content: function () {
                    return $(this).next('.popover-content').html();
                }
            });

        });

    });
</script>