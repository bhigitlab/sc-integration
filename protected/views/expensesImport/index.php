<?php
/* @var $this ExpensesImportController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Expenses Imports',
);

$this->menu=array(
	array('label'=>'Create ExpensesImport', 'url'=>array('create')),
	array('label'=>'Manage ExpensesImport', 'url'=>array('admin')),
);
?>

<h1>Expenses Imports</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
