<?php

/**
 * This is the model class for table "{{remark}}".
 *
 * The followings are the available columns in table '{{remark}}':
 * @property integer $remark_id
 * @property string $remark_remark
 * @property integer $remark_postedby
 * @property string $remark_posteddate
 * @property integer $purchase_id
 * @property integer $remark_status
 */
class Remark extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{remark}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('remark_remark, remark_postedby, remark_posteddate, purchase_id, remark_status', 'required'),
			array('remark_postedby, purchase_id, remark_status', 'numerical', 'integerOnly'=>true),
			array('remark_remark', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('remark_id, remark_remark, remark_postedby, remark_posteddate, purchase_id, remark_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'remark_id' => 'Remark',
			'remark_remark' => 'Remark Remark',
			'remark_postedby' => 'Remark Postedby',
			'remark_posteddate' => 'Remark Posteddate',
			'purchase_id' => 'Purchase',
			'remark_status' => 'Remark Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('remark_id',$this->remark_id);
		$criteria->compare('remark_remark',$this->remark_remark,true);
		$criteria->compare('remark_postedby',$this->remark_postedby);
		$criteria->compare('remark_posteddate',$this->remark_posteddate,true);
		$criteria->compare('purchase_id',$this->purchase_id);
		$criteria->compare('remark_status',$this->remark_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Remark the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
