<?php

/**
 * This is the model class for table "{{deposit}}".
 *
 * The followings are the available columns in table '{{deposit}}':
 * @property integer $deposit_id
 * @property string $deposit_name
 */
class Deposit extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{deposit}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('deposit_name', 'required'),
			array('deposit_name', 'length', 'max'=>100),
			array('deposit_name', 'unique'),
                        //array('deposit_name', 'checkexist'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('deposit_id, deposit_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'deposit_id' => 'Deposit',
			'deposit_name' => 'Deposit Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('deposit_id',$this->deposit_id);
		$criteria->compare('deposit_name',$this->deposit_name,true);
		$user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                foreach($arrVal as $arr) {
                    if ($newQuery) $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET('".$arr."', company_id)";
                }
                $criteria->addCondition($newQuery);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>false,
		));
	}
        
        public function checkexist($attribute, $params) {

            $id = Deposit::model()->findByAttributes(array('deposit_id' => $this->deposit_id));
            if(!empty($id)){
                $name = Deposit::model()->findByAttributes(array('deposit_name' => $this->deposit_name, 'company_id' => Yii::app()->user->company_id),array('condition'=>'deposit_id != '.$this->deposit_id.''));
                if(!empty($name)) {
                    $this->addError("deposit_name", ("Deposit already exists"));
               }
            } else{
                $name = Deposit::model()->findByAttributes(array('deposit_name' => $this->deposit_name, 'company_id' => Yii::app()->user->company_id));
                if(!empty($name)) {
                    $this->addError("deposit_name", ("Deposit already exists"));
                }
            }
        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Deposit the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
