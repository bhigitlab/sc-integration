<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/tablestyles.css" />
<h1>Expense Details</h1>

<table class="table table-bordered table-striped tab1" style="width:100%;border:1px solid gray;">

    <thead>
        <tr>
          
            <th>Sl No.</th>
            <th>Date</th>         
            <th>Description</th>
            <th>Expense Type</th>
            <th>Credit Amount</th>
            <th>Debit Amount</th>
            
        </tr>   
    </thead>
    <tbody>
        
       
        <?php
        $i = 0;
        if ($model == NULL) {
            echo '<tr><td colspan="2">No records Found</td></tr>';
        } else { ?>          
        <tr>
			<td></td>
			<td></td>
			<td></td>
			<td class="text-right"><b>Grand Total : </b></td>
			<td class="text-right"><?php echo Controller::money_format_inr(Yii::app()->user->totalcredit[0]['sumamt'],2); ?></td>
			<td class="text-right"><?php echo Controller::money_format_inr(Yii::app()->user->totaldebit[0]['sumamt'],2); ?></td>
		</tr>
            
            <?php
           foreach ($model as $data) {
                $i++;
               ?>
       
             <tr>
       
				<td class="text-right"  ><?php echo $i; ?></td>
				<td style="width:80px;"><?php
					if ($data['date'] != "") {
						echo date("d-M-Y", strtotime($data['date']));
					}
					?>
				 </td>
				 <td><?php echo $data['description']; ?></td>
				 <td><?php echo $data['name']; ?></td>
				 
					 <?php if($data['exp_type'] == 0) { ?>
					 <td><?php echo ($data['amount']!='')?Controller::money_format_inr($data['amount'],2):'0.00'; ?></td>
					 <?php } else {?>
					 <td></td>
					 <?php } ?>
				 
					 <?php if($data['exp_type'] == 1) { ?>
					 <td><?php echo ($data['amount'] !='')?Controller::money_format_inr($data['amount'],2):'0.00'; ?></td>
					 <?php } else {?>
					  <td></td>
					 <?php } ?>
        
			</tr> 
                <?php
            }
        }
        ?>
    </tbody>
</table>

