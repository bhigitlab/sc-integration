<?php

class ExpensetypeController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        /*return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'newlist'),
                'users' => array('@'),
                'expression' => 'yii::app()->user->role<=3',
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'savetopdf','savetoexcel'),
                'users' => array('@'),
                'expression' => 'yii::app()->user->role<=2',
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
                'expression' => 'yii::app()->user->role==1',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );*/
        $accessArr = array();
        $accessauthArr = array();
        $controller = Yii::app()->controller->id;
        if (isset(Yii::app()->user->menuall)) {
            if (array_key_exists($controller, Yii::app()->user->menuall)) {
                $accessArr = Yii::app()->user->menuall[$controller];
            }
        }

        if (isset(Yii::app()->user->menuauth)) {
            if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                $accessauthArr = Yii::app()->user->menuauth[$controller];
            }
        }
        $access_privlg = count($accessauthArr);
        return array(
            array(
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => $accessArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),

            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        if(!isset($_POST['ExpenseType']['expense_type']) or $_POST['ExpenseType']['expense_type']!=96){
            $model = new ExpenseType('ignore_template_type');           
        }else{
            $model = new ExpenseType;
        }        

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['ExpenseType'])) {
            $model->attributes = $_POST['ExpenseType'];
            // $model->labour_label = $_POST['ExpenseType']['labour_label'];
            $model->wage_label =0;
            // $model->wage_rate_label = $_POST['ExpenseType']['wage_rate_label'];
            $model->helper_label = 0;
            // $model->helper_labour_label = $_POST['ExpenseType']['helper_labour_label'];
            $model->lump_sum_label = 0;
            // $model->labour_status = $_POST['ExpenseType']['labour_status'];
            // $model->wage_status = $_POST['ExpenseType']['wage_status'];
            // $model->wagerate_status = $_POST['ExpenseType']['wagerate_status'];
            // $model->helper_status = $_POST['ExpenseType']['helper_status'];
            // $model->helperlabour_status = $_POST['ExpenseType']['helperlabour_status'];
            // $model->lump_sum_status = $_POST['ExpenseType']['lump_sum_status'];
            $model->company_id   = Yii::app()->user->company_id;
            if ($model->save()){
               $projects = Projects::model()->findAll('auto_update = :auto_update', [':auto_update' => 1]);
                if(!empty($projects)){
                    foreach($projects as $project){
                        $providedExptype=$model->type_id;
                       $checkExist = ProjectExpType::model()->exists('type_id = :type_id AND project_id = :project_id', [
                            ':type_id' => $providedExptype,
                            ':project_id' => $project->pid
                        ]);

                        if(empty($checkExist)){
                            $proj_exp = new ProjectExpType();
                            $proj_exp->project_id=$project->pid;
                            $proj_exp->type_id=$providedExptype;
                            $proj_exp->save();

                        }

                    }
                    

                }

                if(isset($_REQUEST['popup'])){
                    $type_id = $model->type_id;
                    $project_id = $_REQUEST['projectid'];

                    $sql = "INSERT INTO `jp_project_exptype` "
                    . " (`expid`, `project_id`, `type_id`) "
                    . "  VALUES (NULL, $project_id, $type_id)";
                    Yii::app()->db->createCommand($sql)->execute();

                    $tblpx = Yii::app()->db->tablePrefix;
                    $user = Users::model()->findByPk(Yii::app()->user->id);
                    $arrVal = explode(',', $user->company_id);
                    $newQuery = "";
                    foreach ($arrVal as $arr) {
                        if ($newQuery)
                            $newQuery .= ' OR';
                        $newQuery .= " FIND_IN_SET('" . $arr . "', v.company_id)";
                    }

                    $sql = "SELECT vendor_id  FROM " . $tblpx . "vendors v  "
                    . " WHERE 1=1  AND ($newQuery)";
                    $vendorData = Yii::app()->db->createCommand($sql)->queryAll();

                    foreach ($vendorData as $key => $value) {
                        $vendorId = $value['vendor_id'];
                        $sql = "INSERT INTO `jp_vendor_exptype` "
                            . " (`exp_id`, `vendor_id`, `type_id`) "
                            . "  VALUES (NULL, $vendorId, $type_id)";
                        Yii::app()->db->createCommand($sql)->execute();
                    }
                    
                    
                }
                $this->redirect(array('newList'));
            }
        }
        if (isset($_GET['layout']))
            $this->layout = false;
        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['ExpenseType'])) {
            $model->attributes = $_POST['ExpenseType'];
            $model->company_id   = Yii::app()->user->company_id;
            if ($model->template_type == 1) {
                $model->helper_label = $model->helper_labour_label = $model->lump_sum_label = $model->helper_status = $model->helperlabour_status = $model->lump_sum_status = NULL;
            } elseif ($model->template_type == 2) {
                $model->wage_label = $model->lump_sum_label = $model->wage_status = $model->lump_sum_status = NULL;
            } elseif ($model->template_type == 3) {
                $model->helper_labour_label = $model->lump_sum_label = $model->helperlabour_status =  $model->lump_sum_status = '';
            } elseif ($model->template_type == 4) {
                $model->labour_label = $model->wage_label = $model->wage_rate_label = $model->helper_label = $model->helper_labour_label = $model->labour_status = $model->wage_status = $model->wagerate_status = $model->helper_status = $model->helperlabour_status = NULL;
            } else {
                $model->labour_label = $model->wage_label = $model->wage_rate_label = $model->helper_label = $model->helper_labour_label = $model->labour_status = $model->wage_status = $model->wagerate_status = $model->helper_status = $model->helperlabour_status = $model->lump_sum_label = NULL;
            }
            if ($model->save())
                //$this->redirect(array('newList'));
                $projects = Projects::model()->findAll('auto_update = :auto_update', [':auto_update' => 1]);
                if(!empty($projects)){
                    foreach($projects as $project){
                        $providedExptype=$model->type_id;
                       $checkExist = ProjectExpType::model()->exists('type_id = :type_id AND project_id = :project_id', [
                            ':type_id' => $providedExptype,
                            ':project_id' => $project->pid
                        ]);

                        if(empty($checkExist)){
                            $proj_exp = new ProjectExpType();
                            $proj_exp->project_id=$project->pid;
                            $proj_exp->type_id=$providedExptype;
                            $proj_exp->save();

                        }

                    }
                    

                }

                if (Yii::app()->user->returnUrl != 0) {
                    $this->redirect(array('newlist', 'ExpenseType_page' => Yii::app()->user->returnUrl));
                } else {
                    $this->redirect(array('newlist'));
                }
        }
        if (isset($_GET['layout']))
            $this->layout = false;

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    	public function actionDelete($id)
    	{
    		$model = $this->loadModel($id);
            try{
                $sql1 = "SELECT count(*) FROM `jp_expenses` WHERE `exptype` =".$id;
				$relatedExpensesCount = Yii::app()->db->createCommand($sql1)->queryScalar();
                $sql2 = "SELECT count(*) FROM `jp_dailyexpense` WHERE `expensehead_id` =".$id;
				$relatedDailyExpensesCount = Yii::app()->db->createCommand($sql2)->queryScalar();
                if(!$relatedExpensesCount && !$relatedDailyExpensesCount){    
                    if ($model->delete()) {
                        $response = [
                            'success' => 'success',
                            'message' => 'Record deleted successfully.'
                        ];
                    } else {
                        throw new Exception(json_encode($model->getErrors()));
                    
                    }
                }else{
                    $error_message = "You cannot delete this record because it is associated with other records.";
                    throw new Exception(json_encode($error_message));
                }
            }catch(Exception $e){
                $error_message = "You cannot delete this record because it is associated with other records.";
                $response = [
                    'success' => 'danger',
                    'message' =>$error_message
                ];
            }            
            echo json_encode($response);
            }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('ExpenseType');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new ExpenseType('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ExpenseType']))
            $model->attributes = $_GET['ExpenseType'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ExpenseType the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = ExpenseType::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param ExpenseType $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'expense-type-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionNewlist()
    {

        $model = new ExpenseType('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ExpenseType']))
            $model->attributes = $_GET['ExpenseType'];


        if (Yii::app()->user->role == 1) {

            $this->render('newlist', array(
                'model' => $model,
                'dataProvider' => $model->search(),
            ));
        } else {
            $this->render('newlist', array(
                'model' => $model,
                'dataProvider' => $model->search(Yii::app()->user->id),
            ));
        }
    }

    public function actionSavetopdf()
    {
        $this->logo = $this->realpath_logo;
        $tblpx = Yii::app()->db->tablePrefix;
        $qry = "SELECT * FROM " . $tblpx . "expense_type";
        $data = Yii::app()->db->createCommand($qry)->queryAll();

        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
        $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $mPDF1->AddPage('','', '', '', '', 0,0,40, 30, 10,0);
        $mPDF1->WriteHTML($this->renderPartial('expensetypepdf', array(
            'model' => $data,
        ), true));
        $filename = "Expense Heads";
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    public function actionSavetoexcel()
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $qry = "SELECT * FROM " . $tblpx . "expense_type";
        $data = Yii::app()->db->createCommand($qry)->queryAll();
        $finaldata = array();
        $arraylabel = array('Sl No.', 'Expense Head');
        $i = 1;
        $finaldata = array();
        foreach ($data as $key => $datavalue) {
            $finaldata[$key][] = $i;
            $finaldata[$key][] = $datavalue['type_name'];
            $i++;
        }

        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'expense_head.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }
}
