<?php

/**
 * This is the model class for table "{{purchase_sub_category}}".
 *
 * The followings are the available columns in table '{{purchase_sub_category}}':
 * @property integer $id
 * @property string $sub_category_name
 * @property string $company_id
 * @property integer $created_by
 * @property string $created_date
 */
class PurchaseSubCategory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{purchase_sub_category}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sub_category_name,company_id', 'required'),
			array('created_by', 'numerical', 'integerOnly'=>true),
			array('sub_category_name', 'length', 'max'=>100),
			array('sub_category_name','checkexist'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sub_category_name, company_id, created_by, created_date', 'safe', 'on'=>'search'),
		);
	}
	public function checkexist() {
        $id = PurchaseSubCategory::model()->findByAttributes(array('id' => $this->id));
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        if (!empty($id)) {
            $name = PurchaseSubCategory::model()->findByAttributes(array('sub_category_name' => $this->sub_category_name), array('condition' => 'id != ' . $this->id . ' AND(' . $newQuery . ')'));
            if (!empty($name)) {
                $this->addError("sub_category_name", ("Sub Category name already exists"));
            }
        } else {
            $name = PurchaseSubCategory::model()->findByAttributes(array('sub_category_name' => $this->sub_category_name), array('condition' => $newQuery));
            if (!empty($name)) {
                $this->addError("sub_category_name", ("Sub Category name already exists"));
            }
        }
    }


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sub_category_name' => 'Sub Category Name',
			'company_id' => 'Company',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sub_category_name',$this->sub_category_name,true);
		$criteria->compare('company_id',$this->company_id,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);

		$user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        $criteria->order = 'id DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => false,
        ));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PurchaseSubCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
