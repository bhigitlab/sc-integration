<div class="container">
    
    <div class="expenses-heading">
        <h3>Pending Deliveries</h3>
    </div>
    <div id="table-wrapper " class="table-height-adj">      
        <table class="table mb-0">
            <thead class="entry-table">        
                <tr class="first-thead-row">
                    <th>Warehouse</th>
                    <th>Date</th>
                    <th>Bill Number / Description</th>
                    <th colspan="2" width="20%">Total Amount</th>
                </tr>
            </thead>
            <tbody>
                <?php                                         
                foreach ($data as $key => $value) { 
                    $i=0;
                    $pending_data = $data[$key];
                    $warehouse = $key;                    
                ?>
                <tr>                                    
                    <?php                 
                    foreach ($pending_data as $key => $value) { 
                        if($i==$key){
                    ?>
                    <td rowspan="<?php echo count($pending_data) ?>" width ="23.1%;"> <?php echo $warehouse ?></td> 
                    <?php } ?>                         
                    <td width="12.21%"><?php echo Yii::app()->controller->changeDateForamt($value['bill_date']); ?></td>
                    <td width="45.6%">
                        <?php 
                        echo CHtml::link($value['bill_number'], 'index.php?r=bills/view&id=' . $value['bill_id'], array('class' => 'link', 'target' => '_blank'))                        
                        ?>
                    </td>
                    <td class="text-right">
                        <?php echo Yii::app()->Controller->money_format_inr($value['bill_totalamount'],2); ?>
                    </td>
                    <td class="text-right">
                    <?php 
                        echo CHtml::link('Add Receipt', 'index.php?r=wh/warehousereceipt/create&bill_id=' . $value['bill_id'], array('class' => 'btn btn-default btn-sm', 'target' => '_blank'))                        
                        ?>
                    
                    </td>
                </tr>             
                <?php 
                }
                $i++;
                }             
                ?>                
            </tbody>
        </table>
    </div>
</div>
