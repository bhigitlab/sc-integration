<div class="container">
  <div class="expenses-heading mb-10">
    <h3>Export Data</h3>
  </div>
  <?php
  if (isset($_GET['fromdate']) || isset($_GET['todate'])) {
    $date_from = $_GET['fromdate'];
    $date_to = $_GET['todate'];
  }
  $fromdate = (!empty($date_from) ? date('d-M-Y', strtotime($date_from)) : '');
  $todate = (!empty($date_to) ? date('d-M-Y', strtotime($date_to)) : '');
  ?>
  <div class="page_filter custom-form-style">
    <div class="row">
      <div class="form-group col-xs-12 col-sm-3 col-md-2">
        <!-- <div class="display-flex"> -->
        <label class="margin-top-5">From</label>
        <?php echo CHtml::textField('date_from', (!empty($date_from) ? date('d-M-Y', strtotime($date_from)) : ''), array("id" => "date_from", 'class' => 'form-control', "readonly" => true, 'placeholder' => 'Date From', 'style' => 'display:inline-block', 'autocomplete' => 'off')); ?>
        <!-- </div> -->
      </div>
      <div class="form-group col-xs-12 col-sm-3 col-md-2">
        <!-- <div class="display-flex"> -->
        <label class="margin-top-5">To</label>
        <?php echo CHtml::textField('date_to', (!empty($date_to) ? date('d-M-Y', strtotime($date_to)) : ''), array("id" => "date_to", 'class' => 'form-control', "readonly" => true, 'placeholder' => 'Date To', 'style' => 'display:inline-block', 'autocomplete' => 'off')); ?>
        <!-- </div> -->
      </div>
      <div class="form-group col-xs-12 col-sm-3 col-md-2 text-sm-left text-right">
        <label class="d-sm-block d-none">&nbsp;</label>
        <div>
          <?php
          echo CHtml::submitButton('GO', array('id' => 'btSubmit', 'class' => 'btn btn-sm btn-primary'));
          ?>
          <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('exportdata/index') . '"', 'class' => 'btn btn-sm btn-default')); ?>
        </div>
      </div>
    </div>
  </div>
  <?php if (!empty($fromdate) || !empty($todate)) { ?>
    <div>
      <ul>
        <?php if ((isset(Yii::app()->user->role) && (in_array('/exportdata/savetoexcelsales', Yii::app()->user->menuauthlist)))) { ?>
          <li>Sales :
            <?php if ($sales_count != 0) { ?>
              <a style="cursor:pointer;" id="download" class=" btn-sm "
                href="<?php echo $this->createAbsoluteUrl('exportdata/savetoexcelsales', array("fromdate" => $fromdate, "todate" => $todate)); ?>">DOWNLOAD</a>
            <?php } else {
              echo "No data available!";
            } ?>
          </li>
        <?php } ?>
        <?php if ((isset(Yii::app()->user->role) && (in_array('/exportdata/savetoexcelpurchase', Yii::app()->user->menuauthlist)))) { ?>
          <li>Purchase :
            <?php if ($count_purchase != 0) { ?>
              <a style="cursor:pointer;" id="download" class=" btn-sm "
                href="<?php echo $this->createAbsoluteUrl('exportdata/savetoexcelpurchase', array("fromdate" => $fromdate, "todate" => $todate)); ?>">DOWNLOAD</a>
            <?php } else {
              echo "No data available!";
            } ?>
          </li>
        <?php } ?>
        <?php if ((isset(Yii::app()->user->role) && (in_array('/exportdata/savetoexcelexpense', Yii::app()->user->menuauthlist)))) { ?>
          <li>Receipt :
            <?php if ($count_expense != 0) { ?>
              <a style="cursor:pointer;" id="download" class=" btn-sm "
                href="<?php echo $this->createAbsoluteUrl('exportdata/savetoexcelexpense', array("fromdate" => $fromdate, "todate" => $todate)); ?>">DOWNLOAD</a>
            <?php } else {
              echo "No data available!";
            } ?>
          </li>
        <?php } ?>

        <?php if ((isset(Yii::app()->user->role) && (in_array('/exportdata/savetoexcelpayment', Yii::app()->user->menuauthlist)))) { ?>
          <li>Payment :
            <?php if ($payment_count != 0) { ?>
              <a style="cursor:pointer;" id="download" class=" btn-sm "
                href="<?php echo $this->createAbsoluteUrl('exportdata/savetoexcelpayment', array("fromdate" => $fromdate, "todate" => $todate)); ?>">DOWNLOAD</a>
            <?php } else {
              echo "No data available!";
            } ?>
          </li>
        <?php } ?>
        <?php if ((isset(Yii::app()->user->role) && (in_array('/exportdata/savetoexcelcontra', Yii::app()->user->menuauthlist)))) { ?>
          <li>CONTRA :
            <?php if ($contra_count != 0) { ?>
              <a style="cursor:pointer;" id="download" class=" btn-sm "
                href="<?php echo $this->createAbsoluteUrl('exportdata/savetoexcelcontra', array("fromdate" => $fromdate, "todate" => $todate)); ?>">DOWNLOAD</a>
            <?php } else {
              echo "No data available!";
            } ?>
          </li>
        <?php } ?>
        <?php if ((isset(Yii::app()->user->role) && (in_array('/exportdata/savetoexcelreturn', Yii::app()->user->menuauthlist)))) { ?>
          <li>Purchase Return :
            <?php if ($count_return != 0) { ?>
              <a style="cursor:pointer;" id="download" class=" btn-sm "
                href="<?php echo $this->createAbsoluteUrl('exportdata/savetoexcelreturn', array("fromdate" => $fromdate, "todate" => $todate)); ?>">DOWNLOAD</a>
            <?php } else {
              echo "No data available!";
            } ?>
          </li>
        <?php } ?>
      </ul>
    </div>
  <?php } ?>
</div>
<script>
  $(function () {
    $("#date_from").datepicker({
      dateFormat: 'dd-M-yy',
      maxDate: $("#date_to").val()
    });
    $("#date_to").datepicker({
      dateFormat: 'dd-M-yy',
    });
    $("#btSubmit").click(function () {
      var datefrom = $("#date_from").val();
      var dateto = $("#date_to").val();
      location.href = "<?php echo $this->createUrl('exportdata/index'); ?>" + "&fromdate=" + datefrom + "&todate=" + dateto;
    });
  });
</script>