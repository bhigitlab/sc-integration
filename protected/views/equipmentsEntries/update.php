<?php
/* @var $this EquipmentsEntriesController */
/* @var $model EquipmentsEntries */

$this->breadcrumbs=array(
	'Equipments Entries'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EquipmentsEntries', 'url'=>array('index')),
	array('label'=>'Create EquipmentsEntries', 'url'=>array('create')),
	array('label'=>'View EquipmentsEntries', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EquipmentsEntries', 'url'=>array('admin')),
);
?>

<h1>Update EquipmentsEntries <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>