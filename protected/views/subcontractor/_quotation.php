<?php
/* @var $this SalesInvoiceController */
/* @var $model SalesInvoice */

$this->breadcrumbs = array(
    'Sales Invoices' => array('index'),
    'Create',
);
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<div class="container">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="expenses-heading">
        <div class="sub-heading">
            <?php if ($model->isNewRecord) { ?>
                <h3>Create Subcontractor Quotation</h3>
            <?php } else { ?>
                <h3>Update Subcontractor Quotation</h3>
            <?php }
            ?>
            <?php echo CHtml::link('Quotations', array('subcontractor/quotations'), array('class' => 'btn btn-primary ')); ?>
        </div>
        <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>

    </div>
    <?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="flash-holder flash-success">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>
    <div id="myflashwrapper" style="display: none;"></div>

    <?php if (Yii::app()->user->hasFlash('error')): ?>
        <div class="alert alert-danger  alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
    <?php endif; ?>
    <div id="main_div">
        <?php $this->renderPartial('_quotation_form', array('model' => $model, 'user_companies' => $user_companies, 'main_item_model' => $main_item_model, 'sub_item_model' => $sub_item_model, 'payment_model' => $payment_model, 'payment_entries' => $payment_entries, 'scq_no' => $scq_no, 'serial_number' => $serial_number, 'terms' => $terms));
        ?>
    </div>

</div>
<script>
    $(function () {
        var id = "<?php echo isset($_GET['scquotation_id']) ? $_GET['scquotation_id'] : '' ?>";
        if (id == "") {
            $("#Scquotation_scquotation_date").trigger("change");
            $("#Scquotation_scquotation_date").datepicker('option', 'maxDate', new Date());
        } else {
            $("#Scquotation_completion_date").datepicker('option', 'minDate', $("#Scquotation_scquotation_date").val());
            $("#Scquotation_scquotation_date").datepicker('option', 'maxDate', $("#Scquotation_completion_date").val());
        }
    })
    $(document).ready(function () {
        $('#loading').hide();
    })
    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });

    function onlySpecialchars(str) {
        var regex = /^[^a-zA-Z0-9]+$/;
        var message = "";
        var matchedAuthors = regex.test(str);

        if (matchedAuthors) {
            var message = "Special characters not allowed";
        }
        return message;
    }


    $("#Scquotation_scquotation_no").keyup(function () {
        var message = onlySpecialchars(this.value);
        $(this).siblings(".errorMessage").show().html(message);
        $(this).siblings(".errorMessage").addClass('d-block');
    });
</script>