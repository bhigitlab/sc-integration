ALTER TABLE `jp_subcontractorbill` ADD CONSTRAINT `fk_stage_id` FOREIGN KEY (`stage_id`) REFERENCES `jp_sc_payment_stage`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `jp_subcontractor_payment` ADD CONSTRAINT `fk_payment_stage_id` FOREIGN KEY (`stage_id`) REFERENCES `jp_sc_payment_stage`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `jp_payment_stage` ADD `delete_approve_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>approved,1=>Pending for approval' AFTER `invoice_status`;

ALTER TABLE `jp_sc_payment_stage` ADD `delete_approve_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>approved,1=>Pending for approval' AFTER `bill_status`;

ALTER TABLE `jp_scquotation_items` ADD CONSTRAINT `fk_sc_quotation_id` FOREIGN KEY (`scquotation_id`) REFERENCES `jp_scquotation`(`scquotation_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `jp_terms_conditions` ADD `type` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0=>sales,1=>subcontractor' AFTER `template_id`;

