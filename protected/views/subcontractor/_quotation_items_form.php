<?php
if (!$model->isNewRecord) {
    $category_id = isset($main_item_model) ? $main_item_model->id : '';
    $category_total = ScquotationItems::model()->getCategoryTotal($model->scquotation_id, $category_id);
    $key_val = isset($main_item_model) ? $main_item_model['id'] : '0';
?>
    <div class="panel panel-default addItem">
        <div class="row padding-bottom-0 categ_toggle" data-toggle="collapse" data-target="#collapse_<?php echo isset($main_item_model) ? $main_item_model['id'] : '0'; ?>">
            <div class="col-xs-12">
                <div class="summary-wrapper">
                    <div class="final-value-label"><?php echo isset($main_item_model) ? $main_item_model['main_title'] : ''; ?></div>

                    <div class="input-info pull-right">
                        <label class="final-section-label">Total Items:</label>
                        <span class="final-value-label">1</span>
                       
                        <label class="final-section-label">Total:</label>
                              <span class="final-value-label"><?php echo $category_total; ?></span>
                              <i class="fa fa-edit nav-view" title="Edit"></i>
                              <i class="fa fa-trash nav-view delete-category" title="Delete" id="<?php echo $category_id; ?>"></i>
                              <button type="button" class="btn btn-default btn-sm quotation-add" style="margin: -6px 10px;"> 
                            Add Items<i class="toggle-caret fa fa-angle-up"></i>
                        </button>
                    </div>
                    <!-- <div class="pull-right"> -->
                        <!-- <label>Total:</label>
                        <span class="total_sum" id="total_sum" style="padding-right: 19px;"></span> -->
                        <!-- <input type="text" name="" value="php echo $category_total; ?>" class="total_sum" id="total_sum"> -->
                    <!-- </div> -->
                </div>
                
            </div>
        </div>
        <div id="collapse_<?php echo isset($main_item_model) ? $main_item_model['id'] : '0'; ?>" class="panel-collapse collapse">
            <div class="panel-body">
                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'sc-quotation-item-category-form' . $key_val,
                    'enableAjaxValidation' => false,
                    'htmlOptions' => array(
                        'class' => 'category_form',
                    )
                )); ?>
                <input type="hidden" name="ScQuotationItemCategory[sc_quotaion_id]" id="ScQuotationItemCategory_sc_quotaion_id" value="<?php echo $model->scquotation_id; ?>">
                <input type="hidden" name="ScQuotationItemCategory[id]" id="ScQuotationItemCategory_id" value="<?php echo isset($main_item_model) ? $main_item_model['id'] : ''; ?>">
                <div class="row">
                    <div class="col-md-2">
                        <?php echo $form->labelEx($main_item_model, 'main_title'); ?>
                        <?php
                        echo $form->textField($main_item_model, 'main_title',  array('class' => 'form-control require'));
                        ?>
                        <div class="text-danger hide">Field required</div>
                    </div>
                    <div class="col-md-6">
                        <label for="">Description</label>
                        <?php
                        echo $form->textArea($main_item_model, 'main_description',  array('class' => 'form-control require text_editor','data-key-val' => $key_val, 'id' => 'ScQuotationItemCategory_main_description-' . $key_val));
                        ?>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="add-item-block">
                        <div class="row">
                            <div class="col-xs-6">
                            <div class="heading-title">Add Item</div>
                            </div>
                            <div class="col-xs-6">
                            <!-- <button type="button" class="btn btn-sm btn-secondary pull-right margin-top-10">
                                +
                            </button> -->
                            <a href="" class="add_new_row btn btn-sm btn-secondary pull-right margin-top-50" style="margin-right: 20px;" id="row-<?php echo $category_id; ?>">Add More Item</a>
                            
                            </div>
                            <div class="col-xs-12">
                            <div class="dotted-line"></div>
                            </div>
                            
                            
                        </div>

                        <div class="row">
                            <div class="panel-body">
                                <?php $form = $this->beginWidget('CActiveForm', array(
                                'id' => 'scquotation-items-form',
                                'action' => array('addQuotationItems'),
                                'enableAjaxValidation' => false,
                                'htmlOptions' => array(
                                    'class' => '',
                                )
                                )); ?>
                                <input type="hidden" name="ScquotationItems[scquotation_id]" id="ScquotationItems_scquotation_id" value="<?php echo $model->scquotation_id; ?>">
                                <input type="hidden" name="ScquotationItems[item_category_id]" id="ScquotationItems_item_category_id" value="<?php echo $category_id; ?>">
                                <div class="container-fluid quotation_items_form">
                                    <div class="row form_datas">
                                        <div class="col-md-6 desc_sec">
                                            <input type="hidden" name="ScquotationItems[item_id][]" id="ScquotationItems_item_id" value="">
                                            <?php echo $form->labelEx($sub_item_model, 'item_description'); ?>
                                            <?php
                                            echo $form->textArea($sub_item_model, 'item_description[]',  array('class' => 'form-control require text_editor', 'id' => 'ScquotationItems_item_description-' . $key_val));
                                            ?>
                                             <div class="error-discription text-danger" style="display: none;"></div>
                                        </div>
                                       
                                        <div class="col-md-6 calc_values">
                                            <div class="row ">
                                                    <div class="form-group col-md-4" >
                                                        <?php echo $form->labelEx($sub_item_model, 'item_type'); ?>
                                                        <?php
                                                        echo CHtml::activeDropDownList(
                                                            $sub_item_model,
                                                            'item_type[]',
                                                            array('1' => 'Qty*rate', '2' => 'lumpsum'/*, '3' => 'Length*width', '4' => 'Length*width*height'*/),
                                                            array('empty' => 'Select Type', 'class' => 'form-control item_type')
                                                        );
                                                        ?>
                                                        <div class="error-type text-danger" style="display: none;"></div>
                                                    </div>
                                                
                                                <div class="form-group col-md-4" id="quantity_div">
                                                    
                                                        <?php echo $form->labelEx($sub_item_model, 'item_quantity'); ?>
                                                        <?php echo $form->textField($sub_item_model, 'item_quantity[]', array('class' => 'form-control calculate quantity')); ?>
                                                     <div class="error-quantity text-danger" style="display: none;"></div>
                                                </div>
                                                <div class="form-group col-md-4 " id="unit_div">
                                                    
                                                        <?php echo $form->labelEx($sub_item_model, 'item_unit'); ?>
                                                        <?php echo $form->textField($sub_item_model, 'item_unit[]', array('class' => 'form-control unit')); ?>
                                                        <div class="error-unit text-danger" style="display: none;"></div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-4" id="rate_div">
                                                        <?php echo $form->labelEx($sub_item_model, 'item_rate'); ?>
                                                        <?php echo $form->textField($sub_item_model, 'item_rate[]', array('class' => 'form-control calculate rate')); ?>
                                                        <div class="error-rate text-danger" style="display: none;"></div>
                                                </div>
                                                <div class="form-group col-md-4" id="amount_div">
                                                    <label class="text-right">Amount</label>
                                                    <?php echo $form->textField($sub_item_model, 'item_amount[]', array('class' => 'form-control amount')); ?>
                                                    <div class="error-amount text-danger" style="display: none;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12 text-right">
                            
                                <button type="submit" class="btn btn-primary item_save" id="0">Save</button>
                                <button type="reset" class="btn btn-default">Reset</button>
                            </div>
                        </div>
                        
                        <!-- <div class="text-center mt">
                        
                            <button class="btn btn-info item_save mt" id="0">Save</button>
                            <input type="reset" value="Reset" class="btn mt">
                        </div> -->
                        <?php $this->endWidget(); ?>
                        <a href="" class="remove_row" id="row-<?php echo $category_id; ?>">Remove Row</a>
                        </div>
                        
                    </div>
                </div>
                
            </div>
        </div>
        <?php
            $quotation_list = $model->getQuotationItems($model->scquotation_id, $category_id);
            if (!empty($quotation_list)) {
                $this->renderPartial('_quotation_list', array('model' => $model, 'main_item_model'=>$main_item_model,'quotation_list' => $quotation_list));
            } ?>
    </div>
<?php

}
?>