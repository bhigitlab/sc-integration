<?php

class SubcontractorController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        $controller = Yii::app()->controller->id;
        $hidden_actions =  array('saveinvoice', 'addpayments', 'removepaymententry', 'addquotations', 'addcategory', 'addCategoryDesc', 'getItemById', 'addquotationitems', 'deleteCategory','deleteCategoryData','addPaymentStage','getPaymentStage','removePaymentStage','approveScItems','saveterms','addQuotationTax','removeSubcontractor','getitemquantity','cloneQuotation','getDynamicQuotNo','getTemplateDetails','approveScQuotLabourRateChanges');
        $rules = AccessRulesHelper::getRules($controller, $hidden_actions);
        //Additional action rules

        return $rules;
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($_GET['layout']))
            $this->layout = false;
        $model = new Subcontractor;
        $this->performAjaxValidation($model);
        $model->subcontractor_status = 1;

        if (isset($_POST['Subcontractor'])) {
            $model->attributes = $_POST['Subcontractor'];
            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d H:i:s');
            if (!empty($_POST['Subcontractor']['company_id'])) {
                $company = implode(",", $_POST['Subcontractor']['company_id']);
                $model->company_id = $company;
            } else {
                $model->company_id = NULL;
            }
            if ($model->save()) {
                $vendor_id = Yii::app()->db->getLastInsertID();
            }
            if (isset($_POST['type']) && count($_POST['type'])) {
                $type = $_POST['type'];
                foreach ($type as $ty) {
                    $query2[] = '(null, ' . $vendor_id . ', ' . $ty . ')';
                }
                $insertassign2 = 'insert into ' . $tblpx . 'subcontractor_exptype values ' . implode(',', $query2);
                Yii::app()->db->createCommand($insertassign2)->query();
            }
            $this->redirect(array('newlist'));
        }
        $this->render('create', array(
            'model' => $model,
        ));
    }
    public function actionCreate1()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $this->layout = false;
        $model = new Scquotation;
        $this->performAjaxValidation2($model);

        if (isset($_POST['Scquotation'])) {
            $model->attributes       = $_POST['Scquotation'];
            $model->scquotation_date = date("Y-m-d", strtotime($_POST["Scquotation"]["scquotation_date"]));
            $model->created_by       = yii::app()->user->id;
            $model->created_date     = date('Y-m-d H:i:s');
            $model->company_id   = Yii::app()->user->company_id;
            if ($model->save()) {
                $this->redirect(array('quotations'));;
            }
        }
        $this->render('quotations', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */

    public function actionUpdate($id)
    {
        if (isset($_GET['layout']))
            $this->layout = false;
        $model = $this->loadModel($id);
        $this->performAjaxValidation($model);
        if (isset($_POST['Subcontractor'])) {

            $model->attributes = $_POST['Subcontractor'];
            if (!empty($_POST['Subcontractor']['company_id'])) {
                $company = implode(",", $_POST['Subcontractor']['company_id']);
                $model->company_id = $company;
            } else {
                $model->company_id = NULL;
            }
            if ($model->save()) {
                $subcontractor_id = $id;
                $query_part       = array();
                $tbl_px = Yii::app()->db->tablePrefix;

                if (isset($_POST['type']) && count($_POST['type'])) {

                    $type = $_POST['type'];

                    $userArr = array();
                    $res = Yii::app()->db->createCommand("SELECT type_id from " . $tbl_px . "subcontractor_exptype WHERE subcontractor_id = " . $subcontractor_id)->queryAll();
                    foreach ($res as $value) {
                        if (!in_array($value['type_id'], $_POST['type'])) {
                            Yii::app()->db->createCommand('delete from ' . $tbl_px . 'subcontractor_exptype where subcontractor_id = ' . $subcontractor_id . ' AND type_id =' . $value['type_id'])->query();
                        }
                        $userArr[] = $value['type_id'];
                    }
                    foreach ($type as $ty) {
                        if (!in_array($ty, $userArr)) {
                            $query2[] = '(null, ' . $subcontractor_id . ', ' . $ty . ')';
                        }
                    }
                    if (!empty($query2)) {
                        $insertassign2 = 'insert into ' . $tbl_px . 'subcontractor_exptype values ' . implode(',', $query2);
                        Yii::app()->db->createCommand($insertassign2)->query();
                    }
                } else {
                    if (empty($_POST['type'])) {
                        Yii::app()->db->createCommand('delete from ' . $tbl_px . 'subcontractor_exptype where subcontractor_id =' . $id)->query();
                    }
                }

                if (Yii::app()->user->returnUrl != 0) {
                    $this->redirect(array('newlist', 'Subcontractor_page' => Yii::app()->user->returnUrl));
                } else {
                    $this->redirect(array('newlist'));
                }
            }
        }
        $this->render('update', array(
            'model' => $model,
        ));
    }
    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {

        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actiondeleteCategoryData()
    {
        $id = $_POST['id'];
        $model = ScQuotationItemCategory::model()->findBypk($id);
        
        if($model!==null){
            $scquotationItems = ScquotationItems::model()->findAllByAttributes(array('item_category_id' => $model->id));
            foreach ($scquotationItems as $item) {
                $item->delete();
            }
        }
		$transaction = Yii::app()->db->beginTransaction();
		try {
			if (!$model->delete()) {
				$success_status = 0;
				throw new Exception(json_encode($model->getErrors()));
			} else {
				$success_status = 1;
			}
			$transaction->commit();
		} catch (Exception $error) {
			$transaction->rollBack();
			$success_status = 0;
		} finally {
			if ($success_status == 1) {
				echo json_encode(array('response' => 'success', 'msg' => 'Item Deleted successfully'));
			} else {
				if ($error->errorInfo[1] == 1451) {
					echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
				} else {
					echo json_encode(array('response' => 'error', 'msg' => 'Item Deleted error'));
				}
			}
		}
    }

    public function actiondeleteCategory()
    {
        $id = $_POST['id'];
        $model = ScquotationItems::model()->findBypk($id);
		$transaction = Yii::app()->db->beginTransaction();
		try {
			if (!$model->delete()) {
				$success_status = 0;
				throw new Exception(json_encode($model->getErrors()));
			} else {
				$success_status = 1;
			}
			$transaction->commit();
		} catch (Exception $error) {
			$transaction->rollBack();
			$success_status = 0;
		} finally {
			if ($success_status == 1) {
				echo json_encode(array('response' => 'success', 'msg' => 'Item Deleted successfully'));
			} else {
				if ($error->errorInfo[1] == 1451) {
					echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
				} else {
					echo json_encode(array('response' => 'error', 'msg' => 'Item Deleted error'));
				}
			}
		}
    }


    public function actionIndex()
    {
        $dataProvider = Subcontractor('search');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }


    public function actionAdmin()
    {
        $model = new Subcontractor('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Subcontractor']))
            $model->attributes = $_GET['Subcontractor'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Subcontractor the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Subcontractor::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Subcontractor $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'subcontractor-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    protected function performAjaxValidation2($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'scquotation-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    public function actionNewlist()
    {
        $model = new Subcontractor('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Subcontractor']))
            $model->attributes = $_GET['Subcontractor'];
        if (Yii::app()->user->role == 1) {
            $this->render('newlist', array(
                'model' => $model,
                'dataProvider' => $model->search(),
            ));
        } else {
            $this->render('newlist', array(
                'model' => $model,
                'dataProvider' => $model->search(Yii::app()->user->id),
            ));
        }
    }

    public function actionQuotations()
    {
        $model = new Scquotation;
        $model->unsetAttributes();
        $this->performAjaxValidation2($model);
        $writeoff = new Writeoff;
        $total_quot_amountsum=0;
        if (isset($_REQUEST['Scquotation'])) {
            $model->attributes          = $_REQUEST['Scquotation'];
            $model->company_id          = $_REQUEST['Scquotation']['company_id'];
            $model->project_id          = $_REQUEST['Scquotation']['project_id'];
            $model->subcontractor_id    = $_REQUEST['Scquotation']['subcontractor_id'];
            $model->scquotation_no    = $_REQUEST['Scquotation']['scquotation_no'];
            $model->scquotation_amount  = $_REQUEST['Scquotation']['scquotation_amount'];
            $model->approve_status  = isset($_REQUEST['Scquotation']['approve_status'])?$_REQUEST['Scquotation']['approve_status']:'';
        }
        //echo "<pre>";print_r($_REQUEST);exit;
        //$amountsum = $model->getSum($dataProvider);
        if (Yii::app()->user->role == 1) {
            $dataProvider= $model->search();
            $total_dataProvider =$model->total_quotation_search();
        }else{
            $dataProvider= $model->search(Yii::app()->user->id);
            $total_dataProvider =$model->total_quotation_search();
        }
        // Get the quotation IDs from the filtered data
        $quotationIds = [];
        foreach ($dataProvider->getData() as $quotation) {
            $quotationIds[] = $quotation->scquotation_id;
        }
        $totalquotationIds = [];
        foreach ($total_dataProvider->getData() as $quotation) {
            $totalquotationIds[] = $quotation->scquotation_id;
        }

        // Calculate item_amount sum for ScquotationItems
        $scquotationItemsModel = new ScquotationItems;
        $amountsum = $scquotationItemsModel->getSumByQuotationId($quotationIds);
        $total_quot_amountsum=    $scquotationItemsModel->getSumByQuotationId($totalquotationIds);
        if (Yii::app()->user->role == 1) {
                // $amountsum = $model->getSum($model->search());
                $this->render('quotations', array(
                    'model' => $model,
                    'dataProvider' => $model->search(),
                    'amountsum' => $amountsum,
                    'writeoff' => $writeoff,
                    'total_quot_amountsum'=>$total_quot_amountsum,
                ));
            } else {
                //  $amountsum = $model->getSum($model->search(Yii::app()->user->id));
                $this->render('quotations', array(
                    'model' => $model,
                    'dataProvider' => $model->search(Yii::app()->user->id),
                    'amountsum' => $amountsum,
                    'writeoff' => $writeoff,
                    'total_quot_amountsum'=>$total_quot_amountsum,
                ));
            }
    }
    public function actionUpdate1()
    {
        $id             = $_REQUEST["id"];
        $tblpx          = Yii::app()->db->tablePrefix;
        $quotModel      = Scquotation::model()->find('scquotation_id = ' . $id . '');

        $newmodel       = new JpLog;

        $newmodel->log_data         = json_encode($quotModel->attributes);
        $newmodel->log_table        = $tblpx . "scquotation";
        $newmodel->log_primary_key  = $id;
        $newmodel->log_datetime     = date('Y-m-d H:i:s');
        $newmodel->log_action       = 1;
        $newmodel->log_action_by    = yii::app()->user->id;
        $newmodel->company_id       = yii::app()->user->company_id;
        $created_by     = $quotModel["created_by"];
        $created_date   = $quotModel["created_date"];

        $model = new Scquotation;
        if (isset($_POST['Scquotation'])) {
            $model->attributes          = $_POST['Scquotation'];
            $model->scquotation_date    = date("Y-m-d", strtotime($_POST["Scquotation"]["scquotation_date"]));

            $model->created_by          = $created_by;
            $model->created_date        = $created_date;

            $model->updated_by          = yii::app()->user->id;
            $model->updated_date        = date('Y-m-d H:i:s');
            $model->company_id          = Yii::app()->user->company_id;

            if ($model->save()) {
                if ($newmodel->save()) {
                    $deleteQuot = Yii::app()->db->createCommand("DELETE FROM {$tblpx}scquotation WHERE scquotation_id = '" . $id . "'");
                    $deleteQuot->execute();

                    $model1  = new Scquotation('search');
                    $model1->unsetAttributes();
                    $client = $this->widget('zii.widgets.CListView', array('dataProvider' => $model1->search(), 'itemView' => '_newview', 'template' => '<div>{summary}{sorter}</div><div class="tblwrap scrollTable"><div class="container1"><table cellpadding="10" class="table  list-view sorter">{items}</table></div></div>{pager}',));
                    return $client;
                }
            }
        }
    }

    public function actionGetQuotations()
    {
        $id                     = $_REQUEST["id"];
        $quotation              = Scquotation::model()->find('scquotation_id = ' . $id . '');
        $quot["id"]             = $quotation["scquotation_id"];
        $quot["date"]           = date("d-m-Y", strtotime($quotation["scquotation_date"]));
        $quot["project"]        = $quotation["project_id"];
        $quot["subcontractor"]  = $quotation["subcontractor_id"];
        $quot["scamount"]       = $quotation["scquotation_amount"];
        $quot["decription"]     = $quotation["scquotation_decription"];

        echo json_encode($quot);
    }
    public function actionDeleteQuotations()
    {
        $id                     = $_REQUEST["id"];
        $tblpx                  = Yii::app()->db->tablePrefix;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $payment_model = SubcontractorPayment::model()->find(array(
                'select' => '*',
                'condition' => 'quotation_number = ' . $id
            ));
            $payment_exist = 0;
            if($payment_model['quotation_number'] !=""){
                $success_status = 0;
                $payment_exist = 1;
                throw new Exception('Cannot Delete !  Already used in payments');
            }
            
            $quotModel              = Scquotation::model()->find('scquotation_id = ' . $id . '');

            $itemmodel = ScquotationItems::model()->find('scquotation_id = ' . $id . '');
            if(!empty($itemmodel)){  
                if ($itemmodel->delete()) {                
                    $success_status = 1;
                }else{
                    $success_status = 0;
                    throw new Exception(json_encode($itemmodel->getErrors()));    
                }
            }
            

            $categorymodel = ScQuotationItemCategory::model()->find('sc_quotaion_id = ' . $id . '');
            if(!empty($categorymodel)){
                if ($categorymodel->delete()) {                
                    $success_status = 1;
                }else{
                    $success_status = 0;
                    throw new Exception(json_encode($categorymodel->getErrors()));    
                }
            }

            $newmodel               = new JpLog;
            $newmodel->log_data         = json_encode($quotModel->attributes);
            $newmodel->log_table        = $tblpx . "scquotation";
            $newmodel->log_primary_key  = $id;
            $newmodel->log_datetime     = date('Y-m-d H:i:s');
            $newmodel->log_action       = 2;
            $newmodel->log_action_by    = yii::app()->user->id;
            
            if ($quotModel->delete()) {
                $newmodel->save();
                $success_status = 1;
            }else{
                $success_status = 0;
                throw new Exception(json_encode($quotModel->getErrors()));    
            }
            $transaction->commit();
        }catch(Exception $error){
            $transaction->rollBack();
            $success_status = 0;
        }finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Data Deleted Successfully'));
            } else {
                if (isset($error->errorInfo[1]) && $error->errorInfo[1] == 1451) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    if($payment_exist ==1){
                        echo json_encode(array('response' => 'warning', 'msg' =>$error->getMessage()));
                    }else{
                        echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                    }
                    
                }
            }
        }
    }

    public function actionaddquotationquantity()
    {
        $model = new Scquotation;
        $tblpx      = Yii::app()->db->tablePrefix;
        $project    = Yii::app()->db->createCommand("SELECT DISTINCT pid, name  FROM {$tblpx}projects WHERE company_id=" . Yii::app()->user->company_id . " ORDER BY pid DESC")->queryAll();
        $subcontractor     = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}subcontractor WHERE company_id=" . Yii::app()->user->company_id . " ORDER BY subcontractor_id DESC")->queryAll();
        $expensehead = ExpenseType::model()->findAll(array('condition' => 'expense_type IN (97, 98)'));
        $payment_model = new ScquotationPaymentEntries();
        $this->render('addquotationquantity', array(
            'model' => $model,
            'project' => $project,
            'subcontractor' => $subcontractor,
            'expensehead' => $expensehead,
            'payment_model' => $payment_model
        ));
    }

    public function actionAjax()
    {
        echo json_encode('success');
    }

    public function actioncreatenewquotation()
    {
        if (isset($_GET['subcontractor']) && isset($_GET['default_date']) && isset($_GET['description']) && isset($_GET['project'])) {
            $quotation_id = $_GET['quotation_id'];
            if ($quotation_id == 0) {
                $tblpx = Yii::app()->db->tablePrefix;
                $model = new Scquotation;
                $model->subcontractor_id  = $_GET['subcontractor'];
                $model->project_id  = $_GET['project'];
                $model->scquotation_decription  = $_GET['description'];
                $model->scquotation_date  = date('Y-m-d', strtotime($_REQUEST['default_date']));
                $model->scquotation_no = $_GET['quotation_no'];
                $model->created_date = date('Y-m-d');
                $model->created_by   = Yii::app()->user->id;
                $model->company_id   = $_GET['company'];
                $model->type = $_GET['type'];
                if (Yii::app()->user->role == 7)
                    $model->scquotation_status = 0;
                else
                    $model->scquotation_status = 1;
                if ($model->save()) {
                    $last_id = $model->scquotation_id;
                    echo json_encode(array('response' => 'success', 'msg' => 'Quotation added successfully', 'quotation_id' => $last_id));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
                }
            } else {

                $model = Scquotation::model()->findByPk($quotation_id);
                $model->subcontractor_id  = $_GET['subcontractor'];
                $model->project_id  = $_GET['project'];
                $model->scquotation_decription  = $_GET['description'];
                $model->scquotation_no = $_GET['quotation_no'];
                $model->scquotation_date  = date('Y-m-d', strtotime($_REQUEST['default_date']));
                $model->updated_date = date('Y-m-d');
                $model->updated_by   = Yii::app()->user->id;
                $model->company_id   = $_GET['company'];
                if ($model->save(false)) {
                    echo json_encode(array('response' => 'success', 'msg' => 'Quotation updated successfully', 'quotation_id' => $quotation_id));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Problem Occured'));
                }
            }
        }
    }

    public function actionquotationitem()
    {
        $data           = $_REQUEST['data'];
        $result         = '';
        if ($data['quotation_id'] == 0) {
            echo json_encode(array('response' => 'error', 'msg' => 'Please enter quotation details'));
        } else {
            if (isset($data['item_description'])) {
                $item_description   = $data['item_description'];
            } else {
                $item_description   = '';
            }
            if (isset($data['expense_head'])) {
                $expense_head = $data['expense_head'];
            } else {
                $expense_head = '';
            }
            if (isset($data['item_amount'])) {
                $item_amount        = $data['item_amount'];
            } else {
                $item_amount        = '';
            }
            if (isset($data['item_date'])) {
                $item_date          = $data['item_date'];
            } else {
                $item_date          = '';
            }

            $item_model                 = new ScquotationItems();
            $item_model->item_amount    = $item_amount;
            if (Yii::app()->user->role == 7) {
                $item_model->expensehead_id = $expense_head;
            } else {
                $item_model->expensehead_id = $expense_head;
                $item_model->item_description = $item_description;
            }
            $item_model->item_date      = date('Y-m-d', strtotime($item_date));
            $item_model->scquotation_id = $data['quotation_id'];
            $item_model->created_date   = date('Y-m-d');
            $item_model->created_by     = Yii::app()->user->id;
            if ($item_model->save(false)) {
                $last_id    = $item_model->item_id;
                $tblpx      = Yii::app()->db->tablePrefix;
                $data1      = Yii::app()->db->createCommand("SELECT SUM(item_amount) as qt_amount FROM {$tblpx}scquotation_items WHERE scquotation_id=" . $data['quotation_id'] . "")->queryRow();
                $model      = Scquotation::model()->findByPk($data['quotation_id']);
                if ($item_model->approve_status == "No") {
                    $model->approve_status = "No";
                }
                $model->scquotation_amount = $data1['qt_amount'];
                $grand_total    = $data1['qt_amount'];
                if ($model->save(false)) {
                    $result         = '';
                    $bill_details   = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}scquotation_items  WHERE scquotation_id=" . $data['quotation_id'] . " ORDER BY item_id desc")->queryAll();
                    foreach ($bill_details as $key => $values) {
                        if ($values['approve_status'] == 'No') {
                            $class = "permission_style";
                        } else {
                            $class = "";
                        }

                        if ($values["expensehead_id"] === NULL) {
                            $expensehead = '';
                        } else {
                            $expenseheadmodel   = ExpenseType::model()->findByPk($values["expensehead_id"]);
                            $expensehead        = $expenseheadmodel["type_name"];
                        }
                        $result            .= '<input type="hidden" name="expensehead' . $values['item_id'] . '" id="expensehead' . $values['item_id'] . '" value="' . $values["expensehead_id"] . '"/>';
                        $result .= '<tr class=' . $class . '>';
                        $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                        $result .= '<td><div class="item_description">' . $expensehead . '</div> </td>';
                        $result .= '<td><div class="item_description">' . $values['item_description'] . '</div> </td>';
                        $result .= '<td> <div class="" id="item_date"> ' . date('d-m-Y', strtotime($values['item_date'])) . '</div> </td>';
                        $result .= '<td class="text-right"><div class="" id="item_amount">' . number_format($values['item_amount'], 2, '.', '') . '</div></td>';
                        $result .= '<td><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                                        <div class="popover-content hide">
                                            <ul class="tooltip-hiden">
                                                <li><a href="#" id=' . $values['item_id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li>
                                                <li><a href="" id=' . $values['item_id'] . ' class="btn btn-xs btn-default edit_item">Edit</a></li>&nbsp;';
                        if ((isset(Yii::app()->user->role) && (in_array('/subcontractor/permissionapprove', Yii::app()->user->menuauthlist)))) {
                            if ($values['approve_status'] == 'No') {
                                $result .= '<li><a href="#" id=' . $values['item_id'] . ' class="btn btn-xs btn-default permission_item approveoption_' . $values['item_id'] . '">Approve</a></li>';
                            }
                        }
                        $result .= '</ul></div></td>';
                        $result .= '</tr>';
                    }
                    echo json_encode(array('response' => 'success', 'msg' => 'Quotation item update successfully', 'html' => $result, 'grand_total' => number_format($grand_total, 2, '.', '')));
                }
            } else {
                echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
            }
        }
    }

    public function actionquotationitembyquantity()
    {
        $data           = $_REQUEST['data'];
        $result         = '';
        if ($data['quotation_id'] == 0) {
            echo json_encode(array('response' => 'error', 'msg' => 'Please enter quotation details'));
        } else {
            if (isset($data['item_description'])) {
                $item_description   = $data['item_description'];
            } else {
                $item_description   = '';
            }
            if (isset($data['expense_head'])) {
                $expense_head = $data['expense_head'];
            } else {
                $expense_head = '';
            }
            if (isset($data['item_amount'])) {
                $item_amount        = $data['item_amount'];
            } else {
                $item_amount        = '';
            }
            if (isset($data['item_date'])) {
                $item_date          = $data['item_date'];
            } else {
                $item_date          = '';
            }

            if (isset($data['item_quantity'])) {
                $item_quantity        = $data['item_quantity'];
            } else {
                $item_quantity        = '';
            }

            if (isset($data['item_unit'])) {
                $item_unit        = $data['item_unit'];
            } else {
                $item_unit        = '';
            }

            if (isset($data['item_rate'])) {
                $item_rate        = $data['item_rate'];
            } else {
                $item_rate        = '';
            }

            $item_model                 = new ScquotationItems();
            $item_model->item_amount    = $item_amount;
            $item_model->item_quantity  = $item_quantity;
            $item_model->item_unit      = $item_unit;
            $item_model->item_rate      = $item_rate;
            if (Yii::app()->user->role == 7) {
                $item_model->expensehead_id = $expense_head;
            } else {
                $item_model->expensehead_id = $expense_head;
                $item_model->item_description = $item_description;
            }
            $item_model->item_date      = date('Y-m-d', strtotime($item_date));
            $item_model->scquotation_id = $data['quotation_id'];
            $item_model->created_date   = date('Y-m-d');
            $item_model->created_by     = Yii::app()->user->id;
            if ($item_model->save(false)) {
                $last_id    = $item_model->item_id;
                $tblpx      = Yii::app()->db->tablePrefix;
                $data1      = Yii::app()->db->createCommand("SELECT SUM(item_amount) as qt_amount FROM {$tblpx}scquotation_items WHERE scquotation_id=" . $data['quotation_id'] . "")->queryRow();
                $model      = Scquotation::model()->findByPk($data['quotation_id']);
                if ($item_model->approve_status == "No") {
                    $model->approve_status = "No";
                }
                $model->scquotation_amount = $data1['qt_amount'];
                $model->type = 2;
                $grand_total    = $data1['qt_amount'];
                if ($model->save(false)) {
                    $result         = '';
                    $bill_details   = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}scquotation_items  WHERE scquotation_id=" . $data['quotation_id'] . " ORDER BY item_id desc")->queryAll();
                    foreach ($bill_details as $key => $values) {
                        if ($values['approve_status'] == 'No') {
                            $class = "permission_style";
                        } else {
                            $class = "";
                        }

                        if ($values["expensehead_id"] === NULL) {
                            $expensehead = '';
                        } else {
                            $expenseheadmodel   = ExpenseType::model()->findByPk($values["expensehead_id"]);
                            $expensehead        = $expenseheadmodel["type_name"];
                        }
                        $result            .= '<input type="hidden" name="expensehead' . $values['item_id'] . '" id="expensehead' . $values['item_id'] . '" value="' . $values["expensehead_id"] . '"/>';
                        $result .= '<tr class=' . $class . '>';
                        $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                        $result .= '<td><div class="item_description">' . $expensehead . '</div> </td>';
                        $result .= '<td><div class="item_description">' . $values['item_description'] . '</div> </td>';
                        $result .= '<td> <div class="" id="item_date"> ' . date('d-m-Y', strtotime($values['item_date'])) . '</div> </td>';
                        $result .= '<td class="text-right"><div class="" id="item_quantity">' . number_format($values['item_quantity'], 2, '.', '') . '</div></td>';
                        $result .= '<td class="text-right"><div class="" id="item_unit">' . $values['item_unit'] . '</div></td>';
                        $result .= '<td class="text-right"><div class="" id="item_rate">' . number_format($values['item_rate'], 2, '.', '') . '</div></td>';
                        $result .= '<td class="text-right"><div class="" id="item_amount">' . number_format($values['item_amount'], 2, '.', '') . '</div></td>';
                        $result .= '<td><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                                        <div class="popover-content hide">
                                            <ul class="tooltip-hiden">
                                                <li><a href="#" id=' . $values['item_id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li>
                                                <li><a href="" id=' . $values['item_id'] . ' class="btn btn-xs btn-default edit_item">Edit</a></li>&nbsp;';
                        if ((isset(Yii::app()->user->role) && (in_array('/subcontractor/permissionapprove', Yii::app()->user->menuauthlist)))) {
                            if ($values['approve_status'] == 'No') {
                                $result .= '<li><a href="#" id=' . $values['item_id'] . ' class="btn btn-xs btn-default permission_item approveoption_' . $values['item_id'] . '">Approve</a></li>';
                            }
                        }
                        $result .= '</ul></div></td>';
                        $result .= '</tr>';
                    }
                    echo json_encode(array('response' => 'success', 'msg' => 'Quotation item update successfully', 'html' => $result, 'grand_total' => number_format($grand_total, 2, '.', '')));
                }
            } else {
                echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
            }
        }
    }

    public function actionupdatesquotationitem()
    {
        $result = array('html' => '');
        $html = '';
        $data = $_REQUEST['data'];
        if (isset($data['item_description'])) {
            $item_description = $data['item_description'];
        } else {
            $item_description = '';
        }

        if (isset($data['expense_head'])) {
            $expense_head = $data['expense_head'];
        } else {
            $expense_head = '';
        }

        if (isset($data['item_amount'])) {
            $item_amount = $data['item_amount'];
        } else {
            $item_amount  = '';
        }
        if (isset($data['item_date'])) {
            $item_date = $data['item_date'];
        } else {
            $item_date  = '';
        }


        $item_model = ScquotationItems::model()->findByPk($data['item_id']);
        $item_model->item_amount = $item_amount;
        if (Yii::app()->user->role == 7) {
            $item_model->expensehead_id = $expense_head;
        } else {
            $item_model->expensehead_id = $expense_head;
            $item_model->item_description = $item_description;
        }
        $item_model->approve_status = 'No';
        $item_model->item_date = date('Y-m-d', strtotime($item_date));
        if ($item_model->save(false)) {
            $tblpx = Yii::app()->db->tablePrefix;
            $data1 = Yii::app()->db->createCommand("SELECT SUM(item_amount) as qt_amount FROM {$tblpx}scquotation_items WHERE scquotation_id=" . $data['quotation_id'] . "")->queryRow();
            $model = Scquotation::model()->findByPk($data['quotation_id']);
            $model->scquotation_amount = $data1['qt_amount'];
            $grand_total = $data1['qt_amount'];
            if ($item_model->approve_status == "No") {
                $model->approve_status = "No";
            }
            if ($model->save(false)) {
                $result = '';
                $bill_details  = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}scquotation_items  WHERE scquotation_id=" . $data['quotation_id'] . " ORDER BY item_id desc")->queryAll();
                foreach ($bill_details as $key => $values) {
                    if ($values['approve_status'] == 'No') {
                        $class = "permission_style";
                    } else {
                        $class = "";
                    }

                    if ($values["expensehead_id"] === NULL) {
                        $expensehead = '';
                    } else {
                        $expenseheadmodel   = ExpenseType::model()->findByPk($values["expensehead_id"]);
                        $expensehead        = $expenseheadmodel["type_name"];
                    }
                    $result             .= '<input type="hidden" name="expensehead' . $values['item_id'] . '" id="expensehead' . $values['item_id'] . '" value="' . $values["expensehead_id"] . '"/>';
                    $result .= '<tr class=' . $class . '>';
                    $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                    $result .= '<td><div class="item_description">' . $expensehead . '</div> </td>';
                    $result .= '<td><div class="item_description">' . $values['item_description'] . '</div> </td>';
                    $result .= '<td> <div class="" id="item_date"> ' . date('d-m-Y', strtotime($values['item_date'])) . '</div> </td>';
                    $result .= '<td class="text-right"><div class="" id="item_amount">' . number_format($values['item_amount'], 2, '.', '') . '</div></td>';
                    $result .= '<td><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                                                            <div class="popover-content hide">
                                                                <ul class="tooltip-hiden">
                                                                        <li><a href="#" id=' . $values['item_id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li>
                                                                        <li><a href="" id=' . $values['item_id'] . ' class="btn btn-xs btn-default edit_item">Edit</a></li>&nbsp;';
                    if ((isset(Yii::app()->user->role) && (in_array('/subcontractor/permissionapprove', Yii::app()->user->menuauthlist)))) {
                        if ($values['approve_status'] == 'No') {
                            $result .= '<li><a href="#" id=' . $values['item_id'] . ' class="btn btn-xs btn-default permission_item approveoption_' . $values['item_id'] . '">Approve</a></li>';
                        }
                    }
                    $result .= '</ul></div></td>';
                    $result .= '</tr>';
                }
                echo json_encode(array('response' => 'success', 'msg' => 'Quotation item update successfully', 'html' => $result, 'grand_total' => number_format($grand_total, 2, '.', '')));
            }
        } else {
            echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
        }
    }


    public function actionupdatesquotationitemquantity()
    {
        $result = '';
        $html = '';
        $data = $_REQUEST['data'];
        if (isset($data['item_description'])) {
            $item_description = $data['item_description'];
        } else {
            $item_description = '';
        }

        if (isset($data['expense_head'])) {
            $expense_head = $data['expense_head'];
        } else {
            $expense_head = '';
        }

        if (isset($data['item_amount'])) {
            $item_amount = $data['item_amount'];
        } else {
            $item_amount  = '';
        }
        if (isset($data['item_date'])) {
            $item_date = $data['item_date'];
        } else {
            $item_date  = '';
        }

        if (isset($data['item_quantity'])) {
            $item_quantity = $data['item_quantity'];
        } else {
            $item_quantity  = '';
        }

        if (isset($data['item_unit'])) {
            $item_unit = $data['item_unit'];
        } else {
            $item_unit  = '';
        }

        if (isset($data['item_rate'])) {
            $item_rate = $data['item_rate'];
        } else {
            $item_rate  = '';
        }


        $item_model = ScquotationItems::model()->findByPk($data['item_id']);
        $item_model->item_amount = $item_amount;
        $item_model->item_quantity = $item_quantity;
        $item_model->item_unit = $item_unit;
        $item_model->item_rate = $item_rate;
        if (Yii::app()->user->role == 7) {
            $item_model->expensehead_id = $expense_head;
        } else {
            $item_model->expensehead_id = $expense_head;
            $item_model->item_description = $item_description;
        }
        $item_model->approve_status = 'No';
        $item_model->item_date = date('Y-m-d', strtotime($item_date));
        if ($item_model->save(false)) {
            $tblpx = Yii::app()->db->tablePrefix;
            $data1 = Yii::app()->db->createCommand("SELECT SUM(item_amount) as qt_amount FROM {$tblpx}scquotation_items WHERE scquotation_id=" . $data['quotation_id'] . "")->queryRow();
            $model = Scquotation::model()->findByPk($data['quotation_id']);
            $model->scquotation_amount = $data1['qt_amount'];
            $model->type = 2;
            $grand_total = $data1['qt_amount'];
            if ($item_model->approve_status == "No") {
                $model->approve_status = "No";
            }
            if ($model->save(false)) {
                $result = '';
                $bill_details  = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}scquotation_items  WHERE scquotation_id=" . $data['quotation_id'] . " ORDER BY item_id desc")->queryAll();
                foreach ($bill_details as $key => $values) {
                    if ($values['approve_status'] == 'No') {
                        $class = "permission_style";
                    } else {
                        $class = "";
                    }

                    if ($values["expensehead_id"] === NULL) {
                        $expensehead = '';
                    } else {
                        $expenseheadmodel   = ExpenseType::model()->findByPk($values["expensehead_id"]);
                        $expensehead        = $expenseheadmodel["type_name"];
                    }
                    $result             .= '<input type="hidden" name="expensehead' . $values['item_id'] . '" id="expensehead' . $values['item_id'] . '" value="' . $values["expensehead_id"] . '"/>';
                    $result .= '<tr class=' . $class . '>';
                    $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                    $result .= '<td><div class="item_description">' . $expensehead . '</div> </td>';
                    $result .= '<td><div class="item_description">' . $values['item_description'] . '</div> </td>';
                    $result .= '<td> <div class="" id="item_date"> ' . date('d-m-Y', strtotime($values['item_date'])) . '</div> </td>';
                    $result .= '<td class="text-right"><div class="" id="item_quantity">' . number_format($values['item_quantity'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="item_unit">' . $values['item_unit'] . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="item_rate">' . number_format($values['item_rate'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="item_amount">' . number_format($values['item_amount'], 2, '.', '') . '</div></td>';
                    $result .= '<td><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                                                            <div class="popover-content hide">
                                                                <ul class="tooltip-hiden">
                                                                        <li><a href="#" id=' . $values['item_id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li>
                                                                        <li><a href="" id=' . $values['item_id'] . ' class="btn btn-xs btn-default edit_item">Edit</a></li>&nbsp;';
                    if ((isset(Yii::app()->user->role) && (in_array('/subcontractor/permissionapprove', Yii::app()->user->menuauthlist)))) {
                        if ($values['approve_status'] == 'No') {
                            $result .= '<li><a href="#" id=' . $values['item_id'] . ' class="btn btn-xs btn-default permission_item approveoption_' . $values['item_id'] . '">Approve</a></li>';
                        }
                    }
                    $result .= '</ul></div></td>';
                    $result .= '</tr>';
                }
                echo json_encode(array('response' => 'success', 'msg' => 'Quotation item update successfully', 'html' => $result, 'grand_total' => number_format($grand_total, 2, '.', '')));
            }
        } else {
            echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
        }
    }

    public function actionremovequotationitem()
    {
        $data   = $_REQUEST['data'];
        $tblpx = Yii::app()->db->tablePrefix;
        $del = Yii::app()->db->createCommand()->delete($tblpx . 'scquotation_items', 'item_id=:item_id', array(':item_id' => $data['item_id']));
        if ($del) {
            $data1 = Yii::app()->db->createCommand("SELECT SUM(item_amount) as qt_amount FROM {$tblpx}scquotation_items WHERE scquotation_id=" . $data['quotation_id'] . "")->queryRow();
            $model = Scquotation::model()->findByPk($data['quotation_id']);
            $model->scquotation_amount = $data1['qt_amount'];
            $model->company_id = Yii::app()->user->company_id;
            $grand_total = $data1['qt_amount'];
            $model->save();
            $result = '';
            $bill_details  = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}scquotation_items  WHERE scquotation_id=" . $data['quotation_id'] . " ORDER BY item_id desc")->queryAll();
            foreach ($bill_details as $key => $values) {
                if ($values['approve_status'] == 'No') {
                    $class = "permission_style";
                } else {
                    $class = "";
                }
                if ($values["expensehead_id"] === NULL) {
                    $expensehead = '';
                } else {
                    $expenseheadmodel   = ExpenseType::model()->findByPk($values["expensehead_id"]);
                    $expensehead        = $expenseheadmodel["type_name"];
                }
                $result .= '<tr class=' . $class . '>';
                $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                $result .= '<td><div class="item_description">' . $expensehead . '</div> </td>';
                $result .= '<td><div class="item_description">' . $values['item_description'] . '</div> </td>';
                $result .= '<td> <div class="" id="item_date"> ' . date('d-m-Y', strtotime($values['item_date'])) . '</div> </td>';
                $result .= '<td class="text-right"><div class="" id="item_amount">' . number_format($values['item_amount'], 2, '.', '') . '</div></td>';

                $result .= '<td><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                                                            <div class="popover-content hide">
                                                                <ul class="tooltip-hiden">
                                                                        <li><a href="#" id=' . $values['item_id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li>
                                                                        <li><a href="" id=' . $values['item_id'] . ' class="btn btn-xs btn-default edit_item">Edit</a></li>&nbsp;';
                if ((isset(Yii::app()->user->role) && (in_array('/subcontractor/permissionapprove', Yii::app()->user->menuauthlist)))) {
                    if ($values['approve_status'] == 'No') {
                        $result .= '<li><a href="#" id=' . $values['item_id'] . ' class="btn btn-xs btn-default permission_item approveoption_' . $values['item_id'] . '">Approve</a></li>';
                    }
                }
                $result .= '</ul></div></td>';
                $result .= '</tr>';
            }
            echo json_encode(array('response' => 'success', 'msg' => 'Quotation item deleted successfully', 'html' => $result, 'grand_total' => number_format($grand_total, 2, '.', '')));
        } else {
            echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
        }
    }

    public function actionremovequotationitemquantity()
    {
        $data   = $_REQUEST['data'];
        $tblpx = Yii::app()->db->tablePrefix;
        $del = Yii::app()->db->createCommand()->delete($tblpx . 'scquotation_items', 'item_id=:item_id', array(':item_id' => $data['item_id']));
        if ($del) {
            $data1 = Yii::app()->db->createCommand("SELECT SUM(item_amount) as qt_amount FROM {$tblpx}scquotation_items WHERE scquotation_id=" . $data['quotation_id'] . "")->queryRow();
            $model = Scquotation::model()->findByPk($data['quotation_id']);
            $model->scquotation_amount = $data1['qt_amount'];
            $model->company_id = Yii::app()->user->company_id;
            $grand_total = $data1['qt_amount'];
            $model->save();
            $result = '';
            $bill_details  = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}scquotation_items  WHERE scquotation_id=" . $data['quotation_id'] . " ORDER BY item_id desc")->queryAll();
            foreach ($bill_details as $key => $values) {
                if ($values['approve_status'] == 'No') {
                    $class = "permission_style";
                } else {
                    $class = "";
                }
                if ($values["expensehead_id"] === NULL) {
                    $expensehead = '';
                } else {
                    $expenseheadmodel   = ExpenseType::model()->findByPk($values["expensehead_id"]);
                    $expensehead        = $expenseheadmodel["type_name"];
                }
                $result .= '<tr class=' . $class . '>';
                $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                $result .= '<td><div class="item_description">' . $expensehead . '</div> </td>';
                $result .= '<td><div class="item_description">' . $values['item_description'] . '</div> </td>';
                $result .= '<td> <div class="" id="item_date"> ' . date('d-m-Y', strtotime($values['item_date'])) . '</div> </td>';
                $result .= '<td class="text-right"><div class="" id="item_quantity">' . number_format($values['item_quantity'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="item_unit">' . $values['item_unit'] . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="item_rate">' . number_format($values['item_rate'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="item_amount">' . number_format($values['item_amount'], 2, '.', '') . '</div></td>';
                $result .= '<td><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                                    <div class="popover-content hide">
                                        <ul class="tooltip-hiden">
                                                <li><a href="#" id=' . $values['item_id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li>
                                                <li><a href="" id=' . $values['item_id'] . ' class="btn btn-xs btn-default edit_item">Edit</a></li>&nbsp;';
                if ((isset(Yii::app()->user->role) && (in_array('/subcontractor/permissionapprove', Yii::app()->user->menuauthlist)))) {
                    if ($values['approve_status'] == 'No') {
                        $result .= '<li><a href="#" id=' . $values['item_id'] . ' class="btn btn-xs btn-default permission_item approveoption_' . $values['item_id'] . '">Approve</a></li>';
                    }
                }
                $result .= '</ul></div></td>';
                $result .= '</tr>';
            }
            echo json_encode(array('response' => 'success', 'msg' => 'Quotation item deleted successfully', 'html' => $result, 'grand_total' => number_format($grand_total, 2, '.', '')));
        } else {
            echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
        }
    }

    public function actioneditquotation($scquotation_id)
    {
        $model     = Scquotation::model()->find(array("condition" => "scquotation_id = '$scquotation_id'"));
        if (empty($model)) {
            $this->redirect(array('subcontractor/quotations'));
        }
        $payment_model = new ScquotationPaymentEntries();
        $payment_entries = ScquotationPaymentEntries::model()->findAllByAttributes(array('scquotation_id' => $scquotation_id));
        $tblpx = Yii::app()->db->tablePrefix;
        $item_model = ScquotationItems::model()->findAll('scquotation_id=' . $scquotation_id, array('order' => 'item_id DESC'));
        $project    = Yii::app()->db->createCommand("SELECT DISTINCT pid, name  FROM {$tblpx}projects WHERE FIND_IN_SET(" . $model->company_id . ",company_id) ORDER BY pid DESC")->queryAll();
        $subcontractor     = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}subcontractor WHERE FIND_IN_SET(" . $model->company_id . ",company_id) ORDER BY subcontractor_id DESC")->queryAll();
        $expensehead = ExpenseType::model()->findAll(array('condition' => 'expense_type IN (97, 98)'));
        $this->render('quotation_update', array(
            'model' => $model,
            'item_model' => $item_model,
            'quotation_id' => $scquotation_id,
            'project' => $project,
            'subcontractor' => $subcontractor,
            'expensehead' => $expensehead,
            'payment_model' => $payment_model,
            'payment_entries' => $payment_entries
        ));
    }

    public function actioneditquotationquantity($scquotation_id)
    {
        $model     = Scquotation::model()->find(array("condition" => "scquotation_id = '$scquotation_id'"));
        if (empty($model)) {
            $this->redirect(array('subcontractor/quotations'));
        }
        $tblpx = Yii::app()->db->tablePrefix;
        $payment_model = new ScquotationPaymentEntries();
        $payment_entries = ScquotationPaymentEntries::model()->findAllByAttributes(array('scquotation_id' => $scquotation_id));
        $item_model = ScquotationItems::model()->findAll('scquotation_id=' . $scquotation_id, array('order' => 'item_id DESC'));
        $project    = Yii::app()->db->createCommand("SELECT DISTINCT pid, name  FROM {$tblpx}projects WHERE FIND_IN_SET(" . $model->company_id . ",company_id) ORDER BY pid DESC")->queryAll();
        $subcontractor     = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}subcontractor WHERE FIND_IN_SET(" . $model->company_id . ",company_id) ORDER BY subcontractor_id DESC")->queryAll();
        $expensehead = ExpenseType::model()->findAll(array('condition' => 'expense_type IN (97, 98)'));
        $this->render('quotation_updatequantity', array(
            'model' => $model,
            'item_model' => $item_model,
            'quotation_id' => $scquotation_id,
            'project' => $project,
            'subcontractor' => $subcontractor,
            'expensehead' => $expensehead,
            'payment_model' => $payment_model,
            'payment_entries' => $payment_entries
        ));
    }


    public function actionpermissionapprove()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $data  = Yii::app()->db->createCommand("SELECT approve_status, scquotation_id FROM {$tblpx}scquotation_items WHERE item_id='" . $_REQUEST['item_id'] . "' ")->queryRow();
        if ($data['approve_status'] == 'No') {
            $update = Yii::app()->db->createCommand("UPDATE {$tblpx}scquotation_items SET approve_status = 'Yes' WHERE item_id = '" . $_REQUEST['item_id'] . "'")->execute();
            if ($update) {
                $permission = ScquotationItems::model()->findAll(array("condition" => "scquotation_id=" . $data['scquotation_id'] . " AND approve_status='No'"));
                if ($permission) {
                    Yii::app()->db->createCommand("UPDATE {$tblpx}scquotation SET approve_status = 'No' WHERE scquotation_id = '" . $data['scquotation_id'] . "'")->execute();
                } else {
                    Yii::app()->db->createCommand("UPDATE {$tblpx}scquotation SET approve_status = 'Yes' WHERE scquotation_id = '" . $data['scquotation_id'] . "'")->execute();
                    $notification = new Notifications;
                    $notification->action = "Quotaion Approved";

                    $quotationitem  = ScquotationItems::model()->findByPk($_REQUEST['item_id']);
                    $quotation      = Scquotation::model()->findByPk($quotationitem->scquotation_id);
                    $notification->message = "Subcontractor quotation for " . $quotation->subcontractor->subcontractor_name . " in project " . $quotation->projects->name . " on " . date("d-m-Y", strtotime($quotation->scquotation_date)) . " is approved";
                    $notification->parent_id = $quotation->scquotation_id;
                    $notification->date = date("Y-m-d");
                    $notification->requested_by = $quotationitem->created_by;
                    $notification->approved_by = Yii::app()->user->id;
                    $notification->save();
                }
                echo json_encode(array('response' => 'success', 'msg' => 'Permission approved Successfully '));
            } else {
                echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
            }
        } else {
            echo json_encode(array('response' => 'warning', 'msg' => 'Permission already approved'));
        }
    }

    public function actiongetproject($id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $model = new SubcontractorPermission();
        $sql  = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "scquotation LEFT JOIN " . $tblpx . "projects ON " . $tblpx . "scquotation.project_id = " . $tblpx . "projects.pid WHERE " . $tblpx . "scquotation.subcontractor_id= " . $id . " GROUP BY " . $tblpx . "scquotation.project_id")->queryAll();
        $this->renderPartial('_projects', array(
            'id' => $id,
            'sql' => $sql,
            'model' => $model

        ));
    }

    public function actionpermissionsave($id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $model = new SubcontractorPermission();
        if (isset($_POST['type']) && count($_POST['type'])) {
            $type = $_POST['type'];
            $userArr = array();
            $res = Yii::app()->db->createCommand("SELECT project_id from " . $tblpx . "subcontractor_permission WHERE subcontractor_id = " . $id)->queryAll();
            foreach ($res as $value) {
                if (!in_array($value['project_id'], $_POST['type'])) {
                    Yii::app()->db->createCommand('delete from ' . $tblpx . 'subcontractor_permission where subcontractor_id=' . $id . ' AND project_id =' . $value['project_id'])->query();
                }
                $userArr[] = $value['project_id'];
            }
            foreach ($type as $ty) {
                if (!in_array($ty, $userArr)) {
                    $query2[] = '(null, ' . $id . ', ' . $ty . ')';
                }
            }
            if (!empty($query2)) {
                $insertassign2 = 'insert into ' . $tblpx . 'subcontractor_permission values ' . implode(',', $query2);
                Yii::app()->db->createCommand($insertassign2)->query();
            }
            $this->redirect(array('subcontractor/newlist'));
        } else {
            if (empty($_POST['type'])) {
                Yii::app()->db->createCommand('delete from ' . $tblpx . 'subcontractor_permission where subcontractor_id=' . $id)->query();
            }
            $this->redirect(array('subcontractor/newlist'));
        }
    }

    public function actiondynamicproject()
    {
        $company_id = $_POST['company_id'];
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}expense_type.company_id)";
        }
        $html['html'] = '';
        $html['status'] = '';
        $html['subcontractor'] = '';
        $expense_type  = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects WHERE FIND_IN_SET(" . $company_id . ",company_id)")->queryAll();
        if (!empty($expense_type)) {
            $html['html'] .= '<option value="">Select Project</option>';
            foreach ($expense_type as $key => $value) {
                $html['html'] .= '<option value="' . $value['pid'] . '">' . $value['name'] . '</option>';
                $html['status'] = 'success';
            }
        } else {
            $html['status'] = 'no_success';
            $html['html'] .= '<option value="">Select Project</option>';
        }

        $subcontractor  = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}subcontractor WHERE FIND_IN_SET(" . $company_id . ",company_id)")->queryAll();
        if (!empty($subcontractor)) {
            $html['subcontractor'] .= '<option value="">Select Subcontractor</option>';
            foreach ($subcontractor as $key => $value) {
                $html['subcontractor'] .= '<option value="' . $value['subcontractor_id'] . '">' . $value['subcontractor_name'] . '</option>';
                $html['status'] = 'success';
            }
        } else {
            $html['status'] = 'no_success';
            $html['subcontractor'] .= '<option value="">Select Subcontractor</option>';
        }
        echo json_encode($html);
    }

    public function actiondynamicexpensehead()
    {
        $id = $_REQUEST['sub_id'];
        $tblpx = Yii::app()->db->tablePrefix;

        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', sub.company_id)";
        }
        $expheadData = Yii::app()->db->createCommand("SELECT ex.type_id as typeid, ex.type_name FROM " . $tblpx . "expense_type ex LEFT JOIN " . $tblpx . "subcontractor_exptype sbe ON ex.type_id = sbe.type_id LEFT JOIN " . $tblpx . "subcontractor sub ON sub.subcontractor_id=sbe.subcontractor_id WHERE sub.subcontractor_id = " . $id . " AND (" . $newQuery . ") GROUP BY ex.type_id")->queryAll();
        $expOptions = "<option value=''>-Select Expense Head-</option>";
        foreach ($expheadData as $eData) {
            $expOptions .= "<option value='" . $eData["typeid"] . "'>" . $eData["type_name"] . "</option>";
        }
        echo $expOptions;
    }
    public function actionQuotationpermission()
    {
        $quotation_id   = $_REQUEST["quotation_id"];
        $model          = Scquotation::model()->findByPk($quotation_id);
        if ($model->scquotation_status == 0)
            $model->scquotation_status = 1;
        if ($model->save()) {
            echo 1;
        } else {
            echo 2;
        }
    }
    public function actionCreatebill()
    {

        $this->render('createbill', array(
            'model' => 1,
        ));
    }
    public function actionWriteoff()
    {
        if (isset($_POST['Writeoff'])) {
            if (isset($_POST['Writeoff']['id']) && !empty($_POST['Writeoff']['id'])) {
                $model      = Writeoff::model()->findByPk($_POST['Writeoff']['id']);
                $message    = "Writeoff updated succesfully!";
            } else {
                $model      = new Writeoff;
                $message    = "Writeoff succesfully added!";
            }
            $model->subcontractor_id    = $_POST['Writeoff']['subcontractor_id'];
            $model->amount              = $_POST['Writeoff']['amount'];
            $model->description         = $_POST['Writeoff']['description'];
            $model->status              = 1;
            if ($model->save()) {
                Yii::app()->user->setFlash('success', $message);
                $this->redirect(array('quotations'));
            } else {
                Yii::app()->user->setFlash('danger', "Failed!");
                $this->redirect(array('quotations'));
            }
        } else {
            Yii::app()->user->setFlash('danger', "Failed!");
            $this->redirect(array('quotations'));
        }
    }
    public function  actionCheckwriteoff()
    {
        $quotation_id   = $_REQUEST["quotation_id"];
        $writeoff       = Writeoff::model()->find(array('condition' => 'subcontractor_id = ' . $quotation_id));
        $quotation = Scquotation::model()->findByPk($quotation_id);
        $qamount = $quotation->getTotalQuotationAmount($quotation_id);
        $sgst_amount = ($quotation->sgst_percent / 100) * $quotation->getTotalQuotationAmount($quotation_id);
        $cgst_amount = ($quotation->cgst_percent / 100) * $quotation->getTotalQuotationAmount($quotation_id);
        $igst_amount = ($quotation->igst_percent / 100) * $quotation->getTotalQuotationAmount($quotation_id);

        $result["id"]           = "";
        $result["quotation"]    = "";
        $result["amount"]       = "";
        $result["description"]  = "";
        $result["status"]       = "";
        $result["quotation_amount"]       =  $qamount+$sgst_amount+$cgst_amount+$igst_amount;

        if ($writeoff) {
            $result["id"]           = $writeoff->id;
            $result["quotation"]    = $writeoff->subcontractor_id;
            $result["amount"]       = $writeoff->amount;
            $result["description"]  = $writeoff->description;
            $result["status"]       = $writeoff->status;            
        } 
        echo json_encode($result);
    }

    public function actionviewQuotation($scquotation_id)
    {
        $company_name = '';
        $model     = Scquotation::model()->find(array("condition" => "scquotation_id = '$scquotation_id'"));
        if (!empty($model->company_id)) {
            $company_model = Company::model()->findByPk($model->company_id);
            if (!empty($company_model))
                $company_name = $company_model['name'];
        }
        if (empty($model)) {
            $this->redirect(array('subcontractor/quotations'));
        }
        $item_model = ScquotationItems::model()->findAll('scquotation_id=' .
            $scquotation_id, array('order' => 'item_id DESC'));
        $quotation_categories = ScQuotationItemCategory::model()->findAll('sc_quotaion_id=' .
            $scquotation_id, array('order' => 'id DESC'));
        $view_datas = $this->setSCQuotationsList($item_model, $model, $quotation_categories);
        $payment_entries = ScquotationPaymentEntries::model()->findAllByAttributes(array('scquotation_id' => $scquotation_id));
        if (isset($_GET['exportpdf'])) {
            $this->logo = $this->realpath_logo;
            $mPDF1 = Yii::app()->ePdf->mPDF();
            $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
            $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
        	$selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
            $template = $this->getTemplate($selectedtemplate);
        	$mPDF1->SetHTMLHeader($template['header']);
            
        	$mPDF1->AddPage('','', '', '', '', 0,0,55, 50, 10,10);
            $mPDF1->WriteHTML($this->renderPartial('_view_quotation', array(
                'model' => $model,
                'company_name' => $company_name,
                'view_datas' => $view_datas,
                'payment_entries' => $payment_entries
            ), true));
            
            $mPDF1->SetHTMLFooter($template['footer']);
            $mPDF1->Output('Subcontraction_quotations.pdf', 'D');
        } else {
            $this->render('_view_quotation', array(
                'model' => $model,
                'company_name' => $company_name,
                'view_datas' => $view_datas,
                'payment_entries' => $payment_entries

            ));
        }
    }

    public function setSCQuotationsList($items_model, $model, $quotation_categories)
    {
        $items = array();
        foreach ($items_model as $item) {
            $items[] = $item->attributes;
        }

        $lists = array();
        if (!empty($quotation_categories)) {
            foreach ($quotation_categories as $quotation_category) {
                $data_items = array();
                $keys = array_keys(array_column($items, 'item_category_id'), $quotation_category->id);
                foreach ($keys as $key) {
                    array_push($data_items, $items[$key]);
                }
                $data = array('category_details' => $quotation_category->attributes, 'items_list' => $data_items);
                array_push($lists, $data);
            }
        } else {
            $data_items = array();
            $data = array();
            foreach ($items as $item) {
                array_push($data_items, $item);
                $data = array('category_details' => '', 'items_list' => $data_items);
            }
            if (!empty($data)) {
                array_push($lists, $data);
            }
        }
        return $lists;
    }


    public function actionSaveInvoice($scqid)
    {
        $model             = Scquotation::model()->find(array("condition" => "scquotation_id='$scqid'"));
        $mPDF1 = Yii::app()->ePdf->mPDF();

        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');

        $mPDF1->WriteHTML($stylesheet, 1);

        $mPDF1->WriteHTML($this->renderPartial('scqpdf', array(
            'model' => $model,
        ), true));

        $mPDF1->Output('Scq.pdf', 'D');
    }

    public function actionaddPayments()
    {
        $data =  $_REQUEST['ScquotationPaymentEntries'];
        if (!empty($data['id'])) {
            $model = ScquotationPaymentEntries::model()->findByPk($data['id']);
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d H:i:s');
        } else {
            $model = new ScquotationPaymentEntries();
            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d H:i:s');
        }
        $model->attributes = $_REQUEST['ScquotationPaymentEntries'];
        if ($model->save()) {
            $payment_list = $this->getPaymentEntries($model);
            $result = array('status' => 1, 'message' => 'Added successfully', 'list' => $payment_list);
        } else {
            $errors = $model->getErrors();
            if (!empty($errors))
                $message = json_encode($errors);
            $result = array('status' => 0, 'message' => $message);
        }

        echo json_encode($result);
    }

    public function getPaymentEntries($model)
    {
        $payments = ScquotationPaymentEntries::model()->findAllByAttributes(array('scquotation_id' => $model->scquotation_id));
        $result = '';
        foreach ($payments as $payment) {
            $result .= '<div class="payment_entries">
            <table><tbody><tr><td><span id="title_' . $payment->id . '">' . $payment->payment_title . '</span></td><td><span id="amount_' . $payment->id . '">' . $payment->amount . '</span></td>';
            $result .= '<td class="text-right"><span><a id="' . $payment->id . '" class="edit_payment_entry" data-attr-qtn="' . $payment->scquotation_id . '"><i class="fa fa-edit"></i></a></span>';
            $result .= '<span><a id="' . $payment->id . '" class="delete_payment_entry"><i class="fa fa-trash"></i></a></span></td></tr><tbody></table></div>';
        }
        return $result;
    }

    public function actionremovepaymententry()
    {
        $result = array('status' => 0, 'message' => 'An error occured');
        if (!empty($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $model = ScquotationPaymentEntries::model()->findByPk($id);
            if ($model->delete()) {
                $payment_list = $this->getPaymentEntries($model);
                $result = array('status' => 1, 'message' => 'Successfully deleted', 'list' => $payment_list);
            }
        }
        echo json_encode($result);
    }

    public function actionaddQuotation($scquotation_id = false)
    {
        $model = new Scquotation();
        $tblpx = Yii::app()->db->tablePrefix;
        $main_item_model = new ScQuotationItemCategory();
        $payment_model = new ScquotationPaymentEntries();
        $payment_entries = ScquotationPaymentEntries::model()->findAllByAttributes(array('scquotation_id' => $scquotation_id));
        $sub_item_model = new ScquotationItems('search');
        $serial_number = '';
        if (!empty($scquotation_id)) {
            $model = Scquotation::model()->findByPk($scquotation_id);
            $payment_stage = Yii::app()->db->createCommand("SELECT max(serial_number) AS serial_number FROM {$tblpx}sc_payment_stage WHERE quotation_id ='".$scquotation_id."'")->queryRow();
            $serial_number = $payment_stage['serial_number'];
            if($serial_number==''){
                $serial_number = 1;
            }else{
                $serial_number++;
            }
        }
        
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $qtnNo="";
        $activeProjectTemplate = $this->getActiveTemplate();
        if ($activeProjectTemplate == 'TYPE-4') {
            $sql = "SELECT count(*)  FROM `jp_scquotation`";  
            $counter = Yii::app()->db->createCommand($sql)->queryScalar();

            if($counter == 0){
                $quotationno = str_pad($counter+1,3,"0",STR_PAD_LEFT);            
                $qtnNo = date('y')."/ARCH/SCQ-".$quotationno;
    
            }else{			
                $last_quotationno = Yii::app()->db->createCommand("SELECT scquotation_no FROM `jp_scquotation` ORDER BY scquotation_id DESC LIMIT 1")->queryScalar();
                
                $scArray = explode('-',$last_quotationno);
                $last_qtn_no = isset($scArray[1])?$scArray[1]:$counter;		
                $next_qtn = str_pad($last_qtn_no+1,3,"0",STR_PAD_LEFT);
                $qtnNo = date('y')."/ARCH/SCQ-".$next_qtn;			
    
            }

                                   
        }

        if (isset($_POST['Scquotation'])) {
            $model->attributes = $_POST['Scquotation'];
            if(empty($_POST['Scquotation']['completion_date'])){
                $completion_date=Date('Y-m-d');
            }else{
                $completion_date= date('Y-m-d', strtotime($_POST['Scquotation']['completion_date']));
            }
            if(empty($_POST['Scquotation']['work_order_date'])){
                $work_order_date=Date('Y-m-d');
            }else{
                $work_order_date= date('Y-m-d', strtotime($_POST['Scquotation']['work_order_date']));
            }
            if(empty($_POST['Scquotation']['scquotation_date'])){
                $scquotation_date = date('Y-m-d');
            } else {
                $scquotation_date = date('Y-m-d', strtotime($_POST['Scquotation']['scquotation_date']));
            }
            if (empty($scquotation_id)) {
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date('Y-m-d H:i:s');
                if (Yii::app()->user->role == 7) {
                    $model->scquotation_status = 0;
                } else {
                    $model->scquotation_status = 1;
                }
            } else {
                $completion_date= $model->completion_date;
                $scquotation_date=$model->scquotation_date;
                $work_order_date=$model->work_order_date;
                if(!empty($_POST['Scquotation']['scquotation_date'])){
                  
                    $scquotation_date = date('Y-m-d', strtotime($_POST['Scquotation']['scquotation_date']));
                }
                if(!empty($_POST['Scquotation']['completion_date'])){
                 
                    $completion_date= date('Y-m-d', strtotime($_POST['Scquotation']['completion_date']));
                }
                if(!empty($_POST['Scquotation']['work_order_date'])){
                 
                    $work_order_date= date('Y-m-d', strtotime($_POST['Scquotation']['work_order_date']));
                }
                $model->updated_by = Yii::app()->user->id;
                $model->updated_date = date('Y-m-d H:i:s');
            }
            $model->completion_date = $completion_date;
            $model->scquotation_date = $scquotation_date;
            $model->work_order_date = $work_order_date;
            if(isset($_POST['Scquotation']['template'])){
               $template= $_POST['Scquotation']['template'];
               if($template != 0){
                $model->labour_template=$template;
               }
                
                
            }
            
            if ($model->save()) {
                $id = $model->scquotation_id;
                $scquot_id=$model->scquotation_id;
                $labour_query      = array();
                $project_id=$_POST['Scquotation']['project_id'];
                if (isset($_POST['Scquotation']['template']) && ($_POST['Scquotation']['template'] != 0) ) {
                    $project_id= $_POST['Scquotation']['project_id'];
                    $projectid= $_POST['Scquotation']['project_id'];
                    $res = Yii::app()->db->createCommand("SELECT DISTINCT template_id from " . $tblpx . "sc_quot_labour_template WHERE sc_quot_id  = " . $id)->queryScalar();
                    if ($res) {                        
                        Yii::app()->db->createCommand('delete from ' . $tblpx . 'sc_quot_labour_template where sc_quot_id=' . $id . ' AND template_id =' . $res)->query();                        
                    }

                    $template = Yii::app()->db->createCommand("SELECT * FROM `jp_labour_template` WHERE `id` = ".$_POST['Scquotation']['template'])->queryRow();

                    if(!empty($template)){
                        $template_id=$_POST['Scquotation']['template'];
                        $arrVal = explode(',', $template['labour_type']);                    
                        foreach ($arrVal as $val) {
                        
                        // echo "<pre>"; print_r($labour_rate);exit;
                        }
                    }
                    
                   $labour_rate_change='0';
                   $template_lab_rate_change=0;
                    if(!empty($_POST['Scquotation']['lab_templates'])){
                       //echo "<pre>";print_r($_POST['Scquotation']['lab_templates']);exit;
                        foreach($_POST['Scquotation']['lab_templates'] as $key=>$val){
                            $labour_rate_change='0';
                            $rate = floatval(str_replace(',', '', $val["rate"]));
                           
                             $type_id = $val['type_id'];
                           
                             $project_id=$_POST['Scquotation']['project_id'];
                             $project_model=Projects::Model()->findByPk($project_id);
                             $proj_template_id= $project_model["labour_template"];
                            if(!empty($proj_template_id)){
                               
                                if($proj_template_id==$template_id){
                                    $sql="SELECT labour_rate FROM `jp_project_labour` WHERE `template_id` = " . $template_id . " AND `project_id` =" . $projectid ." AND labour_id=". $type_id;
                                    $temp_labours_rate= Yii::app()->db->createCommand($sql)->queryRow();
                                
                                    if($rate > $temp_labours_rate["labour_rate"] ){
                                        $labour_rate_change='1';
                                        $template_lab_rate_change++;
                                    }else{
                                        $labour_rate_change='0';
                                    }
                                
                                }else{
                                   
                                   $sql = "SELECT * FROM `jp_labour_template` WHERE `id` = ".$_POST['Scquotation']['template'];
                                   
                                    $template = Yii::app()->db->createCommand($sql)->queryRow();

                                    if(!empty($template)){
                                        $template_id=$_POST['Scquotation']['template'];
                                        $arrVal = explode(',', $template['labour_type']);                   
                                        foreach ($arrVal as $val) {
                                          if($val==$type_id){
                                           
                                            $labour_data_rate = Yii::app()->db->createCommand("SELECT rate FROM `jp_labour_worktype` WHERE `type_id` = " . $val)->queryRow();
                                           
                                            if($rate > $labour_data_rate['rate'] ){
                                              
                                                $labour_rate_change='1';
                                                $template_lab_rate_change++;
                                              
                                            }else{
                                                $labour_rate_change='0';
                                            }
                                          }
                                          
                                           
                                        }
                                    }  
                                }
                                
        
                             }else{
                                $template = Yii::app()->db->createCommand("SELECT * FROM `jp_labour_template` WHERE `id` = ".$_POST['Scquotation']['template'])->queryRow();

                                if(!empty($template)){
                                    $template_id=$_POST['Scquotation']['template'];
                                    $arrVal = explode(',', $template['labour_type']);                    
                                    foreach ($arrVal as $val) {
                                      if($val==$type_id){
                                        $labour_data_rate = Yii::app()->db->createCommand("SELECT rate FROM `jp_labour_worktype` WHERE `type_id` = " . $val)->queryRow();
                                        if($rate > $labour_data_rate['rate'] ){
                                            $labour_rate_change='1';
                                            $template_lab_rate_change++;
                                        }else{
                                            $labour_rate_change='0';
                                        }
                                      }
                                      
                                       
                                    }
                                }
                               
                             }
                        
                            $labour_query[] = '(null, ' . $projectid . ', ' . $template_id . ',' . $type_id . ',' .  $rate . ','. $scquot_id. ', \'' . $labour_rate_change . '\')';;
                        }
                       
                    }
                    //echo "<pre>";print_r($labour_query);exit;
                    if (!empty($labour_query)) {
                        $insertassign = 'insert into ' . $tblpx . 'sc_quot_labour_template values ' . implode(',', $labour_query);
                        Yii::app()->db->createCommand($insertassign)->query();
                    }
                    $upd = Scquotation::Model()->findByPk($model->scquotation_id);
                    if($template_lab_rate_change > 0){

                        $upd->labour_template_rate_change='1';
                        $upd->save();
                    }else{
                        $upd->labour_template_rate_change='0';
                        $upd->save();
                    }

                } else {
                   
                    // if (isset($_POST['Scquotation']['template']) && ($_POST['Scquotation']['template']=='0')) {
                    //     $project_id=$_POST['Scquotation']['project_id'];
                    //     $project_model=Projects::Model()->findByPk($project_id);
                    //     $proj_template_id= $project_model["labour_template"];
                        
                    //     if(!empty($proj_template_id)){
                    //         $scmodel = Scquotation::model()->findByPk($id);
                    //         $scmodel->labour_template=$proj_template_id;
                    //         $scmodel->save();
                    //         $data=Yii::app()->db->createCommand('select * from ' . $tblpx . 'project_labour where project_id=' . $project_id)->queryAll();
                    //         $template_data = [];
                    //        $rate_changes='0';
                    //         foreach($data as $row) {
                               
                    //             $rate = floatval(str_replace(',', '', $row['labour_rate']));
                    //             $template_data[] = [
                                    
                    //                 $project_id,    // project_id
                    //                 $proj_template_id,  // template_id
                    //                 $row['labour_id'], // type_id
                    //                 $rate,         // rate
                    //                 $scquot_id,
                                    
                    //             ];
                    //         }
                           
                    //         if (!empty($template_data)) {
                    //             $values = [];
                    //             foreach ($template_data as $row) {
                    //                 $values[] = '(' . implode(',', $row) . ')';
                    //             }
                    //             $insertassign1 = 'INSERT INTO ' . $tblpx . 'sc_quot_labour_template(project_id,template_id,labour_id,labour_rate,sc_quot_id) VALUES ' . implode(',', $values);
                                
                    //             // Execute the query
                    //             Yii::app()->db->createCommand($insertassign1)->execute();
                    //             $upd1 = Scquotation::Model()->findByPk($model->scquotation_id);
                    //             $upd1->labour_template_rate_change='0';
                    //             $upd1->save();
                    //         }

                    //     }else{
                           
                           
                    //     }
                                               

                    // }
                }
              
                $message = Yii::app()->user->setFlash('success', "Successfully Created");
                $scquotation_id=$model->scquotation_id;
            
                echo json_encode(['status' => '1','scquotation_id' =>$scquotation_id]);
            } else {
                echo json_encode(['status' => '0', 'errors' => $model->getErrors()]);
            }
            Yii::app()->end();
        }
        $termsql = "SELECT * FROM `jp_terms_conditions` WHERE `type`='1'";
        $terms = Yii::app()->db->createCommand($termsql)->queryRow();

        $this->render('_quotation', array('model' => $model, 'user_companies' => $user->company_id, 'main_item_model' => $main_item_model, 'sub_item_model' => $sub_item_model, 'payment_model' => $payment_model, 'payment_entries' => $payment_entries,'scq_no'=>$qtnNo,'terms'=>$terms,'serial_number'=>$serial_number));
    }
    public function actionaddCategoryDesc()
    {
        $message = '';
        $status = 1;
        $errors = array();

        if (isset($_POST['id'])) {

            $id =  $_POST['id'];
        }
        $model = ScQuotationItemCategory::model()->findByPk($id);
        if (isset($_POST['description'])) {
            $model->main_description = $_POST['description'];
        } else {
            $model->main_description = "";
        }

        if (!$model->save()) {
            $errors = json_encode($model->getErrors());
            $status = 0;
        } else {
            $message = 'Successfully Updated';
            $status = 1;
        }
        $result = array('status' => $status, 'message' => $message, 'model' => $model->attributes, 'errors' => $errors);
        echo json_encode($result);
    }
    public function actiongetDynamicQuotNo(){
        if(isset($_POST['company_id'])){
           
            $company_id = $_POST['company_id'];
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $company = Company::model()->findByPk($company_id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        $default_sqno= '';
        
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        $scqno = '';
        $status='';
        $org_prefix_slash="";
        if(!empty($company->sc_quotation_format )){
            $sc_quotation_format =trim($company["sc_quotation_format"]);
			$general_scq_format_arr = explode('/',$sc_quotation_format);
			// print_r($general_scq_format_arr);exit;
			$org_prefix = $general_scq_format_arr[0];
             $org_prefix_slash=$general_scq_format_arr[0].'/';
            
			$prefix_key = 0;
			$scq_key= '';
			$current_key ='';
			foreach($general_scq_format_arr as $key =>$value){
				
				if($value == '{scq_sequence}'){
					$scq_key = $key;
				}else if ($value == '{year}'){
					$current_key =$key;
				}
			}
			
			$current_year = date('Y');
			$previous_year = date('Y') -1;
			$next_year = date('Y') +1;
			
			$where = '';
			if($current_key == 2){
				$where .= " AND `scquotation_no` LIKE '%$current_year'";
			}else if($current_key == 1){
				$where .= " AND `scquotation_no` LIKE '%$current_year%'";
			}
			$sql = "SELECT * FROM ".$tblpx."scquotation 
        WHERE `scquotation_no` LIKE '$org_prefix_slash%' 
          AND company_id = ".$_POST['company_id']." 
        ORDER BY `scquotation_id` DESC 
        LIMIT 1;";

			
			$previous_scq_no='';
			$scq_seq_no = 1;
            $scq_seq_prefix ='SCQ-' ;
			$previous_data = Yii::app()->db->createCommand($sql)->queryRow();
			if( !empty($previous_data) ){
				$previous_scq_no = $previous_data['scquotation_no'];
				$prefix_arr = explode('/', $previous_scq_no);
				$prefix = $prefix_arr[0];
				$previous_prefix_seq_no = $prefix_arr[$scq_key];
                $scq_split = explode('-',$previous_prefix_seq_no);
               
                $previous_prefix_seq_no_val =$scq_split[1];
				$scq_seq_no =  $previous_prefix_seq_no_val + 1;
               				
			}
			$digit = strlen((string) $scq_seq_no);
			if ($this->getActiveTemplate() == 'TYPE-4' && ENV =='production') {
				$sql = "SELECT count(*)  FROM `jp_scquotation`";  
            $counter = Yii::app()->db->createCommand($sql)->queryScalar();

            if($counter == 0){
                $quotationno = str_pad($counter+1,3,"0",STR_PAD_LEFT);            
                $qtnNo = date('y')."/ARCH/SCQ-".$quotationno;
                $default_sqno= $qtnNo;
    
            }else{			
                $last_quotationno = Yii::app()->db->createCommand("SELECT scquotation_no FROM `jp_scquotation` ORDER BY scquotation_id DESC LIMIT 1")->queryScalar();
                
                $scArray = explode('-',$last_quotationno);
                $last_qtn_no = isset($scArray[1])?$scArray[1]:$counter;		
                $next_qtn = str_pad($last_qtn_no+1,3,"0",STR_PAD_LEFT);
                $qtnNo = date('y')."/ARCH/SCQ-".$next_qtn;			
                $status=2;
                $default_sqno= $qtnNo;
            }
					
			}else{
				if ($digit == 1) {
					$scq_no =$scq_seq_prefix.'00' .$scq_seq_no;
				} else if ($digit == 2) {
					$scq_no = $scq_seq_prefix.'0' . $scq_seq_no;
				} else {
					$scq_no =$scq_seq_prefix.$scq_seq_no;
				}
                $status=1;
			}
           
			$new_invoice = str_replace('{year}',$current_year,$sc_quotation_format);
			$new_scq_no = str_replace('{scq_sequence}',$scq_no,$new_invoice);
            //die($new_scq_no);
            $scqno = $new_scq_no;
            
        } 
        
           $result = array('status' => $status,'scqno'=> $scqno,'default_sqno' => $default_sqno);
           echo json_encode($result);
        }
    }
    public function actionaddCategory()
    {
        $model = new ScQuotationItemCategory();
        if (isset($_POST['ScQuotationItemCategory'])) {
            $message = '';
            $status = 1;
            $errors = array();
            if (!empty($_POST['ScQuotationItemCategory']['id'])) {
                $id = $_POST['ScQuotationItemCategory']['id'];
                $message = 'Successfully Updated';
                $model = ScQuotationItemCategory::model()->findByPk($id);
            }
            $model->attributes = $_POST['ScQuotationItemCategory'];
            if (!empty($id)) {
                $model->updated_by = Yii::app()->user->id;
                $model->updated_date = date('Y-m-d H:i:s');
            } else {
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date('Y-m-d H:i:s');
            }
            if (!$model->save()) {
                $errors = json_encode($model->getErrors());
                $status = 0;
            } else {
                $message = 'Successfully Created';
                $status = 1;
                $items = ScquotationItems::model()->findAll(array('condition' => 'item_category_id IS NULL AND scquotation_id = ' . $model->sc_quotaion_id));

                foreach ($items as $item) {
                    $item->item_category_id = $model->id;
                    $item->save();
                }
                // $criteria = new CDbCriteria;
                // $criteria->addCondition('item_category_id IS NULL');
                // ScquotationItems::model()->updateAll(array('item_category_id' =>  $model->id), $criteria);
            }
            $result = array('status' => $status, 'message' => $message, 'model' => $model->attributes, 'errors' => $errors);
            echo json_encode($result);
        }
    }

    public function actiongetItemById()
    {

        $id = $_REQUEST['id'];
        if (!empty($id)) {

            $model = ScquotationItems::model()->findByPk($id);
            echo json_encode($model->attributes);
        }
    }

    public function actionaddQuotationItems()
    {
        if (isset($_POST['ScquotationItems'])) {
            $data = $_POST['ScquotationItems'];
            $ids = $_POST['ScquotationItems']['item_id'];
            $transaction = Yii::app()->db->beginTransaction();
            $quotation_model = Scquotation::model()->findByPk($data['scquotation_id']);
            $termsql = "SELECT * FROM `jp_terms_conditions` WHERE `type`='1'";
            $terms = Yii::app()->db->createCommand($termsql)->queryRow();
            try {
                foreach ($ids as $key => $id) {
                    if (!empty($id)) {
                        $model = ScquotationItems::model()->findByPk($id);
                        $model->item_id = $id;
                    } else {
                        $model = new ScquotationItems();
                        $model->created_by = Yii::app()->user->id;
                        $model->created_date = date('Y-m-d');
                    }
                    $model->scquotation_id = $data['scquotation_id'];
                    $model->item_category_id = $data['item_category_id'];
                    $model->item_description = $data['item_description'][$key];
                    $model->item_type = $data['item_type'][$key];
                    $model->item_quantity = $data['item_quantity'][$key];
                    $model->item_unit = $data['item_unit'][$key];
                    $model->item_rate = $data['item_rate'][$key];
                    $model->item_amount = $data['item_amount'][$key];

                    if ($model->save()) {
                        
                        $success_status = 1;
                        if ($model->approve_status == 'No') {
                            $quotation_model->approve_status = 'No';
                        }
                    } else {
                        $errors = json_encode($model->getErrors());
                        throw new Exception($errors);
                        $success_status = 0;
                    }
                }
                $quotation_model->scquotation_amount = Scquotation::model()->getTotalQuotationAmount($data['scquotation_id']);
                if (!$quotation_model->save()) {
                    $errors = json_encode($quotation_model->getErrors());
                    throw new Exception($errors);
                    $success_status = 0;
                }
                $transaction->commit();
            } catch (Exception $error) {
                $transaction->rollBack();
                $success_status = 0;
                $error =  $error->getMessage();
            } finally {
                if ($success_status == 1) {
                    Yii::app()->user->setFlash('success', "Successfully Created");
                } else {
                    Yii::app()->user->setFlash('error', $error);
                }
                $this->redirect(array('subcontractor/addquotation', 'scquotation_id' => $model->scquotation_id,'terms'=>$terms,'activateTab' => '2'));
                
            }
        }
    }

    public function actionaddPaymentStage(){        
        $model = new ScPaymentStage;
        
        if(isset($_POST['id']) && ($_POST['id'] !="")){
            $model = ScPaymentStage::model()->findByPk($_POST['id']);
        }
        $model->attributes = $_POST;
        if(isset($_POST["floated_percentage"]))
        $model->stage_percent = $_POST["floated_percentage"];
    
        $model->stage_amount ='0.00'; // check if vary with change in quotatn amount
        $tblpx = Yii::app()->db->tablePrefix;
        if(isset($_POST['serial_number'])){
            $serial_number = $_POST['serial_number'];
            if(!empty($serial_number)){
                $serial_no_exists = ScPaymentStage::model()->findByAttributes(array('serial_number'=>$serial_number,'quotation_id'=>$_POST['quotation_id']));
                if(!empty($serial_no_exists)){
                    $exist_serial_no=$serial_no_exists->serial_number;
                    $sql="SELECT * FROM {$tblpx}sc_payment_stage WHERE quotation_id ='".$_POST['quotation_id']."' AND serial_number >= $exist_serial_no";
                    $payment_stages = Yii::app()->db->createCommand($sql )->queryAll();
                   
                    if(!empty($payment_stages)){
                        foreach($payment_stages as $paymentstage){
                            $scpaymodel = ScPaymentStage::model()->findByPk($paymentstage['id']);
                            // echo "<pre>";print_r($model);exit;
                            $newserial_no = $scpaymodel->serial_number+1;
                            $scpaymodel->serial_number=$newserial_no;
                            $scpaymodel->save();
                        }
                    }

                    //echo "<pre>";print_r($payment_stages);exit;
                }
             
            }
        }
        //$model->stage_amount =$_POST["stage_amount"];
        $model->serial_number = isset($_POST['serial_number'])?$_POST['serial_number']:'';

        $model->created_date = date('Y-m-d');
        $model->created_by = Yii::app()->user->id;
        $quotation_id=$model->quotation_id;
        if($model->save()){
            echo json_encode(array('msg' => 'Stage Added','quotation_id'=>$quotation_id));
        }else{
            echo json_encode(array('msg' => 'Error Occured'));
        }
    }

    public function actiongetPaymentStage(){
        $model = ScPaymentStage::model()->findByPk($_POST['id']);
        $quotation_amount =Scquotation::model()->getTotalQuotationAmount($model->quotation_id) ;
        $stage_amount = $quotation_amount * ($model->stage_percent / 100);
        echo json_encode(array('id' => $model->id,
            'payment_stage'=>$model->payment_stage,
            'stage_percent'=>number_format($model->stage_percent,2,'.',''),
            'stage_amount'=>$stage_amount,
            'serial_number'=>$model->serial_number
            )
        );
        
    }

    public function actionremovePaymentStage(){
        
        $model = ScPaymentStage::model()->findByPk($_POST['id']);	
        $tblpx = Yii::app()->db->tablePrefix;
	    $transaction = Yii::app()->db->beginTransaction();
        try {
			if (Yii::app()->user->role != 1) {
				$deletedata = new Deletepending;				
				$deletedata->deletepending_table    = $tblpx . "sc_payment_stage";
				$deletedata->deletepending_data     = json_encode($model->attributes);
				$deletedata->deletepending_parentid = $_REQUEST['id'];
				$deletedata->user_id                = Yii::app()->user->id;
				$deletedata->requested_date         = date("Y-m-d");
				if ($deletedata->save()) {
					
					$model->delete_approve_status=1;

					if (!$model->save()) {
						$success_status = 0;
						throw new Exception(json_encode($model->getErrors()));
					} else {
						$success_status = 2;
                        if(isset($_POST['id'])){
                            $model = ScPaymentStage::model()->findByPk($_POST['id']); 
                            $sql="SELECT * FROM {$tblpx}sc_payment_stage WHERE quotation_id ='".$model['quotation_id']."' AND serial_number > $model->serial_number";
                        $payment_stages = Yii::app()->db->createCommand($sql )->queryAll();
                            if(!empty($payment_stages)){
                                foreach($payment_stages as $paymentstage){
                                    $scpaymodel = ScPaymentStage::model()->findByPk($paymentstage['id']);
                                    // echo "<pre>";print_r($model);exit;
                                    $newserial_no = $scpaymodel->serial_number-1;
                                    $scpaymodel->serial_number=$newserial_no;
                                    $scpaymodel->save();
                                }
                            }
                        }					
					}
				} else {
					$success_status = 0;
					throw new Exception(json_encode($deletedata->getErrors()));
				}
			}else{

				if (!$model->delete()) {
					$success_status = 0;
					throw new Exception(json_encode($model->getErrors()));
				} else {
                    if(isset($_POST['id'])){
                            
                        $sql="SELECT * FROM {$tblpx}sc_payment_stage WHERE quotation_id ='".$model['quotation_id']."' AND serial_number > $model->serial_number";
                        $payment_stages = Yii::app()->db->createCommand($sql )->queryAll();
                            if(!empty($payment_stages)){
                                foreach($payment_stages as $paymentstage){
                                    $scpaymodel = ScPaymentStage::model()->findByPk($paymentstage['id']);
                                    $newserial_no = $scpaymodel->serial_number-1;
                                    $scpaymodel->serial_number=$newserial_no;
                                    $scpaymodel->save();
                                }
                            }
                        }
					$success_status = 1;
										
				}
			}

            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Payment Stage Deleted Successfully'));
				
            }else if ($success_status == 2) {
				echo json_encode(array('response' => 'success', 'msg' => 'Payment Stage Delete Request Sent Successfully'));
			} else {                
                if (isset($error->errorInfo) && ($error->errorInfo[1] == 1451)) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }

    public function actionapproveScItems()
    {
        $quotationId = $_REQUEST['quotation_id'];
        $tblpx = Yii::app()->db->tablePrefix;
        $transaction = Yii::app()->db->beginTransaction();
        
        try {
            // Update all items related to this quotation to approve status
            $updateItems = Yii::app()->db->createCommand("UPDATE {$tblpx}scquotation_items SET approve_status = 'Yes' WHERE scquotation_id = :quotation_id")
                ->bindParam(':quotation_id', $quotationId)
                ->execute();

            if ($updateItems) {
                // Update main quotation approve status to "Yes"
                $updateQuotation = Yii::app()->db->createCommand("UPDATE {$tblpx}scquotation SET approve_status = 'Yes' WHERE scquotation_id = :quotation_id")
                    ->bindParam(':quotation_id', $quotationId)
                    ->execute();
                    
                if ($updateQuotation) {
                    // Create a notification for the approval
                    $quotation = Scquotation::model()->findByPk($quotationId);
                    $notification = new Notifications;
                    $notification->action = "SC Quotation Approved";
                    $notification->message = "Subcontractor quotation for " . (isset($quotation->subcontractor->subcontractor_name) ? $quotation->subcontractor->subcontractor_name : "") . 
                        " in project " . (!empty($quotation->projects) ? $quotation->projects->name : "") . 
                        " on " . date("d-m-Y", strtotime($quotation->scquotation_date)) . " is approved";
                    $notification->parent_id = $quotation->scquotation_id;
                    $notification->date = date("Y-m-d");
                    $notification->requested_by = $quotation->created_by;
                    $notification->approved_by = Yii::app()->user->id;

                    if (!$notification->save()) {
                        echo 0;
                        throw new Exception($notification->getErrors());
                    }
                    
                    echo 1; // Approval successful
                } else {
                    echo 0;
                    throw new Exception('Error in Quotation Update!');
                }
                
                $transaction->commit();
            } else {
                echo 0;
                throw new Exception('Error in Item Update!');
            }
            
        } catch (Exception $error) {
            $transaction->rollback();
            echo 0; 
        }
    }


    public function actionapproveScQuotLabourRateChanges(){
        $scquotation_id=$_REQUEST['quotation_id'];
        $tblpx = Yii::app()->db->tablePrefix;
        $transaction = Yii::app()->db->beginTransaction();
        try{
            $update = Yii::app()->db->createCommand("UPDATE {$tblpx}scquotation SET labour_template_rate_change = '0' WHERE scquotation_id = '" . $_REQUEST['quotation_id'] . "'")->execute();

                if ($update) {
                    echo 1;
                }else{
                    echo 0;                    
                    throw new Exception('Error in Quotation Update !');
                    
                }

           
            $transaction->commit();
            
        }catch (Exception $error){            
            $transaction->rollback();
        }
    }

    public function actionsaveterms(){
        
        $model = new  TermsConditions;
        $msg = "Terms and conditions Added Successfully / ".$_POST['term_id'];

        if(isset($_POST['term_id']) && $_POST['term_id'] !=""){
            $model = TermsConditions::model()->findByPk($_POST['term_id']);
            $msg = "Terms and conditions Updated Successfully / ".$_POST['term_id'];
        }
        
        $model->terms_and_conditions = $_POST['terms'];
        $model->template_id = 1;
        $model->type= "1";
        if($model->save()){
            echo $msg;
        }else{
            echo "<pre>";
            print_r($model->getErrors());exit;
        }
    }

    public function actionaddQuotationTax(){
        $model = Scquotation::model()->findByPk($_POST['qtid']);
        $model->sgst_percent = $_POST['sgst_percent'];
        $model->cgst_percent = $_POST['cgst_percent'];
        $model->igst_percent = $_POST['igst_percent'];
        if($model->save()){
            echo 1;
        }
    }

    public function actionremoveSubcontractor(){
        
        $model = Subcontractor::model()->findByPk($_POST['id']);	
        $tblpx = Yii::app()->db->tablePrefix;
	    $transaction = Yii::app()->db->beginTransaction();
        try {
			if (Yii::app()->user->role != 1) {
				$deletedata = new Deletepending;				
				$deletedata->deletepending_table    = $tblpx . "subcontractor";
				$deletedata->deletepending_data     = json_encode($model->attributes);
				$deletedata->deletepending_parentid = $_REQUEST['id'];
				$deletedata->user_id                = Yii::app()->user->id;
				$deletedata->requested_date         = date("Y-m-d");
				if ($deletedata->save()) {
					
					$model->delete_approve_status=1;
					if (!$model->save()) {
						$success_status = 0;
						throw new Exception(json_encode($model->getErrors()));
					} else {
						$success_status = 2;					
					}
				} else {
					$success_status = 0;
					throw new Exception(json_encode($deletedata->getErrors()));
				}
			}else{

				if (!$model->delete()) {
					$success_status = 0;
					throw new Exception(json_encode($model->getErrors()));
				} else {
					$success_status = 1;
										
				}
			}

            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {            
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Subcontractor Deleted Successfully'));
				
            }else if ($success_status == 2) {
				echo json_encode(array('response' => 'success', 'msg' => 'Subcontractor Delete Request Sent Successfully'));
			} else {                
                if (isset($error->errorInfo) && ($error->errorInfo[1] == 1451)) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }

    public function actionGetItemQuantity(){
        $tbl_px = Yii::app()->db->tablePrefix;
        $id = isset($_POST['id'])?$_POST['id']:'';
        $quotation_id = isset($_POST['quotation_id'])?$_POST['quotation_id']:'';

        $res = Yii::app()->db->createCommand("SELECT SUM(executed_quantity) executed_quantity from " . $tbl_px . "scbillitems WHERE item_id =".$id ." and  scquotation_id=".$quotation_id." ")->queryRow();

        $res_old = Yii::app()->db->createCommand("SELECT item_quantity from " . $tbl_px . "scquotation_items WHERE item_id =".$id ." and  scquotation_id=".$quotation_id." ")->queryRow();

        echo json_encode(array('response' => 'success', 'executed_quantity'=>$res['executed_quantity'],'old_quantity'=>$res_old['item_quantity']));
    }
    
    public function actionCloneQuotation($scquotation_id = false)
{
    if (!empty($scquotation_id)) {
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $model = Scquotation::model()->findByPk($scquotation_id);

            if (!$model) {
                throw new Exception("Quotation not found");
            }

            // Calculate the new scquotation_no here.
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $qtnNo="";
            $activeProjectTemplate = $this->getActiveTemplate();
            if ($activeProjectTemplate == 'TYPE-4') {
                $sql = "SELECT count(*)  FROM `jp_scquotation`";  
                $counter = Yii::app()->db->createCommand($sql)->queryScalar();
    
                if($counter == 0){
                    $quotationno = str_pad($counter+1,3,"0",STR_PAD_LEFT);            
                    $qtnNo = date('y')."/ARCH/SCQ-".$quotationno;
        
                }else{			
                    $last_quotationno = Yii::app()->db->createCommand("SELECT scquotation_no FROM `jp_scquotation` ORDER BY scquotation_id DESC LIMIT 1")->queryScalar();
                    
                    $scArray = explode('-',$last_quotationno);
                    $last_qtn_no = isset($scArray[1])?$scArray[1]:$counter;		
                    $next_qtn = str_pad($last_qtn_no+1,3,"0",STR_PAD_LEFT);
                    $qtnNo = date('y')."/ARCH/SCQ-".$next_qtn;			
        
                } 
            }
            $clone_model = new Scquotation();
            $clone_model->attributes = $model->attributes;
            $clone_model->scquotation_no = $model->scquotation_no.uniqid();
            $clone_model->created_by = Yii::app()->user->id;
            $clone_model->created_date = date('Y-m-d H:i:s');
            $clone_model->sgst_percent = $model->sgst_percent;
            $clone_model->cgst_percent = $model->cgst_percent;
            $clone_model->igst_percent = $model->igst_percent;
            $clone_model->scquotation_amount = Scquotation::model()->getTotalQuotationAmount($scquotation_id);
                
            if (!$clone_model->save()) {
                throw new Exception("Error saving cloned Scquotation");
            }
            //clone category
            $scquotationcategories = ScQuotationItemCategory::model()->findAllByAttributes(array('sc_quotaion_id' => $scquotation_id));
            
            foreach ($scquotationcategories as $sc_category_model) {
                $clone_category_model = new ScQuotationItemCategory();
                $clone_category_model->attributes = $sc_category_model->attributes;
                $clone_category_model->sc_quotaion_id = $clone_model->scquotation_id;
                $clone_category_model->created_by = Yii::app()->user->id;
                $clone_category_model->created_date = date('Y-m-d H:i:s');

                if (!$clone_category_model->save()) {
                    throw new Exception("Error saving cloned ScquotationCategory");
                }
                // Clone ScquotationItems
                $scquotationItems = ScquotationItems::model()->findAllByAttributes(array('scquotation_id' => $scquotation_id,'item_category_id' => $sc_category_model->id));
                foreach ($scquotationItems as $sc_item_model) {
                    $clone_sc_item_model = new ScquotationItems();
                    $clone_sc_item_model->attributes = $sc_item_model->attributes;
                    $clone_sc_item_model->scquotation_id = $clone_model->scquotation_id;
                    $clone_sc_item_model->item_category_id =$clone_category_model->id;
                    $clone_sc_item_model->created_by = Yii::app()->user->id;
                    $clone_sc_item_model->created_date = date('Y-m-d H:i:s');

                    if (!$clone_sc_item_model->save()) {
                        throw new Exception("Error saving cloned ScquotationItems");
                    }
                }
            }
             
            //clone payment
            $payment_entries = ScquotationPaymentEntries::model()->findAllByAttributes(array('scquotation_id' => $scquotation_id));
            foreach ($payment_entries as $sc_payment_model) {
                $clone_sc_payment_model = new ScquotationPaymentEntries();
                $clone_sc_payment_model->attributes = $sc_payment_model->attributes;
                $clone_sc_payment_model->scquotation_id = $clone_model->scquotation_id;
                $clone_sc_payment_model->created_by = Yii::app()->user->id;
                $clone_sc_payment_model->created_date = date('Y-m-d H:i:s');

                if (!$clone_sc_payment_model->save()) {
                    throw new Exception("Error saving cloned ScquotationPaymentEntries");
                }
            }
            //clone stage
            $payment_stages = ScPaymentStage::model()->findAllByAttributes(array('quotation_id' => $scquotation_id));
            foreach ($payment_stages as $sc_stage_model) {
                $clone_sc_stage_model = new ScPaymentStage();
                $clone_sc_stage_model->attributes = $sc_stage_model->attributes;
                $clone_sc_stage_model->quotation_id = $clone_model->scquotation_id;
                $clone_sc_stage_model->bill_status = 0;
                $clone_sc_stage_model->serial_number = $sc_stage_model->serial_number;
                $clone_sc_stage_model->created_by = Yii::app()->user->id;
                $clone_sc_stage_model->created_date = date('Y-m-d H:i:s');

                if (!$clone_sc_stage_model->save()) {
                    throw new Exception("Error saving cloned ScquotationStages");
                }
            }
            $transaction->commit();
            Yii::app()->user->setFlash('success', "Successfully Cloned");
        } catch (Exception $error) {
            $transaction->rollBack();
            Yii::app()->user->setFlash('error', "Error: " . $error->getMessage());
        }
    }
    $this->redirect(array('subcontractor/quotations'));
    }
    public function actiongetTemplateDetails(){
        $template = $_POST['template_id'];
        $type=1;
       
        $project='';
        if(isset($_POST['type'])){
            $type=$_POST['type'];
        }
        if(isset($_POST['project'])){
            $project=$_POST['project'];
        }
        echo $this->renderPartial('_sc_labour_template',array('template_id'=>$template,'type'=>$type,'project'=>$project));
    }

}
