<?php
/* @var $this QuotationItemMasterController */
/* @var $model QuotationItemMaster */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'quotation_category_id'); ?>
		<?php echo $form->textField($model,'quotation_category_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'work_type_id'); ?>
		<?php echo $form->textField($model,'work_type_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'worktype_label'); ?>
		<?php echo $form->textField($model,'worktype_label',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'work_type_description'); ?>
		<?php echo $form->textField($model,'work_type_description',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'image'); ?>
		<?php echo $form->textField($model,'image',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'material_id'); ?>
		<?php echo $form->textField($model,'material_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'finish_id'); ?>
		<?php echo $form->textField($model,'finish_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rate'); ?>
		<?php echo $form->textField($model,'rate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'profit'); ?>
		<?php echo $form->textField($model,'profit'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_on'); ?>
		<?php echo $form->textField($model,'created_on'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->