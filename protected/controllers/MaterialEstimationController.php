<?php

class MaterialEstimationController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
    {
        $accessArr = array();
        $accessauthArr = array();
        $controller = Yii::app()->controller->id;
        if (isset(Yii::app()->user->menuall)) {
            if (array_key_exists($controller, Yii::app()->user->menuall)) {
                $accessArr = Yii::app()->user->menuall[$controller];
            }
        }

        if (isset(Yii::app()->user->menuauth)) {
            if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                $accessauthArr = Yii::app()->user->menuauth[$controller];
            }
        }
        $access_privlg = count($accessauthArr);
        return array(
            array(
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => $accessArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),

            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),

            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('ajaxcall', 'dynamicVendor'),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MaterialEstimation;
		$model_labour=new LabourEstimation;
		$model_equipment = new EquipmentsEstimation;
		$estimation='';
		$project_id='';
		
		if(isset($_POST['project_id'])){
			$project_id=$_POST['project_id'];

		}

		if(isset($_POST['estimation'])){
			$estimation=$_POST['estimation'];
		}

        if(isset($_POST['MaterialEstimation']['project_id'])){
			$project_id=$_POST['MaterialEstimation']['project_id'];
		}
		if(isset($_POST['LabourEstimation']['project_id'])){
			$project_id=$_POST['LabourEstimation']['project_id'];
		}
		if(isset($_POST['EquipmentsEstimation']['project_id'])){
			$project_id=$_POST['EquipmentsEstimation']['project_id'];
		}
		$model_exist= Itemestimation::model()->findAll(array("condition" => "project_id = '$project_id'"));
		if(empty($model_exist)){
			if(!empty($project_id)){
				$modelItemEstimation = new Itemestimation;
				$modelItemEstimation->project_id =$project_id;
				$modelItemEstimation->itemestimation_status = 1;
				$modelItemEstimation->save();
			}
		}
		else{
			$modelItemEstimation = Itemestimation::model()->findByAttributes(array("project_id" => $project_id));
		}
		
		if(isset($_POST['LabourEstimation']))
		{
			$model_labour->attributes=$_POST['LabourEstimation'];
            $model_labour->estimation_id=$modelItemEstimation->itemestimation_id;
			if($model_labour->save()){
				$this->redirect(array('index', 'project_id' => $model_labour->project_id));
			}
		}
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['MaterialEstimation']))
		{
			$model->attributes=$_POST['MaterialEstimation'];
			$model->estimation_id=$modelItemEstimation->itemestimation_id;
			if (isset($model->specification) && is_array($model->specification)) {
				$model->specification = implode(',', $model->specification);
			}
			if($model->save())
			{
				$this->redirect(array('index', 'project_id' => $model->project_id));
			}
		}

		if (isset($_POST['EquipmentsEstimation'])) {
			
			$model_equipment->attributes = $_POST['EquipmentsEstimation'];
			$model_equipment->estimation_id=$modelItemEstimation->itemestimation_id;
			if ($model_equipment->save()) {
				$this->redirect(array('index', 'project_id' => $model_equipment->project_id));
			}
		}

		$this->renderPartial('create',array(
			'model'=>$model,
			'model_labour'=>$model_labour,
			'model_equipment'=>$model_equipment,
			'estimation'=>$estimation,
			'project_id'=>$project_id,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$estimation = '';
		$project_id='';
		
		if(isset($_POST['project_id'])){
			$project_id=$_POST['project_id'];
		}
		if (isset($_POST['estimation'])) {
			$estimation = $_POST['estimation'];
		}

		$model = null;
		$model_labour = null;
		$model_equipment = null;
		
		if ($estimation == 'material_estimation') {
			$model = $this->loadModel($id);
		} elseif ($estimation == 'labour_estimation') {
			$model_labour = LabourEstimation::model()->findByPk($id);
			
		} elseif ($estimation == 'equipment_estimation') {
			$model_equipment = EquipmentsEstimation::model()->findByPk($id);
			
		}
		
		if (isset($_POST['MaterialEstimation'])) {
			$model = $this->loadModel($id);
			if(!empty($model)){
				$model->attributes = $_POST['MaterialEstimation'];
				if (isset($model->specification) && is_array($model->specification)) {
					$model->specification = implode(',', $model->specification);
				}
				if ($model->save()) {
										
					$estimationModel=MaterialEstimation::model()->findByPk($id);
                   // $estimation_det = $estimationModel
					["estimation_id"];  
					$est_Model = Itemestimation::Model()->findByPk($estimationModel
					["estimation_id"]); 
					 if ($est_Model !== null) { 
							$est_Model->itemestimation_status = 1;
							$est_Model->save();
					}
						
						
					$this->redirect(array('editEstimation','project_id' => $model->project_id));
				}

				
				
			}
			
		} elseif (isset($_POST['LabourEstimation'])) {
			$model_labour = LabourEstimation::model()->findByPk($id);
			// No need to find the model again; its already loaded above
			if(!empty($model_labour)){
				$model_labour->attributes = $_POST['LabourEstimation'];
				$estimation_det = $model_labour->estimation_id;
				$est_Model = Itemestimation::Model()->findByPk($estimation_det);

				if ($model_labour->save()) {
					$est_Model->itemestimation_status=1;
					$est_Model->save();
					$this->redirect(array('editEstimation','project_id' => $model_labour->project_id));
				}	
			}
			
		} elseif (isset($_POST['EquipmentsEstimation'])) {
			$model_equipment = EquipmentsEstimation::model()->findByPk($id);
			$model_equipment->attributes = $_POST['EquipmentsEstimation'];
			if(!empty($model_equipment)){
				if ($model_equipment->save()) {
					$estimation_det = $model_equipment->estimation_id;
					$est_Model = Itemestimation::Model()->findByPk($estimation_det);
					$est_Model->itemestimation_status=1;
					$est_Model->save();
					$this->redirect(array('editEstimation','project_id' => $model_equipment->project_id));
				}

			}
			
		}
		// print_r($model_labour);exit;
		$this->renderPartial('update', array(
			'model' => $model,
			'model_labour' => $model_labour,
			'model_equipment' => $model_equipment,
			'estimation' => $estimation,
			'project_id'=>$project_id,
		));
	}

	public function actioneditEstimation(){
		$project_id = $_REQUEST["project_id"];
        $tblpx      = Yii::app()->db->tablePrefix;
        $pmodel     = Projects::model()->findByPk($project_id);
        $model      = Itemestimation::model()->findAll(array("condition" => "project_id = '$project_id'"));
        //material Estimation
        $model_material = MaterialEstimation::model()->findAll(array("condition" => "project_id = '$project_id'"));
        $material_ids = array_map(function($item) {
            return $item->primaryKey; 
        }, $model_material);
        $criteria = new CDbCriteria();
        $criteria->addInCondition('id', $material_ids); 
        $dataProvider = new CActiveDataProvider('MaterialEstimation', array(
            'criteria' => $criteria,
        ));
        //labour estimation
        $model_labour = LabourEstimation::model()->findAll(array("condition" => "project_id = '$project_id'"));
        $labour_ids = array_map(function($item) {
            return $item->primaryKey; 
        }, $model_labour);

        $criteria_labour = new CDbCriteria();
        $criteria_labour->addInCondition('id', $labour_ids);

        $dataProviderLabour = new CActiveDataProvider('LabourEstimation', array(
            'criteria' => $criteria_labour,
        ));
         //equipment estimation
         $model_equipment = EquipmentsEstimation::model()->findAll(array("condition" => "project_id = '$project_id'"));
         $equipment_ids = array_map(function($item) {
             return $item->primaryKey; 
         }, $model_equipment);
         $criteria_equipment = new CDbCriteria();
         $criteria_equipment->addInCondition('id', $equipment_ids);
         $dataProviderEquipment = new CActiveDataProvider('EquipmentsEstimation', array(
             'criteria' => $criteria_equipment,
         ));
		 $editFlag=true;
        $this->render('index', array(
            'model' => $model,
			'flag'=>$editFlag,
            'project_id' => $project_id,
            'model_material' => $model_material,
            'model_labour' => $model_labour,
            'model_equipment' => $model_equipment,
            'projects' => $pmodel,
            'dataProvider' => $dataProvider,
            'dataProvider1' => $dataProviderLabour,
            'dataProvider2' => $dataProviderEquipment,
        ));
	}

	public function actionGetMaterialData() {
		$materialId = Yii::app()->request->getPost('material_id');
		$projectId = Yii::app()->request->getPost('project_id');
		$materialItems = ApiMaterials::model()->findByAttributes(array('template_material_id' => $materialId));

		if ($materialItems !== null) {
			$pms_api_integration_model=ApiSettings::model()->findByPk(1);
                $pms_api_integration =$pms_api_integration_model->api_integration_settings;

			if($pms_api_integration==1){
				$exists = MaterialEstimation::model()->exists(
					'material = :material AND project_id = :project_id',
					array(':material' => $materialId, ':project_id' => $projectId)
				);
				if ($exists) {
					echo json_encode(array('status' => 'exists', 'message' => 'Material alredy exists.'));
					Yii::app()->end();
				}

			}

			
			$materials = Materials::model()->findAllByAttributes(array('id' => $materialItems->coms_material_id));
			
			$materialsData = array();
			foreach ($materials as $material) {
				$materialsData[] = $material->attributes;
			}
			$specificationIds = explode(',', $materialsData[0]['specification']);
			$specifications = Specification::model()->findAllByAttributes(array('id' => $specificationIds));
			$specificationsData = array();
			foreach ($specifications as $specification) {
				$specificationsData[] = $specification->attributes;
			}
			$response = array(
				'status' => 'success',
				'data' => $materialItems->attributes,
				'data1' => $materialsData,
				'data2'=> $specificationsData,
			);
		} else {
			$response = array(
				'status' => 'error',
				'message' => 'Material not found.',
			);
		}
	
		echo CJSON::encode($response);
		Yii::app()->end();
	}
	
	public function actionGetLabourData() {
		$labourId = Yii::app()->request->getPost('labour_id');
		$labourItems = ApiLabours::model()->findByAttributes(array('template_labour_id' => $labourId));
		
		
		if ($labourItems !== null) {
			$response = array(
				'status' => 'success',
				'data' => $labourItems->attributes,
			);
		} else {
			$response = array(
				'status' => 'error',
				'message' => 'Material not found.',
			);
		}
	
		echo CJSON::encode($response);
		Yii::app()->end();
	}
	
	public function actionGetEquipmentData() {
		$equipmentId = Yii::app()->request->getPost('equipment_id');
		$equipmentItems = ApiEquipments::model()->findByAttributes(array('template_equipment_id' => $equipmentId));
		
		
		if ($equipmentItems !== null) {
			$response = array(
				'status' => 'success',
				'data' => $equipmentItems->attributes,
			);
		} else {
			$response = array(
				'status' => 'error',
				'message' => 'Material not found.',
			);
		}
	
		echo CJSON::encode($response);
		Yii::app()->end();
	}

	public function actionRefreshButton(){
		$returnUrl = Yii::app()->request->getParam('returnUrl');
		$attributes = array('project_id' => null);
		$projectPms=Itemestimation::model()->findAllByAttributes($attributes);
		if(!empty($projectPms)){
			foreach($projectPms as $project){
				$projectId = $project->pms_project_id;

				$acc_project_model = Projects::model()
				->findByAttributes(array('pms_project_id' => $projectId));
				if ($acc_project_model !== null) {
					$acc_project_id = $acc_project_model->pid;
					$project->project_id = $acc_project_id;
					$project->update();
				}
			}
		}
		//Material Refresh
        // $attributes = array('coms_project_id' => null);
        // $materialsProject = ApiMaterials::model()->findAllByAttributes($attributes);
        // 	if(!empty($materialsProject)){
        //         foreach($materialsProject as $material){
        //             $pms_project_id_from_Estimation = $material->project_id;

        //             $acc_project_model = Projects::model()
		// 			->findByAttributes(array('pms_project_id' => $pms_project_materialid_from_Estimation));
        //             if ($acc_project_model !== null) {
        //                 $acc_project_id = $acc_project_model->pid;
        //                 $material->coms_project_id = $acc_project_id;
        //                 $material->update();
        //             }
        //         }
        //     }
		$attribute_materail = array('material' => null);
		$materialsId = MaterialEstimation::model()->findAllByAttributes($attribute_materail);
			
        	if(!empty($materialsId)){
                foreach($materialsId as $material){
                    $pms_material_id_from_Estimation = $material->pms_material_id;

                    $acc_materaial_model = Materials::model()
					->findByAttributes(array('pms_material_id' => $pms_material_id_from_Estimation));

                    if ($acc_materaial_model !== null) {
                        $acc_material_id = $acc_materaial_model->id;
                        $material->material = $acc_material_id;
                        $material->update();
                    }
                }
            }
		$attribute_unit = array('unit' => null);
		$materialsUnit = MaterialEstimation::model()->findAllByAttributes($attribute_unit);
		
		if(!empty($materialsUnit)){
			foreach($materialsUnit as $material){
				$pms_unit_id_from_Estimation = $material->pms_unit_id;

				$acc_unit_model = Unit::model()
				->findByAttributes(array('pms_unit_id' => $pms_unit_id_from_Estimation));
				
				if ($acc_unit_model !== null) {
					$acc_unit_id = $acc_unit_model->id;
					$material->unit = $acc_unit_id;
					$material->update();
				}
			}
		}
		//Labour Refesh
		// $attributes = array('coms_project_id' => null);
		// $laboursProject = ApiLabours::model()->findAllByAttributes($attributes);
		// if (!empty($laboursProject)) {
		// 	foreach ($laboursProject as $labour) {
		// 		$pms_project_id_from_Estimation = $labour->project_id;

		// 		$acc_project_model = Projects::model()->findByAttributes(array('pms_project_id' => $pms_project_id_from_Estimation));
		// 		if ($acc_project_model !== null) {
		// 			$acc_project_id = $acc_project_model->pid;
		// 			$labour->coms_project_id = $acc_project_id;
		// 			$labour->update();
		// 		}
		// 	}
		// }

		$attribute_labour = array('labour' => null);
		$laboursId = LabourEstimation::model()->findAllByAttributes($attribute_labour);
		if (!empty($laboursId)) {
			foreach ($laboursId as $labour) {
				$pms_labour_id_from_Estimation = $labour->pms_labour_id;

				$acc_labour_model = LabourWorktype::model()->findByAttributes(array('pms_labour_id' => $pms_labour_id_from_Estimation));
				if ($acc_labour_model !== null) {
					$acc_labour_id = $acc_labour_model->type_id;
					$labour->labour = $acc_labour_id;
					$labour->update();
				}
			}
		}

		$attribute_unit = array('unit' => null);
		$laboursUnit = LabourEstimation::model()->findAllByAttributes($attribute_unit);
		if (!empty($laboursUnit)) {
			foreach ($laboursUnit as $labour) {
				$pms_unit_id_from_Estimation = $labour->pms_unit_id;

				$acc_unit_model = Unit::model()->findByAttributes(array('pms_unit_id' => $pms_unit_id_from_Estimation));
				if ($acc_unit_model !== null) {
					$acc_unit_id = $acc_unit_model->id;
					$labour->unit = $acc_unit_id;
					$labour->update();
				}
			}
		}
		//Equipment Refresh 
		// $attributes = array('coms_project_id' => null);
		// $equipmentsProject = ApiEquipments::model()->findAllByAttributes($attributes);
		// if (!empty($equipmentsProject)) {
		// 	foreach ($equipmentsProject as $equipment) {
		// 		$pms_project_id_from_Estimation = $equipment->project_id;

		// 		$acc_project_model = Projects::model()->findByAttributes(array('pms_project_id' => $pms_project_id_from_Estimation));
		// 		if ($acc_project_model !== null) {
		// 			$acc_project_id = $acc_project_model->pid;
		// 			$equipment->coms_project_id = $acc_project_id;
		// 			$equipment->update();
		// 		}
		// 	}
		// }

		$attribute_equipment = array('equipment' => null);
		$equipmentsId = EquipmentsEstimation::model()->findAllByAttributes($attribute_equipment);
		if (!empty($equipmentsId)) {
			foreach ($equipmentsId as $equipment) {
				$pms_equipment_id_from_Estimation = $equipment->pms_equipment_id;

				$acc_equipment_model = Equipments::model()->findByAttributes(array('pms_equipment_id' => $pms_equipment_id_from_Estimation));
				if ($acc_equipment_model !== null) {
					$acc_equipment_id = $acc_equipment_model->id;
					$equipment->equipment = $acc_equipment_id;
					$equipment->update();
				}
			}
		}

		$attribute_unit = array('unit' => null);
		$equipmentsUnit = EquipmentsEstimation::model()->findAllByAttributes($attribute_unit);
		if (!empty($equipmentsUnit)) {
			foreach ($equipmentsUnit as $equipment) {
				$pms_unit_id_from_Estimation = $equipment->pms_unit_id;

				$acc_unit_model = Unit::model()->findByAttributes(array('pms_unit_id' => $pms_unit_id_from_Estimation));
				if ($acc_unit_model !== null) {
					$acc_unit_id = $acc_unit_model->id;
					$equipment->unit = $acc_unit_id;
					$equipment->update();
				}
			}
		}

        $this->redirect($returnUrl);
    }

	public function actionquantityApprove(){
		$estimation = Yii::app()->request->getPost('estimation');
        $itemId = Yii::app()->request->getPost('entryId');
        $quantity = Yii::app()->request->getPost('quantity');
        
		if ($estimation == 'material_estimation') {
            $model = $this->loadModel($itemId);
			if($model->quantity>$quantity){
				$itemEstimation = Itemestimation::model()->findByAttributes(array('project_id' => $model->project_id));
				$itemEstimation->itemestimation_status=3;
				$itemEstimation->save();
			}
        } else if ($estimation == 'labour_estimation') {
            $model_labour = LabourEstimation::model()->findByPk($itemId);
			if($model_labour->quantity>$quantity){
				$itemEstimation = Itemestimation::model()->findByAttributes(array('project_id' => $model_labour->project_id));
				$itemEstimation->itemestimation_status=3;
				print_r($itemEstimation);exit;
				$itemEstimation->save();
			}
        } else if ($estimation == 'equipment_estimation') {
            $model_equipment = EquipmentsEstimation::model()->findByPk($itemId);
			if($model_equipment->quantity>$quantity){
				$itemEstimation = Itemestimation::model()->findByAttributes(array('project_id' => $model_equipment->project_id));
				$itemEstimation->itemestimation_status=3;
				$itemEstimation->save();
			}
        }
        
        $response = array(
            'status' => 'success',
            'data' => 'Your response data here'
        );
        
        echo CJSON::encode($response);
        Yii::app()->end();	
	}
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$response = [
			'success' => 'error',
			'message' => 'An error occurred while deleting the record.'
		];

		if (isset($_POST['estimation'])) {
			$estimation = $_POST['estimation'];

			$model = null;

			try {
				if ($estimation == 'material_estimation') {
					$model = $this->loadModel($id);
				} elseif ($estimation == 'labour_estimation') {
					$model = LabourEstimation::model()->findByPk($id);
				} elseif ($estimation == 'equipment_estimation') {
					$model = EquipmentsEstimation::model()->findByPk($id);
				}

				if ($model !== null) {
					if ($model->delete()) {
						$response = [
							'success' => 'success',
							'message' => 'Record deleted successfully.'
						];
					} else {
						throw new Exception(json_encode($model->getErrors()));
					}
				} else {
					$response['message'] = 'Record not found.';
				}
			} catch (Exception $e) {
				$response['message'] = $e->getMessage();
			}
		}

		echo json_encode($response);
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('MaterialEstimation');
		$dataProvider1=new CActiveDataProvider('LabourEstimation');
		$dataProvider2=new CActiveDataProvider('EquipmentsEstimation');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'dataProvider1'=>$dataProvider1,
			'dataProvider2'=>$dataProvider2,
		));
	}
	public function actionFetchProjectData()
	{
		$project_id = Yii::app()->request->getParam('project_id');

		// Fetch the data based on the project ID
		$model_material = MaterialEstimation::model()->findAll(array("condition" => "project_id = '$project_id'"));
        $material_ids = array_map(function($item) {
            return $item->primaryKey; 
        }, $model_material);
        $criteria = new CDbCriteria();
        $criteria->addInCondition('id', $material_ids); 
        $dataProviderMaterial = new CActiveDataProvider('MaterialEstimation', array(
            'criteria' => $criteria,
        ));

		 //labour estimation
		 $model_labour = LabourEstimation::model()->findAll(array("condition" => "project_id = '$project_id'"));
		 $labour_ids = array_map(function($item) {
			 return $item->primaryKey; 
		 }, $model_labour);
 
		 $criteria_labour = new CDbCriteria();
		 $criteria_labour->addInCondition('id', $labour_ids);
 
		 $dataProviderLabour = new CActiveDataProvider('LabourEstimation', array(
			 'criteria' => $criteria_labour,
		 ));
		  //equipment estimation
		  $model_equipment = EquipmentsEstimation::model()->findAll(array("condition" => "project_id = '$project_id'"));
		  $equipment_ids = array_map(function($item) {
			  return $item->primaryKey; 
		  }, $model_equipment);
		  $criteria_equipment = new CDbCriteria();
		  $criteria_equipment->addInCondition('id', $equipment_ids);
		  $dataProviderEquipment = new CActiveDataProvider('EquipmentsEstimation', array(
			  'criteria' => $criteria_equipment,
		  ));
		// echo ////////
		// Render partial views for each table
		$materialTable = $this->renderPartial('_materialTable', array('dataProvider' => $dataProviderMaterial), true);
		$labourTable = $this->renderPartial('_labourTable', array('dataProvider1' => $dataProviderLabour), true);
		$equipmentTable = $this->renderPartial('_equipmentTable', array('dataProvider2' => $dataProviderEquipment), true);

		// Return the rendered tables as JSON
		echo CJSON::encode(array(
			'materialTable' => $materialTable,
			'labourTable' => $labourTable,
			'equipmentTable' => $equipmentTable,
		));
	}


	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new MaterialEstimation('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MaterialEstimation']))
			$model->attributes=$_GET['MaterialEstimation'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MaterialEstimation the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MaterialEstimation::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MaterialEstimation $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='material-estimation-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
