<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/moment.js"></script>
<script  type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<script src="<?php echo Yii::app()->baseUrl . '/ckeditor/ckeditor.js'; ?>"></script>
<style>
    .cke_contents {
        height: 80px !important;
    }

    .panel-body>.panel {
        margin-top: 10px;
    }

    .pb-2 {
        padding-bottom: 10px;
    }

    .panel-heading.h-head {
        height: auto;
    }

    .master_worktype {
        padding: 10px 15px;        
    }    

    #quotation_item{
        background:#f8fbff;
    }

    .add_new_sec button {
        border: none;
        box-shadow: none;
    }

    .add_new_sec h4 {
        margin: 0px !important;
        padding-top: 10px;
    }


    .panel-heading h4 {
        margin: 0px !important;
    }

    h4.categ_item {
        color: #337ab7;
        line-height: 3px;
        font-size: 1.5rem;
        font-weight: bold;
    }

    i.toggle-caret {
        font-size: 2rem;
        font-weight: normal;
        color: #3383ca;
    }

    input.total_sum {
        width: 50px;
        margin-right: 3rem;
    }

    .sum_sec {
        max-width: 400px;
        text-align: right;
        margin-left: auto;
        background: #fafafa;
        margin-bottom: 10px;
        font-weight: bold;
        white-space: nowrap;
        padding: 10px;
    }

    .toast-container {
        width: 350px;
    }

    .toast-position-top-right {
        top: 57px;
        right: 6px;
    }

    .toast-item-close {
        background-image: none;
        cursor: pointer;
        width: 12px;
        height: 12px;
        text-align: center;
        border-radius: 2px;
    }

    .toast-item-image {
        font-size: 24px;
    }

    .toast-item-close:hover {
        color: red;
    }

    .toast-item {
        border: transparent;
        border-radius: 3px;
        font-size: 10px;
        opacity: 1;
        background-color: rgba(34, 45, 50, 0.8);
    }

    .toast-item-wrapper p {
        margin: 0px 5px 0px 42px;
        font-size: 14px;
        text-align: justify;
    }


    .toast-type-success {
        background-color: #00A65A;
        border-color: #00A65A;
    }

    .toast-type-error {
        background-color: #DD4B39;
        border-color: #DD4B39;
    }

    .toast-type-notice {
        background-color: #00C0EF;
        border-color: #00C0EF;
    }

    .toast-type-warning {
        background-color: #F39C12;
        border-color: #F39C12;
    }

    .permission_style {
        background: #f8cbcb !important;
    }

    .addRow label {
        display: inline-block;
    }

    input[type="radio"] {
        margin: 4px 4px 0;
    }

    input[type="checkbox"][readonly] {
        pointer-events: none;
    }

    .row-margin {
        padding-bottom: 50px;
    }

    .row-margin-append {
        margin-top: 10px;
        margin-left: 0px;
        margin-right: 0px;
    }

    .button-panel {
        margin-top: 30px;
    }

    .panel-margin {
        padding-left: 30px;
        padding-right: 30px;
    }

    span.select2.select2-container.select2-container--default {
        width: 100% !important;
    }

    .qitem_table tbody tr td {
        vertical-align: bottom;
    }
    table.table tbody tr.catehead td {
        background-color: #c5c5dd;
        font-weight: 600;
        color: #000;
        padding: 15px 10px;}
    h4{
        margin: 10px 0px !important;
    }
    .p-0{
        padding:0px !important;
    }
    .p-1{
        padding: 1.5rem;
    }
    .m-1{
        margin: 1.5rem;
    }
    .bg-grey{
        background: #e0e0e0;
        padding: 10px;
    }
    .w-40{
        width:40%;
    }
</style>
<?php if (Yii::app()->user->hasFlash('success')) : ?>
    <div class="flash-holder flash-success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<?php if (Yii::app()->user->hasFlash('error')) : ?>
    <div class="alert alert-danger  alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php endif; ?>
<div class="alert alert-success" role="alert" style="display:none;">
</div>
<div class="alert alert-danger" role="alert" style="display:none;">
</div>
<div class="alert alert-warning" role="alert" style="display:none;"> </div>

<div id="previous_details"></div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="mt-0"><?php echo $model->isNewRecord ? 'Add' : 'Edit' ?> Entry</h4>
    </div>
    <div class="panel-body p-0">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'quotation-form',
            'enableClientValidation' => true,
            'clientOptions' => array('validateOnChange' => true),
            'enableAjaxValidation' => false,
        ));
        ?>
        <div class="row p-1">

            <div class="col-md-3">
                <div class="form-group">
                    <input type="hidden" name="Quotation[quotation_id]" id="Quotation_quotation_id" value="">
                    <input type="hidden" name="Quotation[total_amount]" id="Quotation_total_amount" value="">
                    <?php echo $form->labelEx($model, 'company_id'); ?>
                    <?php
                    $user = Users::model()->findByPk(Yii::app()->user->id);
                    $arrVal = explode(',', $user->company_id);
                    $newQuery = "";
                    foreach ($arrVal as $arr) {
                        if ($newQuery)
                            $newQuery .= ' OR';
                        $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
                    }
                    $companyInfo = Company::model()->findAll(array('condition' => $newQuery));
                    ?>
                    <?php
                    echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array('condition' => $newQuery)), 'id', 'name'), array('class' => 'form-control js-example-basic-single field_change invoice_add require', 'empty' => '-Select Company-', 'style' => 'width:100%'));
                    ?>
                    <?php echo $form->error($model, 'company_id'); ?>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'client_name'); ?>
                    <?php echo $form->textField($model, 'client_name', array('class' => 'form-control ')); ?>
                    <?php echo $form->error($model, 'client_name'); ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'address'); ?>
                    <?php echo $form->textField($model, 'address', array('class' => 'form-control ')); ?>
                    <?php echo $form->error($model, 'address'); ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'phone_no'); ?>
                    <?php echo $form->textField($model, 'phone_no', array('class' => 'form-control ', 'value' => $model->phone_no)); ?>
                    <?php echo $form->error($model, 'phone_no'); ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'email'); ?>
                    <?php echo $form->textField($model, 'email', array('class' => 'form-control ')); ?>
                    <?php echo $form->error($model, 'email'); ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>DATE : </label>
                    <input type="text" value="<?php echo ((isset($date) && $date != '') ? date("d-m-Y", strtotime($date)) : ''); ?>" id="datepicker" class="txtBox date inputs target form-control invoice_add require" name="date" placeholder="Please click to edit" readonly="true">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">

                    <?php echo $form->labelEx($model, 'location_id'); ?>
                    <?php
                    echo $form->dropDownList($model, 'location_id', CHtml::listData($location_data, 'id', 'name'), array('class' => 'form-control js-example-basic-single field_change invoice_add require', 'empty' => '-Select Location-', 'style' => 'width:100%'));
                    ?>
                    <?php echo $form->error($model, 'location_id'); ?>

                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <?php
                    echo $form->labelEx($model, 'invoice_no');
                    echo $form->textField($model, 'invoice_no', array('class' => 'form-control require', 'readonly' => true));
                    ?>
                    <?php echo $form->error($model, 'invoice_no'); ?>
                </div>
                <input type="hidden" value="<?php echo $model->revision_no ?>">
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'house_name'); ?>
                    <?php  echo $form->textField($model, 'house_name', array('class' => 'form-control'));
                    ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                <?php echo $form->labelEx($model, 'project_type',array('class'=>'d-block')); ?>
                <?php echo $form->radioButton($model, 'project_type', array('value' => '0')) . ' Residential'; ?>
                <?php echo $form->radioButton($model, 'project_type', array('value' => '1')) . ' Commercial'; ?>
                <?php echo $form->error($model, 'project_type'); ?>

                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                <label>Site Address </label>
                <?php  echo $form->textField($model, 'site_address', array('class' => 'form-control')); ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">

                    <?php echo $form->labelEx($model, 'sales_executive_id'); ?>
                    <?php
                    echo $form->dropDownList($model, 'sales_executive_id', CHtml::listData(Salesexecutive::model()->findAll(), 'id', 'name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Executive-', 'style' => 'width:100%'));
                    ?>
                    <?php echo $form->error($model, 'location_id'); ?>

                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">                    
                    <label>Ex. Phone</label>
                    <?php
                    $phone ="";
                    if($model->sales_executive_id !=""){
                        $salesExecutive = Salesexecutive::model()->findByPk($model->sales_executive_id);
                        $phone = $salesExecutive['phone'];
                    }
                     
                    ?>
                    <input type="text" class="ex_phone form-control" value="<?php echo $phone ?>" readonly />
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">                    
                    <label>Ex. Designation</label>
                    <?php
                    $designation ="";
                    if($model->sales_executive_id !=""){
                        $salesExecutive = Salesexecutive::model()->findByPk($model->sales_executive_id);
                        $designation = $salesExecutive['designation'];
                    }
                     
                    ?>
                    <input type="text" class="ex_designation form-control" value="<?php echo $designation ?>" readonly />
                </div>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div> 
<!-- Main form ends -->

<div class="alert alert-success  alert-dismissible alert-msg-box"  style="display:none;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <span class="alert-msg"></span>
</div>
<?php
if (isset($_GET['qid']) && $_GET['qid'] != "") {
    ?>

    <div class="clearfix border-bottom">
        <h4 class="pull-left">Item Details</h4>
        <button type="button" class="btn btn-primary btn-sm pull-right btn-preview ml-1" data-toggle="modal" data-target="#exampleModal" data-qtid="<?php echo $_GET['qid'] ?>" data-rev="<?php echo $_GET['rev'] ?>" data-status="<?php echo $_GET['status'] ?>" data-inter-rev="<?php echo (isset($_GET['selected_revision'])?$_GET['selected_revision']:"") ?>">
            Preview
        </button>
        
        <?php echo CHtml::link('SAVE AS PDF', array('SaveQuotation', 'qid'=>$_GET['qid'],'rev_no'=>'REV-'.$_GET['rev']),array("class"=>"btn btn-primary btn-sm pull-right")); ?>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header clearfix">
                    <h4 class="modal-title pull-left" id="exampleModalLabel">Quotation Details</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>

            </div>
        </div>
    </div>

    <br>


    <div id="item_details">

        <div>
            <div class="add_sec">                
                <div class="sections_block">
                    <div class="quotation_sec">
                        <!-- start main sec -->
                        <div class="panel" style="position:relative">
                            <div class="panel-heading">
                                <h5 class="bold">Main Section</h4>
                            </div>
                            <div class="panel-body">
                                <?php echo $this->renderPartial('_maincategorysection', array('model' => $main_sec_model,'main_wtype_model'=>$main_wtype_model,
                                'quotation_model'=>$model)) ?>            

                                <div class="sub_sec_block" style="display:none;"> 
                                <br>
                                    <div class="sub_section panel"> 
                                        <div class="panel-heading">
                                            <h5 class="bold">Sub Section</h5>
                                        </div>
                                        <br>
                                        
                                        <div id="items_holder" class="position-relative items_holder panel-body" style="padding-top:0px;padding-bottom:0px">
                                            <!-- <div class="panel-heading">
                                                <h5 class="bold">Sub Section</h5>
                                            </div> -->
                                            <div class="quotationmaindiv" id="items_list">
                                                <div class="add_new_sec ">                        
                                                    <div class="h-head">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label class="bold"></label>   
                                                                <input type="hidden" id="category_label_id" value="">                     
                                                                <?php
                                                                echo $form->textField($main_cat_model, 'category_label', array('class' => 'form-control require', 'id' => 'category_label', 'placeholder' => 'Sub Section'));
                                                                ?>
                                                                <div class="errorMessage"></div>
                                                            </div>                                    
                                                            <div class="col-md-2">
                                                                <?php
                                                                echo $form->dropDownList($main_cat_model, 'unit', CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name'), array('class' => 'form-control js-example-basic-single   require ', 'empty' => '-Select Unit-', 'name' => 'unit', 'style' => 'width:100%', 'id' => 'cat_unit')
                                                                );
                                                                ?>
                                                            </div>
                                                            <?php
                                                            $activeProjectTemplate = $this->getActiveTemplate();
                                                            if ($activeProjectTemplate == 'TYPE-5') {
                                                            ?>
                                                            <div class="col-md-2">
                                                                <?php
                                                                echo $form->textField($main_cat_model, 'hsn_code', array('class' => 'form-control', 'name' => 'hsn_code','placeholder'=>'HSN Code', 'style' => 'width:100%', 'id' => 'sec_hsn_code')
                                                                );
                                                                ?>
                                                            </div>
                                                            <?php } ?>
                                                            <div class="col-md-1">
                                                                <?php echo $form->textField($main_cat_model, 'quantity', array('class' => 'form-control allownumericdecimal quantity_sub', 'autocomplete' => 'off', 'placeholder' => 'qty', 'name' => 'quantity', 'id' => 'cat_quantity')); ?>
                                                                <?php echo $form->error($main_cat_model, 'quantity'); ?>
                                                            </div>
                                                            <?php 
                                                            $activeProjectTemplate = $this->getActiveTemplate();
                                                            if ($activeProjectTemplate != 'TYPE-5') {
                                                            ?>
                                                            <div class="col-md-1">
                                                                <?php echo $form->textField($main_cat_model, 'quantity_nos', array('class' => 'form-control allownumericdecimal ', 'autocomplete' => 'off', 'placeholder' => 'qty nos', 'name' => 'quantity_nos', 'id' => 'cat_quantity_nos')); ?>
                                                                <?php echo $form->error($main_cat_model, 'quantity_nos'); ?>
                                                            </div>
                                                            <?php } ?>
                                                            <div class="col-md-1">
                                                                <?php echo $form->textField($main_cat_model, 'mrp', array('class' => 'form-control allownumericdecimal ', 'autocomplete' => 'off', 'placeholder' => 'MRP', 'name' => 'mrp', 'id' => 'cat_mrp')); ?>
                                                                <?php echo $form->error($main_cat_model, 'mrp'); ?>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <?php echo $form->textField($main_cat_model, 'amount_after_discount', array('class' => 'form-control allownumericdecimal amountafter-discount', 'autocomplete' => 'off', 'name' => 'amount_after_discount', 'placeholder' => 'amt after disc', 'id' => 'cat_amount_after_discount')); ?>
                                                                <?php echo $form->error($main_cat_model, 'amount_after_discount'); ?>
                                                            </div>

                                                        </div> <!-- row-->
                                                        <br>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="" id="quotation_item">
                                            </div>


                                            <!-- <div class="add_wrktype toggle_btn1" title="Add Sub Section">        
                                                <i class="toggle-caret fa fa-plus"></i>
                                            </div> -->
                                            <div class="worktype_label_sec px-3 mt-2" style="display:none;">
                                                <div class=" panel panel-default">
                                                    <div class="panel-heading position-relative">
                                                        <div class="clearfix">
                                                            <div class="pull-left">
                                                                <h5>Add Sub Items</h5>
                                                            </div>


                                                        </div>

                                                    </div>
                                                    <div class="panel-body" style="background: #f8fbff;">
                                                        <?php
                                                        echo $this->renderPartial('_worktype_label_sec',
                                                                array('main_wtype_model' => $main_wtype_model, 'qid' => $qid, 'rev' => $rev, 'status' => $status))
                                                        ?>
                                                        <div class="clearfix">
                                                            <div class="pull-right">
                                                                <button class="btn btn-primary btn-sm add_more_wtype">Add more Sub Item</button>
                                                                <button class="btn btn-danger btn-sm remove_wtype hide">Remove</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="" id="quotation_item_edit">
                                            </div>

                                        </div>
                                    </div>
                                    <div class="clearfix">                                                
                                        <div class="pull-right">
                                            <button class="btn btn-primary btn-sm add_more_subsec">Add More Sub Section</button>
                                            <button class="btn btn-danger btn-sm remove_sub hide">Remove</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- end main sec -->
                        </div>

                    </div>


                </div>
                <div class="clearfix addmore_btns">
                    <div class="pull-right">
                        <button class="btn btn-primary btn-sm add_more_section">Add More Section</button>
                        <button class="btn btn-danger btn-sm remove_section hide">Remove</button>
                    </div>  
                </div> 
            </div>
        </div>

        <!-- sum sec starts -->

        <?php
        if ($status == 1) {
            if ((!empty($salesquotation_edit) or!empty($salesquotation_extra_edit))) {
                $this->renderPartial('_quotation_revisionedit_crd', array(
                    'model' => $main_sec_model,
                    'modelsales' => $salesquotation_edit,
                    'sales_extra' => $salesquotation_extra_edit,
                    'rev_no' => $rev,
                    'qid' => $qid,
                    'qmodel' => $model,
                    'main_cat_model'=>$main_cat_model,
                    'main_wtype_model'=>$main_wtype_model
                ));
            }
        }

        if (isset($_GET['qid'])) {
            $revision_no = 'REV-' . $rev;

            $sql = 'SELECT * FROM jp_quotation_revision '
                    . ' WHERE qid=' . $_GET['qid']
                    . ' AND revision_no="' . $revision_no . '"';
            $revision_model = Yii::app()->db->createCommand($sql)->queryRow();
            if (empty($revision_model) && $_GET['status'] == 1) {
                $revision_no = $_GET['selected_revision'];
                $sql = 'SELECT * FROM jp_quotation_revision '
                        . ' WHERE qid=' . $_GET['qid']
                        . ' AND revision_no="' . $_GET['selected_revision'] . '"';
                $revision_model = Yii::app()->db->createCommand($sql)->queryRow();
            }
            ?>

            <div class="card">
                <div class="card-body">
                    <div class="revision_calculation">
                        <br>
                        <div class='row px-3'>
                            <div class="col-md-2" >
                                <div class="form-group">
                                    <label>Tax Slab:</label>
                                    <?php
                                    $datas = TaxSlabs::model()->getAllDatas();
                                    ?>
                                    <select class="form-control" name="tax_slab" id="add_tax_slab">
                                        <option value="">Select one</option>
                                        <?php
                                        foreach ($datas as $value) {
                                            if ($value['set_default'] == 1) {
                                                echo ' <option data-id="' . $value['id'] . '" value="' . $value['tax_slab_value'] . '" selected>' . $value['tax_slab_value'] . '%</option>';
                                            } else {
                                                echo ' <option  data-id="' . $value['id'] . '" value="' . $value['tax_slab_value'] . '">' . $value['tax_slab_value'] . '%</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-1" >                       
                                <div class="form-group">
                                    <label>SGST% :</label>
                                    <input type ="text" value="<?php echo $revision_model['sgst_percent'] ?>" class="allownumericdecimal form-control calculation" id="sgst_percent" >                      
                                    <div id="sgst_amount" class="padding-box"><?php echo $revision_model['sgst_amount'] ?></div>                    
                                </div>
                            </div>		
                            <div class="col-md-1" >
                                <div class="form-group">
                                    <label>CGST% :</label>
                                    <input type ="text" value="<?php echo $revision_model['cgst_percent'] ?>" class="allownumericdecimal form-control calculation" id="cgst_percent" >                      
                                    <div id="cgst_amount" class="padding-box"><?php echo $revision_model['cgst_amount'] ?></div>                      
                                </div>
                            </div>
                            <div class="col-md-1" >
                                <div class="form-group">
                                    <label>IGST% :</label>
                                    <input type ="text" value="<?php echo $revision_model['igst_percent'] ?>" class="allownumericdecimal form-control calculation" id="igst_percent" >                      
                                    <div id="igst_amount" class="padding-box"><?php echo $revision_model['igst_amount'] ?></div>                      
                                </div>
                            </div>
                            <div class="col-md-1" >
                                <div class="form-group">
                                    <label>Discount% :</label>
                                    <input type ="text" value="<?php echo $revision_model['discount_precent'] ?>" class="allownumericdecimal form-control calculation" id="discount_precent" >                      
                                    <div id="discount_amount" class="padding-box"><?php echo $revision_model['discount_amount'] ?></div>                      
                                </div>
                            </div>
                            <div class="col-md-2" >
                                <label>Total After Discount</label>
                                <div class="form-group">
                                    <?php 
                                    $rev_status = NULL;
                                    if($_GET['status']==1){
                                        $intrRev = isset($_GET['selected_revision'])?($_GET['selected_revision'].$_GET['rev']):$_GET['rev'];
                                        $newRev = 'REV-' . $_GET['rev'];
                                        $revision_no = ("'$intrRev'" .','. "'$newRev'");
                                        $rev_status = 1;
                                    }
                                    
                                    $total_amount = Controller::getConcordQuotationSum($_GET['qid'], $revision_no,$rev_status); ?>
                                    <input type ="text" value="<?php echo Controller::money_format_inr(($total_amount - $revision_model['discount_amount']) , 2); ?>" data-val="<?php echo $total_amount ?>" class="allownumericdecimal form-control calculation" id="total_after_discount" readonly>  

                                </div>
                            </div>
                            <div class="col-md-2" >
                                <label>Total Tax Amount</label>
                                <div class="form-group">
                                    <input type ="text" value="" class="allownumericdecimal form-control calculation" id="total-tax" readonly>                
                                </div>
                            </div>

                            <div class="col-md-2" >
                                <label>Total Amount With Tax</label>
                                <div class="form-group">
                                    <input type ="text" value="" class="allownumericdecimal form-control calculation" id="maintotal_withtax" readonly>                

                                </div>
                            </div>                            
                        </div>
                        <div class="row px-3">
                            <div class="col-md-2" >
                                <?php
                                $amount_after_dis = "";
                                if(isset($_GET['selected_revision'])){
                                    $amount_after_dis = Yii::app()->db->createCommand("SELECT total_after_additionl_discount FROM `jp_quotation_revision` WHERE `qid` = ".$_GET['qid']." AND `revision_no` LIKE '".$_GET['selected_revision']."'")->queryScalar();
                                }
                                
                                ?>
                                <label>Amount after additional Discount</label>
                                <div class="form-group">
                                    <input type ="text" value="<?php echo !is_null($amount_after_dis)?$amount_after_dis:"" ?>" class="allownumericdecimal form-control calculation" id="total_after_additionl_discount">                

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?> 

        <!-- sum sec ends -->
        <?php
    } ?>
    <div class="text-center">
        <?php
       
        if(isset($_GET['status'])&& $_GET['status'] == 1){
            $inter_rev = $_GET['selected_revision'].$_GET['rev'];
            $sql = 'SELECT count(*) FROM `jp_sales_quotation` '
                    . ' WHERE master_id =' . $_GET['qid']
                    . ' AND revision_no="' . $inter_rev . '" AND `deleted_status` = 1';
                    
            $inter_rev_count = Yii::app()->db->createCOmmand($sql)->queryScalar();
    
            $disabled = '';
            if($inter_rev_count == 0){
                $disabled = 'disabled';
            }
            
            echo CHtml::button('Update',array('class'=>'btn btn-primary  updateRevision',
                            'data-rev'=>$inter_rev,'data-qid'=>$_GET['qid'],'disabled'=>$disabled));
        }
        
        ?>
    </div>
    <script>
        var count = 1;
        $(function () {
            $('p#add_field').click(function () {
                count += 1;
                $('#container').append(
                        '<div class="row addRow row-margin-append">' +
                        '<div class="col-md-1">' +
                        '<label for="Clients_Shutter_Work">Extra Work</label>                </div>' +
                        '<div class="col-md-2">' +
                        '<input class="form-control" type="file" name="img" id="img_' + count + '" ><option value=""></input>      ' +
                        '           </div>' +
                        '<div class="col-md-2">' +
                        '<input class="form-control" id="descrition_' + count + '" name="description[]' + '" type="text" placeholder="description"></input>      ' +
                        '           </div>' +
                        '<div class="col-md-1">' +
                        '<select class="form-control" id="sq_feet_' + count + '" name="sq_feet[]' + '" type="text"><option value="">Sq Ft.</option></select>      ' +
                        '          </div>' +
                        '<div class="col-md-1">' +
                        '<input class="form-control" id="qty_' + count + '" name="qty[]' + '" type="text" placeholder="qty"></input>      ' +
                        '           </div>' +
                        '<div class="col-md-1">' +
                        '<input class="form-control" id="qtynos_' + count + '" name="qtynos[]' + '" type="text" placeholder="qty nos"></input>      ' +
                        '           </div>' +
                        '<div class="col-md-2">' +
                        '<input class="form-control" id="mrp_' + count + '" name="mrp[]' + '" type="text" placeholder="MRP"></input>      ' +
                        '           </div>' +
                        '<div class="col-md-2">' +
                        '<input class="form-control" id="amt_dis_' + count + '" name="amt_dis[]' + '" type="text" placeholder="amt after disc"></input>      ' +
                        '           </div>' +
                        '           </div>' +
                        '<br/>');
            });
        });

        $("#quotation-form :input").change(function () {
            var allRequired = true;
            var $inputs = $('#quotation-form :input').filter('.require');
            var values = {};
            $inputs.each(function () {
                if ($(this).val() == '') {
                    allRequired = false;
                }
                values[this.name] = $(this).val();
            });
            if (allRequired) {
                $("#quotation-form").submit();
            }
        });
        $("#category_label").change(function () {
            $category_label = $("#category_label").val();
            $("#cat_name").val($category_label);
        });

        $("#SalesQuotation_id").change(function () {
            $category_label = $("#category_label").val();
            $("#cat_name").val($category_label);
        });


        $(".company_id").select2();
        $(function () {
            $("#datepicker").datepicker({
                dateFormat: 'dd-mm-yy',
                maxDate: '0'
            }).datepicker("setDate", new Date());
        });

        function saveRevisionData() {
            var revision_no = "<?php echo isset($_GET['rev']) ? $_GET['rev'] : '' ?>";
            var qid = "<?php echo isset($_GET['qid']) ? $_GET['qid'] : '' ?>";

            var tax_slab = $('#add_tax_slab').val();
            var sgst_percent = $('#sgst_percent').val();
            var sgst_amount = $('#sgst_amount').text();


            var cgst_percent = $('#cgst_percent').val();
            var cgst_amount = $('#cgst_amount').text();

            var igst_percent = $('#igst_percent').val();
            var igst_amount = $('#igst_amount').text();
            var status = "<?php echo isset($_GET['status']) ? $_GET['status'] : "" ?>";

            var discount_percent = $('#discount_precent').val();
            var discount_amount = $('#discount_amount').text();

            var total_after_additionl_discount = $('#total_after_additionl_discount').val();


            $.ajax({
                url: "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/saveRevisionData') ?>",
                data: {
                    qid: qid,
                    rev: revision_no,
                    tax_slab: tax_slab,
                    sgst_percent: sgst_percent,
                    sgst_amount: sgst_amount,
                    cgst_percent: cgst_percent,
                    cgst_amount: cgst_amount,
                    igst_percent: igst_percent,
                    igst_amount: igst_amount,
                    discount_percent:discount_percent,
                    discount_amount:discount_amount,
                    total_after_additionl_discount:total_after_additionl_discount
                },
                type: "POST",
                success: function (data) {
                    console.log('Amount saved');
                }
            })
        }

        $('#sgst_percent').change(function () {

            var sgst_percent = $('#sgst_percent').val();
            var total_amount = $('#total_after_discount').attr('data-val');
            var totaltax = $('#total-tax').val();
            var cgst_amount = $('#cgst_amount').text();
            var igst_amount = $('#igst_amount').text();
            var discount_amount = isNaN($('#discount_amount').text()) ? 0.00 : $('#discount_amount').text();
            if (total_amount > 0) {
                var sgst_amount = (total_amount * sgst_percent) / 100;
                $('#sgst_amount').text(sgst_amount.toFixed(2));
                $('#sgst_amount_hidden').val(sgst_amount.toFixed(2));
                var total_tax = parseFloat(cgst_amount) + parseFloat(sgst_amount) + parseFloat(igst_amount);
                $('#total-tax').val(total_tax.toFixed(2));
                
                var total_with_discount = parseFloat(total_amount) ;
                
                $('#total_after_discount').val(total_with_discount.toFixed(2));
                var total_with_tax = parseFloat(total_with_discount) + parseFloat(total_tax) - parseFloat(discount_amount);
                $('#maintotal_withtax').val(total_with_tax.toFixed(2));
                if (status == 1) {
                    $(".btn_update1").trigger("click");
                }
                saveRevisionData();
            } else {
                $('#sgst_amount').text('0.00');
            }
        });

        $('#cgst_percent').change(function () {
            var cgst_percent = $('#cgst_percent').val();
            var total_amount = $('#total_after_discount').attr('data-val');
            var totaltax = $('#total-tax').val();
            var sgst_amount = $('#sgst_amount').text();
            var igst_amount = $('#igst_amount').text();
            var discount_amount = isNaN($('#discount_amount').text()) ? 0.00 : $('#discount_amount').text();
            if (total_amount > 0) {

                var cgst_amount = (total_amount * cgst_percent) / 100;
                $('#cgst_amount_hidden').val(cgst_amount.toFixed(2));
                $('#cgst_amount').text(cgst_amount.toFixed(2));
                var total_tax = parseFloat(cgst_amount) + parseFloat(sgst_amount) + parseFloat(igst_amount);
                $('#total-tax').val(total_tax.toFixed(2));
                
                var total_with_discount = parseFloat(total_amount);
               $('#total_after_discount').val(total_with_discount.toFixed(2));
                var total_with_tax = parseFloat(total_with_discount) + parseFloat(total_tax) - parseFloat(discount_amount);
                $('#maintotal_withtax').val(total_with_tax.toFixed(2));
                if (status == 1) {
                    $(".btn_update1").trigger("click");
                }
                saveRevisionData();

            } else {
                $('#cgst_amount').text('0.00');

            }
        });

        $('#igst_percent').change(function () {
            var igst_percent = $('#igst_percent').val();
            var total_amount = $('#total_after_discount').attr('data-val');
            var totaltax = $('#total-tax').val();
            var sgst_amount = $('#sgst_amount').text();
            var cgst_amount = $('#cgst_amount').text();
            var discount_amount = isNaN($('#discount_amount').text()) ? 0.00 : $('#discount_amount').text();

            if (total_amount > 0) {
                var igst_amount = (total_amount * igst_percent) / 100;
                $('#igst_amount').text(igst_amount.toFixed(2));
                $('#igst_amount_hidden').val(igst_amount.toFixed(2));
                var total_tax = parseFloat(cgst_amount) + parseFloat(sgst_amount) + parseFloat(igst_amount);
                $('#total-tax').val(total_tax.toFixed(2));
                var total_with_discount = parseFloat(total_amount);
                $('#total_after_discount').val(total_with_discount.toFixed(2));
                var total_with_tax = parseFloat(total_with_discount) + parseFloat(total_tax)- parseFloat(discount_amount);
                $('#maintotal_withtax').val(total_with_tax.toFixed(2));
                if (status == 1) {
                    $(".btn_update1").trigger("click");
                }
                saveRevisionData();
            } else {
                $('#igst_amount').text('0.00');
            }
        });

        $('#discount_precent').change(function () {
            var discount_precent = $(this).val();
            var total_after_discount = $('#total_after_discount').attr('data-val');
            var totaltax = $('#total-tax').val();
            var total_amount_after_gst = parseFloat(total_after_discount) +  parseFloat(totaltax);
            
            if (total_amount_after_gst > 0) {
                var discount_amount = (total_amount_after_gst * discount_precent) / 100;
                $('#discount_amount').text(discount_amount.toFixed(2));                                
                var total_with_discount = parseFloat(total_amount_after_gst) - parseFloat(discount_amount);
                $('#maintotal_withtax').val(total_with_discount.toFixed(2));
                
                if (status == 1) {
                    $(".btn_update1").trigger("click");
                }
                saveRevisionData();
            } else {
                $('#discount_amount').text('0.00');
            }
        });

        $(function () {
            $("#sgst_percent").change();
            $("#cgst_percent").change();
            $("#igst_percent").change();
            $('#discount_precent').change();
            $("#total_after_additionl_discount").change();
        });

        $("#total_after_additionl_discount").change(function(){
            saveRevisionData();
        })

        $(document).on("click",".delete_subitem",function(){
            var elem = $(this);
            var subitem_id = $(this).parent(".worktype_label_row").find("#worktype_label_id").val();
            var revision_no = "<?php echo isset($_GET['rev']) ? $_GET['rev'] : '' ?>";
            var qid = "<?php echo isset($_GET['qid']) ? $_GET['qid'] : '' ?>";
            if(subitem_id!=""){
                $.ajax({
                url: "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/deleteSubItem') ?>",
                data: {
                    qid: qid,
                    rev: revision_no,
                    subitem_id: subitem_id,                    
                },
                type: "POST",
                dataType:"json",
                success: function (data) {
                    console.log(data);
                    if(data.response == 1){
                        elem.parent(".worktype_label_row").find("input").val("");
                        elem.parent(".worktype_label_row").find("#quotation_item").html("");
                        elem.parent(".worktype_label_row").find(".QuotationGenWorktype_master_cat_id").val('');
                        $("#sgst_percent").change();
                        $("#cgst_percent").change();
                        $("#igst_percent").change(); 
                        $('#discount_precent').change();
                        $(document).find("#total_after_discount").val(data.total_amount)
                        $(document).find("#total_after_discount").attr('data-val',data.data_val);
                    }else{
                        console.log("error occured");
                    }
                }
            })
            }
        })
    </script> 



