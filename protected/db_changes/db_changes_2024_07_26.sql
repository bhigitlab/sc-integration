ALTER TABLE jp_equipments_items ADD pms_equipment_id INT(11) NULL AFTER unit, ADD pms_template_equipment_id INT(11) NULL AFTER pms_equipment_id, ADD pms_unit_id INT(11) NULL AFTER pms_template_equipment_id;
ALTER TABLE jp_equipments_entries ADD pms_project_id INT(11) NULL AFTER status;

ALTER TABLE jp_equipments_entries ADD pms_wpr_id INT(11) NULL AFTER status;

ALTER TABLE jp_equipments_items CHANGE equipments equipments INT(11) NULL;
ALTER TABLE jp_equipments_items CHANGE unit unit VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;

ALTER TABLE jp_equipments_entries ADD request_from INT(11) NOT NULL DEFAULT '1' COMMENT '1=> accounts,2=>PMS' AFTER pms_project_id;