<?php

class PurchaseController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        $accessArr = array();
        $accessauthArr = array();
        $controller = Yii::app()->controller->id;
        if (isset(Yii::app()->user->menuall)) {
            if (array_key_exists($controller, Yii::app()->user->menuall)) {
                $accessArr = Yii::app()->user->menuall[$controller];
            }
        }

        if (isset(Yii::app()->user->menuauth)) {
            if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                $accessauthArr = Yii::app()->user->menuauth[$controller];
            }
        }
        $access_privlg = count($accessauthArr);
        return array(
            array(
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => $accessArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('gmail', 'ratedecline', 'Viewremarks', 'addremarks', 'Dynamicexpensehead', 'Dynamicvendor', 'approve', 'additionalCharge', 'deleteadditionalCharge', 'Dynamiccompany', 'loadPoItemsModal', 'getProjectByMrId', 'RejectEstimation'),
                'users' => array('*'),
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionGmail()
    {

        $mail = new JPhpMailer(); // create a new object
        $mail->IsSMTP(); // enable SMTP
        $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth = true; // authentication enabled
        $mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host = "smtp.gmail.com";
        $mail->Port = 587; // or 587
        $mail->IsHTML(true);
        $mail->Username = "clarinstech@gmail.com";
        $mail->Password = "clarins@tech05";
        $mail->SetFrom("clarinstech@gmail.com");
        $mail->Subject = "Test";
        $mail->Body = "hello";
        $mail->AddAddress("bala@bluehorizoninfotech.com");

        if (!$mail->Send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo "Message has been sent";
        }

        die;

        $mail = new JPhpMailer();
        $mail->IsSMTP();
        $mail->Mailer = "smtp";
        $mail->SMTPDebug = 1;
        $mail->SMTPAuth = TRUE;
        $mail->SMTPSecure = "tls";
        $mail->Port = 587;
        $mail->Host = "smtp.gmail.com";
        $mail->Username = "clarinstech@gmail.com";
        $mail->Password = "clarins@tech05";

        $mail->IsHTML(true);
        $mail->AddAddress("bala@bluehorizoninfotech.com", "recipient-name");
        $mail->SetFrom('clarinstech@gmail.com', "from-name");
        $mail->Subject = "Test is Test Email sent via Gmail SMTP Server using PHP Mailer";
        $content = "<b>This is a Test Email sent via Gmail SMTP Server using PHP mailer class.</b>";

        $mail->MsgHTML($content);
        if (!$mail->Send()) {
            echo "Error while sending Email.";
            var_dump($mail);
        } else {
            echo "Email sent successfully";
        }
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Purchase;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Purchase'])) {
            $model->attributes = $_POST['Purchase'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->p_id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Purchase'])) {
            $model->attributes = $_POST['Purchase'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->p_id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Purchase');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '')
    {
        if (!is_array($user_tree_array))
            $user_tree_array = array();
        $data = array();
        $final = array();
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        $spec_sql = "SELECT id, specification, brand_id, cat_id,sub_cat_id "
            . " FROM {$tblpx}specification "
            . " WHERE specification_type = 'O' AND (" . $newQuery . ") AND spec_status = '1'";
        $specification = Yii::app()->db->createCommand($spec_sql)->queryAll();
        foreach ($specification as $key => $value) {
            $brand = '';
            if ($value['brand_id'] != NULL) {
                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $value['brand_id'] . "")->queryRow();
                $brand = '-' . ucwords($brand_details['brand_name']);
            }
            if ($value['cat_id'] != "") {
                $result = $this->actionGetParent($value['cat_id']);
                $final[$key]['data'] = ucwords($result['category_name']) . $brand . '-' . ucwords($value['specification']);
                $final[$key]['id'] = $value['id'];
                $category = ucwords($result['category_name']);
            }
            if ($value['sub_cat_id'] != "") {
                $result = $this->actionGetSubcategory($value['sub_cat_id']);
                $final[$key]['data'] = ucwords($category) . '-' . ucwords($result['sub_category_name']) . $brand . '-' . ucwords($value['specification']);
                $final[$key]['id'] = $value['id'];
            }
        }
        return $final;
    }

    public function actionGetParent($id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $category = Yii::app()->db->createCommand("SELECT parent_id, id , category_name FROM {$tblpx}purchase_category WHERE id=" . $id . "")->queryRow();
        return $category;
    }
    public function actionGetSubcategory($id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $subcategory = Yii::app()->db->createCommand("SELECT  id , sub_category_name FROM {$tblpx}purchase_sub_category WHERE id=" . $id . "")->queryRow();
        return $subcategory;
    }

    /** Add Purchase  * */
    public function actionAddpurchase()
    {
        //$this->layout = '//layouts/blank';
        $model = new Purchase();
        $item_model = new PurchaseItems();
        $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        $newQuery1 = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            if ($newQuery1)
                $newQuery1 .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
            $newQuery1 .= " FIND_IN_SET('" . $arr . "', p.company_id)";
        }
        if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) {
            $project = Yii::app()->db->createCommand("SELECT pid, name  FROM {$tblpx}projects WHERE " . $newQuery . " ORDER BY name")->queryAll();
        } else {
            $project = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects p JOIN {$tblpx}project_assign pa ON p.pid = pa.projectid WHERE " . $newQuery1 . " AND pa.userid = '" . Yii::app()->user->id . "'")->queryAll();
        }
        $vendor = Yii::app()->db->createCommand("SELECT DISTINCT vendor_id, name FROM {$tblpx}vendors WHERE " . $newQuery . " ORDER BY name")->queryAll();
        $list_description = Yii::app()->db->createCommand("SELECT DISTINCT description FROM {$tblpx}purchase_items ORDER BY description")->queryAll();
        $this->render('purchase', array(
            'model' => $model,
            'project' => $project,
            'vendor' => $vendor,
            'list_description' => $list_description,
            'specification' => $specification,
        ));
    }



    /**  update purchase **/


    public function actionUpdatepurchase($pid)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $model = Purchase::model()->find(array("condition" => "p_id = '$pid'"));
        if (empty($model)) {
            $this->redirect(array('purchase/admin'));
        }
        $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
        $item_model = PurchaseItems::model()->findAll(array("condition" => "purchase_id = '$pid'"));
        $total_discount_value = Yii::app()->db->createCommand("SELECT SUM(discount_amount) as discount_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $pid . "")->queryRow();
        $total_tax_amount = Yii::app()->db->createCommand("SELECT SUM(tax_amount) as tax_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $pid . "")->queryRow();

        $additional_charge = Yii::app()->db->createCommand("SELECT SUM(amount)  FROM `jp_additional_po_charge` WHERE `purchase_id` = " . $pid)->queryScalar();

        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        $newQuery1 = "";
        $newQuery2 = "";
        $newQuery3 = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            if ($newQuery1)
                $newQuery1 .= ' OR';
            if ($newQuery1)
                $newQuery2 .= ' OR';
            if ($newQuery1)
                $newQuery3 .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
            $newQuery1 .= " FIND_IN_SET('" . $arr . "', p.company_id)";
            $newQuery2 .= " FIND_IN_SET('" . $arr . "', {$tblpx}expense_type.company_id)";
            $newQuery3 .= " FIND_IN_SET('" . $arr . "', v.company_id)";
        }
        if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) {
            $project = Yii::app()->db->createCommand("SELECT pid, name  FROM {$tblpx}projects WHERE FIND_IN_SET(" . $model->company_id . ",company_id) ORDER BY name")->queryAll();
        } else {
            $project = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects p JOIN {$tblpx}project_assign pa ON p.pid = pa.projectid WHERE  FIND_IN_SET(" . $model->company_id . ",company_id) AND pa.userid = '" . Yii::app()->user->id . "' ORDER BY p.name")->queryAll();
        }
        $list_description = Yii::app()->db->createCommand("SELECT DISTINCT description FROM {$tblpx}purchase_items ORDER BY description")->queryAll();
        $vendor = array();
        $expense_head = array();
        if (isset($model->project_id)) {
            $expense_head = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}project_exptype LEFT JOIN {$tblpx}expense_type ON {$tblpx}project_exptype.type_id={$tblpx}expense_type.type_id WHERE {$tblpx}project_exptype.project_id=" . $model->project_id)->queryAll();
        }
        if (isset($model->expensehead_id)) {
            $vendor = Yii::app()->db->createCommand("SELECT v.vendor_id as vendorid, v.name as vendorname FROM " . $tblpx . "vendors v LEFT JOIN " . $tblpx . "vendor_exptype vet ON v.vendor_id = vet.vendor_id WHERE vet.type_id = " . $model->expensehead_id . " AND ($newQuery3)")->queryAll();
        }


        $this->render('purchase_update', array(
            'model' => $model,
            'item_model' => $item_model,
            'pid' => $pid,
            'vendor' => $vendor,
            'project' => $project,
            'list_description' => $list_description,
            'specification' => $specification,
            'expense_head' => $expense_head,
            'total_discount_value' => $total_discount_value,
            'total_tax_amount' => $total_tax_amount,
            'additional_charge' => $additional_charge
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    { //mod by rajisha - loading time issue action changed to adminold and new action below in actionadmin
        $model = new Purchase('search');
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        $datefrom = "";
        $dateto = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        $criteria = new CDbCriteria;
        $criteria->select = 'DISTINCT pid,name';
        if (Yii::app()->user->role == 1) {
            $criteria->condition = $newQuery;
        } else {
            $criteria->condition = $newQuery . ' AND pid IN(SELECT projectid FROM jp_project_assign WHERE userid = ' . Yii::app()->user->id . ')';
        }
        $criteria->order = 'name ASC';
        $project = Projects::model()->findAll($criteria);

        $vendor = Yii::app()->db->createCommand("SELECT DISTINCT vendor_id,name FROM {$tblpx}vendors WHERE (" . $newQuery . ") ORDER BY name")->queryAll();
        $expense = Yii::app()->db->createCommand("SELECT DISTINCT type_id,type_name FROM {$tblpx}expense_type WHERE (" . $newQuery . ") ORDER BY type_name")->queryAll();
        if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) {
            $purchase_no = Yii::app()->db->createCommand("SELECT DISTINCT purchase_no,purchase_date FROM {$tblpx}purchase WHERE (" . $newQuery . ") AND purchase_no IS NOT NULL ORDER BY p_id")->queryAll();
        } else {
            $purchase_no = Yii::app()->db->createCommand("SELECT DISTINCT purchase_no,purchase_date FROM {$tblpx}purchase WHERE (" . $newQuery . ") AND purchase_no IS NOT NULL AND project_id IN(SELECT projectid FROM {$tblpx}project_assign WHERE userid = " . Yii::app()->user->id . ") ORDER BY p_id")->queryAll();
        }
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['date_from']) && isset($_GET['date_to']) && $_GET['date_to'] != '' && $_GET['date_from'] != '') {
            $datefrom = $_GET['date_from'];
            $dateto = $_GET['date_to'];
        } else {
            $dateto = date("Y-m-d");
            $datefrom = date("Y-m-d", strtotime("-6 months"));
        }
        if (isset($_GET['project_id'])) {
            $model->project_id = $_GET['project_id'];
            $model->vendor_id = $_GET['vendor_id'];
            $model->expensehead_id = $_GET['expensehead_id'];
            $model->purchase_no = $_GET['purchase_no'];
            $model->purchase_status = $_GET['purchase_status'];
            $model->purchase_billing_status = $_GET['purchase_billing_status'];
            $model->date_from = $datefrom;
            $model->date_to = $dateto;
            $model->company_id = $_GET['company_id'];
        } else {
            $model->date_from = $datefrom;
            $model->date_to = $dateto;
        }


        $projectid = '';
        $vendorid = '';
        $expenseheadid = '';
        $purchase_no1 = '';
        $purchase_status = '';
        $purchase_billing_status = '';
        $company_id = '';
        if (isset($_GET['project_id'])) {
            $projectid = $_GET['project_id'];
        }
        if (isset($_GET['vendor_id'])) {
            $vendorid = $_GET['vendor_id'];
        }
        if (isset($_GET['expensehead_id'])) {
            $expenseheadid = $_GET['expensehead_id'];
        }
        if (isset($_GET['purchase_no'])) {
            $purchase_no1 = $_GET['purchase_no'];
        }
        if (isset($_GET['purchase_status'])) {
            $purchase_status = $_GET['purchase_status'];
        }
        if (isset($_GET['purchase_billing_status'])) {
            $purchase_billing_status = $_GET['purchase_billing_status'];
        }
        if (isset($_GET['company_id'])) {
            $company_id = $_GET['company_id'];
        }
        $getTotalbillamount = 0;
        $getTotalbillamount = $this->getTotalbillamount($datefrom, $dateto, $projectid, $vendorid, $expenseheadid, $purchase_no1, $purchase_status, $purchase_billing_status, $company_id);
        // $total_blanceamount = $this->getTotalbalanceammount($datefrom, $dateto, $projectid, $vendorid, $expenseheadid, $purchase_no1, $purchase_status, $purchase_billing_status, $company_id);

        $this->render('newlist', array(
            'model' => $model,
            'dataProvider' => $model->search(),
            'project' => $project,
            'vendor' => $vendor,
            'expense' => $expense,
            'purchase_no' => $purchase_no,
            'total_billedamount' => $getTotalbillamount,
            //  'total_blanceamount' => $total_blanceamount
        ));
    }

    /**  view purchase **/

    public function actionViewpurchase($pid)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $current_item='';
        $tot_tax ='';
        $model = Purchase::model()->find(array("condition" => "p_id='$pid'"));
        $item_model = PurchaseItems::model()->findAll(array("condition" => "purchase_id = '$pid'"));
        $purchase_items = $this->setPurchaseItems($item_model);

        foreach ($purchase_items as $category) {
            foreach ($category as $key => $item) {
                if (is_array($item) && isset($item['tot_tax'])) {
                    $current_item = $item;
                    $tot_tax = $item['tot_tax'];
                } else {
                    $tot_tax = 0;
                }
            }
        }

        $admin_approval_data = Editrequest::model()->find(['condition' => 'editrequest_table ="' . $tblpx . 'purchase" AND parent_id="' . $pid . '" AND editrequest_status = 0']);
        if (!empty($model)) {
            $project = Projects::model()->findByPK($model->project_id);
            $vendor = Vendors::model()->findByPK($model->vendor_id);
            $expense_head = ExpenseType::model()->findByPK($model->expensehead_id);

            $arrVal = explode(',', $project->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
            }

            $typelist = Company::model()->findAll(array('condition' => $newQuery));
            $settings = GeneralSettings::model()->findByPk(1);
            $selectedtemplate = ProjectTemplate::model()->find(array("condition" => "status='1'"));

            $render_datas = array(
                'model' => $model,
                'item' => $current_item,
                'tax_amount' => $tot_tax,
                'itemmodel' => $purchase_items,
                'project' => $project,
                'vendor' => $vendor,
                'expense_head' => $expense_head,
                'typelist' => $typelist,
                'admin_approval_data' => $admin_approval_data,
                'settings' => $settings,
                'pdf_data' => $selectedtemplate->template_format_purchase
            );
            if (isset($_GET['exportpdf'])) {
                $this->logo = $this->realpath_logo;

                $file = 'purchaseview';
                if ($selectedtemplate->template_format_purchase != '') {
                    $file = 'dynamic_purchaseview';
                    $activeProjectTemplate = $this->getActiveTemplateIDWithCompany($model->company_id);
                    if ($activeProjectTemplate == '') {
                        Yii::app()->user->setFlash('error', 'No active PDF layout for this company!');
                        $this->redirect(array("purchase/viewpurchase", "pid" => $pid));
                    }
                }
                $pdfdata = $this->renderPartial($file, $render_datas, true);
                $filename = 'Purchase-Order-No-' . $model->purchase_no . '-' . str_replace(" ", "_", $project->name) . '.pdf';

                $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');

                // Add custom header and footer if available
                $template = $this->getTemplate($selectedtemplate);
                $mPDF1->SetHTMLHeader($template['header']);
                $mPDF1->SetHTMLFooter($template['footer']);

                // Set page margins based on the template
                if ($selectedtemplate == 'template1') {
                    $mPDF1->AddPage('', '', '', '', '', 0, 0, 50, 45, 10, 0);
                } else {
                    $mPDF1->AddPage('', '', '', '', '', 0, 0, 40, 45, 10, 0);
                }

                $mPDF1->WriteHTML($pdfdata);
                $mPDF1->Output($filename, 'D');
            } else {
                $this->render('purchaseview', $render_datas);
            }
        } else {
            $this->redirect(array('purchase/admin'));
        }
    }
    public function replaceItemlistWithCondition($extractedContent, $replacements)
    {
        $grouppattern = '/\[\((.*?)\)\]/';

        preg_match_all($grouppattern, $extractedContent, $groupmatches);

        foreach ($groupmatches[0] as $match) {
            //  $replaceString = $match;
            $input = str_replace(array("[(", ")]"), '', $match);

            // Check if "length," "width," and "size" are all present
            $variables = preg_match_all('/\{([^}]+)\}/', $match, $matches);
            $requiredVariables = ["length", "width"];

            // Check if any required variable is missing
            // if (count(array_intersect($requiredVariables, $matches[0])) == count($requiredVariables)) {
            $i = 0;
            foreach ($matches[0] as $variable) {
                if (!empty($replacements[$variable])) {
                    $i++;
                    $replaceString = str_replace($variable, $replacements[$variable], $input);

                } else {
                    // If any variable is missing a value, replace it with an empty string
                    $replaceString = str_replace($variable, '', $match);
                }
            }
            if (count($matches[0]) != $i) {
                // Any required variable is missing, replace with an empty string
                $replaceString = '';
            }

            $extractedContent = str_replace($match, $replaceString, $extractedContent);

            //$extractedContent = preg_replace('/\[|\]|\(|\)|\s+/', '', $extractedContent);



        }//echo $extractedContent;die;

        return $extractedContent;
    }

    public function setPurchaseItems($items)
    {
        $data = array();
        foreach ($items as $item) {
            if (!empty($item['category_id']) && $item['category_id'] != 0) {
                $category_model = Specification::model()->findByPk($item['category_id']);
                if ($category_model['brand_id'] != NULL) {
                    $brand_details = Brand::model()->findByPk($category_model['brand_id']);
                    $brand = $brand_details['brand_name'];
                } else {
                    $brand = '';
                }
                $tot_taxamount = $item['cgst_amount'] + $item['igst_amount'] + $item['sgst_amount'];
                $parent_category = PurchaseCategory::model()->findByPk($category_model['cat_id']);
                $specification = $brand . ' - ' . $category_model['specification'];
                $dat = array('specification' => $specification, 'quantity' => $item['quantity'], 'unit' => $item['unit'], 'hsn_code' => $item['hsn_code'], 'rate' => $item['rate'], 'amount' => $item['amount'], 'tax_slab' => $item['tax_slab'], 'sgstp' => $item['sgst_percentage'], 'sgst' => $item['sgst_amount'], 'cgstp' => $item['cgst_percentage'], 'cgst' => $item['cgst_amount'], 'igstp' => $item['igst_percentage'], 'igst' => $item['igst_amount'], 'discp' => $item['discount_percentage'], 'disc' => $item['discount_amount'], 'tot_tax' => $tot_taxamount);
                if (!empty($category_model['cat_id'])) {
                    if (!array_key_exists($category_model['cat_id'], $data)) {
                        $data[$category_model['cat_id']]['category_title'] = $parent_category['category_name'];
                        $data[$category_model['cat_id']][] = $dat;
                    } else {
                        array_push($data[$category_model['cat_id']], $dat);
                    }
                } else {
                    if (!array_key_exists($category_model['id'], $data)) {
                        $data[$category_model['id']]['category_title'] = $parent_category['category_name'];
                        $data[$category_model['id']][] = $dat;
                    } else {
                        array_push($data[$category_model['id']], $dat);
                    }
                }
            }
        }
        return $data;
    }

    /** Export pdf  **/

    public function actionExportpdf()
    {

        //echo 'hai';exit;
        $project_data = Projects::model()->findByPK($_POST['project']);
        $invendor_data = Vendors::model()->findByPK($_POST['vendor']);
        $expense_data = ExpenseType::model()->findByPK($_POST['expensehead']);
        $company_data = Company::model()->findByPK(Yii::app()->user->company_id);
        $date = date('Y-m-d', strtotime($_POST['date']));
        $purchaseno = $_POST['purchaseno'];
        $purchase_data = Purchase::model()->findByAttributes(array('purchase_no' => $purchaseno));
        $project = $project_data['name'];
        $vendor = $invendor_data['name'];
        $expense_head = $expense_data['type_name'];
        $subtot = $_POST['subtot'];
        $grand = $_POST['grand'];
        $company_name = $company_data['name'];
        if (isset($_POST['sl_No'])) {
            $sl_No = $_POST['sl_No'];
        } else {
            $sl_No = array();
        }
        if (isset($_POST['description'])) {
            $description = $_POST['description'];
        } else {
            $description = array();
        }
        if (isset($_POST['quantity'])) {
            $quantity = $_POST['quantity'];
        } else {
            $quantity = array();
        }
        if (isset($_POST['unit'])) {
            $unit = $_POST['unit'];
        } else {
            $unit = array();
        }
        if (isset($_POST['hsn_code'])) {
            $hsn_code = $_POST['hsn_code'];
        } else {
            $hsn_code = array();
        }
        if (isset($_POST['rate'])) {
            $rate = $_POST['rate'];
        } else {
            $rate = array();
        }
        if (isset($_POST['amount'])) {
            $amount = $_POST['amount'];
        } else {
            $amount = array();
        }
        if (isset($_POST['txtCompany'])) {
            $companyName = $_POST['txtCompany'];
        } else {
            $companyName = array();
        }
        if (isset($_POST['txtCompanyGst'])) {
            $companyGst = $_POST['txtCompanyGst'];
        } else {
            $companyGst = array();
        }
        if (isset($_POST["txtPurchaseCompany"])) {
            $poCompanyId = $_POST["txtPurchaseCompany"];
        } else {
            $poCompanyId = array();
        }
        $specification = array();
        if (isset($_POST['purchaseview'])) {
            $specification = $description;
        } else {
            $tblpx = Yii::app()->db->tablePrefix;
            $specification = array();
            for ($i = 0; $i < sizeof($description); $i++) {
                if ($description[$i] == '') {
                    $specification[] = $_POST['remark'][$i];
                } else {
                    $sql = "SELECT specification FROM {$tblpx}purchase_category WHERE id=" . $description[$i];
                    $data = Yii::app()->db->createCommand($sql)->queryRow();
                    $specification[] = $data['specification'];
                }
            }
        }
        $company = Yii::app()->user->company_id;
        $companyInfo = Company::model()->findByPK(Yii::app()->user->company_id);

        $mPDF1 = Yii::app()->ePdf->mPDF();

        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');

        $mPDF1->WriteHTML($stylesheet, 1);
        $render_data = array(
            'specification' => array_filter($specification),
            'date' => $date,
            'purchaseno' => $purchaseno,
            'purchase_data' => $purchase_data,
            'company' => $companyName,
            'gstnum' => $companyGst,
            'project' => $project,
            'vendor' => $vendor,
            'subtot' => $subtot,
            'grand' => $grand,
            'sl_No' => array_filter($sl_No),
            'specification' => array_filter($specification),
            'quantity' => array_filter($quantity),
            'unit' => array_filter($unit),
            'rate' => array_filter($rate),
            'amount' => array_filter($amount),
            'expense_head' => $expense_head,
            'company_name' => $companyName,
            'companyid' => Yii::app()->user->company_id,
            'poCompanyId' => $poCompanyId,
            'hsn_code' => $hsn_code
        );

        $mPDF1->WriteHTML($this->renderPartial('purchasepdfview', $render_data, true));
        $mPDF1->Output('Purchase.pdf', 'D');
    }

    /** Export excel  **/



    public function actionexportexcel()
    {

        $project_data = Projects::model()->findByPK($_POST['project']);
        $invendor_data = Vendors::model()->findByPK($_POST['vendor']);
        $expense_data = ExpenseType::model()->findByPK($_POST['expensehead']);
        $company_data = Company::model()->findByPK(Yii::app()->user->company_id);
        $date = date('d-m-Y', strtotime($_POST['date']));
        $purchaseno = $_POST['purchaseno'];
        $purchase_id = $_POST['purchaseview'];
        if (!empty($purchase_id)) {
            $item_model = PurchaseItems::model()->findAll(array("condition" => "purchase_id = '$purchase_id'"));
            $purchase_items = $this->setPurchaseItems($item_model);
        }
        $purchase_data = Purchase::model()->findByPK($purchase_id);
        $project = $project_data['name'];
        $vendor = $invendor_data['name'];
        $expense_head = $expense_data['type_name'];
        $company_name = $company_data['name'];
        $subtot = $_POST['subtot'];
        $grand = $_POST['grand'];
        if (isset($_POST['sl_No'])) {
            $sl_No = array_filter($_POST['sl_No']);
        } else {
            $sl_No = '';
        }
        if (isset($_POST['description'])) {
            $description = $_POST['description'];
        } else {
            $description = '';
        }
        if (isset($_POST['quantity'])) {
            $quantity = array_filter($_POST['quantity']);
        } else {
            $quantity = '';
        }
        if (isset($_POST['unit'])) {
            $unit = $_POST['unit'];
        } else {
            $unit = '';
        }
        if (isset($_POST['hsn_code'])) {
            $hsn_code = $_POST['hsn_code'];
        } else {
            $hsn_code = '';
        }
        if (isset($_POST['rate'])) {
            $rate = array_filter($_POST['rate']);
        } else {
            $rate = '';
        }
        if (isset($_POST['amount'])) {
            $amount = array_filter($_POST['amount']);
        } else {
            $amount = '';
        }
        if (isset($_POST['txtCompany'])) {
            $companyName = $_POST['txtCompany'];
        } else {
            $companyName = array();
        }
        if (isset($_POST['txtCompanyGst'])) {
            $companyGst = $_POST['txtCompanyGst'];
        } else {
            $companyGst = array();
        }
        if (isset($_POST["txtPurchaseCompany"])) {
            $poCompanyId = $_POST["txtPurchaseCompany"];
        } else {
            $poCompanyId = array();
        }

        $specification = array();
        if (isset($_POST['purchaseview'])) {
            $specification = $description;
        } else {
            $tblpx = Yii::app()->db->tablePrefix;
            for ($i = 0; $i < sizeof($description); $i++) {
                if ($description[$i] == '') {
                    $specification[] = $_POST['remark'][$i];
                } else {
                    $data = Yii::app()->db->createCommand("SELECT specification FROM {$tblpx}purchase_category WHERE id=" . $description[$i] . "")->queryRow();
                    $specification[] = $data['specification'];
                }
            }
        }
        $arraylabel = array('Company', 'GST NO', 'Project Name', 'Expense Head', 'Vendor Name', 'Date', 'Purchase No', 'Contact No', 'Shipping Address');
        $finaldata = array();
        $finaldata[0][] = $companyName;
        $finaldata[0][] = $companyGst;
        $finaldata[0][] = $project;
        $finaldata[0][] = (isset($expense_head)) ? $expense_head : '';
        $finaldata[0][] = (isset($vendor)) ? $vendor : '';
        $finaldata[0][] = $date;
        $finaldata[0][] = $purchaseno;
        $finaldata[0][] = $purchase_data->contact_no;
        $finaldata[0][] = $purchase_data->shipping_address;

        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';
        $finaldata[1][] = '';

        $finaldata[2][] = 'Sl.No';
        $finaldata[2][] = 'Description of Goods';
        $finaldata[2][] = 'HSN Code';
        $finaldata[2][] = 'Quantity';
        $finaldata[2][] = 'Unit Price';
        $finaldata[2][] = 'Taxable';
        $finaldata[2][] = 'GST(%)';
        $finaldata[2][] = 'GST Amount';
        $finaldata[2][] = 'Total';
        $k = 2;
        if (!empty($purchase_items)) {

            $k = 1;
            foreach ($purchase_items as $purchase_item) {

                if (isset($purchase_item['category_title'])) {
                    $finaldata[$k + 2][] = $purchase_item['category_title'];
                    $finaldata[$k + 2][] = '';
                    $finaldata[$k + 2][] = '';
                    $finaldata[$k + 2][] = '';
                    $finaldata[$k + 2][] = '';
                    $finaldata[$k + 2][] = '';
                    $finaldata[$k + 2][] = '';
                    $finaldata[$k + 2][] = '';
                    $finaldata[$k + 2][] = '';
                    $finaldata[$k + 2][] = '';
                }
                $i = $k;
                $discount_total = $tax_total_amount = 0;
                foreach ($purchase_item as $data) {

                    if (isset($data['specification'])) {
                        $discount_total += $data['disc'];
                        $tax_total_amount += $data['tot_tax'];
                        $item_amount = ($data['quantity'] * $data['rate']) - $data['disc'];
                        $total_tax_perc = $data['sgstp'] + $data['cgstp'] + $data['igstp'];
                        $amount_total = $item_amount + $data['tot_tax'];
                        $finaldata[$i + 3][] = '1';
                        $finaldata[$i + 3][] = $data['specification'];
                        $finaldata[$i + 3][] = $data['hsn_code'];
                        $finaldata[$i + 3][] = $data['quantity'] . ' ' . $data['unit'];
                        $finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['rate'], 2, 1);
                        $finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($item_amount, 2, 1);
                        $finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($total_tax_perc, 2, 1);
                        $finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($data['tot_tax'], 2, 1);
                        $finaldata[$i + 3][] = Yii::app()->Controller->money_format_inr($amount_total, 2, 1);
                        $i++;
                        $x = $i + 3;
                    }
                }

                $k += count($purchase_item);
            }
            $finaldata[$x][] = '';
            $finaldata[$x][] = '';
            $finaldata[$x][] = '';
            $finaldata[$x][] = '';
            $finaldata[$x][] = '';
            $finaldata[$x][] = '';
            $finaldata[$x][] = '';
            $finaldata[$x][] = 'Grand Total:';
            $finaldata[$x][] = Yii::app()->Controller->money_format_inr(($purchase_data->getPurchaseTotalAmount($purchase_id)), 2, 1);
            if ($purchase_data->inclusive_gst == 1) {
                $finaldata[$x + 1][] = '';
                $finaldata[$x + 1][] = '';
                $finaldata[$x + 1][] = '';
                $finaldata[$x + 1][] = '';
                $finaldata[$x + 1][] = '*Inclusive of GST';
            }
        } else {
            $finaldata[3][] = '';
            $finaldata[3][] = '';
            $finaldata[3][] = '';
            $finaldata[3][] = '';
            $finaldata[3][] = '';
            $finaldata[3][] = '';


            $finaldata[4][] = '';
            $finaldata[4][] = '';
            $finaldata[4][] = '';
            $finaldata[4][] = '';

            $finaldata[5][] = 'Grand Total';
            $finaldata[5][] = $grand;
        }
        Yii::import('ext.ECSVExport');
        $csv = new ECSVExport($finaldata);
        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Purchase' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actionPurchaseitem()
    {
        $data = $_REQUEST['data'];
        $result = '';
        $html = '';
        $tblpx = Yii::app()->db->tablePrefix;
        if ($data['description'] == 'other') {
            $descriptions = $data['remark'];
            $spc_id = 'other';
        } else {
            $spec_sql = "SELECT id, cat_id, specification, brand_id, unit FROM {$tblpx}specification WHERE id=" . $data['description'] . "";
            $specification = Yii::app()->db->createCommand($spec_sql)->queryRow();
            $parent_sql = "SELECT * FROM {$tblpx}purchase_category "
                . " WHERE id='" . $specification['cat_id'] . "'";
            $parent_category = Yii::app()->db->createCommand($parent_sql)->queryRow();

            if ($specification['brand_id'] != NULL) {
                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                $brand = '-' . $brand_details['brand_name'];
            } else {
                $brand = '';
            }
            $descriptions = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
            $spc_id = $specification['id'];
        }

        if ($data['purchase_id'] == 0) {
            echo json_encode(array('response' => 'error', 'msg' => 'Please enter purchase details'));
        } else {
            $po_details = Purchase::model()->findByPk($data['purchase_id']);
            if (isset($data['sl_No'])) {
                $sl_No = $data['sl_No'];
            } else {
                $sl_No = '';
            }
            if (isset($data['description'])) {
                $description = $data['description'];
            } else {
                $description = '';
            }
            if (isset($data['remark'])) {
                $remark = $data['remark'];
            } else {
                $remark = '';
            }

            if (isset($data['quantity'])) {
                $quantity = $data['quantity'];
            } else {
                $quantity = '';
            }
            if (isset($data['unit'])) {
                $unit = $data['unit'];
            } else {
                $unit = '';
            }
            if (isset($data['rate'])) {
                $rate = $data['rate'];
            } else {
                $rate = '';
            }
            if (isset($data['amount'])) {
                $amount = $data['amount'];
            } else {
                $amount = '';
            }

            if (isset($data['hsn_code'])) {
                $hsn_code = $data['hsn_code'];
            } else {
                $hsn_code = '';
            }

            if (isset($data['base_qty'])) {
                $base_qty = $data['base_qty'];
            } else {
                $base_qty = '';
            }
            if (isset($data['base_unit'])) {
                $base_unit = $data['base_unit'];
            } else {
                $base_unit = '';
            }
            if (isset($data['base_rate'])) {
                $base_rate = $data['base_rate'];
            } else {
                $base_rate = '';
            }

            $previous_rate = array();
            $permission_status = 1;
            $final_rate = "";
            $class = "";
            $style = "";
            $mail_send = "";
            if ($description != 'other') {
                $previous_rate = Yii::app()->db->createCommand("SELECT MAX({$tblpx}billitem.billitem_rate) as max_rate FROM  {$tblpx}billitem LEFT JOIN {$tblpx}bills  ON {$tblpx}bills.bill_id={$tblpx}billitem.bill_id LEFT JOIN {$tblpx}purchase_items ON {$tblpx}billitem.purchaseitem_id={$tblpx}purchase_items.item_id LEFT JOIN {$tblpx}purchase ON {$tblpx}purchase_items.purchase_id= {$tblpx}purchase.p_id WHERE {$tblpx}billitem.category_id=" . $description . " ORDER BY {$tblpx}billitem.billitem_id desc LIMIT 0,4")->queryRow();
            }
            $status = $this->actionGetpurchaseItemStatus();
            $item_model = new PurchaseItems();
            $item_model->purchase_id = $data['purchase_id'];
            $item_model->quantity = $quantity;
            $item_model->unit = $unit;
            $item_model->hsn_code = $hsn_code;
            $item_model->rate = $rate;
            $item_model->amount = $amount;
            $item_model->category_id = $description;
            $item_model->remark = $remark;
            $item_model->item_status = $status['sid'];
            $item_model->created_by = Yii::app()->user->id;
            $item_model->created_date = date('Y-m-d');
            $item_model->base_qty = $base_qty;
            $item_model->base_unit = $base_unit;
            $item_model->base_rate = $base_rate;
            $item_model->estimation_approval_type = null;

            $item_model->tax_slab = isset($data['tax_slab']) ? $data['tax_slab'] : 0;
            $item_model->discount_amount = isset($data['discount_amount']) ? $data['discount_amount'] : 0;
            $item_model->discount_percentage = isset($data['disp']) ? $data['disp'] : 0;

            $item_model->cgst_amount = isset($data['cgst_amount']) ? $data['cgst_amount'] : 0;
            $item_model->cgst_percentage = isset($data['cgstp']) && !empty($data['cgstp']) ? $data['cgstp'] : 0;
            $item_model->igst_amount = isset($data['igst_amount']) ? $data['igst_amount'] : 0;
            $item_model->igst_percentage = isset($data['igstp']) && !empty($data['igstp']) ? $data['igstp'] : 0;
            $item_model->sgst_amount = isset($data['sgst_amount']) ? $data['sgst_amount'] : 0;
            $item_model->sgst_percentage = isset($data['sgstp']) && !empty($data['sgstp']) ? $data['sgstp'] : 0;
            $item_model->tax_amount = isset($data['tax_amount']) ? $data['tax_amount'] : 0;
            $item_model->tax_perc = $item_model->cgst_percentage + $item_model->igst_percentage + $item_model->sgst_percentage;

            // permission
            $company_details = Company::model()->findByPk($po_details->company_id);
            $estimated_qty = Yii::app()->db->createCommand("SELECT `itemestimation_quantity` FROM `jp_itemestimation` WHERE `project_id`='" . $data['project'] . "' AND `category_id`= '" . $description . "'")->queryScalar();

            if (!empty($estimated_qty)) {
                if (($data['remaining_estimation_qty'] >= 0) && $quantity > $data['remaining_estimation_qty']) {
                    $item_model->permission_status = 2;
                    $approve_status = 2;
                    $class = "rate_highlight";
                    $style = "style='cursor:pointer'";
                    $item_model->estimation_approval_type = 2;
                } else {
                    $item_model->permission_status = 1;
                    $approve_status = 1;
                    $class = "";
                    $style = "";
                }
            }
            $estimated_rate = Yii::app()->db->createCommand("SELECT `itemestimation_price` FROM `jp_itemestimation` WHERE `project_id`='" . $data['project'] . "' AND `category_id`= '" . $description . "'")->queryScalar();

            if (!empty($estimated_rate)) {
                $company_details = Company::model()->findByPk($po_details->company_id);
                // $previous_total = $previous_rate['max_rate'] * $company_details->company_tolerance / 100;
                // $final_rate = $previous_total + $previous_rate['max_rate'];
                if ($rate > $estimated_rate) {
                    $item_model->permission_status = 2;
                    $approve_status = 2;
                    $class = "rate_highlight";
                    $style = "style='cursor:pointer'";
                    if ($item_model->estimation_approval_type == 2) {
                        $item_model->estimation_approval_type = 3;//both need approval
                    } else {
                        $item_model->estimation_approval_type = 1;
                    }
                }
            }

            if ($item_model->save()) {
                $item_amount = Yii::app()->db->createCommand("SELECT SUM(amount) as item_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryScalar();

                $qty_total = Yii::app()->db->createCommand("SELECT SUM(quantity) as qty FROM
                 {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryScalar();

                if ($company_details->company_popermission == 1) {
                    $permission_status = 'No';
                } else {
                    if ($company_details->purchaseorder_limit != '') {
                        if ($item_amount > $company_details->purchaseorder_limit) {
                            $permission_status = 'No';
                        } else {
                            $permission_status = 'Yes';
                        }
                    } else {
                        $permission_status = 'Yes';
                    }
                }
                $model = Purchase::model()->findByPk($data['purchase_id']);
                $model->total_amount = $item_amount;
                $model->sub_total = $item_amount;
                $model->permission_status = $permission_status;
                $model->save(false);
                $permission = PurchaseItems::model()->findAll(array("condition" => "purchase_id=" . $data['purchase_id'] . " AND permission_status=2"));
                if ($permission) {
                    $update = Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET 	purchase_status = 'permission_needed' WHERE p_id = '" . $data['purchase_id'] . "'")->execute();
                    $model = Purchase::model()->findByPk($data['purchase_id']);
                    if ($model->mail_status == 'N' && $model->purchase_status == 'permission_needed') {
                        $mail_send = 'Y';
                    } else {
                        $mail_send = 'N';
                    }
                } else {
                    Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET 	purchase_status = 'draft' WHERE p_id = '" . $data['purchase_id'] . "'")->execute();
                    $mail_send = 'N';
                }
                $last_id = $item_model->item_id;

                $result = $this->renderPartial('_purchase_data', array(
                    'data' => $data,
                    'imodel' => $item_model,
                    'spc_id' => $spc_id,
                    'descriptions' => $descriptions,
                    'style' => $style,
                    'last_id' => $last_id,
                    'class' => $class,
                    'permission_status' => $permission_status,
                ), true);

                $purchase_rate = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount FROM  {$tblpx}purchase_items WHERE purchase_id =" . $data['purchase_id'] . "")->queryRow();


                $total_discount_value = Yii::app()->db->createCommand("SELECT SUM(discount_amount) as discount_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryRow();
                $total_tax_amount = Yii::app()->db->createCommand("SELECT SUM(tax_amount) as tax_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryRow();
                $grand_total_amount = ($purchase_rate['total_amount'] + $total_tax_amount['tax_amount']) - $total_discount_value['discount_amount'];

                echo json_encode(array(
                    'response' => 'success',
                    'msg' => 'Purchase item save successfully',
                    'item_id' => $last_id,
                    'html' => $result,
                    'final_amount' => number_format($purchase_rate['total_amount'], 2),
                    'qty_total' => $qty_total,
                    'discount_total' => number_format($total_discount_value['discount_amount'], 2),
                    'tax_total' => number_format($total_tax_amount['tax_amount'], 2),
                    'grand_total' => number_format($grand_total_amount, 2),
                    'mail_send' => $mail_send
                ));
            } else {
                echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
            }
        }
    }

    /* previous transaction details */

    public function actionPrevioustransaction()
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $html = '';
        $html = array('html' => '', 'status' => '', 'unit' => '', 'hsn_code' => '', 'exist' => '', 'units' => '', 'updated_unit' => '', 'estimated_quantity' => '', 'estimated_rate' => '');
        $base_unit = '';

        $project_id = Yii::app()->request->getQuery('project_id');
        if ($_REQUEST['data'] != '' && $_REQUEST['data'] != 'other') {
            $hsn_code = NULL;
            $brand = '';
            $criteria = new CDbCriteria();
            $criteria->select = "id, cat_id, brand_id, specification, unit,hsn_code";
            $criteria->condition = "id=" . $_REQUEST['data'] . " AND specification_type = 'O'";
            $specification = Specification::model()->find($criteria);

            if ($specification['brand_id'] != NULL) {
                $criteria = new CDbCriteria();
                $criteria->select = "brand_name";
                $criteria->condition = "id=" . $specification['brand_id'];
                $brand_details = Brand::model()->find($criteria);
                $brand = '-' . ucwords($brand_details['brand_name']);
            }

            if ($specification['cat_id'] != NULL) {
                $criteria = new CDbCriteria();
                $criteria->select = "category_name";
                $criteria->condition = "id=" . $specification['cat_id'];
                $category = PurchaseCategory::model()->find($criteria);
            }


            if (!empty($specification['hsn_code'])) {
                $hsn_code = $specification['hsn_code'];
            }

            $data = Yii::app()->db->createCommand()
                ->select('t.*,pi.*,b.bill_number,b.bill_date,p.*')
                ->from($this->tableNameAcc('billitem', 0) . ' as t')
                ->where('t.category_id="' . $_REQUEST['data'] . '" AND p.purchase_type="O" 
                AND (t.purchaseitem_id IS NOT NULL AND t.purchaseitem_id <> 0)')
                ->leftJoin($this->tableNameAcc('bills', 0) . ' as b', 't.bill_id = b.bill_id')
                ->leftJoin($this->tableNameAcc('purchase_items', 0) . ' as pi', 't.purchaseitem_id = pi.item_id')
                ->leftJoin($this->tableNameAcc('purchase', 0) . ' as p', 'p.p_id = pi.purchase_id')
                ->order('t.billitem_id  DESC')
                ->queryAll();
            //$estimated_qty = Yii::app()->db->createCommand("SELECT `itemestimation_quantity` FROM `jp_itemestimation` WHERE `project_id`='".$data['project']."' AND `category_id`= '".$description."'")->queryScalar();

            $itemestimation = Yii::app()->db->createCommand("SELECT itemestimation_quantity,itemestimation_price FROM {$tblpx}itemestimation WHERE project_id='" . $project_id . "' AND category_id=" . $specification['id'] . "")->queryRow();

            if (!empty($data)) {
                $html['html'] = $this->renderPartial(
                    "_previous_data",
                    array(
                        'data' => $data,
                        'category' => $category,
                        'brand' => $brand,
                        'specification' => $specification,
                        'type' => 'O',
                        'estimated_quantity' => $itemestimation['itemestimation_quantity'],
                        'estimated_rate' => $itemestimation['itemestimation_price'],
                    ),
                    true
                );

                $html['status'] .= true;
            }
            $html['estimated_quantity'] = '';
            $html['estimated_rate'] = '';
            if (!empty($itemestimation)) {
                $html['estimated_quantity'] = $itemestimation['itemestimation_quantity'];
                $html['estimated_rate'] = $itemestimation['itemestimation_price'];
            }
            if (!empty($specification['unit'])) {
                $base_unit = $specification['unit'];
            }
            $html['base_unit'] = $base_unit;

            if ($base_unit != '') {
                $uc_item_count = UnitConversion::model()->countByAttributes(array(
                    'item_id' => $_REQUEST['data'],
                ));

                if ($uc_item_count > 1) {
                    $html['units'] = $this->GetItemunitlist($_REQUEST['data']);
                    $html['unit'] = $html['units'];
                } else {
                    if (!empty($specification['unit'])) {
                        $html['unit'] .= $base_unit;
                        $html['units'] .= $base_unit;
                    } else {
                        $html['unit'] .= '';
                        $html['units'] = 'Unit';
                    }
                }
            } else {
                $html['unit'] .= '';
                $html['units'] = 'Unit';
            }

            if (!empty($hsn_code)) {
                $html['hsn_code'] .= $hsn_code;
            } else {
                $html['hsn_code'] .= '';
            }
            if (isset($_GET['purchase_id']) && !empty($_GET['purchase_id'])) {
                $purchase_id = $_GET['purchase_id'];
                $itemid_val = $_GET['itemid_val'];
                if (!empty($itemid_val)) {
                    $purchase_items = PurchaseItems::model()->findAll(array('condition' => 'purchase_id = ' . $purchase_id . ' AND category_id = ' . $_REQUEST['data'] . ' AND item_id =' . $itemid_val));
                } else {
                    $purchase_items = PurchaseItems::model()->findAll(array('condition' => 'purchase_id = ' . $purchase_id . ' AND category_id = ' . $_REQUEST['data']));
                }

                if (!empty($purchase_items)) {
                    $html['exist'] .= '1';
                    $html['updated_unit'] .= $purchase_items[0]['unit'];
                }
            }
        } else {

            $html['html'] .= '';
            $html['status'] .= false;
        }

        echo json_encode($html);
    }

    /**  Neethu  * */
    public function actionCreatenewpurchase()
    {
        // Yii::app()->user->setState('form_data', '');
        $mrId = '';
        $material_selected_items = '';
        if (isset($_REQUEST['mrId']) && !empty($_REQUEST['mrId'])) {
            $mrId = $_REQUEST['mrId'];
        }
        if (isset($_REQUEST['selected_items']) && !empty($_REQUEST['selected_items'])) {
            $material_selected_items = $_REQUEST['selected_items'];
        }

        $purchase_id = $_REQUEST['purchase_id'];

        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($_REQUEST['purchaseno']) && isset($_REQUEST['default_date']) && isset($_REQUEST['vendor']) && isset($_REQUEST['project'])) {
            if ($_REQUEST['project'] > 0) {
                $purchasedata = Purchase::model()->findAll(array('condition' => 'project_id = ' . $_REQUEST['project'] . ''));
                if (count($purchasedata) == 0) {
                    $sql = "UPDATE {$tblpx}projects SET project_status = 84 WHERE pid = " . $_REQUEST['project'];
                    Yii::app()->db->createCommand($sql)->execute();
                }
            }
            //die($purchase_id);
            if ($purchase_id == 0) {
                $billing_status = Yii::app()->db->createCommand("SELECT sid FROM {$tblpx}status WHERE status_type='purchase_bill_status' AND caption='PO not issued'")->queryRow();
                $puchase_no = $_REQUEST['purchaseno'];
                $company = $_REQUEST['company'];
                $sql = "SELECT * from " . $tblpx . "purchase  WHERE `purchase_no` = '" . $puchase_no . "' AND `company_id` = '" . $company . "' ORDER BY `p_id` DESC LIMIT 1;";
                $res = Yii::app()->db->createCommand($sql)->queryRow();
                //echo "<pre>";print_r($res);exit;
                if (empty($res)) {

                    $model = new Purchase;
                    $model->purchase_no = $_REQUEST['purchaseno'];
                    $model->expected_delivery_date = !empty($_REQUEST['delivery_date']) ? date('Y-m-d', strtotime($_REQUEST['delivery_date'])) : NULL;
                    $model->project_id = $_REQUEST['project'];
                    $model->vendor_id = $_REQUEST['vendor'];
                    $model->expensehead_id = $_REQUEST['expense_head'];
                    $model->purchase_date = date('Y-m-d', strtotime($_REQUEST['default_date']));
                    $model->contact_no = $_REQUEST['contact_no'];
                    $model->shipping_address = $_REQUEST['shipping_address'];
                    $model->purchase_description = $_REQUEST['description_po'];
                    $model->inclusive_gst = $_REQUEST['inclusive_gst'];
                    $company_details = Company::model()->findByPk(Yii::app()->user->company_id);
                    $model->purchase_status = 'draft';
                    $model->purchase_billing_status = $billing_status['sid'];
                    $model->created_date = date('Y-m-d');
                    $model->created_by = Yii::app()->user->id;
                    if (!empty($mrId)) {
                        $model->mr_id = $mrId;

                    }
                    if (!empty($material_selected_items)) {
                        $model->mr_material_ids = $material_selected_items;
                    }

                    $model->company_id = $_REQUEST['company'];
                    $projects = Projects::model()->findByPk($_REQUEST['project']);
                    $users = Users::model()->findByPk(Yii::app()->user->id);
                    $name = $users->first_name . '' . $users->last_name;
                    $notification = new Notifications;
                    $notification->action = "Create";
                    $notification->date = date("Y-m-d");
                    $notification->message = "PO created for project " . $projects->name . " by user " . $name . " ";
                    $notification->requested_by = Yii::app()->user->id;
                    $notification->approved_by = Yii::app()->user->id;
                    $model->save(false);
                    $last_id = $model->p_id;
                    $notification->parent_id = $last_id;
                    $notification->save();
                    if (!empty($mrId)) {
                        $materialreq = MaterialRequisition::model()->findByPk($mrId);
                        if (!empty($materialreq)) {
                            $new_pos = "";
                            $pos = $materialreq->purchase_id;
                            if (!empty($pos)) {
                                $new_pos = $pos . ',' . $last_id;
                            }
                            if (empty($new_pos)) {
                                $new_pos = $last_id;
                            }

                            $materialreq->purchase_id = $new_pos;
                            $materialreq->save();
                            $mr_no = Controller::updateDynamicMaterialReqNo($mrId, $last_id);
                        }

                    }
                    if (!empty($material_selected_items)) {
                        $selected_materials_arr = explode(',', $material_selected_items);
                        foreach ($selected_materials_arr as $selected_material) {
                            $criteria = new CDbCriteria();
                            $criteria->condition = 'mr_id = :mr_id AND material_item_id = :material_item_id';
                            $criteria->params = array(':mr_id' => $mrId, ':material_item_id' => $selected_material);

                            $mr_materials_model = MaterialRequisitionItems::Model()->findAll($criteria);

                            if (!empty($mr_materials_model)) {
                                foreach ($mr_materials_model as $material) {
                                    $material->po_id = $last_id;
                                    $material->save(); // Update the po_id value

                                }
                            }
                        }
                    }
                    echo json_encode(array('response' => 'success', 'msg' => 'Purchase added successfully', 'p_id' => $last_id));
                } else {
                    // echo json_encode(array('response' => 'error', 'msg' => 'Purchase number already exist'));
                }
            } else {
                $res = Purchase::model()->findAll(array('condition' => 'purchase_no = "' . $_REQUEST['purchaseno'] . '" AND p_id !=' . $purchase_id . ' AND company_id = ' . $_REQUEST['company'] . ''));
                if (empty($res)) {
                    $model = Purchase::model()->findByPk($purchase_id);
                    $model->purchase_no = $_REQUEST['purchaseno'];
                    $model->expected_delivery_date = !empty($_REQUEST['delivery_date']) ? date('Y-m-d', strtotime($_REQUEST['delivery_date'])) : NULL;
                    $model->project_id = $_REQUEST['project'];
                    $model->vendor_id = $_REQUEST['vendor'];
                    $model->expensehead_id = $_REQUEST['expense_head'];
                    $model->contact_no = $_REQUEST['contact_no'];
                    $model->shipping_address = $_REQUEST['shipping_address'];
                    $model->purchase_date = date('Y-m-d', strtotime($_REQUEST['default_date']));
                    $model->purchase_description = $_REQUEST['description_po'];
                    $model->inclusive_gst = $_REQUEST['inclusive_gst'];
                    $company_details = Company::model()->findByPk(Yii::app()->user->company_id);
                    $model->created_date = date('Y-m-d');
                    $model->company_id = $_REQUEST['company'];
                    $model->created_by = Yii::app()->user->id;
                    if ($model->save(false)) {
                        echo json_encode(array('response' => 'success', 'msg' => 'Purchase updated successfully', 'p_id' => $purchase_id));
                    } else {
                        echo json_encode(array('response' => 'error', 'msg' => 'Please enter all details'));
                    }
                } else {
                    // echo json_encode(array('response' => 'error', 'msg' => 'Purchase number already exist'));
                }
            }
        }
    }

    public function actionUpdatepurchaseitem()
    {
        $data = $_REQUEST['data'];


        $po_details = Purchase::model()->findByPk($data['purchase_id']);

        if (isset($data['description'])) {
            $description = $data['description'];
        } else {
            $description = '';
        }

        if (isset($data['remark'])) {
            $remark = $data['remark'];
        } else {
            $remark = '';
        }

        if (isset($data['quantity'])) {
            $quantity = $data['quantity'];
        } else {
            $quantity = '';
        }
        if (isset($data['unit'])) {
            $unit = $data['unit'];
        } else {
            $unit = '';
        }
        if (isset($data['rate'])) {
            $rate = $data['rate'];
        } else {
            $rate = '';
        }
        if (isset($data['amount'])) {
            $amount = $data['amount'];
        } else {
            $amount = '';
        }

        if (isset($data['base_qty'])) {
            $base_qty = $data['base_qty'];
        } else {
            $base_qty = '';
        }
        if (isset($data['base_unit'])) {
            $base_unit = $data['base_unit'];
        } else {
            $base_unit = '';
        }
        if (isset($data['base_rate'])) {
            $base_rate = $data['base_rate'];
        } else {
            $base_rate = '';
        }

        if (isset($data['hsn_code'])) {
            $hsn_code = $data['hsn_code'];
        } else {
            $hsn_code = '';
        }


        $result = '';
        $html = '';
        $mail_send = '';
        $tblpx = Yii::app()->db->tablePrefix;
        $previous_rate = array();
        if ($description != 'other') {
            $previous_rate = Yii::app()->db->createCommand("SELECT MAX({$tblpx}billitem.billitem_rate) as max_rate FROM  {$tblpx}billitem LEFT JOIN {$tblpx}bills  ON {$tblpx}bills.bill_id={$tblpx}billitem.bill_id LEFT JOIN {$tblpx}purchase_items ON {$tblpx}billitem.purchaseitem_id={$tblpx}purchase_items.item_id LEFT JOIN {$tblpx}purchase ON {$tblpx}purchase_items.purchase_id= {$tblpx}purchase.p_id WHERE {$tblpx}billitem.category_id=" . $data['description'] . " ORDER BY {$tblpx}billitem.billitem_id desc LIMIT 0,4")->queryRow();
        }
        //echo "<pre>";print_r($data);exit;
        if (!empty($data) && $data['item_id'] != 0) {
            $item_model = PurchaseItems::model()->findByPk($data['item_id']);
            $item_model->quantity = $data['quantity'];
            $item_model->amount = $data['amount'];

            $item_model->hsn_code = $data['hsn_code'];
            $item_model->rate = $data['rate'];
            if (!empty($data['unit'])) {
                $item_model->unit = $data['unit'];
            }
            if (!empty($data['base_qty'])) {
                $item_model->base_qty = $data['base_qty'];
            }
            if (!empty($data['base_rate'])) {
                $item_model->base_rate = $data['base_rate'];
            }
            if (!empty($data['base_unit'])) {
                $item_model->base_unit = $data['base_unit'];
            }
            $item_model->category_id = $data['description'];
            $item_model->remark = $data['remark'];
            $item_model->tax_slab = isset($data['tax_slab']) ? $data['tax_slab'] : 0;
            $item_model->discount_amount = isset($data['discount_amount']) ? $data['discount_amount'] : 0;
            $item_model->discount_percentage = isset($data['disp']) ? $data['disp'] : 0;
            $item_model->cgst_amount = isset($data['cgst_amount']) ? $data['cgst_amount'] : 0;
            $item_model->cgst_percentage = isset($data['cgstp']) ? $data['cgstp'] : 0;
            $item_model->igst_amount = isset($data['igst_amount']) ? $data['igst_amount'] : 0;
            $item_model->igst_percentage = isset($data['igstp']) ? $data['igstp'] : 0;
            $item_model->sgst_amount = isset($data['sgst_amount']) ? $data['sgst_amount'] : 0;
            $item_model->sgst_percentage = isset($data['sgstp']) ? $data['sgstp'] : 0;
            $item_model->tax_amount = isset($data['tax_amount']) ? $data['tax_amount'] : 0;
            $item_model->tax_perc = $item_model->cgst_percentage + $item_model->igst_percentage + $item_model->sgst_percentage;
            $company_details = Company::model()->findByPk($po_details->company_id);
            $estimated_rate = Yii::app()->db->createCommand("SELECT `itemestimation_price` FROM `jp_itemestimation` WHERE `project_id`='" . $data['project'] . "' AND `category_id`= '" . $data['description'] . "'")->queryScalar();
            if (!empty($estimated_rate)) {
                $company_details = Company::model()->findByPk($po_details->company_id);
                // $previous_total = $previous_rate['max_rate'] * $company_details->company_tolerance / 100;
                // $final_rate = $previous_total + $previous_rate['max_rate'];
                if ($rate > $estimated_rate) {
                    $item_model->permission_status = 2;
                } else {
                    $item_model->permission_status = 1;
                }
            }

            if ($item_model->save()) {

                $item_amount = Yii::app()->db->createCommand("SELECT SUM(amount) as item_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryScalar();
                $qty_total = Yii::app()->db->createCommand("SELECT SUM(quantity) as qty FROM
                 {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryScalar();
                if ($company_details->company_popermission == 1) {
                    $permission_status = 'No';
                } else {
                    if ($company_details->purchaseorder_limit != '') {
                        if ($item_amount > $company_details->purchaseorder_limit) {
                            $permission_status = 'No';
                        } else {
                            $permission_status = 'Yes';
                        }
                    } else {
                        $permission_status = 'Yes';
                    }
                }

                $model = Purchase::model()->findByPk($data['purchase_id']);
                $model->total_amount = $item_amount;
                $model->sub_total = $item_amount;
                $model->permission_status = $permission_status;
                $model->save(false);
                $permission = PurchaseItems::model()->findAll(array("condition" => "purchase_id=" . $data['purchase_id'] . " AND permission_status=2"));
                if ($permission) {
                    Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET 	purchase_status = 'permission_needed' WHERE p_id = '" . $data['purchase_id'] . "'")->execute();
                    $model = Purchase::model()->findByPk($data['purchase_id']);
                    if ($model->mail_status == 'N' && $model->purchase_status == 'permission_needed') {
                        $mail_send = 'Y';
                    } else {
                        $mail_send = 'N';
                    }
                } else {
                    Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET 	purchase_status = 'draft' WHERE p_id = '" . $data['purchase_id'] . "'")->execute();
                    $mail_send = 'N';
                }
                $result = '';
                $style = "";
                $grand_total = 0;
                $discount_total = $tax_amount_total = $grand_total_amount = 0;

                $purchase_details = Yii::app()->db->createCommand(" SELECT * FROM {$tblpx}purchase_items  WHERE purchase_id=" . $data['purchase_id'] . "")->queryAll();
                foreach ($purchase_details as $key => $values) {
                    $class = "";
                    if ($values['permission_status'] == 'not_approved') {
                        $class = "rate_highlight";
                        $style = "style='cursor:pointer'";
                    } else {
                        $class = "";
                        $style = "";
                    }

                    $grand_total += $values['amount'];
                    $discount_total += $values['discount_amount'];
                    $tax_amount_total += $values['tax_amount'];
                    $grand_total_amount += (($values['amount'] + $values['tax_amount']) - $values['discount_amount']);
                    if ($values['category_id'] == 0) {

                        $spc_details = $values['remark'];
                        $spc_id = 'other';
                    } else {
                        $spec_sql = "SELECT id, cat_id,brand_id, specification, unit "
                            . " FROM {$tblpx}specification "
                            . " WHERE id=" . $values['category_id'] . "";
                        $specification = Yii::app()->db->createCommand($spec_sql)->queryRow();
                        $parent_sql = "SELECT * FROM {$tblpx}purchase_category WHERE id='" . $specification['cat_id'] . "'";
                        $parent_category = Yii::app()->db->createCommand($parent_sql)->queryRow();

                        if ($specification['brand_id'] != NULL) {
                            $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                            $brand = '-' . $brand_details['brand_name'];
                        } else {
                            $brand = '';
                        }

                        $spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
                        $spc_id = $specification['id'];
                    }



                    $result .= '<tr>';
                    $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                    $result .= '<td><div class="item_description" id="' . $spc_id . '">' . $spc_details . '</div> </td>';
                    $result .= '<td> <div class="unit" id="unit"> ' . $values['hsn_code'] . '</div> </td>';
                    $result .= '<td class="text-right"> <div class="" id="quantity"> ' . $values['quantity'] . '</div> </td>';
                    $result .= '<td> <div class="unit" id="unit"> ' . $values['unit'] . '</div> </td>';
                    $result .= '<td class="text-right ' . $class . '" ' . $style . ' id=' . $values['item_id'] . '><div class="" id="rate">' . number_format($values['rate'], 2, '.', ',') . '</div></td>';
                    $result .= '<td class="text-right"><div class=""  id="base_qty"> ' . $values['base_qty'] . '</div></td>';
                    $result .= '<td class="text-right"><div class=""  id="base_unit"> ' . $values['base_unit'] . '</div></td>';
                    $result .= '<td class="text-right ' . $class . '" ' . $style . ' id=' . $values['item_id'] . '><div class="" id="base_rate">' . number_format($values['base_rate'], 2, '.', ',') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" > ' . number_format($values['amount'], 2, '.', ',') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" > ' . $values['tax_slab'] . '</div></td>';
                    $result .= '<td class="text-right"><div class="">' . number_format($values['sgst_amount'], 2) . '</div> </td>';
                    $result .= '<td class="text-right"><div class=""> ' . number_format($values['sgst_percentage'], 2, '.', ',') . '</div> </td>';
                    $result .= '<td class="text-right"><div class="" >' . number_format($values['cgst_amount'], 2) . '</div></td>';
                    $result .= '<td class="text-right"><div class="" >' . number_format($values['cgst_percentage'], 2, '.', ',') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" >' . number_format($values['igst_amount'], 2) . '</div></td>';
                    $result .= '<td class="text-right"><div class="" >' . number_format($values['igst_percentage'], 2, '.', ',') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" >' . number_format($values['discount_percentage'], 2, '.', ',') . '</div> </td>';
                    $result .= '<td class="text-right"><div class="">' . number_format($values['discount_amount'], 2, '.', ',') . '</div></td>';

                    $result .= '<td class="text-right"><div class="" > ' . number_format($values['tax_amount'], 2, '.', ',') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" > ' . number_format((($values['tax_amount'] + $values['amount']) - $values['discount_amount']), 2, '.', ',') . '</div></td>';
                    $result .= '<td width="70"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                                                        <div class="popover-content hide">
                                                            <ul class="tooltip-hiden">
                                                                    <li><a href="#" id=' . $values['item_id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li>
                                                                    <li><a href="" id=' . $values['item_id'] . ' class="btn btn-xs btn-default edit_item">Edit</a></li>&nbsp;';
                    if ($values['permission_status'] == 'not_approved' && Yii::app()->user->role == 1) {
                        $result .= '<li><a href="" id=' . $values['item_id'] . ' class="btn btn-xs btn-default approve_item approveoption_' . $values['item_id'] . '">Rate Approve</a></li>';
                    }
                    $result .= '</ul></div></td></tr>';

                    $result .= '</tr>';
                }

                echo json_encode(array('response' => 'success', 'msg' => 'Purchase item update successfully', 'html' => $result, 'final_amount' => number_format($grand_total, 2, '.', ','), 'discount_total' => number_format($discount_total, 2, '.', ','), 'tax_total' => number_format($tax_amount_total, 2, '.', ','), 'grand_total' => number_format($grand_total_amount, 2, '.', ','), 'mail_send' => $mail_send));
            } else {

                echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
            }
        }
    }

    public function actionRemovepurchaseitem()
    {
        $data = $_REQUEST['data'];
        $grand = $data['grand'];
        $subtot = $data['subtot'];
        $tblpx = Yii::app()->db->tablePrefix;
        $del = Yii::app()->db->createCommand()->delete($tblpx . 'purchase_items', 'item_id=:id', array(':id' => $data['item_id']));
        if ($del) {
            $po_details = Purchase::model()->findByPk($data['purchase_id']);
            $item_amount = Yii::app()->db->createCommand(" SELECT sum(amount) as total_amount FROM {$tblpx}purchase_items  WHERE purchase_id=" . $data['purchase_id'] . "")->queryRow();
            $company_details = Company::model()->findByPk($po_details->company_id);
            if ($company_details->company_popermission == 1) {
                $permission_status = 'No';
            } else {
                if ($company_details->purchaseorder_limit != '') {
                    if ($item_amount['total_amount'] > $company_details->purchaseorder_limit) {
                        $permission_status = 'No';
                    } else {
                        $permission_status = 'Yes';
                    }
                } else {
                    $permission_status = 'Yes';
                }
            }

            Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET total_amount = '" . Controller::money_format_inr($item_amount['total_amount'], 2, 1) . "', sub_total = '" . Controller::money_format_inr($item_amount['total_amount'], 2, 1) . "', permission_status = '" . $permission_status . "' WHERE p_id = '" . $data['purchase_id'] . "'")->execute();

            $result = '';
            $style = "";
            $grand_total = 0;
            $gamount = 0;
            $mail_send = '';
            $permission = PurchaseItems::model()->findAll(array("condition" => "purchase_id=" . $data['purchase_id'] . " AND permission_status=2"));
            if ($permission) {
                Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET 	purchase_status = 'permission_needed' WHERE p_id = '" . $data['purchase_id'] . "'")->execute();
                $model = Purchase::model()->findByPk($data['purchase_id']);
                if ($model->mail_status == 'N' && $model->purchase_status == 'permission_needed') {
                    $mail_send = 'Y';
                } else {
                    $mail_send = 'N';
                }
            } else {
                Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET 	purchase_status = 'draft' WHERE p_id = '" . $data['purchase_id'] . "'")->execute();
                $mail_send = 'N';
            }
            $purchase_details = Yii::app()->db->createCommand(" SELECT * FROM {$tblpx}purchase_items  WHERE purchase_id=" . $data['purchase_id'] . "")->queryAll();
            $discount_total = $tax_amount_total = $grand_total_amount = 0;
            foreach ($purchase_details as $key => $values) {
                $class = "";
                if ($values['permission_status'] == 'not_approved') {
                    $class = "rate_highlight";
                    $style = "style='cursor:pointer'";
                } else {
                    $class = "";
                    $style = "";
                }
                $grand_total += $values['amount'];
                $gamount = number_format($grand_total, 2, '.', '');
                $discount_total += $values['discount_amount'];
                $tax_amount_total += $values['tax_amount'];
                $grand_total_amount += (($values['amount'] + $values['tax_amount']) - $values['discount_amount']);
                if ($values['category_id'] == 0) {

                    $spc_details = $values['remark'];
                    $spc_id = 'other';
                } else {
                    $spec_sql = "SELECT id, cat_id,brand_id, specification, unit FROM {$tblpx}specification WHERE id=" . $values['category_id'] . "";
                    $specification = Yii::app()->db->createCommand($spec_sql)->queryRow();

                    $parent_sql = "SELECT * FROM {$tblpx}purchase_category "
                        . " WHERE id='" . $specification['cat_id'] . "'";
                    $parent_category = Yii::app()->db->createCommand($parent_sql)->queryRow();

                    if ($specification['brand_id'] != NULL) {
                        $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                        $brand = '-' . $brand_details['brand_name'];
                    } else {
                        $brand = '';
                    }

                    $spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
                    $spc_id = $specification['id'];
                }

                $result .= '<tr>';
                $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                $result .= '<td><div class="item_description" id="' . $spc_id . '">' . $spc_details . '</div> </td>';
                $result .= '<td> <div class="unit" id="unit"> ' . $values['hsn_code'] . '</div> </td>';
                $result .= '<td class="text-right"> <div class="" id="quantity"> ' . $values['quantity'] . '</div> </td>';
                $result .= '<td> <div class="unit" id="unit"> ' . $values['unit'] . '</div> </td>';
                $result .= '<td class="text-right ' . $class . '" ' . $style . ' id=' . $values['item_id'] . '><div class="" id="rate">' . number_format($values['rate'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class=""  id="base_qty"> ' . $values['base_qty'] . '</div></td>';
                $result .= '<td class="text-right"><div class=""  id="base_unit"> ' . $values['base_unit'] . '</div></td>';
                $result .= '<td class="text-right ' . $class . '" ' . $style . ' id=' . $values['item_id'] . '><div class="" id="base_rate">' . number_format($values['base_rate'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" > ' . number_format($values['amount'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" > ' . $values['tax_slab'] . '</div></td>';
                $result .= '<td class="text-right"><div class="">' . number_format($values['sgst_amount'], 2) . '</div> </td>';
                $result .= '<td class="text-right"><div class=""> ' . number_format($values['sgst_percentage'], 2, '.', '') . '</div> </td>';
                $result .= '<td class="text-right"><div class="" >' . number_format($values['cgst_amount'], 2) . '</div></td>';
                $result .= '<td class="text-right"><div class="" >' . number_format($values['cgst_percentage'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" >' . number_format($values['igst_amount'], 2) . '</div></td>';
                $result .= '<td class="text-right"><div class="" >' . number_format($values['igst_percentage'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" >' . number_format($values['discount_percentage'], 2, '.', '') . '</div> </td>';
                $result .= '<td class="text-right"><div class="">' . number_format($values['discount_amount'], 2, '.', '') . '</div></td>';

                $result .= '<td class="text-right"><div class="" > ' . number_format($values['tax_amount'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" > ' . number_format((($values['tax_amount'] + $values['amount']) - $values['discount_amount']), 2, '.', '') . '</div></td>';
                $result .= '<td width="70"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                <div class="popover-content hide">
                <ul class="tooltip-hiden">
                <li><a href="#" id=' . $values['item_id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li>
                <li><a href="" id=' . $values['item_id'] . ' class="btn btn-xs btn-default edit_item">Edit</a></li>&nbsp;';
                if ($values['permission_status'] == 'not_approved' && Yii::app()->user->role == 1) {
                    $result .= '<li><a href="" id=' . $values['item_id'] . ' class="btn btn-xs btn-default approve_item approveoption_' . $values['item_id'] . '">Rate Approve</a></li>';
                }
                $result .= '</ul></div></td></tr>';

                $result .= '</tr>';
            }

            echo json_encode(array('response' => 'success', 'msg' => 'Item deleted successfully', 'html' => $result, 'grand_total' => $gamount, 'discount_total' => number_format($discount_total, 2, '.', ''), 'tax_total' => number_format($tax_amount_total, 2, '.', ''), 'grand_total_amount' => number_format($grand_total_amount, 2, '.', ''), 'mail_send' => $mail_send));
        } else {
            echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
        }
    }

    public function actionGetpurchaseItemStatus()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $status = Yii::app()->db->createCommand("SELECT sid, caption  FROM {$tblpx}status WHERE status_type='purchase_item' AND caption ='Pending'")->queryRow();
        return $status;
    }

    public function actionUpdatepurchasestatus()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $billing_status = Yii::app()->db->createCommand("SELECT sid FROM {$tblpx}status WHERE status_type='purchase_bill_status' AND caption='Pending to be Billed'")->queryRow();
        $data = Yii::app()->db->createCommand("SELECT p_id, purchase_status FROM {$tblpx}purchase WHERE p_id='" . $_REQUEST['purchase_id'] . "' ")->queryRow();
        if ($data['purchase_status'] == 'draft') {

            $update = Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET purchase_status = 'saved', purchase_billing_status = " . $billing_status['sid'] . " WHERE p_id = '" . $_REQUEST['purchase_id'] . "'")->execute();
            $pms_api_integration_model = ApiSettings::model()->findByPk(1);
            $pms_api_integration = $pms_api_integration_model->api_integration_settings;
            $mr_status = '';
            if ($pms_api_integration == 1) {
                $p_id = $_REQUEST['purchase_id'];
                $purchase_model = Purchase::model()->findByPk($p_id);
                $mrId = $purchase_model->mr_id;
                //die($mrId);
                if (!empty($mrId)) {
                    $materialRequisition = MaterialRequisition::model()->findByPk($mrId);
                    $pms_mr_id = "";
                    $mr_status = "";
                    if ($materialRequisition) {
                        $request = array();
                        $pms_mr_id = $materialRequisition->pms_mr_id;
                        $sql = "SELECT po_id FROM pms_material_requisition_items WHERE mr_id = :mr_id";
                        $command = Yii::app()->db->createCommand($sql);
                        $command->bindParam(':mr_id', $mrId, PDO::PARAM_INT);
                        $items = $command->queryAll();
                        //echo "<pre>";print_r($items);exit;
                        $allSaved = '1';

                        foreach ($items as $item) {
                            if (empty($item['po_id'])) {
                                $allSaved = '0';
                                break;
                            }
                            if ($allSaved) {
                                // Check the po_status of the current item
                                $sql = "SELECT purchase_status FROM jp_purchase WHERE p_id = :po_id";
                                $command = Yii::app()->db->createCommand($sql);
                                $command->bindParam(':po_id', $item['po_id'], PDO::PARAM_INT);
                                $poStatus = $command->queryScalar();

                                if ($poStatus !== 'saved') {
                                    $allSaved = '0';
                                    break;
                                }
                            }
                        }
                        if ($allSaved == '1') {
                            $mr_status = "4";
                        } else {
                            $mr_status = "3";
                        }
                        // Update the mr_status based on the check
                        $attributes = array('po_id' => $p_id, 'mr_id' => $mrId);
                        $materialRequisition_items = MaterialRequisitionItems::model()->findAllByAttributes($attributes);
                        $template_materials = array();

                        foreach ($materialRequisition_items as $materialRequisition_item) {
                            $pms_template_id = $materialRequisition_item->pms_template_material_id;
                            array_push($template_materials, $pms_template_id);
                        }

                        $request = [
                            'template_materials' => $template_materials,
                            'mr_status' => $mr_status,
                        ];



                    }

                    $slug = "api/mr-approve/" . $pms_mr_id;
                    $method = "POST";

                    $globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
                }

            }
            if ($update) {
                echo json_encode(array('response' => 'success', 'msg' => 'Purchase status changed to saved'));
            }
        } else {
            echo json_encode(array('response' => 'warning', 'msg' => 'Purchase status already updated'));
        }
    }

    public function actionTest()
    {
        echo json_encode('success');
    }

    public function actionPermissionapprove()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $data = Yii::app()->db->createCommand("SELECT permission_status, purchase_id FROM {$tblpx}purchase_items WHERE item_id='" . $_POST['item_id'] . "' ")->queryRow();
        $mail_send = '';
        if ($data['permission_status'] == 'not_approved') {
            $update = Yii::app()->db->createCommand("UPDATE {$tblpx}purchase_items SET permission_status = 1 WHERE item_id = '" . $_POST['item_id'] . "'")->execute();
            if ($update) {
                $permission = PurchaseItems::model()->findAll(array("condition" => "purchase_id=" . $data['purchase_id'] . " AND permission_status=2"));
                if ($permission) {
                    Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET 	purchase_status = 'permission_needed' WHERE p_id = '" . $data['purchase_id'] . "'")->execute();
                    $model = Purchase::model()->findByPk($data['purchase_id']);
                    if ($model->mail_status == 'N' && $model->purchase_status == 'permission_needed') {
                        $mail_send = 'Y';
                    } else {
                        $mail_send = 'N';
                    }
                } else {
                    Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET 	purchase_status = 'draft' WHERE p_id = '" . $data['purchase_id'] . "'")->execute();
                    $mail_send = 'N';
                }
                echo json_encode(array('response' => 'success', 'msg' => 'Permission approved Successfully ', 'mail_send' => $mail_send));
            } else {
                echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
            }
        } else {
            echo json_encode(array('response' => 'warning', 'msg' => 'Permission already approved'));
        }
    }

    public function actionPreviousratedetails()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $html = '';
        $html['html'] = '';
        $html['status'] = '';
        $html['unit'] = '';

        $item_data = Yii::app()->db->createCommand("SELECT category_id FROM {$tblpx}purchase_items WHERE item_id=" . $_GET['item_id'] . "")->queryRow();
        $category_id = $item_data['category_id'];

        if ($item_data) {
            $spec_sql = "SELECT id, cat_id, brand_id, specification, unit "
                . " FROM {$tblpx}specification "
                . " WHERE id=" . $category_id . "";
            $specification = Yii::app()->db->createCommand($spec_sql)->queryRow();

            if ($specification['brand_id'] != NULL) {
                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                $brand = '-' . ucwords($brand_details['brand_name']);
            } else {
                $brand = '';
            }

            $cat_sql = "SELECT category_name "
                . " FROM {$tblpx}purchase_category "
                . " WHERE id=" . $specification['cat_id'] . "";
            $category = Yii::app()->db->createCommand($cat_sql)->queryRow();
            // $unit = Yii::app()->db->createCommand("SELECT id, unit_name FROM {$tblpx}unit WHERE id=" . $specification['unit'] . "")->queryRow();

            $data = Yii::app()->db->createCommand("SELECT {$tblpx}bills.bill_number, {$tblpx}bills.bill_date, {$tblpx}billitem.billitem_quantity, {$tblpx}billitem.billitem_unit, {$tblpx}billitem.billitem_rate, {$tblpx}billitem.billitem_amount, {$tblpx}purchase.purchase_no, {$tblpx}purchase.vendor_id,{$tblpx}purchase.project_id FROM  {$tblpx}billitem LEFT JOIN {$tblpx}bills  ON {$tblpx}bills.bill_id={$tblpx}billitem.bill_id LEFT JOIN {$tblpx}purchase_items ON {$tblpx}billitem.purchaseitem_id={$tblpx}purchase_items.item_id LEFT JOIN {$tblpx}purchase ON {$tblpx}purchase_items.purchase_id= {$tblpx}purchase.p_id WHERE {$tblpx}billitem.category_id=" . $category_id . " ORDER BY {$tblpx}billitem.billitem_id desc LIMIT 0,4")->queryAll();

            if (!empty($data)) {
                $html['html'] .= '<div class="alert alert-success get_prevdata alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <div id="parent2">
                        <table style="width:100%;" id="pre_fixtable2">
                            <thead class="entry-table">
                            <tr>
                                <th style="text-align:center">Sl.No</th>
                                <th style="text-align:center">Project</th>
                                <th style="text-align:center">Bill No</th>
                                <th style="text-align:center">Purchase No</th>
                                <th style="text-align:center">Bill Date</th>
                                <th style="text-align:center">Vendor</th>
                                <th style="text-align:center">Quantity</th>
                                <th style="text-align:center">Unit</th>
                                <th style="text-align:center">Rate</th>
                                <th style="text-align:center">Amount</th>
                            </tr>
                            <thead>
                        <tbody><tr><td colspan="9"><p>' . ucwords($category['category_name']) . $brand . ' - ' . ucwords($specification['specification']) . ' Previous "' . count($data) . '" ​Purchase Rate</p></td></tr>			';
                foreach ($data as $key => $value) {
                    $project = Projects::model()->findByPk($value['project_id']);
                    $vendor = Vendors::model()->findByPk($value['vendor_id']);
                    $html['html'] .= '<tr><td style="text-align:center;">' . ($key + 1) . ' </td><td style="text-align:center;">' . (isset($project['name']) ? $project['name'] : '') . '</td><td style="text-align:center;">' . $value['bill_number'] . '</td><td style="text-align:center;">' . $value['purchase_no'] . '</td><td style="text-align:center">' . date("Y-m-d", strtotime($value['bill_date'])) . '</td><td style="text-align:center;">' . (isset($vendor['name']) ? $vendor['name'] : '') . '</td><td style="text-align:center">' . $value['billitem_quantity'] . '</td><td style="text-align:center">' . $value['billitem_unit'] . '</td><td  style="text-align:center;">' . $value['billitem_rate'] . '</td><td style="text-align:center;">' . $value['billitem_amount'] . '</td></tr>';
                }
                $html['html'] .= '</tbody></table></div></div>';
                $html['status'] .= true;
            } else {
                $html['html'] .= '';
                $html['status'] .= false;
            }
            if (!empty($specification)) {
                $html['unit'] .= $specification['unit'];
            } else {
                $html['unit'] .= '';
            }
        } else {

            $html['html'] .= '';
            $html['status'] .= false;
        }
        echo json_encode($html);
    }

    public function actionDynamicvendor()
    {
        $html['html'] = '';
        $expId = $_POST["exp_id"];
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', v.company_id)";
        }
        $vendorData = Yii::app()->db->createCommand("SELECT v.vendor_id as vendorid, v.name as vendorname FROM " . $tblpx . "vendors v LEFT JOIN " . $tblpx . "vendor_exptype vet ON v.vendor_id = vet.vendor_id WHERE vet.type_id = " . $expId . " AND ($newQuery)")->queryAll();
        $html['html'] .= "<option value=''>Choose Vendor</option>";
        if (!empty($vendorData)) {
            foreach ($vendorData as $vData) {
                $selected = '';
                if (isset($_REQUEST['vendor']) && $_REQUEST['vendor'] != "") {
                    $selected = ($_REQUEST['vendor'] == $vData["vendorid"]) ? ' selected=selected' : '';
                } else {
                    $selected = '';
                    if (isset($_REQUEST['popup'])) {
                        $sql = "SELECT `vendor_id` FROM {$tblpx}vendors "
                            . " ORDER BY vendor_id DESC LIMIT 1";
                        $id = Yii::app()->db->createCommand($sql)->queryScalar();
                        $selected = ($id == $vData["vendorid"]) ? ' selected=selected' : '';
                    }
                }
                $html['html'] .= "<option value='" . $vData["vendorid"] . "' '" . $selected . "'>" . $vData["vendorname"] . "</option>";
            }
        }
        echo json_encode($html);
    }

    public function actionDynamicexpensehead()
    {
        $project_id = $_POST['project_id'];
        $html['html'] = '';
        $html['status'] = '';
        $html['address'] = '';
        $html['msg'] = "";
        if ($project_id != "") {
            $project = Projects::model()->findByPk($project_id);
            $site = $project->site;
            $msg = "";
            if ($project->project_status == '85') {
                $msg = 'Project ' . $project->name . ' is already completed';
            }
            $tblpx = Yii::app()->db->tablePrefix;
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}expense_type.company_id)";
            }

            $html['msg'] = $msg;
            if ($project->auto_update) {
                // $sql = "SELECT * FROM {$tblpx}expense_type "
                //         . " ORDER BY type_name";
                $sql = "SELECT * FROM {$tblpx}project_exptype "
                        . "LEFT JOIN {$tblpx}expense_type "
                        . " ON {$tblpx}project_exptype.type_id={$tblpx}expense_type.type_id "
                        . " WHERE {$tblpx}project_exptype.project_id=" . $project_id;
                $expense_type = Yii::app()->db->createCommand($sql)->queryAll();
            } else {
                $sql = "SELECT * FROM {$tblpx}project_exptype "
                    . "LEFT JOIN {$tblpx}expense_type "
                    . " ON {$tblpx}project_exptype.type_id={$tblpx}expense_type.type_id "
                    . " WHERE {$tblpx}project_exptype.project_id=" . $project_id;
                $expense_type = Yii::app()->db->createCommand($sql)->queryAll();
            }
            $html['address'] = $site;
            if (!empty($expense_type)) {
                $html['html'] .= '<option value="">Choose Expense Head</option>';
                foreach ($expense_type as $key => $value) {
                    $selected = '';
                    if (isset($_REQUEST['expense_head']) && $_REQUEST['expense_head'] != "") {
                        $selected = ($_REQUEST['expense_head'] == $value['type_id']) ? ' selected=selected' : '';
                    } else {
                        $selected = '';
                        if (isset($_REQUEST['popup'])) {
                            $sql = "SELECT `type_id` FROM {$tblpx}expense_type "
                                . " ORDER BY type_id DESC LIMIT 1";
                            $id = Yii::app()->db->createCommand($sql)->queryScalar();
                            $selected = ($id == $value['type_id']) ? ' selected=selected' : '';
                        }
                    }
                    $html['html'] .= '<option value="' . $value['type_id'] . '" "' . $selected . '">' . $value['type_name'] . '</option>';
                    $html['status'] = 'success';
                }
            } else {
                $html['status'] = 'no_success';
                $html['html'] .= '<option value="">Choose Expense Head</option>';
            }
        }
        echo json_encode($html);
    }

    // send mail
    private function sendmail($purchase_id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $purchase = Purchase::model()->findByPk($purchase_id);
        $mail_array = array();
        if ($purchase['mail_status'] == 'N') {
            $project = Projects::model()->findByPk($purchase['project_id']);
            $expense_type = ExpenseType::model()->findByPk($purchase['expensehead_id']);
            $vendor = Vendors::model()->findByPk($purchase['vendor_id']);
            $mail = new JPhpMailer();
            $compamy = Company::model()->findByPk($purchase->company_id);
            $usere_mail = Yii::app()->db->createCommand("SELECT email FROM {$tblpx}users WHERE userid IN (" . $compamy['po_email_userid'] . ")")->queryAll();
            $useremail = array_map('array_filter', $usere_mail);
            $useremail = array_filter($useremail);
            $subject = "" . Yii::app()->name . ": Purchase Permission Needed";
            $headers = "" . Yii::app()->name . "";
            $bodyContent = "<p>Hi</p><p>Permission Needed, Please find the details.</p><p>Project : " . $project['name'] . "</p><p>Expense Head : " . $expense_type['type_name'] . "</p><p>Vendor : " . $vendor['name'] . "</p><p>Purchase NO : " . $purchase['purchase_no'] . "</p><br><br><p>Regards,</p><p>" . Yii::app()->name . "</p>";
            $server = (($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == 'bhi.localhost.com') ? '0' : '1');
            if ($server == 0) {
                $mail->IsSMTP();
                $mail->Host = SMTPHOST;
                $mail->SMTPSecure = SMTPSECURE;
                $mail->SMTPAuth = true;
                $mail->Username = SMTPUSERNAME;
                $mail->Password = SMTPPASS;
            }
            $newdate = date('Y-m-d');
            $mail->setFrom(EMAILFROM, Yii::app()->name);
            if (!empty($useremail)) {
                foreach ($useremail as $key => $value) {
                    if ($value['email'] != '') {
                        $mail->addAddress($value['email']);
                        array_push($mail_array, '1');
                    }
                }
            } else {
                $user = Users::model()->findByPk(Yii::app()->user->id);
                if ($user['email'] != '') {
                    array_push($mail_array, '1');
                    $mail->addAddress($user['email']);
                } else {
                    array_push($mail_array, '0');
                }
            }
            $mail->isHTML(true);
            $mail->Subject = "" . Yii::app()->name . " : Purchase Permission Needed";
            $mail->Body = $bodyContent;
            if (in_array("1", $mail_array)) {
                if ($mail->Send()) {
                    $update = Yii::app()->db->createCommand()->update("" . $tblpx . "purchase", array('mail_status' => "Y"), 'p_id=:p_id', array(':p_id' => $purchase_id));
                    echo json_encode('success');
                } else {
                    echo json_encode('error');
                }
            } else {
                echo json_encode('warning');
            }
        }
    }

    public function actiontestransaction()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $html = '';
        $other = '';
        if (!isset($_POST['searchTerm'])) {
            $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
            $other = array("id" => 'other', "text" => 'Other');
        } else {
            $search = $_POST['searchTerm'];
            $length = strlen($search);
            $specification = $this->actionGetItemCategorySearch($parent = 0, $spacing = '', $user_tree_array = '', $search);
            if ($search != '' && $length >= 3) {
                if ($search == 'other') {
                    $other = array("id" => 'other', "text" => 'Other');
                } else {
                    $other = '';
                }
                $html = array(
                    'html' => '',
                    'status' => '',
                    'unit' => '',
                    'units' => '',
                    'hsn_code' => ''
                );

                if (!empty($specification)) {
                    $i = 1;
                    foreach ($specification as $key => $value) {
                        $spec_sql = "SELECT id, cat_id, brand_id, specification, unit "
                            . " FROM {$tblpx}specification "
                            . " WHERE id=" . $value['id'] . " ";
                        $specificationd = Yii::app()->db->createCommand($spec_sql)->queryRow();

                        if ($specificationd['brand_id'] != NULL) {
                            $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specificationd['brand_id'] . "")->queryRow();
                            $brand = '-' . ucwords($brand_details['brand_name']);
                        } else {
                            $brand = '';
                        }
                        $category = Yii::app()->db->createCommand("SELECT category_name FROM {$tblpx}purchase_category WHERE id=" . $specificationd['cat_id'] . "")->queryRow();

                        $hsn_code = Yii::app()->db->createCommand("SELECT hsn_code FROM {$tblpx}specification WHERE id=" . $specificationd['id'] . "")->queryRow();

                        $data = Yii::app()->db->createCommand("SELECT {$tblpx}bills.bill_number, {$tblpx}bills.bill_date, {$tblpx}billitem.billitem_quantity, {$tblpx}billitem.billitem_unit, {$tblpx}billitem.billitem_rate, {$tblpx}billitem.billitem_amount, {$tblpx}purchase.purchase_no, {$tblpx}purchase.vendor_id, {$tblpx}purchase.project_id FROM  {$tblpx}billitem LEFT JOIN {$tblpx}bills  ON {$tblpx}bills.bill_id={$tblpx}billitem.bill_id LEFT JOIN {$tblpx}purchase_items ON {$tblpx}billitem.purchaseitem_id={$tblpx}purchase_items.item_id LEFT JOIN {$tblpx}purchase ON {$tblpx}purchase_items.purchase_id= {$tblpx}purchase.p_id WHERE {$tblpx}billitem.category_id=" . $value['id'] . " AND ({$tblpx}purchase.p_id IS NOT NULL 
                        OR {$tblpx}purchase.p_id != ' ') AND {$tblpx}purchase.purchase_type ='O' ORDER BY {$tblpx}billitem.billitem_id desc LIMIT 0,4")->queryAll();
                        if (!empty($data)) {
                            if ($i == 1) {
                                $html['html'] .= '<div class="alert alert-success get_prevdata alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
                                $html['html'] .= '<div id="parent3">
                                <table style="width:100%;" id="pre_fixtable3">
                                <thead>
                                <tr>
                                    <th style="text-align:center">Sl.No</th>
                                    <th style="text-align:center">Project</th>
                                    <th style="text-align:center">Bill No</th>
                                    <th style="text-align:center">Purchase No</th>
                                    <th style="text-align:center">Bill Date</th>
                                    <th style="text-align:center">Vendor</th>
                                    <th style="text-align:center">Quantity</th>
                                    <th style="text-align:center">Unit</th>
                                    <th style="text-align:center">Rate</th>
                                    <th style="text-align:center">Amount</th>
                                </tr>
                                <thead>
                                                                                                ';
                            }
                            $html['html'] .= '<tbody><tr><td colspan="9"><p>' . ucwords($category['category_name']) . $brand . '-' . ucwords($specificationd['specification']) . ' Previous "' . count($data) . '" ​Purchase Rate</p></td></tr>
								';
                            foreach ($data as $key => $value) {
                                $vendor = Vendors::model()->findByPk($value['vendor_id']);
                                $project = Projects::model()->findByPk($value['project_id']);
                                $html['html'] .= '<tr class="getprevious" style="cursor:pointer;" data-id="' . $specificationd['id'] . ',' . $value['billitem_quantity'] . ',' . $value['billitem_unit'] . ',' . $value['billitem_rate'] . ',' . $value['billitem_amount'] . '"><td style="text-align:center;">' . ($key + 1) . ' </td><td style="text-align:center;">' . (isset($project['name']) ? $project['name'] : '') . '</td><td style="text-align:center;">' . $value['bill_number'] . '</td><td style="text-align:center;">' . $value['purchase_no'] . '</td><td style="text-align:center;">' . date("Y-m-d", strtotime($value['bill_date'])) . '</td><td style="text-align:center;">' . (isset($vendor['name']) ? $vendor['name'] : '') . '</td><td style="text-align:center;">' . $value['billitem_quantity'] . '</td><td style="text-align:center;">' . $value['billitem_unit'] . '</td><td  style="text-align:center;">' . $value['billitem_rate'] . '</td><td style="text-align:center;">' . $value['billitem_amount'] . '</td></tr>';
                            }

                            $html['status'] .= true;
                            $i++;
                        } else {
                            $html['html'] .= '';
                            $html['status'] .= false;
                        }
                        if (!empty($specificationd)) {
                            $base_unit = $specificationd['unit'];
                        } else {
                            $base_unit = '';
                        }

                        $html['base_unit'] = $base_unit;
                        if ($base_unit != '') {
                            $uc_item_count = UnitConversion::model()->countByAttributes(array(
                                'item_id' => $specificationd['id'],
                            ));
                            if ($uc_item_count > 1) {
                                $html['units'] = $this->GetItemunitlist($specificationd['id']);
                                $html['unit'] = $html['units'];
                            } else {
                                if (!empty($specificationd['unit'])) {
                                    $html['unit'] .= $base_unit;
                                    $html['units'] .= $base_unit;
                                } else {
                                    $html['unit'] .= '';
                                    $html['units'] = 'Unit';
                                }
                            }
                        } else {
                            $html['unit'] .= '';
                            $html['units'] = 'Unit';
                        }
                        if (!empty($hsn_code)) {
                            $html['hsn_code'] .= $hsn_code['hsn_code'];
                        } else {
                            $html['hsn_code'] .= '';
                        }
                    }
                    $html['html'] .= '</tbody></table></div>';
                    $html['html'] .= '</div>';
                }
                $html = $html['html'];
            } else {
                $html = '';
                $other = array("id" => 'other', "text" => 'Other');
            }
        }
        $data = array();
        foreach ($specification as $key => $value) {
            $data[] = array("id" => $value['id'], "text" => $value['data']);
        }
        $data[] = $other;

        echo json_encode(array('data' => $data, 'html' => $html));
    }

    public function actionGetItemCategorySearch($parent = 0, $spacing = '', $user_tree_array = '', $search)
    {
        if (!is_array($user_tree_array))
            $user_tree_array = array();
        $data = array();
        $final = array();
        $tblpx = Yii::app()->db->tablePrefix;
        $where = '';
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }


        $spec_sql = "SELECT id, specification, brand_id, cat_id "
            . " FROM {$tblpx}specification "
            . " WHERE  (" . $newQuery . ") "
            . " AND specification LIKE '%" . $search . "%' " . $where . " "
            . " AND specification_type='O' AND spec_status='1'";
        $specification = Yii::app()->db->createCommand($spec_sql)->queryAll();

        foreach ($specification as $key => $value) {
            if ($value['brand_id'] != NULL) {
                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $value['brand_id'] . "")->queryRow();
                $brand = '-' . ucwords($brand_details['brand_name']);
            } else {
                $brand = '';
            }
            $result = $this->actionGetParent($value['cat_id']);
            $final[$key]['data'] = ucwords($result['category_name']) . $brand . '-' . ucwords($value['specification']);
            $final[$key]['id'] = $value['id'];
        }
        return $final;
    }

    public function actionGetunits()
    {
        $id = $_POST['id'];
        $tblpx = Yii::app()->db->tablePrefix;
        $spec_sql = "SELECT id, cat_id, brand_id, specification, unit"
            . "  FROM {$tblpx}specification "
            . " WHERE id=" . $id . "";
        $specificationd = Yii::app()->db->createCommand($spec_sql)->queryRow();
        // $unit = Yii::app()->db->createCommand("SELECT id, unit_name FROM {$tblpx}unit WHERE id=" . $specificationd['unit'] . "")->queryRow();
        if (!empty($specificationd)) {
            $unit = $specificationd['unit'];
        } else {
            $unit = '';
        }
        echo json_encode($unit);
    }

    public function actionPermissionmail()
    {
        $purchase_id = $_REQUEST['purchase_id'];
        $this->sendmail($purchase_id);
    }

    public function actionPreviouspurchase()
    {
        $date_from = '';
        $date_to = '';
        $item_id = '';
        $vendor_id = '';
        $data = array();
        $where = 'WHERE 1=1';
        $tblpx = Yii::app()->db->tablePrefix;
        if ((isset($_GET['item_id']) && $_GET['item_id'] != '') || (isset($_GET['vendor_id']) && $_GET['vendor_id'] != '') || (isset($_GET['date_from']) && $_GET['date_from'] != '') || (isset($_GET['date_to']) && $_GET['date_to'] != '')) {
            $item_id = $_GET['item_id'];
            $vendor_id = $_GET['vendor_id'];

            if ($_GET['date_from'] != '' && $_GET['date_to'] != '') {
                $date_from = date('Y-m-d', strtotime($_GET['date_from']));
                $date_to = date('Y-m-d', strtotime($_GET['date_to']));
                $where .= " AND {$tblpx}bills.bill_date between '" . $date_from . "' and'" . $date_to . "'";
            } else {
                if ($_GET['date_from'] != '') {
                    $date_from = date('Y-m-d', strtotime($_GET['date_from']));
                    $where .= " AND {$tblpx}bills.bill_date >= '" . $date_from . "'";
                }
                if ($_GET['date_to'] != '') {
                    $date_to = date('Y-m-d', strtotime($_POST['date_to']));
                    $where .= " AND {$tblpx}bills.bill_date <= '" . $date_to . "'";
                }
            }

            if (!empty($vendor_id)) {
                $where .= " AND {$tblpx}purchase.vendor_id = " . $vendor_id . "";
            }

            if (!empty($item_id)) {
                $where .= " AND {$tblpx}billitem.category_id = " . $item_id . "";
            }
            $data = Yii::app()->db->createCommand("SELECT {$tblpx}billitem.billitem_id,{$tblpx}bills.bill_number, {$tblpx}bills.bill_date, {$tblpx}billitem.billitem_quantity, {$tblpx}billitem.billitem_unit, {$tblpx}billitem.billitem_rate, {$tblpx}billitem.billitem_amount, {$tblpx}purchase.purchase_no, {$tblpx}billitem.billitem_taxamount, {$tblpx}billitem.billitem_discountamount, {$tblpx}purchase.vendor_id, {$tblpx}billitem.category_id,{$tblpx}bills.bill_id,{$tblpx}purchase.p_id FROM  {$tblpx}billitem LEFT JOIN {$tblpx}bills  ON {$tblpx}bills.bill_id={$tblpx}billitem.bill_id LEFT JOIN {$tblpx}purchase_items ON {$tblpx}billitem.purchaseitem_id={$tblpx}purchase_items.item_id LEFT JOIN {$tblpx}purchase ON {$tblpx}bills.purchase_id= {$tblpx}purchase.p_id " . $where . " ORDER BY {$tblpx}billitem.billitem_amount asc")->queryAll();
        }

        $dataprovider = new CArrayDataProvider($data, array(
            'id' => 'user',
            'keyField' => 'billitem_id',
            'pagination' => false,
        ));

        $items = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
        $this->render('previous_purchase', array(
            'date_from' => $date_from,
            'date_to' => $date_to,
            'item_id' => $item_id,
            'items' => $items,
            'vendor_id' => $vendor_id,
            'dataprovider' => $dataprovider
        ));
    }

    public function actionpreviouspurchasepdf($date_from, $date_to, $item_id, $vendor_id)
    {
        $this->logo = $this->realpath_logo;
        $data = array();
        $where = 'WHERE 1=1';
        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($date_from) || isset($date_to) || isset($item_id) || isset($vendor_id)) {
            if ($date_from != '' && $date_to != '') {
                $date_from = date('Y-m-d', strtotime($date_from));
                $date_to = date('Y-m-d', strtotime($date_to));
                $where .= " AND {$tblpx}bills.bill_date between '" . $date_from . "' and'" . $date_to . "'";
            } else {
                if ($date_from != '') {
                    $date_from = date('Y-m-d', strtotime($date_from));
                    $where .= " AND {$tblpx}bills.bill_date >= '" . $date_from . "'";
                }
                if ($date_to != '') {
                    $date_to = date('Y-m-d', strtotime($date_to));
                    $where .= " AND {$tblpx}bills.bill_date <= '" . $date_to . "'";
                }
            }
            if (!empty($vendor_id)) {
                $where .= " AND {$tblpx}purchase.vendor_id = " . $vendor_id . "";
            }
            if (!empty($item_id)) {
                $where .= " AND {$tblpx}billitem.category_id = " . $item_id . "";
            }
            $data = Yii::app()->db->createCommand("SELECT {$tblpx}billitem.billitem_id,{$tblpx}bills.bill_number, {$tblpx}bills.bill_date, {$tblpx}billitem.billitem_quantity, {$tblpx}billitem.billitem_unit, {$tblpx}billitem.billitem_rate, {$tblpx}billitem.billitem_amount, {$tblpx}purchase.purchase_no, {$tblpx}billitem.billitem_taxamount, {$tblpx}billitem.billitem_discountamount, {$tblpx}purchase.vendor_id, {$tblpx}billitem.category_id,{$tblpx}bills.bill_id,{$tblpx}purchase.p_id FROM  {$tblpx}billitem LEFT JOIN {$tblpx}bills  ON {$tblpx}bills.bill_id={$tblpx}billitem.bill_id LEFT JOIN {$tblpx}purchase_items ON {$tblpx}billitem.purchaseitem_id={$tblpx}purchase_items.item_id LEFT JOIN {$tblpx}purchase ON {$tblpx}bills.purchase_id= {$tblpx}purchase.p_id " . $where . " ORDER BY {$tblpx}billitem.billitem_amount asc")->queryAll();
        }

        $mPDF1 = Yii::app()->ePdf->mPDF();

        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');

        $sql = 'SELECT template_name '
            . 'FROM jp_quotation_template '
            . 'WHERE status="1"';
        $selectedtemplate = Yii::app()->db->createCommand($sql)->queryScalar();

        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $mPDF1->AddPage('', '', '', '', '', 0, 0, 40, 50, 10, 0);

        $mPDF1->WriteHTML($this->renderPartial('previouspurchase_pdf', array('date_from' => $date_from, 'date_to' => $date_to, 'item_id' => $item_id, 'items' => $items, 'vendor_id' => $vendor_id, 'result' => $data), true));

        $mPDF1->Output('Previous_purchase.pdf', 'D');
    }

    public function actionpreviouspurchasexcel($date_from, $date_to, $item_id, $vendor_id)
    {
        $result = array();
        $where = 'WHERE 1=1';
        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($date_from) || isset($date_to) || isset($item_id) || isset($vendor_id)) {
            if ($date_from != '' && $date_to != '') {
                $date_from = date('Y-m-d', strtotime($date_from));
                $date_to = date('Y-m-d', strtotime($date_to));
                $where .= " AND {$tblpx}bills.bill_date between '" . $date_from . "' and'" . $date_to . "'";
            } else {
                if ($date_from != '') {
                    $date_from = date('Y-m-d', strtotime($date_from));
                    $where .= " AND {$tblpx}bills.bill_date >= '" . $date_from . "'";
                }
                if ($date_to != '') {
                    $date_to = date('Y-m-d', strtotime($date_to));
                    $where .= " AND {$tblpx}bills.bill_date <= '" . $date_to . "'";
                }
            }
            if (!empty($vendor_id)) {
                $where .= " AND {$tblpx}purchase.vendor_id = " . $vendor_id . "";
            }
            if (!empty($item_id)) {
                $where .= " AND {$tblpx}billitem.category_id = " . $item_id . "";
            }
            $result = Yii::app()->db->createCommand("SELECT {$tblpx}billitem.billitem_id,{$tblpx}bills.bill_number, {$tblpx}bills.bill_date, {$tblpx}billitem.billitem_quantity, {$tblpx}billitem.billitem_unit, {$tblpx}billitem.billitem_rate, {$tblpx}billitem.billitem_amount, {$tblpx}purchase.purchase_no, {$tblpx}billitem.billitem_taxamount, {$tblpx}billitem.billitem_discountamount, {$tblpx}purchase.vendor_id, {$tblpx}billitem.category_id,{$tblpx}bills.bill_id,{$tblpx}purchase.p_id FROM  {$tblpx}billitem LEFT JOIN {$tblpx}bills  ON {$tblpx}bills.bill_id={$tblpx}billitem.bill_id LEFT JOIN {$tblpx}purchase_items ON {$tblpx}billitem.purchaseitem_id={$tblpx}purchase_items.item_id LEFT JOIN {$tblpx}purchase ON {$tblpx}bills.purchase_id= {$tblpx}purchase.p_id " . $where . " ORDER BY {$tblpx}billitem.billitem_amount asc")->queryAll();
        }

        $arraylabel = array('Bill No', 'Purchase No', 'Bill date', 'Vendor', 'Item', 'Quantity', 'Rate', 'Amount', 'Total Amount');
        $finaldata = array();
        $finaldata[0][] = '';
        $finaldata[0][] = '';
        $finaldata[0][] = '';
        $finaldata[0][] = '';
        $finaldata[0][] = '';
        $finaldata[0][] = '';
        $finaldata[0][] = '';
        $i = 1;
        foreach ($result as $key => $data) {
            $vendor = Vendors::model()->findByPk($data['vendor_id']);
            if ($data['category_id'] != NULL) {
                $spec_sql = "SELECT id, cat_id, brand_id, specification, unit"
                    . " FROM {$tblpx}specification"
                    . "  WHERE id=" . $data['category_id'] . "";
                $specification = Yii::app()->db->createCommand($spec_sql)->queryRow();
                if ($specification['brand_id'] != NULL) {
                    $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                    $brand = '-' . ucwords($brand_details['brand_name']);
                } else {
                    $brand = '';
                }
                $cat_sql = "SELECT category_name "
                    . " FROM {$tblpx}purchase_category "
                    . " WHERE id=" . $specification['cat_id'] . "";
                $category = Yii::app()->db->createCommand($cat_sql)->queryRow();
            }
            $finaldata[$i][] = $data['bill_number'];
            $finaldata[$i][] = $data['purchase_no'];
            $finaldata[$i][] = date('Y-m-d', strtotime($data['bill_date']));
            $finaldata[$i][] = $vendor['name'];
            if ($data['category_id'] != NULL) {
                $finaldata[$i][] = ucwords($category['category_name']) . $brand . '-' . ucwords($specification['specification']);
            } else {
                $finaldata[$i][] = '';
            }
            $finaldata[$i][] = $data['billitem_quantity'];
            $finaldata[$i][] = Controller::money_format_inr($data['billitem_rate'], 2);
            $finaldata[$i][] = Controller::money_format_inr($data['billitem_amount'], 2);
            $finaldata[$i][] = Controller::money_format_inr(($data['billitem_amount'] + $data['billitem_taxamount']) - $data['billitem_discountamount'], 2);
            $i++;
        }



        Yii::import('ext.ECSVExport');
        $csv = new ECSVExport($finaldata);
        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Previous_purchase' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actionpermission()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $data = Yii::app()->db->createCommand("SELECT p_id, permission_status FROM {$tblpx}purchase WHERE p_id='" . $_POST['item_id'] . "' ")->queryRow();
        if ($data['permission_status'] == 'No') {
            $update = Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET permission_status = 'Yes' WHERE p_id = '" . $_POST['item_id'] . "'")->execute();
            if ($update) {
                echo json_encode(array('response' => 'success', 'msg' => 'Permission approved Successfully '));
            } else {
                echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
            }
        } else {
            echo json_encode(array('response' => 'warning', 'msg' => 'Permission already approved'));
        }
    }

    public function actionsendattachment()
    {
        $p_id = $_POST['p_id'];
        $mail_to = $_POST['mail_to'];
        $mail_content = $_POST['mail_content'];
        $path = $_POST['pdf_path'];
        $name = $_POST['pdf_name'];
        $mail = new JPhpMailer();
        $subject = "" . Yii::app()->name . ": Purchase Order Email";
        $headers = "" . Yii::app()->name . "";
        $bodyContent = "<p>Hi</p><p>" . $mail_content . "</p><p>Please find the attachment</p>";
        $server = (($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == 'bhi.localhost.com') ? '0' : '1');
        $newdate = date('Y-m-d');
        $mail->setFrom(EMAILFROM, Yii::app()->name);
        $allfiles = glob(Yii::getPathOfAlias('webroot') . '/documents/*');
        $mailSendData = array();
        $mailSendData['email_from_name'] = Yii::app()->name;
        $mailSendData['mail_to'] = $mail_to;
        $mailSendData['subject'] = Yii::app()->name . " : Purchase Order Email";
        $mailSendData['send_copy_status'] = $_POST['send_copy'];
        if (file_exists($path)) {
            $mailSendData['attachment'][] = array($path, $name);
        }
        $mailSendData['message'] = $bodyContent;
        $mail_send_status = $this->sendSMTPMail($mailSendData);
        if ($mail_send_status['status'] == 1) {
            foreach ($allfiles as $file) {
                if (is_file($file))
                    unlink($file);
            }
        }
        echo json_encode($mail_send_status);
    }

    public function actionpdfgeneration()
    {
        $this->logo = $this->realpath_logo;
        $p_id = $_POST['p_id'];
        $model = Purchase::model()->findByPk($p_id);
        $item_model = PurchaseItems::model()->findAll(array("condition" => "purchase_id = " . $p_id . ""));
        $purchase_items = $this->setPurchaseItems($item_model);
        if (!empty($model)) {
            $vendor = Vendors::model()->findByPK($model->vendor_id);
            $project = Projects::model()->findByPK($model->project_id);
            $expense_head = ExpenseType::model()->findByPK($model->expensehead_id);
        }
        $render_datas = array(
            'model' => $model,
            'itemmodel' => $purchase_items,
            'project' => $project,
            'vendor' => $vendor,
            'expense_head' => $expense_head
        );
        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $activeProjectTemplate = $this->getActiveTemplate();

        if ($selectedtemplate == 'template1' && $activeProjectTemplate == 'TYPE-6') {
            $mPDF1->AddPage('', '', '', '', '', 0, 0, 35, 15, 10, 0);
        } else {
            if ($selectedtemplate == 'template1') {
                $mPDF1->AddPage('', '', '', '', '', 0, 0, 50, 45, 10, 0);
            } else {
                $mPDF1->AddPage('', '', '', '', '', 0, 0, 40, 45, 10, 0);
            }
        }
        $mPDF1->WriteHTML($this->renderPartial('createpdf', $render_datas, true));
        $newdate = date('Y-m-d');
        $path = Yii::getPathOfAlias('webroot') . '/documents/Purchase_' . $newdate . '.pdf';
        $mPDF1->Output($path, 'F');
        echo json_encode(array('path' => $path, 'name' => 'Purchase_' . $newdate . '.pdf'));
    }

    public function actiondynamicproject()
    {
        $company_id = $_POST['company_id'];
        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($_POST['company_id'])) {

            $user = Users::model()->findByPk(Yii::app()->user->id);
            $company = Company::model()->findByPk($company_id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}expense_type.company_id)";
            }
            if (!empty($company->purchase_order_format)) {
                $html['auto_pono'] = 2;
                $purchase_order_format = trim($company["purchase_order_format"]);
                $general_po_format_arr = explode('/', $purchase_order_format);
                // print_r($general_po_format_arr);exit;
                $org_prefix = $general_po_format_arr[0];
                $prefix_key = 0;
                $po_key = '';
                $current_key = '';
                foreach ($general_po_format_arr as $key => $value) {

                    if ($value == '{po_sequence}') {
                        $po_key = $key;
                    } else if ($value == '{year}') {
                        $current_key = $key;
                    }
                }

                $current_year = date('Y');
                $previous_year = date('Y') - 1;
                $next_year = date('Y') + 1;

                $where = '';
                if ($current_key == 2) {
                    $where .= " AND `purchase_no` LIKE '%$current_year'";
                } else if ($current_key == 1) {
                    $where .= " AND `purchase_no` LIKE '%$current_year%'";
                }
                $sql = "SELECT * from " . $tblpx . "purchase  WHERE `purchase_no` LIKE '$org_prefix%' $where ORDER BY `p_id` DESC LIMIT 1;";
                //echo "<pre>";print_r($sql);exit;
                $previous_po_no = '';
                $po_seq_no = 1;
                $po_seq_prefix = 'PO-';
                $previous_data = Yii::app()->db->createCommand($sql)->queryRow();
                if (!empty($previous_data)) {
                    $previous_po_no = $previous_data['purchase_no'];
                    $prefix_arr = explode('/', $previous_po_no);
                    $prefix = $prefix_arr[0];
                    $previous_prefix_seq_no = $prefix_arr[$po_key];
                    $po_split = explode('-', $previous_prefix_seq_no);
                    $previous_prefix_seq_no_val = $po_split[1];
                    $po_seq_no = $previous_prefix_seq_no_val + 1;

                }
                $digit = strlen((string) $po_seq_no);
                if ($this->getActiveTemplate() == 'TYPE-4' && ENV == 'production') {
                    $po_no = $this->getArchInvoiceNo();

                } else {
                    if ($digit == 1) {
                        $po_no = $po_seq_prefix . '00' . $po_seq_no;
                    } else if ($digit == 2) {
                        $po_no = $po_seq_prefix . '0' . $po_seq_no;
                    } else {
                        $po_no = $po_seq_prefix . $po_seq_no;
                    }
                }

                $new_invoice = str_replace('{year}', $current_year, $purchase_order_format);
                $new_po_no = str_replace('{po_sequence}', $po_no, $new_invoice);

                //die($new_po_no);

                $sql = "SELECT * from " . $tblpx . "purchase  WHERE `purchase_no` LIKE '$new_po_no%' $where  AND  company_id = " . $company_id . " ORDER BY `p_id` DESC LIMIT 1;";
                $duplicate_data = Yii::app()->db->createCommand($sql)->queryRow();
                if (!empty($duplicate_data)) {
                    $previous_po_no = $duplicate_data['purchase_no'];
                    $prefix_arr = explode('/', $previous_po_no);
                    $prefix = $prefix_arr[0];
                    $previous_prefix_seq_no = $prefix_arr[$po_key];
                    $po_split = explode('-', $previous_prefix_seq_no);
                    $previous_prefix_seq_no_val = $po_split[1];
                    $po_seq_no = $previous_prefix_seq_no_val + 1;
                    $digit = strlen((string) $po_seq_no);
                    if ($this->getActiveTemplate() == 'TYPE-4' && ENV == 'production') {
                        $po_no = $this->getArchInvoiceNo();

                    } else {
                        if ($digit == 1) {
                            $po_no = $po_seq_prefix . '00' . $po_seq_no;
                        } else if ($digit == 2) {
                            $po_no = $po_seq_prefix . '0' . $po_seq_no;
                        } else {
                            $po_no = $po_seq_prefix . $po_seq_no;
                        }
                    }

                }
                $new_invoice = str_replace('{year}', $current_year, $purchase_order_format);
                $new_po_no = str_replace('{po_sequence}', $po_no, $new_invoice);

                $html['purchase_no'] = $new_po_no;
            } else if ($company->purchase_order_format == NULL && $company->auto_purchaseno == 1) {
                $html['auto_pono'] = $company->auto_purchaseno;
            }
            $html['html'] = '';
            $html['status'] = '';


            $activeProjectTemplate = $this->getActiveTemplate();


            if ($company->purchase_order_format == NULL && $company->auto_purchaseno == 1) {
                $maxData = Yii::app()->db->createCommand("SELECT MAX(CAST(purchase_no as signed)) as largenumber FROM {$tblpx}purchase")->queryRow();
                $maxNo = $maxData["largenumber"];
                $newPONo = $maxNo + 1;
                $prNo = $newPONo;
                do {
                    $purchase_no = Purchase::model()->findAll(array("condition" => "purchase_no = '$prNo'"));
                    if (empty($purchase_no)) {
                        $status = 1;
                    } else {
                        $prNo = $prNo + 1;
                        $status = 2;
                    }
                } while ($status == 2);
                $html['purchase_no'] = $prNo;
            } else if (!empty($company->purchase_order_format)) {
                $html['auto_pono'] = 2;
                $purchase_order_format = trim($company["purchase_order_format"]);
                $general_po_format_arr = explode('/', $purchase_order_format);
                // print_r($general_po_format_arr);exit;
                $org_prefix = $general_po_format_arr[0];
                $prefix_key = 0;
                $po_key = '';
                $current_key = '';
                foreach ($general_po_format_arr as $key => $value) {

                    if ($value == '{po_sequence}') {
                        $po_key = $key;
                    } else if ($value == '{year}') {
                        $current_key = $key;
                    }
                }

                $current_year = date('Y');
                $previous_year = date('Y') - 1;
                $next_year = date('Y') + 1;

                $where = '';
                if ($current_key == 2) {
                    $where .= " AND `purchase_no` LIKE '%$current_year'";
                } else if ($current_key == 1) {
                    $where .= " AND `purchase_no` LIKE '%$current_year%'";
                }
                $sql = "SELECT * from " . $tblpx . "purchase  WHERE `purchase_no` LIKE '$org_prefix%' $where   AND  company_id = " . $company_id . " ORDER BY `p_id` DESC LIMIT 1;";
                //echo "<pre>";print_r($sql);exit;
                $previous_po_no = '';
                $po_seq_no = 1;
                $po_seq_prefix = 'PO-';
                $previous_data = Yii::app()->db->createCommand($sql)->queryRow();
                if (!empty($previous_data)) {
                    $previous_po_no = $previous_data['purchase_no'];
                    $prefix_arr = explode('/', $previous_po_no);
                    $prefix = $prefix_arr[0];
                    $previous_prefix_seq_no = $prefix_arr[$po_key];
                    $po_split = explode('-', $previous_prefix_seq_no);
                    $previous_prefix_seq_no_val = $po_split[1];
                    $po_seq_no = $previous_prefix_seq_no_val + 1;

                }
                $digit = strlen((string) $po_seq_no);
                if ($this->getActiveTemplate() == 'TYPE-4' && ENV == 'production') {
                    $po_no = $this->getArchInvoiceNo();

                } else {
                    if ($digit == 1) {
                        $po_no = $po_seq_prefix . '00' . $po_seq_no;
                    } else if ($digit == 2) {
                        $po_no = $po_seq_prefix . '0' . $po_seq_no;
                    } else {
                        $po_no = $po_seq_prefix . $po_seq_no;
                    }
                }

                $new_invoice = str_replace('{year}', $current_year, $purchase_order_format);
                $new_po_no = str_replace('{po_sequence}', $po_no, $new_invoice);
                //die($new_po_no);
                $html['purchase_no'] = $new_po_no;
            } else {
                $html['purchase_no'] = '';
            }


            if (Yii::app()->user->role == 1) {
                $expense_type = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects WHERE FIND_IN_SET(" . $company_id . ",company_id) ORDER BY name")->queryAll();
            } else {
                $expense_type = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects WHERE FIND_IN_SET(" . $company_id . ",company_id) AND pid IN(SELECT projectid FROM {$tblpx}project_assign WHERE userid = " . Yii::app()->user->id . ") ORDER BY name")->queryAll();
            }


            if (!empty($expense_type)) {
                if (Yii::app()->user->hasState('form_data')) {
                    $session_datas = Yii::app()->user->getState('form_data');
                    extract($session_datas);
                }
                $html['html'] .= '<option value="">Choose Project</option>';
                foreach ($expense_type as $key => $value) {
                    $selected = '';
                    if (isset($_REQUEST['project']) && $_REQUEST['project'] != "") {
                        $selected = ($_REQUEST['project'] == $value['pid']) ? ' selected=selected' : '';
                    } else {
                        $selected = '';
                        if (isset($_REQUEST['popup'])) {
                            $sql = "SELECT `pid` FROM {$tblpx}projects "
                                . " ORDER BY pid DESC LIMIT 1";
                            $id = Yii::app()->db->createCommand($sql)->queryScalar();
                            $selected = ($id == $value['pid']) ? ' selected=selected' : '';
                        }
                    }

                    $html['html'] .= '<option value="' . $value['pid'] . '" "' . $selected . '">' . $value['name'] . '</option>';
                    $html['status'] = 'success';
                }
            } else {
                $html['status'] = 'no_success';
                $html['html'] .= '<option value="">Choose Project</option>';
            }
            echo json_encode($html);
        }

    }



    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Purchase the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Purchase::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Purchase $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'purchase-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionTest2()
    {
        $criteria = new CDbCriteria;
        $total = Purchase::model()->count();

        $pages = new CPagination($total);
        $pages->pageSize = 100;
        $pages->applyLimit($criteria);

        $posts = Purchase::model()->findAll($criteria);
        $model = new Purchase('search');
        $this->render('test', array(
            'posts' => $posts,
            'pages' => $pages,
            'dataProvider' => $model->search()
        ));
    }

    public function actionAdmin1()
    {
        $model = new Purchase('searchtest');
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) {
            $project = Yii::app()->db->createCommand("SELECT DISTINCT pid,name FROM {$tblpx}projects WHERE (" . $newQuery . ") ORDER BY name")->queryAll();
        } else {
            $project = Yii::app()->db->createCommand("SELECT DISTINCT pid,name FROM {$tblpx}projects WHERE (" . $newQuery . ") AND pid IN(SELECT projectid FROM {$tblpx}project_assign WHERE userid = " . Yii::app()->user->id . ") ORDER BY name")->queryAll();
        }
        $vendor = Yii::app()->db->createCommand("SELECT DISTINCT vendor_id,name FROM {$tblpx}vendors WHERE (" . $newQuery . ") ORDER BY name")->queryAll();
        $expense = Yii::app()->db->createCommand("SELECT DISTINCT type_id,type_name FROM {$tblpx}expense_type WHERE (" . $newQuery . ") ORDER BY type_name")->queryAll();
        if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) {
            $purchase_no = Yii::app()->db->createCommand("SELECT DISTINCT purchase_no,purchase_date FROM {$tblpx}purchase WHERE (" . $newQuery . ") AND purchase_no IS NOT NULL ORDER BY p_id")->queryAll();
        } else {
            $purchase_no = Yii::app()->db->createCommand("SELECT DISTINCT purchase_no,purchase_date FROM {$tblpx}purchase WHERE (" . $newQuery . ") AND purchase_no IS NOT NULL AND project_id IN(SELECT projectid FROM {$tblpx}project_assign WHERE userid = " . Yii::app()->user->id . ") ORDER BY p_id")->queryAll();
        }
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['project_id'])) {
            $model->project_id = $_GET['project_id'];
            $model->vendor_id = $_GET['vendor_id'];
            $model->expensehead_id = $_GET['expensehead_id'];
            $model->purchase_no = $_GET['purchase_no'];
            $model->purchase_status = $_GET['purchase_status'];
            $model->purchase_billing_status = $_GET['purchase_billing_status'];
            $model->date_from = $_GET['date_from'];
            $model->date_to = $_GET['date_to'];
            $model->company_id = $_GET['company_id'];
        }
        if (isset($_GET['date_from']) && isset($_GET['date_to']) && $_GET['date_to'] != '' && $_GET['date_from'] != '') {
            $datefrom = $_GET['date_from'];
            $dateto = $_GET['date_to'];
        } else {
            $datefrom = date('Y-m-d', strtotime('today - 30 days'));
            $dateto = date("Y-m-d");
        }
        $projectid = '';
        $vendorid = '';
        $expenseheadid = '';
        $purchase_no1 = '';
        $purchase_status = '';
        $purchase_billing_status = '';
        $company_id = '';
        if (isset($_GET['project_id'])) {
            $projectid = $_GET['project_id'];
        }
        if (isset($_GET['vendor_id'])) {
            $vendorid = $_GET['vendor_id'];
        }
        if (isset($_GET['expensehead_id'])) {
            $expenseheadid = $_GET['expensehead_id'];
        }
        if (isset($_GET['purchase_no'])) {
            $purchase_no1 = $_GET['purchase_no'];
        }
        if (isset($_GET['purchase_status'])) {
            $purchase_status = $_GET['purchase_status'];
        }
        if (isset($_GET['purchase_billing_status'])) {
            $purchase_billing_status = $_GET['purchase_billing_status'];
        }
        if (isset($_GET['company_id'])) {
            $company_id = $_GET['company_id'];
        }
        $getTotalbillamount = 0;
        $getTotalbillamount = $this->getTotalbillamount($datefrom, $dateto, $projectid, $vendorid, $expenseheadid, $purchase_no1, $purchase_status, $purchase_billing_status, $company_id);
        $total_blanceamount = $this->getTotalbalanceammount($datefrom, $dateto, $projectid, $vendorid, $expenseheadid, $purchase_no1, $purchase_status, $purchase_billing_status, $company_id);
        $this->render('testlist', array(
            'model' => $model,
            'dataProvider' => $model->searchtest(),
            'project' => $project,
            'vendor' => $vendor,
            'expense' => $expense,
            'purchase_no' => $purchase_no,
            'total_billedamount' => $getTotalbillamount,
            'total_blanceamount' => $total_blanceamount
        ));
    }

    protected function getTotalbillamount($datefrom, $dateto, $projectid, $vendorid, $expenseheadid, $purchase_no, $purchase_status, $purchase_billing_status, $company_id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', p.company_id)";
        }
        $where = "1=1";
        if (isset($projectid) && $projectid != '') {
            $where .= " AND p.project_id = '" . $projectid . "'";
        }
        if (isset($vendorid) && $vendorid != '') {
            $where .= " AND p.vendor_id = '" . $vendorid . "'";
        }
        if (isset($expenseheadid) && $expenseheadid != '') {
            $where .= " AND p.expensehead_id = '" . $expenseheadid . "'";
        }
        if (isset($purchase_no) && $purchase_no != '') {
            $where .= " AND p.purchase_no = '" . $purchase_no . "'";
        }
        if (isset($purchase_status) && $purchase_status != '') {
            $where .= " AND p.purchase_status = '" . $purchase_status . "'";
        }
        if (isset($purchase_billing_status) && $purchase_billing_status != '') {
            $where .= " AND p.purchase_billing_status = '" . $purchase_billing_status . "'";
        }
        if (isset($company_id) && $company_id != '') {
            $where .= " AND p.company_id = '" . $company_id . "'";
        }

        $total = Yii::app()->db->createCommand("SELECT SUM(b.bill_totalamount) AS totalbillamount FROM {$tblpx}bills b INNER JOIN {$tblpx}purchase p ON b.purchase_id = p.p_id WHERE p.type = 'po' AND p.purchase_no IS NOT NULL AND p.purchase_date >= '" . date('Y-m-d', strtotime($datefrom)) . "' AND p.purchase_date <= '" . date('Y-m-d', strtotime($dateto)) . "' AND ( " . $newQuery . ") AND (p.project_id IN (SELECT projectid FROM {$tblpx}project_assign WHERE userid = '" . Yii::app()->user->id . "') AND " . $where . ")")->queryRow();
        return $total['totalbillamount'];
    }

    protected function getTotalbalanceammount($datefrom, $dateto, $projectid, $vendorid, $expenseheadid, $purchase_no, $purchase_status, $purchase_billing_status, $company_id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', p.company_id)";
        }
        $total_blanceamount = 0;
        $where = "1=1";
        if (isset($projectid) && $projectid != '') {
            $where .= " AND p.project_id = '" . $projectid . "'";
        }
        if (isset($vendorid) && $vendorid != '') {
            $where .= " AND p.vendor_id = '" . $vendorid . "'";
        }
        if (isset($expenseheadid) && $expenseheadid != '') {
            $where .= " AND p.expensehead_id = '" . $expenseheadid . "'";
        }
        if (isset($purchase_no) && $purchase_no != '') {
            $where .= " AND p.purchase_no = '" . $purchase_no . "'";
        }
        if (isset($purchase_status) && $purchase_status != '') {
            $where .= " AND p.purchase_status = '" . $purchase_status . "'";
        }
        if (isset($purchase_billing_status) && $purchase_billing_status != '') {
            $where .= " AND p.purchase_billing_status = '" . $purchase_billing_status . "'";
        }
        if (isset($company_id) && $company_id != '') {
            $where .= " AND p.company_id = '" . $company_id . "'";
        }

        $values = Yii::app()->db->createCommand("SELECT item_id, quantity, rate, purchase_id,tax_amount FROM {$tblpx}purchase_items WHERE purchase_id IN (SELECT p.p_id FROM {$tblpx}purchase p WHERE p.type = 'po' AND p.purchase_no IS NOT NULL AND p.purchase_date >= '" . date('Y-m-d', strtotime($datefrom)) . "' AND p.purchase_date <= '" . date('Y-m-d', strtotime($dateto)) . "' AND ( " . $newQuery . ") AND (p.project_id IN (SELECT projectid FROM {$tblpx}project_assign WHERE userid = '" . Yii::app()->user->id . "') AND " . $where . "))")->queryAll();
        // echo '<pre>';
        // print_r($values);
        // exit;
        foreach ($values as $t_data) {
            $bill_item = Yii::app()->db->createCommand("SELECT sum(billitem_quantity) as billitem_quantity FROM {$tblpx}billitem WHERE purchaseitem_id=" . $t_data['item_id'])->queryRow();
            if (!empty($bill_item)) {

                $blance_quantity = $t_data['quantity'] - $bill_item['billitem_quantity'];
                $max = $blance_quantity * $t_data['rate'];
                $total_blanceamount += $max;
            } else {
                if ($record['purchase_billing_status'] == 93) {
                    $total_blanceamount += 0.00;
                } else {
                    $total_blanceamount += $t_data['quantity'] * $t_data['rate'];
                }
            }

            if (!empty($t_data['tax_amount'])) {
                $total_blanceamount += $t_data['tax_amount'];
            }
        }
        return $total_blanceamount;
    }

    protected function getUnbilledAmount($purchase_id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $unbilled = Yii::app()->db->createCommand("SELECT SUM(((pi.quantity - IFNULL((SELECT SUM(billitem_quantity) FROM {$tblpx}billitem WHERE purchaseitem_id = pi.item_id), 0)) * pi.rate)) as unbilled_amount FROM {$tblpx}purchase_items pi WHERE pi.purchase_id = {$purchase_id}")->queryRow();
        $unbilled_amount = $unbilled["unbilled_amount"];
        if ($unbilled_amount > 0)
            return $unbilled_amount;
        else
            return 0;
    }

    public function actionPurchasepermissionbulkaction()
    {
        $company_id = $_REQUEST["company_id"];
        $company = Company::model()->findByPk($company_id);
        echo "<u>Company Name: " . $company["name"] . "</u><br/><br/>";
        if ($company->company_popermission == 0) {
            $poLimit = $company->purchaseorder_limit;
            if ($poLimit > 0) {
                $purchase = Purchase::model()->findAll(array("condition" => "purchase_status = 'draft' AND company_id = " . $company_id . ""));
                foreach ($purchase as $data) {
                    $model = Purchase::model()->findByPk($data->p_id);
                    $totalAmount = $data->total_amount;
                    if ($totalAmount > $poLimit)
                        $model->permission_status = "No";
                    else
                        $model->permission_status = "Yes";
                    $model->save();
                }
                echo "All purchase order greater that {$poLimit} of this company needs permission. It is successfully done.";
            } else {
                echo "Invalid purchase order limit.";
            }
        } else {
            $purchase = Purchase::model()->findAll(array("condition" => "purchase_status = 'draft' AND company_id = " . $company_id . ""));
            foreach ($purchase as $data) {
                $model = Purchase::model()->findByPk($data->p_id);
                $model->permission_status = "No";
                $model->save();
            }
            echo "All purchase order of this company needs permission. It is successfully done.";
        }
    }

    public function actionCompanyedit()
    {
        $prev_company = $_GET['prev_company_id'];
        $company_id = $_GET['company_id'];
        $purchase_id = $_GET['purchase_id'];
        $company = Company::model()->findByPk($company_id);
        $tblpx = Yii::app()->db->tablePrefix;
        $purchase = Purchase::model()->findByPk($purchase_id);
        $project_model = Projects::model()->findByPK($purchase->project_id);
        $company_array = explode(',', $project_model->company_id);
        if (in_array($company_id, $company_array)) {
            $purchase->company_id = $company_id;
            if ($purchase->update()) {
                $prev_company_data = ['prev_company' => $_GET['prev_comp_name'], 'current_company' => $company->name, 'project_name' => $project_model->name, 'purchase_no' => $purchase->purchase_no];
                $edit_log = new CompanyEditLog;
                $edit_log->p_id = $purchase->p_id;
                $edit_log->project_id = $purchase->project_id;
                $edit_log->prev_company_id = $prev_company;
                $edit_log->current_company_id = $company_id;
                $edit_log->log_date = date('Y-m-d');
                $edit_log->changed_by = Yii::app()->user->id;
                $edit_log->log_data = json_encode($prev_company_data);
                $edit_log->save();
                if (Yii::app()->user->role != 1) {
                    $data = ['purchase_id' => $purchase->p_id, 'no' => $purchase->purchase_no, 'company' => $company->name, 'date' => date("Y-m-d H:i:s"), 'description' => 'Company changed', 'company_id' => $company_id, 'project_name' => $project_model->name];
                    $requestdata = Editrequest::model()->find(array('condition' => 'editrequest_table = "' . $tblpx . 'purchase" AND parent_id = "' . $purchase->p_id . '" AND editrequest_status = "0"'));

                    if (!$requestdata) {
                        $editrequest = new Editrequest;
                        $editrequest->editrequest_data = json_encode($data);
                        $editrequest->editrequest_table = "{$tblpx}purchase";
                        $editrequest->editrequest_payment = 0;
                        $editrequest->parent_id = $purchase->p_id;
                        $editrequest->user_id = Yii::app()->user->id;
                        $editrequest->editrequest_date = date("Y-m-d H:i:s");
                        $editrequest->editrequest_status = 0;
                        $editrequest->save();
                    } else {
                        $requestdata->editrequest_data = json_encode($data);
                        $requestdata->user_id = Yii::app()->user->id;
                        $requestdata->editrequest_date = date("Y-m-d H:i:s");
                        $requestdata->update();
                    }
                    echo json_encode(array('company' => $company->name, 'status' => 2, 'id' => $company_id, 'gstnum' => $company->company_gstnum));
                } else {

                    $bill_model = Bills::model()->find(['condition' => 'purchase_id ="' . $purchase_id . '"']);
                    if (!empty($bill_model)) {
                        $bill_model->company_id = $company_id;
                        $bill_model->save();
                    }

                    echo json_encode(array('company' => $company->name, 'status' => 1, 'id' => $company_id, 'gstnum' => $company->company_gstnum));
                }
            } else {
                echo json_encode(array('company' => "", 'status' => 0, 'id' => $company_id));
            }
        } else {
            echo json_encode(array('company' => "", 'status' => 0, 'id' => $company_id));
        }
    }

    public function actionDeletepurchasebill($pid)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $model = Purchase::model()->find(array("condition" => "p_id='$pid'"));
        $model2 = Bills::model();
        $item_model = PurchaseItems::model()->findAll(array("condition" => "purchase_id = '$pid'"));
        $bill_model = Bills::model()->findAll(array("condition" => "purchase_id='$pid'"));
        $admin_approval_data = Editrequest::model()->find(['condition' => 'editrequest_table ="' . $tblpx . 'purchase" AND parent_id="' . $pid . '" AND editrequest_status = 0']);
        if (!empty($model)) {
            $project = Projects::model()->findByPK($model->project_id);

            $vendor = Vendors::model()->findByPK($model->vendor_id);
            $expense_head = ExpenseType::model()->findByPK($model->expensehead_id);
            $arrVal = explode(',', $project->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
            }
            $typelist = Company::model()->findAll(array('condition' => $newQuery));

            $this->render('deletepurchasebill', array(
                'model' => $model,
                'itemmodel' => $item_model,
                'project' => $project,
                'vendor' => $vendor,
                'expense_head' => $expense_head,
                'typelist' => $typelist,
                'admin_approval_data' => $admin_approval_data,
                'bill_model' => $bill_model,
                'model2' => $model2,
            ));
        } else {
            $this->redirect(array('purchase/admin'));
        }
    }

    public function actionDeletebill()
    {

        $purchase_id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
        $bill_id = isset($_REQUEST['bill_id']) ? $_REQUEST['bill_id'] : '';

        $tblpx = Yii::app()->db->tablePrefix;
        $reconTabName = $tblpx . "expenses";
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if ($bill_id != "") {
                $bill_model = Bills::model()->findAll(array("condition" => "bill_id='$bill_id'"));
            } else {
                $bill_model = Bills::model()->findAll(array("condition" => "purchase_id='$purchase_id'"));
            }
            $bill_data = Bills::model()->findByPk($bill_id);
            $purchase_id = ($purchase_id > 0) ? $purchase_id : $bill_data->purchase_id;
            if (!empty($bill_model)) {
                foreach ($bill_model as $key => $value) {
                    $expense_model = Expenses::model()->findAll(array("condition" => "bill_id=" . $value['bill_id'] . ""));
                    if (!empty($expense_model)) {
                        foreach ($expense_model as $key => $value2) {
                            $value1 = Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $value2['exp_id'] . " AND reconciliation_table = '" . $reconTabName . "'"));
                        }
                        $model1 = Expenses::model()->deleteAll(array("condition" => "bill_id=" . $value['bill_id'] . ""));
                    }
                    $bill_itemmodel = Billitem::model()->deleteAll(array("condition" => "bill_id=" . $value['bill_id'] . ""));
                }
                if($bill_id !=""){
                    if(!empty($bill_id)){
                         $bill_items = Billitem::model()->findAll(array("condition" => "bill_id = :bill_id", "params" => array(':bill_id' => $bill_id)));
                        if(!empty($bill_items)){
                         $bill_itemmodel = Billitem::model()->deleteAll(array("condition" => "bill_id=" . $bill_id . ""));
                        }
                    };
                    $logmodel = new JpLog('search');
                    $logmodel->log_data = json_encode($bill_data->attributes);
                    $logmodel->log_action = 2;
                    $logmodel->log_table = $tblpx . "bills";
                    $logmodel->log_datetime = date('Y-m-d H:i:s');
                    $logmodel->log_action_by = Yii::app()->user->id;
                    $logmodel->company_id = yii::app()->user->company_id;
                    $logmodel->save();  
                    $model2 = Bills::model()->deleteAll(array("condition" => "bill_id='$bill_id'"));
                    
                            $criteria = new CDbCriteria();
                            $criteria->condition = 'bill_id = :bill_id';
                            $criteria->params = array(':bill_id' => $bill_id);
                    
                            $mr_materials_model = MaterialRequisitionItems::Model()->findAll($criteria);
                            
                            if (!empty($mr_materials_model)) {
                                foreach ($mr_materials_model as $material) {
                                    $material->bill_id = null;
                                    $material->po_id = null;
                                    $material->save(); // Update the po_id value
                                    
                                }
                            }

                } else {
                    $model2 = Bills::model()->deleteAll(array("condition" => "purchase_id='$purchase_id'"));
                    
                    $criteria = new CDbCriteria();
                    $criteria->condition = 'po_id = :purchase_id';
                    $criteria->params = array(':purchase_id' => $purchase_id);

                    $mr_materials_model = MaterialRequisitionItems::Model()->findAll($criteria);

                    if (!empty($mr_materials_model)) {
                        foreach ($mr_materials_model as $material) {
                            $material->bill_id = null;
                            $material->po_id = null;
                            $material->save(); // Update the po_id value

                        }
                    }
                }

                if ($model2) {
                    $bill_exist = Bills::model()->findAll(array("condition" => "purchase_id='$purchase_id'"));
                    if (count($bill_exist)) {
                        $update = Yii::app()->db->createCommand()->update($tblpx . 'purchase', array('purchase_billing_status' => 94), 'p_id=:p_id', array(':p_id' => $purchase_id));
                    } else {
                        $update = Yii::app()->db->createCommand()->update($tblpx . 'purchase', array('purchase_billing_status' => 95, 'purchase_status' => 'draft'), 'p_id=:p_id', array(':p_id' => $purchase_id));
                    }
                    $status = 1;
                } else {
                    $status = 2;
                }
            } else {
                $status = 3;
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $status = 2;
        } finally {
            if ($status == 1) {
                echo json_encode(array('status' => 1));
            } else if ($status == 3) {
                echo json_encode(array('status' => 3));
            } else {

                if ($error->errorInfo[1] == 1451) {
                    echo json_encode(array('status' => 4));
                } else {
                    echo json_encode(array('status' => 2));
                }
            }
        }
    }

    public function actiondeletePurchaseOrder()
    {
        $purchase_id = $_POST['item_id'];
        $tblpx = Yii::app()->db->tablePrefix;
        if (!empty($purchase_id)) {
            $purchase_model = Purchase::model()->findByPk($purchase_id);
            $result = array('status' => '0', 'msg' => 'An error Occured');
            $transaction = Yii::app()->db->beginTransaction();
            $items = PurchaseItems::model()->findAll('purchase_id = :purchase_id', array(':purchase_id' => $purchase_id));
            try {
                if (!empty($items)) {
                    if (PurchaseItems::model()->deleteAll('purchase_id = :purchase_id', array(':purchase_id' => $purchase_id))) {
                        $criteria = new CDbCriteria();
                        $criteria->condition = 'po_id = :po_id';
                        $criteria->params = array(':po_id' => $purchase_id);

                        $mr_materials_model = MaterialRequisitionItems::Model()->findAll($criteria);

                        if (!empty($mr_materials_model)) {
                            foreach ($mr_materials_model as $material) {
                                $material->bill_id = null;
                                $material->po_id = null;
                                $material->save(); // Update the po_id value

                            }
                        }

                    } else {
                        throw new Exception('An error occured in delete items');
                    }
                }
                $logmodel = new JpLog('search');
                $logmodel->log_data = json_encode($purchase_model->attributes);
                $logmodel->log_action = 2;
                $logmodel->log_table = $tblpx . "purchase";
                $logmodel->log_datetime = date('Y-m-d H:i:s');
                $logmodel->log_action_by = Yii::app()->user->id;
                $logmodel->company_id = yii::app()->user->company_id;
                if ($logmodel->save()) {
                    if (!$purchase_model->delete()) {
                        throw new Exception('An error occured in delete purchase order');
                    } else {
                        Yii::app()->user->setFlash('success', "Successfully deleted!");
                        $result = array('status' => '1', 'msg' => 'Successfully deleted');
                    }
                } else {
                    throw new Exception('An error occured in delete purchase order log');
                }
                $transaction->commit();
            } catch (Exception $error) {
                $transaction->rollBack();
                $result = array('status' => '0', 'msg' => $error->getMessage());
            } finally {
                echo json_encode($result);
                exit;
            }
        }
    }

    public function actionrateDecline()
    {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
        $comments = isset($_REQUEST['comments']) ? $_REQUEST['comments'] : "";
        if (!empty($id)) {
            $data = Purchase::model()->findByPk($id);
            if ($data->permission_status == 'No') {
                $data->permission_status = 'Declined';
                $data->decline_comment = $comments;
                $data->declined_by = Yii::app()->user->id;
                $data->declined_date = date('Y-m-d');
                if ($data->save()) {
                    echo json_encode(array('response' => 'success', 'msg' => 'PO declined successfully '));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => $data->getErrors()));
                    ;
                }
            } elseif ($data->permission_status == 'Yes') {
                echo json_encode(array('response' => 'warning', 'msg' => 'Rate already approved'));
            } else {
                echo json_encode(array('response' => 'warning', 'msg' => 'Rate already declined'));
            }
        }
    }

    public function actionItemestimation()
    {
        $model = new Itemestimation('search');
        $tblpx = Yii::app()->db->tablePrefix;
        $projectsql = "SELECT * FROM {$tblpx}itemestimation";
        $projects = Yii::app()->db->createCommand($projectsql)->queryAll();
        $approve = 1;

        foreach ($projects as &$project) {
            $project['approve'] = 1;
            $projectId = $project['pms_project_id'];

            $estimation = Itemestimation::model()->find(array(
                'condition' => 'pms_project_id = :projectId AND pms_project_id IS NOT NULL AND pms_project_id != ""',
                'params' => array(':projectId' => $projectId),
            ));


            $estimationId = $estimation && $estimation->itemestimation_id ? $estimation->itemestimation_id : null;

            // materials
            $materials = MaterialEstimation::model()->findAll(array(
                'condition' => 'estimation_id = :estimationId AND (amount IS NULL OR amount = "")',
                'params' => array(':estimationId' => $estimationId),
            ));

            // labours
            $labours = LabourEstimation::model()->findAll(array(
                'condition' => 'estimation_id = :estimationId AND (amount IS NULL OR amount = "")',
                'params' => array(':estimationId' => $estimationId),
            ));

            //equipment
            $equipments = EquipmentsEstimation::model()->findAll(array(
                'condition' => 'estimation_id = :estimationId AND (amount IS NULL OR amount = "")',
                'params' => array(':estimationId' => $estimationId),
            ));

            if (!empty($equipments) || !empty($labours) || !empty($materials)) {
                $project['approve'] = 0;
            }

        }
        unset($project);
        

        $this->render('itemestimation', array(
            'model' => $model,
            'dataProvider' => $model->search(),
            'project' => $projects,
            'approve' => $approve,
        ));
    }

    public function actionViewestimation()
    {
        $project_id = $_REQUEST["project_id"];
        $tblpx = Yii::app()->db->tablePrefix;
        $pmodel = Projects::model()->findByPk($project_id);
        $model = Itemestimation::model()->findAll(array("condition" => "project_id = '$project_id'"));
        //material Estimation
        $model_material = MaterialEstimation::model()->findAll(array("condition" => "project_id = '$project_id'"));
        $material_ids = array_map(function ($item) {
            return $item->primaryKey;
        }, $model_material);
        $criteria = new CDbCriteria();
        $criteria->addInCondition('id', $material_ids);
        $dataProvider = new CActiveDataProvider('MaterialEstimation', array(
            'criteria' => $criteria,
        ));
        //labour estimation
        $model_labour = LabourEstimation::model()->findAll(array("condition" => "project_id = '$project_id'"));
        $labour_ids = array_map(function ($item) {
            return $item->primaryKey;
        }, $model_labour);

        $criteria_labour = new CDbCriteria();
        $criteria_labour->addInCondition('id', $labour_ids);

        $dataProviderLabour = new CActiveDataProvider('LabourEstimation', array(
            'criteria' => $criteria_labour,
        ));
        //equipment estimation
        $model_equipment = EquipmentsEstimation::model()->findAll(array("condition" => "project_id = '$project_id'"));
        $equipment_ids = array_map(function ($item) {
            return $item->primaryKey;
        }, $model_equipment);
        $criteria_equipment = new CDbCriteria();
        $criteria_equipment->addInCondition('id', $equipment_ids);
        $dataProviderEquipment = new CActiveDataProvider('EquipmentsEstimation', array(
            'criteria' => $criteria_equipment,
        ));

        $this->render('viewestimation', array(
            'model' => $model,
            'project_id' => $project_id,
            'model_material' => $model_material,
            'model_labour' => $model_labour,
            'model_equipment' => $model_equipment,
            'projects' => $pmodel,
            'dataProvider' => $dataProvider,
            'dataProviderLabour' => $dataProviderLabour,
            'dataProviderEquipment' => $dataProviderEquipment,
        ));
    }

    public function actionEstimation()
    {
        $model = new Itemestimation('search');
        $tblpx = Yii::app()->db->tablePrefix;
        $project = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects")->queryAll();
        $projectid = "";
        $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');

        $this->render('estimation', array(
            'model' => $model,
            'dataProvider' => $model->search(),
            'project' => $project,
            'projectid' => $projectid,
            'specification' => $specification,
        ));
    }

    public function actionGetCategoryByProject()
    {
        $project = $_REQUEST["project"];
        $type = $_REQUEST["type"];
        $tblpx = Yii::app()->db->tablePrefix;
        $condition = "";
        if (isset($project) && !empty($project)) {
            $condition = " s.id NOT IN (SELECT category_id "
                . " FROM {$tblpx}itemestimation WHERE project_id = {$project} "
                . " GROUP BY category_id)";
        }

        $sql = "SELECT s.id, s.specification, s.brand_id, s.cat_id, "
            . " pc1.category_name,br.brand_name "
            . " FROM jp_specification s LEFT JOIN jp_purchase_category pc1 "
            . " ON s.cat_id = pc1.id "
            . " LEFT JOIN jp_brand br ON s.brand_id = br.id "
            . " WHERE " . $condition;
        $specification = Yii::app()->db->createCommand($sql)->queryAll();
        $result = "<option value=''>Select one</option>";
        foreach ($specification as $value) {
            $result .= "<option value='" . $value["id"] . "'>" . $value["category_name"] . "-" . $value["brand_name"] . "-" . $value["specification"] . "</option>";
        }
        echo $result;
    }

    public function actionGetUnitByCategory()
    {
        $categoryId = $_REQUEST["catId"];
        $tblpx = Yii::app()->db->tablePrefix;
        $sql = "SELECT * FROM jp_specification s "
            . " LEFT JOIN jp_unit u ON s.unit = u.unit_name "
            . " WHERE s.id = {$categoryId}";

        $unit = Yii::app()->db->createCommand($sql)->queryRow();
        $unitName = $unit["unit"];
        echo $unitName;
    }

    public function actionAddEstimation()
    {
        $project = $_REQUEST["project_id"];
        $model = Itemestimation::model()->findByAttributes(['project_id' => $project]);


        if ($model === null) {
            $model = new Itemestimation;
            $model->project_id = $project;
            $model->itemestimation_status = 1;
        }

        if ($model->save()) {
            echo 1;
        } else {
            echo 2;
        }
    }


    public function actionUpdateestimation()
    {
        $estimation_id = $_REQUEST["estimation_id"];
        $project_id = $_REQUEST["project_id"];
        $model = Itemestimation::model()->findByPk($estimation_id);
        $tblpx = Yii::app()->db->tablePrefix;
        $prsql = "SELECT * FROM {$tblpx}projects WHERE pid = {$project_id}";
        $project = Yii::app()->db->createCommand($prsql)->queryAll();
        $type = "";
        $categoryId = $model->category_id;

        $catsql = "SELECT pc.id,pc.category_name, "
            . "pc1.id as category_id, pc1.specification,br.brand_name "
            . "FROM {$tblpx}purchase_category pc LEFT JOIN "
            . "{$tblpx}specification pc1 ON pc.id = pc1.cat_id "
            . "LEFT JOIN {$tblpx}brand br ON pc1.brand_id = br.id "
            . "WHERE pc1.id NOT IN (SELECT category_id FROM  "
            . "{$tblpx}itemestimation WHERE project_id = {$project_id} "
            . "AND category_id != {$categoryId} GROUP BY category_id)";
        $category = Yii::app()->db->createCommand($catsql)->queryAll();
        $i = 0;
        foreach ($category as $categoryList) {
            $specification[$i]["data"] = $categoryList["category_name"] . "-" . $categoryList["brand_name"] . "-" . $categoryList["specification"];
            $specification[$i]["id"] = $categoryList["category_id"];
            $i = $i + 1;
        }
        $this->render('updateestimation', array(
            'model' => $model,
            'project' => $project,
            'projectid' => $project_id,
            'specification' => $specification,
        ));
    }

    public function actionSaveUpdateestimation()
    {
        $project = $_REQUEST["project"];
        $type = $_REQUEST["type"];
        $category = $_REQUEST["category"];
        $width = $_REQUEST["width"];
        $height = $_REQUEST["height"];
        $length = $_REQUEST["length"];
        $quantity = $_REQUEST["quantity"];
        $unit = $_REQUEST["unit"];
        $rate = $_REQUEST["rate"];
        $amount = $_REQUEST["amount"];
        $remark = $_REQUEST["remark"];
        $estimation = $_REQUEST["estimation"];
        $model = Itemestimation::model()->findByPk($estimation);
        $model->project_id = $project;
        $model->purchase_type = $type;
        $model->category_id = $category;
        if (!empty($width))
            $model->itemestimation_width = $width;
        if (!empty($height))
            $model->itemestimation_height = $height;
        if (!empty($length))
            $model->itemestimation_length = $length;
        $model->itemestimation_unit = $unit;
        $model->itemestimation_quantity = $quantity;
        $model->itemestimation_price = $rate;
        $model->itemestimation_amount = $amount;
        $model->itemestimation_description = $remark;
        $model->itemestimation_status = 1;
        if ($model->save()) {
            echo 1;
        } else {
            echo 2;
        }
    }

    public function actionDeleteestimation()
    {
        $estimation_id = $_POST['estimation_id'];
        $project_id = $_POST['project_id'];
        $tblpx = Yii::app()->db->tablePrefix;
        $deletemodel = Itemestimation::model()->findByPk($estimation_id)->delete();
        if ($deletemodel) {
            $materials = MaterialEstimation::model()->findAllByAttributes(array('project_id' => $project_id));
            foreach ($materials as $material) {
                $material->delete();
            }
            $labours = LabourEstimation::model()->findAllByAttributes(array('project_id' => $project_id));
            foreach ($labours as $labour) {
                $labour->delete();
            }

            $equipments = EquipmentsEstimation::model()->findAllByAttributes(array('project_id' => $project_id));
            foreach ($equipments as $equipment) {
                $equipment->delete();
            }

            $response = [
                'success' => 'success',
                'message' => 'Record deleted successfully.'
            ];
        } else {
            $response['message'] = 'Record not found.';
        }
        echo json_encode($response);
    }

    public function actionapprove()
    {
        $id = $_POST['id'];
        $tblpx = Yii::app()->db->tablePrefix;
        $transaction = Yii::app()->db->beginTransaction();
        $estimation = Itemestimation::model()->findByPk($id);
        if ($estimation->itemestimation_status == 3) {
            try {
                $update = Yii::app()->db->createCommand()
                    ->update("{$tblpx}itemestimation", array(
                        'itemestimation_status' => 2,
                    ), 'itemestimation_id=:itemestimation_id', array(':itemestimation_id' => $id));
                if ($update) {

                    $notification = new Notifications;
                    $notification->action = "Estimation Approved";
                    $estimation = Itemestimation::model()->findByPk($id);
                    $projectModel = Projects::model()->findByPk($estimation->project_id);
                    $notification->message = "Estimation created for project " . $projectModel->name . " quantity change has been approved.";
                    $notification->parent_id = $estimation->itemestimation_id;
                    $notification->date = date("Y-m-d");
                    $notification->requested_by = Yii::app()->user->id;
                    $notification->approved_by = Yii::app()->user->id;

                    if (!$notification->save()) {
                        echo 0;
                        throw new Exception($notification->getErrors());
                    }
                    $pms_api_integration = ApiSettings::model()->pmsIntegrationStatus();
                    if ($pms_api_integration == 1 && $estimation->pms_estimation_id) {
                        $request = ['pms_estimation_id' => $estimation->pms_estimation_id, 'status' => 1];
                        $slug = "api/estimation-approve-reject";
                        $method = "POST";
                        $globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
                    }

                    echo 1;
                } else {
                    echo 0;
                    throw new Exception('Error updating material entry status.');
                }

                $transaction->commit();
            } catch (Exception $error) {
                $transaction->rollback();
                echo 0;
                throw new CHttpException(500, $error->getMessage());
            }
        } else {
            try {
                $update = Yii::app()->db->createCommand()
                    ->update("{$tblpx}itemestimation", array(
                        'itemestimation_status' => 2,
                    ), 'itemestimation_id=:itemestimation_id', array(':itemestimation_id' => $id));
                if ($update) {
                    $notification = new Notifications;
                    $notification->action = "Estimation Approved";
                    $estimation = Itemestimation::model()->findByPk($id);
                    $projectModel = Projects::model()->findByPk($estimation->project_id);
                    $notification->message = "Estimation created for project " . $projectModel->name . " has been approved.";
                    "PO created for project AI Mobile by user annbhi has been approved";
                    $notification->parent_id = $estimation->itemestimation_id;
                    $notification->date = date("Y-m-d");
                    $notification->requested_by = Yii::app()->user->id;
                    $notification->approved_by = Yii::app()->user->id;

                    if (!$notification->save()) {
                        echo 0;
                        throw new Exception($notification->getErrors());
                    }
                    $pms_api_integration = ApiSettings::model()->pmsIntegrationStatus();
                    if ($pms_api_integration == 1 && $estimation->pms_estimation_id) {
                        $request = ['pms_estimation_id' => $estimation->pms_estimation_id, 'status' => 1];
                        $slug = "api/estimation-approve-reject";
                        $method = "POST";
                        $globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
                    }
                    echo 1;
                } else {
                    echo 0;
                    throw new Exception('Error updating material entry status.');
                }

                $transaction->commit();
            } catch (Exception $error) {
                $transaction->rollback();
                echo 0;
                throw new CHttpException(500, $error->getMessage());
            }
        }

    }

    public function actionRejectEstimation()
    {
        if (isset($_POST['id']) && isset($_POST['remarks'])) {
            $id = $_POST['id'];
            $remarks = $_POST['remarks'];
            $estimation = Itemestimation::model()->findByPk($id);
            if ($estimation) {
                $estimation->itemestimation_status = '4';
                $estimation->remarks = $remarks;
                if ($estimation->save()) {
                    //Api calling  
                    $pms_api_integration = ApiSettings::model()->pmsIntegrationStatus();
                    if ($pms_api_integration == 1 && $estimation->pms_estimation_id) {
                        $request = ['pms_estimation_id' => $estimation->pms_estimation_id, 'status' => 2];
                        $slug = "api/estimation-approve-reject";
                        $method = "POST";
                        $globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
                    }
                    echo json_encode(array('response' => 'success', 'msg' => 'Estimation rejected successfully'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Failed to reject the estimation'));
                }
            } else {
                echo json_encode(array('response' => 'error', 'msg' => 'Estimation not found'));
            }
        } else {
            echo json_encode(array('response' => 'error', 'msg' => 'Invalid request'));
        }
    }

    public function actionAddglasspurchase()
    {
        $model = new Purchase();
        $item_model = new PurchaseItems();
        $specification = $this->actionGetItemCategoryglass($parent = 0, $spacing = '', $user_tree_array = '');
        $tblpx = Yii::app()->db->tablePrefix;
        $projectsql = "SELECT DISTINCT pid, name  "
            . " FROM {$tblpx}projects "
            . " WHERE company_id=" . Yii::app()->user->company_id . " "
            . " ORDER BY name";
        $project = Yii::app()->db->createCommand($projectsql)->queryAll();
        $vendorsql = "SELECT DISTINCT vendor_id, name "
            . " FROM {$tblpx}vendors "
            . " WHERE company_id=" . Yii::app()->user->company_id . " "
            . " ORDER BY name";
        $vendor = Yii::app()->db->createCommand($vendorsql)->queryAll();
        $sql = "SELECT DISTINCT description "
            . " FROM {$tblpx}purchase_items "
            . " ORDER BY description";
        $list_description = Yii::app()->db->createCommand($sql)->queryAll();
        $this->render('purchaseglass', array(
            'model' => $model,
            'project' => $project,
            'vendor' => $vendor,
            'list_description' => $list_description,
            'specification' => $specification,
        ));
    }

    public function actionAddpurchasebylength()
    {
        // echo "<pre>";
        // print_r($_POST);exit;
        $model = new Purchase();
        $item_model = new PurchaseItems();
        $specification = $this->actionGetItemCategory1($parent = 0, $spacing = '', $user_tree_array = '');
        //echo "<pre>";print_r($specification);exit;
        $tblpx = Yii::app()->db->tablePrefix;
        $project = Yii::app()->db->createCommand("SELECT DISTINCT pid, name  FROM {$tblpx}projects WHERE company_id=" . Yii::app()->user->company_id . " ORDER BY name")->queryAll();
        $vendor = Yii::app()->db->createCommand("SELECT DISTINCT vendor_id, name FROM {$tblpx}vendors WHERE company_id=" . Yii::app()->user->company_id . " ORDER BY name")->queryAll();
        $list_description = Yii::app()->db->createCommand("SELECT DISTINCT description FROM {$tblpx}purchase_items ORDER BY description")->queryAll();
        $this->render('addpurchasebylength', array(
            'model' => $model,
            'project' => $project,
            'vendor' => $vendor,
            'list_description' => $list_description,
            'specification' => $specification,
        ));
    }
    public function actionGetItemCategory1($parent = 0, $spacing = '', $user_tree_array = '')
    {
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= "  FIND_IN_SET('" . $arr . "', id)";
        }
        if (!is_array($user_tree_array))
            $user_tree_array = array();

        $data = array();
        $final = array();
        $tblpx = Yii::app()->db->tablePrefix;
        $spec_sql = "SELECT id, specification, unit, brand_id, cat_id,sub_cat_id "
            . " FROM {$tblpx}specification "
            . " WHERE  specification_type = 'A' "
            . " AND spec_status = '1'"
            . "AND". $newQuery;
        //die($spec_sql);

        $specification = Yii::app()->db->createCommand($spec_sql)->queryAll();
        foreach ($specification as $key => $value) {
            if ($value['brand_id'] != NULL) {
                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $value['brand_id'] . "")->queryRow();
                $brand = '-' . ucfirst($brand_details['brand_name']);
            } else {
                $brand = '';
            }
            if ($value['unit'] != NULL) {
                $unit = '-' . $value['unit'];
            } else {
                $unit = '';
            }
            $category = '';
            if ($value['cat_id'] != '') {
                $result = $this->actionGetParent($value['cat_id']);
                $category = ucwords($result['category_name']);
                $final[$key]['data'] = ucfirst($result['category_name']) . $brand . '-' . ucfirst($value['specification']);
            } else {

                $final[$key]['data'] = $brand . '-' . $value['specification'] . ' - ' . $unit . "(unit)";
            }
            if ($value['sub_cat_id'] != '') {
                $result = $this->actionGetSubcategory($value['sub_cat_id']);

                $final[$key]['data'] = $category . '-' . ucfirst($result['sub_category_name']) . $brand . '-' . ucfirst($value['specification']) . ' - ' . $unit . "(unit)";
            } else {

                $final[$key]['data'] = $category . $brand . '-' . $value['specification'] . ' - ' . $unit . "(unit)";
            }


            $final[$key]['id'] = $value['id'];
        }
        return $final;
    }

    public function actionGetItemCategoryglass($parent = 0, $spacing = '', $user_tree_array = '')
    {
        if (!is_array($user_tree_array))
            $user_tree_array = array();
        $data = array();
        $final = array();
        $tblpx = Yii::app()->db->tablePrefix;
        $specsql = "SELECT id, specification, brand_id, cat_id,sub_cat_id "
            . " FROM {$tblpx}specification "
            . " WHERE  specification_type = 'G' "
            . " AND company_id=" . Yii::app()->user->company_id . "";

        $specification = Yii::app()->db->createCommand($specsql)->queryAll();


        foreach ($specification as $key => $value) {
            if ($value['brand_id'] != NULL) {
                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $value['brand_id'] . "")->queryRow();
                $brand = '-' . ucfirst($brand_details['brand_name']);
            } else {
                $brand = '';
            }
            $category = '';
            if ($value['cat_id'] != '') {
                $result = $this->actionGetParent($value['cat_id']);
                $final[$key]['data'] = ucfirst($result['category_name']) . $brand . '-' . ucfirst($value['specification']);
                $category = ucfirst($result['category_name']);
            } else {
                $final[$key]['data'] = $brand . '-' . $value['specification'];
            }
            if ($value['sub_cat_id'] != '') {
                $result = $this->actionGetSubcategory($value['sub_cat_id']);

                $final[$key]['data'] = $category . '-' . ucfirst($result['sub_category_name']) . $brand . '-' . ucfirst($value['specification']);
            } else {

                $final[$key]['data'] = $category . $brand . '-' . $value['specification'];
            }



            $final[$key]['id'] = $value['id'];
        }
        return $final;
    }

    public function actionCreatenewpurchase2()
    {
        $purchase_id = $_REQUEST['purchase_id'];

        if (isset($_REQUEST['purchaseno']) && isset($_REQUEST['default_date']) && isset($_REQUEST['vendor']) && isset($_REQUEST['project']) && isset($_REQUEST['company'])) {

            if ($purchase_id == 0) {
                $tblpx = Yii::app()->db->tablePrefix;
                $billing_status = Yii::app()->db->createCommand("SELECT sid FROM {$tblpx}status WHERE status_type='purchase_bill_status' AND caption='PO not issued'")->queryRow();
                $res = Purchase::model()->findAll(array('condition' => 'purchase_no = "' . $_REQUEST['purchaseno'] . '"'));
                if (empty($res)) {
                    $model = new Purchase;
                    $model->purchase_no = $_REQUEST['purchaseno'];
                    $model->expected_delivery_date = !empty($_REQUEST['delivery_date']) ? date('Y-m-d', strtotime($_REQUEST['delivery_date'])) : NULL;
                    $model->project_id = $_REQUEST['project'];
                    $model->vendor_id = $_REQUEST['vendor'];
                    $model->company_id = $_REQUEST['company'];
                    $model->expensehead_id = $_REQUEST['expense_head'];
                    $model->purchase_date = date('Y-m-d', strtotime($_REQUEST['default_date']));
                    $company_details = Company::model()->findByPk(Yii::app()->user->company_id);
                    if ($company_details->company_popermission == 1) {
                        $model->purchase_status = 'permission_needed';
                    } else {
                        $model->purchase_status = 'draft';
                    }
                    $model->purchase_billing_status = $billing_status['sid'];
                    $model->created_date = date('Y-m-d');
                    $model->created_by = Yii::app()->user->id;
                    $model->purchase_type = "A";
                    $model->save(false);
                    $last_id = $model->p_id;
                    echo json_encode(array('response' => 'success', 'msg' => 'Purchase added successfully', 'p_id' => $last_id));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Purchase no already exist'));
                }
            } else {
                $res = Purchase::model()->findAll(array('condition' => 'purchase_no = "' . $_REQUEST['purchaseno'] . '" AND p_id !=' . $purchase_id . ''));
                if (empty($res)) {
                    $model = Purchase::model()->findByPk($purchase_id);
                    $model->expected_delivery_date = !empty($_REQUEST['delivery_date']) ? date('Y-m-d', strtotime($_REQUEST['delivery_date'])) : NULL;
                    $model->purchase_no = $_REQUEST['purchaseno'];
                    $model->project_id = $_REQUEST['project'];
                    $model->vendor_id = $_REQUEST['vendor'];
                    $model->expensehead_id = $_REQUEST['expense_head'];
                    $model->purchase_date = date('Y-m-d', strtotime($_REQUEST['default_date']));
                    $model->company_id = $_REQUEST['company'];
                    $company_details = Company::model()->findByPk(Yii::app()->user->company_id);
                    if ($company_details->company_popermission == 1) {
                        $model->purchase_status = 'permission_needed';
                    } else {
                        $model->purchase_status = 'draft';
                    }
                    $model->created_date = date('Y-m-d');
                    $model->created_by = Yii::app()->user->id;
                    $model->purchase_type = "A";
                    if ($model->save(false)) {
                        echo json_encode(array('response' => 'success', 'msg' => 'Purchase updated successfully', 'p_id' => $purchase_id));
                    } else {
                        echo json_encode(array('response' => 'error', 'msg' => 'Please enter all details'));
                    }
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Purchase no already exit'));
                }
            }
        }
    }
    public function actionSearchtransaction2()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $html = '';
        $other = '';
        if (!isset($_POST['searchTerm']) || $_POST['searchTerm'] == '') {
            $specification = $this->actionGetItemCategory1($parent = 0, $spacing = '', $user_tree_array = '');
        } else {
            $search = $_POST['searchTerm'];
            $length = strlen($search);
            $specification = $this->actionGetItemCategorySearch1($parent = 0, $spacing = '', $user_tree_array = '', $search);
            if ($search != '' && $length >= 3) {
                $html = '';
                $html['html'] = '';
                $html['status'] = '';
                $html['unit'] = '';
                if (!empty($specification)) {
                    $i = 1;
                    foreach ($specification as $key => $value) {
                        $spec_sql = "SELECT id, cat_id, brand_id, specification, unit "
                            . " FROM {$tblpx}specification "
                            . " WHERE id=" . $value['id'] . " AND specification_type='A'";
                        $specificationd = Yii::app()->db->createCommand($spec_sql)->queryRow();
                        if ($specificationd['brand_id'] != NULL) {
                            $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specificationd['brand_id'] . "")->queryRow();
                            $brand = '-' . ucwords($brand_details['brand_name']);
                        } else {
                            $brand = '';
                        }
                        $cat_sql = "SELECT category_name "
                            . " FROM {$tblpx}purchase_category "
                            . " WHERE id=" . $specificationd['cat_id'] . "";
                        $category = Yii::app()->db->createCommand($cat_sql)->queryRow();

                        $data = Yii::app()->db->createCommand("SELECT {$tblpx}bills.bill_number, {$tblpx}bills.bill_date, {$tblpx}billitem.billitem_quantity, {$tblpx}billitem.billitem_unit, {$tblpx}billitem.billitem_rate, {$tblpx}billitem.billitem_amount, {$tblpx}purchase.purchase_no, {$tblpx}purchase.vendor_id, {$tblpx}billitem.billitem_length, {$tblpx}billitem.remark FROM  {$tblpx}billitem LEFT JOIN {$tblpx}bills  ON {$tblpx}bills.bill_id={$tblpx}billitem.bill_id LEFT JOIN {$tblpx}purchase_items ON {$tblpx}billitem.purchaseitem_id={$tblpx}purchase_items.item_id LEFT JOIN {$tblpx}purchase ON {$tblpx}purchase_items.purchase_id= {$tblpx}purchase.p_id WHERE {$tblpx}billitem.category_id=" . $value['id'] . " AND {$tblpx}purchase.purchase_type ='A' ORDER BY {$tblpx}billitem.billitem_id desc LIMIT 0,4")->queryAll();
                        if (!empty($data)) {
                            if ($i == 1) {
                                $html['html'] .= '<div class="alert alert-success get_prevdata alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
                                $html['html'] .= '<div id="parent3">
                                                                                            <table style="width:100%;" id="pre_fixtable3">
                                                                                                <thead>
                                                                                                <tr>
                                                                                                    <th style="text-align:center">Sl.No</th>
                                                                                                    <th style="text-align:center">Bill No</th>
                                                                                                    <th style="text-align:center">Purchase No</th>
                                                                                                    <th style="text-align:center">Bill Date</th>
                                                                                                    <th style="text-align:center">Vendor</th>
                                                                                                    <th style="text-align:center">Length</th>
                                                                                                    <th style="text-align:center">Quantity</th>
                                                                                                    <th style="text-align:center">Unit</th>
                                                                                                    <th style="text-align:center">Rate</th>
                                                                                                    <th style="text-align:center">Amount</th>
                                                                                                </tr>
                                                                                                <thead>';
                            }
                            $html['html'] .= '<tbody><tr><td colspan="9"><p>' . ucwords($category['category_name']) . $brand . ' - ' . ucwords($specificationd['specification']) . ' Previous "' . count($data) . '" ​Purchase Rate</p></td></tr>
								';
                            foreach ($data as $key => $value) {
                                $vendor = Vendors::model()->findByPk($value['vendor_id']);
                                $html['html'] .= '<tr class="getprevious" style="cursor:pointer;" data-id="' . $specificationd['id'] . ',' . $value['billitem_quantity'] . ',' . $value['billitem_unit'] . ',' . $value['billitem_rate'] . ',' . $value['billitem_amount'] . ',' . $value['billitem_length'] . ',' . $value['remark'] . '"><td style="text-align:center;">' . ($key + 1) . ' </td><td style="text-align:center;">' . $value['bill_number'] . '</td><td style="text-align:center;">' . $value['purchase_no'] . '</td><td style="text-align:center;">' . date("Y-m-d", strtotime($value['bill_date'])) . '</td><td style="text-align:center;">' . $vendor['name'] . '</td><td style="text-align:center">' . $value['billitem_length'] . '</td><td style="text-align:center;">' . $value['billitem_quantity'] . '</td><td style="text-align:center">' . $value['billitem_unit'] . '</td><td  style="text-align:center;">' . $value['billitem_rate'] . '</td><td style="text-align:center;">' . $value['billitem_amount'] . '</td></tr>';
                            }

                            $html['status'] .= true;
                            $i++;
                        } else {
                            $html['html'] .= '';
                            $html['status'] .= false;
                        }
                        if (!empty($specificationd['unit'])) {
                            $html['unit'] .= $specificationd['unit'];
                        } else {
                            $html['unit'] .= '';
                        }
                    }
                    $html['html'] .= '</tbody></table></div>';
                    $html['html'] .= '</div>';
                }
                $html = $html['html'];
            } else {
                $html = '';
                //$other = array("id"=>'other', "text"=>'Other');
            }
        }
        $data = array();
        foreach ($specification as $key => $value) {
            $data[] = array("id" => $value['id'], "text" => $value['data']);
        }
        $data[] = $other;
        echo json_encode(array('data' => $data, 'html' => $html));
    }

    public function actionCreatenewpurchase1()
    {
        $purchase_id = $_REQUEST['purchase_id'];

        if (isset($_REQUEST['purchaseno']) && isset($_REQUEST['default_date']) && isset($_REQUEST['vendor']) && isset($_REQUEST['project'])) {
            if ($purchase_id == 0) {
                $tblpx = Yii::app()->db->tablePrefix;
                $billing_status = Yii::app()->db->createCommand("SELECT sid FROM {$tblpx}status WHERE status_type='purchase_bill_status' AND caption='PO not issued'")->queryRow();
                $res = Purchase::model()->findAll(array('condition' => 'purchase_no = "' . $_REQUEST['purchaseno'] . '"'));
                if (empty($res)) {
                    $model = new Purchase;
                    $model->purchase_no = $_REQUEST['purchaseno'];
                    $model->expected_delivery_date = !empty($_REQUEST['delivery_date']) ? date('Y-m-d', strtotime($_REQUEST['delivery_date'])) : NULL;
                    $model->project_id = $_REQUEST['project'];
                    $model->vendor_id = $_REQUEST['vendor'];
                    $model->expensehead_id = $_REQUEST['expense_head'];
                    $model->company_id = $_REQUEST['company'];
                    $model->purchase_date = date('Y-m-d', strtotime($_REQUEST['default_date']));
                    $company_details = Company::model()->findByPk(Yii::app()->user->company_id);
                    if ($company_details->company_popermission == 1) {
                        $model->purchase_status = 'permission_needed';
                    } else {
                        $model->purchase_status = 'draft';
                    }
                    $model->purchase_billing_status = $billing_status['sid'];
                    $model->created_date = date('Y-m-d');
                    $model->created_by = Yii::app()->user->id;
                    // $model->company_id = Yii::app()->user->company_id;
                    $model->purchase_type = "G";
                    $model->save(false);
                    $last_id = $model->p_id;
                    echo json_encode(array('response' => 'success', 'msg' => 'Purchase added successfully', 'p_id' => $last_id));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Purchase no already exist'));
                }
            } else {
                $res = Purchase::model()->findAll(array('condition' => 'purchase_no = "' . $_REQUEST['purchaseno'] . '" AND p_id !=' . $purchase_id . ''));
                if (empty($res)) {
                    $model = Purchase::model()->findByPk($purchase_id);
                    $model->purchase_no = $_REQUEST['purchaseno'];
                    $model->expected_delivery_date = !empty($_REQUEST['delivery_date']) ? date('Y-m-d', strtotime($_REQUEST['delivery_date'])) : NULL;
                    $model->project_id = $_REQUEST['project'];
                    $model->vendor_id = $_REQUEST['vendor'];
                    $model->expensehead_id = $_REQUEST['expense_head'];
                    $model->company_id = $_REQUEST['company'];
                    $model->purchase_date = date('Y-m-d', strtotime($_REQUEST['default_date']));
                    $company_details = Company::model()->findByPk(Yii::app()->user->company_id);
                    if ($company_details->company_popermission == 1) {
                        $model->purchase_status = 'permission_needed';
                    } else {
                        $model->purchase_status = 'draft';
                    }
                    $model->created_date = date('Y-m-d');

                    $model->created_by = Yii::app()->user->id;
                    $model->purchase_type = "G";
                    if ($model->save(false)) {
                        echo json_encode(array('response' => 'success', 'msg' => 'Purchase updated successfully', 'p_id' => $purchase_id));
                    } else {
                        echo json_encode(array('response' => 'error', 'msg' => 'Please enter all details'));
                    }
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Purchase no already exit'));
                }
            }
        }
    }

    public function actionSearchtransaction1()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $html = '';
        $other = '';
        if (!isset($_POST['searchTerm']) || $_POST['searchTerm'] == '') {
            $specification = $this->actionGetItemCategoryglass($parent = 0, $spacing = '', $user_tree_array = '');
        } else {
            $search = $_POST['searchTerm'];
            $length = strlen($search);
            $specification = $this->actionGetItemCategorySearchglass($parent = 0, $spacing = '', $user_tree_array = '', $search);
            if ($search != '' && $length >= 3) {

                $html = '';
                $html['html'] = '';
                $html['status'] = '';
                $html['unit'] = '';
                if (!empty($specification)) {
                    $i = 1;
                    foreach ($specification as $key => $value) {
                        $specificationd = Yii::app()->db->createCommand("SELECT id, parent_id, brand_id, specification, unit FROM {$tblpx}purchase_category WHERE id=" . $value['id'] . "")->queryRow();
                        if ($specificationd['brand_id'] != NULL) {
                            $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specificationd['brand_id'] . "")->queryRow();
                            $brand = '-' . ucwords($brand_details['brand_name']);
                        } else {
                            $brand = '';
                        }
                        $category = Yii::app()->db->createCommand("SELECT category_name FROM {$tblpx}purchase_category WHERE id=" . $specificationd['parent_id'] . "")->queryRow();
                        $data = Yii::app()->db->createCommand("SELECT {$tblpx}bills.bill_number, {$tblpx}bills.bill_date, {$tblpx}billitem.billitem_quantity, {$tblpx}billitem.billitem_unit, {$tblpx}billitem.billitem_rate, {$tblpx}billitem.billitem_amount, {$tblpx}purchase.purchase_no, {$tblpx}purchase.vendor_id, {$tblpx}billitem.billitem_width, {$tblpx}billitem.billitem_height, {$tblpx}billitem.remark FROM  {$tblpx}billitem LEFT JOIN {$tblpx}bills  ON {$tblpx}bills.bill_id={$tblpx}billitem.bill_id LEFT JOIN {$tblpx}purchase_items ON {$tblpx}billitem.purchaseitem_id={$tblpx}purchase_items.item_id LEFT JOIN {$tblpx}purchase ON {$tblpx}purchase_items.purchase_id= {$tblpx}purchase.p_id WHERE {$tblpx}billitem.category_id=" . $value['id'] . " AND {$tblpx}purchase.purchase_type ='G' ORDER BY {$tblpx}billitem.billitem_id desc LIMIT 0,4")->queryAll();
                        if (!empty($data)) {
                            if ($i == 1) {
                                $html['html'] .= '<div class="alert alert-success get_prevdata alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
                                $html['html'] .= '<div id="parent3">
                                                                                            <table style="width:100%;" id="pre_fixtable3">
                                                                                                <thead>
                                                                                                <tr>
                                                                                                    <th style="text-align:center">Sl.No</th>
                                                                                                    <th style="text-align:center">Bill No</th>
                                                                                                    <th style="text-align:center">Purchase No</th>
                                                                                                    <th style="text-align:center">Bill Date</th>
                                                                                                    <th style="text-align:center">Vendor</th>
                                                                                                    <th style="text-align:center">Width</th>
                                                                                                    <th style="text-align:center">Height</th>
                                                                                                    <th style="text-align:center">Quantity</th>
                                                                                                    <th style="text-align:center">Unit</th>
                                                                                                    <th style="text-align:center">Rate</th>
                                                                                                    <th style="text-align:center">Amount</th>
                                                                                                </tr>
                                                                                                <thead>';
                            }
                            $html['html'] .= '<tbody><tr><td colspan="9"><p>' . ucwords($category['category_name']) . $brand . ' - ' . ucwords($specificationd['specification']) . ' Previous "' . count($data) . '" ​Purchase Rate</p></td></tr>
								';
                            foreach ($data as $key => $value) {
                                $vendor = Vendors::model()->findByPk($value['vendor_id']);
                                $html['html'] .= '<tr class="getprevious" style="cursor:pointer" data-id="' . $specificationd['id'] . ',' . $value['billitem_quantity'] . ',' . $value['billitem_unit'] . ',' . $value['billitem_rate'] . ',' . $value['billitem_amount'] . ',' . $value['billitem_width'] . ',' . $value['billitem_height'] . ',' . $value['remark'] . '"><td style="text-align:center;">' . ($key + 1) . ' </td><td style="text-align:center;">' . $value['bill_number'] . '</td><td style="text-align:center;">' . $value['purchase_no'] . '</td><td style="text-align:center;">' . date("Y-m-d", strtotime($value['bill_date'])) . '</td><td style="text-align:center;">' . $vendor['name'] . '</td><td style="text-align:center;">' . $value['billitem_width'] . '</td><td style="text-align:center;">' . $value['billitem_height'] . '</td><td style="text-align:center">' . $value['billitem_quantity'] . '</td><td style="text-align:center;">' . $value['billitem_unit'] . '</td><td  style="text-align:center;">' . $value['billitem_rate'] . '</td><td style="text-align:center;">' . $value['billitem_amount'] . '</td></tr>';
                            }

                            $html['status'] .= true;
                            $i++;
                        } else {
                            $html['html'] .= '';
                            $html['status'] .= false;
                        }
                        if (!empty($specificationd['unit'])) {
                            $html['unit'] .= $specificationd['unit'];
                        } else {
                            $html['unit'] .= '';
                        }
                    }
                    $html['html'] .= '</tbody></table></div>';
                    $html['html'] .= '</div>';
                }
                $html = $html['html'];
            } else {
                $html = '';
            }
        }
        $data = array();
        foreach ($specification as $key => $value) {
            $data[] = array("id" => $value['id'], "text" => $value['data']);
        }
        $data[] = $other;
        echo json_encode(array('data' => $data, 'html' => $html));
    }

    public function GetItemunitlist($purchase_itemid)
    {


        $purchase_itemid = $purchase_itemid;
        $final_list = array();
        $ItemUnitlist = UnitConversion::model()->findAll(
            array("condition" => "item_id =  $purchase_itemid", "order" => "id")
        );
        $final_list = array();
        foreach ($ItemUnitlist as $key => $value) {
            $final_list[$key]['id'] = $value['conversion_unit'];
            $final_list[$key]['value'] = $value['conversion_unit'];
        }
        return $final_list;
    }

    public function actiongetUnitconversionFactor()
    {
        if (isset($_POST)) {
            extract($_POST);
            $conversion_value = 0;
            $model = UnitConversion::model()->findByAttributes(array('base_unit' => $base_unit, 'conversion_unit' => $purchase_unit, 'item_id' => $item_id));

            $conversion_value = $model['conversion_factor'];

            echo $conversion_value;
        }
    }


    public function actionPurchaseitem1()
    {

        $data = $_REQUEST['data'];


        $result['html'] = '';


        $tblpx = Yii::app()->db->tablePrefix;
        if ($data['description'] == 'other') {
            $descriptions = $data['remark'];
            $spc_id = 'other';
        } else {
            $spec_sql = "SELECT id, cat_id, specification, brand_id, unit "
                . " FROM {$tblpx}specification "
                . " WHERE id=" . $data['description'] . "";
            $specification = Yii::app()->db->createCommand($spec_sql)->queryRow();
            $parent_sql = "SELECT * FROM {$tblpx}purchase_category "
                . " WHERE id='" . $specification['cat_id'] . "'";
            $parent_category = Yii::app()->db->createCommand($parent_sql)->queryRow();

            if ($specification['brand_id'] != NULL) {
                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                $brand = '-' . $brand_details['brand_name'];
            } else {
                $brand = '';
            }
            $descriptions = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
            $spc_id = $specification['id'];
        }

        if ($data['purchase_id'] == 0) {
            echo json_encode(array('response' => 'error', 'msg' => 'Please enter purchase details'));
        } else {
            $subtot = $data['subtot'];
            $grand = $data['grand'];
            if (isset($data['sl_No'])) {
                $sl_No = $data['sl_No'];
            } else {
                $sl_No = '';
            }
            if (isset($data['description'])) {
                $description = $data['description'];
            } else {
                $description = '';
            }
            if (isset($data['itemwidth'])) {
                $itemwidth = $data['itemwidth'];
            } else {
                $itemwidth = '';
            }
            if (isset($data['itemheight'])) {
                $itemheight = $data['itemheight'];
            } else {
                $itemheight = '';
            }

            if (isset($data['remark'])) {
                $remark = $data['remark'];
            } else {
                $remark = '';
            }

            if (isset($data['quantity'])) {
                $quantity = $data['quantity'];
            } else {
                $quantity = '';
            }
            if (isset($data['unit'])) {
                $unit = $data['unit'];
            } else {
                $unit = '';
            }
            if (isset($data['rate'])) {
                $rate = $data['rate'];
            } else {
                $rate = '';
            }
            if (isset($data['amount'])) {
                $amount = $data['amount'];
            } else {
                $amount = '';
            }
            if (isset($data['tax_slab'])) {
                $tax_slab = $data['tax_slab'];
            } else {
                $tax_slab = '';
            }
            if (isset($data['sgst_amount'])) {
                $sgst_amount = $data['sgst_amount'];
            } else {
                $sgst_amount = '';
            }
            if (isset($data['sgstp'])) {
                $sgstp = $data['sgstp'];
            } else {
                $sgstp = '';
            }
            if (isset($data['cgst_amount'])) {
                $cgst_amount = $data['cgst_amount'];
            } else {
                $cgst_amount = '';
            }
            if (isset($data['cgstp'])) {
                $cgstp = $data['cgstp'];
            } else {
                $cgstp = '';
            }
            if (isset($data['igst_amount'])) {
                $igst_amount = $data['igst_amount'];
            } else {
                $igst_amount = '';
            }
            if (isset($data['igstp'])) {
                $igstp = $data['igstp'];
            } else {
                $igstp = '';
            }
            if (isset($data['discount_amount'])) {
                $discount_amount = $data['discount_amount'];
            } else {
                $discount_amount = '';
            }
            if (isset($data['disp'])) {
                $disp = $data['disp'];
            } else {
                $disp = '';
            }
            if (isset($data['tax_amount'])) {
                $tax_amount = $data['tax_amount'];
            } else {
                $tax_amount = '';
            }
            if (isset($data['total_amount'])) {
                $total_amount = $data['total_amount'];
            } else {
                $total_amount = '';
            }



            $previous_rate = array();
            $permission_status = 1;
            $final_rate = "";
            $class = "";
            $style = "";
            if ($description != 'other') {
                $previous_rate = Yii::app()->db->createCommand("SELECT MAX({$tblpx}billitem.billitem_rate) as max_rate FROM  {$tblpx}billitem LEFT JOIN {$tblpx}bills  ON {$tblpx}bills.bill_id={$tblpx}billitem.bill_id LEFT JOIN {$tblpx}purchase_items ON {$tblpx}billitem.purchaseitem_id={$tblpx}purchase_items.item_id LEFT JOIN {$tblpx}purchase ON {$tblpx}purchase_items.purchase_id= {$tblpx}purchase.p_id WHERE {$tblpx}billitem.category_id=" . $description . " ORDER BY {$tblpx}billitem.billitem_id desc LIMIT 0,4")->queryRow();
            }
            $status = $this->actionGetpurchaseItemStatus();
            $model = Purchase::model()->findByPk($data['purchase_id']);
            $model->total_amount = $grand;
            $model->sub_total = $subtot;
            $model->company_id = Yii::app()->user->company_id;
            //if($model->save()){
            $item_model = new PurchaseItems();
            $item_model->purchase_id = $data['purchase_id'];
            $item_model->quantity = $quantity;
            $item_model->item_width = $data['itemwidth'];
            $item_model->item_height = $data['itemheight'];
            $item_model->unit = $unit;
            $item_model->rate = $rate;
            $item_model->amount = $amount;
            $item_model->category_id = $description;
            $item_model->remark = $remark;
            $item_model->item_status = $status['sid'];
            $item_model->created_by = Yii::app()->user->id;
            $item_model->created_date = date('Y-m-d');

            $item_model->tax_slab = isset($data['tax_slab']) ? $data['tax_slab'] : 0;
            $item_model->discount_amount = isset($data['discount_amount']) ? $data['discount_amount'] : 0;
            $item_model->discount_percentage = isset($data['disp']) ? $data['disp'] : 0;

            $item_model->cgst_amount = isset($data['cgst_amount']) ? $data['cgst_amount'] : 0;
            $item_model->cgst_percentage = isset($data['cgstp']) && !empty($data['cgstp']) ? $data['cgstp'] : 0;
            $item_model->igst_amount = isset($data['igst_amount']) ? $data['igst_amount'] : 0;
            $item_model->igst_percentage = isset($data['igstp']) && !empty($data['igstp']) ? $data['igstp'] : 0;
            $item_model->sgst_amount = isset($data['sgst_amount']) ? $data['sgst_amount'] : 0;
            $item_model->sgst_percentage = isset($data['sgstp']) && !empty($data['sgstp']) ? $data['sgstp'] : 0;
            $item_model->tax_amount = isset($data['tax_amount']) ? $data['tax_amount'] : 0;
            $item_model->tax_perc = $item_model->cgst_percentage + $item_model->igst_percentage + $item_model->sgst_percentage;
            if ($item_model->save()) {
                $item_amount = Yii::app()->db->createCommand("SELECT SUM(amount) as item_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryScalar();
                $qty_total = Yii::app()->db->createCommand("SELECT SUM(quantity) as qty FROM {$tblpx}purchase_items 
                WHERE purchase_id=" . $data['purchase_id'] . "")->queryScalar();

                $model = Purchase::model()->findByPk($data['purchase_id']);
                $model->total_amount = $item_amount;
                $model->sub_total = $item_amount;
                $model->company_id = Yii::app()->user->company_id;
                $model->save(false);

                // permission
                $company_details = Company::model()->findByPk(Yii::app()->user->company_id);
                if ($company_details->company_popermission == 1) {
                    $item_model->permission_status = 2;
                    $permission_status = 2;
                    $class = "rate_highlight";
                    $style = "style='cursor:pointer'";
                } else {
                    if (!empty($previous_rate['max_rate'])) {
                        $company_details = Company::model()->findByPk(Yii::app()->user->company_id);
                        $previous_total = $previous_rate['max_rate'] * $company_details->company_tolerance / 100;
                        $final_rate = $previous_total + $previous_rate['max_rate'];
                        if ($rate > $final_rate) {
                            $item_model->permission_status = 2;
                            $permission_status = 2;
                            $class = "rate_highlight";
                            $style = "style='cursor:pointer'";
                        } else {
                            $item_model->permission_status = 1;
                            $permission_status = 1;
                            $class = "";
                            $style = "";
                        }
                    }
                }
                $item_model->save(false);
                $permission = PurchaseItems::model()->findAll(array("condition" => "purchase_id=" . $data['purchase_id'] . " AND permission_status=2"));
                if ($permission) {
                    Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET 	purchase_status = 'permission_needed' WHERE p_id = '" . $data['purchase_id'] . "'")->execute();
                } else {
                    Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET 	purchase_status = 'draft' WHERE p_id = '" . $data['purchase_id'] . "'")->execute();
                }
                $last_id = $item_model->item_id;
                $result = '';
                $result .= '<tr>';
                $result .= '<td><div id="item_sl_no">' . ($data['sl_no'] + 1) . '</div></td>';
                $result .= '<td><div class="item_description" id="' . $spc_id . '">' . $descriptions . '</div> </td>';
                $result .= '<td class="item_description"> <div class="" id="width"> ' . $itemwidth . '</div> </td>';
                $result .= '<td class="item_description"> <div class="" id="height"> ' . $itemheight . '</div> </td>';
                $result .= '<td class="text-right"> <div class="" id="unit"> ' . $quantity . '</div> </td>';
                $result .= '<td> <div class="unit" id="unit"> ' . $unit . '</div> </td>';
                $result .= '<td class="text-right ' . $class . '" ' . $style . ' id=' . $last_id . '><div class="" id="rate">' . number_format($rate, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($amount, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($tax_slab, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format((float) $sgst_amount, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format((float) $sgstp, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format((float) $cgst_amount, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format((float) $cgstp, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format((float) $igst_amount, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format((float) $igstp, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format((float) $disp, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format((float) $discount_amount, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format((float) $tax_amount, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format((float) $total_amount, 2, '.', '') . '</div></td>';
                $result .= '<td class="item_description"> <div class="" id="remark"> ' . $remark . '</div> </td>';
                $result .= '<td width="70"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>'
                    . '<div class ="popover-content hide"> '
                    . '<ul class="tooltip-hiden">'
                    . '<li><a href="#" id=' . $last_id . ' class="btn btn-default btn-xs removebtn">Delete</a></li>'
                    . '<li><a href="" id=' . $last_id . ' class="btn btn-default btn-xs edit_item" style="margin-left:3px">Edit</a></li>';
                if ($permission_status == 2 && Yii::app()->user->role == 1) {
                    $result .= '<li><a href="" id=' . $last_id . ' class="btn btn-default btn-xs approve_item approveoption_' . $last_id . '"  style="margin-left:3px">Rate Approve</a></li>';
                }
                $result .= '</ul>';
                $result .= '</div> ';
                $result .= '</td>';

                $result .= '</tr>';

                $purchase_rate = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount FROM  {$tblpx}purchase_items WHERE purchase_id =" . $data['purchase_id'] . "")->queryRow();

                $total_discount_value = Yii::app()->db->createCommand("SELECT SUM(discount_amount) as discount_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryRow();
                $total_tax_amount = Yii::app()->db->createCommand("SELECT SUM(tax_amount) as tax_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryRow();
                $grand_total_amount = ($purchase_rate['total_amount'] + $total_tax_amount['tax_amount']) - $total_discount_value['discount_amount'];


                echo json_encode(array(
                    'response' => 'success',
                    'msg' => 'Purchase item save successfully',
                    'item_id' => $last_id,
                    'html' => $result,
                    'final_amount' => number_format($purchase_rate['total_amount'], 2),
                    'qty_total' => $qty_total,
                    'discount_total' => number_format($total_discount_value['discount_amount'], 2),
                    'tax_total' => number_format($total_tax_amount['tax_amount'], 2),
                    'grand_total' => number_format($grand_total_amount, 2)
                ));
            } else {
                echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
            }
        }
    }

    public function actionPurchaseitem2()
    {
        $data = $_REQUEST['data'];
        $result['html'] = '';
        $tblpx = Yii::app()->db->tablePrefix;
        if ($data['description'] == 'other') {
            $descriptions = $data['remark'];
            $spc_id = 'other';
        } else {
            $sql = "SELECT id, cat_id, specification, brand_id, unit"
                . " FROM {$tblpx}specification "
                . " WHERE id=" . $data['description'] . "";
            $specification = Yii::app()->db->createCommand($sql)->queryRow();

            $parent_sql = "SELECT * FROM {$tblpx}purchase_category WHERE id='" . $specification['cat_id'] . "'";
            $parent_category = Yii::app()->db->createCommand($parent_sql)->queryRow();

            if ($specification['brand_id'] != NULL) {
                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                $brand = '-' . $brand_details['brand_name'];
            } else {
                $brand = '';
            }
            $descriptions = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
            $spc_id = $specification['id'];
        }

        if ($data['purchase_id'] == 0) {
            echo json_encode(array('response' => 'error', 'msg' => 'Please enter purchase details'));
        } else {
            $subtot = $data['subtot'];
            $grand = $data['grand'];
            if (isset($data['sl_No'])) {
                $sl_No = $data['sl_No'];
            } else {
                $sl_No = '';
            }
            if (isset($data['description'])) {
                $description = $data['description'];
            } else {
                $description = '';
            }

            if (isset($data['remark'])) {
                $remark = $data['remark'];
            } else {
                $remark = '';
            }

            if (isset($data['quantity'])) {
                $quantity = $data['quantity'];
            } else {
                $quantity = '';
            }
            if (isset($data['length'])) {
                $length = $data['length'];
            } else {
                $length = '';
            }
            if (isset($data['unit'])) {
                $unit = $data['unit'];
            } else {
                $unit = '';
            }
            if (isset($data['rate'])) {
                $rate = $data['rate'];
            } else {
                $rate = '';
            }
            if (isset($data['amount'])) {
                $amount = $data['amount'];
            } else {
                $amount = '';
            }
            if (isset($data['tax_slab'])) {
                $tax_slab = $data['tax_slab'];
            } else {
                $tax_slab = '';
            }
            if (isset($data['sgst_amount'])) {
                $sgst_amount = $data['sgst_amount'];
            } else {
                $sgst_amount = '';
            }
            if (isset($data['sgstp'])) {
                $sgstp = $data['sgstp'];
            } else {
                $sgstp = '';
            }
            if (isset($data['cgst_amount'])) {
                $cgst_amount = $data['cgst_amount'];
            } else {
                $cgst_amount = '';
            }
            if (isset($data['cgstp'])) {
                $cgstp = $data['cgstp'];
            } else {
                $cgstp = '';
            }
            if (isset($data['igst_amount'])) {
                $igst_amount = $data['igst_amount'];
            } else {
                $igst_amount = '';
            }
            if (isset($data['igstp'])) {
                $igstp = $data['igstp'];
            } else {
                $igstp = '';
            }
            if (isset($data['discount_amount'])) {
                $discount_amount = $data['discount_amount'];
            } else {
                $discount_amount = '';
            }
            if (isset($data['disp'])) {
                $disp = $data['disp'];
            } else {
                $disp = '';
            }
            if (isset($data['tax_amount'])) {
                $tax_amount = $data['tax_amount'];
            } else {
                $tax_amount = '';
            }
            if (isset($data['total_amount'])) {
                $total_amount = $data['total_amount'];
            } else {
                $total_amount = '';
            }


            $previous_rate = array();
            $permission_status = 1;
            $final_rate = "";
            $class = "";
            $style = "";
            if ($description != 'other') {
                $previous_rate = Yii::app()->db->createCommand("SELECT MAX({$tblpx}billitem.billitem_rate) as max_rate FROM  {$tblpx}billitem LEFT JOIN {$tblpx}bills  ON {$tblpx}bills.bill_id={$tblpx}billitem.bill_id LEFT JOIN {$tblpx}purchase_items ON {$tblpx}billitem.purchaseitem_id={$tblpx}purchase_items.item_id LEFT JOIN {$tblpx}purchase ON {$tblpx}purchase_items.purchase_id= {$tblpx}purchase.p_id WHERE {$tblpx}billitem.category_id=" . $description . " ORDER BY {$tblpx}billitem.billitem_id desc LIMIT 0,4")->queryRow();
            }
            $spec = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}purchase_category WHERE id=" . $description . "")->queryRow();

            $status = $this->actionGetpurchaseItemStatus();
            //if($model->save()){
            $item_model = new PurchaseItems();
            $item_model->purchase_id = $data['purchase_id'];
            $item_model->quantity = $quantity;
            $item_model->unit = $unit;
            $item_model->rate = $rate;
            $item_model->amount = $amount;
            $item_model->category_id = $description;
            $item_model->remark = $remark;
            $item_model->item_dieno = $spec["dieno"];
            $item_model->item_length = $length;
            $item_model->item_status = $status['sid'];
            $item_model->created_by = Yii::app()->user->id;
            $item_model->created_date = date('Y-m-d');
            $item_model->tax_slab = isset($data['tax_slab']) ? $data['tax_slab'] : 0;
            $item_model->discount_amount = isset($data['discount_amount']) ? $data['discount_amount'] : 0;
            $item_model->discount_percentage = isset($data['disp']) ? $data['disp'] : 0;

            $item_model->cgst_amount = isset($data['cgst_amount']) ? $data['cgst_amount'] : 0;
            $item_model->cgst_percentage = isset($data['cgstp']) && !empty($data['cgstp']) ? $data['cgstp'] : 0;
            $item_model->igst_amount = isset($data['igst_amount']) ? $data['igst_amount'] : 0;
            $item_model->igst_percentage = isset($data['igstp']) && !empty($data['igstp']) ? $data['igstp'] : 0;
            $item_model->sgst_amount = isset($data['sgst_amount']) ? $data['sgst_amount'] : 0;
            $item_model->sgst_percentage = isset($data['sgstp']) && !empty($data['sgstp']) ? $data['sgstp'] : 0;
            $item_model->tax_amount = isset($data['tax_amount']) ? $data['tax_amount'] : 0;
            $item_model->tax_perc = $item_model->cgst_percentage + $item_model->igst_percentage + $item_model->sgst_percentage;
            // permission
            $company_details = Company::model()->findByPk(Yii::app()->user->company_id);
            if ($company_details->company_popermission == 1) {
                $item_model->permission_status = 2;
                $permission_status = 2;
                $class = "rate_highlight";
                $style = "style='cursor:pointer'";
            } else {
                if (!empty($previous_rate['max_rate'])) {
                    $company_details = Company::model()->findByPk(Yii::app()->user->company_id);
                    $previous_total = $previous_rate['max_rate'] * $company_details->company_tolerance / 100;
                    $final_rate = $previous_total + $previous_rate['max_rate'];
                    if ($rate > $final_rate) {
                        $item_model->permission_status = 2;
                        $permission_status = 2;
                        $class = "rate_highlight";
                        $style = "style='cursor:pointer'";
                    } else {
                        $item_model->permission_status = 1;
                        $permission_status = 1;
                        $class = "";
                        $style = "";
                    }
                }
            }
            if ($item_model->save()) {
                $item_amount = Yii::app()->db->createCommand("SELECT SUM(amount) as item_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryScalar();
                $model = Purchase::model()->findByPk($data['purchase_id']);
                $model->total_amount = $item_amount;
                $model->sub_total = $item_amount;
                $model->company_id = Yii::app()->user->company_id;
                $model->save(false);

                $permission = PurchaseItems::model()->findAll(array("condition" => "purchase_id=" . $data['purchase_id'] . " AND permission_status=2"));
                if ($permission) {
                    Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET 	purchase_status = 'permission_needed' WHERE p_id = '" . $data['purchase_id'] . "'")->execute();
                } else {
                    Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET 	purchase_status = 'draft' WHERE p_id = '" . $data['purchase_id'] . "'")->execute();
                }
                $last_id = $item_model->item_id;
                $result = '';
                $result .= '<tr>';
                $result .= '<td><div id="item_sl_no">' . ($data['sl_no'] + 1) . '</div></td>';
                $result .= '<td><div class="item_description" id="' . $spc_id . '">' . $descriptions . '</div> </td>';
                $result .= '<td class="item_length"> <div class="" id="length"> ' . $length . '</div> </td>';
                $result .= '<td class="text-right"> <div class="" id="unit"> ' . $quantity . '</div> </td>';
                $result .= '<td> <div class="unit" id="unit"> ' . $unit . '</div> </td>';
                $result .= '<td class="text-right ' . $class . '" ' . $style . ' id=' . $last_id . '><div class="" id="rate">' . number_format($rate, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($amount, 2, '.', '') . '</div></td>';

                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($tax_slab, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format((float) $sgst_amount, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format((float) $sgstp, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format((float) $cgst_amount, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format((float) $cgstp, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format((float) $igst_amount, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format((float) $igstp, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format((float) $disp, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format((float) $discount_amount, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($tax_amount, 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($total_amount, 2, '.', '') . '</div></td>';

                $result .= '<td class="item_description"> <div class="" id="remark"> ' . $remark . '</div> </td>';
                $result .= '<td width="70"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>'
                    . '<div class ="popover-content hide"> '
                    . '<ul class="tooltip-hiden">'
                    . '<li><a href="#" id=' . $last_id . ' class="removebtn btn btn-default btn-xs">Delete</a></li>'
                    . ' <li><a href="" id=' . $last_id . ' class="edit_item btn btn-default btn-xs" style="margin-left:3px">Edit</a></li>';
                if ($permission_status == 2 && Yii::app()->user->role == 1) {
                    $result .= '<li><a href="" id=' . $last_id . ' class="btn btn-default btn-xs approve_item approveoption_' . $last_id . '" style="margin-left:3px">Rate Approve</a></li>';
                }
                $result .= '</ul>';
                $result .= '</div>';
                $result .= '</td>';

                $result .= '</tr>';
                $purchase_rate = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount 
                FROM  {$tblpx}purchase_items WHERE purchase_id =" . $data['purchase_id'] . "")->queryRow();

                $total_qty = Yii::app()->db->createCommand("SELECT SUM(quantity) as tot_qty FROM  {$tblpx}purchase_items WHERE purchase_id =" . $data['purchase_id'] . "")->queryRow();
                $total_discount_value = Yii::app()->db->createCommand("SELECT SUM(discount_amount) as discount_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryRow();
                $total_tax_amount = Yii::app()->db->createCommand("SELECT SUM(tax_amount) as tax_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryRow();
                $grand_total_amount = ($purchase_rate['total_amount'] + $total_tax_amount['tax_amount']) - $total_discount_value['discount_amount'];
                echo json_encode(array(
                    'response' => 'success',
                    'msg' => 'Purchase item save successfully',
                    'item_id' => $last_id,
                    'html' => $result,
                    'final_amount' => number_format($purchase_rate['total_amount'], 2),
                    'total_qty' => $total_qty['tot_qty'],
                    'discount_total' => number_format($total_discount_value['discount_amount'], 2),
                    'tax_total' => number_format($total_tax_amount['tax_amount'], 2),
                    'grand_total' => number_format($grand_total_amount, 2),
                ));
            } else {
                echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
            }
        }
    }

    public function actionGetItemCategorySearch1($parent = 0, $spacing = '', $user_tree_array = '', $search)
    {

        if (!is_array($user_tree_array))
            $user_tree_array = array();
        $data = array();
        $final = array();
        $tblpx = Yii::app()->db->tablePrefix;
        $where = '';
        $specsql = "SELECT id, specification, brand_id, cat_id "
            . " FROM {$tblpx}specification "
            . " WHERE  specification_type ='A' "
            . " AND company_id=" . Yii::app()->user->company_id . " "
            . " AND  specification LIKE '%" . $search . "%' " . $where . "";

        $specification = Yii::app()->db->createCommand($specsql)->queryAll();
        foreach ($specification as $key => $value) {
            if ($value['brand_id'] != NULL) {
                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $value['brand_id'] . "")->queryRow();
                $brand = '-' . ucfirst($brand_details['brand_name']);
            } else {
                $brand = '';
            }
            $result = $this->actionGetParent($value['cat_id']);
            $final[$key]['data'] = ucfirst($result['category_name']) . $brand . '-' . ucfirst($value['specification']);
            $final[$key]['id'] = $value['id'];
        }
        return $final;
    }

    public function actionPrevioustransaction2()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $html['html'] = '';
        $html['status'] = '';
        $html['unit'] = '';

        if ($_REQUEST['data'] != '' && $_REQUEST['data'] != 'other') {
            $brand = '';
            $criteria = new CDbCriteria();
            $criteria->select = "id, cat_id, brand_id, specification, unit";
            $criteria->condition = "id=" . $_REQUEST['data'] . " AND specification_type = 'A'";
            $specification = Specification::model()->find($criteria);

            if ($specification['brand_id'] != NULL) {
                $criteria = new CDbCriteria();
                $criteria->select = "brand_name";
                $criteria->condition = "id=" . $specification['brand_id'];
                $brand_details = Brand::model()->find($criteria);
                $brand = '-' . ucwords($brand_details['brand_name']);
            }

            if ($specification['cat_id'] != NULL) {
                $criteria = new CDbCriteria();
                $criteria->select = "category_name";
                $criteria->condition = "id=" . $specification['cat_id'];
                $category = PurchaseCategory::model()->find($criteria);
            }

            $data = Yii::app()->db->createCommand()
                ->select('t.*,pi.*,b.bill_number,b.bill_date,p.*')
                ->from($this->tableNameAcc('billitem', 0) . ' as t')
                ->where('t.category_id="' . $_REQUEST['data'] . '" AND p.purchase_type="A"')
                ->leftJoin($this->tableNameAcc('bills', 0) . ' as b', 't.bill_id = b.bill_id')
                ->leftJoin($this->tableNameAcc('purchase_items', 0) . ' as pi', 't.purchaseitem_id = pi.item_id')
                ->leftJoin($this->tableNameAcc('purchase', 0) . ' as p', 'p.p_id = pi.purchase_id')
                ->order('t.billitem_id  DESC')
                ->queryAll();


            if (!empty($data)) {
                $html['html'] = $this->renderPartial(
                    "_previous_data",
                    array(
                        'data' => $data,
                        'category' => $category,
                        'brand' => $brand,
                        'specification' => $specification,
                        'type' => 'A'
                    ),
                    true
                );
                $html['status'] .= true;
            }

            if (!empty($specification['unit'])) {
                $html['unit'] .= $specification['unit']; //get
            }
        } else {
            $html['status'] .= false;
        }

        echo json_encode($html);
    }

    public function actionUpdatepurchaseitem2()
    {
        $data = $_REQUEST['data'];
        $grand = $data['grand'];
        $subtot = $data['subtot'];


        if (isset($data['description'])) {
            $description = $data['description'];
        } else {
            $description = '';
        }

        if (isset($data['remark'])) {
            $remark = $data['remark'];
        } else {
            $remark = '';
        }
        if (isset($data['length'])) {
            $length = $data['length'];
        } else {
            $length = '';
        }
        if (isset($data['quantity'])) {
            $quantity = $data['quantity'];
        } else {
            $quantity = '';
        }
        if (isset($data['unit'])) {
            $unit = $data['unit'];
        } else {
            $unit = '';
        }
        if (isset($data['rate'])) {
            $rate = $data['rate'];
        } else {
            $rate = '';
        }
        if (isset($data['amount'])) {
            $amount = $data['amount'];
        } else {
            $amount = '';
        }
        if (isset($data['total_amount'])) {
            $total_amount = $data['total_amount'];
        } else {
            $total_amount = '';
        }

        $result = '';
        $result['html'] = '';
        $html = '';
        $tblpx = Yii::app()->db->tablePrefix;
        $previous_rate = array();
        if ($description != 'other') {
            $previous_rate = Yii::app()->db->createCommand("SELECT MAX({$tblpx}billitem.billitem_rate) as max_rate FROM  {$tblpx}billitem LEFT JOIN {$tblpx}bills  ON {$tblpx}bills.bill_id={$tblpx}billitem.bill_id LEFT JOIN {$tblpx}purchase_items ON {$tblpx}billitem.purchaseitem_id={$tblpx}purchase_items.item_id LEFT JOIN {$tblpx}purchase ON {$tblpx}purchase_items.purchase_id= {$tblpx}purchase.p_id WHERE {$tblpx}billitem.category_id=" . $data['description'] . " ORDER BY {$tblpx}billitem.billitem_id desc LIMIT 0,4")->queryRow();
        }
        $spec_sql = "SELECT * FROM {$tblpx}specification "
            . " WHERE id=" . $description . "";
        $spec = Yii::app()->db->createCommand($spec_sql)->queryRow();

        if (!empty($data) && $data['item_id'] != 0) {
            $item_model = PurchaseItems::model()->findByPk($data['item_id']);
            $item_model->quantity = $quantity;
            $item_model->amount = $amount;
            $item_model->unit = $unit;
            $item_model->rate = $rate;
            $item_model->category_id = $description;
            $item_model->remark = $remark;
            $item_model->item_dieno = $spec["dieno"];
            $item_model->item_length = $length;
            $item_model->tax_slab = isset($data['tax_slab']) ? $data['tax_slab'] : 0;
            $item_model->discount_amount = isset($data['discount_amount']) ? $data['discount_amount'] : 0;
            $item_model->discount_percentage = isset($data['disp']) ? $data['disp'] : 0;

            $item_model->cgst_amount = isset($data['cgst_amount']) ? $data['cgst_amount'] : 0;
            $item_model->cgst_percentage = isset($data['cgstp']) && !empty($data['cgstp']) ? $data['cgstp'] : 0;
            $item_model->igst_amount = isset($data['igst_amount']) ? $data['igst_amount'] : 0;
            $item_model->igst_percentage = isset($data['igstp']) && !empty($data['igstp']) ? $data['igstp'] : 0;
            $item_model->sgst_amount = isset($data['sgst_amount']) ? $data['sgst_amount'] : 0;
            $item_model->sgst_percentage = isset($data['sgstp']) && !empty($data['sgstp']) ? $data['sgstp'] : 0;
            $item_model->tax_amount = isset($data['tax_amount']) ? $data['tax_amount'] : 0;
            $item_model->tax_perc = $item_model->cgst_percentage + $item_model->igst_percentage + $item_model->sgst_percentage;
            $company_details = Company::model()->findByPk(Yii::app()->user->company_id);
            if ($company_details->company_popermission == 1) {
                $item_model->permission_status = 2;
            } else {
                if (!empty($previous_rate['max_rate'])) {
                    $company_details = Company::model()->findByPk(Yii::app()->user->company_id);
                    $previous_total = $previous_rate['max_rate'] * $company_details->company_tolerance / 100;
                    $final_rate = $previous_total + $previous_rate['max_rate'];
                    if ($rate > $final_rate) {
                        $item_model->permission_status = 2;
                    } else {
                        $item_model->permission_status = 1;
                    }
                }
            }
            if ($item_model->save()) {
                $item_amount = Yii::app()->db->createCommand("SELECT SUM(amount) as item_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryScalar();
                $model = Purchase::model()->findByPk($data['purchase_id']);
                $model->total_amount = $item_amount;
                $model->sub_total = $item_amount;
                $model->company_id = Yii::app()->user->company_id;
                $model->save(false);

                $permission = PurchaseItems::model()->findAll(array("condition" => "purchase_id=" . $data['purchase_id'] . " AND permission_status=2"));
                if ($permission) {
                    Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET 	purchase_status = 'permission_needed' WHERE p_id = '" . $data['purchase_id'] . "'")->execute();
                } else {
                    Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET 	purchase_status = 'draft' WHERE p_id = '" . $data['purchase_id'] . "'")->execute();
                }
                $result = '';
                $style = "";
                $grand_total = 0;
                $purchase_details = Yii::app()->db->createCommand(" SELECT * FROM {$tblpx}purchase_items  WHERE purchase_id=" . $data['purchase_id'] . "")->queryAll();
                $tot_qty = 0;

                foreach ($purchase_details as $key => $values) {
                    $tot_qty += $values['quantity'];
                    $class = "";
                    if ($values['permission_status'] == 'not_approved') {
                        $class = "rate_highlight";
                        $style = "style='cursor:pointer'";
                    } else {
                        $class = "";
                        $style = "";
                    }

                    $grand_total += $values['amount'];

                    if ($values['category_id'] == 0) {

                        $spc_details = $values['remark'];
                        $spc_id = 'other';
                    } else {
                        $sql = "SELECT id, cat_id,brand_id, specification, unit "
                            . " FROM {$tblpx}specification"
                            . "  WHERE id=" . $values['category_id'] . "";
                        $specification = Yii::app()->db->createCommand($sql)->queryRow();
                        $parent_sql = "SELECT * FROM {$tblpx}purchase_category "
                            . " WHERE id='" . $specification['cat_id'] . "'";
                        $parent_category = Yii::app()->db->createCommand($parent_sql)->queryRow();

                        if ($specification['brand_id'] != NULL) {
                            $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                            $brand = '-' . $brand_details['brand_name'];
                        } else {
                            $brand = '';
                        }

                        $spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
                        $spc_id = $specification['id'];
                    }

                    $result .= '<tr>';
                    $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                    $result .= '<td><div class="item_description" id="' . $spc_id . '">' . $spc_details . '</div> </td>';
                    $result .= '<td class="item_length"> <div class="" id="length"> ' . $values['item_length'] . '</div> </td>';
                    $result .= '<td class="text-right"> <div class="" id="quantity"> ' . $values['quantity'] . '</div> </td>';
                    $result .= '<td> <div class="unit" id="unit"> ' . $values['unit'] . '</div> </td>';
                    $result .= '<td class="text-right ' . $class . '" ' . $style . ' id=' . $values['item_id'] . '><div class="" id="rate">' . number_format($values['rate'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['amount'], 2, '.', '') . '</div></td>';

                    $result .= '<td class="text-right"><div class="" id="tax_slab"> ' . number_format($values['tax_slab'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="sgst_amount"> ' . number_format($values['sgst_amount'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="sgst_percentage"> ' . number_format($values['sgst_percentage'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="cgst_amount"> ' . number_format($values['cgst_amount'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="cgst_percentage"> ' . number_format($values['cgst_percentage'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="igst_amount"> ' . number_format($values['igst_amount'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="igst_percentage"> ' . number_format($values['igst_percentage'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="discount_percentage"> ' . number_format($values['discount_percentage'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="discount_amount"> ' . number_format($values['discount_amount'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="tax_amount"> ' . number_format($values['tax_amount'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="total_amount"> ' . number_format(($values['amount'] + $values['tax_amount'] - $values['discount_amount']), 2, '.', '') . '</div></td>';

                    $result .= '<td class="item_description break_word"> <div class="" id="remark"> ' . $values['remark'] . '</div> </td>';
                    $result .= '<td width="70"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>'
                        . '<div class ="popover-content hide"> '
                        . '<ul class="tooltip-hiden">'
                        . '<li><a href="#" id=' . $values['item_id'] . ' class="btn btn-xs btn-default removebtn">Delete</span> </a></li>'
                        . '<li><a href="" id=' . $values['item_id'] . ' class="btn btn-xs btn-default edit_item" style="margin-left:3px">Edit</a></li>';
                    if ($values['permission_status'] == 'not_approved' && Yii::app()->user->role == 1) {
                        $result .= '<li><a href="" id=' . $values['item_id'] . ' class="btn btn-xs btn-default approve_item approveoption_' . $values['item_id'] . '"  style="margin-left:3px">Rate Approve</a></li>';
                    }
                    $result .= '</ul>';
                    $result .= '</div> ';
                    $result .= '</td>';

                    $result .= '</tr>';
                }
                $purchase_rate = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount 
                FROM  {$tblpx}purchase_items WHERE purchase_id =" . $data['purchase_id'] . "")->queryRow();
                $total_discount_value = Yii::app()->db->createCommand("SELECT SUM(discount_amount) as discount_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryRow();
                $total_tax_amount = Yii::app()->db->createCommand("SELECT SUM(tax_amount) as tax_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryRow();
                $grand_total_amount = ($purchase_rate['total_amount'] + $total_tax_amount['tax_amount']) - $total_discount_value['discount_amount'];
                echo json_encode(array(
                    'response' => 'success',
                    'msg' => 'Purchase item update successfully',
                    'html' => $result,
                    'final_amount' => number_format($grand_total, 2, '.', ''),
                    'tot_qty' => $tot_qty,
                    'discount_total' => number_format($total_discount_value['discount_amount'], 2),
                    'tax_total' => number_format($total_tax_amount['tax_amount'], 2),
                    'grand_total' => number_format($grand_total_amount, 2)
                ));
            } else {
                echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
            }
        }
    }

    public function actionRemovepurchaseitem2()
    {
        $data = $_REQUEST['data'];
        $grand = $data['grand'];
        $subtot = $data['subtot'];
        $tblpx = Yii::app()->db->tablePrefix;
        $del = Yii::app()->db->createCommand()->delete($tblpx . 'purchase_items', 'item_id=:id', array(':id' => $data['item_id']));
        if ($del) {
            $item_amount = Yii::app()->db->createCommand(" SELECT sum(amount) as total_amount FROM {$tblpx}purchase_items  WHERE purchase_id=" . $data['purchase_id'] . "")->queryRow();
            Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET total_amount = '" . $item_amount['total_amount'] . "', sub_total = '" . $item_amount['total_amount'] . "' WHERE p_id = '" . $data['purchase_id'] . "'")->execute();

            $result = '';
            $style = "";
            $grand_total = 0;
            $gamount = 0;
            $purchase_details = Yii::app()->db->createCommand(" SELECT * FROM {$tblpx}purchase_items  WHERE purchase_id=" . $data['purchase_id'] . "")->queryAll();
            $tot_qtyy = 0;
            foreach ($purchase_details as $key => $values) {
                $tot_qtyy += $values['quantity'];
                $class = "";
                if ($values['permission_status'] == 'not_approved') {
                    $class = "rate_highlight";
                    $style = "style='cursor:pointer'";
                } else {
                    $class = "";
                    $style = "";
                }

                $grand_total += $values['amount'];
                $gamount = number_format($grand_total, 2, '.', '');

                if ($values['category_id'] == 0) {

                    $spc_details = $values['remark'];
                    $spc_id = 'other';
                } else {
                    $spec_sql = "SELECT id, cat_id,brand_id, specification, unit"
                        . " FROM {$tblpx}specification "
                        . " WHERE id=" . $values['category_id'] . "";
                    $specification = Yii::app()->db->createCommand($spec_sql)->queryRow();
                    $parent_sql = "SELECT * FROM {$tblpx}purchase_category "
                        . " WHERE id='" . $specification['cat_id'] . "'";
                    $parent_category = Yii::app()->db->createCommand($parent_sql)->queryRow();

                    if ($specification['brand_id'] != NULL) {
                        $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                        $brand = '-' . $brand_details['brand_name'];
                    } else {
                        $brand = '';
                    }

                    $spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
                    $spc_id = $specification['id'];
                }

                $result .= '<tr>';
                $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                $result .= '<td><div class="item_description" id="' . $spc_id . '">' . $spc_details . '</div> </td>';
                $result .= '<td class="item_length"> <div class="" id="length"> ' . $values['item_length'] . '</div> </td>';
                $result .= '<td class="text-right"> <div class="" id="quantity"> ' . $values['quantity'] . '</div> </td>';
                $result .= '<td> <div class="unit" id="unit"> ' . $values['unit'] . '</div> </td>';
                $result .= '<td class="text-right ' . $class . '" ' . $style . ' id=' . $values['item_id'] . '><div class="" id="rate">' . number_format($values['rate'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['amount'], 2, '.', '') . '</div></td>';

                $result .= '<td class="text-right"><div class="" id="tax_slab"> ' . number_format($values['tax_slab'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="sgst_amount"> ' . number_format($values['sgst_amount'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="sgst_percentage"> ' . number_format($values['sgst_percentage'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="cgst_amount"> ' . number_format($values['cgst_amount'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="cgst_percentage"> ' . number_format($values['cgst_percentage'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="igst_amount"> ' . number_format($values['igst_amount'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="igst_percentage"> ' . number_format($values['igst_percentage'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="discount_percentage"> ' . number_format($values['discount_percentage'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="discount_amount"> ' . number_format($values['discount_amount'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="tax_amount"> ' . number_format($values['tax_amount'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format(($values['amount'] + $values['tax_amount'] - $values['discount_amount']), 2, '.', '') . '</div></td>';

                $result .= '<td class="item_description break_word"> <div class="" id="remark"> ' . $values['remark'] . '</div> </td>';
                $result .= '<td width="70"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>'
                    . '<div class ="popover-content hide"> '
                    . '<ul class="tooltip-hiden">'
                    . '<li><a href="#" id=' . $values['item_id'] . ' class="removebtn btn btn-xs btn-default">Delete</a></li>'
                    . ' <li><a href="" id=' . $values['item_id'] . ' class="btn btn-xs btn-default edit_item" style="margin-left: 3px;">Edit</a></li>';
                if ($values['permission_status'] == 'not_approved' && Yii::app()->user->role == 1) {
                    $result .= '<li><a href="" id=' . $values['item_id'] . ' class="btn btn-xs btn-default approve_item approveoption_' . $values['item_id'] . '" style="margin-left: 3px;">Rate Approve</a></li>';
                }
                $result .= '</ul>';
                $result .= '</div> ';
                $result .= '</td>';

                $result .= '</tr>';
            }
            $purchase_rate = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount 
                FROM  {$tblpx}purchase_items WHERE purchase_id =" . $data['purchase_id'] . "")->queryRow();
            $total_discount_value = Yii::app()->db->createCommand("SELECT SUM(discount_amount) as discount_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryRow();
            $total_tax_amount = Yii::app()->db->createCommand("SELECT SUM(tax_amount) as tax_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryRow();
            $grand_total_amount = ($purchase_rate['total_amount'] + $total_tax_amount['tax_amount']) - $total_discount_value['discount_amount'];
            echo json_encode(array(
                'response' => 'success',
                'msg' => 'Item deleted successfully',
                'html' => $result,
                'grand_total' => $gamount,
                'tot_qty' => $tot_qtyy,
                'total_discount_value' => $total_discount_value,
                'total_tax_amount' => $total_tax_amount,
                'grand_total_amount' => $grand_total_amount
            ));
        } else {
            echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
        }
    }


    public function actionUpdatepurchaseglass($pid)
    {
        $model = Purchase::model()->find(array("condition" => "p_id = '$pid'"));
        if (empty($model)) {
            $this->redirect(array('purchase/admin'));
        }
        $specification = $this->actionGetItemCategoryglass($parent = 0, $spacing = '', $user_tree_array = '');
        $item_model = PurchaseItems::model()->findAll(array("condition" => "purchase_id = '$pid'"));
        $tblpx = Yii::app()->db->tablePrefix;
        $list_description = Yii::app()->db->createCommand("SELECT DISTINCT description FROM {$tblpx}purchase_items ORDER BY description")->queryAll();
        $project = Yii::app()->db->createCommand("SELECT DISTINCT pid, name  FROM {$tblpx}projects WHERE company_id=" . Yii::app()->user->company_id . " ORDER BY name")->queryAll();
        $vendor = array();
        $expense_head = array();
        if (isset($model->project_id)) {
            $expense_head = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}project_exptype LEFT JOIN {$tblpx}expense_type ON {$tblpx}project_exptype.type_id={$tblpx}expense_type.type_id WHERE {$tblpx}project_exptype.project_id=" . $model->project_id . " AND {$tblpx}expense_type.company_id=" . Yii::app()->user->company_id . "")->queryAll();
        }
        if (isset($model->expensehead_id)) {
            $vendor = Yii::app()->db->createCommand("SELECT v.vendor_id as vendorid, v.name as vendorname FROM " . $tblpx . "vendors v LEFT JOIN " . $tblpx . "vendor_exptype vet ON v.vendor_id = vet.vendor_id WHERE vet.type_id = " . $model->expensehead_id . " AND v.company_id=" . Yii::app()->user->company_id . "")->queryAll();
        }
        $qty_total = Yii::app()->db->createCommand("SELECT SUM(quantity) as qty 
        FROM {$tblpx}purchase_items  WHERE purchase_id=" . $pid . "")->queryScalar();
        $purchase_rate = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount 
        FROM  {$tblpx}purchase_items WHERE purchase_id =" . $pid . "")->queryRow();
        $total_discount_value = Yii::app()->db->createCommand("SELECT SUM(discount_amount) as discount_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $pid . "")->queryRow();
        $total_tax_amount = Yii::app()->db->createCommand("SELECT SUM(tax_amount) as tax_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $pid . "")->queryRow();
        $grand_total_amount = ($purchase_rate['total_amount'] + $total_tax_amount['tax_amount']) - $total_discount_value['discount_amount'];


        $this->render('purchaseglass_update', array(
            'model' => $model,
            'item_model' => $item_model,
            'pid' => $pid,
            'vendor' => $vendor,
            'project' => $project,
            'list_description' => $list_description,
            'specification' => $specification,
            'expense_head' => $expense_head,
            'qty_total' => $qty_total,
            'total_discount_value' => $total_discount_value,
            'total_tax_amount' => $total_tax_amount,
            'grand_total_amount' => $grand_total_amount
        ));
    }



    public function actionUpdatepurchasebylength($pid)
    {
        $model = Purchase::model()->find(array("condition" => "p_id = '$pid'"));
        if (empty($model)) {
            $this->redirect(array('purchase/admin'));
        }
        $specification = $this->actionGetItemCategory1($parent = 0, $spacing = '', $user_tree_array = '');
        $item_model = PurchaseItems::model()->findAll(array("condition" => "purchase_id = '$pid'"));
        $tblpx = Yii::app()->db->tablePrefix;
        $list_description = Yii::app()->db->createCommand("SELECT DISTINCT description FROM {$tblpx}purchase_items ORDER BY description")->queryAll();
        $sql = "SELECT DISTINCT pid, name  FROM {$tblpx}projects ORDER BY name";
        $project = Yii::app()->db->createCommand($sql)->queryAll();
        $vendor = array();
        $expense_head = array();
        if (isset($model->project_id)) {
            $sql = "SELECT * FROM {$tblpx}expense_type ORDER BY type_name";
            $expense_head = Yii::app()->db->createCommand($sql)->queryAll();
        }
        if (isset($model->expensehead_id)) {
            $sql = "SELECT v.vendor_id as vendorid, v.name as vendorname "
                . " FROM " . $tblpx . "vendors v "
                . " LEFT JOIN " . $tblpx . "vendor_exptype vet "
                . " ON v.vendor_id = vet.vendor_id "
                . " WHERE vet.type_id = " . $model->expensehead_id
                . " AND v.company_id=" . Yii::app()->user->company_id;
            $vendor = Yii::app()->db->createCommand($sql)->queryAll();
        }

        $purchase_rate = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount 
                FROM  {$tblpx}purchase_items WHERE purchase_id =" . $pid . "")->queryRow();
        $total_discount_value = Yii::app()->db->createCommand("SELECT SUM(discount_amount) as discount_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $pid . "")->queryRow();
        $total_tax_amount = Yii::app()->db->createCommand("SELECT SUM(tax_amount) as tax_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $pid . "")->queryRow();
        $grand_total_amount = ($purchase_rate['total_amount'] + $total_tax_amount['tax_amount']) - $total_discount_value['discount_amount'];
        $this->render('purchasebylength_update', array(
            'model' => $model,
            'item_model' => $item_model,
            'pid' => $pid,
            'vendor' => $vendor,
            'project' => $project,
            'list_description' => $list_description,
            'specification' => $specification,
            'expense_head' => $expense_head,
            'grand_total_amount' => $grand_total_amount,
            'total_discount_value' => $total_discount_value,
            'total_tax_amount' => $total_tax_amount

        ));
    }
    public function actionGetItemCategorySearchglass($parent = 0, $spacing = '', $user_tree_array = '', $search)
    {

        if (!is_array($user_tree_array))
            $user_tree_array = array();
        $data = array();
        $final = array();
        $tblpx = Yii::app()->db->tablePrefix;
        $where = '';
        $where1 = '';
        $search1 = 'zu';
        $brandids = array();
        $parentids = array();

        $spec_sql = "SELECT id, specification, brand_id, cat_id"
            . " FROM {$tblpx}specification "
            . " WHERE specification_type ='G'  "
            . " AND  specification LIKE '%" . $search . "%' " . $where . "";
        $specification = Yii::app()->db->createCommand($spec_sql)->queryAll();

        foreach ($specification as $key => $value) {
            if ($value['brand_id'] != NULL) {
                $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $value['brand_id'] . "")->queryRow();
                $brand = '-' . ucfirst($brand_details['brand_name']);
            } else {
                $brand = '';
            }
            $result = $this->actionGetParent($value['cat_id']);
            $final[$key]['data'] = ucfirst($result['category_name']) . $brand . '-' . ucfirst($value['specification']);
            $final[$key]['id'] = $value['id'];
        }
        return $final;
    }

    public function actionPrevioustransaction1()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        //$html  = '';
        $html['html'] = '';
        $html['status'] = '';
        $html['unit'] = '';

        if ($_REQUEST['data'] != '' && $_REQUEST['data'] != 'other') {

            $brand = '';
            $criteria = new CDbCriteria();
            $criteria->select = "id, cat_id, brand_id, specification, unit";
            $criteria->condition = "id=" . $_REQUEST['data'] . " AND specification_type = 'G'";
            $specification = Specification::model()->find($criteria);

            if ($specification['brand_id'] != NULL) {
                $criteria = new CDbCriteria();
                $criteria->select = "brand_name";
                $criteria->condition = "id=" . $specification['brand_id'];
                $brand_details = Brand::model()->find($criteria);
                $brand = '-' . ucwords($brand_details['brand_name']);
            }

            if ($specification['cat_id'] != NULL) {
                $criteria = new CDbCriteria();
                $criteria->select = "category_name";
                $criteria->condition = "id=" . $specification['cat_id'];
                $category = PurchaseCategory::model()->find($criteria);
            }

            $data = Yii::app()->db->createCommand()
                ->select('t.*,pi.*,b.bill_number,b.bill_date,p.*')
                ->from($this->tableNameAcc('billitem', 0) . ' as t')
                ->where('t.category_id="' . $_REQUEST['data'] . '" AND p.purchase_type="G"')
                ->leftJoin($this->tableNameAcc('bills', 0) . ' as b', 't.bill_id = b.bill_id')
                ->leftJoin($this->tableNameAcc('purchase_items', 0) . ' as pi', 't.purchaseitem_id = pi.item_id')
                ->leftJoin($this->tableNameAcc('purchase', 0) . ' as p', 'p.p_id = pi.purchase_id')
                ->order('t.billitem_id  DESC')
                ->queryAll();



            if (!empty($data)) {
                $html['html'] = $this->renderPartial(
                    "_previous_data",
                    array(
                        'data' => $data,
                        'category' => $category,
                        'brand' => $brand,
                        'specification' => $specification,
                        'type' => 'G'
                    ),
                    true
                );

                $html['status'] .= true;
            }
            if (!empty($specification['unit'])) {
                $html['unit'] .= $specification['unit'];
            }
        } else {

            $html['html'] .= '';
            $html['status'] .= false;
        }
        echo json_encode($html);
    }


    public function actionUpdatepurchaseitem1()
    {
        $data = $_REQUEST['data'];
        $grand = $data['grand'];
        $subtot = $data['subtot'];

        if (isset($data['description'])) {
            $description = $data['description'];
        } else {
            $description = '';
        }

        if (isset($data['remark'])) {
            $remark = $data['remark'];
        } else {
            $remark = '';
        }
        if (isset($data['itemwidth'])) {
            $itemwidth = $data['itemwidth'];
        } else {
            $itemwidth = '';
        }
        if (isset($data['itemheight'])) {
            $itemheight = $data['itemheight'];
        } else {
            $itemheight = '';
        }

        if (isset($data['quantity'])) {
            $quantity = $data['quantity'];
        } else {
            $quantity = '';
        }
        if (isset($data['unit'])) {
            $unit = $data['unit'];
        } else {
            $unit = '';
        }
        if (isset($data['rate'])) {
            $rate = $data['rate'];
        } else {
            $rate = '';
        }
        if (isset($data['amount'])) {
            $amount = $data['amount'];
        } else {
            $amount = '';
        }

        if (isset($data['tax_slab'])) {
            $tax_slab = $data['tax_slab'];
        } else {
            $tax_slab = '';
        }
        if (isset($data['sgst_amount'])) {
            $sgst_amount = $data['sgst_amount'];
        } else {
            $sgst_amount = '';
        }
        if (isset($data['sgstp'])) {
            $sgstp = $data['sgstp'];
        } else {
            $sgstp = '';
        }
        if (isset($data['cgst_amount'])) {
            $cgst_amount = $data['cgst_amount'];
        } else {
            $cgst_amount = '';
        }
        if (isset($data['cgstp'])) {
            $cgstp = $data['cgstp'];
        } else {
            $cgstp = '';
        }
        if (isset($data['igst_amount'])) {
            $igst_amount = $data['igst_amount'];
        } else {
            $igst_amount = '';
        }
        if (isset($data['igstp'])) {
            $igstp = $data['igstp'];
        } else {
            $igstp = '';
        }
        if (isset($data['discount_amount'])) {
            $discount_amount = $data['discount_amount'];
        } else {
            $discount_amount = '';
        }
        if (isset($data['disp'])) {
            $disp = $data['disp'];
        } else {
            $disp = '';
        }
        if (isset($data['tax_amount'])) {
            $tax_amount = $data['tax_amount'];
        } else {
            $tax_amount = '';
        }
        if (isset($data['total_amount'])) {
            $total_amount = $data['total_amount'];
        } else {
            $total_amount = '';
        }

        $result = '';
        $result['html'] = '';
        $html = '';
        $tblpx = Yii::app()->db->tablePrefix;
        $previous_rate = array();
        if ($description != 'other') {
            $previous_rate = Yii::app()->db->createCommand("SELECT MAX({$tblpx}billitem.billitem_rate) as max_rate FROM  {$tblpx}billitem LEFT JOIN {$tblpx}bills  ON {$tblpx}bills.bill_id={$tblpx}billitem.bill_id LEFT JOIN {$tblpx}purchase_items ON {$tblpx}billitem.purchaseitem_id={$tblpx}purchase_items.item_id LEFT JOIN {$tblpx}purchase ON {$tblpx}purchase_items.purchase_id= {$tblpx}purchase.p_id WHERE {$tblpx}billitem.category_id=" . $description . " ORDER BY {$tblpx}billitem.billitem_id desc LIMIT 0,4")->queryRow();
        }
        if (!empty($data) && $data['item_id'] != 0) {
            $item_model = PurchaseItems::model()->findByPk($data['item_id']);
            $item_model->quantity = $data['quantity'];
            $item_model->item_width = $data['itemwidth'];
            $item_model->item_height = $data['itemheight'];
            $item_model->amount = $data['amount'];
            $item_model->unit = $data['unit'];
            $item_model->rate = $data['rate'];
            $item_model->category_id = $data['description'];
            $item_model->remark = $data['remark'];
            $item_model->tax_slab = isset($data['tax_slab']) ? $data['tax_slab'] : 0;
            $item_model->discount_amount = isset($data['discount_amount']) ? $data['discount_amount'] : 0;
            $item_model->discount_percentage = isset($data['disp']) ? $data['disp'] : 0;

            $item_model->cgst_amount = isset($data['cgst_amount']) ? $data['cgst_amount'] : 0;
            $item_model->cgst_percentage = isset($data['cgstp']) && !empty($data['cgstp']) ? $data['cgstp'] : 0;
            $item_model->igst_amount = isset($data['igst_amount']) ? $data['igst_amount'] : 0;
            $item_model->igst_percentage = isset($data['igstp']) && !empty($data['igstp']) ? $data['igstp'] : 0;
            $item_model->sgst_amount = isset($data['sgst_amount']) ? $data['sgst_amount'] : 0;
            $item_model->sgst_percentage = isset($data['sgstp']) && !empty($data['sgstp']) ? $data['sgstp'] : 0;
            $item_model->tax_amount = isset($data['tax_amount']) ? $data['tax_amount'] : 0;
            $item_model->tax_perc = $item_model->cgst_percentage + $item_model->igst_percentage + $item_model->sgst_percentage;
            $company_details = Company::model()->findByPk(Yii::app()->user->company_id);
            if ($company_details->company_popermission == 1) {
                $item_model->permission_status = 2;
            } else {
                if (!empty($previous_rate['max_rate'])) {
                    $company_details = Company::model()->findByPk(Yii::app()->user->company_id);
                    $previous_total = $previous_rate['max_rate'] * $company_details->company_tolerance / 100;
                    $final_rate = $previous_total + $previous_rate['max_rate'];
                    if ($rate > $final_rate) {
                        $item_model->permission_status = 2;
                    } else {
                        $item_model->permission_status = 1;
                    }
                }
            }

            if ($item_model->save()) {
                $item_amount = Yii::app()->db->createCommand("SELECT SUM(amount) as item_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryScalar();
                $qty_total = Yii::app()->db->createCommand("SELECT SUM(quantity) as qty FROM {$tblpx}purchase_items 
                WHERE purchase_id=" . $data['purchase_id'] . "")->queryScalar();
                $model = Purchase::model()->findByPk($data['purchase_id']);
                $model->total_amount = $item_amount;
                $model->sub_total = $item_amount;
                $model->company_id = Yii::app()->user->company_id;
                $model->save(false);

                $permission = PurchaseItems::model()->findAll(array("condition" => "purchase_id=" . $data['purchase_id'] . " AND permission_status=2"));
                if ($permission) {
                    Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET 	purchase_status = 'permission_needed' WHERE p_id = '" . $data['purchase_id'] . "'")->execute();
                } else {
                    Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET 	purchase_status = 'draft' WHERE p_id = '" . $data['purchase_id'] . "'")->execute();
                }
                $result = '';
                $style = "";
                $grand_total = 0;
                $purchase_details = Yii::app()->db->createCommand(" SELECT * FROM {$tblpx}purchase_items  WHERE purchase_id=" . $data['purchase_id'] . "")->queryAll();

                foreach ($purchase_details as $key => $values) {
                    $class = "";
                    if ($values['permission_status'] == 'not_approved') {
                        $class = "rate_highlight";
                        $style = "style='cursor:pointer'";
                    } else {
                        $class = "";
                        $style = "";
                    }

                    $grand_total += $values['amount'];

                    if ($values['category_id'] == 0) {

                        $spc_details = $values['remark'];
                        $spc_id = 'other';
                    } else {
                        $spec_sql = "SELECT id, cat_id,brand_id, specification, unit"
                            . " FROM {$tblpx}specification "
                            . " WHERE id=" . $values['category_id'] . "";
                        $specification = Yii::app()->db->createCommand($spec_sql)->queryRow();
                        $parent_sql = "SELECT * FROM {$tblpx}purchase_category"
                            . "  WHERE id='" . $specification['cat_id'] . "'";
                        $parent_category = Yii::app()->db->createCommand($parent_sql)->queryRow();

                        if ($specification['brand_id'] != NULL) {
                            $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                            $brand = '-' . $brand_details['brand_name'];
                        } else {
                            $brand = '';
                        }

                        $spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
                        $spc_id = $specification['id'];
                    }
                    $result .= '<tr>';
                    $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                    $result .= '<td><div class="item_description" id="' . $spc_id . '">' . $spc_details . '</div> </td>';
                    $result .= '<td class="item_description"> <div class="" id="width"> ' . $values['item_width'] . '</div> </td>';
                    $result .= '<td class="item_description"> <div class="" id="height"> ' . $values['item_height'] . '</div> </td>';
                    $result .= '<td class="text-right"> <div class="" id="quantity"> ' . $values['quantity'] . '</div> </td>';
                    $result .= '<td> <div class="unit" id="unit"> ' . $values['unit'] . '</div> </td>';
                    $result .= '<td class="text-right ' . $class . '" ' . $style . ' id=' . $values['item_id'] . '><div class="" id="rate">' . number_format($values['rate'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['amount'], 2, '.', '') . '</div></td>';

                    $result .= '<td class="text-right"><div class="" id="tax_slab"> ' . number_format($values['tax_slab'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="sgst_amount"> ' . number_format($values['sgst_amount'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="sgst_percentage"> ' . number_format($values['sgst_percentage'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="cgst_amount"> ' . number_format($values['cgst_amount'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="cgst_percentage"> ' . number_format($values['cgst_percentage'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="igst_amount"> ' . number_format($values['igst_amount'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="igst_percentage"> ' . number_format($values['igst_percentage'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="discount_percentage"> ' . number_format($values['discount_percentage'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="discount_amount"> ' . number_format($values['discount_amount'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="tax_amount"> ' . number_format($values['tax_amount'], 2, '.', '') . '</div></td>';
                    $result .= '<td class="text-right"><div class="" id="discount_amount"> ' . number_format(($values['amount'] + $values['tax_amount'] - $values['discount_amount']), 2, '.', '') . '</div></td>';

                    $result .= '<td class="item_description break_word"> <div class="" id="remark"> ' . $values['remark'] . '</div> </td>';
                    $result .= '<td width="70"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>'
                        . '<div class ="popover-content hide"> '
                        . '<ul class="tooltip-hiden">'
                        . '<li><a href="#" id=' . $values['item_id'] . ' class="btn btn-default btn-xs removebtn">Delete</a></li>'
                        . '<li><a href="" id=' . $values['item_id'] . ' class="btn btn-default btn-xs  edit_item" style="margin-left:3px">Edit</a></li>';
                    if ($values['permission_status'] == 'not_approved' && Yii::app()->user->role == 1) {
                        $result .= '<li><a href="" id=' . $values['item_id'] . ' class="btn btn-default btn-xs approve_item approveoption_' . $values['item_id'] . '"  style="margin-left:3px">Rate Approve</a></li>';
                    }
                    $result .= '</ul>';
                    $result .= '</div> ';
                    $result .= '</td>';

                    $result .= '</tr>';
                }


                $purchase_rate = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount FROM  {$tblpx}purchase_items WHERE purchase_id =" . $data['purchase_id'] . "")->queryRow();

                $total_discount_value = Yii::app()->db->createCommand("SELECT SUM(discount_amount) as discount_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryRow();
                $total_tax_amount = Yii::app()->db->createCommand("SELECT SUM(tax_amount) as tax_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryRow();
                $grand_total_amount = ($purchase_rate['total_amount'] + $total_tax_amount['tax_amount']) - $total_discount_value['discount_amount'];

                echo json_encode(array(
                    'response' => 'success',
                    'msg' => 'Purchase item update successfully',
                    'html' => $result,
                    'final_amount' => number_format($grand_total, 2, '.', ''),
                    'qty_total' => $qty_total,
                    'discount_total' => number_format($total_discount_value['discount_amount'], 2),
                    'tax_total' => number_format($total_tax_amount['tax_amount'], 2),
                    'grand_total' => number_format($grand_total_amount, 2)
                ));
            } else {
                echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
            }
        }
    }

    public function actionRemovepurchaseitem1()
    {

        $data = $_REQUEST['data'];
        $grand = $data['grand'];
        $subtot = $data['subtot'];
        $tblpx = Yii::app()->db->tablePrefix;
        $del = Yii::app()->db->createCommand()->delete($tblpx . 'purchase_items', 'item_id=:id', array(':id' => $data['item_id']));

        if ($del) {
            $item_amount = Yii::app()->db->createCommand(" SELECT sum(amount) as total_amount FROM {$tblpx}purchase_items  WHERE purchase_id=" . $data['purchase_id'] . "")->queryRow();

            $qty_total = Yii::app()->db->createCommand(" SELECT sum(quantity) as qty FROM {$tblpx}purchase_items 
             WHERE purchase_id=" . $data['purchase_id'] . "")->queryRow();
            Yii::app()->db->createCommand("UPDATE {$tblpx}purchase SET total_amount = '" . $item_amount['total_amount'] . "', sub_total = '" . $item_amount['total_amount'] . "' WHERE p_id = '" . $data['purchase_id'] . "'")->execute();

            $result = '';
            $style = "";
            $grand_total = 0;
            $gamount = 0;

            $purchase_details = Yii::app()->db->createCommand(" SELECT * FROM {$tblpx}purchase_items  WHERE purchase_id=" . $data['purchase_id'] . "")->queryAll();

            foreach ($purchase_details as $key => $values) {
                $class = "";
                if ($values['permission_status'] == 'not_approved') {
                    $class = "rate_highlight";
                    $style = "style='cursor:pointer'";
                } else {
                    $class = "";
                    $style = "";
                }

                $grand_total += $values['amount'];
                $gamount = number_format($grand_total, 2, '.', '');

                if ($values['category_id'] == 0) {

                    $spc_details = $values['remark'];
                    $spc_id = 'other';
                } else {
                    $spec_sql = "SELECT id, cat_id,brand_id, specification, unit FROM {$tblpx}specification WHERE id=" . $values['category_id'] . "";
                    $specification = Yii::app()->db->createCommand($spec_sql)->queryRow();
                    $parent_sql = "SELECT * FROM {$tblpx}purchase_category WHERE id='" . $specification['cat_id'] . "'";
                    $parent_category = Yii::app()->db->createCommand($parent_sql)->queryRow();

                    if ($specification['brand_id'] != NULL) {
                        $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
                        $brand = '-' . $brand_details['brand_name'];
                    } else {
                        $brand = '';
                    }

                    $spc_details = $parent_category['category_name'] . $brand . ' - ' . $specification['specification'];
                    $spc_id = $specification['id'];
                }
                $result .= '<tr>';
                $result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
                $result .= '<td><div class="item_description" id="' . $spc_id . '">' . $spc_details . '</div> </td>';
                $result .= '<td class="item_description"> <div class="" id="width"> ' . $values['item_width'] . '</div> </td>';
                $result .= '<td class="item_description"> <div class="" id="height"> ' . $values['item_height'] . '</div> </td>';
                $result .= '<td class="text-right"> <div class="" id="quantity"> ' . $values['quantity'] . '</div> </td>';
                $result .= '<td> <div class="unit" id="unit"> ' . $values['unit'] . '</div> </td>';
                $result .= '<td class="text-right ' . $class . '" ' . $style . ' id=' . $values['item_id'] . '><div class="" id="rate">' . number_format($values['rate'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['amount'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['tax_slab'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['sgst_amount'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['sgst_percentage'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['cgst_amount'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['cgst_percentage'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['igst_amount'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['igst_percentage'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['discount_percentage'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['discount_amount'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['tax_amount'], 2, '.', '') . '</div></td>';
                $result .= '<td class="text-right"><div class="" id="amount"> ' . number_format(($values['amount'] + $values['tax_amount'] - $values['discount_amount']), 2, '.', '') . '</div></td>';


                $result .= '<td class="item_description break_word"> <div class="" id="remark"> ' . $values['remark'] . '</div> </td>';
                $result .= '<td width="70"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>'
                    . '<div class ="popover-content hide"> '
                    . '<ul class="tooltip-hiden">'
                    . '<li><a href="#" id=' . $values['item_id'] . ' class="removebtn btn btn-xs btn-default" >Delete</a></li>'
                    . ' <li><a href="" id=' . $values['item_id'] . ' class="edit_item btn btn-xs btn-default" style="margin-left: 3px;">Edit</a></li>';
                if ($values['permission_status'] == 'not_approved' && Yii::app()->user->role == 1) {
                    $result .= '<li><a href="" id=' . $values['item_id'] . ' class="btn btn-xs btn-default approve_item approveoption_' . $values['item_id'] . '" style="margin-left: 3px;">Rate Approve</a></li>';
                }
                $result .= '</ul>';
                $result .= '</div> ';
                $result .= '</td>';

                $result .= '</tr>';
            }

            $purchase_rate = Yii::app()->db->createCommand("SELECT SUM(amount) as total_amount FROM  {$tblpx}purchase_items WHERE purchase_id =" . $data['purchase_id'] . "")->queryRow();

            $total_discount_value = Yii::app()->db->createCommand("SELECT SUM(discount_amount) as discount_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryRow();
            $total_tax_amount = Yii::app()->db->createCommand("SELECT SUM(tax_amount) as tax_amount FROM {$tblpx}purchase_items WHERE purchase_id=" . $data['purchase_id'] . "")->queryRow();
            $grand_total_amount = ($purchase_rate['total_amount'] + $total_tax_amount['tax_amount']) - $total_discount_value['discount_amount'];

            echo json_encode(array(
                'response' => 'success',
                'msg' => 'Item deleted successfully',
                'html' => $result,
                'grand_total' => $gamount,
                'qty_total' => $qty_total['qty'],
                'discount_total' => number_format($total_discount_value['discount_amount'], 2),
                'tax_total' => number_format($total_tax_amount['tax_amount'], 2),
                'grand_total' => number_format($grand_total_amount, 2)
            ));
        } else {
            echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
        }
    }

    public function actionAddremarks()
    {
        $purchaseId = $_REQUEST["purchaseid"];
        $remarks = $_REQUEST["remark"];
        $model = new Remark;
        $model->remark_remark = $remarks;
        $model->remark_postedby = Yii::app()->user->id;
        $model->remark_posteddate = date("Y-m-d H:i:s");
        $model->purchase_id = $purchaseId;
        $model->remark_status = 1;
        $tblpx = Yii::app()->db->tablePrefix;
        if ($model->save()) {
            $sql = "SELECT * FROM {$tblpx}remark WHERE purchase_id = " . $purchaseId
                . " ORDER BY remark_id DESC";
            $remarksList = Yii::app()->db->createCommand($sql)->queryAll();
            $result = "";
            foreach ($remarksList as $remark) {
                $users = Users::model()->findByPk($remark["remark_postedby"]);
                $result .= '<div class="ind_remarks">';
                $result .= '<p><cite>By: ' . $users["username"] . '.</cite><span class="pull-right"> ' . $remark["remark_posteddate"] . '</span></p>';
                $result .= '<p>' . $remark["remark_remark"] . '</p>';
                $result .= '</div>';
            }
        }
        echo $result;
    }

    public function actionViewremarks()
    {
        $purchaseId = $_REQUEST["purchaseid"];
        $tblpx = Yii::app()->db->tablePrefix;
        $sql = "SELECT * FROM {$tblpx}remark WHERE purchase_id = " . $purchaseId
            . " ORDER BY remark_id DESC";
        $remarksList = Yii::app()->db->createCommand($sql)->queryAll();
        $purchase = Purchase::model()->find(array("condition" => "p_id = '$purchaseId'"));
        $result = "";
        $comment = "";
        foreach ($remarksList as $remark) {
            $users = Users::model()->findByPk($remark["remark_postedby"]);
            $result .= '<div class="ind_remarks">';
            $result .= '<p><cite>By: ' . $users["username"] . '.</cite><span class="pull-right"> ' . $remark["remark_posteddate"] . '</span></p>';
            $result .= '<p>' . $remark["remark_remark"] . '</p>';
            $result .= '</div>';
        }

        if ($purchase["decline_comment"] != "") {
            $comment .= '<h4 class="head_remarks">Decline Comment</h4>';
            $comment .= '<p class="border-top-1"></p>';
            if ($purchase["declined_by"] != "") {
                $users = Users::model()->findByPk($purchase["declined_by"]);
                $comment .= '<p><cite>By: ' . $users["first_name"] . ' ' . $users["last_name"] . '.</cite><span class="pull-right"> ' . $purchase["declined_date"] . '</span></p>';
            }
            $comment .= '<p><br>' . $purchase["decline_comment"] . '</p>';
        }

        echo json_encode(array(
            'result' => $result,
            'po_no' => $purchase->purchase_no,
            'comment' => $comment
        ));
    }

    public function getArchPurchaseNo($template, $company)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $prNo = "";
        if ($template == 'TYPE-4') { //Archadeo
            $sql = "SELECT count(*)  FROM `jp_purchase` "
                . " WHERE `purchase_no` IS NOT NULL";
            $counter = Yii::app()->db->createCommand($sql)->queryScalar();

            if ($counter == 0) {
                $purchaseno = str_pad($counter + 1, 3, "0", STR_PAD_LEFT);
                $prNo = date('y') . "/ARCH/PO-" . $purchaseno;
            } else {
                $sql = "SELECT purchase_no FROM `jp_purchase`  "
                    . " WHERE `purchase_no` IS NOT NULL "
                    . " ORDER BY p_id DESC LIMIT 1";
                $last_po_no = Yii::app()->db->createCommand($sql)->queryScalar();

                $poArray = explode('-', $last_po_no);
                $next_po = str_pad($poArray[1] + 1, 3, "0", STR_PAD_LEFT);
                $prNo = date('y') . "/ARCH/PO-" . $next_po;

            }
        } else if ($template == 'TYPE-3') {  //Amzer 
            $nextPO = Controller::checkPOAutoIncrement();
            $prNo = 'AMZ/PO-' . $nextPO . '/' . date('Y');
            $html['purchase_no'] = $prNo;
        } else if ($template == 'TYPE-6') { //Kapcher

            $futureDate = date('y', strtotime('+1 year'));
            $sql = "SELECT count(*)  FROM `jp_purchase` "
                . " WHERE `purchase_no` IS NOT NULL";
            $counter = Yii::app()->db->createCommand($sql)->queryScalar();

            if ($counter == 0) {
                $purchaseno = str_pad($counter + 1, 3, "0", STR_PAD_LEFT);
                $prNo = "CDIPL/PO/" . date('Y') . "-" . $futureDate . "/" . $purchaseno;
            } else {
                $sql = "SELECT purchase_no FROM `jp_purchase`  "
                    . " WHERE `purchase_no` IS NOT NULL "
                    . " ORDER BY p_id DESC LIMIT 1";
                $last_po_no = Yii::app()->db->createCommand($sql)->queryScalar();


                $poArray = explode('/', $last_po_no);
                $next_po = str_pad($poArray[3] + 1, 3, "0", STR_PAD_LEFT);
                $prNo = "CDIPL/PO/" . date('Y') . "-" . $futureDate . "/" . $next_po;
            }
        } else {
            $prNo = "";
            if ($company->auto_purchaseno == 1) {
                $maxData = Yii::app()->db->createCommand("SELECT MAX(CAST(purchase_no as signed)) as largenumber FROM {$tblpx}purchase")->queryRow();
                $maxNo = $maxData["largenumber"];
                $newPONo = $maxNo + 1;
                $prNo = $newPONo;
                do {
                    $purchase_no = Purchase::model()->findAll(array("condition" => "purchase_no = '$prNo'"));
                    if (empty($purchase_no)) {
                        $status = 1;
                    } else {
                        $prNo = $prNo + 1;
                        $status = 2;
                    }
                } while ($status == 2);

            } else {
                $prNo = "";
            }
        }

        return $prNo;

    }


    public function actionadditionalCharge()
    {
        if (isset($_POST)) {
            $purchase_id = $_POST['pid'];
            $category = $_POST['category'];
            $amount = $_POST['amount'];

            $tblpx = Yii::app()->db->tablePrefix;
            $addPoCharges = Yii::app()->db->createCommand(" INSERT INTO " . $tblpx . "additional_po_charge(`id`, `purchase_id`, `category`, `amount`, `created_by`, `created_date`)VALUES (null, " . $purchase_id . ",'" . $category . "'," . $amount . "," . yii::app()->user->id . ",'" . date('Y-m-d') . "')");

            $data = $addPoCharges->execute();
            if ($data) {
                $sql = "SELECT * FROM `jp_additional_po_charge` WHERE `purchase_id` =" . $purchase_id;
                $addchargesall = Yii::app()->db->createCommand($sql)->queryAll();
                $charges = "";
                if (!empty($addchargesall)) {
                    foreach ($addchargesall as $data) {
                        $charges .= '<div class="display_additional" style="display:flex;"> 
							<p style="font-weight: bold;width: 100px;">' .
                            $data['category'] . ' : 
							</p> 
							  <span class="totalprice1" style="width: 70px;">' .
                            Controller::money_format_inr($data['amount'], 2) . '
							</span>&nbsp;&nbsp;<span class="icon icon-trash deletecharge" title="Delete" id="' . $data['id'] . '" purchase-id="' . $data['purchase_id'] . '">
							</span>
						</div>';
                    }
                }

                $total_additional_charge = Yii::app()->db->createCommand("SELECT SUM(amount)  FROM `jp_additional_po_charge` WHERE `purchase_id` = " . $purchase_id)->queryScalar();

                echo json_encode(array('charges' => $charges, 'total_additional_charge' => $total_additional_charge));

            }
        }
    }

    public function actiondeleteadditionalCharge()
    {

        if (isset($_POST)) {
            $id = $_POST['id'];

            $tblpx = Yii::app()->db->tablePrefix;
            $delpo_charge = Yii::app()->db->createCommand("DELETE FROM " . $tblpx . "additional_po_charge WHERE `id`=" . $id);

            $data = $delpo_charge->execute();
            if ($data) {
                $total_additional_charge = 0;
                if (isset($_POST['purchase_id'])) {
                    $total_additional_charge = Yii::app()->db->createCommand("SELECT SUM(amount)  FROM `jp_additional_po_charge` WHERE `purchase_id` = " . $_POST['purchase_id'])->queryScalar();
                }

                echo json_encode(array('total_additional_charge' => Yii::app()->Controller->money_format_inr($total_additional_charge, 2)));
            }
        }
    }


    public function actionDynamiccompany()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $html['html'] = '';
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
        }
        $companyInfo = Company::model()->findAll(array('condition' => $newQuery));
        $html['html'] .= "<option value=''>Choose Company</option>";
        if (!empty($companyInfo)) {
            foreach ($companyInfo as $cData) {
                $selected = '';
                if (isset($_REQUEST['company_id']) && $_REQUEST['company_id'] != "") {
                    $selected = ($_REQUEST['company_id'] == $cData["id"]) ? ' selected=selected' : '';
                } else {
                    $selected = '';
                    if (isset($_REQUEST['popup'])) {
                        $sql = "SELECT `id` FROM {$tblpx}company "
                            . " ORDER BY id DESC LIMIT 1";
                        $id = Yii::app()->db->createCommand($sql)->queryScalar();
                        $selected = ($id == $cData["id"]) ? ' selected=selected' : '';
                    }
                }

                $html['html'] .= "<option value='" . $cData["id"] . "' '" . $selected . "'>" . $cData["name"] . "</option>";
            }
        }
        echo json_encode($html);
    }

    public function actionloadPoItemsModal($mrId = '', $itemId = '')
    {
        if (isset($_GET['mrId'])) {
            $mrId = $_GET['mrId'];
        }
        if (isset($_GET['itemId'])) {
            $itemId = $_GET['itemId'];
            // $mr_materials_model = MaterialRequisitionItems::Model()->findByPk($itemId);
            // if(!empty($mr_materials_model)){
            //     $material_id=$mr_materials_model->material_item_id;
            // }

        }
        $this->layout = '//layouts/blank';
        $specification = $this->actionGetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');

        $purchaseitemDataProvider = PurchaseItems::model()->search(); // You may adjust the search criteria as per your requirements

        $this->render('_purchase_items_form', array('purchaseitemDataProvider' => $purchaseitemDataProvider, 'specification' => $specification, 'mrId' => $mrId, 'itemId' => $itemId));

    }
    public function actionGetProjectByMrId()
    {
        $mrId = Yii::app()->request->getPost('mrId');
        $company_id = Yii::app()->request->getPost('companyId');
        $tblpx = Yii::app()->db->tablePrefix;
        $html = ''; // Initialize the HTML variable
        $auto_puno = '';
        $html_arr = '';
        $org_prefix_slash='';
        if (!empty($mrId) && !empty($company_id)) {
            $materialReq = MaterialRequisition::model()->findByPk($mrId);
            $company = Company::model()->findByPk($company_id);
            if (!empty($company->purchase_order_format)) {
                $html_arr['auto_pono'] = 2;
                $purchase_order_format = trim($company["purchase_order_format"]);
                $general_po_format_arr = explode('/', $purchase_order_format);
                // print_r($general_po_format_arr);exit;
                $org_prefix = $general_po_format_arr[0];
                $prefix_key = 0;
                $po_key = '';
                $current_key = '';
                $org_prefix_slash=$general_po_format_arr[0].'/';
                foreach ($general_po_format_arr as $key => $value) {

                    if ($value == '{po_sequence}') {
                        $po_key = $key;
                    } else if ($value == '{year}') {
                        $current_key = $key;
                    }
                }

                $current_year = date('Y');
                $previous_year = date('Y') - 1;
                $next_year = date('Y') + 1;

                $where = '';
                if ($current_key == 2) {
                    $where .= " AND `purchase_no` LIKE '%$current_year'";
                } else if ($current_key == 1) {
                    $where .= " AND `purchase_no` LIKE '%$current_year%'";
                }
                $sql = "SELECT * from " . $tblpx . "purchase  WHERE `purchase_no` LIKE $org_prefix_slash%' $where  AND  company_id = " . $company_id . " ORDER BY `p_id` DESC LIMIT 1;";
                //echo "<pre>";print_r($sql);exit;
                $previous_po_no = '';
                $po_seq_no = 1;
                $po_seq_prefix = 'PO-';
                $previous_data = Yii::app()->db->createCommand($sql)->queryRow();
                if (!empty($previous_data)) {
                    $previous_po_no = $previous_data['purchase_no'];
                    $prefix_arr = explode('/', $previous_po_no);
                    $prefix = $prefix_arr[0];
                    $previous_prefix_seq_no = $prefix_arr[$po_key];
                    $po_split = explode('-', $previous_prefix_seq_no);
                    $previous_prefix_seq_no_val = $po_split[1];
                    $po_seq_no = $previous_prefix_seq_no_val + 1;

                }
                $digit = strlen((string) $po_seq_no);
                if ($this->getActiveTemplate() == 'TYPE-4' && ENV == 'production') {
                    $po_no = $this->getArchInvoiceNo();

                } else {
                    if ($digit == 1) {
                        $po_no = $po_seq_prefix . '00' . $po_seq_no;
                    } else if ($digit == 2) {
                        $po_no = $po_seq_prefix . '0' . $po_seq_no;
                    } else {
                        $po_no = $po_seq_prefix . $po_seq_no;
                    }
                }

                $new_invoice = str_replace('{year}', $current_year, $purchase_order_format);
                $new_po_no = str_replace('{po_sequence}', $po_no, $new_invoice);

                $sql = "SELECT * from " . $tblpx . "purchase  WHERE `purchase_no` LIKE '$new_po_no%' $where ORDER BY `p_id` DESC LIMIT 1;";
                $duplicate_data = Yii::app()->db->createCommand($sql)->queryRow();
                if (!empty($duplicate_data)) {
                    $previous_po_no = $duplicate_data['purchase_no'];
                    $prefix_arr = explode('/', $previous_po_no);
                    $prefix = $prefix_arr[0];
                    $previous_prefix_seq_no = $prefix_arr[$po_key];
                    $po_split = explode('-', $previous_prefix_seq_no);
                    $previous_prefix_seq_no_val = $po_split[1];
                    $po_seq_no = $previous_prefix_seq_no_val + 1;
                    $digit = strlen((string) $po_seq_no);
                    if ($this->getActiveTemplate() == 'TYPE-4' && ENV == 'production') {
                        $po_no = $this->getArchInvoiceNo();

                    } else {
                        if ($digit == 1) {
                            $po_no = $po_seq_prefix . '00' . $po_seq_no;
                        } else if ($digit == 2) {
                            $po_no = $po_seq_prefix . '0' . $po_seq_no;
                        } else {
                            $po_no = $po_seq_prefix . $po_seq_no;
                        }
                    }

                }
                $new_invoice = str_replace('{year}', $current_year, $purchase_order_format);
                $new_po_no = str_replace('{po_sequence}', $po_no, $new_invoice);

                $html_arr['purchase_no'] = $new_po_no;
            } else if ($company->purchase_order_format == NULL && $company->auto_purchaseno == 1) {
                $html_arr['auto_pono'] = $company->auto_purchaseno;
            }
            $html_arr['html_arr'] = '';
            $html_arr['status'] = '';


            $activeProjectTemplate = $this->getActiveTemplate();


            if ($company->purchase_order_format == NULL && $company->auto_purchaseno == 1) {
                $maxData = Yii::app()->db->createCommand("SELECT MAX(CAST(purchase_no as signed)) as largenumber FROM {$tblpx}purchase")->queryRow();
                $maxNo = $maxData["largenumber"];
                $newPONo = $maxNo + 1;
                $prNo = $newPONo;
                do {
                    $purchase_no = Purchase::model()->findAll(array("condition" => "purchase_no = '$prNo'"));
                    if (empty($purchase_no)) {
                        $status = 1;
                    } else {
                        $prNo = $prNo + 1;
                        $status = 2;
                    }
                } while ($status == 2);
                $html_arr['purchase_no'] = $prNo;
            } else if (!empty($company->purchase_order_format)) {
                $html_arr['auto_pono'] = 2;
                $purchase_order_format = trim($company["purchase_order_format"]);
                $general_po_format_arr = explode('/', $purchase_order_format);
                // print_r($general_po_format_arr);exit;
                $org_prefix = $general_po_format_arr[0];
                $prefix_key = 0;
                $po_key = '';
                $current_key = '';
                $org_prefix_slash=$general_po_format_arr[0].'/';
                foreach ($general_po_format_arr as $key => $value) {

                    if ($value == '{po_sequence}') {
                        $po_key = $key;
                    } else if ($value == '{year}') {
                        $current_key = $key;
                    }
                }

                $current_year = date('Y');
                $previous_year = date('Y') - 1;
                $next_year = date('Y') + 1;

                $where = '';
                if ($current_key == 2) {
                    $where .= " AND `purchase_no` LIKE '%$current_year'";
                } else if ($current_key == 1) {
                    $where .= " AND `purchase_no` LIKE '%$current_year%'";
                }
                $sql = "SELECT * from " . $tblpx . "purchase  WHERE `purchase_no` LIKE '$org_prefix_slash%' $where   AND  company_id = " . $company_id . " ORDER BY `p_id` DESC LIMIT 1;";
                //echo "<pre>";print_r($sql);exit;
                $previous_po_no = '';
                $po_seq_no = 1;
                $po_seq_prefix = 'PO-';
                $previous_data = Yii::app()->db->createCommand($sql)->queryRow();
                if (!empty($previous_data)) {
                    $previous_po_no = $previous_data['purchase_no'];
                    $prefix_arr = explode('/', $previous_po_no);
                    $prefix = $prefix_arr[0];
                    $previous_prefix_seq_no = $prefix_arr[$po_key];
                    $po_split = explode('-', $previous_prefix_seq_no);
                    $previous_prefix_seq_no_val = $po_split[1];
                    $po_seq_no = $previous_prefix_seq_no_val + 1;

                }
                $digit = strlen((string) $po_seq_no);
                if ($this->getActiveTemplate() == 'TYPE-4' && ENV == 'production') {
                    $po_no = $this->getArchInvoiceNo();

                } else {
                    if ($digit == 1) {
                        $po_no = $po_seq_prefix . '00' . $po_seq_no;
                    } else if ($digit == 2) {
                        $po_no = $po_seq_prefix . '0' . $po_seq_no;
                    } else {
                        $po_no = $po_seq_prefix . $po_seq_no;
                    }
                }

                $new_invoice = str_replace('{year}', $current_year, $purchase_order_format);
                $new_po_no = str_replace('{po_sequence}', $po_no, $new_invoice);
                //die($new_po_no);
                $html_arr['purchase_no'] = $new_po_no;
            } else {
                $html_arr['purchase_no'] = '';
            }

            if (!empty($materialReq)) {
                $project = Projects::model()->findByPk($materialReq->project_id);
                if (!empty($project)) {
                    $selected = "selected"; // Variable to hold the selected attribute if the project matches

                    // Build the HTML for the option tag with the selected attribute if applicable
                    $html .= '<option value="' . $project->pid . '" ' . $selected . '>' . $project->name . '</option>';
                }
            }
        }

        echo CJSON::encode(array('html' => $html, 'html_arr' => $html_arr));
        Yii::app()->end();
    }

}
