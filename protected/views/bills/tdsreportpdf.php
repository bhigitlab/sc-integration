<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/tablestyles.css" />
<link rel="stylesheet" type="text/css" href="uploads/new_template/css/newstyle.css" />
<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }
    /*.container{max-width: 1050px;margin:0 auto;padding: 15px;border: 1px solid #ddd;}*/
    .text-right{text-align:right}
    .details,.info-table{border: 1px solid #212121;margin-top: 20px;}
    .details td, .info-table td,.info-table th{padding: 6px 8px; }
    .info-table{margin-bottom: 10px;}
    .text-center{text-align:center;}
    .img-hold{width: 10%;}
    .companyhead{font-size: 18px;font-weight:bold;margin-top:0px;color: #789AD1;}
</style>
<div class="container" id="project">
    <?php
    $company_address = Company::model()->findBypk(($company_id)?$company_id:Yii::app()->user->company_id);	
    ?>
    <table border=0>
        <tbody>
                <tr>
                <td class="img-hold"><img  src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" alt="img" style='margin-top:-45px;'/></td>
                    <td class=text-right>GST NO: <?php echo isset($company_address["company_gstnum"])?$company_address["company_gstnum"]:''; ?></td>
                </tr>
        </tbody>
    </table>
    <table border=0>
        <tbody>
            <tr>
                <td class="text-center"><p class='companyhead'><?php echo isset($company_address['name'])?$company_address['name']:''; ?></p><br>
                    <p><?php echo ( (isset($company_address['address']) && !empty($company_address['address'])) ? ($company_address['address']) : ""); ?><br>
                    PIN : <?php echo ( (isset($company_address['pincode']) && !empty($company_address['pincode'])) ?($company_address['pincode']) : ""); ?>,&nbsp;
                    PHONE : <?php echo ( (isset($company_address['phone']) && !empty($company_address['phone'])) ?( $company_address['phone']) : ""); ?><br>
                    EMAIL : <?php echo ( (isset($company_address['email_id']) && !empty($company_address['email_id'])) ?( $company_address['email_id']) : ""); ?></p><br>                        
                    <h3>TDS Report</h3>   
                </td>
            </tr>
        </tbody>
    </table>
    <div style="position:relative">
    <?php if(count($tdsdata) > 0) { ?>
    <table border="1">
        <tr>
            <?php if(!empty($date_from)) { ?>
            <td>From: <?php echo date("d/m/Y", strtotime($date_from)); ?></td>
            <?php } if(!empty($date_to)) { ?>
            <td>To: <?php echo date("d/m/Y", strtotime($date_to)); ?></td>
            <?php } if(!empty($vendor_id)) {
                $vendor = Vendor::model()->findByPk($vendor_id);
            ?>
            <td>Vendor: <?php echo $vendor->name; ?></td>
            <?php } if(!empty($subcontractor_id)) {
                $subcontractor = Subcontractor::model()->findByPk($subcontractor_id);
            ?>
            <td>Subcontractor: <?php echo $subcontractor->subcontractor_name; ?></td>
            <?php } if(!empty($company_id)) {
            $company = Company::model()->findByPk($company_id);
            ?>
            <td>Company: <?php echo $company->company_id; ?></td>
            <?php } ?>
        </tr>
    </table>
    <div id="parent">
      <table class="table table-bordered " id="fixTable" border="1">
          <thead>
              <tr>
                  <th>Sl No</th>
                  <th>Project</th>
                  <th>Date</th>
                  <th>Vendor</th>
                  <th>Subcontractor</th>
                  <th>Amount Paid</th>
                  <th>TDS (%)</th>
                  <th>TDS Amount</th>
                  <th>Balance</th>
              </tr>
              <?php
                $totalpaid          = 0;
                $totaltdsp          = 0;
                $totaltdsamount     = 0;
                $totalpaidamount    = 0;
  
                foreach($tdsdata as $data) {
                    $totalpaid          = $totalpaid + $data["paid"];
                    $totaltdsp          = $totaltdsp + $data["tdsp"];
                    $totaltdsamount     = $totaltdsamount + $data["tdsamount"];
                    $totalpaidamount    = $totalpaidamount + $data["balance"];
                }
                $averagetdsp = $totaltdsp / count($tdsdata);

?>
              <tr>
                    <th colspan="5" class="text-right">Total</th>
                    <th class="text-right"><?php echo Controller::money_format_inr($totalpaid, 2); ?></th>
                    <th class="text-right"><?php echo Controller::money_format_inr($averagetdsp, 2); ?></th>
                    <th class="text-right"><?php echo Controller::money_format_inr($totaltdsamount, 2); ?></th>
                    <th class="text-right"><?php echo Controller::money_format_inr($totalpaidamount, 2); ?></th>
                </tr>
            </thead>
            <tbody>
            <?php
                $totalpaid          = 0;
                $totaltdsp          = 0;
                $totaltdsamount     = 0;
                $totalpaidamount    = 0;
                $i                  = 1;
                foreach($tdsdata as $data) {
                    $totalpaid          = $totalpaid + $data["paid"];
                    $totaltdsp          = $totaltdsp + $data["tdsp"];
                    $totaltdsamount     = $totaltdsamount + $data["tdsamount"];
                    $totalpaidamount    = $totalpaidamount + $data["balance"];
                ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $data["project"]; ?></td>
                        <td><?php echo date("d-m-Y", strtotime($data["date"])); ?></td>
                        <td><?php echo $data["vendor"]; ?></td>
                        <td><?php echo $data["subcontractor"]; ?></td>
                        <td class="text-right"><?php echo Controller::money_format_inr(floatval(round($data["paid"], 2)), 2); ?></td>
                        <td class="text-right"><?php echo Controller::money_format_inr(floatval(round($data["tdsp"], 2)), 2); ?></td>
                        <td class="text-right"><?php echo Controller::money_format_inr(floatval(round($data["tdsamount"], 2)), 2); ?></td>
                        <td class="text-right"><?php echo Controller::money_format_inr(floatval(round($data["balance"],2 )), 2); ?></td>
                    </tr>
                <?php
                  $i = $i + 1;
                }
                $averagetdsp = $totaltdsp / count($tdsdata);
                ?>
            </tbody>

            <!-- <tfoot>
                <tr>
                    <th colspan="5" class="text-right">Total</th>
                    <th class="text-right"><?php echo Controller::money_format_inr($totalpaid, 2); ?></th>
                    <th class="text-right"><?php echo Controller::money_format_inr($averagetdsp, 2); ?></th>
                    <th class="text-right"><?php echo Controller::money_format_inr($totaltdsamount, 2); ?></th>
                    <th class="text-right"><?php echo Controller::money_format_inr($totalpaidamount, 2); ?></th>
                </tr>
            </tfoot> -->
      </table>
    </div>
    <div style="right: 3px;position: absolute;top: -19px;">
      Total <?php echo count($tdsdata); ?> results
    </div>
    <?php } else {
        echo "No records found.";
    } ?>
  </div>

</div>
<style>
    #parent,#parent2 {max-height: 400px;}
    #parent table th, #parent2 table th{background-color: #eee;}
     .table-bordered>thead>tr>th{background-color: rgb(238, 238, 238) !important;}
    .list-view .sorter {margin-bottom: 0px;}
    .table{margin-bottom:0px;}
</style>
