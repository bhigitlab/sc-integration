<div class="table-responsive">
    <table cellpadding="10" class="table  list-view " id="fixtable">
        <thead class="entry-table">
            <tr>
                <th>SL No</th>
                <th>Vendor</th>
                <th>Receipt No</th>
                <th>Date</th>
                <th>Item Description</th>
                <th>Qty in Nos</th>
                <th>Rate</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $daterange = '';
            if (!empty($date_from) || !empty($date_to)) {
                if ((isset($date_from) && !empty($date_from)) && ((isset($date_to) && empty($date_to)) || !isset($date_to))) {
                    $daterange .= ' WHERE `created_date` >="' . $date_from . '"';
                } else if ((isset($date_to) && !empty($date_to)) && ((isset($date_from) && empty($date_from)) || !isset($date_from))) {
                    $daterange .= ' WHERE `created_date` <="' . $date_to . '"';
                } else if ((isset($date_to) && !empty($date_to)) && (isset($date_from) && !empty($date_from))) {
                    $daterange .= 'WHERE created_date >="' . $date_from . '" AND created_date <="' . $date_to . '"';
                } else {
                    $daterange .= '';
                }
            }

            $tblpx = Yii::app()->db->tablePrefix;
            $sql = "SELECT * FROM jp_warehousereceipt_items " . $daterange;
            $despatch_items = Yii::app()->db->createCommand($sql)->queryAll();
            $i = 1;
            foreach ($despatch_items as $key => $value) {
                ?>
                <tr>
                    <td>
                        <?php echo $i; ?>
                    </td>
                    <td>
                        <?php
                        $vendor = "";
                        $warehouse_receipt = Warehousereceipt::model()->findByPk($value['warehousereceipt_id']);
                        $bill = Bills::model()->findByPk($warehouse_receipt['warehousereceipt_bill_id']);
                        if ($bill["purchase_id"] != NULL) {
                            $tblpx = Yii::app()->db->tablePrefix;
                            $sql = "SELECT {$tblpx}projects.name as project_name,"
                                . " {$tblpx}vendors.name as vendor_name "
                                . " FROM {$tblpx}purchase LEFT JOIN {$tblpx}projects "
                                . " ON {$tblpx}purchase.project_id = {$tblpx}projects.pid "
                                . " LEFT JOIN {$tblpx}vendors "
                                . " ON {$tblpx}purchase.vendor_id={$tblpx}vendors.vendor_id  "
                                . " WHERE {$tblpx}purchase.p_id = " . $bill["purchase_id"];
                            $purchase_data = Yii::app()->db->createCommand($sql)->queryRow();
                            $vendor = $purchase_data['vendor_name'];
                        }
                        echo !empty($vendor) ? $vendor : "N/A";
                        ?>
                    </td>
                    <td>
                        <?php
                        $warehouse_receipt = Warehousereceipt::model()->findByPk($value['warehousereceipt_id']);

                        echo CHtml::link($warehouse_receipt->warehousereceipt_no, 'index.php?r=wh/warehousereceipt/view&receiptid=' . $warehouse_receipt['warehousereceipt_id'], array('class' => 'link', 'target' => '_blank'));
                        ?>
                    </td>
                    <td>
                        <?php echo date("d-m-Y", strtotime($value['created_date'])); ?>
                    </td>

                    <td>
                        <?php
                        $spec_sql = "SELECT id, specification, brand_id, cat_id "
                            . " FROM {$tblpx}specification "
                            . "WHERE id=" . $value['warehousereceipt_itemid'];
                        $specification = Yii::app()->db->createCommand($spec_sql)->queryRow();

                        $brand = '';
                        if ($specification['brand_id'] != NULL) {
                            $sql = "SELECT brand_name FROM {$tblpx}brand "
                                . "WHERE id=" . $specification['brand_id'];
                            $brand_details = Yii::app()->db->createCommand($sql)->queryRow();
                            $brand = '-' . ucwords($brand_details['brand_name']);
                        }
                        $category_name = "";
                        if ($specification['cat_id'] != "") {
                            $category = PurchaseCategory::model()->findByPk($specification['cat_id']);
                            $category_name = $category->category_name;
                        }
                        $item = ucwords($category_name) . $brand . '-' . ucwords($specification['specification']);
                        echo $item;
                        ?>
                    </td>
                    <td>
                        <?php echo $value['warehousereceipt_effective_quantity']; ?>
                    </td>
                    <td class="text-right">
                        <?php echo $value['warehousereceipt_rate']; ?>
                    </td>
                    <td class="text-right">
                        <?php echo $value['warehousereceipt_effective_quantity'] * $value['warehousereceipt_rate']; ?>
                    </td>
                </tr>
                <?php
                $i++;
            }
            ?>
        </tbody>

    </table>
</div>