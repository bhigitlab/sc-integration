<?php
/* @var $this ClientsController */
/* @var $model Clients */

$this->breadcrumbs = array(
	'Buyers' => array('index'),
	$model->name => array('view', 'id' => $model->id),
	'Update',
);

?>

<div class="panel panel-gray">
	<div class="panel-heading form-head">
		<h3 class="panel-title">Edit Buyer</h3>
	</div>
	<?php echo $this->renderPartial('_form', array('model' => $model, 'users' => $users, 'projects' => $projects, 'flat_numbers' => $flat_numbers)); ?>
</div>