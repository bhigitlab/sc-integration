<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript"
	src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>

<script>
	var shim = (function () { document.createElement('datalist'); })();
</script>
<style>
	.lefttdiv {
		float: left;
	}

	.select2 {
		width: 100%;
	}

	.company_id {
		display: inline;
	}
</style>
<script>
	$(function () {
		var update_date = $('#datepicker').val();
		$("#datepicker").datepicker({ dateFormat: 'dd-mm-yy' }).datepicker("setDate", new Date(update_date));

	});

</script>
<style>
	.dropdown {
		position: relative;
		display: inline-block;
	}

	.dropdown::before {
		position: absolute;
		content: " \2193";
		top: 0px;
		right: -8px;
		height: 20px;
		width: 20px;
	}

	button#caret {
		border: none;
		background: none;
		position: absolute;
		top: 0px;
		right: 0px;
	}

	.invoicemaindiv th,
	.invoicemaindiv td {
		padding: 10px;
		text-align: left;
	}

	.addcolumn {
		cursor: pointer;
		background-color: black;
		border: none;
		color: white;
		text-align: center;
		text-decoration: none;
		margin: 2px;
		font-size: 30px;
		width: 40px;
	}

	.invoicemaindiv .pull-right,
	.invoicemaindiv .pull-left {
		float: none !important;
	}


	.checkek_edit {
		pointer-events: none;
	}



	.add_selection {
		background: #000;
	}




	.toast-container {
		width: 350px;
	}

	.toast-position-top-right {
		top: 57px;
		right: 6px;
	}

	.toast-item-close {
		background-image: none;
		cursor: pointer;
		width: 12px;
		height: 12px;
		text-align: center;
		border-radius: 2px;
	}

	.toast-item-image {
		font-size: 24px;
	}

	.toast-item-close:hover {
		color: red;
	}

	.toast-item {
		border: transparent;
		border-radius: 3px;
		font-size: 10px;
		opacity: 1;
		background-color: rgba(34, 45, 50, 0.8);
	}

	.toast-item-wrapper p {
		margin: 0px 5px 0px 42px;
		font-size: 14px;
		text-align: justify;
	}


	.toast-type-success {
		background-color: #00A65A;
		border-color: #00A65A;
	}

	.toast-type-error {
		background-color: #DD4B39;
		border-color: #DD4B39;
	}

	.toast-type-notice {
		background-color: #00C0EF;
		border-color: #00C0EF;
	}

	.toast-type-warning {
		background-color: #F39C12;
		border-color: #F39C12;
	}

	.span_class {
		min-width: 70px;
		display: inline-block;
	}



	.remark {
		display: none;
	}



	th {
		height: auto;
	}


	.client_data {
		margin-top: 6px;
	}

	#previous_details {
		position: fixed;
		top: 94px;
		left: 70px;
		right: 70px;
		z-index: 2;
		max-width: 1150px;
		margin: 0 auto;
	}

	.text_align {
		text-align: center;
	}

	*:focus {
		border: 1px solid #333;
		box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
	}

	.tooltip-hiden {
		width: auto;
	}

	@media(max-width: 767px) {


		.quantity,
		.rate,
		.small_class {
			width: auto !important;
		}




		#tax_amount,
		#item_amount {
			display: inline-block;
			float: none;
			text-align: right;
		}
	}

	@media(min-width: 767px) {
		.invoicemaindiv .pull-right {
			float: right !important;
		}

		.invoicemaindiv .pull-left {
			float: left !important;
		}
	}

	span.required {
		color: red;
	}
</style>
<div class="container">
	<div class="invoicemaindiv">
		<h3>Sales Book </h3>
		<div id="msg_box"></div>
		<form id="pdfvals1" method="post" action="<?php echo $this->createAbsoluteUrl('purchase/addpurchase'); ?>">
			<input type="hidden" name="remove" id="remove" value="">
			<input type="hidden" name="invoice_id" id="invoice_id" value="<?php echo $model->invoice_id; ?>">
			<div class="entries-wrapper">
				<div class="row">
					<div class="col-xs-12">
						<div class="heading-title">Add Details</div>
						<div class="dotted-line"></div>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-xs-12 col-sm-4 col-md-3">
						<label>COMPANY </label>
						<select name="company_id" class="inputs target company_id form-control" id="company_id">
							<option value="">Choose Company</option>
							<?php
							$user = Users::model()->findByPk(Yii::app()->user->id);
							$arrVal = explode(',', $user->company_id);
							$newQuery = "";
							foreach ($arrVal as $arr) {
								if ($newQuery)
									$newQuery .= ' OR';
								$newQuery .= " FIND_IN_SET('" . $arr . "', id)";
							}
							$companyInfo = Company::model()->findAll(array('condition' => $newQuery));

							foreach ($companyInfo as $key => $value) {

								?>
								<?php if ($model->company_id == null && ANCAPMS_LINKED) {
									$project_arr = Projects::model()->findByPk($model->project_id);
									?>
									<option value="<?php echo $value['id']; ?>" <?= $value['id'] == $project_arr->company_id ? 'selected' : '' ?>>
										<?php echo $value['name']; ?>
									</option>

								<?php } else { ?>
									<option value="<?php echo $value['id']; ?>" <?= $value['id'] == $model->company_id ? 'selected' : '' ?>>
										<?php echo $value['name']; ?>
									</option>

								<?php }
								?>
								<?php
							}
							?>
						</select>
						<?php
						$tblpx = Yii::app()->db->tablePrefix;
						$user = Users::model()->findByPk(Yii::app()->user->id);
						$arrVal = explode(',', $user->company_id);
						$newQuery = "";
						$newQuery1 = "";
						foreach ($arrVal as $arr) {
							if ($newQuery)
								$newQuery .= ' OR';
							if ($newQuery1)
								$newQuery1 .= ' OR';
							$newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
							$newQuery1 .= " FIND_IN_SET('" . $arr . "', p.company_id)";
						}
						//  $company = Company::model()->findByPk($model->company_id);
						if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) {
							$project_ = Yii::app()->db->createCommand("SELECT pid, name  FROM {$tblpx}projects WHERE " . $newQuery . " ORDER BY name")->queryAll();
						} else {
							$project_ = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}projects p JOIN {$tblpx}project_assign pa ON p.pid = pa.projectid WHERE " . $newQuery1 . " AND pa.userid = '" . Yii::app()->user->id . "'")->queryAll();
						}


						//  echo $company['name']; ?>
					</div>
					<div class="form-group col-xs-12 col-sm-4 col-md-3">
						<label>PROJECT </label>
						<select name="project" class="inputs target project form-control" id="project">
							<option value="">Choose Project</option>
							<?php
							foreach ($project_ as $key => $value) {
								?>
								<option value="<?php echo $value['pid']; ?>" <?= $value['pid'] == $project->pid ? 'selected' : '' ?>>
									<?php echo $value['name']; ?>
								</option>
							<?php } ?>

						</select>
						<?php //echo $project->name; ?>
					</div>
					<div class="form-group col-xs-12 col-sm-3 col-md-2 margin-bottom-26">
						<label>CLIENT </label>
						<div><span id="client_name">
								<?php echo (isset($client->name)) ? $client->name : ''; ?>
							</span></div>
					</div>
					<div class="form-group col-xs-12 col-sm-3 col-md-2">
						<label>DATE </label>
						<input type="text" value="<?= $model->date ?>" id="datepicker"
							class="txtBox date inputs target form-control" name="date"
							placeholder="Please click to edit">
					</div>
					<div class="form-group col-xs-12 col-sm-4 col-md-2">
						<label>INVOICE NO </label>
						<?php

						if ($model->status_id == 108 && ANCAPMS_LINKED) { ?>
							<input type="text" required value="<?php echo $model->inv_no; ?>"
								class="txtBox inputs target check_type inv_no  form-control change_val" name="inv_no"
								id="inv_no" readonly>
						<?php } else { ?>

							<div>
								<?php echo $model->inv_no; ?>
							</div>
						<?php } ?>

					</div>
					<div class="form-group col-xs-12 text-right">
						<a id="sales_edit_form" class="btn btn-primary btn-sm">Submit</a>
					</div>
				</div>
				<div class="clearfix"></div>
				<br>
				<div id="msg"></div>

				<div id="previous_details"></div>

				<div class="purchase_items" <?php if (ANCAPMS_LINKED) { ?> style="display:none;" <?php } ?>>
					<div class="row">
						<div class="col-xs-12">
							<div class="heading-title">Sales Book Entries</div>
							<div class="dotted-line"></div>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-xs-12 col-sm-3 col-md-2">
							<label>Type <span class="required">*</span> </label>
							<?php
							echo CHtml::activeDropDownList(
								$newmodel,
								'type',
								array('0' => 'By Lumpsum', '1' => 'By Quantity * Rate'),
								array(
									'empty' => 'Select Type',
									'class' => 'form-control',
									'style' => 'pointer-events: auto;'
								)
							);
							?>
						</div>
						<div class="form-group col-xs-12 col-sm-3 col-md-2 ">
							<label>Description <span class="required">*</span> </label>
							<input type="text" class="inputs target txtBox description form-control" id="description"
								name="description[]" placeholder="" /></td>
						</div>
						<div class="form-group col-xs-12 col-sm-3 col-md-2 ">
							<label>HSN Code <span class="required">*</span> </label>
							<input type="text" class="inputs target txtBox hsn_code form-control" id="hsn_code"
								name="hsn_code[]" placeholder="" /></td>
						</div>
						<!-- ANCAPMS_LINKED -->
						<?php if (ANCAPMS_LINKED) { ?>
							<div class="form-group col-xs-12 col-sm-3 col-md-2 ">
								<label>Hours <span class="required">*</span> </label>
								<input type="text" class="inputs target txtBox description form-control" id="hours"
									name="hours[]" placeholder="" /></td>
							</div>
							<?php
							$workTypes = WorkType::model()->findAll();
							$workTypeList = array();

							foreach ($workTypes as $workType) {
								$workTypeList[$workType->wtid] = $workType->work_type;
							} ?>
							<div class="form-group col-xs-12 col-sm-3 col-md-2 ">
								<label>Work type <span class="required">*</span> </label>
								<?php echo CHtml::dropDownList('selectedWorkType', null, $workTypeList, array('empty' => 'Select Work Type', 'class' => 'form-control'));
								?>
							</div>
							<div class="form-group col-xs-12 col-sm-3 col-md-2 ">
								<label>Rate card <span class="required">*</span> </label>
								<?php $rateCards = RateCard::model()->findAll();
								$rateCardList = CHtml::listData($rateCards, 'id', 'rate');
								echo CHtml::dropDownList('selectedRateCard', null, $rateCardList, array('empty' => 'Select Rate Card', 'class' => 'form-control'));
								?>
							</div>
						<?php } ?>
						<!-- ANCAPMS_LINKED ends -->
						<div class="form-group col-xs-12 col-sm-3 col-md-2 quantity_div lumpsum_sec">
							<label>Amount <span class="required">*</span> </label>
							<input type="text"
								class="inputs target txtBox amount allownegativenumericdecimal form-control" id="amount"
								name="amount[]" placeholder=" <?php echo (ANCAPMS_LINKED) ? 'readonly' : ''; ?>" />
							</td>
						</div>
						<!-- qty*rate -->
						<div class="form-group col-xs-12 col-sm-3 col-md-2 quantity_div qty_rate_sec">
							<label>Quantity <span class="required">*</span> </label>
							<input type="text" class="inputs target txtBox quantity allownumericdecimal form-control"
								id="quantity" name="quantity[]" placeholder=" <?php echo (ANCAPMS_LINKED) ? 'readonly' : ''; ?>" /></td>
						</div>
						<div class="form-group col-xs-12 col-sm-3 col-md-2 qty_rate_sec">
							<label>Units <span class="required">*</span> </label>
							<input type="text" class="inputs target txtBox unit form-control" id="unit" name="unit[]"
								placeholder="" /></td>
						</div>
						<div class="form-group col-xs-12 col-sm-3 col-md-2 qty_rate_sec">
							<label>Rate</label>
							<input type="text"
								class="inputs target txtBox rate allownegativenumericdecimal form-control" id="rate"
								name="rate[]" placeholder="" />
						</div>
						<!-- qty*rate ends -->

					</div>
					<div class="row">

						<div class="form-group col-xs-12 col-md-2 gsts">
							<label>SGST(%)</label>
							<div class="input-info">
								<input type="text"
									class="inputs target small_class txtBox sgst allownumericdecimal calculation form-control"
									id="sgst" name="sgst[]" placeholder="" />
								<div id="sgst_amount" class="value-label">0.00</div>
							</div>
						</div>

						<div class="form-group col-xs-12 col-md-2 gsts">
							<label>CGST(%)</label>
							<div class="input-info">
								<input type="text"
									class="inputs target txtBox cgst small_class allownumericdecimal calculation form-control"
									id="cgst" name="cgst[]" placeholder="" />
								<div id="cgst_amount" class="value-label">0.00</div>
							</div>
						</div>

						<div class="form-group col-xs-12 col-md-2 gsts">
							<label>IGST(%)</label>
							<div class="input-info">

								<input type="text"
									class="inputs target txtBox igst  small_class allownumericdecimal calculation form-control"
									id="igst" name="igst[]" placeholder="" />
								<div id="igst_amount" class="value-label">0.00</div>
							</div>
						</div>


					</div>
					<div class="row width-sm-40 width-100 pull-right">
						<div class="form-group col-xs-12 sub-heading amt_sec margin-bottom-10">
							<label class="final-section-label">Amount</label>
							<span id="item_amount" class="value-label final-label-info">0.00</span>
						</div>
						<div class="form-group col-xs-12 sub-heading amt_sec">
							<label class="final-section-label">TAX Amount</label>
							<div id="tax_amount" class="value-label final-label-info">0.00</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-xs-12 text-right">
							<input type="button" class="item_save btn btn-info" id="0" value="Save">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div id="faux-table" class="faux-table" aria="hidden"></div>
						<div class="table-wrap table-responsive">
							<table border="1" class="table main-table" id="main-table">
								<thead class="entry-table sticky-thead">
									<tr>
										<th>Sl.No</th>
										<th>Description</th>
										<th>HSN Code</th>
										<!-- qty * rate -->
										<th>Quantity</th>
										<th>Unit</th>
										<th>Rate</th>
										<!-- qty*rate -->
										<th>Type</th>
										<th>SGST</th>
										<th>SGST Amount</th>
										<th>CGST</th>
										<th>CGST Amount</th>
										<th>IGST</th>
										<th>IGST Amount</th>
										<?php if (ANCAPMS_LINKED) { ?>
											<th>Hours</th>
											<th>Work Type</th>
											<th>Rate Card</th>
										<?php } ?>
										<th>Amount</th>
										<th>Tax Amount</th>
										<th></th>
									</tr>
								</thead>
								<tbody class="addrow">
									<?php
									$i = 1;
									foreach ($item_model as $new) {
										?>
										<tr class="tr_class" id="item_<?php echo $new['id']; ?>">

											<td>
												<div id="item_sl_no">
													<?php echo $i; ?>
												</div>
											</td>
											<td>
												<div class="item_description" id="">
													<?php echo $new['description']; ?>
												</div>
											</td>
											<td>
												<div class="item_hsn_code" id="">
													<?php echo $new['hsn_code']; ?>
												</div>
											</td>


											<td>
												<div class="text-right" id="quantity">
													<?php echo $new['quantity']; ?>
												</div>
											</td>
											<td>
												<div class="text-right" id="unit">
													<?php echo $new['unit']; ?>
												</div>
											</td>
											<td>
												<div class="text-right" id="rate">
													<?php echo number_format($new['rate'], 2, '.', ''); ?>
												</div>
											</td>
											<td id="<?php echo $new['type'] ?>">
												<div class="type" id="">
													<?php echo ($new['type'] == 0) ? "Lumpsum" : "Quantity * Rate"; ?>
												</div>
											</td>

											<td>
												<div class="text-right" id="rate">
													<?php echo number_format($new['sgst'], 2, '.', ''); ?>
												</div>
											</td>
											<td>
												<div class="text-right" id="rate">
													<?php echo number_format($new['sgst_amount'], 2, '.', ''); ?>
												</div>
											</td>
											<td>
												<div class="text-right" id="rate">
													<?php echo number_format($new['cgst'], 2, '.', ''); ?>
												</div>
											</td>
											<td>
												<div class="text-right" id="rate">
													<?php echo number_format($new['cgst_amount'], 2, '.', ''); ?>
												</div>
											</td>
											<td>
												<div class="text-right" id="rate">
													<?php echo number_format($new['igst'], 2, '.', ''); ?>
												</div>
											</td>
											<td>
												<div class="text-right" id="rate">
													<?php echo number_format($new['igst_amount'], 2, '.', ''); ?>
												</div>
											</td>
											<?php if (ANCAPMS_LINKED) {
												$worktype = WorkType::model()->findByPk($new['worktype_id']);
												$ratecard = RateCard::model()->findByPk($new['ratecard_id']); ?>
												<td>
													<div class="text-right" id="hours">
														<?php echo $new['hours']; ?>
													</div>
												</td>

												<td id="<?= $new['worktype_id']; ?>">
													<div class="text-right" id="worktype">
														<?php echo $worktype['work_type']; ?>
													</div>
												</td>
												<td id="<?= $new['ratecard_id']; ?>">
													<div class="text-right" id="ratecard">
														<?php echo $ratecard['rate']; ?>
													</div>
												</td>
											<?php } ?>
											<td>
												<div class="amount text-right" id="amount"
													style="max-width: -3px !important;">
													<?php echo Yii::app()->Controller->money_format_inr($new['amount'], 2); ?>
												</div>
											</td>
											<td>
												<div class="amount text-right" id="amount"
													style="max-width: -3px !important;">
													<?php echo number_format($new['tax_amount'], 2, '.', ''); ?>
												</div>
											</td>

											<td><span class="icon icon-options-vertical popover-test" data-toggle="popover"
													data-placement="left" type="button" data-html="true"
													style="cursor: pointer;"></span>
												<div class="popover-content hide">
													<ul class="tooltip-hiden">
														<li><a href="" id='<?php echo $new['id']; ?>'
																class="btn btn-xs btn-default removebtn">Delete</a>
														</li>
														<?php
														$edit_class = ($new['type'] == 0) ? "edit_item_lumpsum" : "edit_item";

														?>
														<li><a href="" id='<?php echo $new['id']; ?>'
																class=" btn btn-xs btn-default <?php echo $edit_class ?>">Edit</a>
														</li>
													</ul>
												</div>
											</td>
										</tr>

										<?php
										$i++;
									}
									?>
								</tbody>

								<tfoot>
									<tr>
										<th colspan="<?php echo (ANCAPMS_LINKED) ? 16 : 13; ?>" class="text-right">
											TOTAL:
										</th>
										<th class="text-right">
											<div id="grand_total">
												<?php echo ($model->amount == '') ? '0.00' : Yii::app()->Controller->money_format_inr($model->amount, 2); ?>
											</div>
										</th>
										<th class="text-right">
											<div id="grand_taxamount">
												<?php echo ($model->tax_amount == '') ? '0.00' : number_format($model->tax_amount, 2, '.', ''); ?>
										</th>
										<th></th>
									</tr>
									<tr>
										<th class="text-right" colspan="<?php echo (ANCAPMS_LINKED) ? 17 : 14; ?>"
											class="text-right">Round
											Off:


										</th>
										<th class="text-right"> <input type="text" name="sales_round_off"
												class="text-right form-control" id="sales_round_off"
												value="<?php echo $model['round_off'] != '' ? $model['round_off'] : 0; ?>">
										</th>
										<th></th>
									</tr>
									<tr>
										<th class="text-right" colspan="<?php echo (ANCAPMS_LINKED) ? 17 : 14; ?>"
											class="text-right">GRAND
											TOTAL:
										</th>
										<th class="text-right"> <span id="net_amount">
												<?php echo Yii::app()->Controller->money_format_inr($model->tax_amount + $model->amount + ($model->round_off), 2) ?>
											</span></th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</div>
						<?php
						$addchargesall = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "invoice_additional_charges WHERE `invoice_id`=" . $model->invoice_id)->queryAll();
						?>

					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="heading-title d-flex">
							Additional Charges
							<div class="content-toggle-btn" onclick="toggleDiv()"><i class="fa fa-plus addcharge"></i>
							</div>
						</div>
						<div class="dotted-line"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="extracharge">

							<?php if (!empty($addchargesall)) {
								foreach ($addchargesall as $data) { ?>
									<div> <span style="font-weight: bold;">
											<?= $data['category'] ?>
										</span> : <span class='totalprice1'>
											<?= Controller::money_format_inr($data['amount'], 2) ?>
										</span>&nbsp;&nbsp;<span class='icon icon-trash delbil' title='Delete'
											id="<?= $data['id'] ?>"></span></div>
								<?php }
							} ?>
							<div class="chargeadd" style="display:none;">
								<div class="row">
									<div class="form-group col-xs-12 col-sm-3">
										<input type="text" placeholder="Charge Category" name="packagecharge"
											id="labelcat" class="form-control textbox" />
									</div>
									<div class="form-group col-xs-12 col-sm-3">
										<input type="number" placeholder="Enter Amount" name="packagecharge"
											id="currentval" class="form-control textbox currentval" />
									</div>
									<div class="form-group col-xs-12 col-sm-3">
										<input type="button" class=" btn btn-sm btn-primary savecharge" value="save" />
									</div>
								</div>
							</div>
							<div id="addtional_list"></div>
						</div>
						<div class="data-amnts box_holder mt">
							<div>
								<label class="inline">Amount:</label>
								<span id="Bills_bill_amount">
									<?php echo $model->amount ? Yii::app()->Controller->money_format_inr($model->amount, 2) : 0; ?>
								</span>
							</div>
							<div>
								<label class="inline">Tax Amount:</label>
								<span id="Bills_bill_taxamount">
									<?php echo $model->tax_amount ? Yii::app()->Controller->money_format_inr($model->tax_amount) : 0; ?>
								</span>
							</div>

							<?php
							$addcharges = Yii::app()->db->createCommand("SELECT ROUND(SUM(amount), 2) as amount FROM " . $tblpx . "invoice_additional_charges WHERE `invoice_id`=" . $model->invoice_id)->queryRow();

							$bill_amount = str_replace(',', '', $model->amount);
							if (!empty($addcharges['amount'])) {
								$model->amount = $bill_amount + $addcharges['amount'];
							}

							?>
							<div <?php if (empty($addcharges['amount'])) { ?> style="display: none;" <?php } ?>
								class="additional_charge">
								<label class="inline">Additional charges :</label>
								<span id="totaladditional_charge">
									<?= (!empty($addcharges)) ? $addcharges['amount'] : 0; ?>
								</span>
							</div>
							<div>
								<label class="inline">Round Off:</label>
								<input type="text" name="Bills[round_off]" id="Bills_round_off"
									value="<?php echo $model->round_off ? $model->round_off : 0; ?>"
									style="border: none;width:100px;" class="text-right" readonly=true>
							</div>
							<div>
								<label class="inline">Grand Total:</label>
								<input type="text" name="Bills[bill_totalamount]" id="Bills_bill_totalamount"
									value="<?php echo number_format($model->tax_amount + $model->amount + ($model->round_off), 2, '.', ','); ?>"
									style="border: none;width:100px;" class="text-right" readonly=true>
							</div>
						</div>
						<input type="hidden" name="invoice_id" id="invoice_id"
							value="<?php echo $model->invoice_id; ?>">

						<input type="hidden" name="final_amount" id="final_amount"
							value="<?php echo ($model->amount == '') ? '0.00' : number_format($model->amount, 2, '.', ''); ?>">

						<input type="hidden" name="item_amount_temp" id="item_amount_temp" value="0">

						<input type="hidden" name="final_tax" id="final_tax"
							value="<?php echo ($model->tax_amount == '') ? '0.00' : number_format($model->tax_amount, 2, '.', ''); ?>">
						<input type="hidden" name="final_amount_with_tax" id="final_amount_with_tax"
							value="<?php echo ($model->tax_amount + $model->amount); ?>">

						<input type="hidden" name="item_tax_temp" id="item_tax_temp" value="0">
					</div>
				</div>
				<div style="padding-right: 0px;"
					class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6">
					<input type="hidden" value="<?php echo ((isset($subtot) && $subtot != '') ? $subtot : ''); ?>"
						class="txtBox pastweek" readonly=ture name="subtot" /></td>
					<input type="hidden" value="<?php echo ((isset($grand) && $grand != '') ? $grand : ''); ?>"
						class="txtBox pastweek grand" name="grand" readonly=true />
				</div>
			</div>
	</div>
	</form>
</div>
<?php $addadditionalurl = Yii::app()->createAbsoluteUrl("invoice/addadditionalcharge"); ?>
<?php $delbillurl = Yii::app()->createAbsoluteUrl("invoice/deleteadditionalcharge"); ?>

<script>

	$(document).ready(function () {
		$('#project').change(function () {
			var project_change = $(this).val();
			$.ajax({
				type: "POST",
				async: false,
				data: { project_change: project_change },
				dataType: "json",
				url: '<?php echo Yii::app()->createUrl('invoice/updateinvoicenew'); ?>',
				success: function (response) {
					if (response.response == 'success') {

						$('#client_name').text(response.client);
					}

				}
			});
		})
		$("#company_id").change(function () {

			var val = $(this).val();
			$("#project").html('<option value="">Select Project</option>');
			$.ajax({
				url: '<?php echo Yii::app()->createUrl('invoice/dynamicproject'); ?>',
				method: 'POST',
				data: { company_id: val },
				dataType: "json",
				success: function (response) {
					if (response.status == 'success') {
						$("#project").html(response.html);
					} else {
						$("#project").html(response.html);
					}
				}

			})
		})
		$('#sales_edit_form').click(function () {
			var inv_no = '';
			var company = $('#company_id').val();
			var project = $('#project').val();
			var date = $('#datepicker').val();
			inv_no = $('input[name="inv_no"]').val();
			invoice_id = $('#invoice_id').val();



			// alert(invoice_id);
			$.ajax({
				type: "POST",
				async: false,
				data: { company: company, project: project, date: date, invoice_id: invoice_id, inv_no: inv_no },
				dataType: "json",
				url: '<?php echo Yii::app()->createUrl('invoice/updateinvoicenew'); ?>',
				success: function (response) {
					if (response.response == 'success') {
						$().toastmessage('showSuccessToast', "" + response.msg + "");
						$('#client_name').text(response.client);
					}

				}
			});

		});
		$('.addcharge').click(function () {
			$(".chargeadd").toggle();
			$(this).toggleClass("icon-minus hidecharge");
		});
		$(".savecharge").on('keyup keypress keydown click', function (e) {
			var label = $("#labelcat").val();
			var currentval = $("#currentval").val();
			var invoice_id = $("#invoice_id").val();
			var totalamount = $("#final_amount_with_tax").val().replace(/,/g, '');
			totalamount = parseFloat(totalamount).toFixed(2);
			console.log(totalamount);
			var addtot = parseFloat($("#totaladditional_charge").text());
			if (invoice_id == '') {
				$('.errorcharge').html('Please add item');

			} else if (label != '' && currentval != '') {
				$(".savecharge").attr('disabled', 'disabled');
				$('.loading-overlay').addClass('is-active');
				$.ajax({
					url: "<?php echo $addadditionalurl; ?>",
					data: {
						category: label,
						amount: currentval,
						invoice_id: invoice_id
					},
					type: "POST",
					dataType: 'json',
					success: function (data) {
						$(".savecharge").removeAttr('disabled');
						if (data != '') {
							$("#addtional_list").append("<div>" + label + " : " + "<span class='totalprice'>" + parseFloat(currentval).toFixed(2) + "</span> &nbsp;&nbsp&nbsp;<span class='icon icon-trash deleteitem' title='Delete' id=" + data.itemid + "></span> " + "</div>");
							$(".additional_total").show();
							$('#labelcat').val('');
							$('#currentval').val('');
							if (isNaN(addtot)) {
								var totaladd = parseFloat(currentval).toFixed(2);
							} else {
								var totaladd = parseFloat(addtot) + parseFloat(currentval);
								totaladd = parseFloat(totaladd).toFixed(2);;
							}

							$('.additional_charge').show();
							$("#totaladditional_charge").text(totaladd);
							var tot = parseFloat(totalamount) + parseFloat(currentval);

							$("#final_amount_with_tax").val(tot.toFixed(2));

							$(".deleteitem").click(function () {
								var invoice_id = $("#invoice_id").val();
								$(this).parent().remove();

								var id = $(this).attr('id');
								$('.loading-overlay').addClass('is-active');
								$.ajax({
									url: "<?php echo $delbillurl; ?>",
									data: {
										id: id,
										invoice_id: invoice_id
									},
									type: "POST",
									success: function (data) {
										$(".savecharge").removeAttr('disabled');
										window.location.reload();

									}
								});
							});
							window.location.reload();
						}
					}
				});
			} else {

				if (label == '' || currentval == '') {
					$('.errorcharge').html('Both fields are required');
				}
			}
		});
		$(".delbil").click(function () {
			$('.loading-overlay').addClass('is-active');
			var id = $(this).attr('id');
			var invoice_id = $("#invoice_id").val();
			$.ajax({
				url: "<?php echo $delbillurl; ?>",
				data: {
					id: id,
					invoice_id: invoice_id
				},
				type: "POST",
				success: function (data) {
					window.location.reload();
				}
			});
		});
		//PMS linked 
		$('#selectedWorkType').change(function () {
			var selectedWorkType = $(this).val();
			$.ajax({
				type: 'POST',
				url: '<?php echo Yii::app()->createUrl('invoice/getRateCard'); ?>',
				dataType: 'json',
				data: { work_type: selectedWorkType },
				success: function (response) {
					if (response.status == 'success') {
						$("#selectedRateCard").html(response.html);
					} else {
						$("#selectedRateCard").html(response.html);
					}
				}
			});
		});
		$('#hours').change(function () {
			var hours = parseFloat($('#hours').val());
			var rate = parseFloat($('#selectedRateCard option:selected').text());
			var amount = parseFloat(hours * rate);
			$('#amount').val(amount);
			var sgst = parseFloat($("#sgst").val());
			var cgst = parseFloat($("#cgst").val());
			var igst = parseFloat($("#igst").val());

			var new_amount = 0;

			var sgst_amount = (sgst / 100) * amount;
			var cgst_amount = (cgst / 100) * amount;
			var igst_amount = (igst / 100) * amount;

			if (isNaN(sgst_amount)) sgst_amount = 0;
			if (isNaN(cgst_amount)) cgst_amount = 0;
			if (isNaN(igst_amount)) igst_amount = 0;
			if (isNaN(amount)) amount = 0;

			var tax_amount = sgst_amount + cgst_amount + igst_amount;

			var total_amount = amount + tax_amount;

			$("#sgst_amount").html(sgst_amount.toFixed(2));
			$("#cgst_amount").html(cgst_amount.toFixed(2));
			$("#igst_amount").html(igst_amount.toFixed(2));
			$("#item_amount").html(amount.toFixed(2));
			$("#tax_amount").html(tax_amount.toFixed(2));
			$("#total_amount").html(total_amount.toFixed(2));
		});
		$('#selectedRateCard').change(function () {
			var selectedRateCard = $(this).val();
			var hours = parseFloat($('#hours').val());
			var rate = parseFloat($(this).find(":selected").text());
			var amount = parseFloat(hours * rate);
			$('#amount').val(amount);
			var sgst = parseFloat($("#sgst").val());
			var cgst = parseFloat($("#cgst").val());
			var igst = parseFloat($("#igst").val());

			var new_amount = 0;

			var sgst_amount = (sgst / 100) * amount;
			var cgst_amount = (cgst / 100) * amount;
			var igst_amount = (igst / 100) * amount;

			if (isNaN(sgst_amount)) sgst_amount = 0;
			if (isNaN(cgst_amount)) cgst_amount = 0;
			if (isNaN(igst_amount)) igst_amount = 0;
			if (isNaN(amount)) amount = 0;

			var tax_amount = sgst_amount + cgst_amount + igst_amount;

			var total_amount = amount + tax_amount;

			$("#sgst_amount").html(sgst_amount.toFixed(2));
			$("#cgst_amount").html(cgst_amount.toFixed(2));
			$("#igst_amount").html(igst_amount.toFixed(2));
			$("#item_amount").html(amount.toFixed(2));
			$("#tax_amount").html(tax_amount.toFixed(2));
			$("#total_amount").html(total_amount.toFixed(2));
		});
		//PMS linked ends
	});

</script>

<script>

	var invoice_id = "<?php echo $model->invoice_id; ?>";
	jQuery.extend(jQuery.expr[':'], {
		focusable: function (el, index, selector) {
			return $(el).is('button, :input, [tabindex]');
		}
	});

	$(document).on('keypress', 'input,select', function (e) {
		if (e.which == 13) {
			e.preventDefault();
			// Get all focusable elements on the page
			var $canfocus = $(':focusable');
			var index = $canfocus.index(document.activeElement) + 1;
			if (index >= $canfocus.length) index = 0;
			$canfocus.eq(index).focus();
		}
	});

	$(document).ready(function () {
		$(".popover-test").popover({
			html: true,
			content: function () {
				return $(this).next('.popover-content').html();
			}
		});
		$('[data-toggle=popover]').on('click', function (e) {
			$('[data-toggle=popover]').not(this).popover('hide');
		});
		$('body').on('hidden.bs.popover', function (e) {
			$(e.target).data("bs.popover").inState.click = false;
		});
		$('body').on('click', function (e) {
			$('[data-toggle=popover]').each(function () {
				// hide any open popovers when the anywhere else in the body is clicked
				if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
					$(this).popover('hide');
				}
			});
		});
		$(document).ajaxComplete(function () {
			$(".popover-test").popover({
				html: true,
				content: function () {
					return $(this).next('.popover-content').html();
				}
			});

		});

		$(".js-example-basic-single").select2();
		$(".project").select2();
		$(".client").select2();

	});


	$('.project').change(function () {

		$("#client").select2("open");
	})





	$(document).ready(function () {
		$('#description').first().focus();
		//$('.js-example-basic-single').select2('focus');
	});



</script>



<script>
	$(document).ready(function () {
		$().toastmessage({
			sticky: false,
			position: 'top-right',
			inEffectDuration: 1000,
			stayTime: 3000,
			closeText: '<i class="icon ion-close-round"></i>',
		});




	});


	$(document).on('click', '.removebtn', function (e) {
		e.preventDefault();
		element = $(this);
		var item_id = $(this).attr('id');
		var answer = confirm("Are you sure you want to delete?");
		if (answer) {
			var item_id = $(this).attr('id');
			var invoice_id = $("#invoice_id").val();
			var subtot = $('input[name="subtot"]').val();
			var grand = $('input[name="grand"]').val();
			var tax_grand = $('#grand_taxamount').text();
			var data = { 'invoice_id': invoice_id, 'item_id': item_id, 'grand': grand, 'subtot': subtot, 'tax_grand': tax_grand };
			$.ajax({
				method: "GET",
				async: false,
				data: { data: data },
				dataType: "json",
				url: '<?php echo Yii::app()->createUrl('invoice/removeinvoicelumpsum'); ?>',
				success: function (response) {
					if (response.response == 'success') {
						$('#grand_total').html(response.final_amount);
						$('#grand_taxamount').html(response.final_tax);
						$('#net_amount').html(response.grand_total);
						$().toastmessage('showSuccessToast', "" + response.msg + "");
						$('.addrow').html(response.html);
					} else {
						$().toastmessage('showErrorToast', "" + response.msg + "");
					}
				}
			});

		}
		else {

			return false;
		}

	});



	$(document).on("click", ".addcolumn, .removebtn", function () {
		//alert('hi');

		$("table.table  input[name='sl_No[]']").each(function (index, element) {
			$(element).val(index + 1);
			$('.sl_No').html(index + 1);
		});
	});






	$(".inputSwitch span").on("click", function () {

		var $this = $(this);

		$this.hide().siblings("input").val($this.text()).show();

	});

	$(".inputSwitch input").bind('blur', function () {

		var $this = $(this);

		$(this).attr('value', $(this).val());

		$this.hide().siblings("span").text($this.val()).show();

	}).hide();


	$(".allownumericdecimal").keydown(function (event) {



		if (event.shiftKey == true) {
			event.preventDefault();
		}

		if ((event.keyCode >= 48 && event.keyCode <= 57) ||
			(event.keyCode >= 96 && event.keyCode <= 105) ||
			event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
			event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
			var splitfield = $(this).val().split(".");
			if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
				event.preventDefault();
			}
		} else {
			event.preventDefault();
		}

		if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
			event.preventDefault();
		//if a decimal has been added, disable the "."-button





	});

	$(".allownegativenumericdecimal").keydown(function (event) {
		if (event.shiftKey == true) {
			event.preventDefault();
		}

		if (
			(event.keyCode >= 48 && event.keyCode <= 57) ||
			(event.keyCode >= 96 && event.keyCode <= 105) ||
			event.keyCode == 8 ||
			event.keyCode == 9 ||
			event.keyCode == 37 ||
			event.keyCode == 39 ||
			event.keyCode == 46 ||
			event.keyCode == 190 ||
			event.keyCode == 110 ||
			event.keyCode == 13 ||
			event.keyCode == 189 // Allow the '-' key
		) {
			var splitfield = $(this).val().split(".");
			if (
				(splitfield[1] !== undefined &&
					splitfield[1].length >= 2) &&
				event.keyCode != 8 &&
				event.keyCode != 0 &&
				event.keyCode != 13
			) {
				event.preventDefault();
			}
		} else {
			event.preventDefault();
		}

		if (
			$(this).val().indexOf('.') !== -1 &&
			(event.keyCode == 190 || event.keyCode == 110)
		)
			event.preventDefault();
		//if a decimal has been added, disable the "."-button
	});






	$('.other').click(function () {
		if (this.checked) {


			$('#autoUpdate').fadeIn('slow');
			$('#select_div').hide();
		} else {

			$('#autoUpdate').fadeOut('slow');
			$('#select_div').show();
		}

	})


	$(document).on("change", ".other", function () {
		if (this.checked) {
			$(this).closest('tr').find('#autoUpdate').fadeIn('slow');
			$(this).closest('tr').find('#select_div').hide();
		} else {
			$(this).closest('tr').find('#autoUpdate').fadeOut('slow');
			$(this).closest('tr').find('#select_div').show();
		}
	});


















</script>


<script>



	var sl_no = 1;
	var howMany = 0;
	$('.item_save').click(function () {
		var elem = $(this);
		if ($("#InvList_type").val() == 0) {
			saveItemByLumpsum(elem);
		} else {
			saveItemByQuantityAndRate(elem);
		}
	})

	function saveItemByLumpsum(elem) {
		$("#previous_details").hide();
		var element = elem;
		var item_id = elem.attr('id');

		if (item_id == 0) {

			var type = $("#InvList_type").val();
			var description = $('#description').val();
			var hsn_code = $('#hsn_code').val();
			var cgst = $('#cgst').val();
			var cgst_amount = $('#cgst_amount').html();
			var sgst = $('#sgst').val();
			var sgst_amount = $('#sgst_amount').html();
			var igst = $('#igst').val();
			var igst_amount = $('#igst_amount').html();
			var tax_amount = $('#tax_amount').html();

			var amount = $('#amount').val();
			var project = $('input[name="project"]').val();
			var client = $('input[name="client"]').val();
			var date = $('input[name="date"]').val();
			var inv_no = $('input[name="inv_no"]').val();
			var rowCount = $('.table .addrow tr').length;
			var pms_linked = "<?php echo ANCAPMS_LINKED; ?>";
			if (pms_linked) {
				var hours = $('#hours').val();
				var ratecard_id = $('#selectedRateCard').val();
				var worktype_id = $('#selectedWorkType').val();
			} else {
				var hours = '';
				var ratecard_id = '';
				var worktype_id = '';
			}

			if (project == '' || client == '' || date == '' || inv_no == '') {
				$().toastmessage('showErrorToast', "Please enter purchase details");
				//$('#msg_box').html('<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong></strong> Please enter purchase details. </div>');
			} else {
				var pms_error = 0;
				if (description != '' && amount != '' && hsn_code != '' && amount > 0) {
					var pms_linked = "<?php echo ANCAPMS_LINKED; ?>";
					if (pms_linked) {
						if (hours == '' || ratecard_id == '' || worktype_id == '') {
							pms_error = 1;
							$().toastmessage('showErrorToast', "Please fill the mandatory fields");
						}
					}

					howMany += 1;
					if (howMany == 1 && pms_error == 0) {
						var invoice_id = $('input[name="invoice_id"]').val();
						var subtot = $('#grand_total').text();
						var grand = $('#grand_total').text();
						var grand_tax = $('#grand_taxamount').text();
						var data = { 'sl_no': rowCount, 'description': description, 'amount': amount, 'sgst': sgst, 'sgst_amount': sgst_amount, 'cgst': cgst, 'cgst_amount': cgst_amount, 'igst': igst, 'igst_amount': igst_amount, 'tax_amount': tax_amount, 'invoice_id': invoice_id, 'grand': grand, 'subtot': subtot, 'grand_tax': grand_tax, 'hsn_code': hsn_code, 'type': type, 'hours': hours, 'worktype_id': worktype_id, 'ratecard_id': ratecard_id, };
						$.ajax({
							url: '<?php echo Yii::app()->createAbsoluteUrl('invoice/invoicelumpsumitem'); ?>',
							type: 'GET',
							dataType: 'json',
							data: { data: data },
							success: function (response) {
								if (response.response == 'success') {
									$('#grand_total').html(response.final_amount);
									$('#grand_taxamount').html(response.final_tax);
									$('#net_amount').html(response.grand_total);
									$().toastmessage('showSuccessToast', "" + response.msg + "");
									$('.addrow').html(response.html);
								} else {
									$().toastmessage('showErrorToast', "" + response.msg + "");
								}
								howMany = 0;
								//$('#description').val('');
								$('#description').val('').trigger('change');
								$('#remarks').val('');
								$('#hsn_code').val('');
								$('#amount').val('');
								$('#item_amount').html('');
								$('#tax_amount').html('0.00');
								$('#sgst').val('');
								$('#sgst_amount').html('0.00');
								$('#cgst').val('');
								$('#cgst_amount').html('0.00');
								$('#igst').val('');
								$('#igst_amount').html('0.00');
								$('#description').focus();
								$("#InvList_type").prop('disabled', false);
								var pms_linked = "<?php echo ANCAPMS_LINKED; ?>";
								if (pms_linked) {

									$(".purchase_items").hide();
									$('#hours').val('');
									$('#selectedRateCard').val('');
									$('#selectedWorkType').val('');
									$('#item_amount').prop('readonly', true);
								}
							}
						});
					}

				} else {
					$().toastmessage('showErrorToast', "Please fill the mandatory fields");
				}



			}



		} else {
			// update 

			var type = $("#InvList_type").val();
			var description = $('#description').val();
			var hsn_code = $('#hsn_code').val();
			var amount = $('#amount').val();
			var project = $('input[name="project"]').val();
			var client = $('input[name="client"]').val();
			var date = $('input[name="date"]').val();
			var inv_no = $('input[name="inv_no"]').val();

			var cgst = $('#cgst').val();
			var cgst_amount = $('#cgst_amount').html();
			var sgst = $('#sgst').val();
			var sgst_amount = $('#sgst_amount').html();
			var igst = $('#igst').val();
			var igst_amount = $('#igst_amount').html();
			var tax_amount = $('#tax_amount').html();
			var pms_linked = "<?php echo ANCAPMS_LINKED; ?>";
			if (pms_linked) {
				var hours = $('#hours').val();
				var ratecard_id = $('#selectedRateCard').val();
				var worktype_id = $('#selectedWorkType').val();
			} else {
				var hours = '';
				var ratecard_id = '';
				var worktype_id = '';
			}

			if (project == '' || client == '' || date == '' || inv_no == '') {

			} else {
				var pms_error = 0;
				if (description != '' && amount != '' && hsn_code != '' && amount > 0) {
					var pms_linked = "<?php echo ANCAPMS_LINKED; ?>";
					if (pms_linked) {
						if (hours == '' || ratecard_id == '' || worktype_id == '') {
							pms_error = 1;
							$().toastmessage('showErrorToast', "Please fill the mandatory fields");
						}
					}
					howMany += 1;
					if (howMany == 1 && pms_error == 0) {
						var invoice_id = $('input[name="invoice_id"]').val();
						var subtot = $('#grand_total').text();
						var grand = $('#grand_total').text();
						var grand_tax = $('#grand_taxamount').text();
						var data = { 'item_id': item_id, 'sl_no': sl_no, 'description': description, 'amount': amount, 'sgst': sgst, 'sgst_amount': sgst_amount, 'cgst': cgst, 'cgst_amount': cgst_amount, 'igst': igst, 'igst_amount': igst_amount, 'tax_amount': tax_amount, 'invoice_id': invoice_id, 'grand': grand, 'subtot': subtot, 'grand_tax': grand_tax, 'hsn_code': hsn_code, 'type': type, 'hours': hours, 'worktype_id': worktype_id, 'ratecard_id': ratecard_id, 'inv_no': inv_no };
						//alert(inv_no);
						$.ajax({
							url: '<?php echo Yii::app()->createAbsoluteUrl('invoice/updateslumpsumitem'); ?>',
							type: 'GET',
							dataType: 'json',
							data: { data: data },
							success: function (response) {
								//element.closest('tr').hide();
								if (response.response == 'success') {
									$('#grand_total').html(response.final_amount);
									$('#grand_taxamount').html(response.final_tax);
									$('#net_amount').html(response.grand_total);
									$().toastmessage('showSuccessToast', "" + response.msg + "");
									$('.addrow').html(response.html);
									$('#Bills_bill_amount').html(response.final_amount);
									$('#Bills_bill_taxamount').html(response.final_tax);
									var total_additional = parseFloat($('#totaladditional_charge').html());
									var grand_sum = response.grand_total_number + total_additional;
									var formatted_grand_sum = grand_sum.toLocaleString('en-IN', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
									$('#Bills_bill_totalamount').val(formatted_grand_sum);

								} else {
									$().toastmessage('showErrorToast', "" + response.msg + "");
								}

								howMany = 0;
								$('#description').val('').trigger('change');
								$('#remarks').val('');
								$('#hsn_code').val('');
								$('#amount').val('');
								$('#item_amount').html('');
								$('#tax_amount').html('0.00');
								$('#sgst').val('');
								$('#sgst_amount').html('0.00');
								$('#cgst').val('');
								$('#cgst_amount').html('0.00');
								$('#igst').val('');
								$('#igst_amount').html('0.00');
								$('#description').focus();
								$('#item_amount_temp').val(0);
								$('#item_tax_temp').val(0);
								var pms_linked = "<?php echo ANCAPMS_LINKED; ?>";
								if (pms_linked) {
									$(".purchase_items").hide();
									$('#hours').val('');
									$('#selectedRateCard').val('');
									$('#selectedWorkType').val('');
									$('#item_amount').prop('readonly', true);
								}
								$(".item_save").attr('value', 'Save');
								$(".item_save").attr('id', 0);
								//$(".js-example-basic-single").select2("val", "");
								$('#description').focus();
							}
						});

					}
				} else {
					$().toastmessage('showErrorToast', "Please fill the mandatory fields");

				}



			}


		}




	};

	function saveItemByQuantityAndRate(elem) {
		$("#previous_details").hide();
		var element = elem;
		var item_id = elem.attr('id');

		if (item_id == 0) {

			var type = $("#InvList_type").val();
			var description = $('#description').val();
			var hsn_code = $('#hsn_code').val();
			var quantity = $('#quantity').val();
			var unit = $('#unit').val();
			var rate = $('#rate').val();

			var cgst = $('#cgst').val();
			var cgst_amount = $('#cgst_amount').html();
			var sgst = $('#sgst').val();
			var sgst_amount = $('#sgst_amount').html();
			var igst = $('#igst').val();
			var igst_amount = $('#igst_amount').html();
			var tax_amount = $('#tax_amount').html();

			var amount = $('#item_amount').html();
			var project = $('input[name="project"]').val();
			var client = $('input[name="client"]').val();
			var date = $('input[name="date"]').val();
			var inv_no = $('input[name="inv_no"]').val();
			var rowCount = $('.table .addrow tr').length;


			if (project == '' || client == '' || date == '' || inv_no == '') {
				$().toastmessage('showErrorToast', "Please enter purchase details");
				//$('#msg_box').html('<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong></strong> Please enter purchase details. </div>');
			} else {

				if (description != '' && rate != '' && hsn_code != '') {


					howMany += 1;
					if (howMany == 1) {
						if (quantity == '') {
							quantity = 1;
						}
						var invoice_id = $('input[name="invoice_id"]').val();
						var subtot = $('#grand_total').text();
						var grand = $('#grand_total').text();
						var grand_tax = $('#grand_taxamount').text();
						var data = { 'sl_no': rowCount, 'quantity': quantity, 'description': description, 'unit': unit, 'rate': rate, 'amount': amount, 'sgst': sgst, 'sgst_amount': sgst_amount, 'cgst': cgst, 'cgst_amount': cgst_amount, 'igst': igst, 'igst_amount': igst_amount, 'tax_amount': tax_amount, 'invoice_id': invoice_id, 'grand': grand, 'subtot': subtot, 'grand_tax': grand_tax, 'hsn_code': hsn_code, 'type': type, 'inv_no': inv_no };
						$.ajax({
							url: '<?php echo Yii::app()->createAbsoluteUrl('invoice/invoiceitem'); ?>',
							type: 'GET',
							dataType: 'json',
							data: { data: data },
							success: function (response) {
								if (response.response == 'success') {
									$('#grand_total').html(response.final_amount);
									$('#grand_taxamount').html(response.final_tax);
									$('#net_amount').html(response.grand_total);
									$().toastmessage('showSuccessToast', "" + response.msg + "");
									$('.addrow').html(response.html);
								} else {
									$().toastmessage('showErrorToast', "" + response.msg + "");
								}
								howMany = 0;
								//$('#description').val('');
								$('#description').val('').trigger('change');
								$('#remarks').val('');
								$('#hsn_code').val('');
								$('#quantity').val('');
								$('#unit').val('');
								$('#rate').val('');
								$('#item_amount').html('');
								$('#tax_amount').html('0.00');
								$('#sgst').val('');
								$('#sgst_amount').html('0.00');
								$('#cgst').val('');
								$('#cgst_amount').html('0.00');
								$('#igst').val('');
								$('#igst_amount').html('0.00');
								$('#description').focus();
							}
						});
					}

				} else {
					$().toastmessage('showErrorToast', "Please fill the mandatory fields");
				}



			}



		} else {
			// update 

			var type = $("#InvList_type").val();
			var description = $('#description').val();
			var hsn_code = $('#hsn_code').val();
			var quantity = $('#quantity').val();
			var unit = $('#unit').val();
			var rate = $('#rate').val();
			var amount = $('#item_amount').html();
			var project = $('input[name="project"]').val();
			var client = $('input[name="client"]').val();
			var date = $('input[name="date"]').val();
			var inv_no = $('input[name="inv_no"]').val();

			var cgst = $('#cgst').val();
			var cgst_amount = $('#cgst_amount').html();
			var sgst = $('#sgst').val();
			var sgst_amount = $('#sgst_amount').html();
			var igst = $('#igst').val();
			var igst_amount = $('#igst_amount').html();
			var tax_amount = $('#tax_amount').html();

			if (project == '' || client == '' || date == '' || inv_no == '') {

			} else {

				if (description != '' && rate != '' && hsn_code != '') {
					howMany += 1;
					if (howMany == 1) {
						if (quantity == '') {
							quantity = 1;
						}
						var invoice_id = $('input[name="invoice_id"]').val();
						var subtot = $('#grand_total').text();
						var grand = $('#grand_total').text();
						var grand_tax = $('#grand_taxamount').text();
						var data = { 'item_id': item_id, 'sl_no': sl_no, 'quantity': quantity, 'description': description, 'unit': unit, 'rate': rate, 'amount': amount, 'sgst': sgst, 'sgst_amount': sgst_amount, 'cgst': cgst, 'cgst_amount': cgst_amount, 'igst': igst, 'igst_amount': igst_amount, 'tax_amount': tax_amount, 'invoice_id': invoice_id, 'grand': grand, 'subtot': subtot, 'grand_tax': grand_tax, 'hsn_code': hsn_code, 'type': type, 'inv_no': inv_no };
						$.ajax({
							url: '<?php echo Yii::app()->createAbsoluteUrl('invoice/updatesinvoiceitem'); ?>',
							type: 'GET',
							dataType: 'json',
							data: { data: data },
							success: function (response) {
								//element.closest('tr').hide();
								if (response.response == 'success') {
									$('#grand_total').html(response.final_amount);
									$('#grand_taxamount').html(response.final_tax);
									$('#net_amount').html(response.grand_total);
									$().toastmessage('showSuccessToast', "" + response.msg + "");
									$('.addrow').html(response.html);
								} else {
									$().toastmessage('showErrorToast', "" + response.msg + "");
								}

								howMany = 0;
								$('#description').val('').trigger('change');
								$('#remarks').val('');
								$('#hsn_code').val('');
								$('#quantity').val('');
								$('#unit').val('');
								$('#rate').val('');
								$('#item_amount').html('');
								$('#tax_amount').html('0.00');
								$('#sgst').val('');
								$('#sgst_amount').html('0.00');
								$('#cgst').val('');
								$('#cgst_amount').html('0.00');
								$('#igst').val('');
								$('#igst_amount').html('0.00');
								$('#description').focus();
								$('#item_amount_temp').val(0);
								$('#item_tax_temp').val(0);

								$(".item_save").attr('value', 'Save');
								$(".item_save").attr('id', 0);
								//$(".js-example-basic-single").select2("val", "");
								$('#description').focus();
							}
						});

					}
				} else {
					$().toastmessage('showErrorToast', "Please fill the mandatory fields");

				}



			}


		}

	}



	$(document).on('click', '.edit_item_lumpsum', function (e) {
		$(".lumpsum_sec").show();

		var pms_linked = "<?php echo ANCAPMS_LINKED; ?>";
		if (pms_linked) {
			$(".purchase_items").hide();
		}
		$(".purchase_items").show();

		$(".qty_rate_sec").hide();
		e.preventDefault();


		$('.remark').css("display", "inline-block");
		$('#remark').focus();

		var item_id = $(this).attr('id');
		var $tds = $(this).closest('tr').find('td');
		console.log($tds);
		var description = $tds.eq(1).text();
		var hsn_code = $tds.eq(2).text();
		var type = $tds.eq(6).attr('id');
		var sgst = $tds.eq(7).text();
		var sgst_amount = $tds.eq(8).text();
		var cgst = $tds.eq(9).text();
		var cgst_amount = $tds.eq(10).text();

		var igst = $tds.eq(11).text();
		var igst_amount = $tds.eq(12).text();
		var pms_linked = "<?php echo ANCAPMS_LINKED; ?>";
		if (pms_linked) {
			var hours = $tds.eq(13).text();
			var worktype = $tds.eq(14).text();
			var worktypeValue = $tds.eq(14).attr('id');
			var ratecard = $tds.eq(15).text();
			var ratecardValue = $tds.eq(15).attr('id');
			var amount = $tds.eq(16).text();
			var tax_amount = $tds.eq(17).text();
			$('#hours').val(hours.trim());
			$('#selectedWorkType').val(worktypeValue);
			var selectedWorkType = worktypeValue;
			$.ajax({
				type: 'POST',
				url: '<?php echo Yii::app()->createUrl('invoice/getRateCard'); ?>',
				dataType: 'json',
				data: { work_type: selectedWorkType },
				success: function (response) {
					if (response.status == 'success') {
						$("#selectedRateCard").html(response.html);
						$('#selectedRateCard').val(ratecardValue);
					} else {
						$("#selectedRateCard").html(response.html);
						$('#selectedRateCard').val(ratecardValue);
					}
				}
			});

			$('#item_amount').prop('readonly', true);
		} else {
			var amount = $tds.eq(13).text();
			var tax_amount = $tds.eq(14).text();
		}

		$('#description').val(description.trim())
		$('#hsn_code').val(hsn_code.trim())


		let cleanedAmount = $.trim(amount.replace(/,/g, ''));

		$('#amount').val(cleanedAmount);
		$('#item_amount').html(cleanedAmount)
		$('#item_amount_temp').val(cleanedAmount)

		$('#InvList_type').val(type)
		$('#InvList_type').prop('disabled', true);

		$('#tax_amount').html(parseFloat(tax_amount))
		$('#item_tax_temp').val(parseFloat(tax_amount))
		$('#cgst').val(cgst.trim());
		$('#cgst_amount').text(cgst_amount)
		$('#sgst').val(sgst.trim())
		$('#sgst_amount').text(sgst_amount)
		$('#igst').val(igst.trim())
		$('#igst_amount').text(igst_amount)

		$(".item_save").attr('value', 'Update');
		$(".item_save").attr('id', item_id);
		$(".item_save").attr('id', item_id);
		$('#description').focus();


	})



	/*$('#sgst').change(function(){
	   var val= $(this).val();
	   var amount = $('#item_amount').html();
	   if(amount!=0){
		var sgst =  parseFloat(amount/100*val);
		var cgst =  $('#cgst_amount').text();
		var igst =  $('#igst_amount').text();
		var total = Number(sgst)+Number(cgst)+Number(igst);
		$('#sgst_amount').text(sgst.toFixed(2));
		$('#tax_amount').text(total.toFixed(2));	
	   }
	})
	
	 $('#cgst').change(function(){
	   var val= $(this).val();
	   var amount = $('#item_amount').html();
	   if(amount!=0){
		var cgst =  parseFloat(amount/100*val);
		var sgst =  $('#sgst_amount').text();
		var igst =  $('#igst_amount').text();
		var total = Number(sgst)+Number(cgst)+Number(igst);
		$('#cgst_amount').text(cgst.toFixed(2));
		$('#tax_amount').text(total.toFixed(2));	
	   }
	})
	
	$('#igst').change(function(){
	   var val= $(this).val();
	   var amount = $('#item_amount').html();
	   if(amount!=0){
		var igst =  parseFloat(amount/100*val);
		var cgst =  $('#cgst_amount').text();
		var sgst =  $('#sgst_amount').text();
		var total = Number(sgst)+Number(cgst)+Number(igst);
		$('#igst_amount').text(igst.toFixed(2));
		$('#tax_amount').text(total.toFixed(2));	
	   }
	})
	*/






	$('.item_save').keypress(function (e) {
		if (e.keyCode == 13) {
			$('.item_save').click();
		}
	});


	function filterDigits(eventInstance) {
		eventInstance = eventInstance || window.event;
		key = eventInstance.keyCode || eventInstance.which;
		if ((47 < key) && (key < 58) || key == 8) {
			return true;
		} else {
			if (eventInstance.preventDefault)
				eventInstance.preventDefault();
			eventInstance.returnValue = false;
			return false;
		} //if
	}


	$(document).ready(function () {
		$(".popover-test").popover({
			html: true,
			content: function () {
				//return $('#popover-content').html();
				return $(this).next('.popover-content').html();
			}
		});
		$('[data-toggle=popover]').on('click', function (e) {
			$('[data-toggle=popover]').not(this).popover('hide');
		});
		$('body').on('hidden.bs.popover', function (e) {
			$(e.target).data("bs.popover").inState.click = false;
		});
		$('body').on('click', function (e) {
			$('[data-toggle=popover]').each(function () {
				// hide any open popovers when the anywhere else in the body is clicked
				if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
					$(this).popover('hide');
				}
			});
		});

		$(function () {
			var anca_pms_linked = '<?php echo ANCAPMS_LINKED; ?>';
			if (anca_pms_linked) {
				$('#company_id').addClass('nonClickable');
				$('#select2-project-container').addClass('nonClickable');
				$('#InvList_type').addClass('nonClickable');
				let company_id = $('#company_id').val();

				//alert(company_id);
				$.ajax({
					url: " <?php echo Yii::app()->createAbsoluteUrl('invoice/getInvoiceNo'); ?>",
					type: "GET",
					data: { 'company_id': company_id },
					success: function (response) {
						if (response == 1) {

						} else {
							// alert(response);
							$("#inv_no").val(response);
						}

					}
				});
			}
		});
	});

	// calculation

	$("#quantity, #rate,#amount, #sgst, #cgst, #igst").blur(function () {
		var quantity = parseFloat($("#quantity").val());
		var rate = parseFloat($("#rate").val());

		var amount = parseFloat($("#amount").val());
		var sgst = parseFloat($("#sgst").val());
		var cgst = parseFloat($("#cgst").val());
		var igst = parseFloat($("#igst").val());

		if ($("#InvList_type").val() == 1) {
			var amount = quantity * rate; //qty*rate
		}

		var new_amount = 0;

		var sgst_amount = (sgst / 100) * amount;
		var cgst_amount = (cgst / 100) * amount;
		var igst_amount = (igst / 100) * amount;

		if (isNaN(sgst_amount)) sgst_amount = 0;
		if (isNaN(cgst_amount)) cgst_amount = 0;
		if (isNaN(igst_amount)) igst_amount = 0;
		if (isNaN(amount)) amount = 0;

		var tax_amount = sgst_amount + cgst_amount + igst_amount;

		var total_amount = amount + tax_amount;

		$("#sgst_amount").html(sgst_amount.toFixed(2));
		$("#cgst_amount").html(cgst_amount.toFixed(2));
		$("#igst_amount").html(igst_amount.toFixed(2));
		$("#item_amount").html(amount.toFixed(2));
		$("#tax_amount").html(tax_amount.toFixed(2));
		$("#total_amount").html(total_amount.toFixed(2));

	});

	$('.calculation').keyup(function (event) {
		var val = $(this).val();
		if (val > 100) {
			$().toastmessage('showErrorToast', "This percentage must be less than 100%");
			$(this).val('');
		}
	})
</script>

<script>
		(function () {
			if ($("#InvList_type").val() == 0) {
				$(".lumpsum_sec").show();
				$(".qty_rate_sec").hide();
			}

			var mainTable = document.getElementById("main-table");
			if (mainTable !== null) {
				var tableHeight = mainTable.offsetHeight;
				if (tableHeight > 380) {
					var fauxTable = document.getElementById("faux-table");
					document.getElementById("table-wrap").className += ' ' + 'fixedON';
					var clonedElement = mainTable.cloneNode(true);
					clonedElement.id = "";
					fauxTable.appendChild(clonedElement);
				}
			}
		})();

	$(document).on("change", "#InvList_type", function () {
		var type = $(this).val();
		$(this).val(type);
		if (type == 0) {
			$(".lumpsum_sec").show();
			$(".qty_rate_sec").hide();
		} else {
			$(".lumpsum_sec").hide();
			$(".qty_rate_sec").show()
		}

	})


	$(document).on('click', '.edit_item', function (e) {
		$(".lumpsum_sec").hide();
		$(".qty_rate_sec").show();
		e.preventDefault();


		$('.remark').css("display", "inline-block");
		$('#remark').focus();

		var item_id = $(this).attr('id');
		var $tds = $(this).closest('tr').find('td');
		var description = $tds.eq(1).text();
		var hsn_code = $tds.eq(2).text();
		var quantity = $tds.eq(3).text();
		var unit = $tds.eq(4).text();
		var rate = $tds.eq(5).text();
		var type = $tds.eq(6).attr('id');
		var sgst = $tds.eq(7).text();
		var sgst_amount = $tds.eq(8).text();
		var cgst = $tds.eq(9).text();
		var cgst_amount = $tds.eq(10).text();

		var igst = $tds.eq(11).text();
		var igst_amount = $tds.eq(12).text();
		var amount = $tds.eq(13).text();
		var tax_amount = $tds.eq(14).text();

		$('#description').val(description.trim())
		$('#hsn_code').val(hsn_code.trim())
		$('#InvList_type').val(type)
		$('#InvList_type').prop('disabled', true);
		$('#quantity').val(parseFloat(quantity))
		$('#unit').val(unit.trim())
		$('#item_amount').html(parseFloat(amount))
		$('#item_amount_temp').val(parseFloat(amount))
		$('#rate').val(parseFloat(rate))

		$('#tax_amount').html(parseFloat(tax_amount))
		$('#item_tax_temp').val(parseFloat(tax_amount))
		$('#cgst').val(cgst.trim());
		$('#cgst_amount').text(cgst_amount)
		$('#sgst').val(sgst.trim())
		$('#sgst_amount').text(sgst_amount)
		$('#igst').val(igst.trim())
		$('#igst_amount').text(igst_amount)

		$(".item_save").attr('value', 'Update');
		$(".item_save").attr('id', item_id);
		$(".item_save").attr('id', item_id);
		$('#description').focus();


	})

	$(document).on("change", "#sales_round_off", function (event) {

		var roundOff = parseFloat(this.value);
		var invoice_id = $('#invoice_id').val();
		if (isNaN(roundOff)) roundOff = 0;
		if (invoice_id !== '') {
			$('.loading-overlay').addClass('is-active');
			$.ajax({
				url: "<?php echo Yii::app()->createAbsoluteUrl("invoice/updateInvoiceamount"); ?>",
				data: {
					'round_off': roundOff,
					invoice_id: invoice_id
				},
				type: "POST",
				dataType: 'json',
				success: function (data) {
					if (data.status === 1) {
						$('#net_amount').html(parseFloat(data.totalamount).toFixed(2));
						var total_additional = parseFloat($('#totaladditional_charge').html());
						var grand_sum = parseFloat($('#net_amount').html());
						var grand_sum = grand_sum + total_additional;
						var formatted_grand_sum = grand_sum.toLocaleString('en-IN', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
						$('#Bills_bill_totalamount').val(formatted_grand_sum);
						$('#Bills_round_off').val(roundOff);
						$().toastmessage('showSuccessToast', "Total amount updated successfully");
					} else {
						$().toastmessage('showErrorToast', "Error occured");
					}
				}
			});
		}
	});
</script>