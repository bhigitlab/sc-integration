<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Labour Template',
);
$page = Yii::app()->request->getParam('labtemplate_page');
if ($page != '') {
    Yii::app()->user->setReturnUrl($page);
} else {
    Yii::app()->user->setReturnUrl(0);
}
?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container" id="expensetype">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
   
    <div class="clearfix">
        <div class="add-btn pull-right">
            <?php
            if (isset(Yii::app()->user->role) && (in_array('/labtemplate/create', Yii::app()->user->menuauthlist))) {
            ?>
                <a class="button addexpense">Add Labour Template</a>
            <?php } ?>
        </div>
        <h2>Labour Template</h2>
    </div>
    <div id="addexpense" style="display:none;"></div>
    <div id="errMsg"></div>
    <div class="row">
        <div class="col-md-12">
            <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_newview',
                'template' => '<div class="container1"><table cellpadding="10" id="exptypetbl" class="table">{items}</table></div>',
                'ajaxUpdate' => false,
            ));
            ?>
        </div>

        <?php
        Yii::app()->clientScript->registerScript('myjavascript', '
             $(document).ready(function(){
               $("#exptypetbl").dataTable( {
                    "scrollY": "300px",
                    "scrollCollapse": true,
                    "paging": true,
                     "columnDefs"        : [       
                        { 
                            "searchable"    : false, 
                            "targets"       : [0,2] 
                        },
                        { "bSortable": false, "aTargets": [-1]  }
                    ],
                } );
                });

            ');
        ?>

        <script>
            function closeaction() {
                $('#addexpense').slideUp(500);
            }

            $(document).ready(function() {
                $('.addexpense').click(function() {
                    $('.loading-overlay').addClass('is-active');
                    var id = $(this).attr('data-id');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $this->createUrl('labtemplate/create&layout=1') ?>",
                        success: function(response) {
                            $('#addexpense').html(response).slideDown();
                        },
                    });
                });
                $('.editProject').click(function() {
                    $('.loading-overlay').addClass('is-active');
                    var id = $(this).attr('data-id');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $this->createUrl('labtemplate/update&layout=1&id=') ?>" + id,
                        success: function(response) {
                            $('.loading-overlay').removeClass('is-active');
                            $('#addexpense').html(response).slideDown();
                        },
                    });
                });
                $('.deleteProject').click(function() {
                    $('.loading-overlay').addClass('is-active');
                    var id = $(this).attr('data-id');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $this->createUrl('labtemplate/delete&layout=1&id=') ?>" + id,
                        dataType: 'json',
                        success: function(response) {
                            $('html, body').animate({ scrollTop: 0 }, 'slow');
                            $("#errMsg").show()
                                .html('<div class="alert alert-'+response.success+'">'+response.message+'</div>')
                                .fadeOut(10000);
                            setTimeout(function () {                        
                                location.reload(true);
                            }, 3000);
                                
                           
                        },
                    });
                });


                jQuery(function($) {
                    $('#addExpensetype').on('keydown', function(event) {
                        if (event.keyCode == 13) {
                            $("#expensetypesearch").submit();
                        }
                    });
                });

            });
            $(document).ajaxComplete(function() {
                $('.loading-overlay').removeClass('is-active');
                $('#loading').hide();
            });
        </script>
    </div>
</div>
<style>
    .savepdf {
        background-color: #6a8ec7;
        border: 1px solid #6a8ec8;
        color: #fff;
        padding: 5px;
    }

    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    .panel {
        border: 1px solid #ddd;
    }

    .panel-heading {
        background-color: #eee;
        height: 40px;
    }
</style>