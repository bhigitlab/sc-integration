<?php
$tblpx      = Yii::app()->db->tablePrefix;
$section    = '';
if ($data['editrequest_table'] == "{$tblpx}purchase") {
    $section = "Company Edit";
}
if ($data['editrequest_table'] == "{$tblpx}expenses") {
    $section = "Daybook";
} else if ($data['editrequest_table'] == "{$tblpx}dailyvendors") {
    $section    = "Vendor Payment";
}
if ($data['editrequest_table'] == "{$tblpx}subcontractor_payment") {
    $section    = "Subcontractor Payment";
}
if ($data['editrequest_table'] == "{$tblpx}dailyexpense") {
    $section    = "Daily Expense";
}
if ($data['editrequest_table'] == "{$tblpx}dailyreport") {
    $section    = "Daily Report";
}
if ($data['editrequest_table'] == "{$tblpx}purchase") {
    $olddata = Purchase::model()->findByPk($data['parent_id']);
} else if ($data['editrequest_table'] == "{$tblpx}expenses") {
    $olddata = Expenses::model()->findByPk($data['parent_id']);
} else if ($data['editrequest_table'] == "{$tblpx}dailyvendors") {
    $olddata = Dailyvendors::model()->findByPk($data['parent_id']);
} else if ($data['editrequest_table'] == "{$tblpx}subcontractor_payment") {
    $olddata = SubcontractorPayment::model()->findByPk($data['parent_id']);
} else if ($data['editrequest_table'] == "{$tblpx}dailyreport") {
    $olddata = Dailyreport::model()->findByPk($data['parent_id']);
} else {
    $olddata = Dailyexpense::model()->findByPk($data['parent_id']);
}

$editrequest  = Editrequest::model()->findByPK($data['editrequest_id']);
$editdata    = json_decode($editrequest->editrequest_data);

?>
<div class="parent_row">
    <div class="row box-collapse">
        <div class="col-md-1 logo_sec">
            <input type="checkbox" name="editrequest" class="editrequestcheck" id="editrequest<?php echo $index; ?>" value="<?php echo $data->editrequest_id; ?>" />
            <button class="btn_logo">ER</button>
        </div>
        <div class="col-md-8 content_sec">
            <div class="row  clearfix">
                <div class="col-md-8">
                    <div class="list-item">
                        <?php
                        $user = Users::model()->findByPk($data['user_id']);
                        ?>
                        <b><?php echo $user->first_name . ' ' . $user->last_name; ?></b> Changed <b><?php echo $section; ?></b> data

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="list-item pull-right">
                        <label>Date:</label>
                        <span><?php echo date('d-m-Y', strtotime($data['editrequest_date'])); ?></span>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="list-item text-right act_btn">
                <button class="btn btn-xs btn_approve btn_sec approve_edit" id="<?php echo $data['editrequest_id']; ?>">Approve</button>&nbsp;<button class="btn btn-xs btn_decline btn_sec cancel_edit" id="<?php echo $data['editrequest_id']; ?>">Decline </button>
                <button class="compare_icon popup btn btn-xs btn-default" title="Compare">Compare</i></button>
                <button class="box_close btn btn-xs btn-default" style="display:none;">Close</i></button>
            </div>
        </div>
    </div>
    <div class="compare_box popuptext" style="display:none;">
        <?php if ($data['editrequest_table'] == "{$tblpx}purchase") { ?>
            <div class="box_content">
                <table class="table w-100">
                    <thead>
                        <tr>
                            <th>Columns</th>
                            <th class="text-center">Company</th>
                            <th class="text-center">Project</th>
                            <th class="text-center">Expense Head</th>
                            <th class="text-center">Vendor</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Purchase No</th>
                            <th class="text-center">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            $company = Company::model()->findByPk($editdata->company_id);
                            ?>
                            <th>Old</th>
                            <td class="text-center"><?php echo $company->name; ?></td>
                            <td class="text-center"><?php echo $olddata->project->name; ?></td>
                            <td class="text-center"><?php echo $olddata->expensehead->type_name; ?></td>
                            <td class="text-center"><?php echo $olddata->vendor->name; ?></td>
                            <td class="text-center"><?php echo date('d-m-Y', strtotime($olddata->purchase_date)); ?></td>
                            <td class="text-center"><?php echo $olddata->purchase_no; ?></td>
                            <td class="text-center"><?php echo $olddata->total_amount; ?></td>

                        </tr>
                        <tr>
                            <th>New</th>

                            <td class="text-center" style="background: #fff3cd;"><?php echo $olddata->company->name; ?></td>
                            <td class="text-center"><?php echo $editdata->project_name; ?></td>
                            <td class="text-center"><?php echo $olddata->expensehead->type_name; ?></td>
                            <td class="text-center"><?php echo $olddata->vendor->name; ?></td>
                            <td class="text-center"><?php echo date('d-m-Y', strtotime($editdata->date)); ?></td>
                            <td class="text-center"><?php echo $editdata->no; ?></td>
                            <td class="text-center"><?php echo $olddata->total_amount; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        <?php } ?>
        <?php
        if ($data['editrequest_table'] == "{$tblpx}dailyexpense") {

            if ($olddata->dailyexpense_type == "deposit") {
                $depositId = $olddata->expensehead_id;
                $deposit   = Deposit::model()->findByPk($depositId);
                $thead = "Deposit - " . $deposit["deposit_name"];
            } else if ($olddata->dailyexpense_type == "expense") {
                $expense   = Companyexpensetype::model()->findByPk($olddata->exp_type_id);
                $thead = "Expense - " . $expense['name'];
            } else {
                $receipt   = Companyexpensetype::model()->findByPk($olddata->exp_type_id);
                $thead = "Receipt - " . $receipt['name'];
            }
            if ($olddata->expense_type) {
                if ($olddata->expense_type == 88) {
                    $expenseType = "Cheque";
                } else if ($olddata->expense_type == 89) {
                    $expenseType = "Cash";
                } else if ($olddata->expense_type == 103) {
                    $expenseType = "Petty Cash";
                } else {
                    $expenseType = "Credit";
                }
            } else {
                $expenseType = NULL;
            }

            if ($olddata->dailyexpense_purchase_type) {
                if ($olddata->dailyexpense_purchase_type == 1) $purchaseType = "Credit";
                else if ($olddata->dailyexpense_purchase_type == 2) $purchaseType = "Full Paid";
                else if ($olddata->dailyexpense_purchase_type == 3) $purchaseType = "Partially Paid";
            } else {
                $internalTrans  = Dailyexpense::model()->find(array('condition' => 'transaction_parent="' . $olddata->dailyexp_id . '"'));
                if (!empty($internalTrans)) {
                    $type_transaction = $internalTrans->dailyexpense_purchase_type;
                    if ($type_transaction == 1) $purchaseType = "Credit";
                    else if ($type_transaction == 2) $purchaseType = "Full Paid";
                    else if ($type_transaction == 3) $purchaseType = "Partially Paid";
                } else {
                    $purchaseType = "";
                }
            }

            if ($olddata->employee_id != '') {
                $users = Users::model()->findByPk($olddata->employee_id);
                $employee = $users->first_name . ' ' . $users->last_name;
            } else {
                $employee = '';
            }

            $gsttotal = $olddata->dailyexpense_sgst + $olddata->dailyexpense_cgst + $olddata->dailyexpense_igst;

            if ($editdata->model1->dailyexpense_type == 3) {
                $depositId = $editdata->model1->expensehead_id;
                $deposit   = Deposit::model()->findByPk($depositId);
                $thead2 = "Deposit - " . $deposit["deposit_name"];
            } else if ($editdata->model1->dailyexpense_type == 1) {
                $expense   = Companyexpensetype::model()->findByPk($editdata->model1->exp_type_id);
                $thead2 = "Expense - " . $expense['name'];
            } else {
                $receipt   = Companyexpensetype::model()->findByPk($editdata->model1->exp_type_id);
                $thead2 = "Receipt - " . $receipt['name'];
            }
            if ($editdata->model1->expense_type) {
                if ($editdata->model1->expense_type == 88) {
                    $expenseType2 = "Cheque";
                } else if ($editdata->model1->expense_type == 89) {
                    $expenseType2 = "Cash";
                } else if ($editdata->model1->expense_type == 103) {
                    $expenseType2 = "Petty Cash";
                } else {
                    $expenseType2 = "Credit";
                }
            } else {
                $expenseType2 = NULL;
            }

            if ($editdata->model1->dailyexpense_purchase_type) {
                if ($editdata->model1->dailyexpense_purchase_type == 1) $purchaseType2 = "Credit";
                else if ($editdata->model1->dailyexpense_purchase_type == 2) $purchaseType2 = "Full Paid";
                else if ($editdata->model1->dailyexpense_purchase_type == 3) $purchaseType2 = "Partially Paid";
            } else {
                $internalTrans  = Dailyexpense::model()->find(array('condition' => 'transaction_parent="' . $editdata->model1->dailyexp_id . '"'));
                if (!empty($internalTrans)) {
                    $type_transaction = $internalTrans->dailyexpense_purchase_type;
                    if ($type_transaction == 1) $purchaseType2 = "Credit";
                    else if ($type_transaction == 2) $purchaseType2 = "Full Paid";
                    else if ($type_transaction == 3) $purchaseType2 = "Partially Paid";
                } else {
                    $purchaseType2 = "";
                }
            }

            if ($editdata->model1->employee_id != '') {
                $users = Users::model()->findByPk($editdata->model1->employee_id);
                $employee2 = $users->first_name . ' ' . $users->last_name;
            } else {
                $employee2 = '';
            }

            $gsttotal2 = $editdata->model1->dailyexpense_sgst + $editdata->model1->dailyexpense_cgst + $editdata->model1->dailyexpense_igst;
            $company = Company::model()->findByPk($editdata->model1->company_id);
            $bank = Bank::model()->findByPk($editdata->model1->bank_id);
        ?>
            <div class="box_content">
                <table style="width:100%">
                    <thead>
                        <tr>
                            <th>Columns</th>
                            <th class="text-center">Company</th>
                            <th class="text-center">Bill No / Transaction Head</th>
                            <th class="text-center">Transaction Type</th>
                            <th class="text-center">Bank</th>
                            <th class="text-center">Cheque No</th>
                            <th class="text-center">Description</th>
                            <th class="text-center">Amount</th>
                            <th class="text-center">Tax</th>
                            <th class="text-center">Total</th>
                            <th class="text-center">Receipt</th>
                            <th class="text-center">Paid</th>
                            <th class="text-center">Payment Type</th>
                            <th class="text-center">Paid To</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                            <th>Old</th>
                            <td class="text-center"><?php echo $olddata->Company->name; ?></td>
                            <td class="text-center"><?php echo $thead; ?></td>
                            <td class="text-center"><?php echo $expenseType; ?></td>
                            <td class="text-center"><?php echo isset($olddata->bank_id) ? $olddata->Bank->bank_name : ""; ?></td>
                            <td class="text-center"><?php echo $olddata->dailyexpense_chequeno; ?></td>
                            <td class="text-center"><?php echo $olddata->description; ?></td>
                            <td class="text-center"><?php echo $olddata->dailyexpense_amount ? Controller::money_format_inr($olddata->dailyexpense_amount, 2) : ""; ?></td>
                            <td class="text-center"><?php echo $gsttotal ? Controller::money_format_inr($gsttotal, 2) : ""; ?></td>
                            <td class="text-center"><?php echo $olddata->amount ? Controller::money_format_inr($olddata->amount, 2) : ""; ?></td>
                            <td class="text-center"><?php echo $olddata->dailyexpense_receipt ? Controller::money_format_inr($olddata->dailyexpense_receipt, 2) : ""; ?></td>
                            <td class="text-center"><?php echo ($olddata->dailyexpense_paidamount != NULL) ? Controller::money_format_inr($olddata->dailyexpense_paidamount, 2) : ""; ?></td>
                            <td class="text-center"><?php echo $purchaseType ? $purchaseType : ""; ?></td>
                            <td class="text-center"><?php echo $employee; ?></td>
                        </tr>
                        <tr>
                            <th>New</th>
                            <td class="text-center" style="<?php echo ($olddata->Company->name != $company->name) ? "background: #fff3cd" : ""; ?>"><?php echo $company->name; ?></td>
                            <td class="text-center" style="<?php echo ($thead != $thead2) ? "background: #fff3cd" : ""; ?>"><?php echo $thead2; ?></td>
                            <td class="text-center" style="<?php echo ($expenseType != $expenseType2) ? "background: #fff3cd" : ""; ?>"><?php echo $expenseType2; ?></td>
                            <td class="text-center" style="<?php echo ((isset($olddata->bank_id) ? $olddata->Bank->bank_name : "") != (isset($editdata->model1->bank_id) ? $bank->bank_name : "")) ? "background: #fff3cd" : ""; ?>"><?php echo isset($editdata->model1->bank_id) ? $bank->bank_name : ""; ?></td>
                            <td class="text-center" style="<?php echo ((isset($olddata->dailyexpense_chequeno) ? $olddata->dailyexpense_chequeno : "") != ($editdata->model1->dailyexpense_chequeno)) ? "background: #fff3cd" : ""; ?>"><?php echo $editdata->model1->dailyexpense_chequeno; ?></td>
                            <td class="text-center" style="<?php echo ($olddata->description != $editdata->model1->description) ? "background: #fff3cd" : ""; ?>"><?php echo $editdata->model1->description; ?></td>
                            <td class="text-center" style="<?php echo (($olddata->dailyexpense_amount ? Controller::money_format_inr($olddata->dailyexpense_amount, 2) : "") != ($editdata->model1->dailyexpense_amount ? Controller::money_format_inr($editdata->model1->dailyexpense_amount, 2) : "")) ? "background: #fff3cd" : ""; ?>><?php echo $editdata->model1->dailyexpense_amount ? Controller::money_format_inr($editdata->model1->dailyexpense_amount, 2) : ""; ?></td>
                        <td class=" text-center" style="<?php echo (($gsttotal ? Controller::money_format_inr($gsttotal, 2) : "") != ($gsttotal2 ? Controller::money_format_inr($gsttotal2, 2) : "")) ? "background: #fff3cd" : ""; ?>"><?php echo $gsttotal2 ? Controller::money_format_inr($gsttotal2, 2) : ""; ?></td>
                            <td class="text-center" style="<?php echo (($olddata->amount ? Controller::money_format_inr($olddata->amount, 2) : "") != ($editdata->model1->amount ? Controller::money_format_inr($editdata->model1->amount, 2) : "")) ? "background: #fff3cd" : ""; ?>"><?php echo $editdata->model1->amount ? Controller::money_format_inr($editdata->model1->amount, 2) : ""; ?></td>
                            <td class="text-center" style="<?php echo (($olddata->dailyexpense_receipt ? Controller::money_format_inr($olddata->dailyexpense_receipt, 2) : "") != ($editdata->model1->dailyexpense_receipt ? Controller::money_format_inr($editdata->model1->dailyexpense_receipt, 2) : "")) ? "background: #fff3cd" : ""; ?>"><?php echo $editdata->model1->dailyexpense_receipt ? Controller::money_format_inr($editdata->model1->dailyexpense_receipt, 2) : ""; ?></td>
                            <td class="text-center" style="<?php echo ((($olddata->dailyexpense_paidamount != NULL) ? Controller::money_format_inr($olddata->dailyexpense_paidamount, 2) : "") != (($editdata->model1->dailyexpense_paidamount != NULL) ? Controller::money_format_inr($olddata->dailyexpense_paidamount, 2) : "")) ? "background: #fff3cd" : ""; ?>"><?php echo ($editdata->model1->dailyexpense_paidamount != NULL) ? Controller::money_format_inr($olddata->dailyexpense_paidamount, 2) : ""; ?></td>
                            <td class="text-center" style="<?php echo (($purchaseType ? $purchaseType : "") != ($purchaseType2 ? $purchaseType2 : "")) ? "background: #fff3cd" : ""; ?>"><?php echo $purchaseType2 ? $purchaseType2 : ""; ?></td>
                            <td class="text-center" style="<?php echo ($employee != $employee2) ? "background: #fff3cd" : ""; ?>"><?php echo $employee2; ?></td>

                        </tr>
                    </tbody>
                </table>
            </div>
        <?php } ?>

        <?php if ($data['editrequest_table'] == "{$tblpx}expenses") { ?>
            <div class="box_content overflow_auto">
                <table style="width:100%">
                    <thead>
                        <tr>
                            <th>Columns</th>
                            <th class="text-center">Company</th>
                            <th class="text-center">Project</th>
                            <th class="text-center">Bill Number</th>
                            <th class="text-center">Invoice Number</th>
                            <th class="text-center">Return Number</th>
                            <th class="text-center">Expense Head</th>
                            <th class="text-center">Vendor</th>
                            <th class="text-center">Expense Type</th>
                            <th class="text-center">Petty Cash By</th>
                            <th class="text-center">Bank</th>
                            <th class="text-center">Cheque No</th>
                            <th class="text-center">Description</th>
                            <th class="text-center">Amount</th>
                            <th class="text-center">Tax</th>
                            <th class="text-center">Total</th>
                            <th class="text-center">Amount Paid</th>
                            <th class="text-center">TDS</th>
                            <th class="text-center">Receipt Type</th>
                            <th class="text-center">Receipt</th>
                            <th class="text-center">Purchase Type</th>
                            <th class="text-center">Amount to Beneficiary</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            $company = Company::model()->findByPk($olddata->company_id);
                            $bill    = Bills::model()->findByPk($olddata->bill_id);
                            $incoice = Invoice::model()->findByPk($olddata->invoice_id);
                            $return = PurchaseReturn::model()->findByPk($olddata->return_id);
                            $exphead = ExpenseType::model()->findByPk($olddata->exptype);
                            $gsttotal = $olddata->expense_sgst + $olddata->expense_cgst + $olddata->expense_igst;
                            if ($olddata->payment_type) {
                                if ($olddata->payment_type == 88) {
                                    $receiptType = "Cheque";
                                } else if ($olddata->payment_type == 89) {
                                    $receiptType = "Cash";
                                } else if ($olddata->payment_type == 103) {
                                    $receiptType = "Petty Cash";
                                } else {
                                    $receiptType = "Credit";
                                }
                            } else {
                                $receiptType = NULL;
                            }
                            if ($olddata->expense_type) {
                                if ($olddata->expense_type == 88) {
                                    $expenseType = "Cheque";
                                } else if ($olddata->expense_type == 89) {
                                    $expenseType = "Cash";
                                } else if ($olddata->expense_type == 103) {
                                    $expenseType = "Petty Cash";
                                } else {
                                    $expenseType = "Credit";
                                }
                            } else {
                                $expenseType = NULL;
                            }
                            if ($olddata->purchase_type) {
                                if ($olddata->purchase_type == 1) $purchaseType = "Credit";
                                else if ($olddata->purchase_type == 2) $purchaseType = "Full Paid";
                                else if ($olddata->purchase_type == 3) $purchaseType = "Partially Paid";
                            } else
                                $purchaseType = "";
                            if ($olddata->expense_type == 0) {
                                $expense_type = "Credit";
                            } else if ($olddata->expense_type == 88) {
                                $expense_type = "Cheque";
                            } else if ($olddata->expense_type == 89) {
                                $expense_type = "Cash";
                            } else if ($olddata->expense_type == 103) {
                                $expense_type = "Petty Cash";
                            }
                            if ($olddata->employee_id != '') {
                                $user = Users::model()->findByPk($olddata->employee_id);
                                $petty_cash_by = $user->first_name . ' ' . $user->last_name;
                            } else {
                                $petty_cash_by = "";
                            }

                            if ($olddata->bank_id != '') {
                                $exp_bank_old = Bank::model()->findByPk($olddata->bank_id);
                                $bank_name = $exp_bank_old->bank_name;
                            } else {
                                $bank_name = "";
                            }


                            $company2 = Company::model()->findByPk($editdata->company_id);
                            $bill2    = Bills::model()->findByPk($editdata->bill_id);
                            $incoice2 = Invoice::model()->findByPk($editdata->invoice_id);
                            $return2 = PurchaseReturn::model()->findByPk($editdata->return_id);
                            $exphead2 = ExpenseType::model()->findByPk($editdata->exptype);
                            $gsttotal2 = $editdata->expense_sgst + $editdata->expense_cgst + $editdata->expense_igst;
                            $expense_type2 = "";
                            if ($editdata->payment_type) {
                                if ($editdata->payment_type == 88) {
                                    $receiptType2 = "Cheque";
                                } else if ($editdata->payment_type == 89) {
                                    $receiptType2 = "Cash";
                                } else if ($editdata->payment_type == 103) {
                                    $receiptType2 = "Petty Cash";
                                } else {
                                    $receiptType2 = "Credit";
                                }
                            } else {
                                $receiptType2 = NULL;
                            }
                            if ($editdata->expense_type) {
                                if ($editdata->expense_type == 88) {
                                    $expenseType2 = "Cheque";
                                } else if ($editdata->expense_type == 89) {
                                    $expenseType2 = "Cash";
                                } else if ($editdata->expense_type == 103) {
                                    $expenseType2 = "Petty Cash";
                                } else {
                                    $expenseType2 = "Credit";
                                }
                            } else {
                                $expenseType2 = NULL;
                            }
                            if (isset($editdata->purchase_type)) {
                                if ($editdata->purchase_type == 1) $purchaseType2 = "Credit";
                                else if ($editdata->purchase_type == 2) $purchaseType2 = "Full Paid";
                                else if ($editdata->purchase_type == 3) $purchaseType2 = "Partially Paid";
                            } else
                                $purchaseType2 = "";
                            if ($editdata->expense_type == 0) {
                                $expense_type2 = "Credit";
                            } else if ($editdata->expense_type == 88) {
                                $expense_type2 = "Cheque";
                            } else if ($editdata->expense_type == 89) {
                                $expense_type2 = "Cash";
                            } else if ($editdata->expense_type == 103) {
                                $expense_type2 = "Petty Cash";
                            }
                            if ($editdata->employee_id != '') {
                                $user = Users::model()->findByPk($editdata->employee_id);
                                $petty_cash_by2 = $user->first_name . ' ' . $user->last_name;
                            } else {
                                $petty_cash_by2 = "";
                            }

                            if ($editdata->bank_id != '') {
                                $exp_bank = Bank::model()->findByPk($editdata->bank_id);
                                $bank_name2 = $exp_bank->bank_name;
                            } else {
                                $bank_name2 = "";
                            }

                            $project = Projects::model()->findByPk($editdata->projectid);

                            if ($editdata->vendor_id != '') {
                                $vendor = Vendors::model()->findByPk($editdata->vendor_id);
                                $vendor_name = $vendor->name;
                            } else {
                                $vendor_name = "";
                            }
                            ?>
                            <th>Old</th>
                            <td class="text-center"><?php echo $company->name; ?></td>
                            <td class="text-center"><?php echo $olddata->project->name; ?></td>
                            <td class="text-center"><?php echo isset($bill->billno) ? $bill->billno : ""; ?></td>
                            <td class="text-center"><?php echo isset($olddata->invoice_no) ? $incoice->invoice_no : ""; ?></td>
                            <td class="text-center"><?php echo isset($olddata->return_id) ? $return->return_number : ""; ?></td>
                            <td class="text-center"><?php echo isset($olddata->exptype) ? $exphead->type_name : ""; ?></td>
                            <td class="text-center"><?php echo isset($olddata->vendor_id) ? $olddata->vendor->name : ""; ?></td>
                            <td class="text-center"><?php echo $expense_type; ?></td>
                            <td class="text-center"><?php echo $petty_cash_by; ?></td>
                            <td class="text-center"><?php echo $bank_name; ?></td>
                            <td class="text-center"><?php echo isset($olddata->cheque_no) ? $olddata->cheque_no : ""; ?></td>
                            <td class="text-center"><?php echo isset($olddata->description) ? $olddata->description : ""; ?></td>
                            <td class="text-center"><?php echo $olddata->expense_amount ? Controller::money_format_inr($olddata->expense_amount, 2) : ""; ?></td>
                            <td class="text-center"><?php echo $gsttotal ? Controller::money_format_inr($gsttotal, 2) : ""; ?></td>
                            <td class="text-center"><?php echo $olddata->amount ? Controller::money_format_inr($olddata->amount, 2) : ""; ?></td>
                            <td class="text-center"><?php echo ($olddata->paid != 0) ? Controller::money_format_inr($olddata->paid, 2) : ""; ?></td>
                            <td class="text-center"><?php echo $olddata->expense_tds ? Controller::money_format_inr($olddata->expense_tds, 2) : ""; ?></td>

                            <td class="text-center"><?php echo $receiptType ? $receiptType : ""; ?></td>
                            <td class="text-center"><?php echo $olddata->receipt ? Controller::money_format_inr($olddata->receipt, 2) : ""; ?></td>
                            <td class="text-center"><?php echo $purchaseType ? $purchaseType : ""; ?></td>
                            <td class="text-center"><?php echo ($olddata->paidamount != 0) ? Controller::money_format_inr($olddata->paidamount, 2) : ""; ?></td>
                        </tr>
                        <tr>
                            <th>New</th>
                            <td class="text-center" style="<?php echo ($company->name != $company2->name) ? "background: #fff3cd" : ""; ?>"><?php echo $company2->name; ?></td>
                            <td class="text-center" style="<?php echo ($olddata->project->name != $project->name) ? "background: #fff3cd" : ""; ?>"><?php echo $project->name; ?></td>
                            <td class="text-center" style="<?php echo ((isset($bill->billno) ? $bill->billno : "") != (isset($bill2->billno) ? $bill2->billno : "")) ? "background: #fff3cd" : ""; ?>"><?php echo isset($bill2->billno) ? $bill2->billno : ""; ?></td>
                            <td class="text-center" style="<?php echo ((isset($olddata->invoice_no) ? $incoice->invoice_no : "") != (isset($editdata->invoice_no) ? $incoice2->invoice_no : "")) ? "background: #fff3cd" : ""; ?>"><?php echo isset($editdata->invoice_no) ? $incoice2->invoice_no : ""; ?></td>
                            <td class="text-center" style="<?php echo ((isset($olddata->return_id) ? $return->return_number : "") != (isset($editdata->return_id) ? $return2->return_number : "")) ? "background: #fff3cd" : ""; ?>"><?php echo isset($editdata->return_id) ? $return2->return_number : ""; ?></td>
                            <td class="text-center" style="<?php echo ((isset($olddata->exptype) ? $exphead->type_name : "") != (isset($editdata->exptype) ? $exphead2->type_name : "")) ? "background: #fff3cd" : ""; ?>"><?php echo isset($editdata->exptype) ? $exphead2->type_name : ""; ?></td>
                            <td class="text-center" style="<?php echo ((isset($olddata->vendor_id) ? $olddata->vendor->name : "") != $vendor_name) ? "background: #fff3cd" : ""; ?>"><?php echo $vendor_name; ?></td>
                            <td class="text-center" style="<?php echo ($expense_type != $expense_type2) ? "background: #fff3cd" : ""; ?>"><?php echo $expense_type2; ?></td>
                            <td class="text-center" style="<?php echo ($petty_cash_by != $petty_cash_by2) ? "background: #fff3cd" : ""; ?>"><?php echo $petty_cash_by2; ?></td>
                            <td class="text-center" style="<?php echo ($bank_name != $bank_name2) ? "background: #fff3cd" : ""; ?>"><?php echo $bank_name2; ?></td>
                            <td class="text-center" style="<?php echo ((isset($olddata->cheque_no) ? $olddata->cheque_no : "") != (isset($editdata->cheque_no) ? $editdata->cheque_no : "")) ? "background: #fff3cd" : ""; ?>"><?php echo isset($editdata->cheque_no) ? $editdata->cheque_no : ""; ?></td>
                            <td class="text-center" style="<?php echo ((isset($olddata->description) ? $olddata->description : "") != (isset($editdata->description) ? $editdata->description : "")) ? "background: #fff3cd" : ""; ?>"><?php echo isset($editdata->description) ? $editdata->description : ""; ?></td>
                            <td class="text-center" style="<?php echo (($olddata->expense_amount ? Controller::money_format_inr($olddata->expense_amount, 2) : "") != ($editdata->expense_amount ? Controller::money_format_inr($editdata->expense_amount, 2) : "")) ? "background: #fff3cd" : ""; ?>"><?php echo $editdata->expense_amount ? Controller::money_format_inr($editdata->expense_amount, 2) : ""; ?></td>
                            <td class="text-center" style="<?php echo (($gsttotal ? Controller::money_format_inr($gsttotal, 2) : "") != ($gsttotal ? Controller::money_format_inr($gsttotal, 2) : "")) ? "background: #fff3cd" : ""; ?>"><?php echo $gsttotal ? Controller::money_format_inr($gsttotal, 2) : ""; ?></td>
                            <td class="text-center" style="<?php echo (($olddata->amount ? Controller::money_format_inr($olddata->amount, 2) : "") != ($editdata->amount ? Controller::money_format_inr($editdata->amount, 2) : "")) ? "background: #fff3cd" : ""; ?>"><?php echo $editdata->amount ? Controller::money_format_inr($editdata->amount, 2) : ""; ?></td>
                            <td class="text-center" style="<?php echo ((($olddata->paid != 0) ? Controller::money_format_inr($olddata->paid, 2) : "") != (($editdata->paid != 0) ? Controller::money_format_inr($editdata->paid, 2) : "")) ? "background: #fff3cd" : ""; ?>"><?php echo ($editdata->paid != 0) ? Controller::money_format_inr($editdata->paid, 2) : ""; ?></td>
                            <td class="text-center" style="<?php echo (($olddata->expense_tds ? Controller::money_format_inr($olddata->expense_tds, 2) : "") != ($editdata->expense_tds ? Controller::money_format_inr($editdata->expense_tds, 2) : "")) ? "background: #fff3cd" : ""; ?>"><?php echo $editdata->expense_tds ? Controller::money_format_inr($editdata->expense_tds, 2) : ""; ?></td>

                            <td class="text-center" style="<?php echo ($receiptType ? $receiptType : "" != ($receiptType2 ? $receiptType2 : "")) ? "background: #fff3cd" : ""; ?>"><?php echo $receiptType2 ? $receiptType2 : ""; ?></td>
                            <td class="text-center" style="<?php echo (($olddata->receipt ? Controller::money_format_inr($olddata->receipt, 2) : "") != ($editdata->receipt ? Controller::money_format_inr($editdata->receipt, 2) : "")) ? "background: #fff3cd" : ""; ?>"><?php echo $editdata->receipt ? Controller::money_format_inr($editdata->receipt, 2) : ""; ?></td>
                            <td class="text-center" style="<?php echo (($purchaseType ? $purchaseType : "") != ($purchaseType2 ? $purchaseType2 : "")) ? "background: #fff3cd" : ""; ?>"><?php echo $purchaseType2 ? $purchaseType2 : ""; ?></td>
                            <td class="text-center" style="<?php echo ((($olddata->paidamount != 0) ? Controller::money_format_inr($olddata->paidamount, 2) : "") != (($editdata->paidamount != 0) ? Controller::money_format_inr($editdata->paidamount, 2) : "")) ? "background: #fff3cd" : ""; ?>"><?php echo ($editdata->paidamount != 0) ? Controller::money_format_inr($editdata->paidamount, 2) : ""; ?></td>

                        </tr>
                    </tbody>
                </table>
            </div>
        <?php } ?>
        <?php if ($data['editrequest_table'] == "{$tblpx}dailyvendors") { ?>
            <div class="box_content">
                <table style="width:100%">
                    <thead>
                        <tr>
                            <th>Columns</th>
                            <th class="text-center">Company</th>
                            <th class="text-center">Project</th>
                            <th class="text-center">Vendor</th>
                            <th class="text-center">Description</th>
                            <th class="text-center">Transaction Type</th>
                            <th class="text-center">Bank</th>
                            <th class="text-center">Cheque No</th>
                            <th class="text-center">Amount</th>
                            <th class="text-center">Tax</th>
                            <th class="text-center">TDS Amount</th>
                            <th class="text-center">Paid Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            $company = Company::model()->findByPk($olddata->company_id);
                            if ($olddata->payment_type == 89) {
                                $payment_type = 'Cash';
                            } else {
                                $payment_type = 'Cheque';
                            }
                            if ($olddata->bank != '') {
                                $vendor_bank_old = Bank::model()->findByPk($olddata->bank);
                                $bank_name = $vendor_bank_old->bank_name;
                            } else {
                                $bank_name = '';
                            }

                            $company2 = Company::model()->findByPk($editdata->company_id);
                            if ($editdata->payment_type == 89) {
                                $payment_type2 = 'Cash';
                            } else {
                                $payment_type2 = 'Cheque';
                            }
                            if ($editdata->bank != '') {
                                $vendor_bank = Bank::model()->findByPk($editdata->bank);
                                $bank_name2 = $vendor_bank->bank_name;
                            } else {
                                $bank_name2 = '';
                            }
                            $project = Projects::model()->findByPk($editdata->project_id);
                            $vendor = Vendors::model()->findByPk($editdata->vendor_id);
                            ?>
                            <th>Old</th>
                            <td class="text-center"><?php echo $company->name; ?></td>
                            <td class="text-center"><?php echo (($olddata->project['name']) ? $olddata->project['name'] : ""); ?></td>
                            <td class="text-center"><?php echo $olddata->vendor->name; ?></td>
                            <td class="text-center"><?php echo $olddata->description; ?></td>
                            <td class="text-center"><?php echo $payment_type; ?></td>
                            <td class="text-center"><?php echo $bank_name; ?></td>
                            <td class="text-center"><?php echo $olddata->cheque_no; ?></td>
                            <td class="text-center"><?php echo $olddata->amount ? Controller::money_format_inr($olddata->amount, 2) : ""; ?></td>
                            <td class="text-center" <?php echo $olddata->tax_amount ? Controller::money_format_inr($olddata->tax_amount, 2) : ""; ?></td> <td class="text-center"><?php echo Controller::money_format_inr($olddata->tds_amount, 2); ?></td>
                            <td class="text-center"><?php echo Controller::money_format_inr($olddata->paidamount, 2); ?></td>
                        </tr>
                        <tr>
                            <th>New</th>
                            <td class="text-center" style="<?php echo ($company->name != $company2->name) ? "background: #fff3cd" : ""; ?>"><?php echo $company2->name; ?></td>
                            <td class="text-center" style="<?php echo ($olddata->project['name'] != $project['name']) ? "background: #fff3cd" : "";
                                                            ?>"><?php echo ($project != "") ? $project->name : ""; ?></td>
                            <td class="text-center" style="<?php echo ($olddata->vendor->name != $vendor->name) ? "background: #fff3cd" : ""; ?>"><?php echo $vendor->name; ?></td>
                            <td class="text-center" style="<?php echo ($olddata->description != $editdata->description) ? "background: #fff3cd" : ""; ?>"><?php echo $editdata->description; ?></td>
                            <td class="text-center" style="<?php echo ($payment_type != $payment_type2) ? "background: #fff3cd" : ""; ?>"><?php echo $payment_type2; ?></td>
                            <td class="text-center" style="<?php echo ($bank_name != $bank_name2) ? "background: #fff3cd" : ""; ?>"><?php echo $bank_name2; ?></td>
                            <td class="text-center" style="<?php echo ($olddata->cheque_no != $editdata->cheque_no) ? "background: #fff3cd" : ""; ?>"><?php echo $editdata->cheque_no; ?></td>
                            <td class="text-center" style="<?php echo (($olddata->amount ? Controller::money_format_inr($olddata->amount, 2) : "") != ($editdata->amount ? Controller::money_format_inr($editdata->amount, 2) : "")) ? "background: #fff3cd" : ""; ?>"><?php echo $editdata->amount ? Controller::money_format_inr($editdata->amount, 2) : ""; ?></td>
                            <td class="text-center" style="<?php echo (($olddata->tax_amount ? Controller::money_format_inr($olddata->tax_amount, 2) : "") != ($editdata->tax_amount ? Controller::money_format_inr($editdata->tax_amount, 2) : "")) ? "background: #fff3cd" : ""; ?>"><?php echo $editdata->tax_amount ? Controller::money_format_inr($editdata->tax_amount, 2) : ""; ?></td>
                            <td class="text-center" style="<?php echo ((isset($olddata->tds_amount) ? Controller::money_format_inr($olddata->tds_amount, 2) : "") != (isset($editdata->tds_amount) ? Controller::money_format_inr($editdata->tds_amount, 2) : "")) ? "background: #fff3cd" : ""; ?>"><?php echo isset($editdata->tds_amount) ? Controller::money_format_inr($editdata->tds_amount, 2) : ""; ?></td>
                            <td class="text-center" style="<?php echo ((isset($olddata->paidamount) ? Controller::money_format_inr($olddata->paidamount, 2) : "") != (isset($editdata->paidamount) ? Controller::money_format_inr($editdata->paidamount, 2) : "")) ? "background: #fff3cd" : ""; ?>"><?php echo isset($editdata->paidamount) ? Controller::money_format_inr($editdata->paidamount, 2) : ""; ?></td>

                        </tr>
                    </tbody>
                </table>
            </div>
        <?php } ?>

        <?php if ($data['editrequest_table'] == "{$tblpx}subcontractor_payment") { ?>
            <div class="box_content">
                <table style="width:100%">
                    <thead>
                        <tr>
                            <th>Columns</th>
                            <th class="text-center">Company</th>
                            <th class="text-center">Subcontractor</th>
                            <th class="text-center">Project</th>
                            <th class="text-center">Description</th>
                            <th class="text-center">Transaction Type</th>
                            <th class="text-center">Bank</th>
                            <th class="text-center">Cheque No</th>
                            <th class="text-center">Amount</th>
                            <th class="text-center">Tax</th>
                            <th class="text-center">TDS Amount</th>
                            <th class="text-center">Paid Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            $company = Company::model()->findByPk($editdata->company_id);
                            if (isset($olddata->payment_type)) {
                                if ($olddata->payment_type == 89) {
                                    $payment_type = 'Cash';
                                } else {
                                    $payment_type = 'Cheque';
                                }
                            } else {
                                $payment_type = "";
                            }
                            if (isset($olddata->bank)) {
                                $subcon_bank_old = Bank::model()->findByPk($olddata->bank);
                                $bank_name = $subcon_bank_old->bank_name;
                            } else {
                                $bank_name = '';
                            }

                            $company2 = Company::model()->findByPk($editdata->payment0->company_id);
                            if ($editdata->payment0->payment_type == 89) {
                                $payment_type2 = 'Cash';
                            } else {
                                $payment_type2 = 'Cheque';
                            }
                            if ($editdata->payment0->bank != '') {
                                $subcon_bank = Bank::model()->findByPk($editdata->payment0->bank);
                                $bank_name2 = $subcon_bank->bank_name;
                            } else {
                                $bank_name2 = '';
                            }
                            $project = Projects::model()->findByPk($editdata->payment0->project_id);
                            $project2 = Projects::model()->findByPk($editdata->payment0->project_id);
                            $subcontractor = Subcontractor::model()->findByPk($editdata->payment0->subcontractor_id);
                            ?>
                            <th>Old</th>
                            <td class="text-center"><?php echo $company->name; ?></td>
                            <td class="text-center"><?php // echo isset($editdata->payment0->subcontractor_id)?$olddata->subcontractor->subcontractor_name:""; 
                                                    ?></td>
                            <td class="text-center"><?php echo $project->name; ?></td>
                            <td class="text-center"><?php echo isset($olddata->description) ? $olddata->description : ""; ?></td>
                            <td class="text-center"><?php echo $payment_type; ?></td>
                            <td class="text-center"><?php echo $bank_name; ?></td>
                            <td class="text-center"><?php echo isset($olddata->cheque_no) ? $olddata->cheque_no : ""; ?></td>
                            <td class="text-center"><?php echo isset($olddata->amount) ? Controller::money_format_inr($olddata->amount, 2) : ""; ?></td>
                            <td class="text-center" <?php echo isset($olddata->tax_amount) ? Controller::money_format_inr($olddata->tax_amount, 2) : ""; ?></td> <td class="text-center"><?php echo isset($olddata->tds_amount) ? Controller::money_format_inr($olddata->tds_amount, 2) : ""; ?></td>
                            <td class="text-center"><?php echo isset($olddata->paidamount) ? Controller::money_format_inr($olddata->paidamount, 2) : ""; ?></td>
                        </tr>
                        <tr>
                            <th>New</th>
                            <td class="text-center" style="<?php echo ($company->name != $company2->name) ? "background: #fff3cd" : ""; ?>"><?php echo $company2->name; ?></td>
                            <td class="text-center" style="<?php echo ((isset($olddata->subcontractor) ? $olddata->subcontractor->subcontractor_name : "") != $subcontractor->subcontractor_name) ? "background: #fff3cd" : ""; ?>"><?php echo $subcontractor->subcontractor_name; ?></td>
                            <td class="text-center" style="<?php echo ($project->name != $project2->name) ? "background: #fff3cd" : ""; ?>"><?php echo $project2->name; ?></td>
                            <td class="text-center" style="<?php echo ((isset($olddata->description) ? $olddata->description : "") != $editdata->payment0->description) ? "background: #fff3cd" : ""; ?>"><?php echo $editdata->payment0->description; ?></td>
                            <td class="text-center" style="<?php echo ($payment_type != $payment_type2) ? "background: #fff3cd" : ""; ?>"><?php echo $payment_type2; ?></td>
                            <td class="text-center" style="<?php echo ($bank_name != $bank_name2) ? "background: #fff3cd" : ""; ?>"><?php echo $bank_name2; ?></td>
                            <td class="text-center" style="<?php echo ((isset($olddata->cheque_no) ? $olddata->cheque_no : "") != (isset($editdata->payment0->cheque_no) ? $editdata->payment0->cheque_no : "")) ? "background: #fff3cd" : ""; ?>"><?php echo isset($editdata->payment0->cheque_no) ? $editdata->payment0->cheque_no : ""; ?></td>
                            <td class="text-center" style="<?php echo ((isset($olddata->amount) ? Controller::money_format_inr($olddata->amount, 2) : "") != ($editdata->payment0->amount ? Controller::money_format_inr($editdata->payment0->amount, 2) : "")) ? "background: #fff3cd" : ""; ?>"><?php echo $editdata->payment0->amount ? Controller::money_format_inr($editdata->payment0->amount, 2) : ""; ?></td>
                            <td class="text-center" style="<?php echo ((isset($olddata->tax_amount) ? Controller::money_format_inr($olddata->tax_amount, 2) : "") != ($editdata->payment0->tax_amount ? Controller::money_format_inr($editdata->payment0->tax_amount, 2) : "")) ? "background: #fff3cd" : ""; ?>"><?php echo $editdata->payment0->tax_amount ? Controller::money_format_inr($editdata->payment0->tax_amount, 2) : ""; ?></td>
                            <td class="text-center" style="<?php echo ((isset($olddata->tds_amount) ? Controller::money_format_inr($olddata->tds_amount, 2) : "") != (isset($editdata->payment0->tds_amount) ? Controller::money_format_inr($editdata->payment0->tds_amount, 2) : "")) ? "background: #fff3cd" : ""; ?>"><?php echo isset($editdata->payment0->tds_amount) ? Controller::money_format_inr($editdata->payment0->tds_amount, 2) : ""; ?></td>
                            <td class="text-center" style="<?php echo ((isset($olddata->paidamount) ? Controller::money_format_inr($olddata->paidamount, 2) : "") != (isset($editdata->payment0->paidamount) ? Controller::money_format_inr($editdata->payment0->paidamount, 2) : "")) ? "background: #fff3cd" : ""; ?>"><?php echo isset($editdata->payment0->paidamount) ? Controller::money_format_inr($editdata->payment0->paidamount, 2) : ""; ?></td>

                        </tr>
                    </tbody>
                </table>
            </div>
        <?php } ?>

        <?php if ($data['editrequest_table'] == "{$tblpx}dailyreport") { ?>
            <div class="box_content">
                <table style="width:100%">
                    <thead>
                        <tr>
                            <th>Columns</th>
                            <th class="text-center">Company</th>
                            <th class="text-center">Project</th>
                            <th class="text-center">Subcontractor</th>
                            <th class="text-center">Expense Head</th>
                            <th class="text-center">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            $company = Company::model()->findByPk($olddata->company_id);
                            $exptype    = ExpenseType::model()->findByPk($olddata->expensehead_id);
                            $subcontractor = Subcontractor::model()->findByPk($olddata->subcontractor_id);
                            $project = Projects::model()->findByPk($olddata->projectid);

                            $company2 = Company::model()->findByPk($editdata->company_id);
                            $exptype2    = ExpenseType::model()->findByPk($editdata->expensehead_id);
                            $subcontractor2 = Subcontractor::model()->findByPk($editdata->subcontractor_id);
                            $project2 = Projects::model()->findByPk($editdata->projectid);
                            ?>
                            <th>Old</th>
                            <td class="text-center"><?php echo $company->name; ?></td>
                            <td class="text-center"><?php echo $project->name; ?></td>
                            <td class="text-center"><?php echo $subcontractor->subcontractor_name; ?></td>
                            <td class="text-center"><?php echo $exptype->type_name; ?></td>

                            <td class="text-center"><?php echo ($olddata->amount != 0) ? Controller::money_format_inr($olddata->amount, 2) : ""; ?></td>
                        </tr>
                        <tr>
                            <th>New</th>
                            <td class="text-center" style="<?php echo ($company->name != $company2->name) ? "background: #fff3cd" : ""; ?>"><?php echo $company2->name; ?></td>
                            <td class="text-center" style="<?php echo ($project->name != $project2->name) ? "background: #fff3cd" : ""; ?>"><?php echo $project->name; ?></td>
                            <td class="text-center" style="<?php echo ($subcontractor->subcontractor_name != $subcontractor2->subcontractor_name) ? "background: #fff3cd" : ""; ?>"><?php echo $subcontractor2->subcontractor_name; ?></td>
                            <td class="text-center" style="<?php echo ($exptype->type_name != $exptype2->type_name) ? "background: #fff3cd" : ""; ?>"><?php echo $exptype2->type_name; ?></td>
                            <td class="text-center" style="<?php echo ($olddata->amount != $editdata->amount) ? "background: #fff3cd" : ""; ?>"><?php echo Controller::money_format_inr($editdata->amount, 2); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        <?php } ?>

    </div>
</div>