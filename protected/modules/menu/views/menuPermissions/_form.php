<?php
/* @var $this MenuPermissionsController */
/* @var $model MenuPermissions */
/* @var $form CActiveForm */


$url = Yii::app()->createAbsoluteUrl("menu/menuPermissions/getusers");
$saveurl = Yii::app()->createAbsoluteUrl("menu/menuPermissions/savepermissions");
$geturl = Yii::app()->createAbsoluteUrl("menu/menuPermissions/getpermissions");

Yii::app()->clientScript->registerScript('myjquery', "
  

$('#clearbtn').click(function(){
    $('.checkitem').prop('checked', false);
    $('.selectall').prop('checked', false);
    $('.users-listview li').removeClass('active');
});

$('.selectall').change(function(){ 
    var id = $(this).attr('id');

    $('.checkbox'+id).prop('checked', $(this).prop('checked')); 
});


$('input:checkbox.checkitem').change(function(){
    var item = $(this);
    var classname = item.attr('class');
    var id = classname.split('checkbox').pop();

    
    if(false == $(this).prop('checked')){ 
        $('#'+id).prop('checked', false); 
    }
    
    selectcheck(id); 
});

    $('input[type=radio][name=type]').change(function() {
        $('.menupermission').addClass('hide');
    $('.checkitem').prop('checked', false);
    $('.selectall').prop('checked', false);
    $('.users-listview li').removeClass('active');
        var type = $(this).val();
        $.ajax({
            type: 'POST',
            url: '" . $url . "',
            data:{type:type},
            success:function(data){
                $('#userslist').html(data);
            },
            error: function(data) { // if error occured
                  alert('Error occured.please try again');                 
            },


        });

      
    });



    $('input[name^=\"level\"]').click(function () {
        $(this).parent().find('input[name^=level]').prop('checked', this.checked);

        if (!this.checked) {
            var level = this.name.substring(this.name.length - 1);
            for (var i = level - 1; i > 0; i--) {
                //$('input[name=\"level-' + i + '\"]').prop('checked', false);
            }
        }
    });
    
    $('#savebtn').click(function(){
    
        var type = $('input[name=type]:checked').val();
        var userids = [];
        var groupids = [];
        var profileids = [];
        var menus = [];
        if(type == 'individual'){

//            $('.userid:checked').each(function() {
//                userids.push(this.value);
//            });
            
            var userids = $('li.userid.active').attr('data-id');
            
            
            $('.checkitem:checked').each(function() {
                menus.push(this.value);
            });
            
            //if((userids.length !=0)){
                $.ajax({
                    type: 'POST',
                    url: '" . $saveurl . "',
                    data:{type: type, userids:userids, menus: menus},
                    success:function(data){
                        alert('success');
                        //location.reload();
                    },
                    error: function(data) { // if error occured
                          alert('Error occured.please try again');

                    },


                });


            //}

        }
        else if(type == 'department'){
            
//            $('.departmentid:checked').each(function() {
//                departmentids.push(this.value);
//            });
            var departmentids = $('li.departmentid.active').attr('data-id');
            
            $('.checkitem:checked').each(function() {
                menus.push(this.value);
            });
            

            //if((departmentids.length !=0)){
                $.ajax({
                    type: 'POST',
                    url: '" . $saveurl . "',
                    data:{type: type, departmentids:departmentids, menus: menus},
                    success:function(data){
                        alert('success');
						location.reload();

                    },
                    error: function(data) { // if error occured
                          alert('Error occured.please try again');

                    },
                });
            //}
        }
        else if(type == 'profile'){
//            $('.profileid:checked').each(function() {
//                profileids.push(this.value);
//            });
            var profileids = $('li.profileid.active').attr('data-id');
            
            $('.checkitem:checked').each(function() {
                menus.push(this.value);
            });
            

            //if((profileids.length !=0)){
                $.ajax({
                    type: 'POST',
                    url: '" . $saveurl . "',
                    data:{type: type, profileids:profileids, menus: menus},
                    success:function(data){
                        alert('success');
						location.reload();

                    },
                    error: function(data) { // if error occured
                          alert('Error occured.please try again');

                    },
                });
            //}
        }
        else{
        }

    });

    $(document).on('click', '.userid',function(){
        $('.menupermission').removeClass('hide');
            $('.checkitem').prop('checked', false);
            $('.selectall').prop('checked', false);
            $('.users-listview li').removeClass('active');
            //if(($('input.userid:checked').length) == 1){
                var id = $(this).attr('data-id');
                $(this).closest('li').addClass('active');
                var type = 'individual';
                $.ajax({
                    type: 'POST',
                    url: '" . $geturl . "',
                    data:{type: type, id: id},
                    dataType: 'json',
                    success:function(response){  
                        for (var i=0; i<response.data.length; i++) {
                            var val = response.data[i].menu_id;
                            $('.checkitem[value=\"' + val + '\"]').prop('checked', true); 
                            var parent = response.data[i].parent_id;
                            selectcheck(parent);
                            
                            
                       }
                    },
                    error: function(data) { // if error occured
                          alert('Error occured.please try again');

                    },

                });
            
            //}
        
    });
    
    $(document).on('click', '.departmentid',function(){
        $('.menupermission').removeClass('hide');
  
        $('.checkitem').prop('checked', false);
        $('.selectall').prop('checked', false);
        $('.users-listview li').removeClass('active');
        //if(($('input.departmentid:checked').length) == 1){
            var id = $(this).attr('data-id');
            $(this).closest('li').addClass('active');
            var type = 'department';
            $.ajax({
                type: 'POST',
                url: '" . $geturl . "',
                data:{type: type, id: id},
                dataType: 'json',
                success:function(response){ 
                    console.log(response);
                    for (var i=0; i<response.data.length; i++) {
                        var val = response.data[i];
                        $('.checkitem[value=\"' + val + '\"]').prop('checked', true); 
                        var parent = response.parents[i];
                        selectcheck(parent);
                    }
                },
                error: function(data) { // if error occured
                      alert('Error occured.please try again');

                },

            });
        //}
        
    });
    

    $(document).on('click', '.profileid',function(){
        $('.menupermission').removeClass('hide');
        $('.checkitem').prop('checked', false);
        $('.selectall').prop('checked', false);
        $('.users-listview li').removeClass('active');
        //if(($('input.profileid:checked').length) == 1){
            var id = $(this).attr('data-id');
            $(this).closest('li').addClass('active');
            var type = 'profile';
            $.ajax({
                type: 'POST',
                url: '" . $geturl . "',
                data:{type: type, id: id},
                dataType: 'json',
                success:function(response){ 
                    console.log(response);
                    for (var i=0; i<response.data.length; i++) {
                        var val = response.data[i];
                        $('.checkitem[value=\"' + val + '\"]').prop('checked', true); 
                        var parent = response.parents[i];
                        selectcheck(parent);
                        
                    }
                },
                error: function(data) { // if error occured
                      alert('Error occured.please try again');

                },

            });
        //}
        
    });
    
    function selectcheck(val){
    
        var childs = $('input:checkbox.checkitem.checkbox'+val).length;
        var checkedones = $('input:checkbox.checkitem.checkbox'+val+':checked').length;


        if (childs == checkedones ){
            $('#'+val).prop('checked', true);
        }
    }


");
Yii::app()->clientScript->registerCss('mycss', '
    .gridviewlist{
        /*column-count:1;
        column-gap:10px;
        -moz-column-count:1;
        -moz-column-gap:10px;
        -webkit-column-count:1;
        -webkit-column-gap:10px;*/
        
    }

    /*.gridviewlist div{
        display: inline-block; 
        float: left;
        margin: 5px;
        padding: 5px;
//        width:21%;

    }*/
    /* Non-grid specific CSS */  
body {

	text-rendering: optimizeLegibility;
	-webkit-font-smoothing: antialiased;
}

');
?>


<div class="row" style="border: 1px solid #dedede;">
    <div class="col-md-3" style="border-right: 1px solid #dfdfdf;border-bottom: 1px solid #dfdfdf;">
        <div class="menu-title">
            <h4>Users List</h4>
        </div>
        <div class="typeselect nav nav-tabs">
            <label><input type="radio" name="type" value="individual" checked="checked">Individual</label>&nbsp;&nbsp;<label><input type="radio" name="type" value="profile">Profile</label></li>
        </div>
        <div id="userslist">
            <?php
            echo "<ul class='users-listview'>";
            foreach ($users as $user) {
                echo "<li class='userid' name='userid' data-id=" . $user['userid'] . "><a >" . $user['first_name'] . " " . $user['last_name'] . "</a></li>";
            }

            echo "</ul>";
            ?>
        </div>
    </div>
    <div class="gridviewlist menupermission col-md-9">
        <div class="clearfix">
            <div class="pull-right">
                <button id="savebtn" class="btn btn-info">Save</button>
                <button id="clearbtn" class="btn btn-default">Clear</button>
            </div>
            <h4>Menu Items</h4>
        </div>
        <div class="grid">
            <?php
            $inputlevel1 = array();
            $inputlevel2 = array();
            $mainmenu = array();
            $menus = $dataProvider->getData();
            $total = $dataProvider->getTotalItemCount();


            $controllerArr = array();
            foreach ($menus as $k => $menu) {
                if ($menu->parent_id > 0) {
                    if (!in_array($menu->parent_id, $controllerArr)) {
                        array_push($controllerArr, $menu->parent_id);
                    }
                }
            }


            foreach ($controllerArr as $contr) {
                $c = 0;
                echo  '<div class="grid-view item"  style="border:1px solid #ccc;"> 
                            <table  class="items greytable content">';

                foreach ($menus as $k => $menu) {

                    if ($menu->parent_id == $contr) {
                        if ($c == 0) {
            ?>
                            <thead class="items">
                                <tr class="thead_tr">
                                    <?php if ($menu->parent['menu_name'] == 'Report') {


                                    ?>


                                        <?php
                                    } elseif ($menu->parent['menu_name'] == 'Buyer') {
                                        if ($settings_model['buyer_module'] == 1) {
                                        ?>
                                            <th>
                                                <input type="checkbox" id="<?php echo $menu->parent_id ?>" class="selectall" name="level-1" value="<?php echo $menu->menu_id; ?>" />
                                                <b style="font-size: 16px; "><?php echo $menu->parent['menu_name'];  //$menu->controller; 
                                                                                ?></b>
                                            </th>
                                        <?php
                                        }
                                    } else {
                                        ?>
                                        <th>
                                            <input type="checkbox" id="<?php echo $menu->parent_id ?>" class="selectall" name="level-1" value="<?php echo $menu->menu_id; ?>" />
                                            <b style="font-size: 16px; "><?php echo $menu->parent['menu_name'];  //$menu->controller; 
                                                                            ?></b>
                                        </th>
                                    <?php
                                    } ?>

                                </tr>
                            </thead>
                            <tbody>
                            <?php
                        }
                        $c++;
                            ?>
                            <?php if ($menu->parent['menu_name'] == 'Report') {

                            ?>

                                <?php

                            } elseif ($menu->parent['menu_name'] == 'Buyer') {
                                if ($settings_model['buyer_module'] == 1) {
                                ?>
                                    <tr>
                                        <td><input type="checkbox" class="checkitem checkbox<?php echo $menu->parent_id ?>" name="level-2" value="<?php echo $menu->menu_id ?>" /> <?php echo $menu->menu_name; ?></td>

                                    </tr>
                                <?php
                                }
                            } else {
                                ?>
                                <tr>
                                    <td><input type="checkbox" class="checkitem checkbox<?php echo $menu->parent_id ?>" name="level-2" value="<?php echo $menu->menu_id ?>" /> <?php echo $menu->menu_name; ?></td>

                                </tr>
                            <?php
                            }

                            ?>

                <?php
                    }
                }
                echo ' </tbody>
                        </table>
                    </div>';
            }
                ?>
        </div>
        <div class="clearfix"> &nbsp;
        </div>
    </div>

</div>

<style class="cp-pen-styles">
    .grid {
        display: grid;
        grid-gap: 10px;
        grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
        grid-auto-rows: 20px;
    }

    .menupermission .grid-view {
        padding-top: 0px;
        position: relative;
    }

    .menupermission table.items {
        padding-top: 0px;
        background: #fff;
        margin-bottom: 10px;
    }

    .menupermission .greytable,
    .greytable th,
    .greytable td {
        border: 1px solid transparent;
    }

    .menupermission .items {
        background: #fafafa;
        /*  margin: 0 auto; */
    }

    .menupermission .greytable th {
        text-align: left !important
    }

    .menupermission button {
        margin-top: 8px;
    }

    .menupermission td {
        width: 33.33%;
    }

    .users-listview li a {
        cursor: pointer;
    }

    .users-listview {
        padding: 0 !important;
    }

    .users-listview li {
        padding: 5px;
        cursor: pointer;
        list-style: none;
        border-radius: 3px;
    }

    .users-listview li:hover,
    .users-listview li.active {
        background: #6a8ec7;
    }

    users-listview li:hover,
    .users-listview li.active {
        background: #6a8ec7;
    }

    .users-listview li:hover a,
    .users-listview li.active a {
        color: #fff;
    }

    .users-listview li a {
        text-decoration: none;
        color: #555;
    }

    .typeselect label {
        display: inline;
        /*  padding-left: 15px; */
        text-indent: -15px;
        font-weight: normal;
        cursor: pointer;
    }

    .typeselect input {
        margin-right: 5px;
    }

    .typeselect {
        padding-bottom: 10px;
        margin-bottom: 5px;
    }

    .greytable td,
    th {
        padding: 5px 6px;
    }

    .greytable thead tr:first-child {
        background: #eee;
        color: #555;
    }

    @media (min-width: 1360px) {
        .items {
            max-width: 1300px;
        }
    }

    @media (min-width: 767px) {
        .items {
            padding-top: 25px;
        }
    }
</style>


<script>
    "use strict";
    "object" != typeof window.CP && (window.CP = {}), window.CP.PenTimer = {
        programNoLongerBeingMonitored: !1,
        timeOfFirstCallToShouldStopLoop: 0,
        _loopExits: {},
        _loopTimers: {},
        START_MONITORING_AFTER: 2e3,
        STOP_ALL_MONITORING_TIMEOUT: 5e3,
        MAX_TIME_IN_LOOP_WO_EXIT: 2200,
        exitedLoop: function(o) {
            this._loopExits[o] = !0
        },
        shouldStopLoop: function(o) {
            if (this.programKilledSoStopMonitoring) return !0;
            if (this.programNoLongerBeingMonitored) return !1;
            if (this._loopExits[o]) return !1;
            var t = this._getTime();
            if (0 === this.timeOfFirstCallToShouldStopLoop) return this.timeOfFirstCallToShouldStopLoop = t, !1;
            var i = t - this.timeOfFirstCallToShouldStopLoop;
            if (i < this.START_MONITORING_AFTER) return !1;
            if (i > this.STOP_ALL_MONITORING_TIMEOUT) return this.programNoLongerBeingMonitored = !0, !1;
            try {
                this._checkOnInfiniteLoop(o, t)
            } catch (o) {
                return this._sendErrorMessageToEditor(), this.programKilledSoStopMonitoring = !0, !0
            }
            return !1
        },
        _sendErrorMessageToEditor: function() {
            try {
                if (this._shouldPostMessage()) {
                    var o = {
                        action: "infinite-loop",
                        line: this._findAroundLineNumber()
                    };
                    parent.postMessage(JSON.stringify(o), "*")
                } else this._throwAnErrorToStopPen()
            } catch (o) {
                this._throwAnErrorToStopPen()
            }
        },
        _shouldPostMessage: function() {
            return document.location.href.match(/boomerang/)
        },
        _throwAnErrorToStopPen: function() {
            throw "We found an infinite loop in your Pen. We've stopped the Pen from running. Please correct it or contact support@codepen.io."
        },
        _findAroundLineNumber: function() {
            var o = new Error,
                t = 0;
            if (o.stack) {
                var i = o.stack.match(/boomerang\S+:(\d+):\d+/);
                i && (t = i[1])
            }
            return t
        },
        _checkOnInfiniteLoop: function(o, t) {
            if (!this._loopTimers[o]) return this._loopTimers[o] = t, !1;
            var i = t - this._loopTimers[o];
            if (i > this.MAX_TIME_IN_LOOP_WO_EXIT) throw "Infinite Loop found on loop: " + o
        },
        _getTime: function() {
            return +new Date
        }
    }, window.CP.shouldStopExecution = function(o) {
        var t = window.CP.PenTimer.shouldStopLoop(o);
        return t === !0 && console.warn("[CodePen]: An infinite loop (or a loop taking too long) was detected, so we stopped its execution. Sorry!"), t
    }, window.CP.exitedLoop = function(o) {
        window.CP.PenTimer.exitedLoop(o)
    };
</script>
<script>
    function resizeGridItem(item) {
        grid = document.getElementsByClassName("grid")[0];
        rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows'));
        rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap'));
        rowSpan = Math.ceil((item.querySelector('.content').getBoundingClientRect().height + rowGap) / (rowHeight + rowGap));
        item.style.gridRowEnd = "span " + rowSpan;
    }

    function resizeAllGridItems() {
        allItems = document.getElementsByClassName("item");
        for (x = 0; x < allItems.length; x++) {
            if (window.CP.shouldStopExecution(1)) {
                break;
            }
            resizeGridItem(allItems[x]);
        }
        window.CP.exitedLoop(1);

    }

    function resizeInstance(instance) {
        item = instance.elements[0];
        resizeGridItem(item);
    }

    window.onload = resizeAllGridItems();
    window.addEventListener("resize", resizeAllGridItems);

    allItems = document.getElementsByClassName("item");
    for (x = 0; x < allItems.length; x++) {
        if (window.CP.shouldStopExecution(2)) {
            break;
        }
    }
    window.CP.exitedLoop(2);

    //# sourceURL=test.js

    $(document).ready(function() {
        $('.menupermission').addClass('hide');
        $('.grid-view').each(function() {
            var row = $(this).find('th');

            if (row.length == 0) {
                $(this).hide();
            }
            // console.log($(this).closest("table > thead > tr:first > td").length);
            // var a = $(this).closest('tr').find('input:checkbox').val();
            // console.log(a + "abb");
            // if (a === '') {
            //     console.log('ddd');
            //     // $(this).hide();
            // }
        });

    });
</script>