<?php
/* @var $this ProjectsController */
/* @var $data Projects */
?>



<?php
if ($index == 0) {
    ?>
    <thead class="entry-table first-thead-row">
    <tr> 
        <th>SI No</th>
        <th>Name</th>
        <th>Expense Type</th>
        <?php 
        if(isset(Yii::app()->user->role) && (in_array('/expensetype/updateexpensetype', Yii::app()->user->menuauthlist))){
        ?>
        <th>Action</th>
        <?php } ?>
    </tr>   
    </thead>
<?php } ?>
    <tr> 
        
        <td style="width: 50px;"><?php echo $index+1; ?></td>
        <td>
            <span><?php echo $data->type_name ?></span>            
        </td>        
        <td>
        <span><?php echo CHtml::encode(($data->expense_type ==0)?'':$data->status->caption); ?></span>
        </td>
        <td style="width: 50px;">
        <?php 
            if(isset(Yii::app()->user->role) && (in_array('/expensetype/updateexpensetype', Yii::app()->user->menuauthlist))){
            ?>        
           <a class="fa fa-edit editProject" data-toggle="modal" data-target=".edit" data-id="<?php echo $data->type_id; ?>"></a>
         <?php } 
         if(isset(Yii::app()->user->role) && (in_array('/expensetype/delete', Yii::app()->user->menuauthlist))){
            ?>
            <a class="fa fa-trash deleteProject" data-toggle="modal" data-target=".delete" data-id="<?php echo $data->type_id; ?>"></a>
         <?php } ?>
        </td>        
    </tr>  
