<?php
/* @var $this ExpensesController */
/* @var $model Expenses */
/* @var $form CActiveForm */
?>

<div class="page_filter clearfix">

  <?php $form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
  )); ?>
  <div class="row">

    <div class="form-group col-xs-12 col-sm-3 col-md-2">
      <label for="from">From</label>
      <input type="text" name="Expenses[fromdate]" id="Expenses_fromdate" autocomplete="off" readonly=true
        class="form-control" placeholder="From Date"
        value="<?php echo ($model->fromdate) ? date("d-m-Y", strtotime($model->fromdate)) : ""; ?>" />
    </div>
    <div class="form-group col-xs-12 col-sm-3 col-md-2">
      <label for="to">To</label>
      <input type="text" name="Expenses[todate]" id="Expenses_todate" autocomplete="off" readonly=true
        class="form-control" placeholder="To Date"
        value="<?php echo ($model->todate) ? date("Y-m-d", strtotime($model->todate)) : ""; ?>" />
    </div>
    <div class="form-group col-xs-12 col-sm-3 col-md-2 text-sm-left text-right">
      <label class="d-sm-block d-none">&nbsp;</label>
      <div>
        <button type="submit" class="btn btn-sm btn-primary" name="submitForm" id="submitForm">Go</button>
        <button type="button" class="btn btn-sm btn-default" name="clearForm" id="clearForm">Clear</button>
      </div>
    </div>
  </div>
  <?php $this->endWidget(); ?>

</div><!-- search-form -->