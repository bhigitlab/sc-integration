<?php

class CashbalanceController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        /* return array(
          array('allow',  // allow all users to perform 'index' and 'view' actions
          'actions'=>array('index','view','newlist'),
          'users'=>array('@'),
          ),
          array('allow', // allow authenticated user to perform 'create' and 'update' actions
          'actions'=>array('create','update','cashbalance_report','cashblancereportpdf', 'cashblancereportexcel','savetopdf','savetoexcel','dailyfinancialreport','dailyfinancialdetailedpdf','dailyfinancialdetailedexcel','dailyfinancialsummary','dailyfinancialsummarypdf','dailyfinancialsummaryexcel'),
          'users'=>array('@'),
          ),
          array('allow', // allow admin user to perform 'admin' and 'delete' actions
          'actions'=>array('admin','delete'),
          'users'=>array('@'),
          ),
          array('deny',  // deny all users
          'users'=>array('*'),
          ),
          ); */
        $accessArr = array();
        $accessauthArr = array();
        $controller = Yii::app()->controller->id;
        if (isset(Yii::app()->user->menuall)) {
            if (array_key_exists($controller, Yii::app()->user->menuall)) {
                $accessArr = Yii::app()->user->menuall[$controller];
            }
        }

        if (isset(Yii::app()->user->menuauth)) {
            if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                $accessauthArr = Yii::app()->user->menuauth[$controller];
            }
        }
        $access_privlg = count($accessauthArr);
        return array(
            array(
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => $accessArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionNewlist()
    {

        $model = new Cashbalance('search');
        $model->unsetAttributes();  // clear any default values

        if (isset($_GET['Cashbalance']))
            $model->attributes = $_GET['Cashbalance'];

        if (Yii::app()->user->role == 1) {

            $this->render('newlist', array(
                'model' => $model,
                'dataProvider' => $model->search(),
            ));
        } else {
            $this->render('newlist', array(
                'model' => $model,
                'dataProvider' => $model->search(Yii::app()->user->id),
            ));
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Cashbalance;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Cashbalance'])) {
            $model->attributes = $_POST['Cashbalance'];
            if ($_POST['Cashbalance']['cashbalance_type'] == 100) {
                $model->bank_id = NULL;
            }
            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->company_id = $_POST['Cashbalance']['company_id'];
            if ($model->save())
                $this->redirect(array('newList'));
        }
        if (isset($_GET['layout']))
            $this->layout = false;
        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Cashbalance'])) {
            $model->attributes = $_POST['Cashbalance'];
            $model->company_id = $_POST['Cashbalance']['company_id'];
            if ($_POST['Cashbalance']['cashbalance_type'] == 100) {
                $model->bank_id = NULL;
            }
            if ($model->save())
                //$this->redirect(array('newList'));
                if (Yii::app()->user->returnUrl != 0) {
                    $this->redirect(array('newlist', 'Cashbalance_page' => Yii::app()->user->returnUrl));
                } else {
                    $this->redirect(array('newlist'));
                }
        }
        if (isset($_GET['layout']))
            $this->layout = false;
        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Cashbalance');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Cashbalance('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Cashbalance']))
            $model->attributes = $_GET['Cashbalance'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionCashbalance_report()
    {
        if (isset($_REQUEST['date_from'])) {
            $date_from = $_REQUEST['date_from'];
        } else {
            $date_from = '';
        }
        if (isset($_REQUEST['date_to'])) {
            $date_to = $_REQUEST['date_to'];
        } else {
            $date_to = '';
        }

        if (filter_input(INPUT_GET, 'export') == 'pdf') {
            $this->logo = $this->realpath_logo;
            $mPDF1 = Yii::app()->ePdf->mPDF();
            $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
            $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
            $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
            
            $template = $this->getTemplate($selectedtemplate);
            $mPDF1->SetHTMLHeader($template['header']);
            $mPDF1->SetHTMLFooter($template['footer']);
            $mPDF1->AddPage('','', '', '', '', 0,0,40, 40, 10,0);
            $mPDF1->WriteHTML($this->renderPartial('cashbalance_report', array('date_to' => $date_to, 'date_from' => $date_from), true));
            if ($_GET["ledger"] == 0) {
                $mPDF1->Output('Cash balance report.pdf', 'D');
            } else {
                $mPDF1->Output('Cash balance report - Ledger view.pdf', 'D');                
            }
        } else if(filter_input(INPUT_GET, 'export') == 'csv') {
            $exported_data = $this->renderPartial('cashbalance_report', array('date_to' => $date_to, 'date_from' => $date_from), true);
            $export_filename = 'cash-balance-report' . date('Ymd_His');
            ExportHelper::exportGridAsCSV($exported_data, $export_filename);
        }else{
            $this->render('cashbalance_report', array('date_to' => $date_to, 'date_from' => $date_from));
        }
        
    }

    public function actionCashblancereportpdf($date_to, $date_from)
    {
        $this->logo = $this->realpath_logo;
        $tblpx = Yii::app()->db->tablePrefix;
        $newQuery = "";
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}cashbalance.company_id)";
        }
        $cash_blance = Yii::app()->db->createCommand("SELECT {$tblpx}status.caption,{$tblpx}cashbalance.cashbalance_type,{$tblpx}cashbalance.bank_id,{$tblpx}bank.bank_name,{$tblpx}cashbalance.cashbalance_opening_balance,{$tblpx}cashbalance.cashbalance_deposit,{$tblpx}cashbalance.cashbalance_withdrawal,{$tblpx}cashbalance.cashbalance_closing_balance,{$tblpx}cashbalance.cashbalance_date,{$tblpx}cashbalance.company_id FROM {$tblpx}cashbalance LEFT JOIN {$tblpx}bank ON {$tblpx}cashbalance.bank_id={$tblpx}bank.bank_id LEFT JOIN {$tblpx}status ON {$tblpx}cashbalance.cashbalance_type={$tblpx}status.sid WHERE (" . $newQuery . ") ORDER BY bank_id ASC")->queryAll();
        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
        $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $mPDF1->AddPage('','', '', '', '', 0,0,40, 30, 10,0);
        $mPDF1->WriteHTML($this->renderPartial('cashblancereportpdf', array(
            'cash_blance' => $cash_blance,
            'date_to' => $date_to,
            'date_from' => $date_from
        ), true));
        $filename = "Cash blance report";
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    public function actionCashblancereportexcel($date_to, $date_from)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $arraylabel = array('sl no', 'Company', 'Cash Balance Type', 'Bank', 'Date', 'Opening Balance', 'Deposit', 'Withdrawal', 'Closing Balance');
        $finaldata = array();
        $newQuery = "";
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}cashbalance.company_id)";
        }
        $cash_blance = Yii::app()->db->createCommand("SELECT {$tblpx}status.caption,{$tblpx}cashbalance.cashbalance_type,{$tblpx}cashbalance.bank_id,{$tblpx}bank.bank_name,{$tblpx}cashbalance.cashbalance_opening_balance,{$tblpx}cashbalance.cashbalance_deposit,{$tblpx}cashbalance.cashbalance_withdrawal,{$tblpx}cashbalance.cashbalance_closing_balance,{$tblpx}cashbalance.cashbalance_date,{$tblpx}cashbalance.company_id FROM {$tblpx}cashbalance LEFT JOIN {$tblpx}bank ON {$tblpx}cashbalance.bank_id={$tblpx}bank.bank_id LEFT JOIN {$tblpx}status ON {$tblpx}cashbalance.cashbalance_type={$tblpx}status.sid WHERE (" . $newQuery . ") ORDER BY bank_id ASC")->queryAll();
        foreach ($cash_blance as $key => $value) {
            $current_date = date("Y-m-d");
            $opening_date = date('Y-m-d', strtotime('-1 day', strtotime($current_date)));
            if ($value['bank_id'] != '') {
                $opening_bank = " AND reconciliation_date <= '" . $opening_date . "'";
                // deposit

                $daybook = Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=88 AND bank_id=" . $value['bank_id'] . " AND type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND dailyexpense_receipt_type=88 AND bank_id=" . $value['bank_id'] . " AND exp_type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_check = $daybook['total_amount'];
                $dailyexpense_check = $dailyexpense['total_amount'];
                //$deposit = $daybook_check+$dailyexpense_check+$value['cashbalance_deposit'];
                $deposit = $daybook_check + $dailyexpense_check;


                //withdrawal
                $daybook_with = Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND expense_type=88 AND bank_id=" . $value['bank_id'] . " AND type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense_with = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND expense_type=88 AND bank_id=" . $value['bank_id'] . " AND exp_type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_with_amount = $daybook_with['total_amount'];
                $dailyexpense_with_amount = $dailyexpense_with['total_amount'];

                $dailyvendors = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=88  AND bank=" . $value['bank_id'] . " AND company_id=" . $value['company_id'] . "")->queryRow();
                $subcontractor = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=88  AND bank=" . $value['bank_id'] . " AND company_id=" . $value['company_id'] . "")->queryRow();

                $dailyvendors_check = $dailyvendors['total_amount'];
                $subcontractor_check = $subcontractor['total_amount'];

                //$withdrawal = $dailyvendors_check+$subcontractor_check+$daybook_with_amount+$dailyexpense_with_amount+$value['cashbalance_withdrawal'];
                //$withdrawal = $dailyvendors_check+$subcontractor_check+$daybook_with_amount+$dailyexpense_with_amount;
                $withdrawal = $dailyvendors_check + $daybook_with_amount + $dailyexpense_with_amount;

                // opening balance calculation

                // deposit
                $daybook_opening =  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 " . $opening_bank . " AND payment_type=88 AND bank_id=" . $value['bank_id'] . " AND type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense_opening =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 " . $opening_bank . " AND dailyexpense_receipt_type=88 AND bank_id=" . $value['bank_id'] . " AND exp_type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_check_opening = $daybook_opening['total_amount'];
                $dailyexpense_check_opening = $dailyexpense_opening['total_amount'];
                $deposit_opening = $daybook_check_opening + $dailyexpense_check_opening;

                //                if($deposit_opening ==0){
                //                     $deposit_opening = $value['cashbalance_opening_balance'];
                //                 }

                //withdrawal
                $daybook_with_opening =  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 " . $opening_bank . " AND expense_type=88 AND bank_id=" . $value['bank_id'] . " AND type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense_with_opening =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 " . $opening_bank . " AND expense_type=88 AND bank_id=" . $value['bank_id'] . " AND exp_type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_with_amount_opening = $daybook_with_opening['total_amount'];
                $dailyexpense_with_amount_opening = $dailyexpense_with_opening['total_amount'];

                $dailyvendors_opening =  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE reconciliation_status = 1 " . $opening_bank . " AND payment_type=88  AND bank=" . $value['bank_id'] . " AND company_id=" . $value['company_id'] . "")->queryRow();

                $dailyvendors_check_opening = $dailyvendors_opening['total_amount'];

                $withdrawal_opening = $dailyvendors_check_opening + $daybook_with_amount_opening + $dailyexpense_with_amount_opening;

                $opening_balance = ($deposit_opening + $value['cashbalance_opening_balance']) - $withdrawal_opening;
            } else {
                $opening_hand = " AND date <= '" . $opening_date . "'";
                //deposit
                $daybook_cah = Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=89 AND type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_cash = $daybook_cah['total_amount'];
                //$dailyexpense_cah = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND (dailyexpense_receipt_type=89 OR dailyexpense_receipt_type=103) AND exp_type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense_cah = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND (dailyexpense_receipt_type=89) AND exp_type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense_cash = $dailyexpense_cah['total_amount'];

                //$deposit = $daybook_cash+ $dailyexpense_cash+$value['cashbalance_deposit'];
                $deposit = $daybook_cash + $dailyexpense_cash;
                // withdrawal

                $daybook_with = Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND (expense_type=89 OR expense_type=103) AND type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_with_amount = $daybook_with['total_amount'];
                //$dailyexpense_with = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND (expense_type=89 OR expense_type=103) AND exp_type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense_with = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND (expense_type=89) AND exp_type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense_with_amount = $dailyexpense_with['total_amount'];


                $dailyvendors_cah = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=89 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyvendors_cash = $dailyvendors_cah['total_amount'];
                $subcontractor_cah = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=89 AND company_id=" . $value['company_id'] . "")->queryRow();
                $subcontractor_cash = $subcontractor_cah['total_amount'];

                //$withdrawal = $dailyvendors_cash+$subcontractor_cash+$daybook_with_amount+$dailyexpense_with_amount+$value['cashbalance_withdrawal'];
                //$withdrawal = $dailyvendors_cash+$subcontractor_cash+$daybook_with_amount+$dailyexpense_with_amount;
                $withdrawal = $dailyvendors_cash + $daybook_with_amount + $dailyexpense_with_amount;

                // opening balance calculation

                // deposit
                $daybook_cah_opening =  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE payment_type=89 AND type=72 " . $opening_hand . " AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_cash_opening = $daybook_cah_opening['total_amount'];
                //$dailyexpense_cah_opening=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE (dailyexpense_receipt_type=89 OR dailyexpense_receipt_type=103) AND exp_type=72 ".$opening_hand." AND company_id=".$value['company_id']."")->queryRow();
                $dailyexpense_cah_opening =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE (dailyexpense_receipt_type=89) AND exp_type=72 " . $opening_hand . " AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense_cash_opening = $dailyexpense_cah_opening['total_amount'];
                $deposit_opening = $daybook_cash_opening + $dailyexpense_cash_opening;

                //                if($deposit_opening ==0){
                //                    $deposit_opening = $value['cashbalance_opening_balance'];
                //                }

                // withdrawal

                $daybook_with_opening =  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE (expense_type=89 OR expense_type=103) AND type=73 " . $opening_hand . " AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_with_amount_opening = $daybook_with_opening['total_amount'];
                //$dailyexpense_with_opening=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE (expense_type=89 OR expense_type=103) AND exp_type=73 ".$opening_hand." AND company_id=".$value['company_id']."")->queryRow();
                $dailyexpense_with_opening =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE (expense_type=89) AND exp_type=73 " . $opening_hand . " AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense_with_amount_opening = $dailyexpense_with_opening['total_amount'];


                $dailyvendors_cah_opening =  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE payment_type=89 " . $opening_hand . " AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyvendors_cash_opening = $dailyvendors_cah_opening['total_amount'];
                $withdrawal_opening = $dailyvendors_cash_opening + $daybook_with_amount_opening + $dailyexpense_with_amount_opening;

                $opening_balance = ($deposit_opening + $value['cashbalance_opening_balance']) - $withdrawal_opening;
            }
            $company = Company::model()->findByPk($value['company_id']);
            $finaldata[$key][] = $key + 1;
            $finaldata[$key][] = $company['name'];
            $finaldata[$key][] = $value['caption'];
            $finaldata[$key][] = $value['bank_name'];
            $finaldata[$key][] = $value['cashbalance_date'];
            $finaldata[$key][] = Controller::money_format_inr($opening_balance, 2);
            $finaldata[$key][] = Controller::money_format_inr($deposit, 2);
            $finaldata[$key][] = Controller::money_format_inr($withdrawal, 2);
            $finaldata[$key][] = number_format(($opening_balance + $deposit) - $withdrawal, 2);
        }
        $finaldata[count($cash_blance)][] = '';
        if ($date_from != '' || $date_to != '') {

            $heading = '';
            if (!empty($date_from) && !empty($date_to)) {
                $heading .= date('d-M-Y', strtotime($date_from)) . ' to ' . date('d-M-Y', strtotime($date_to));
            } else {
                if (!empty($date_from)) {
                    $heading .= date('d-M-Y', strtotime($date_from));
                }
                if (!empty($date_to)) {
                    $heading .= date('d-M-Y', strtotime($date_to));
                }
            }

            $finaldata[count($cash_blance) + 1][] = 'Cashbalance Report :' . $heading;
            $k = count($cash_blance) + 2;
            $cash_blance = Yii::app()->db->createCommand("SELECT {$tblpx}status.caption,{$tblpx}cashbalance.cashbalance_type,{$tblpx}cashbalance.bank_id,{$tblpx}bank.bank_name,{$tblpx}cashbalance.cashbalance_opening_balance,{$tblpx}cashbalance.cashbalance_deposit,{$tblpx}cashbalance.cashbalance_withdrawal,{$tblpx}cashbalance.cashbalance_closing_balance,{$tblpx}cashbalance.cashbalance_date FROM {$tblpx}cashbalance LEFT JOIN {$tblpx}bank ON {$tblpx}cashbalance.bank_id={$tblpx}bank.bank_id LEFT JOIN {$tblpx}status ON {$tblpx}cashbalance.cashbalance_type={$tblpx}status.sid WHERE {$tblpx}cashbalance.company_id=" . Yii::app()->user->company_id . " ORDER BY bank_id ASC")->queryAll();
            if (!empty($cash_blance)) {
                foreach ($cash_blance as $key => $value) {

                    $tblpx = Yii::app()->db->tablePrefix;
                    $where_bank = '';
                    $where_hand = '';
                    $where_main = '';
                    if (!empty($date_from) && !empty($date_to)) {
                        $where_main .= " AND {$tblpx}cashbalance.cashbalance_date between '" . $date_from . "' and'" . $date_to . "'";
                        $where_bank .= " AND reconciliation_date between '" . $date_from . "' and'" . $date_to . "'";
                        $where_hand .= " AND date between '" . $date_from . "' and'" . $date_to . "'";
                    } else {
                        if (!empty($_POST['date_from'])) {
                            $date_from = date('Y-m-d', strtotime($_POST['date_from']));
                            $where_main .= " AND {$tblpx}cashbalance.cashbalance_date >= '" . $date_from . "'";
                            $where_bank .= " AND reconciliation_date between '" . $date_from . "' and'" . $current_date . "'";
                            $where_hand .= " AND date between '" . $date_from . "' and'" . $current_date . "'";
                        }
                        if (!empty($_POST['date_to'])) {
                            $date_to = date('Y-m-d', strtotime($_POST['date_to']));
                            $where_main .= " AND {$tblpx}cashbalance.cashbalance_date <= '" . $date_to . "'";
                            $where_bank .= " AND reconciliation_date between '" . $value['cashbalance_date'] . "' and'" . $date_to . "'";
                            $where_hand .= " AND date between '" . $value['cashbalance_date'] . "' and'" . $date_to . "'";
                        }
                    }

                    if ($value['bank_id'] != '') {
                        // deposit

                        $daybook = Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 " . $where_bank . " AND payment_type=88 AND bank_id=" . $value['bank_id'] . " AND type=72 AND company_id=" . Yii::app()->user->company_id . "")->queryRow();
                        $dailyexpense = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 " . $where_bank . " AND dailyexpense_receipt_type=88 AND bank_id=" . $value['bank_id'] . " AND exp_type=72 AND company_id=" . Yii::app()->user->company_id . "")->queryRow();
                        $daybook_check = $daybook['total_amount'];
                        $dailyexpense_check = $dailyexpense['total_amount'];
                        //$deposit = $daybook_check+$dailyexpense_check+$value['cashbalance_deposit'];
                        $deposit = $daybook_check + $dailyexpense_check;


                        //withdrawal
                        $daybook_with = Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 " . $where_bank . " AND expense_type=88 AND bank_id=" . $value['bank_id'] . " AND type=73 AND company_id=" . Yii::app()->user->company_id . "")->queryRow();
                        $dailyexpense_with = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 " . $where_bank . " AND expense_type=88 AND bank_id=" . $value['bank_id'] . " AND exp_type=73 AND company_id=" . Yii::app()->user->company_id . "")->queryRow();
                        $daybook_with_amount = $daybook_with['total_amount'];
                        $dailyexpense_with_amount = $dailyexpense_with['total_amount'];

                        $dailyvendors = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE reconciliation_status = 1 " . $where_bank . " AND payment_type=88  AND bank=" . $value['bank_id'] . " AND company_id=" . Yii::app()->user->company_id . "")->queryRow();
                        $subcontractor = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE reconciliation_status = 1 " . $where_bank . " AND payment_type=88  AND bank=" . $value['bank_id'] . " AND company_id=" . Yii::app()->user->company_id . "")->queryRow();

                        $dailyvendors_check = $dailyvendors['total_amount'];
                        $subcontractor_check = $subcontractor['total_amount'];

                        //$withdrawal = $dailyvendors_check+$subcontractor_check+$daybook_with_amount+$dailyexpense_with_amount+$value['cashbalance_withdrawal'];
                        //$withdrawal = $dailyvendors_check+$subcontractor_check+$daybook_with_amount+$dailyexpense_with_amount;
                        $withdrawal = $dailyvendors_check + $daybook_with_amount + $dailyexpense_with_amount;
                    } else {

                        //deposit
                        $daybook_cah = Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE payment_type=89 AND type=72 " . $where_hand . " AND company_id=" . Yii::app()->user->company_id . "")->queryRow();
                        $daybook_cash = $daybook_cah['total_amount'];
                        $dailyexpense_cah = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE (dailyexpense_receipt_type=89 OR dailyexpense_receipt_type=103) AND exp_type=72 " . $where_hand . " AND company_id=" . Yii::app()->user->company_id . "")->queryRow();
                        $dailyexpense_cash = $dailyexpense_cah['total_amount'];

                        //$deposit = $daybook_cash+ $dailyexpense_cash+$value['cashbalance_deposit'];
                        $deposit = $daybook_cash + $dailyexpense_cash;
                        // withdrawal

                        $daybook_with = Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE expense_type=89 AND type=73 " . $where_hand . " AND company_id=" . Yii::app()->user->company_id . "")->queryRow();
                        $daybook_with_amount = $daybook_with['total_amount'];
                        $dailyexpense_with = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE (expense_type=89 OR expense_type=103) AND exp_type=73 " . $where_hand . " AND company_id=" . Yii::app()->user->company_id . "")->queryRow();
                        $dailyexpense_with_amount = $dailyexpense_with['total_amount'];


                        $dailyvendors_cah = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE payment_type=89 " . $where_hand . " AND company_id=" . Yii::app()->user->company_id . "")->queryRow();
                        $dailyvendors_cash = $dailyvendors_cah['total_amount'];
                        $subcontractor_cah = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE payment_type=89 " . $where_hand . " AND company_id=" . Yii::app()->user->company_id . "")->queryRow();
                        $subcontractor_cash = $subcontractor_cah['total_amount'];

                        //$withdrawal = $dailyvendors_cash+$subcontractor_cash+$daybook_with_amount+$dailyexpense_with_amount+$value['cashbalance_withdrawal'];
                        //$withdrawal = $dailyvendors_cash+$subcontractor_cash+$daybook_with_amount+$dailyexpense_with_amount;
                        $withdrawal = $dailyvendors_cash + $daybook_with_amount + $dailyexpense_with_amount;
                    }

                    $finaldata[$k][] = $key + 1;
                    $finaldata[$k][] = $value['caption'];
                    $finaldata[$k][] = $value['bank_name'];
                    $finaldata[$k][] = $value['cashbalance_date'];
                    $finaldata[$k][] = Controller::money_format_inr($value['cashbalance_opening_balance'], 2, 1);
                    $finaldata[$k][] = Controller::money_format_inr($deposit, 2, 1);
                    $finaldata[$k][] = Controller::money_format_inr($withdrawal, 2, 1);
                    $finaldata[$k][] = number_format(($value['cashbalance_opening_balance'] + $deposit) - $withdrawal, 2);
                    $k++;
                }
            }
        }


        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'cash_blance_report' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actionSavetopdf()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $cash_blance = Yii::app()->db->createCommand("SELECT {$tblpx}status.caption,{$tblpx}cashbalance.cashbalance_type,{$tblpx}cashbalance.bank_id,{$tblpx}bank.bank_name,{$tblpx}cashbalance.cashbalance_opening_balance,{$tblpx}cashbalance.cashbalance_deposit,{$tblpx}cashbalance.cashbalance_withdrawal,{$tblpx}cashbalance.cashbalance_closing_balance,{$tblpx}cashbalance.cashbalance_date FROM {$tblpx}cashbalance LEFT JOIN {$tblpx}bank ON {$tblpx}cashbalance.bank_id={$tblpx}bank.bank_id LEFT JOIN {$tblpx}status ON {$tblpx}cashbalance.cashbalance_type={$tblpx}status.sid ORDER BY bank_id ASC")->queryAll();
        $mPDF1 = Yii::app()->ePdf->mPDF();
        # You can easily override default constructor's params
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $mPDF1->WriteHTML($stylesheet, 1);
        //$mPDF1->SetHtmlHeader("Your header text here");
        //$mPDF1->SetHtmlFooter("Your footer text here");
        $mPDF1->WriteHTML($this->renderPartial('savetopdf', array(
            'cash_blance' => $cash_blance,
        ), true));
        $filename = "Cash blance report";
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    public function actionSavetoexcel()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $arraylabel = array('sl no', 'Cash Balance Type', 'Bank', 'Date', 'Opening Balance', 'Deposit', 'Withdrawal', 'Closing Balance');
        $finaldata = array();
        $cash_blance = Yii::app()->db->createCommand("SELECT {$tblpx}status.caption,{$tblpx}cashbalance.cashbalance_type,{$tblpx}cashbalance.bank_id,{$tblpx}bank.bank_name,{$tblpx}cashbalance.cashbalance_opening_balance,{$tblpx}cashbalance.cashbalance_deposit,{$tblpx}cashbalance.cashbalance_withdrawal,{$tblpx}cashbalance.cashbalance_closing_balance,{$tblpx}cashbalance.cashbalance_date FROM {$tblpx}cashbalance LEFT JOIN {$tblpx}bank ON {$tblpx}cashbalance.bank_id={$tblpx}bank.bank_id LEFT JOIN {$tblpx}status ON {$tblpx}cashbalance.cashbalance_type={$tblpx}status.sid ORDER BY bank_id ASC")->queryAll();
        foreach ($cash_blance as $key => $value) {
            $finaldata[$key][] = $key + 1;
            $finaldata[$key][] = $value['caption'];
            $finaldata[$key][] = $value['bank_name'];
            $finaldata[$key][] = $value['cashbalance_date'];
            $finaldata[$key][] = $value['cashbalance_opening_balance'];
            $finaldata[$key][] = $value['cashbalance_deposit'];
            $finaldata[$key][] = $value['cashbalance_withdrawal'];
            $finaldata[$key][] = $value['cashbalance_closing_balance'];
        }


        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'cash_blance_report' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Cashbalance the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Cashbalance::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Cashbalance $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'cashbalance-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actiondailyfinancialreport()
    {
        if (isset($_POST['date'])) {
            $date = $_POST['date'];
        } else {
            $date = date('Y-m-d');
        }

        if (isset($_POST['company_id'])) {
            $company_id = $_POST['company_id'];
        } else {
            $company_id = "";
        }

        $this->render('daily_financialreport', array('date' => $date, 'company_id' => $company_id));
    }

    public function actiondailyfinancialdetailedpdf($date, $company_id)
    {
        $this->logo = $this->realpath_logo;
        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
        $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $mPDF1->AddPage('','', '', '', '', 0,0,52, 30, 10,0);
        $mPDF1->WriteHTML($this->renderPartial('dailyfinancialdetailedpdf', array(
            'date' => $date,
            'company_id' => $company_id,
        ), true));
        $filename = "Daily Fincial Detailed Report";
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    public function actiondailyfinancialdetailedexcel($date, $company_id)
    {
        $tblpx = Yii::app()->db->tablePrefix;

        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        $newQuery1 = "";
        $newQuery2 = "";
        $newQuery3 = "";
        $newQuery4 = "";
        $newQuery5 = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
        }

        if ($company_id == '') {
            foreach ($arrVal as $arr) {
                if ($newQuery1) $newQuery1 .= ' OR';
                if ($newQuery2) $newQuery2 .= ' OR';
                if ($newQuery3) $newQuery3 .= ' OR';
                if ($newQuery4) $newQuery4 .= ' OR';
                if ($newQuery5) $newQuery5 .= ' OR';
                $newQuery1 .= " FIND_IN_SET('" . $arr . "', {$tblpx}cashbalance.company_id)";
                $newQuery2 .= " FIND_IN_SET('" . $arr . "', company_id)";
                $newQuery3 .= " FIND_IN_SET('" . $arr . "', {$tblpx}expenses.company_id)";
                $newQuery4 .= " FIND_IN_SET('" . $arr . "', {$tblpx}dailyvendors.company_id)";
                $newQuery5 .= " FIND_IN_SET('" . $arr . "', {$tblpx}subcontractor_payment.company_id)";
            }
        } else {
            $newQuery1 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}cashbalance.company_id)";
            $newQuery2 .= " FIND_IN_SET('" . $company_id . "', company_id)";
            $newQuery3 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}expenses.company_id)";
            $newQuery4 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}dailyvendors.company_id)";
            $newQuery5 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}subcontractor_payment.company_id)";
        }

        $arraylabel = array('Daily Financial Detailed Report', '', '', '', '');
        $finaldata = array();
        $a = 0;
        $b = 0;
        $c = 0;
        $d = 0;
        $e = 0;
        $f = 0;
        $g = 0;
        $h = 0;
        $i = 0;
        $j = 0;
        $k = 0;
        $l = 0;
        $m = 0;
        $m = 0;
        $o = 0;
        $n = 0;
        $p = 0;

        //  opening balance
        $finaldata[0][] = 'Opening Balance';
        $a = 1;
        $current_date = date('Y-m-d', strtotime('-1 day', strtotime($date)));
        $cash_blance = Yii::app()->db->createCommand("SELECT {$tblpx}status.caption,{$tblpx}cashbalance.cashbalance_type,{$tblpx}cashbalance.bank_id,{$tblpx}bank.bank_name,{$tblpx}cashbalance.cashbalance_opening_balance,{$tblpx}cashbalance.cashbalance_deposit,{$tblpx}cashbalance.cashbalance_withdrawal,{$tblpx}cashbalance.cashbalance_closing_balance,{$tblpx}cashbalance.cashbalance_date,{$tblpx}cashbalance.company_id FROM {$tblpx}cashbalance LEFT JOIN {$tblpx}bank ON {$tblpx}cashbalance.bank_id={$tblpx}bank.bank_id LEFT JOIN {$tblpx}status ON {$tblpx}cashbalance.cashbalance_type={$tblpx}status.sid WHERE (" . $newQuery1 . ") ORDER BY bank_id ASC")->queryAll();
        foreach ($cash_blance as $key => $value) {
            if ($value['bank_id'] != '') {
                //recepit
                $daybook =  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=88 AND bank_id=" . $value['bank_id'] . " AND type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND dailyexpense_receipt_type=88 AND bank_id=" . $value['bank_id'] . " AND exp_type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_check = $daybook['total_amount'];
                $dailyexpense_check = $dailyexpense['total_amount'];
                //$deposit = $daybook_check+$dailyexpense_check+$value['cashbalance_opening_balance'];
                $deposit = $daybook_check + $dailyexpense_check;

                //expense
                $daybook_with =  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND expense_type=88 AND bank_id=" . $value['bank_id'] . " AND type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense_with =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND expense_type=88 AND bank_id=" . $value['bank_id'] . " AND exp_type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_with_amount = $daybook_with['total_amount'];
                $dailyexpense_with_amount = $dailyexpense_with['total_amount'];
                $dailyvendors =  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=88  AND bank=" . $value['bank_id'] . " AND company_id=" . $value['company_id'] . "")->queryRow();
                $subcontractor =  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=88  AND bank=" . $value['bank_id'] . " AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyvendors_check = $dailyvendors['total_amount'];
                $subcontractor_check = $subcontractor['total_amount'];
                $withdrawal = $dailyvendors_check + $daybook_with_amount + $dailyexpense_with_amount;
            } else {
                //receipt
                $daybook_cah =  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=89 AND type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_cash = $daybook_cah['total_amount'];
                //$dailyexpense_cah=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (dailyexpense_receipt_type=89 OR dailyexpense_receipt_type=103) AND exp_type=72 AND company_id=".$value['company_id']."")->queryRow();
                $dailyexpense_cah =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND (dailyexpense_receipt_type=89) AND exp_type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense_cash = $dailyexpense_cah['total_amount'];
                //$deposit = $daybook_cash+ $dailyexpense_cash+$value['cashbalance_opening_balance'];
                $deposit = $daybook_cash + $dailyexpense_cash;


                // expense
                $daybook_with =  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND (expense_type=89 OR expense_type =103) AND type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_with_amount = $daybook_with['total_amount'];
                //$dailyexpense_with=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (expense_type=89 OR expense_type=103) AND exp_type=73 AND company_id=".$value['company_id']."")->queryRow();
                $dailyexpense_with =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND (expense_type=89) AND exp_type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense_with_amount = $dailyexpense_with['total_amount'];
                $dailyvendors_cah =  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=89 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyvendors_cash = $dailyvendors_cah['total_amount'];
                $subcontractor_cah =  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=89 AND company_id=" . $value['company_id'] . "")->queryRow();
                $subcontractor_cash = $subcontractor_cah['total_amount'];

                $withdrawal = $dailyvendors_cash + $daybook_with_amount + $dailyexpense_with_amount;
            }
            $company = Company::model()->findByPk($value['company_id']);
            $finaldata[$a][] = ($value['caption'] == 'Bank') ? $value['bank_name'] . ' - ' . $company->name : $value['caption'] . ' - ' . $company->name;
            $finaldata[$a][] = Controller::money_format_inr(($value['cashbalance_opening_balance'] + $deposit) - $withdrawal, 2);
            $a++;
        }

        // invoice
        $finaldata[$a][] = 'Invoice';
        $b = $a + 1;
        $invoice_amount = 0;
        $invoice = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses LEFT JOIN {$tblpx}projects ON {$tblpx}expenses.projectid = {$tblpx}projects.pid WHERE {$tblpx}expenses.date = '" . $date . "' AND (" . $newQuery3 . ") AND {$tblpx}expenses.invoice_id IS NOT NULL AND (({$tblpx}expenses.reconciliation_status =1 AND {$tblpx}expenses.payment_type =88) OR ({$tblpx}expenses.payment_type =89)) AND {$tblpx}expenses.type= 72 GROUP BY {$tblpx}projects.pid ORDER BY {$tblpx}expenses.exp_id DESC")->queryAll();
        if (!empty($invoice)) {
            foreach ($invoice as $key => $value) {
                $invoice_amount += $value['receipt'];
                $finaldata[$b][] = $value['name'];
                $finaldata[$b][] = Controller::money_format_inr($value['receipt'], 2);
                $b++;
            }
        } else {
            $b = $b;
        }

        $finaldata[$b][] = 'Total';
        $finaldata[$b][] = Controller::money_format_inr($invoice_amount, 2);

        // receipt

        $finaldata[$b + 1][] = 'Receipt';
        $finaldata[$b + 2][] = 'Daybook';
        $finaldata[$b + 3][] = 'Cash Receipt';
        $c = $b + 4;
        $daybook_cash = 0;
        $receipt_amount = 0;
        $receipt_daybook = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses LEFT JOIN {$tblpx}projects ON {$tblpx}expenses.projectid = {$tblpx}projects.pid WHERE {$tblpx}expenses.date = '" . $date . "' AND (" . $newQuery3 . ") AND {$tblpx}expenses.type=72 AND {$tblpx}expenses.payment_type=89 AND {$tblpx}expenses.invoice_id IS NULL GROUP BY {$tblpx}projects.pid ORDER BY {$tblpx}expenses.exp_id DESC")->queryAll();
        if (!empty($receipt_daybook)) {
            foreach ($receipt_daybook as $key => $value) {
                $project_amount = Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE projectid =" . $value['pid'] . " and date='" . $date . "' AND type=72 AND payment_type= 89 AND invoice_id IS NULL AND (" . $newQuery2 . ") ORDER BY exp_id DESC")->queryRow();
                $finaldata[$c][] = 'Project :' . $value['name'];
                $finaldata[$c][] = Controller::money_format_inr($project_amount['total_amount'], 2);
                $details_1 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses WHERE projectid =" . $value['pid'] . " and date='" . $date . "' AND type=72 AND payment_type= 89 AND invoice_id IS NULL AND (" . $newQuery2 . ") ORDER BY exp_id DESC")->queryAll();
                $d = $c + 1;
                foreach ($details_1 as $key => $values) {
                    $daybook_cash += $values['receipt'];
                    $finaldata[$d][] = $values['description'];
                    $finaldata[$d][] = Controller::money_format_inr($values['receipt'], 2);
                    $d++;
                }
                $c = $d;
            }
        } else {
            $c = $c;
        }
        $finaldata[$c][] = 'Cash Total';
        $finaldata[$c][] = Controller::money_format_inr($daybook_cash, 2);

        $finaldata[$c + 1][] = 'Bank Receipt';
        $e = $c + 2;
        $daybook_bank = 0;
        $receipt_daybook2 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses LEFT JOIN {$tblpx}projects ON {$tblpx}expenses.projectid = {$tblpx}projects.pid WHERE {$tblpx}expenses.date = '" . $date . "' AND (" . $newQuery3 . ") AND {$tblpx}expenses.type=72 AND {$tblpx}expenses.payment_type=88 AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.reconciliation_status=1 GROUP BY {$tblpx}projects.pid ORDER BY {$tblpx}expenses.exp_id DESC")->queryAll();
        if (!empty($receipt_daybook2)) {
            foreach ($receipt_daybook2 as $key => $value) {
                $project_amount = Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE projectid =" . $value['pid'] . " and date='" . $date . "' AND type=72 AND payment_type= 88 AND invoice_id IS NULL AND reconciliation_status=1 AND (" . $newQuery2 . ") ORDER BY exp_id DESC")->queryRow();
                $finaldata[$e][] = 'Project :' . $value['name'];
                $finaldata[$e][] = Controller::money_format_inr($project_amount['total_amount'], 2);
                $details_2 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses WHERE projectid =" . $value['pid'] . " and date='" . $date . "' AND type=72 AND payment_type= 88 AND invoice_id IS NULL AND reconciliation_status=1 AND (" . $newQuery2 . ") ORDER BY exp_id DESC")->queryAll();
                $f = $e + 1;
                foreach ($details_2 as $key => $values) {
                    $daybook_bank += $values['receipt'];
                    $finaldata[$f][] = $values['description'];
                    $finaldata[$f][] = Controller::money_format_inr($values['receipt'], 2);
                    $f++;
                }
                $e = $f;
            }
        } else {
            $e = $e;
        }

        $finaldata[$e][] = 'Bank Total';
        $finaldata[$e][] = Controller::money_format_inr($daybook_bank, 2);

        $finaldata[$e + 1][] = 'Net Amount';
        $finaldata[$e + 1][] = Controller::money_format_inr($daybook_bank + $daybook_cash, 2);

        $finaldata[$e + 2][] = 'Daily Expenses';
        $finaldata[$e + 3][] = 'Cash Receipt';
        $g = $e + 4;
        $dailyexp_cash = 0;
        $receipt_dailyexp = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyexpense WHERE date ='" . $date . "' AND dailyexpense_receipt_type=89 AND (" . $newQuery2 . ") ORDER BY dailyexp_id DESC")->queryAll();
        if ($receipt_dailyexp) {
            foreach ($receipt_dailyexp as $key => $value) {
                $dailyexp_cash += $value['dailyexpense_receipt'];
                $finaldata[$g][] = $value['description'];
                $finaldata[$g][] = Controller::money_format_inr($value['dailyexpense_receipt'], 2);
                $g++;
            }
        } else {
            $g = $g;
        }
        $finaldata[$g][] = 'Cash Total';
        $finaldata[$g][] = Controller::money_format_inr($dailyexp_cash, 2);

        $finaldata[$g + 1][] = 'Bank Receipt';
        $h = $g + 2;
        $dailyexp_bank = 0;
        $receipt_dailyexp = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyexpense WHERE date ='" . $date . "' AND dailyexpense_receipt_type=88 AND (" . $newQuery2 . ") AND reconciliation_status=1 ORDER BY dailyexp_id DESC")->queryAll();
        if (!empty($receipt_dailyexp)) {
            foreach ($receipt_dailyexp as $key => $value) {
                $dailyexp_bank += $value['dailyexpense_receipt'];
                $finaldata[$h][] = $value['description'];
                $finaldata[$h][] = Controller::money_format_inr($value['dailyexpense_receipt'], 2);
                $h++;
            }
        } else {
            $h = $h;
        }
        $finaldata[$h][] = 'Bank Total';
        $finaldata[$h][] = Controller::money_format_inr($dailyexp_bank, 2);

        $finaldata[$h + 1][] = 'Net Amount';
        $finaldata[$h + 1][] = Controller::money_format_inr($dailyexp_bank + $dailyexp_cash, 2);
        $receipt_amount = $dailyexp_bank + $dailyexp_cash + $daybook_bank + $daybook_cash;
        $finaldata[$h + 2][] = 'Grand Total';
        $finaldata[$h + 2][] = Controller::money_format_inr($receipt_amount, 2);

        $finaldata[$h + 3][] = 'Expense';
        $finaldata[$h + 4][] = 'Daybook';
        $i = $h + 5;
        // expenses

        $daybook_expense = 0;
        $expense_amount = 0;
        $expense_daybook = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses LEFT JOIN {$tblpx}projects ON {$tblpx}expenses.projectid = {$tblpx}projects.pid WHERE {$tblpx}expenses.date = '" . $date . "' AND (" . $newQuery3 . ") AND {$tblpx}expenses.type=73 AND {$tblpx}expenses.subcontractor_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND (({$tblpx}expenses.reconciliation_status =1 AND {$tblpx}expenses.expense_type =88) OR ({$tblpx}expenses.expense_type =89 OR  {$tblpx}expenses.expense_type =103)) GROUP BY {$tblpx}projects.pid ORDER BY {$tblpx}expenses.exp_id DESC")->queryAll();
        if (!empty($expense_daybook)) {
            foreach ($expense_daybook as $key => $value) {
                $project_amount = Yii::app()->db->createCommand("SELECT SUM(paid) as project_amount FROM {$tblpx}expenses WHERE projectid =" . $value['pid'] . " and date='" . $date . "' AND type=73 AND invoice_id IS NULL AND subcontractor_id IS NULL AND ((reconciliation_status =1 AND expense_type =88) OR (expense_type =89 OR expense_type = 103)) ORDER BY exp_id DESC")->queryRow();
                $finaldata[$i][] = 'Project :' . $value['name'];
                $finaldata[$i][] = Controller::money_format_inr($project_amount['project_amount'], 2);
                $j = $i + 1;
                $details_1 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses WHERE projectid =" . $value['pid'] . " and date='" . $date . "' AND type=73 AND invoice_id IS NULL AND subcontractor_id IS NULL AND ((reconciliation_status =1 AND expense_type =88) OR (expense_type =89 OR expense_type = 103)) ORDER BY exp_id DESC")->queryAll();
                foreach ($details_1 as $key => $values) {
                    $daybook_expense += $values['paid'];
                    $finaldata[$j][] = $values['description'];
                    $finaldata[$j][] = Controller::money_format_inr($values['paid'], 2);
                    $j++;
                }
                $i = $j;
            }
            $expense_amount += $daybook_expense;
        } else {
            $i = $i;
        }
        $finaldata[$i][] = 'Net Amount';
        $finaldata[$i][] = Controller::money_format_inr($daybook_expense, 2);

        $finaldata[$i + 1][] = 'Daily Expenses';

        $j = $i + 2;
        $dailyexp_expense = 0;
        $receipt_dailyexp = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyexpense WHERE date ='" . $date . "' AND exp_type=73 AND (" . $newQuery2 . ") AND ((expense_type = 88 AND reconciliation_status =1)OR (expense_type = 89)) ORDER BY dailyexp_id DESC")->queryAll();
        if (!empty($receipt_dailyexp)) {
            foreach ($receipt_dailyexp as $key => $value) {
                $dailyexp_expense += $value['dailyexpense_paidamount'];
                $finaldata[$j][] = $value['description'];
                $finaldata[$j][] = Controller::money_format_inr($value['dailyexpense_paidamount'], 2);
                $j++;
            }
            $expense_amount += $dailyexp_expense;
        } else {
            $j = $j;
        }

        $finaldata[$j][] = 'Net Amount';
        $finaldata[$j][] = Controller::money_format_inr($dailyexp_expense, 2);

        $finaldata[$j + 1][] = 'Vendor Payment';
        $k = $j + 2;

        $vendor_expense = 0;
        $expense_vendor = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyvendors LEFT JOIN {$tblpx}projects ON {$tblpx}dailyvendors.project_id = {$tblpx}projects.pid WHERE {$tblpx}dailyvendors.date = '" . $date . "' AND (" . $newQuery4 . ") AND {$tblpx}dailyvendors.project_id IS NOT NULL AND (({$tblpx}dailyvendors.payment_type = 88 AND {$tblpx}dailyvendors.reconciliation_status =1) OR ({$tblpx}dailyvendors.payment_type = 89)) GROUP BY {$tblpx}projects.pid ORDER BY {$tblpx}dailyvendors.daily_v_id DESC")->queryAll();
        if (!empty($expense_vendor)) {
            foreach ($expense_vendor as $key => $value) {
                $project_amount = Yii::app()->db->createCommand("SELECT SUM( IFNULL(tax_amount, 0) + IFNULL(amount, 0) ) as project_amount FROM {$tblpx}dailyvendors WHERE project_id =" . $value['pid'] . " and date='" . $date . "' AND ((payment_type = 88 AND reconciliation_status =1) OR (payment_type = 89)) ORDER BY daily_v_id DESC")->queryRow();
                $finaldata[$k][] = 'Project :' . $value['name'];
                $finaldata[$k][] = Controller::money_format_inr($project_amount['project_amount'], 2);
                $details_1 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyvendors WHERE project_id =" . $value['pid'] . " and date='" . $date . "' AND ((payment_type = 88 AND reconciliation_status =1) OR (payment_type = 89)) ORDER BY daily_v_id DESC")->queryAll();
                $l = $k + 1;
                foreach ($details_1 as $key => $values) {
                    $vendor_expense += $values['amount'] + $values['tax_amount'];
                    $finaldata[$l][] = $values['description'];
                    $finaldata[$l][] = Controller::money_format_inr($values['amount'] + $values['tax_amount'], 2);
                    $l++;
                }
                $k = $l;
            }
        } else {
            $k = $k;
        }

        $finaldata[$k][] = 'Other Expenses';

        $vendor_expense2 = 0;
        $details_2 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyvendors WHERE project_id IS NULL and date='" . $date . "' AND (" . $newQuery4 . ") AND ((payment_type = 88 AND reconciliation_status =1) OR (payment_type = 89)) ORDER BY daily_v_id DESC")->queryAll();
        if (!empty($details_2)) {
            $n = $k + 1;
            foreach ($details_2 as $key => $values) {
                $vendor_expense2 += $values['amount'] + $values['tax_amount'];
                $finaldata[$n][] = $values['description'];
                $finaldata[$n][] = Controller::money_format_inr($values['amount'] + $values['tax_amount'], 2);
                $n++;
            }
            $p = $n;
            //$expense_amount += $vendor_expense2;
        } else {
            $p = $k;
        }

        $expense_amount += $vendor_expense + $vendor_expense2;

        $finaldata[$p][] = 'Net Amount';
        $finaldata[$p][] = Controller::money_format_inr($vendor_expense + $vendor_expense2, 2);

        $finaldata[$p + 1][] = 'Subcontractor Payment';
        $m = $p + 2;

        $subcontractor_expense = 0;
        $expense_subcontractor = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}subcontractor_payment LEFT JOIN {$tblpx}projects ON {$tblpx}subcontractor_payment.project_id = {$tblpx}projects.pid WHERE {$tblpx}subcontractor_payment.date = '" . $date . "' AND (" . $newQuery5 . ") AND (({$tblpx}subcontractor_payment.payment_type = 88 AND {$tblpx}subcontractor_payment.reconciliation_status = 1) OR ({$tblpx}subcontractor_payment.payment_type = 89)) GROUP BY {$tblpx}projects.pid ORDER BY {$tblpx}subcontractor_payment.payment_id DESC")->queryAll();
        if (!empty($expense_subcontractor)) {
            foreach ($expense_subcontractor as $key => $value) {
                $project_amount = Yii::app()->db->createCommand("SELECT SUM( IFNULL(tax_amount, 0) + IFNULL(amount, 0) ) as project_amount FROM {$tblpx}subcontractor_payment WHERE project_id =" . $value['pid'] . " and date='" . $date . "' AND ((payment_type = 88 AND reconciliation_status = 1) OR (payment_type = 89)) ORDER BY payment_id DESC")->queryRow();
                $finaldata[$m][] = 'Project :' . $value['name'];
                $finaldata[$m][] = Controller::money_format_inr($project_amount['project_amount'], 2);
                $details_1 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}subcontractor_payment WHERE project_id =" . $value['pid'] . " and date='" . $date . "' AND ((payment_type = 88 AND reconciliation_status = 1) OR (payment_type = 89)) ORDER BY payment_id DESC")->queryAll();
                $n = $m + 1;
                foreach ($details_1 as $key => $values) {
                    $subcontractor_expense += $values['amount'] + $values['tax_amount'];
                    $finaldata[$n][] = $values['description'];
                    $finaldata[$n][] = Controller::money_format_inr($values['amount'] + $values['tax_amount'], 2);
                    $n++;
                }
                $m = $n;
            }
            $expense_amount += $subcontractor_expense;
        } else {
            $m = $m;
        }

        $finaldata[$m][] = 'Net Amount';
        $finaldata[$m][] = Controller::money_format_inr($subcontractor_expense, 2);
        $finaldata[$m + 1][] = 'Grand Total';
        $finaldata[$m + 1][] = Controller::money_format_inr($expense_amount, 2);
        $finaldata[$m + 2][] = 'Closing Balance';
        $o = $m + 3;
        // closeing blnc

        $current_date = $date;
        $cash_blance = Yii::app()->db->createCommand("SELECT {$tblpx}status.caption,{$tblpx}cashbalance.cashbalance_type,{$tblpx}cashbalance.bank_id,{$tblpx}bank.bank_name,{$tblpx}cashbalance.cashbalance_opening_balance,{$tblpx}cashbalance.cashbalance_deposit,{$tblpx}cashbalance.cashbalance_withdrawal,{$tblpx}cashbalance.cashbalance_closing_balance,{$tblpx}cashbalance.cashbalance_date,{$tblpx}cashbalance.company_id FROM {$tblpx}cashbalance LEFT JOIN {$tblpx}bank ON {$tblpx}cashbalance.bank_id={$tblpx}bank.bank_id LEFT JOIN {$tblpx}status ON {$tblpx}cashbalance.cashbalance_type={$tblpx}status.sid WHERE (" . $newQuery1 . ") ORDER BY bank_id ASC")->queryAll();
        foreach ($cash_blance as $key => $value) {
            if ($value['bank_id'] != '') {
                //recepit
                $daybook =  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=88 AND bank_id=" . $value['bank_id'] . " AND type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND dailyexpense_receipt_type=88 AND bank_id=" . $value['bank_id'] . " AND exp_type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_check = $daybook['total_amount'];
                $dailyexpense_check = $dailyexpense['total_amount'];
                //$deposit = $daybook_check+$dailyexpense_check+$value['cashbalance_opening_balance'];
                $deposit = $daybook_check + $dailyexpense_check;

                //expense
                $daybook_with =  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND expense_type=88 AND bank_id=" . $value['bank_id'] . " AND type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense_with =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND expense_type=88 AND bank_id=" . $value['bank_id'] . " AND exp_type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_with_amount = $daybook_with['total_amount'];
                $dailyexpense_with_amount = $dailyexpense_with['total_amount'];
                $dailyvendors =  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=88  AND bank=" . $value['bank_id'] . " AND company_id=" . $value['company_id'] . "")->queryRow();
                $subcontractor =  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=88  AND bank=" . $value['bank_id'] . " AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyvendors_check = $dailyvendors['total_amount'];
                $subcontractor_check = $subcontractor['total_amount'];
                $withdrawal = $dailyvendors_check + $daybook_with_amount + $dailyexpense_with_amount;
            } else {
                //receipt
                $daybook_cah =  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=89 AND type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_cash = $daybook_cah['total_amount'];
                //$dailyexpense_cah=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (dailyexpense_receipt_type=89 OR dailyexpense_receipt_type=103) AND exp_type=72 AND company_id=".$value['company_id']."")->queryRow();
                $dailyexpense_cah =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND (dailyexpense_receipt_type=89) AND exp_type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense_cash = $dailyexpense_cah['total_amount'];
                //$deposit = $daybook_cash+ $dailyexpense_cash+$value['cashbalance_opening_balance'];
                $deposit = $daybook_cash + $dailyexpense_cash;
                //echo $current_date;
                //echo $daybook_cash;

                // expense
                $daybook_with =  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND (expense_type=89 OR expense_type=103) AND type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_with_amount = $daybook_with['total_amount'];
                //$dailyexpense_with=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (expense_type=89 OR expense_type=103) AND exp_type=73 AND company_id=".$value['company_id']."")->queryRow();
                $dailyexpense_with =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND (expense_type=89) AND exp_type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense_with_amount = $dailyexpense_with['total_amount'];
                $dailyvendors_cah =  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=89 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyvendors_cash = $dailyvendors_cah['total_amount'];
                $subcontractor_cah =  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=89 AND company_id=" . $value['company_id'] . "")->queryRow();
                $subcontractor_cash = $subcontractor_cah['total_amount'];

                $withdrawal = $dailyvendors_cash + $daybook_with_amount + $dailyexpense_with_amount;
            }
            $company = Company::model()->findByPk($value['company_id']);
            $finaldata[$o][] = ($value['caption'] == 'Bank') ? $value['bank_name'] . ' - ' . $company->name : $value['caption'] . ' - ' . $company->name;
            $finaldata[$o][] = Controller::money_format_inr(($value['cashbalance_opening_balance'] + $deposit) - $withdrawal, 2);
            $o++;
        }


        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'daily_financial_detailed_report' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actiondailyfinancialsummary()
    {
        if (isset($_POST['date'])) {
            $date = $_POST['date'];
        } else {
            $date = date('Y-m-d');
        }

        if (isset($_POST['company_id'])) {
            $company_id = $_POST['company_id'];
        } else {
            $company_id = "";
        }

        $this->render('daily_financialsummary', array('date' => $date, 'company_id' => $company_id));
    }

    public function actiondailyfinancialsummarypdf($date, $company_id)
    {
        $this->logo = $this->realpath_logo;
        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
        $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $mPDF1->AddPage('','', '', '', '', 0,0,52, 50, 10,0);
        $mPDF1->WriteHTML($this->renderPartial('dailyfinancialsummarypdf', array(
            'date' => $date,
            'company_id' => $company_id,
        ), true));
        $filename = "Daily Fincial Summary Report";
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    public function actiondailyfinancialsummaryexcel($date, $company_id)
    {
        $tblpx = Yii::app()->db->tablePrefix;

        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        $newQuery1 = "";
        $newQuery2 = "";
        $newQuery3 = "";
        $newQuery4 = "";
        $newQuery5 = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
        }

        if ($company_id == '') {
            foreach ($arrVal as $arr) {
                if ($newQuery1) $newQuery1 .= ' OR';
                if ($newQuery2) $newQuery2 .= ' OR';
                if ($newQuery3) $newQuery3 .= ' OR';
                if ($newQuery4) $newQuery4 .= ' OR';
                if ($newQuery5) $newQuery5 .= ' OR';
                $newQuery1 .= " FIND_IN_SET('" . $arr . "', {$tblpx}cashbalance.company_id)";
                $newQuery2 .= " FIND_IN_SET('" . $arr . "', company_id)";
                $newQuery3 .= " FIND_IN_SET('" . $arr . "', {$tblpx}expenses.company_id)";
                $newQuery4 .= " FIND_IN_SET('" . $arr . "', {$tblpx}dailyvendors.company_id)";
                $newQuery5 .= " FIND_IN_SET('" . $arr . "', {$tblpx}subcontractor_payment.company_id)";
            }
        } else {
            $newQuery1 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}cashbalance.company_id)";
            $newQuery2 .= " FIND_IN_SET('" . $company_id . "', company_id)";
            $newQuery3 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}expenses.company_id)";
            $newQuery4 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}dailyvendors.company_id)";
            $newQuery5 .= " FIND_IN_SET('" . $company_id . "', {$tblpx}subcontractor_payment.company_id)";
        }
        $arraylabel = array('Daily Financial Summary Report', '', '', '', '');
        $finaldata = array();
        $a = 0;
        $b = 0;
        $c = 0;
        $d = 0;
        $e = 0;
        $f = 0;
        $g = 0;
        $h = 0;
        $i = 0;
        $j = 0;
        $k = 0;
        $l = 0;
        $m = 0;
        $m = 0;
        $o = 0;
        $n = 0;
        $p = 0;

        //  opening balance
        $finaldata[0][] = 'Opening Balance';
        $a = 1;
        $current_date = date('Y-m-d', strtotime('-1 day', strtotime($date)));
        $cash_blance = Yii::app()->db->createCommand("SELECT {$tblpx}status.caption,{$tblpx}cashbalance.cashbalance_type,{$tblpx}cashbalance.bank_id,{$tblpx}bank.bank_name,{$tblpx}cashbalance.cashbalance_opening_balance,{$tblpx}cashbalance.cashbalance_deposit,{$tblpx}cashbalance.cashbalance_withdrawal,{$tblpx}cashbalance.cashbalance_closing_balance,{$tblpx}cashbalance.cashbalance_date,{$tblpx}cashbalance.company_id FROM {$tblpx}cashbalance LEFT JOIN {$tblpx}bank ON {$tblpx}cashbalance.bank_id={$tblpx}bank.bank_id LEFT JOIN {$tblpx}status ON {$tblpx}cashbalance.cashbalance_type={$tblpx}status.sid WHERE (" . $newQuery1 . ") ORDER BY bank_id ASC")->queryAll();
        foreach ($cash_blance as $key => $value) {
            if ($value['bank_id'] != '') {
                //recepit
                $daybook =  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=88 AND bank_id=" . $value['bank_id'] . " AND type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND dailyexpense_receipt_type=88 AND bank_id=" . $value['bank_id'] . " AND exp_type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_check = $daybook['total_amount'];
                $dailyexpense_check = $dailyexpense['total_amount'];
                //$deposit = $daybook_check+$dailyexpense_check+$value['cashbalance_opening_balance'];
                $deposit = $daybook_check + $dailyexpense_check;

                //expense
                $daybook_with =  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND expense_type=88 AND bank_id=" . $value['bank_id'] . " AND type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense_with =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND expense_type=88 AND bank_id=" . $value['bank_id'] . " AND exp_type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_with_amount = $daybook_with['total_amount'];
                $dailyexpense_with_amount = $dailyexpense_with['total_amount'];
                $dailyvendors =  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=88  AND bank=" . $value['bank_id'] . " AND company_id=" . $value['company_id'] . "")->queryRow();
                $subcontractor =  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=88  AND bank=" . $value['bank_id'] . " AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyvendors_check = $dailyvendors['total_amount'];
                $subcontractor_check = $subcontractor['total_amount'];
                $withdrawal = $dailyvendors_check + $daybook_with_amount + $dailyexpense_with_amount;
            } else {
                //receipt
                $daybook_cah =  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=89 AND type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_cash = $daybook_cah['total_amount'];
                //$dailyexpense_cah=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (dailyexpense_receipt_type=89 OR dailyexpense_receipt_type=103) AND exp_type=72 AND company_id=".$value['company_id']."")->queryRow();
                $dailyexpense_cah =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND (dailyexpense_receipt_type=89) AND exp_type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense_cash = $dailyexpense_cah['total_amount'];
                //$deposit = $daybook_cash+ $dailyexpense_cash+$value['cashbalance_opening_balance'];
                $deposit = $daybook_cash + $dailyexpense_cash;


                // expense
                $daybook_with =  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND (expense_type=89 OR expense_type = 103) AND type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_with_amount = $daybook_with['total_amount'];
                //$dailyexpense_with=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (expense_type=89 OR expense_type=103) AND exp_type=73 AND company_id=".$value['company_id']."")->queryRow();
                $dailyexpense_with =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND (expense_type=89) AND exp_type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense_with_amount = $dailyexpense_with['total_amount'];
                $dailyvendors_cah =  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=89 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyvendors_cash = $dailyvendors_cah['total_amount'];
                $subcontractor_cah =  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=89 AND company_id=" . $value['company_id'] . "")->queryRow();
                $subcontractor_cash = $subcontractor_cah['total_amount'];

                $withdrawal = $dailyvendors_cash + $daybook_with_amount + $dailyexpense_with_amount;
            }
            $company = Company::model()->findByPk($value['company_id']);
            $finaldata[$o][] = ($value['caption'] == 'Bank') ? $value['bank_name'] . ' - ' . $company->name : $value['caption'] . ' - ' . $company->name;
            $finaldata[$a][] = Controller::money_format_inr(($value['cashbalance_opening_balance'] + $deposit) - $withdrawal, 2);
            $a++;
        }

        // invoice
        $finaldata[$a][] = 'Invoice';
        $b = $a + 1;
        $invoice_amount = 0;
        $invoice = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses LEFT JOIN {$tblpx}projects ON {$tblpx}expenses.projectid = {$tblpx}projects.pid WHERE {$tblpx}expenses.date = '" . $date . "' AND (" . $newQuery3 . ") AND {$tblpx}expenses.invoice_id IS NOT NULL AND (({$tblpx}expenses.reconciliation_status =1 AND {$tblpx}expenses.payment_type =88) OR ({$tblpx}expenses.payment_type =89)) AND {$tblpx}expenses.type= 72 GROUP BY {$tblpx}projects.pid ORDER BY {$tblpx}expenses.exp_id DESC")->queryAll();
        if (!empty($invoice)) {
            foreach ($invoice as $key => $value) {
                $invoice_amount += $value['receipt'];
                $finaldata[$b][] = $value['name'];
                $finaldata[$b][] = Controller::money_format_inr($value['receipt'], 2);
                $b++;
            }
        } else {
            $b = $b;
        }

        $finaldata[$b][] = 'Total';
        $finaldata[$b][] = Controller::money_format_inr($invoice_amount, 2);

        // receipt

        $finaldata[$b + 1][] = 'Receipt';
        $finaldata[$b + 2][] = 'Daybook';
        $finaldata[$b + 3][] = 'Cash Receipt';
        $c = $b + 4;
        $daybook_cash = 0;
        $receipt_amount = 0;
        $receipt_daybook = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses LEFT JOIN {$tblpx}projects ON {$tblpx}expenses.projectid = {$tblpx}projects.pid WHERE {$tblpx}expenses.date = '" . $date . "' AND (" . $newQuery3 . ") AND {$tblpx}expenses.type=72 AND {$tblpx}expenses.payment_type=89 AND {$tblpx}expenses.invoice_id IS NULL GROUP BY {$tblpx}projects.pid ORDER BY {$tblpx}expenses.exp_id DESC")->queryAll();
        if (!empty($receipt_daybook)) {
            foreach ($receipt_daybook as $key => $value) {
                $project_amount = Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE projectid =" . $value['pid'] . " and date='" . $date . "' AND type=72 AND payment_type= 89 AND invoice_id IS NULL AND (" . $newQuery2 . ") ORDER BY exp_id DESC")->queryRow();
                $daybook_cash += $project_amount['total_amount'];
                $finaldata[$c][] = 'Project :' . $value['name'];
                $finaldata[$c][] = Controller::money_format_inr($project_amount['total_amount'], 2);
                $c++;
            }
        } else {
            $c = $c;
        }
        $finaldata[$c][] = 'Cash Total';
        $finaldata[$c][] = Controller::money_format_inr($daybook_cash, 2);

        $finaldata[$c + 1][] = 'Bank Receipt';
        $e = $c + 2;
        $daybook_bank = 0;
        $receipt_daybook2 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses LEFT JOIN {$tblpx}projects ON {$tblpx}expenses.projectid = {$tblpx}projects.pid WHERE {$tblpx}expenses.date = '" . $date . "' AND (" . $newQuery3 . ") AND {$tblpx}expenses.type=72 AND {$tblpx}expenses.payment_type=88 AND {$tblpx}expenses.invoice_id IS NULL AND {$tblpx}expenses.reconciliation_status=1 GROUP BY {$tblpx}projects.pid ORDER BY {$tblpx}expenses.exp_id DESC")->queryAll();
        if (!empty($receipt_daybook2)) {
            foreach ($receipt_daybook2 as $key => $value) {
                $project_amount = Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE projectid =" . $value['pid'] . " and date='" . $date . "' AND type=72 AND payment_type= 88 AND invoice_id IS NULL AND reconciliation_status=1 AND (" . $newQuery2 . ") ORDER BY exp_id DESC")->queryRow();
                $daybook_bank += $project_amount['total_amount'];
                $finaldata[$e][] = 'Project :' . $value['name'];
                $finaldata[$e][] = Controller::money_format_inr($project_amount['total_amount'], 2);
                $e++;
            }
        } else {
            $e = $e;
        }

        $finaldata[$e][] = 'Bank Total';
        $finaldata[$e][] = Controller::money_format_inr($daybook_bank, 2);

        $finaldata[$e + 1][] = 'Net Amount';
        $finaldata[$e + 1][] = Controller::money_format_inr($daybook_bank + $daybook_cash, 2);

        $finaldata[$e + 2][] = 'Daily Expenses';
        $finaldata[$e + 3][] = 'Cash Receipt';
        $g = $e + 4;
        $dailyexp_cash = 0;
        $receipt_dailyexp = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyexpense WHERE date ='" . $date . "' AND dailyexpense_receipt_type=89 AND (" . $newQuery2 . ") ORDER BY dailyexp_id DESC")->queryAll();
        if ($receipt_dailyexp) {
            foreach ($receipt_dailyexp as $key => $value) {
                $dailyexp_cash += $value['dailyexpense_receipt'];
                $finaldata[$g][] = $value['description'];
                $finaldata[$g][] = Controller::money_format_inr($value['dailyexpense_receipt'], 2);
                $g++;
            }
        } else {
            $g = $g;
        }
        $finaldata[$g][] = 'Cash Total';
        $finaldata[$g][] = Controller::money_format_inr($dailyexp_cash, 2);

        $finaldata[$g + 1][] = 'Bank Receipt';
        $h = $g + 2;
        $dailyexp_bank = 0;
        $receipt_dailyexp = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyexpense WHERE date ='" . $date . "' AND dailyexpense_receipt_type=88 AND (" . $newQuery2 . ") AND reconciliation_status=1 ORDER BY dailyexp_id DESC")->queryAll();
        if (!empty($receipt_dailyexp)) {
            foreach ($receipt_dailyexp as $key => $value) {
                $dailyexp_bank += $value['dailyexpense_receipt'];
                $finaldata[$h][] = $value['description'];
                $finaldata[$h][] = Controller::money_format_inr($value['dailyexpense_receipt'], 2);
                $h++;
            }
        } else {
            $h = $h;
        }
        $finaldata[$h][] = 'Bank Total';
        $finaldata[$h][] = Controller::money_format_inr($dailyexp_bank, 2);

        $finaldata[$h + 1][] = 'Net Amount';
        $finaldata[$h + 1][] = Controller::money_format_inr($dailyexp_bank + $dailyexp_cash, 2);
        $receipt_amount = $dailyexp_bank + $dailyexp_cash + $daybook_bank + $daybook_cash;
        $finaldata[$h + 2][] = 'Grand Total';
        $finaldata[$h + 2][] = Controller::money_format_inr($receipt_amount, 2);

        $finaldata[$h + 3][] = 'Expense';
        $finaldata[$h + 4][] = 'Daybook';
        $i = $h + 5;
        // expenses

        $daybook_expense = 0;
        $expense_amount = 0;
        $expense_daybook = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses LEFT JOIN {$tblpx}projects ON {$tblpx}expenses.projectid = {$tblpx}projects.pid WHERE {$tblpx}expenses.date = '" . $date . "' AND (" . $newQuery3 . ") AND {$tblpx}expenses.type=73 AND {$tblpx}expenses.subcontractor_id IS NULL AND {$tblpx}expenses.invoice_id IS NULL AND (({$tblpx}expenses.reconciliation_status =1 AND {$tblpx}expenses.expense_type =88) OR ({$tblpx}expenses.expense_type =89 OR {$tblpx}expenses.expense_type =103)) GROUP BY {$tblpx}projects.pid ORDER BY {$tblpx}expenses.exp_id DESC")->queryAll();
        if (!empty($expense_daybook)) {
            foreach ($expense_daybook as $key => $value) {
                $project_amount = Yii::app()->db->createCommand("SELECT SUM(paid) as project_amount FROM {$tblpx}expenses WHERE projectid =" . $value['pid'] . " and date='" . $date . "' AND type=73 AND invoice_id IS NULL AND subcontractor_id IS NULL AND ((reconciliation_status =1 AND expense_type =88) OR (expense_type =89 OR expense_type = 103)) ORDER BY exp_id DESC")->queryRow();
                $daybook_expense += $project_amount['project_amount'];
                $finaldata[$i][] = 'Project :' . $value['name'];
                $finaldata[$i][] = Controller::money_format_inr($project_amount['project_amount'], 2);
                $i++;
            }
            $expense_amount += $daybook_expense;
        } else {
            $i = $i;
        }
        $finaldata[$i][] = 'Net Amount';
        $finaldata[$i][] = Controller::money_format_inr($daybook_expense, 2);

        $finaldata[$i + 1][] = 'Daily Expenses';

        $j = $i + 2;
        $dailyexp_expense = 0;
        $receipt_dailyexp = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyexpense WHERE date ='" . $date . "' AND exp_type=73 AND (" . $newQuery2 . ") AND ((expense_type = 88 AND reconciliation_status =1)OR (expense_type = 89)) ORDER BY dailyexp_id DESC")->queryAll();
        if (!empty($receipt_dailyexp)) {
            foreach ($receipt_dailyexp as $key => $value) {
                $dailyexp_expense += $value['dailyexpense_paidamount'];
                $finaldata[$j][] = $value['description'];
                $finaldata[$j][] = Controller::money_format_inr($value['dailyexpense_paidamount'], 2);
                $j++;
            }
            $expense_amount += $dailyexp_expense;
        } else {
            $j = $j;
        }

        $finaldata[$j][] = 'Net Amount';
        $finaldata[$j][] = Controller::money_format_inr($dailyexp_expense, 2);

        $finaldata[$j + 1][] = 'Vendor Payment';
        $k = $j + 2;

        $vendor_expense = 0;
        $expense_vendor = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyvendors LEFT JOIN {$tblpx}projects ON {$tblpx}dailyvendors.project_id = {$tblpx}projects.pid WHERE {$tblpx}dailyvendors.date = '" . $date . "' AND (" . $newQuery4 . ") AND {$tblpx}dailyvendors.project_id IS NOT NULL AND (({$tblpx}dailyvendors.payment_type = 88 AND {$tblpx}dailyvendors.reconciliation_status =1) OR ({$tblpx}dailyvendors.payment_type = 89)) GROUP BY {$tblpx}projects.pid ORDER BY {$tblpx}dailyvendors.daily_v_id DESC")->queryAll();
        if (!empty($expense_vendor)) {
            foreach ($expense_vendor as $key => $value) {
                $project_amount = Yii::app()->db->createCommand("SELECT SUM( IFNULL(tax_amount, 0) + IFNULL(amount, 0) ) as project_amount FROM {$tblpx}dailyvendors WHERE project_id =" . $value['pid'] . " and date='" . $date . "' AND ((payment_type = 88 AND reconciliation_status =1) OR (payment_type = 89)) ORDER BY daily_v_id DESC")->queryRow();
                $vendor_expense += $project_amount['project_amount'];
                $finaldata[$k][] = 'Project :' . $value['name'];
                $finaldata[$k][] = Controller::money_format_inr($project_amount['project_amount'], 2);
                $k++;
            }
            //$expense_amount += $vendor_expense;
        } else {
            $k = $k;
        }

        $finaldata[$k][] = 'Other Expenses';

        $vendor_expense2 = 0;
        $details_2 = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyvendors WHERE project_id IS NULL and date='" . $date . "' AND (" . $newQuery4 . ") AND ((payment_type = 88 AND reconciliation_status =1) OR (payment_type = 89)) ORDER BY daily_v_id DESC")->queryAll();
        if (!empty($details_2)) {
            $n = $k + 1;
            foreach ($details_2 as $key => $values) {
                $vendor_expense2 += $values['amount'] + $values['tax_amount'];
                $finaldata[$n][] = $values['description'];
                $finaldata[$n][] = Controller::money_format_inr($values['amount'] + $values['tax_amount'], 2);
                $n++;
            }
            $p = $n;
            //$expense_amount += $vendor_expense2;
        } else {
            $p = $k;
        }

        $expense_amount += $vendor_expense + $vendor_expense2;

        $finaldata[$p][] = 'Net Amount';
        $finaldata[$p][] = Controller::money_format_inr($vendor_expense, 2);

        $finaldata[$p + 1][] = 'Subcontractor Payment';
        $m = $p + 2;

        $subcontractor_expense = 0;
        $expense_subcontractor = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}subcontractor_payment LEFT JOIN {$tblpx}projects ON {$tblpx}subcontractor_payment.project_id = {$tblpx}projects.pid WHERE {$tblpx}subcontractor_payment.date = '" . $date . "' AND (" . $newQuery5 . ") AND (({$tblpx}subcontractor_payment.payment_type = 88 AND {$tblpx}subcontractor_payment.reconciliation_status = 1) OR ({$tblpx}subcontractor_payment.payment_type = 89)) GROUP BY {$tblpx}projects.pid ORDER BY {$tblpx}subcontractor_payment.payment_id DESC")->queryAll();
        if (!empty($expense_subcontractor)) {
            foreach ($expense_subcontractor as $key => $value) {
                $project_amount = Yii::app()->db->createCommand("SELECT SUM( IFNULL(tax_amount, 0) + IFNULL(amount, 0) ) as project_amount FROM {$tblpx}subcontractor_payment WHERE project_id =" . $value['pid'] . " and date='" . $date . "' AND ((payment_type = 88 AND reconciliation_status = 1) OR (payment_type = 89)) ORDER BY payment_id DESC")->queryRow();
                $subcontractor_expense += $project_amount['project_amount'];
                $finaldata[$m][] = 'Project :' . $value['name'];
                $finaldata[$m][] = Controller::money_format_inr($project_amount['project_amount'], 2);
                $m++;
            }
            $expense_amount += $subcontractor_expense;
        } else {
            $m = $m;
        }

        $finaldata[$m][] = 'Net Amount';
        $finaldata[$m][] = Controller::money_format_inr($subcontractor_expense, 2);
        $finaldata[$m + 1][] = 'Grand Total';
        $finaldata[$m + 1][] = Controller::money_format_inr($expense_amount, 2);
        $finaldata[$m + 2][] = 'Closing Balance';
        $o = $m + 3;
        // closeing blnc

        $current_date = $date;
        $cash_blance = Yii::app()->db->createCommand("SELECT {$tblpx}status.caption,{$tblpx}cashbalance.cashbalance_type,{$tblpx}cashbalance.bank_id,{$tblpx}bank.bank_name,{$tblpx}cashbalance.cashbalance_opening_balance,{$tblpx}cashbalance.cashbalance_deposit,{$tblpx}cashbalance.cashbalance_withdrawal,{$tblpx}cashbalance.cashbalance_closing_balance,{$tblpx}cashbalance.cashbalance_date,{$tblpx}cashbalance.company_id FROM {$tblpx}cashbalance LEFT JOIN {$tblpx}bank ON {$tblpx}cashbalance.bank_id={$tblpx}bank.bank_id LEFT JOIN {$tblpx}status ON {$tblpx}cashbalance.cashbalance_type={$tblpx}status.sid WHERE (" . $newQuery1 . ") ORDER BY bank_id ASC")->queryAll();
        foreach ($cash_blance as $key => $value) {
            if ($value['bank_id'] != '') {
                //recepit
                $daybook =  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=88 AND bank_id=" . $value['bank_id'] . " AND type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND dailyexpense_receipt_type=88 AND bank_id=" . $value['bank_id'] . " AND exp_type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_check = $daybook['total_amount'];
                $dailyexpense_check = $dailyexpense['total_amount'];
                //$deposit = $daybook_check+$dailyexpense_check+$value['cashbalance_opening_balance'];
                $deposit = $daybook_check + $dailyexpense_check;

                //expense
                $daybook_with =  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND expense_type=88 AND bank_id=" . $value['bank_id'] . " AND type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense_with =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND expense_type=88 AND bank_id=" . $value['bank_id'] . " AND exp_type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_with_amount = $daybook_with['total_amount'];
                $dailyexpense_with_amount = $dailyexpense_with['total_amount'];
                $dailyvendors =  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=88  AND bank=" . $value['bank_id'] . " AND company_id=" . $value['company_id'] . "")->queryRow();
                $subcontractor =  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE reconciliation_status = 1 AND reconciliation_date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=88  AND bank=" . $value['bank_id'] . " AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyvendors_check = $dailyvendors['total_amount'];
                $subcontractor_check = $subcontractor['total_amount'];
                $withdrawal = $dailyvendors_check + $daybook_with_amount + $dailyexpense_with_amount;
            } else {
                //receipt
                $daybook_cah =  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=89 AND type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_cash = $daybook_cah['total_amount'];
                //$dailyexpense_cah=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (dailyexpense_receipt_type=89 OR dailyexpense_receipt_type=103) AND exp_type=72 AND company_id=".$value['company_id']."")->queryRow();
                $dailyexpense_cah =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND (dailyexpense_receipt_type=89) AND exp_type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense_cash = $dailyexpense_cah['total_amount'];
                //$deposit = $daybook_cash+ $dailyexpense_cash+$value['cashbalance_opening_balance'];
                $deposit = $daybook_cash + $dailyexpense_cash;
                //echo $current_date;
                //echo $daybook_cash;

                // expense
                $daybook_with =  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND (expense_type=89 OR expense_type = 103) AND type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $daybook_with_amount = $daybook_with['total_amount'];
                //$dailyexpense_with=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '".$value['cashbalance_date']."' AND '".$current_date."' AND (expense_type=89 OR expense_type=103) AND exp_type=73 AND company_id=".$value['company_id']."")->queryRow();
                $dailyexpense_with =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND (expense_type=89) AND exp_type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyexpense_with_amount = $dailyexpense_with['total_amount'];
                $dailyvendors_cah =  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=89 AND company_id=" . $value['company_id'] . "")->queryRow();
                $dailyvendors_cash = $dailyvendors_cah['total_amount'];
                $subcontractor_cah =  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE date BETWEEN '" . $value['cashbalance_date'] . "' AND '" . $current_date . "' AND payment_type=89 AND company_id=" . $value['company_id'] . "")->queryRow();
                $subcontractor_cash = $subcontractor_cah['total_amount'];

                $withdrawal = $dailyvendors_cash + $daybook_with_amount + $dailyexpense_with_amount;
            }
            $company = Company::model()->findByPk($value['company_id']);
            $finaldata[$o][] = ($value['caption'] == 'Bank') ? $value['bank_name'] . ' - ' . $company->name : $value['caption'] . ' - ' . $company->name;
            $finaldata[$o][] = Controller::money_format_inr(($value['cashbalance_opening_balance'] + $deposit) - $withdrawal, 2);
            $o++;
        }

        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'daily_financial_summary_report' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actiondynamicbank()
    {
        $company_id = $_POST['company_id'];
        $tblpx = Yii::app()->db->tablePrefix;
        $html  = array('html' => '', 'status' => '', 'type' => '');
        $html['type'] .= '<option value="">Select Type</option>';
        $expense_type  = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}bank WHERE FIND_IN_SET(" . $company_id . ",company_id)")->queryAll();
        if (!empty($expense_type)) {
            $html['html'] .= '<option value="">Select Bank</option>';
            foreach ($expense_type as $key => $value) {
                $html['html'] .= '<option value="' . $value['bank_id'] . '">' . $value['bank_name'] . '</option>';
                $html['status'] = 'success';
            }
        } else {
            $html['status'] = 'no_success';
            $html['html'] .= '<option value="">Select Bank</option>';
        }
        $paymentData = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "cashbalance WHERE cashbalance_type = 100 AND company_id=" . $company_id . "")->queryAll();

        if (empty($paymentData)) {
            $html['type'] .= '<option value="100">In Hand</option><option value="99">Bank</option>';
        } else {
            $html['type'] .= '<option value="99">Bank</option>';
        }

        echo json_encode($html);
    }

    public function actionCashblance_pdf($date_to, $date_from)
    {
        $this->logo = $this->realpath_logo;
        $tblpx = Yii::app()->db->tablePrefix;
        $newQuery = "";
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}cashbalance.company_id)";
        }
        $cash_blance = Yii::app()->db->createCommand("SELECT {$tblpx}status.caption,{$tblpx}cashbalance.cashbalance_type,{$tblpx}cashbalance.bank_id,{$tblpx}bank.bank_name,{$tblpx}cashbalance.cashbalance_opening_balance,{$tblpx}cashbalance.cashbalance_deposit,{$tblpx}cashbalance.cashbalance_withdrawal,{$tblpx}cashbalance.cashbalance_closing_balance,{$tblpx}cashbalance.cashbalance_date FROM {$tblpx}cashbalance LEFT JOIN {$tblpx}bank ON {$tblpx}cashbalance.bank_id={$tblpx}bank.bank_id LEFT JOIN {$tblpx}status ON {$tblpx}cashbalance.cashbalance_type={$tblpx}status.sid WHERE (" . $newQuery . ") ORDER BY bank_id ASC")->queryAll();
        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
        $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $mPDF1->AddPage('','', '', '', '', 0,0,52, 30, 10,0);
        $mPDF1->WriteHTML($this->renderPartial('cashblance_pdf', array(
            'cash_blance' => $cash_blance,
            'date_to' => $date_to,
            'date_from' => $date_from
        ), true));
        $filename = "Cash blance report";
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    public function actionCashblance_excel($date_to, $date_from)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $arraylabel = array('sl no', 'Payment Name', 'Company', 'Bill No. / Transaction head', 'Vendors', 'Description', 'Payment date', 'Expense Amount', 'Receipt Amounts');
        $finaldata = array();
        $heading = '';
        if (empty($date_from) && empty($date_to)) {
            $heading    .= date('d-m-Y');
        } else {
            if (!empty($date_from) && !empty($date_to)) {
                $heading .= date('d-M-Y', strtotime($date_from)) . ' to ' . date('d-M-Y', strtotime($date_to));
            } else {
                if (!empty($date_from)) {
                    $heading .= date('d-M-Y', strtotime($date_from));
                }
                if (!empty($date_to)) {
                    $heading .= date('d-M-Y', strtotime($date_to));
                }
            }
        }
        $finaldata[0][] = 'Cash In-Hand Report :' . $heading;
        $where = '';
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        if (empty($date_from) && empty($date_to)) {
            $where .= " AND date ='" . date("Y-m-d") . "'";
        } else {
            if (!empty($date_from) && !empty($date_to)) {
                $where .= " AND date between '" . $date_from . "' and'" . $date_to . "'";
            } else {
                if (!empty($date_from)) {
                    $where .= " AND date >= '" . $date_from . "'";
                }
                if (!empty($date_to)) {
                    $where .= " AND date <= '" . $date_to . "'";
                }
            }
        }
        $data_array = array();
        // daybook

        $daybook_data =  Yii::app()->db->createCommand("SELECT * FROM {$tblpx}expenses WHERE (" . $newQuery . ") " . $where . " AND (expense_type=89 OR payment_type=89 OR expense_type =103) AND subcontractor_id IS NULL")->queryAll();
        foreach ($daybook_data as $key => $value) {
            $data_array[$key]['payment_type'] = 'Daybook';
            $data_array[$key]['expense_amount'] = $value['paid'];
            $data_array[$key]['receipt_amount'] = $value['receipt'];
            $data_array[$key]['payment_date'] = $value['date'];
            if ($value['type'] == 73) {
                $vendor = Vendors::model()->findByPk($value['vendor_id']);
                $vendor_name = $vendor['name'];
                if ($value['bill_id'] != NULL) {
                    $bills = Bills::model()->findByPk($value['bill_id']);
                    $expense_head = ExpenseType::model()->findByPk($value['exptype']);
                    $bill_thead  = $bills['bill_number'] . ' / ' . $expense_head['type_name'];
                } else {
                    $expense_head = ExpenseType::model()->findByPk($value['exptype']);
                    $bill_thead  = $expense_head['type_name'];
                }
            } else {
                $vendor_name = '';
                $bill_thead  = '';
            }
            $data_array[$key]['vendor'] = $vendor_name;
            $data_array[$key]['description'] = $value['description'];
            $data_array[$key]['bill_exphead'] = $bill_thead;
            $data_array[$key]['company_id'] = $value['company_id'];
        }

        // dailyexpense
        $dailyexpense_data = array();
        //$dailyexpense_data=  Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyexpense WHERE (".$newQuery.") ".$where." AND (expense_type=89 OR dailyexpense_receipt_type=89 OR expense_type=103 OR dailyexpense_receipt_type=103)")->queryAll();
        $dailyexpense_data =  Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyexpense WHERE (" . $newQuery . ") " . $where . " AND (expense_type=89 OR dailyexpense_receipt_type=89)")->queryAll();
        $a = count($daybook_data);
        foreach ($dailyexpense_data as $key => $value) {
            $data_array[$a]['payment_type'] = 'Dailyexpense';
            $data_array[$a]['expense_amount'] = $value['dailyexpense_paidamount'];
            $data_array[$a]['receipt_amount'] = $value['dailyexpense_receipt'];
            $data_array[$a]['payment_date'] = $value['date'];
            if ($value["dailyexpense_type"] == "deposit") {
                $depositId = $value["expensehead_id"];
                $deposit   = Deposit::model()->findByPk($depositId);
                $expense_head = $deposit->deposit_name;
            } else if ($value["dailyexpense_type"] == "expense") {
                $expense   = Companyexpensetype::model()->findByPk($value['expensehead_id']);
                $expense_head = $expense->name;
            } else {
                $receipt   = Companyexpensetype::model()->findByPk($value['exp_type_id']);
                $expense_head = $receipt->name;
            }
            if ($value['bill_id'] != NULL) {
                $bill_thead  =  $value['bill_id'] . ' / ' . $expense_head;
            } else {
                $bill_thead  = $expense_head;
            }

            $data_array[$a]['vendor'] = '';
            $data_array[$a]['description'] = $value['description'];
            $data_array[$a]['bill_exphead'] = $bill_thead;
            $data_array[$a]['company_id'] = $value['company_id'];
            $a++;
        }

        // Vendor payment
        $vendorpayment_data = array();
        $vendorpayment_data =  Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyvendors WHERE (" . $newQuery . ") " . $where . " AND payment_type=89")->queryAll();
        $b = count($daybook_data) + count($dailyexpense_data);
        foreach ($vendorpayment_data as $key => $value) {
            $data_array[$b]['payment_type'] = 'Vendor  Payment';
            $data_array[$b]['expense_amount'] = $value['amount'] + $value['tax_amount'];
            $data_array[$b]['receipt_amount'] = '';
            $data_array[$b]['payment_date'] = $value['date'];
            $vendor = Vendors::model()->findByPk($value['vendor_id']);
            $vendor_name = $vendor->name;
            $data_array[$b]['vendor'] = $vendor_name;
            $data_array[$b]['description'] = $value['description'];
            $data_array[$b]['bill_exphead'] = '';
            $data_array[$b]['company_id'] = $value['company_id'];
            $b++;
        }

        // Subcontractor payment
        $subcontractorpayment_data = array();
        $subcontractorpayment_data =  Yii::app()->db->createCommand("SELECT * FROM {$tblpx}subcontractor_payment WHERE (" . $newQuery . ") " . $where . " AND payment_type=89 AND approve_status ='Yes'")->queryAll();
        $c = count($daybook_data) + count($dailyexpense_data) + count($vendorpayment_data);
        foreach ($subcontractorpayment_data as $key => $value) {
            $data_array[$c]['payment_type'] = 'Subcontractor  Payment';
            $data_array[$c]['expense_amount'] = $value['amount'] + $value['tax_amount'];
            $data_array[$c]['receipt_amount'] = '';
            $data_array[$c]['payment_date'] = $value['date'];
            $data_array[$c]['vendor'] = '';
            $data_array[$c]['description'] = $value['description'];
            $data_array[$c]['bill_exphead'] = '';
            $data_array[$c]['company_id'] = $value['company_id'];
            $c++;
        }
        $total_expense = 0;
        $total_recept  = 0;
        if (!empty($data_array)) {
            $total_expense  = array_sum(array_column($data_array, 'expense_amount'));
            $total_recept   = array_sum(array_column($data_array, 'receipt_amount'));
        }
        usort($data_array, array($this, 'sortByDate'));

        $h = 1;
        foreach ($data_array as $key => $data) {
            $company = Company::model()->findByPk($data['company_id']);
            $total_expense += $data['expense_amount'];
            $total_recept += $data['receipt_amount'];
            $finaldata[$h][] = $key + 1;
            $finaldata[$h][] = $data['payment_type'];
            $finaldata[$h][] = $company->name;
            $finaldata[$h][] = $data['bill_exphead'];
            $finaldata[$h][] = $data['vendor'];
            $finaldata[$h][] = $data['description'];
            $finaldata[$h][] = date('d-m-Y', strtotime($data['payment_date']));
            $finaldata[$h][] = ($data['expense_amount'] != 0) ? Controller::money_format_inr($data['expense_amount'], 2) : '0';
            $finaldata[$h][] = ($data['receipt_amount'] != 0) ? Controller::money_format_inr($data['receipt_amount'], 2) : '0';

            $h++;
            $t = $h;
        }

        if (empty($data_array)) {
            $t = 0;
        }

        $finaldata[$t][1] = '';
        $finaldata[$t][2] = '';
        $finaldata[$t][3] = '';
        $finaldata[$t][4] = '';
        $finaldata[$t][5] = '';
        $finaldata[$t][6] = '';
        $finaldata[$t][7] = Controller::money_format_inr($total_expense, 2);
        $finaldata[$t][8] = Controller::money_format_inr($total_recept, 2);

        $finaldata[count($data_array)][] = '';
        if ($date_from != '' || $date_to != '') {

            $heading = '';
            if (!empty($date_from) && !empty($date_to)) {
                $heading .= date('d-M-Y', strtotime($date_from)) . ' to ' . date('d-M-Y', strtotime($date_to));
            } else {
                if (!empty($date_from)) {
                    $heading .= date('d-M-Y', strtotime($date_from));
                }
                if (!empty($date_to)) {
                    $heading .= date('d-M-Y', strtotime($date_to));
                }
            }

            $finaldata[count($data_array) + 1][] = 'Balance Report :' . $heading;

            $finaldata[count($data_array) + 2][1] = 'Sl No.';
            $finaldata[count($data_array) + 2][2] = 'Company';
            $finaldata[count($data_array) + 2][3] = 'Cash Balance Type';
            $finaldata[count($data_array) + 2][4] = 'Bank';
            $finaldata[count($data_array) + 2][5] = 'Start Date';
            $finaldata[count($data_array) + 2][6] = 'Opening Balance';
            $finaldata[count($data_array) + 2][7] = 'Expense';
            $finaldata[count($data_array) + 2][8] = 'Receipt';
            $finaldata[count($data_array) + 2][9] = 'Closing Balance';

            $k = count($data_array) + 3;
            $newQuery = "";
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            foreach ($arrVal as $arr) {
                if ($newQuery) $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', {$tblpx}cashbalance.company_id)";
            }
            $cash_blance = Yii::app()->db->createCommand("SELECT {$tblpx}status.caption,{$tblpx}cashbalance.cashbalance_type,{$tblpx}cashbalance.bank_id,{$tblpx}bank.bank_name,{$tblpx}cashbalance.cashbalance_opening_balance,{$tblpx}cashbalance.cashbalance_deposit,{$tblpx}cashbalance.cashbalance_withdrawal,{$tblpx}cashbalance.cashbalance_closing_balance,{$tblpx}cashbalance.cashbalance_date,{$tblpx}cashbalance.company_id FROM {$tblpx}cashbalance LEFT JOIN {$tblpx}bank ON {$tblpx}cashbalance.bank_id={$tblpx}bank.bank_id LEFT JOIN {$tblpx}status ON {$tblpx}cashbalance.cashbalance_type={$tblpx}status.sid WHERE (" . $newQuery . ") ORDER BY bank_id ASC")->queryAll();
            if (!empty($cash_blance)) {
                foreach ($cash_blance as $key => $value) {
                    $current_date = date("Y-m-d");
                    $tblpx = Yii::app()->db->tablePrefix;
                    $where_bank = '';
                    $where_hand = '';
                    $where_main = '';
                    $opening_hand = '';
                    $opening_bank = '';
                    if (!empty($date_from) && !empty($date_to)) {
                        $opening_date = date('Y-m-d', strtotime('-1 day', strtotime($date_from)));
                        $where_main .= " AND {$tblpx}cashbalance.cashbalance_date between '" . $date_from . "' and'" . $date_to . "'";
                        $where_bank .= " AND reconciliation_date between '" . $date_from . "' and'" . $date_to . "'";
                        $where_hand .= " AND date between '" . $date_from . "' and'" . $date_to . "'";
                        $opening_hand .= " AND date <= '" . $opening_date . "'";
                        $opening_bank .= " AND reconciliation_date <= '" . $opening_date . "'";
                    } else {
                        if (!empty($date_from)) {
                            $opening_date = date('Y-m-d', strtotime('-1 day', strtotime($date_from)));
                            $date_from = date('Y-m-d', strtotime($date_from));
                            $where_main .= " AND {$tblpx}cashbalance.cashbalance_date >= '" . $date_from . "'";
                            $where_bank .= " AND reconciliation_date between '" . $date_from . "' and'" . $current_date . "'";
                            $where_hand .= " AND date between '" . $date_from . "' and'" . $current_date . "'";
                            $opening_hand .= " AND date <= '" . $opening_date . "'";
                            $opening_bank .= " AND reconciliation_date <= '" . $opening_date . "'";
                        }
                        if (!empty($date_to)) {
                            $opening_date = date('Y-m-d', strtotime('-1 day', strtotime($date_to)));
                            $date_to = date('Y-m-d', strtotime($date_to));
                            $where_main .= " AND {$tblpx}cashbalance.cashbalance_date <= '" . $date_to . "'";
                            $where_bank .= " AND reconciliation_date between '" . $value['cashbalance_date'] . "' and'" . $date_to . "'";
                            $where_hand .= " AND date between '" . $value['cashbalance_date'] . "' and'" . $date_to . "'";
                            $opening_hand .= " AND date <= '" . $opening_date . "'";
                            $opening_bank .= " AND reconciliation_date <= '" . $opening_date . "'";
                        }
                    }

                    if ($value['bank_id'] != '') {
                        // deposit

                        $daybook = Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 " . $where_bank . " AND payment_type=88 AND bank_id=" . $value['bank_id'] . " AND type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                        $dailyexpense = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 " . $where_bank . " AND dailyexpense_receipt_type=88 AND bank_id=" . $value['bank_id'] . " AND exp_type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                        $daybook_check = $daybook['total_amount'];
                        $dailyexpense_check = $dailyexpense['total_amount'];
                        //$deposit = $daybook_check+$dailyexpense_check+$value['cashbalance_deposit'];
                        $deposit = $daybook_check + $dailyexpense_check;


                        //withdrawal
                        $daybook_with = Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 " . $where_bank . " AND expense_type=88 AND bank_id=" . $value['bank_id'] . " AND type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                        $dailyexpense_with = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 " . $where_bank . " AND expense_type=88 AND bank_id=" . $value['bank_id'] . " AND exp_type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                        $daybook_with_amount = $daybook_with['total_amount'];
                        $dailyexpense_with_amount = $dailyexpense_with['total_amount'];

                        $dailyvendors = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE reconciliation_status = 1 " . $where_bank . " AND payment_type=88  AND bank=" . $value['bank_id'] . " AND company_id=" . $value['company_id'] . "")->queryRow();
                        $subcontractor = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE reconciliation_status = 1 " . $where_bank . " AND payment_type=88  AND bank=" . $value['bank_id'] . " AND company_id=" . $value['company_id'] . "")->queryRow();

                        $dailyvendors_check = $dailyvendors['total_amount'];
                        $subcontractor_check = $subcontractor['total_amount'];

                        //$withdrawal = $dailyvendors_check+$subcontractor_check+$daybook_with_amount+$dailyexpense_with_amount+$value['cashbalance_withdrawal'];
                        //$withdrawal = $dailyvendors_check+$subcontractor_check+$daybook_with_amount+$dailyexpense_with_amount;
                        $withdrawal = $dailyvendors_check + $daybook_with_amount + $dailyexpense_with_amount;

                        // opening balance calculation

                        // deposit
                        $daybook_opening =  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 " . $opening_bank . " AND payment_type=88 AND bank_id=" . $value['bank_id'] . " AND type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                        $dailyexpense_opening =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 " . $opening_bank . " AND dailyexpense_receipt_type=88 AND bank_id=" . $value['bank_id'] . " AND exp_type=72 AND company_id=" . $value['company_id'] . "")->queryRow();
                        $daybook_check_opening = $daybook_opening['total_amount'];
                        $dailyexpense_check_opening = $dailyexpense_opening['total_amount'];
                        $deposit_opening = $daybook_check_opening + $dailyexpense_check_opening;

                        //                        if($deposit_opening ==0){
                        //                             $deposit_opening = $value['cashbalance_opening_balance'];
                        //                         }

                        //withdrawal
                        $daybook_with_opening =  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE reconciliation_status = 1 " . $opening_bank . " AND expense_type=88 AND bank_id=" . $value['bank_id'] . " AND type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                        $dailyexpense_with_opening =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE reconciliation_status = 1 " . $opening_bank . " AND expense_type=88 AND bank_id=" . $value['bank_id'] . " AND exp_type=73 AND company_id=" . $value['company_id'] . "")->queryRow();
                        $daybook_with_amount_opening = $daybook_with_opening['total_amount'];
                        $dailyexpense_with_amount_opening = $dailyexpense_with_opening['total_amount'];

                        $dailyvendors_opening =  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE reconciliation_status = 1 " . $opening_bank . " AND payment_type=88  AND bank=" . $value['bank_id'] . " AND company_id=" . $value['company_id'] . "")->queryRow();

                        $dailyvendors_check_opening = $dailyvendors_opening['total_amount'];

                        $withdrawal_opening = $dailyvendors_check_opening + $daybook_with_amount_opening + $dailyexpense_with_amount_opening;

                        $opening_balance = ($deposit_opening + $value['cashbalance_opening_balance']) - $withdrawal_opening;
                    } else {

                        //deposit

                        $daybook_cah = Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE payment_type=89 AND type=72 " . $where_hand . " AND company_id=" . $value['company_id'] . "")->queryRow();
                        $daybook_cash = $daybook_cah['total_amount'];
                        //$dailyexpense_cah = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE (dailyexpense_receipt_type=89 OR dailyexpense_receipt_type=103) AND exp_type=72 " . $where_hand . " AND company_id=" . $value['company_id'] . "")->queryRow();
                        $dailyexpense_cah = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE (dailyexpense_receipt_type=89) AND exp_type=72 " . $where_hand . " AND company_id=" . $value['company_id'] . "")->queryRow();
                        $dailyexpense_cash = $dailyexpense_cah['total_amount'];
                        //$deposit = $daybook_cash+ $dailyexpense_cash+$value['cashbalance_deposit'];
                        $deposit = $daybook_cash + $dailyexpense_cash;

                        // withdrawal

                        $daybook_with = Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE (expense_type=89 OR expense_type =103) AND type=73 " . $where_hand . " AND company_id=" . $value['company_id'] . "")->queryRow();
                        $daybook_with_amount = $daybook_with['total_amount'];
                        //$dailyexpense_with = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE (expense_type=89 OR expense_type=103) AND exp_type=73 " . $where_hand . " AND company_id=" . $value['company_id'] . "")->queryRow();
                        $dailyexpense_with = Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE (expense_type=89) AND exp_type=73 " . $where_hand . " AND company_id=" . $value['company_id'] . "")->queryRow();
                        $dailyexpense_with_amount = $dailyexpense_with['total_amount'];

                        $dailyvendors_cah = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE payment_type=89 " . $where_hand . " AND company_id=" . $value['company_id'] . "")->queryRow();
                        $dailyvendors_cash = $dailyvendors_cah['total_amount'];
                        $subcontractor_cah = Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}subcontractor_payment WHERE payment_type=89 " . $where_hand . " AND company_id=" . $value['company_id'] . "")->queryRow();
                        $subcontractor_cash = $subcontractor_cah['total_amount'];
                        //$withdrawal = $dailyvendors_cash+$subcontractor_cash+$daybook_with_amount+$dailyexpense_with_amount+$value['cashbalance_withdrawal'];
                        $withdrawal = $dailyvendors_cash + $daybook_with_amount + $dailyexpense_with_amount;

                        // opening balance calculation

                        // deposit
                        $daybook_cah_opening =  Yii::app()->db->createCommand("SELECT SUM(receipt) as total_amount FROM {$tblpx}expenses WHERE payment_type=89 AND type=72 " . $opening_hand . " AND company_id=" . $value['company_id'] . "")->queryRow();
                        $daybook_cash_opening = $daybook_cah_opening['total_amount'];
                        //$dailyexpense_cah_opening=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE (dailyexpense_receipt_type=89 OR dailyexpense_receipt_type=103) AND exp_type=72 ".$opening_hand." AND company_id=".$value['company_id']."")->queryRow();
                        $dailyexpense_cah_opening =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_receipt) as total_amount FROM {$tblpx}dailyexpense WHERE (dailyexpense_receipt_type=89) AND exp_type=72 " . $opening_hand . " AND company_id=" . $value['company_id'] . "")->queryRow();
                        $dailyexpense_cash_opening = $dailyexpense_cah_opening['total_amount'];
                        $deposit_opening = $daybook_cash_opening + $dailyexpense_cash_opening;
                        //                        if($deposit_opening ==0){
                        //                            $deposit_opening = $value['cashbalance_opening_balance'];
                        //                        }

                        // withdrawal
                        $daybook_with_opening =  Yii::app()->db->createCommand("SELECT SUM(paid) as total_amount FROM {$tblpx}expenses WHERE (expense_type=89 OR expense_type =103) AND type=73 " . $opening_hand . " AND company_id=" . $value['company_id'] . "")->queryRow();
                        $daybook_with_amount_opening = $daybook_with_opening['total_amount'];
                        //$dailyexpense_with_opening=  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE (expense_type=89 OR expense_type=103) AND exp_type=73 ".$opening_hand." AND company_id=".$value['company_id']."")->queryRow();
                        $dailyexpense_with_opening =  Yii::app()->db->createCommand("SELECT SUM(dailyexpense_paidamount) as total_amount FROM {$tblpx}dailyexpense WHERE (expense_type=89) AND exp_type=73 " . $opening_hand . " AND company_id=" . $value['company_id'] . "")->queryRow();
                        $dailyexpense_with_amount_opening = $dailyexpense_with_opening['total_amount'];


                        $dailyvendors_cah_opening =  Yii::app()->db->createCommand("SELECT SUM( IFNULL(amount, 0) + IFNULL(tax_amount, 0) ) as total_amount FROM {$tblpx}dailyvendors WHERE payment_type=89 " . $opening_hand . " AND company_id=" . $value['company_id'] . "")->queryRow();
                        $dailyvendors_cash_opening = $dailyvendors_cah_opening['total_amount'];
                        $withdrawal_opening = $dailyvendors_cash_opening + $daybook_with_amount_opening + $dailyexpense_with_amount_opening;

                        $opening_balance = ($deposit_opening + $value['cashbalance_opening_balance']) - $withdrawal_opening;
                    }
                    $company = Company::model()->findByPk($value['company_id']);
                    $finaldata[$k][] = $key + 1;
                    $finaldata[$k][] = $company->name;
                    $finaldata[$k][] = $value['caption'];
                    $finaldata[$k][] = $value['bank_name'];
                    $finaldata[$k][] = $value['cashbalance_date'];
                    $finaldata[$k][] = Controller::money_format_inr($opening_balance, 2);
                    $finaldata[$k][] = Controller::money_format_inr($withdrawal, 2);
                    $finaldata[$k][] = Controller::money_format_inr($deposit, 2);
                    $finaldata[$k][] = number_format(($opening_balance + $deposit) - $withdrawal, 2);
                    $k++;
                }
            }
        }


        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'cash_blance_report' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }
    private function sortByDate($a, $b)
    {
        $a = $a['payment_date'];
        $b = $b['payment_date'];
        if ($a == $b) return 0;
        return ($a > $b) ? -1 : 1;
    }
}
