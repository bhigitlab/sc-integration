<?php
/* @var $this SubcontractorbillController */
/* @var $model Subcontractorbill */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'subcontractorbill-form',        
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    )); ?>

    <?php
    $user       = Users::model()->findByPk(Yii::app()->user->id);
    $arrVal     = explode(',', $user->company_id);
    $newQuery   = "";
    foreach ($arrVal as $arr) {
        if ($newQuery) $newQuery .= ' OR';
        $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
    }
    ?>
    <!--ui change-->
    <div class="row">
        
            <div class="form-group col-xs-12 col-md-3 flexchanges">
                <?php echo $form->labelEx($model, 'company_id'); ?>
                <?php echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array(
                    'select' => array('id, name'),
                    "condition" => '(' . $newQuery . ')',
                    'order' => 'id',
                    'distinct' => true
                )), 'id', 'name'), array('class' => 'form-control company_id', 'empty' => '-Select Company-')); ?>
                <?php echo $form->error($model, 'company_id'); ?>
            </div>
        
        
            <div class="form-group col-xs-12 col-md-3 flexchanges">
                <label for="Subcontractorbill_project" class="required projectlabel">Project <span class="required">*</span></label>
                <select class="form-control" id="project_id">
                    <option value="">Select Project</option>
                </select>
                <div class="errorMessage" id="projectErrorMsg"></div>
            </div>
        
       
            <div class="form-group col-xs-12 col-md-3 flexchanges">
                <label for="Subcontractorbill_subcontractor" class="required subcontractorlabel">Subcontractor <span class="required">*</span></label>
                <select class="form-control" id="subcontractor_id">
                    <option value="">Select Subcontractor</option>
                </select>
                <div class="errorMessage" id="subcontractorErrorMsg"></div>
            </div>
        
        
            <div class="form-group col-xs-12 col-md-3 flexchanges">
                <?php echo $form->labelEx($model, 'scquotation_id'); ?>
                <select class="form-control" name="Subcontractorbill[scquotation_id]" id="Subcontractorbill_scquotation_id">
                    <option value="">Select Quotation</option>
                </select>
                <?php echo $form->error($model, 'scquotation_id'); ?>
            </div>
        
        
            <div class="form-group col-xs-12 col-md-3">
                <?php echo $form->labelEx($model, 'stage_id'); ?>
                <select class="form-control" name="Subcontractorbill[stage_id]" id="Subcontractorbill_stage_id">
                    <option value="">Select Stage</option>
                </select>
                <?php echo $form->error($model, 'stage_id'); ?>
            </div>
        
        
            <div class="form-grouform-group col-xs-12 col-md-3 " >
                <?php echo $form->labelEx($model, 'bill_number'); ?>
                <?php echo $form->textField($model, 'bill_number', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'bill_number', array('class' => 'rel-err')); ?>

            </div>
       
       
            <div class="form-group col-xs-12 col-md-3">
                <?php echo $form->labelEx($model, 'date'); ?>
                <?php echo $form->textField($model, 'date', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                <?php echo $form->error($model, 'date', array('class' => 'rel-err')); ?>
            </div>
        
        <div class="form-group col-xs-12 col-md-3">
                                       
                <?php                         
                echo $form->labelEx($model,'total_amount'); ?> 
                <?php echo $form->textField($model, 'total_amount', array('class' => 'form-control total_amount ', 'autocomplete' => 'off','readonly'=>'readonly')); ?>                                                  
           
        </div>
        <div class="form-group col-xs-12 col-md-3">
           
                <?php echo $form->labelEx($model, 'remarks'); ?>
                <?php echo $form->textArea($model, 'remarks', array('size' => 60, 'maxlength' => 200, 'class' => 'form-control vresizing')); ?>
                <?php echo $form->error($model, 'remarks'); ?>
           
        </div>
        <div class="form-group col-xs-12 col-md-3">
            
                <?php echo $form->labelEx($model, 'requested_by'); ?>
                <?php echo $form->textField($model, 'requested_by', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control ')); ?>
                <?php echo $form->error($model, 'requested_by'); ?>
           
        </div>
    
    </div>
     <!--ui change ends-->
   
        
   
    
    <div class="row">
        <div class="form-group col-xs-12 text-right">
           
                <label for="Subcontractorbill_button">&nbsp;</label>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary', 'id' => 'submitButton')); ?>
                <input type="reset" value="Reset" class="btn btn-default" id="reset" />
                <?php //echo CHtml::ResetButton('Reset', array('style'=>'margin-right:3px;','class'=>'btn btn-default')); 
                ?>
            
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">
    $(document).ready(function() {
        $("#Subcontractorbill_company_id").select2();
        $("#project_id").select2();
        $("#subcontractor_id").select2();
        $("#Subcontractorbill_scquotation_id").select2();
        $("#Subcontractorbillitem_quotationitem_id").select2();
        $(function() {
            $("#Subcontractorbill_date").datepicker({
                dateFormat: 'dd-mm-yy'
            });
        });
        $("#Subcontractorbill_company_id").change(function() {
            $('#loading').show();
            var company_id = $(this).val();
            $.ajax({
                url: "<?php echo $this->createUrl('subcontractorbill/getprojectbycompany') ?>",
                data: {
                    "company_id": company_id
                },
                type: "POST",
                success: function(data) {
                    $("#project_id").html(data);
                }
            });
        });
        $("#project_id").change(function() {
            $('#loading').show();
            var project_id = $(this).val();
            var company_id = $("#Subcontractorbill_company_id").val();
            $.ajax({
                url: "<?php echo $this->createUrl('subcontractorbill/getsubcontractorbyproject') ?>",
                data: {
                    "company_id": company_id,
                    "project_id": project_id
                },
                type: "POST",
                success: function(data) {
                    $("#subcontractor_id").html(data);
                }
            });
        });
        $("#subcontractor_id").change(function() {
            $('#loading').show();
            var subcontractor_id = $(this).val();
            var project_id = $("#project_id").val();
            var company_id = $("#Subcontractorbill_company_id").val();
            $.ajax({
                url: "<?php echo $this->createUrl('subcontractorbill/getquotationsbyproject') ?>",
                data: {
                    "company_id": company_id,
                    "project_id": project_id,
                    "subcontractor_id": subcontractor_id
                },
                type: "POST",
                success: function(data) {
                    $("#Subcontractorbill_scquotation_id").html(data);
                }
            });
        });

        $("#Subcontractorbill_scquotation_id").change(function() {
            $('#loading').show();
            //alert('hi');
            var quotation_id = $("#Subcontractorbill_scquotation_id").val();
            var subcontractor_id = $("#subcontractor_id").val();
            var project_id = $("#project_id").val();
            var company_id = $("#Subcontractorbill_company_id").val();
            $.ajax({
                url: "<?php echo $this->createUrl('subcontractorbill/getpaymentstagebyquotation') ?>",
                data: {
                    "company_id": company_id,
                    "project_id": project_id,
                    "subcontractor_id": subcontractor_id,
                    "quotation_id":quotation_id
                },
                type: "POST",
                dataType:"json",
                success: function(data) {
                    $("#Subcontractorbill_stage_id").html(data.stageData);
                    $("#Subcontractorbill_bill_number").val(data.bill_number);
                    if(data.bill_number != ''){
                        $("#Subcontractorbill_bill_number").attr('title',data.bill_number);
                    $("#Subcontractorbill_bill_number").attr('readonly','readonly');
                    }
                    
                }
            });
        });

        $("#Subcontractorbill_bill_number").change(function() {
            $('#loading').show();
            var bill_number = $("#Subcontractorbill_bill_number").val();
           $.ajax({
                url: "<?php echo $this->createUrl('subcontractorbill/billnumberexistance') ?>",
                data: {
                    "bill_number": bill_number,
                },
                type: "POST",
                dataType:"json",
                success: function(data) {
                    if(data>0){
                        if (confirm("Bill number already exist.Do you want to continue?")) {
                            $("#Subcontractorbill_bill_number").val(bill_number);
                        }else{
                            $("#Subcontractorbill_bill_number").val('');
                        }
                    }
                   
                }
            });
        });

        $("#Subcontractorbill_stage_id").change(function() {
            $('#loading').show();
            var quotation_id = $("#Subcontractorbill_scquotation_id").val();
            var stage_id = $(this).val();
            
            $.ajax({
                url: "<?php echo $this->createUrl('subcontractorbill/GetpaymentstageAmount') ?>",
                data: {
                    "stage_id": stage_id,                    
                    "quotation_id":quotation_id
                },
                type: "POST",
                success: function(data) {
                    $(".total_amount").val(data);
                }
            });
        });

        $("#submitButton").click(function() {
            var project_id = $("#project_id").val();
            var subcontractor_id = $("#subcontractor_id").val();
            if (project_id == "") {
                $("#projectErrorMsg").text("Project cannot be blank.");
                $(".projectlabel").css("color", "red");
            }
            if (subcontractor_id == "") {
                $("#subcontractorErrorMsg").text("Subcontractor cannot be blank.");
                $(".subcontractorlabel").css("color", "red");
            }
        });
    });
    $('#reset').on('click', function() { alert('here');
        $("#Subcontractorbill_company_id").val('').trigger('change');
        $("#project_id").val('').trigger('change');
        $("#subcontractor_id").val('').trigger('change');
        $("#Subcontractorbill_scquotation_id").val('').trigger('change');
        $("#Subcontractorbillitem_quotationitem_id").val('').trigger('change');
        $("#Subcontractorbill_remarks").val('').trigger('change');
    });
    $(document).ajaxComplete(function() {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
</script>