<?php
/* @var $this ProjectsController */
/* @var $data Projects */
?>


<?php if ($index == 0) { ?>
    <thead class="entry-table">
        <tr>
            <th>Sl No.</th>
            <th>Name</th>
            <th>Place</th>
            <th>In-charge</th>
            <th>Status</th>
            <th>Address</th>
            <th>ASSIGN TO</th>
            <th>Action</th>            
        </tr>
    </thead>
<?php } ?>
<tr>
    <td><?php echo $index + 1; ?></td>
    <td><?php echo CHtml::encode($data->warehouse_name); ?></td>
    <td><?php echo CHtml::encode($data->warehouse_place); ?></td>
    <td><?php echo CHtml::encode($data->incharge['first_name'] . " " . $data->incharge['last_name']); ?></td>
    <td><?php echo CHtml::encode(($data->status == 1) ? 'Active' : 'Inactive'); ?></td>
    <td><?php echo CHtml::encode($data->warehouse_address); ?></td>
    <td><?php

        $assigned_to_arr = array();
        $assigned_to_ids = '';
        if ($data->assigned_to != '') {
            $assigned_to_ids = explode(",", $data->assigned_to);
            foreach ($assigned_to_ids as $value) {
                $assigned_user = $data->assignedUser($value);
                array_push($assigned_to_arr, $assigned_user);
            }
        } else {
            $assigned_to_ids = '';
            $assigned_to_arr = array();
        }
        if (!empty($assigned_to_arr)) {
            $assigned_to_users = implode(",", $assigned_to_arr);
        } else {
            $assigned_to_users = '';
        }
        echo CHtml::encode($assigned_to_users); ?></td>
        <td style="width:40px;">
         <?php
            if ((isset(Yii::app()->user->role) && (in_array('/wh/warehouse/update', Yii::app()->user->menuauthlist)))) {
            ?>
            <a data-id="<?php echo $data->warehouse_id; ?>" data-id="<?php echo $data->warehouse_id; ?>" data-target=".edit" class="fa fa-edit editWarehouse"></a>
            <?php
            }
            if ((isset(Yii::app()->user->role) && (in_array('/wh/warehouse/delete', Yii::app()->user->menuauthlist)))) {
            ?>
                <a data-id="<?php echo $data->warehouse_id; ?>" data-id="<?php echo $data->warehouse_id; ?>" class="fa fa-trash deleteWarehouse"></a>
            <?php } ?>
        </td>
    
</tr>