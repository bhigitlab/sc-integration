<?php
/* @var $this ProjectTypeController */
/* @var $data ProjectType */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ptid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ptid), array('view', 'id'=>$data->ptid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('project_type')); ?>:</b>
	<?php echo CHtml::encode($data->project_type); ?>
	<br />


</div>