<?php
/* @var $this ProjectsController */
/* @var $model Projects */

$this->breadcrumbs = array(
    'Projects' => array('index'),
    $model->name,
);

$this->menu = array(
    array('label' => 'List Projects', 'url' => array('index')),
    array('label' => 'Create Projects', 'url' => array('create')),
    array('label' => 'Update Projects', 'url' => array('update', 'id' => $model->pid)),
    array('label' => 'Delete Projects', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->pid), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Projects', 'url' => array('admin')),
);
?>

<h1>View Project : <?php echo $model->name; ?></h1>

<?php

$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(

        'name',
        array(
            'name' => 'status',
            'type' => 'raw',
            'value' => $model->status0->caption,
        ),
        'description',
        'created_date',
        array(
            'name' => 'created_by',
            'type' => 'raw',
            'value' => $model->createdBy->first_name,
        ),
		//////created by arun on 14-10-16 starting from here
		array(
            'name' => 'Assigned To',
            'type' => 'raw',
			'value' =>$model->getAssignedusers($model->pid)
            
        ),
		//////created by arun on 14-10-16 ends here
    ),
));
?>
<br/><br/>
<h2>Expenses of this project</h2>
<?php

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'expenses-grid',
    'dataProvider' => $expmodel->search(0,$model->pid),
    'filter' => $expmodel,
    'columns' => array(
        array('class' => 'IndexColumn', 'header' => 'Sl.No.', 'htmlOptions' =>array('style'=>"width:50px;"),),
         array('name'=>'date',
            'value'=>'$data->date',
             'htmlOptions' =>array('style'=>"width:80px;"),
            ),
        array(
            'name' => 'userid',
            'value' => 'Users::model()->findByAttributes(array(\'userid\' => $data->userid))->first_name;',
            'type' => 'raw',
            'htmlOptions' =>array('style'=>"width:100px;"),
             'filter' => CHtml::listData(Users::model()->findAll(
                            array(
                                'select' => array('userid,first_name'),
                                //'condition' => 'status_type="expense"',
                                'order' => 'first_name',
                                'distinct' => true
                    )), "userid", "first_name"),
        ),
         array('name' => 'amount',
            'value' => '"&#8377; ".$data->amount',
             'type' => 'raw',
            'htmlOptions' =>array('style'=>"width:100px;font-weight:bold;"),
        ),
        array(
            'name' => 'type',
            'value' => '"<span style=\"font-weight:bold;color:".($data->type==72?\'Green\':\'red\')."\">".$data->status0->caption."</span>"',
            'type' => 'raw',
            'filter' => CHtml::listData(Status::model()->findAll(
                            array(
                                'select' => array('sid,caption'),
                                'condition' => 'status_type="expense"',
                                'order' => 'status_type',
                                'distinct' => true
                    )), "sid", "caption"),
             'htmlOptions' =>array('style'=>"width:80px;"),
        ),        
//        array(
//            'name' => 'projectid',
//            'value' => '$data->project->name',
//            'type' => 'raw',
//            'filter' => CHtml::listData(Projects::model()->findAll(
//                            array(
//                                'select' => array('pid,name'),
//                                "condition"=> 'pid in (select projectid from project_assign where userid='.Yii::app()->user->id.')',
//                                'order' => 'name',
//                                'distinct' => true
//                    )), "pid", "name"),
//            'htmlOptions' =>array('style'=>"width:300px;"),
//        ),
        'description',
       
       
         array(
            'name' => 'created_by',
            'value' => '$data->createdBy->first_name',
            'type' => 'raw',
            'htmlOptions' =>array('style'=>"width:100px;"),
        ),
         array(
            'name' => 'created_date',
            'value' => '$data->created_date',
            'type' => 'raw',
            'htmlOptions' =>array('style'=>"width:100px;"),
        ),
         /* 'updated_by',
          'updated_date',
         */
       
    ),
));
?>



<style type="text/css">
    th.button-column{
        width:120px !important;
    }    
</style>



