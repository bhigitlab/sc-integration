<?php

/**
 * This is the model class for table "{{dailyexpense_bill}}".
 *
 * The followings are the available columns in table '{{dailyexpense_bill}}':
 * @property integer $id
 * @property string $bill_no
 * @property integer $company_id
 * @property integer $transaction_head
 * @property double $amount
 * @property double $sgst
 * @property double $cgst
 * @property double $igst
 * @property double $total_amount
 * @property string $due_date
 * @property integer $created_by
 * @property string $created_date
 * @property string $description
 */
class DailyexpenseBill extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{dailyexpense_bill}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('bill_no, company_id, transaction_head,transaction_head_type, amount, total_amount, due_date, created_by, created_date', 'required'),
            array('company_id, transaction_head,transaction_head_type, created_by', 'numerical', 'integerOnly'=>true),
            array('amount, sgst, cgst, igst, total_amount', 'numerical'),
            array('bill_no', 'length', 'max'=>50),
            array('description', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, bill_no, company_id, transaction_head, amount, sgst, cgst, igst, total_amount, due_date, created_by, created_date, description', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'bill_no' => 'Bill No',
            'company_id' => 'Company',
            'transaction_head' => 'Transaction Head',
            'amount' => 'Amount',
            'sgst' => 'Sgst',
            'cgst' => 'Cgst',
            'igst' => 'Igst',
            'total_amount' => 'Total Amount',
            'due_date' => 'Due Date',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'description' => 'Description',
            'transaction_head_type' => 'Transaction Head Type',
            
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('bill_no',$this->bill_no,true);
        $criteria->compare('company_id',$this->company_id);
        $criteria->compare('transaction_head',$this->transaction_head);
        $criteria->compare('amount',$this->amount);
        $criteria->compare('sgst',$this->sgst);
        $criteria->compare('cgst',$this->cgst);
        $criteria->compare('igst',$this->igst);
        $criteria->compare('total_amount',$this->total_amount);
        $criteria->compare('due_date',$this->due_date,true);
        $criteria->compare('created_by',$this->created_by);
        $criteria->compare('created_date',$this->created_date,true);
        $criteria->compare('description',$this->description,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return DailyexpenseBill the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
