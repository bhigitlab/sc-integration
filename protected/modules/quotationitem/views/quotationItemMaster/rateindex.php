<div class="container">

<h2>Quotation Item Rate Master</h2>
    <?php if (Yii::app()->user->hasFlash('error')) : ?>
        <div class="alert alert-warning">
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
    <?php endif; ?>
    <?php if (Yii::app()->user->hasFlash('success')) : ?>
        <div class="alert alert-success">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>
    <div class="block_hold shdow_box">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'quotation-item-rate-master-rateindex-form',    
            'enableAjaxValidation'=>false,            
        )); ?>
        <div class="row">
            <?php $items = Yii::app()->controller->GetItemCategory();
            ?>
            <div class="col-md-4">
                <?php echo $form->labelEx($model,'master_category_id'); ?>
                <?php echo $form->dropDownList($model, 'master_category_id', CHtml::listData(QuotationCategoryMaster::model()->findAll(), 'id', 'name'), array('class' => 'form-control js-example-basic-single field_change require', 'empty' => '-Select Category-', 'style' => 'width:100%')); ?>
                
                <?php echo $form->error($model,'master_category_id'); ?>
            </div>

            <div class="col-md-4">
                <?php echo $form->labelEx($model,'material_id'); ?>
                <?php echo $form->dropDownList($model, 'material_id', CHtml::listData($items, 'id', 'data'), array('class' => 'form-control js-example-basic-multiple require material', 'empty' => '-Select material-', 'style' => 'width:100%')); ?>
                <?php //echo $form->textField($model,'material_id',array('class'=>'form-control')); ?>
                <?php echo $form->error($model,'material_id'); ?>
            </div>

            <div class="col-md-2">
                <?php echo $form->labelEx($model,'rate'); ?>
                <?php echo $form->textField($model,'rate',array('class'=>'form-control')); ?>
                <?php echo $form->error($model,'rate'); ?>
            </div>

            <div class="col-md-2">
                <label class="d-block">&nbsp;</label>
                <?php echo CHtml::submitButton('Submit'); ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
        <br>
    </div>
<br>
    <?php
// Create a data provider with pagination
$dataProvider = new CActiveDataProvider('QuotationItemRateMaster', array(
    'pagination' => array(
        'pageSize' => 10,  // Adjust the number of rows per page as needed
    ),
));

$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'viewData' => array('row' => 0),
    'itemView' => '_ratemasterlist', // Create this partial view for each row
    'template' => '<table class="table">
                    <thead>
                        <tr>
                            <th>SI No</th>
                            <th>Master Category</th>
                            <th>Material</th>
                            <th>Rate</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>{items}</tbody>
                   </table>
                   <div class="text-center">{pager}</div>',  // Pagination
    'pager' => array(
        'class' => 'CLinkPager',
        'header' => '',
        'prevPageLabel' => '&laquo; Previous',
        'nextPageLabel' => 'Next &raquo;',
        'firstPageLabel' => 'First',
        'lastPageLabel' => 'Last',
        'maxButtonCount' => 5,  // Adjust the number of page buttons
    ),
));
?>



</div>

<script>    
    $(document).on("click",".btn_ratedelete",function(){
        var id = $(this).attr("id");
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            $.ajax({
                method: "POST",                
                data: {
                    id: id
                },                
                url: '<?php echo Yii::app()->createAbsoluteUrl("/quotationitem/quotationItemMaster/removeRateMaster"); ?>',
                success: function(response) {                    
                    setTimeout(function () {
                    location.reload(true);
                    },500);
                }
            })
        }
    })

    $('.alert').fadeOut(2000);

</script>