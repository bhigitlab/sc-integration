<?php
// Fetch used quantity dynamically
$used_qty_sql = "
    SELECT item_qty FROM `pms_acc_wpr_item_consumed`
    WHERE `warehouse_id` = :warehouse_id
    AND `item_id` = :item_id
    AND `item_rate` = :rate
";
$used_qty_in_pms = Yii::app()->db->createCommand($used_qty_sql)
    ->queryRow(true, [
        ":warehouse_id" => $data->warehouse_id,
        ":item_id" => $data->Item_Id,
        ":rate" => $data->rate,
    ]);

$used_qty = !empty($used_qty_in_pms) ? $used_qty_in_pms['item_qty'] : 0;

// Determine approval status class
$status_classes = [
    0 => 'bg-warning', // Pending
    1 => 'bg-default', // Approved
    2 => 'bg-danger',  // Rejected
];
$approve_status = isset($status_classes[$data->approval_status]) ? $status_classes[$data->approval_status] : '';

// Determine color class based on stock difference
$color = (($data->wh_receipt_quantity - $used_qty) > $data->stock_quantity) ? 'text-danger' : 'text-success';
?>

<tr class="<?php echo $approve_status; ?>">
    <td>
        <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
        <div class="popover-content hide">
            <ul class="tooltip-hiden">
                <?php if ($data->approval_status != 1) { ?>
                    <li><a class="btn btn-xs btn-default approve_btn" data-id="<?php echo $data->id ?>">Approve</a></li>
                <?php } ?>
                <?php if ($data->approval_status != 2) { ?>
                    <li><a class="btn btn-xs btn-default btn_reject" data-id="<?php echo $data->id ?>">Reject</a></li>
                <?php } ?>
            </ul>                    
        </div>
    </td>
    <td><?php echo $index + 1; ?></td>
    <td><?php echo Warehouse::model()->findByPk($data->warehouse_id)->warehouse_name; ?></td>
    <td>
        <?php
        $spec_query = "
            SELECT s.specification, c.category_name, b.brand_name 
            FROM " . $this->tableNameAcc('specification', 0) . " s
            LEFT JOIN " . $this->tableNameAcc('purchase_category', 0) . " c ON s.cat_id = c.id
            LEFT JOIN " . $this->tableNameAcc('brand', 0) . " b ON s.brand_id = b.id
            WHERE s.id = :item_id
        ";
        $specification = Yii::app()->db->createCommand($spec_query)
            ->queryRow(true, [":item_id" => $data->Item_Id]);

        echo $specification['category_name'] . ' - ' . $specification['specification'] . (!empty($specification['brand_name']) ? ' (' . $specification['brand_name'] . ')' : '');
        ?>
    </td>
    <td class="<?php echo $color; ?> text-highlight">
        <?php
        // Fetch latest stock quantity
        $latest_stock_sql = "
            SELECT stock_quantity 
            FROM jp_warehouse_stock_correction
            WHERE warehouse_id = :warehouse_id
            AND Item_Id = :item_id
            AND rate = :rate
            AND id = :id
            AND approval_status = 1
        ";
        $latest_stock = Yii::app()->db->createCommand($latest_stock_sql)
            ->queryScalar([
                ":warehouse_id" => $data->warehouse_id,
                ":item_id" => $data->Item_Id,
                ":rate" => $data->rate,
                ":id" => $data->id,
            ]);

        // Fetch previous stock quantity
        $prev_stock_sql = "
            SELECT stock_quantity 
            FROM jp_warehouse_stock_correction
            WHERE warehouse_id = :warehouse_id
            AND Item_Id = :item_id
            AND rate = :rate
            AND id = (SELECT MAX(id) FROM jp_warehouse_stock_correction WHERE id < :id)
            AND approval_status = 1
        ";
        $prev_stock = Yii::app()->db->createCommand($prev_stock_sql)
            ->queryScalar([
                ":warehouse_id" => $data->warehouse_id,
                ":item_id" => $data->Item_Id,
                ":rate" => $data->rate,
                ":id" => $data->id,
            ]);

        // Calculate difference
        $symbol = '';
        if ($prev_stock != "") {
            $diff = $latest_stock - $prev_stock;
        } else {
            $symbol = (($data->wh_receipt_quantity - $used_qty) > $data->stock_quantity) ? '-' : '+';
            $diff = abs(($data->wh_receipt_quantity - $used_qty) - $data->stock_quantity);
        }

        echo $symbol . $diff;
        ?>
    </td>
    <td><?php echo $data->remarks; ?></td>
</tr>
