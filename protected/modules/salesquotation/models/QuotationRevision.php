<?php

/**
 * This is the model class for table "{{quotation_revision}}".
 *
 * The followings are the available columns in table '{{quotation_revision}}':
 * @property integer $id
 * @property integer $qid
 * @property string $revision_no
 * @property double $tax_slab
 * @property double $sgst_percent
 * @property double $sgst_amount
 * @property double $cgst_percent
 * @property double $cgst_amount
 * @property double $igst_percent
 * @property double $igst_amount
 * @property double $total_after_discount
 * @property double $round_off
 */
class QuotationRevision extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{quotation_revision}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('qid, revision_no', 'required'),
            array('qid', 'numerical', 'integerOnly'=>true),
            array('tax_slab, sgst_percent, sgst_amount, cgst_percent, cgst_amount, igst_percent, igst_amount, total_after_discount, round_off', 'numerical'),
            array('revision_no', 'length', 'max'=>120),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, qid, revision_no, tax_slab, sgst_percent, sgst_amount, cgst_percent, cgst_amount, igst_percent, igst_amount, total_after_discount, round_off', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    public function getRevisionData($dataId, $revisionNo) {
        return QuotationRevision::model()->find(array(
            'select' => 'revision_no, total_after_additionl_discount, tax_slab, sgst_amount, cgst_amount, igst_amount, discount_precent',
            'condition' => 'qid=:qid AND revision_no=:revision_no',
            'params' => array(':qid' => $dataId, ':revision_no' => $revisionNo),
        ));
    }
    

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'qid' => 'Qid',
            'revision_no' => 'Revision No',
            'tax_slab' => 'Tax Slab',
            'sgst_percent' => 'Sgst Percent',
            'sgst_amount' => 'Sgst Amount',
            'cgst_percent' => 'Cgst Percent',
            'cgst_amount' => 'Cgst Amount',
            'igst_percent' => 'Igst Percent',
            'igst_amount' => 'Igst Amount',
            'total_after_discount' => 'Total After Discount',
            'round_off' => 'Round Off',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('qid',$this->qid);
        $criteria->compare('revision_no',$this->revision_no,true);
        $criteria->compare('tax_slab',$this->tax_slab);
        $criteria->compare('sgst_percent',$this->sgst_percent);
        $criteria->compare('sgst_amount',$this->sgst_amount);
        $criteria->compare('cgst_percent',$this->cgst_percent);
        $criteria->compare('cgst_amount',$this->cgst_amount);
        $criteria->compare('igst_percent',$this->igst_percent);
        $criteria->compare('igst_amount',$this->igst_amount);
        $criteria->compare('total_after_discount',$this->total_after_discount);
        $criteria->compare('round_off',$this->round_off);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return QuotationRevision the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}