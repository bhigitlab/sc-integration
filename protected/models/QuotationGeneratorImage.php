<?php

/**
 * This is the model class for table "{{quotation_generator_image}}".
 *
 * The followings are the available columns in table '{{quotation_generator_image}}':
 * @property integer $id
 * @property string $top_bottom_image
 * @property string $type
 * @property integer $created_by
 * @property string $created_at
 */
class QuotationGeneratorImage extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{quotation_generator_image}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('top_bottom_image, created_by', 'required'),
            array('created_by', 'numerical', 'integerOnly'=>true),
            array('top_bottom_image', 'length', 'max'=>300),
            array('top_bottom_image', 'unique'),
            array('type', 'length', 'max'=>1),
            array('created_at', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, top_bottom_image, type, created_by, created_at', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'top_bottom_image' => 'Top Bottom Image',
            'type' => '0=>top image ,1=>bottom image',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('top_bottom_image',$this->top_bottom_image,true);
        $criteria->compare('type',$this->type,true);
        $criteria->compare('created_by',$this->created_by);
        $criteria->compare('created_at',$this->created_at,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return QuotationGeneratorImage the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}