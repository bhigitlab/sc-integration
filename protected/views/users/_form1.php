<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<div class="modal-body">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'users-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
            ));
    ?>

    
		
<!-- Popup content-->
			
				
					<div class="clearfix">
						<div class="">
						
							<div class="row addRow">					
								<div class="col-md-12">
									<?php echo $form->labelEx($model, 'first_name'); ?>
									<?php echo $form->textField($model, 'first_name', array('size' => 30, 'maxlength' => 30)); ?>
									<?php echo $form->error($model, 'first_name'); ?>
								</div>
								<div class="col-md-12">
									<?php echo $form->labelEx($model, 'last_name'); ?>
									<?php echo $form->textField($model, 'last_name', array('size' => 30, 'maxlength' => 30)); ?>
									<?php echo $form->error($model, 'last_name'); ?>
								</div>
								<div class="col-md-12">
									<?php echo $form->labelEx($model, 'username'); ?>
									<?php echo $form->textField($model, 'username', array('size' => 30, 'maxlength' => 20)); ?>
									<?php echo $form->error($model, 'username'); ?>
								</div>
								<div class="col-md-12">						
									<?php echo $form->labelEx($model, 'password'); ?>
									<?php echo $form->passwordField($model, 'password', array('autocomplete' => 'off', 'size' => 30, 'maxlength' => 30)); ?>
									<?php echo $form->error($model, 'password'); ?>
								</div>
							
								<div class="col-md-12 textArea-box">
									<?php echo $form->labelEx($model, 'email'); ?>
									<?php echo $form->textField($model, 'email', array('size' => 30, 'maxlength' => 70)); ?>
									<?php echo $form->error($model, 'email'); ?>
								</div>
								<div class="col-md-12">						
									<?php echo $form->labelEx($model, 'phonenumber'); ?>
									<?php echo $form->textField($model, 'phonenumber', array('size' => 30, 'maxlength' => 20)); ?>
									<?php echo $form->error($model, 'phonenumber'); ?>
								</div>
								<div class="col-md-12 textArea-box">
									<?php echo $form->labelEx($model,'reporting_person'); ?>
									<?php
 							                                $tblpx = Yii::app()->db->tablePrefix;
							                                $sql="SELECT `t`.`userid`, concat_ws(' ',`t`.`first_name`, `t`.`last_name`) as full_name,"
							                                    . "`{$tblpx}user_roles`.`role` "
							                                    . "FROM `{$tblpx}users` `t` JOIN {$tblpx}user_roles ON {$tblpx}user_roles.id= t.user_type "
							                                    . "WHERE status=0 and user_type in (1,2) ORDER BY user_type,full_name ASC";
							                            $result = Yii::app()->db->createCommand($sql)->queryAll();
							                            $listdata = CHtml::listData($result, 'userid', 'full_name','role');
									echo $form->dropDownList($model, 'reporting_person',
									$listdata, 
									array('empty' => '-----------'));
									?>
									
									<?php echo $form->error($model,'reporting_person'); ?>
								</div>
								<div class="col-md-12">						
									 <?php echo $form->labelEx($model, 'user_type'); ?>
									 <?php echo $form->dropDownList($model, 'user_type', CHtml::listData(UserRoles::model()->findAll(array('order' => 'role ASC')), 'id', 'role'), array('empty' => '-Choose a role-')); ?>
									 <?php echo $form->error($model, 'user_type'); ?>
								</div>
							
								<div class="col-md-12">
									<?php echo $form->labelEx($model, 'status'); ?>
									<div class="radio_btn">
										<?php
										echo $form->radioButtonList($model, 'status', array('Active', 'Inactive'), array('separator' => ''));
										?>
									</div>
								</div>
								<!-- <div class="col-md-6 col-sm-6 save-btnHold">						
									 <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
								</div> -->
							</div>
							
						</div>
						<div class="modal-footer save-btnHold">
                            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
                            
                            <button data-dismiss="modal">Close</button>
                      </div>
						</div>
					

			


<?php $this->endWidget(); ?>

</div><!-- form -->