<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<script src="<?php echo Yii::app()->baseUrl . '/ckeditor/ckeditor.js'; ?>"></script>
<style>
    .cke_contents {
        height: 80px !important;
    }

    .panel-body>.panel {
        margin-top: 10px;
    }

    .pb-2 {
        padding-bottom: 10px;
    }

    .h-head {
        height: 45px;
    }

    .add_new_sec {
        margin-bottom: 10px;
        padding: 6px 10px;
    }

    .add_new_sec button {
        border: none;
        box-shadow: none;
    }

    .add_new_sec h4 {
        margin: 0px !important;
    }


    .panel-heading h4 {
        margin: 0px !important;
    }

    h4.categ_item {
        color: #337ab7;

        font-size: 1.5rem;
        font-weight: bold;
    }

    i.toggle-caret {
        font-size: 1.5rem;
        font-weight: bold;
        margin-left: 5px;
    }

    input.total_sum {
        width: 50px;
        margin-right: 3rem;
    }

    .sum_sec {
        max-width: 400px;
        text-align: right;
        margin-left: auto;
        background: #fafafa;
        margin-bottom: 10px;
        font-weight: bold;
        white-space: nowrap;
        padding: 10px;
    }

    .toast-container {
        width: 350px;
    }

    .toast-position-top-right {
        top: 57px;
        right: 6px;
    }

    .toast-item-close {
        background-image: none;
        cursor: pointer;
        width: 12px;
        height: 12px;
        text-align: center;
        border-radius: 2px;
    }

    .toast-item-image {
        font-size: 24px;
    }

    .toast-item-close:hover {
        color: red;
    }

    .toast-item {
        border: transparent;
        border-radius: 3px;
        font-size: 10px;
        opacity: 1;
        background-color: rgba(34, 45, 50, 0.8);
    }

    .toast-item-wrapper p {
        margin: 0px 5px 0px 42px;
        font-size: 14px;
        text-align: justify;
    }


    .toast-type-success {
        background-color: #00A65A;
        border-color: #00A65A;
    }

    .toast-type-error {
        background-color: #DD4B39;
        border-color: #DD4B39;
    }

    .toast-type-notice {
        background-color: #00C0EF;
        border-color: #00C0EF;
    }

    .toast-type-warning {
        background-color: #F39C12;
        border-color: #F39C12;
    }

    .permission_style {
        background: #f8cbcb !important;
    }

    .add_new_row {
        position: absolute;
        right: 0px;
        top: -44px;
        background-image: none;
        font-weight: normal;
    }
</style>
<style>
      table .entry-table tr th {
        background-color: #f4f9ff !important;
      }
      table tfoot tr th {
        background: #fff !important;
      }

      .table > thead > tr > th,
      .table > thead > tr > td {
        vertical-align: middle !important;
      }

      .btn-primary {
        background-color: #6a8ec7;
        border-color: #6a8ec7;
      }

      .btn-default {
        background-color: #f8f9fa;
        border-color: #ccc;
        color: #6a8ec7;
      }
      .padding-top-0 {
        padding-top: 0px;
      }
      .padding-bottom-0 {
        padding-bottom: 0px;
      }
      .ml-auto {
        margin-left: auto;
      }
      .filter-wrapper {
        display: flex;
        align-items: center;
      }

      .heading-title {
        font-weight: bold;
        font-size: 18px;
        margin-right: 20px;
        padding: 10px 0px;
      }
      .divider {
        border-left: 1px dotted #000;
        height: 34px;
        margin-left: 15px;
        margin-right: 15px;
        display: inline-block;
        vertical-align: middle;
      }
      .form-group {
        display: inline-block;
      }
      
      .dotted-line {
        border-bottom: 1px dotted #000;
      }
      .blue-dotted-line {
        border-bottom: 1px dotted #7ab5f1;
      }
      .first-wrapper {
        display: flex;
        align-items: center;
      }
      .entries-wrapper {
        border-radius: 5px;
        margin-top: 10px;
        box-shadow: 0px -1px 14px 5px rgba(246, 246, 255, 1);
      }
      .entries-wrapper .row {
        padding: 5px 10px;
      }
      .no-wrap {
        white-space: nowrap;
      }
      .input-info {
        display: flex;
        white-space: nowrap;
        align-items: center;
        column-gap: 5px;
      }
      .no-col {
        padding-left: 15px;
        padding-right: 15px;
      }
      .w-50p {
        width: 50px;
      }
      .w-100p {
        width: 100px;
      }
      .value-label {
        padding: 5px 5px 4px 5px;
        color: #5580c5;
        background-color: #f6f6ff;
        font-weight: 700;
        width: 100%;
      }
      .margin-top-18 {
        margin-top: 18px;
      }
      .final-section-label {
        margin-bottom: 0px;
        font-size: 14px;
        font-weight: 700;
      }
      .final-label-info {
        width: 100px;
      }
      .summary-wrapper {
        width: 100%;
        background-color: #f6f6ff;
        padding: 10px 10px;
        display: flex;
        justify-content: end;
      }
      .final-value-label {
        padding: 5px 5px 4px 5px;
        color: #5580c5;
        font-weight: 700;
        width: 100%;
        margin-right: 15px;
      }

      /* old styles that need changes */
      /* new background color */
      .page_filter {
        background-color: #f6f6ff;
        padding: 8px;
        border-radius: 4px;
        margin-bottom: 5px;
      }
      /* change it as follows */
      .filter_elem.links_hold {
        margin-top: 0px;
        margin-left: 10px;
      }

      /* table classes */
      .legend {
        list-style: none;
        padding-left: 0;
      }

      .legend li {
        float: left;
        margin-right: 10px;
        display: flex;
        align-items: center;
      }

      .legend span {
        border: 1px solid #ccc;
        float: left;
        width: 12px;
        height: 12px;
        margin-right: 4px;
      }

      .approval_needed {
        background-color: #ff6347;
      }

      .declined {
        background-color: #ffa50057;
      }

      .unreconciled {
        background-color: #dbeff6;
      }

      .table th,
      .table td {
        border-right: 1px solid #ccc !important;
      }

      .total-table {
        margin-bottom: 0;
      }

      #table-wrapper {
        max-height: 400px;
        overflow: auto;
        margin-top: 5px;
      }

      .summary {
        font-weight: bold;
      }

      
      .margin-top-25 {
        margin-top: 25px;
      }

      .margin-bottom-16 {
        margin-bottom: 16px;
      }
      .checkboxList {
        height: 108px;
        margin-bottom: 0px;
      }
      .sub-row-wrapper {
        margin: 0px -25px;
      }
      .new-listing-table {
        /* height: 300px; */
        overflow-y: auto;
      }
      .new-listing-table table thead tr th {
        background-color: #f4f9ff !important;
      }
      .entries-wrapper .nav-tabs {
        display: flex;
        justify-content: space-between;
        border: none;
        padding: 10px;
      }

      .entries-wrapper .nav-tabs > li {
        flex: 1;
        text-align: center;
      }

      .entries-wrapper .nav-tabs > li > a {
        width: 100%;
        padding: 10px;
        text-align: center;
        border: none;
        background-color: #ffffff;
        font-weight: bold;
        color: #7c7c7c;
      }

      .entries-wrapper .nav-tabs > li > a:hover {
        background-color: #ffffff;
      }

      .entries-wrapper .nav-tabs > li.active > a {
        border: none;
        color: #337ab7;
      }

      .entries-wrapper.nav-tabs > li.active > a:hover {
        border: none;
      }
      .progress {
        margin: 0px 15px 15px 15px;
      }
      .progress-bar-striped {
        background-size: 40px 40px;
      }

      .bordered-divider {
        border-top: 1px dotted #000;
        margin-top: 20px;
        margin-bottom: 20px;
      }

      .btn-nav {
        margin-top: 20px;
      }
      .nav-view {
        font-size: 16px;
        display: flex;
        align-items: center;
        color: #5580c5;
        cursor: pointer;
        text-decoration: none !important;
      }
      .categories-block {
        padding: 10px 0px;
        border: 1px solid #7ab5f1;
      }
      .add-item-block {
        border: 1px solid #ccc;
      }
      .margin-top-20 {
        margin-top: 20px;
      }
      .margin-horizontal-10 {
        margin: 0px 10px;
      }
      .width-40 {
        width: 40%;
      }
      .quotation-view .row .form-group {
        display: flex;
        column-gap: 3px;
      }

      .quotation-view .row .form-group label {
        white-space: nowrap;
      }
      .sub-table-heading td {
        background: #f1f1fd;
      }
      .final-table-heading td,
      .final-summary {
        background: #337ab7;
        color: white;
      }
      .final-summary label,
      .final-summary span {
        color: white;
      }
      .final-summary-value-label {
        padding: 5px 5px 4px 5px;
        color: white;
        font-weight: 700;
        width: 100%;
        margin-right: 15px;
      }
      .modal-heading-title {
        font-weight: bold;
        font-size: 18px;
        margin-right: 20px;
      }
      .quotation-view .table {
        width: 100%;
        max-width: 100%;
        margin-bottom: 0px;
      }
      .view-modal .modal-dialog {
        height: 93%;
      }
      .view-modal .modal-content {
        height: 100%;
      }
      .view-modal .modal-body {
        overflow-y: auto;
        height: calc(100% - 65px);
      }
      .padding-top-0 {
        padding-top: 0px !important;
      }
      .padding-bottom-0 {
        padding-bottom: 0px !important;
      }
    </style>
<?php
$total_stagepercent=0;
if(isset($model->scquotation_id) && ($model->scquotation_id !="")){
    $sql = "SELECT SUM(stage_percent)  FROM `jp_sc_payment_stage` WHERE `quotation_id` = ".$model->scquotation_id;
	$total_stagepercent = Yii::app()->db->createCommand($sql)->queryScalar();
}	
	?>
	<input type="hidden" value="<?php echo (!is_null($total_stagepercent)?$total_stagepercent:"0.00") ?>" class="total_stagepercent">
<div class="entries-wrapper">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'scquotation-form',
        'enableClientValidation' => true,
        'clientOptions' => array('validateOnChange' => true),
        'enableAjaxValidation' => false,
    )); ?>
    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#add-entry" data-step="1">Basic Details</a></li>
                        <li><a data-toggle="tab" href="#category" data-step="2">Categories</a></li>
                        <li><a data-toggle="tab" href="#payment-stages" data-step="3" onclick="paymentClick()">Tax & Payment Stages</a></li>
                        <li><a data-toggle="tab" href="#terms-conditions" data-step="4">Terms and Conditions</a></li>
                        <a href="#" data-toggle="modal" data-target="#viewModal" class="modal-button nav-view">
                      <i class="fa fa-eye"></i
                    ></a>
                      <div id="viewModal" class="modal fade view-modal" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content"><?php
                          $render_datas = array('model' => $model,'terms'=>$terms);
                          $this->renderPartial('_quotation_view', $render_datas);
                          ?>
                           
                          </div>
                        </div>
                      </div>
                      </a>
                        
    </ul>
    <!-- Progress Bar -->
    <div class="progress">
        <div class="progress-bar progress-bar-striped active" role="progressbar" style="width: 25%" id="progressBar"></div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="dotted-line"></div>
        </div>
    </div>
    <div class="tab-content">
        <div id="add-entry" data-step="1" class="tab-pane fade in active">
                    <div class="row">
                        <div class="form-group col-xs-12 col-md-3">
                        
                            <input type="hidden" value="<?php echo $model->getTotalQuotationAmount($model->scquotation_id)?>" id="quotation_amount">
                                <input type="hidden" name="Scquotation[scquotation_id]" id="Scquotation_scquotation_id" value="<?php echo $model->scquotation_id; ?>">
                                <?php echo $form->labelEx($model, 'company_id'); ?>
                                <?php
                                echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(array('condition' => 'id IN (' . $user_companies . ')')), 'id', 'name'), array('ajax' => array(
                                    'type' => 'POST',
                                    'url' => Yii::app()->createUrl('projects/dynamicproject'),
                                    'dataType' => 'JSON',
                                    'success' => 'js:function(data)'
                                        . '{'
                                        . '    $("#Scquotation_project_id").html(data.projects_list);'
                                        . '    $("#Scquotation_project_id").val("");'
                                        . '    $("#Scquotation_subcontractor_id").html(data.subcontractor_list);'
                                        . '    $("#Scquotation_subcontractor_id").val("");'
                                        . '}',
                                    'data' => array('company_id' => 'js:this.value'),
                                ), 'class' => 'form-control js-example-basic-single field_change require', 'empty' => '-Select Company-', 'style' => 'width:100%','onchange'=>'getDynamicQuotNo(this.value)'));
                                ?>
                               
                                <span id="company_id_em_" style="color:red;"></span>
                        
                        </div>
                        <div class="form-group col-xs-12 col-md-3">
                            <?php echo $form->labelEx($model, 'project_id'); ?>
                            <?php
                            echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll(array('condition' => 'company_id IN (' . $user_companies . ')')), 'pid', 'name'), array('class' => 'form-control js-example-basic-single require', 'empty' => '-Select Project-', 'style' => 'width:100%'));
                            ?>
                            
                            <span id="project_id_em_" style="color:red;"></span>
                        </div>
                        <div class="form-group col-xs-12 col-md-3">
                        
                            <?php echo $form->labelEx($model, 'subcontractor_id'); ?>
                            <?php
                            echo $form->dropDownList($model, 'subcontractor_id', CHtml::listData(Subcontractor::model()->findAll(array('condition' => 'company_id IN (' . $user_companies . ')')), 'subcontractor_id', 'subcontractor_name'), array('ajax' => array(
                                'type' => 'POST',
                                'url' => Yii::app()->createUrl('subcontractor/dynamicexpensehead'),
                                'success' => 'js:function(data)'
                                    . '{'
                                    . '    $("#Scquotation_expensehead_id").html(data);'
                                    . '    $("#Scquotation_expensehead_id").val("");'
                                    . '}',
                                'data' => array('sub_id' => 'js:this.value'),
                            ), 'class' => 'form-control js-example-basic-single require', 'empty' => '-Select Subcontractor-', 'style' => 'width:100%'));
                            ?>
                            
                            <span id="subcontractor_id_em_" style="color:red;"></span>
                        </div>
                        <div class="form-group col-xs-12 col-md-3">
                           <?php echo $form->labelEx($model, 'scquotation_date'); ?>
                            <?php
                            if ($model->isNewRecord){
                                $model->scquotation_date = date('d-m-Y');                
                            }else{
                                $model->scquotation_date = date('d-m-Y',strtotime($model->scquotation_date));                
                            }
                            echo $form->textField($model, 'scquotation_date', array('class' => 'form-control require')); ?>
                            
                            <span id="scquotation_date_em_" style="color:red;"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12 col-md-3">
                                <?php echo $form->labelEx($model, 'scquotation_no'); ?>
                                <?php
                                if($model->isNewRecord){
                                    $model['scquotation_no'] = $scq_no;
                                }
                                
                                echo $form->textField($model, 'scquotation_no',  array('class' => 'form-control require')); ?>
                                
                                <span id="scquotation_no_em_" style="color:red;"></span>
                        </div>
                        <div class="form-group col-xs-12 col-md-3">
                            <?php echo $form->labelEx($model, 'expensehead_id'); ?>
                            <?php
                            if (!$model->isNewRecord && empty($model->expensehead_id)) {
                                $model->expensehead_id = Scquotation::model()->getExpenseType($model->scquotation_id);
                            }
                            echo $form->dropDownList($model, 'expensehead_id', CHtml::listData(ExpenseType::model()->findAll(array('condition' => 'expense_type IN (97, 98)')), 'type_id', 'type_name'), array('class' => 'form-control js-example-basic-single require', 'empty' => '-Select Expense Head-', 'style' => 'width:100%'));
                            ?>
                            <span id="expensehead_id_em_" style="color:red;"></span>
                        </div>
                        <div class="form-group col-xs-12 col-md-3">
                           <?php 
                            if ($model->isNewRecord){                
                                $model->work_order_date = date('d-m-Y');                
                            }else{
                                if(empty($model->work_order_date)){
                                    $workorderdate=$model->scquotation_date ;
                                }else{
                                    $workorderdate=$model->work_order_date ;
                                }
                                $model->work_order_date = date('d-m-Y',strtotime($workorderdate));    
                            }
                            echo $form->labelEx($model, 'work_order_date'); ?><span class="required"> *</span>
                            <?php
                            echo $form->textField($model, 'work_order_date', array('class' => 'form-control require')); ?>
                        
                            <span id="work_order_date_em_" style="color:red;"></span>
                        </div>
                        <div class="form-group col-xs-12 col-md-3">
                            <?php 
                            if ($model->isNewRecord){                
                                $model->completion_date = date('d-m-Y');                
                            }else{
                                $model->completion_date = date('d-m-Y',strtotime($model->completion_date));    
                            }
                            echo $form->labelEx($model, 'completion_date'); ?>
                            <?php
                            echo $form->textField($model, 'completion_date', array('class' => 'form-control require')); ?>

                            <span id="completion_date_em_" style="color:red;"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12 col-md-3">
                            <label>Labour Template </span></label>
                            <select class="form-control po_select2" name="Scquotation[template]">
                                <option value='0'>Select Template</option>
                                <?php
                                $templatelist = LabourTemplate::model()->findAll();
                                $assigned_template = "";
                                if (!$model->isNewRecord) {
                                    $sctemplate=$model->labour_template;
                                    if(!empty($model->labour_template)){
                                        $assigned_template = Yii::app()->db->createCommand("SELECT DISTINCT template_id FROM `jp_sc_quot_labour_template` WHERE sc_quot_id=".$model->scquotation_id)->queryScalar();  
                                    }
                                                                                    
                                }

                                foreach ($templatelist as $template) {
                                ?>                    
                                    <option value='<?php echo $template->id ?>' <?php echo ($template->id== $assigned_template) ? 'selected' : ''; ?>> 
                                    <?php echo $template->template_label ?>
                                    </option>

                                <?php
                                }
                                ?>
                            </select>
                            <span style="color:red">**</span><span>NOTE:Select Labour template in SC quotation or in project For Labour Entry </span>
                            <span id="labour_template_em_" style="color:red;"></span>
                        </div>
                        <div class="form-group col-xs-12 col-md-3">
                            <?php echo $form->labelEx($model, 'scquotation_decription'); ?>
                            <?php
                            echo $form->textArea($model, 'scquotation_decription',  array('class' => 'form-control require', 'style' => 'width:100%', 'rows' => '1'));
                            ?>
                            <span id="scquotation_decription_em_" style="color:red;"></span>
                        </div>
                        <div id="dialog-modal" class="form-group col-xs-12 col-md-6">
                        </div>
                        <div class="form-group col-xs-12 text-right">
                         
                              <?php if (!$model->isNewRecord) {  ?>
                              <?php echo CHtml::submitButton('Update', array('class' => 'btn btn-primary','id'=>'update-btn','style' => 'margin-top:30px;')); ?>
                              <?php }else{ ?>
                              <?php echo CHtml::submitButton('Create', array('class' => 'btn btn-primary','id'=>'submit-btn','style' => 'margin-top:30px;')); ?>
                              <?php } ?>
                          
                      </div>
                      </div>
                      
                      <div class="row">
                        
                       
            </div>
        </div>
        <?php $this->endWidget(); ?>                    

                    <div id="category" data-step="2" class="tab-pane fade">
                      <div class="row">
                        <div class="col-xs-12">
                          <button type="button" class="btn btn-info pull-right mt-0 mb-10 addcateg" data-toggle="collapse" data-target="#addCategory">Add New</button>
                        </div>
                      </div>
                      
                      <div class="row">
                        <div class="col-xs-12">
                          <div class="categories-block">
                          <?php
                            if (!$model->isNewRecord) {
                            ?>
                                <div id="items_holder">
                                    <div class="quotationmaindiv" id="items_list">
                                        <div class="add_new_sec ">
                                            <?php
                                            $main_items = $model->getCategories($model->scquotation_id);
                                            //total
                                            $scquotationItems = ScquotationItems::model()->findAllByAttributes(array('scquotation_id' => $model->scquotation_id));
                                            $totalAmount = 0;
                                              foreach ($scquotationItems as $item) {
                                                  $totalAmount += $item->item_amount;
                                              }
                                            $render_datas = array('model' => $model, 'user_companies' => $user_companies, 'main_item_model' => $main_item_model, 'sub_item_model' => $sub_item_model);
                                            $this->renderPartial('_quotation_category_form', $render_datas);
                                            if (!empty($main_items)) {
                                                foreach ($main_items as $main_item_model) {
                                                    $render_datas['main_item_model'] = $main_item_model;
                                                    $this->renderPartial('_quotation_items_form', $render_datas);
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-xs-12 text-right">
                        <?php 
                          if (!$model->isNewRecord) {
                              $scItems = ScquotationItems::model()->find('scquotation_id = :scquotation_id', [
                                  ':scquotation_id' => $model->scquotation_id,
                              ]);
                              $hiddenValue = empty($scItems) ? 1 : 0;
                          }
                          ?>
                          <input type="hidden" name="hiddenField" value="<?php echo isset($hiddenValue) ? $hiddenValue : 0; ?>">
                          <button type="button" class="btn btn-primary btn-nav pull-right" id="nextBtn3">Next</button>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-12">
                          <div class="summary-wrapper final-summary">
                            <div class="final-summary-value-label">Summary</div>
                            <div class="input-info pull-right">
                              <label class="final-section-label">Total categories:</label>
                              <span class="final-summary-value-label">
                              <?php
                              if (!$model->isNewRecord) {
                                  $totalCategory=count($main_items);
                                  echo isset($totalCategory) ? $totalCategory : '';
                              }
                              else{
                                echo 0;
                              } 
                               ?>
                              </span>
                              <label class="final-section-label">Total Amount:</label>
                              <span class="final-summary-value-label"><?php echo isset($totalAmount) ? $totalAmount : ''; ?></span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <!-- <div class="form-group col-xs-12 text-right">
                          <button class="btn btn-primary btn-nav pull-right" id="nextBtn2">Save & Continue</button>
                        </div> -->
                      </div>
                    </div>

                    <div id="payment-stages" data-step="3" class="tab-pane fade">
                    <div class="row">
                        <div class="col-xs-12">
                          <div class="summary-wrapper final-summary">
                            <div class="final-summary-value-label">Summary</div>
                            <div class="input-info pull-right">
                              <label class="final-section-label">Total categories:</label>
                              <span class="final-summary-value-label">
                              <?php
                              if (!$model->isNewRecord) {
                                  $totalCategory=count($main_items);
                                  echo isset($totalCategory) ? $totalCategory : '';
                              }
                              else{
                                echo 0;
                              } 
                               ?>
                              </span>
                              <label class="final-section-label">Total Amount:</label>
                              <span class="final-summary-value-label"><?php echo isset($totalAmount) ? $totalAmount : ''; ?></span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div></div>
                      <?php
                          if(isset($_GET['scquotation_id']) && $_GET['scquotation_id'] !=""){
                              if (!empty($main_items)) {
                              $this->renderPartial('_quotation_tax_details', array('scquotation_model' => $model, 'model' => $payment_model));
                              $this->renderPartial('_payment_stage', array('scquotation_model' => $model, 'model' => $payment_model,'serial_number'=>$serial_number));
                              }
                          }
                      ?>
                      <div class="row">
                        <!-- <div class="form-group col-xs-12 text-right">
                          <button class="btn btn-primary btn-nav pull-right" id="nextBtn3">Save & Continue</button>
                        </div> -->
                      </div>
                    </div>

                    <div id="terms-conditions" data-step="4" class="tab-pane fade">
                      <div class="row">
                        
                        <div class="terms_sec">
                            <?php $this->renderPartial('_sc_terms_conditions',array('terms'=>$terms)); ?>
                        </div>
                      </div>
                    </div>
    </div>
    
</div>


<script>

$(document).ready(function() {
  $('#scquotation-form').on('submit', function(e) {
    e.preventDefault(); 
        // Disable the submit button to prevent multiple clicks
    $('#submit-btn, #update-btn').prop('disabled', true);

        var formData = $('#scquotation-form').serialize();
        var Scquotation=$('#Scquotation_scquotation_id').val();
        
        url ='<?php echo Yii::app()->createUrl("subcontractor/addQuotation"); ?>';
        if(Scquotation!==null){
          url ='<?php echo Yii::app()->createUrl("subcontractor/addquotation&scquotation_id=") ?>'+Scquotation;
        }
        $.ajax({
            type: 'POST',
            url: url, 
            data: formData,
            dataType: 'json',
            success: function(data) {
                console.log(data);
                if (data.status === '1') {
                    var id = data.scquotation_id;
                    window.location.href = '<?php echo Yii::app()->createAbsoluteUrl('subcontractor/addquotation&scquotation_id='); ?>' + id + '&activateTab=2';

                    $(window).on('load', function() {
                        var stepToActivate = 2;
                        activateTab(stepToActivate);
                    });
                } else {
                    // Clear previous errors
                    $('.errorMessage').text('').hide();
                    // Show errors from the response
                    $.each(data.errors, function(key, value) {
                        $('#' + key + '_em_').text(value).show();
                    });
                    $('#submit-btn, #update-btn').prop('disabled', false);
                }
            },
            error: function(xhr, status, error) {
                console.error("Error: " + status + " " + error);
                alert('An error occurred while submitting the form.');
                $('#submit-btn, #update-btn').prop('disabled', false);
            }
        });
    });
});

$(document).ready(function() {
    // Check if the URL has the activateTab parameter
    var urlParams = new URLSearchParams(window.location.search);
    var activateTab = urlParams.get('activateTab');
   
    // If activateTab is present in the URL, call activateTab function with the specified step
    if (activateTab) {
        activateTabDiv(activateTab);
    }
});
$(document).ready(function() {
    var urlParams = new URLSearchParams(window.location.search);
    var activateTab = urlParams.get('activateTab');

    if (activateTab) {
        activateTab = parseInt(activateTab, 10); 
        activateTabDiv(activateTab);
    }
});
function activateTabDiv(stepToActivate) {
  
    $('ul.nav-tabs li').removeClass('active');
    $('div.tab-content .tab-pane').removeClass('in active');

    $('ul.nav-tabs li[data-step="' + stepToActivate + '"]').addClass('active');

    $('div.tab-content .tab-pane[data-step="' + stepToActivate + '"]').addClass('in active');

    var percentage = (stepToActivate / 4) * 100; 
    $('#progressBar').css('width', percentage + '%');
}

    $(document).ready(function () {
        const progressValues = [25, 50, 75, 100];
        let currentIndex = 0;

        function updateProgress(step) {
          $("#progressBar").css("width", progressValues[step - 1] + "%");
        }

        // Tab click handler
        $('a[data-toggle="tab"]').on("shown.bs.tab", function (e) {
          const step = $(e.target).data("step");
          updateProgress(step);
          currentIndex = step - 1;
        });

        // Next/Prev button click handlers
        $("#nextBtn").click(function () {
          $(".nav-tabs > .active").next("li").find("a").trigger("click");
        });

        $("#nextBtn2").click(function () {
          $(".nav-tabs > .active").next("li").find("a").trigger("click");
        });

        $("#nextBtn3").click(function () {
          $(".nav-tabs > .active").next("li").find("a").trigger("click");
        });

        $("#prevBtn").click(function () {
          $(".nav-tabs > .active").prev("li").find("a").trigger("click");
        });

        $("#prevBtn2").click(function () {
          $(".nav-tabs > .active").prev("li").find("a").trigger("click");
        });

        $("#prevBtn3").click(function () {
          $(".nav-tabs > .active").prev("li").find("a").trigger("click");
        });
    });
    function clearSelectedLaboursTable() {
    var selectedLaboursTable = document.getElementById('dialog-modal');
    selectedLaboursTable.innerHTML = ''; 
    }

    function podefault() {
    var selectElement = $('select.po_select2');
    var template = selectElement.val();
    var scquotation="<?php echo isset($model->scquotation_id )? $model->scquotation_id:''?>";
    var type='';
    type=1;
    if (template == '') {
        // Do something if template is empty
    } else {
        if (selectElement.val() != 0) {
            $.ajax({
                url: '<?php echo Yii::app()->createUrl('subcontractor/getTemplateDetails'); ?>',
                method: 'POST',
                data: {
                    template_id: template,
                    type:type, 
                    project :scquotation  , 
                },
                success: function(response) {
                    console.log(response);
                    $('#dialog-modal').html(response);
                    $('.labour-rate').each(function() {
                        $(this).prop('readonly', false); // Make input fields editable
                    });
                }
            });
        } else {
            $('#dialog-modal').html("");
        }
    }
}

podefault();
    $('select.po_select2').change(function () {
        var template = $(this).val();
       
        var project = '<?php echo isset($model->pid) ? $model->pid : ''; ?>';
        var type='';
        type=2;
        if ($(this).val() != 0) {
            $.ajax({
            url: '<?php echo Yii::app()->createUrl('Subcontractor/getTemplateDetails'); ?>',
            method: 'POST',
            data: {
                template_id :template ,
                type:type, 
                project :project,          
            },       
            success: function(response) {
                console.log(response);
                $('#dialog-modal').html(response);                
            }
        })
            
        }else{
            $('#dialog-modal').html("");   
        }
    });


    $(".remove_row").hide();
    $(".js-example-basic-single").select2();
    setTimeout(function() {
        $('.flash-holder').fadeOut('2000');
    }, 1000);
    CKEDITOR.replaceAll('text_editor');
    $(function() {
        $("#Scquotation_scquotation_date").datepicker({
            dateFormat: 'dd-mm-yy'
        });
        $("#Scquotation_completion_date").datepicker({
            dateFormat: 'dd-mm-yy'
        });
        $("#Scquotation_work_order_date").datepicker({
            dateFormat: 'dd-mm-yy'
        });
        $("#Scquotation_scquotation_date").change(function() {            
            $("#Scquotation_completion_date").datepicker('option', 'minDate', $(this).val());
           
        });
        $("#Scquotation_completion_date").change(function() {
            $("#Scquotation_scquotation_date").datepicker('option', 'maxDate', $(this).val());
            $("#Scquotation_work_order_date").datepicker('option', 'maxDate', $(this).val());
        });
        $("#Scquotation_work_order_date").change(function() {
            $("#Scquotation_completion_date").datepicker('option', 'minDate', $(this).val());
           
        });
        $('.calculate').keypress(function(event) {
            return isNumber(event, this)
        });
        $('.add_new_row').click(function(e) {
          var panelBody = $(this).closest('.panel-body'); // Adjusted selector to get the correct scope
          var rows = panelBody.find(".quotation_items_form");
          var row_len = rows.children().length;
            if (row_len > 0) {

              panelBody.find(".remove_row").show();
            } else {
              panelBody.find(".remove_row").hide();
            }

            $(this).siblings(".panel-body").find(".quotation_items_form").find(".form_datas").each(function() {
                $(this).children(".desc_sec").find(".cke.cke_reset.cke_chrome").css('display', 'none');
            })
            e.preventDefault();
            $.each(CKEDITOR.instances, function(i) {
                var editor = CKEDITOR.instances[i];
                editor.destroy();
            });
            var category_id = $(this).parents('.collapse').attr('id');
            var row_id = category_id.split('_');
            var regex = /^(.+?)(\d+)$/i;
            var cloneIndex = $('#' + category_id).find(".form_datas").length + 1;
            // console.log(cloneIndex);
            var appendDiv = $("#" + category_id).find(".quotation_items_form");
            $("#" + category_id).find('.form_datas:first').clone().find("input:text").val("").end().appendTo(appendDiv).find('#ScquotationItems_item_description-' + row_id[1]).each(function() {
                var id = this.id || "";
                var match = id.match(regex) || [];
                if (match.length == 3) {
                    this.id = match[1] + (cloneIndex);
                    // console.log(this.id);
                    $('#' + this.id).val('');

                }
            });
            CKEDITOR.replaceAll('text_editor');
        });
    });
    $(document).on('change', '.calculate', function() {

        var category_id = $(this).parents('.collapse').attr('id');
        var type = $(this).parents('.calc_values').find('#ScquotationItems_item_type').val();
        console.log(type);
        var amount = 0;
        if (type === '1') {
            var quantity = $(this).parents('.calc_values').find('#ScquotationItems_item_quantity').val();
            var rate = $(this).parents('.calc_values').find('#ScquotationItems_item_rate').val();
            amount = quantity * rate;

            $(this).parents('.calc_values').find('#ScquotationItems_item_amount').val(parseFloat(amount).toFixed(2));
        }
    });

    $(document).on('change', '.item_type', function() {
        $(this).parents('.calc_values').find('#quantity_div,#rate_div,#amount_div,#unit_div').find("input").val("");
        var type = $(this).val();
        if (type === '2') {
            $(this).parents('.calc_values').find('#quantity_div,#rate_div').addClass('hide');
            $(this).parents('.calc_values').find('#amount_div input').attr('readonly',false);
        }
        else if(type === '1'){
          $(this).parents('.calc_values').find('#quantity_div,#rate_div').addClass('show');
          $(this).parents('.calc_values').find('#amount_div input').attr('readonly',false);
        } else {
            $(this).parents('.calc_values').find('#quantity_div,#rate_div').removeClass('hide');
            $(this).parents('.calc_values').find('#amount_div input').attr('readonly',true);
        }
    });


    function isNumber(evt, element) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (
            (charCode != 46 || $(element).val().indexOf('.') != -1) && // “.” CHECK DOT, AND ONLY ONE.
            (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    $.each(CKEDITOR.instances, function(i) {
        var editor = CKEDITOR.instances[i];
        editor.on('blur', function() {
            var field_name = $(this).attr("name");
            var form_id = $('#' + field_name).parents("form").attr("id");
            var collapse_id = $('#' + field_name).parents(".panel-collapse").attr('id');
            var description_content = $('#cke_' + field_name).find('iframe').contents().find(".cke_editable").html();
            var description_text = $('#cke_' + field_name).find('iframe').contents().find(".cke_editable").text();
            $('#' + field_name).text(description_content);
            itemDescFieldChange(form_id, collapse_id);

        });
    });
    
    function onlySpecialchars(str)
    {        
        var regex = /^[^a-zA-Z0-9]+$/;
        var message ="";      
        var matchedAuthors = regex.test(str);
         
        if (matchedAuthors) {
            var message ="Special characters not allowed";            
        } 
        return message;        
    }
    $("#Scquotation_scquotation_no").keyup(function () {      
        var message =  onlySpecialchars(this.value);   
        $(this).siblings(".errorMessage").show().html(message).addClass('d-block');                         
    });
    
    // $("#submit-btn").click(function() {
    //     event.preventDefault();
    //     $("#submit-btn").attr('disabled','true');
    // var data = $(this).val();
    // if(data != ""){
    //     $(this).siblings(".errorMessage").html("");
    // }
    // var allRequired = true;
    // var error_count = $(".errorMessage").not(':empty').length;        
    // var $inputs = $('#scquotation-form :input').filter('.require');
    
    // var values = {};
    
    // $inputs.each(function() {
    //     if ($(this).val() == '') {
    //         allRequired = false;
    //     } else {
    //         allRequired = true;  
    //     }
    //     values[this.name] = $(this).val();
    // });
    // var completion_date= $("#Scquotation_completion_date").val();
    // var workorderdate=$("#Scquotation_work_order_date").val();
    
    // var completion_date_obj= new Date(completion_date);
    // var workorderdate_obj= new Date(workorderdate);
    // var dateCheck = 0;

    // if(workorderdate_obj < completion_date_obj){
    //     dateCheck=1;
    // }
  
    
    // if (allRequired && error_count == 0 ) {
    //     $("#scquotation-form").submit();
    // }else{
        // if (!dateCheck) {
        //     // Show an error message if the date check fails
        //     $("#Scquotation_work_order_date").siblings(".errorMessage").html("Work order date must be less than completion date.").show();
        //     //return false;
        //     setTimeout(function(){
        //         location.reload();
        //     },5000);
            
        // }
//     }
// });
// ;
    $(document).ready(function() {
        $('#nextBtn3').on('click', function() {
            var hiddenValue = $('input[name="hiddenField"]').val();
            if (hiddenValue === "1") {
                if (confirm("The category item amount is currently empty. Are you sure you want to proceed to the next tab?")) {
                    window.location.href = window.location.href.split('#')[0] + '#tab3';
                    window.location.reload();
                }
                else{
                  window.location.href = window.location.href.split('#')[0] + '#tab2';
                window.location.reload();
                }
            } else {
                window.location.href = window.location.href.split('#')[0] + '#tab3';
                window.location.reload();

            }
        });
    });
    function paymentClick() {
      var hiddenValue = $('input[name="hiddenField"]').val();
            if (hiddenValue === "1") {
                if (confirm("The category item amount is currently empty. Are you sure you want to proceed to the next tab?")) {
                    window.location.href = window.location.href.split('#')[0] + '#tab3';
                    window.location.reload();
                }
                else{
                  window.location.href = window.location.href.split('#')[0] + '#tab2';
                window.location.reload();
                }
            } else {
                window.location.href = window.location.href.split('#')[0] + '#tab3';
              

            }
    }

    $(document).on('change', '.category_form :input', function() {
        var form_id = $(this).parents("form").attr("id");
        var collapse_id = $(this).parents(".panel-collapse").attr('id');
        itemFieldChange(form_id, collapse_id);
    });

    $(document).on('click', '.edit', function() {
        var id = $(this).attr('id');
        $.ajax({
            type: "POST",
            data: {
                id: id,
            },
            dataType: 'JSON',
            url: "<?php echo Yii::app()->createUrl("subcontractor/getItemById") ?>",
            success: function(data) {
                $.each(CKEDITOR.instances, function(i) {
                    var editor = CKEDITOR.instances[i];
                    editor.destroy();
                });
                $('.addItem').addClass('show');
                console.log(data.item_id);
                var collapseElement = $('#collapse_' + data.item_category_id);
                collapseElement.collapse('show');
               $('#collapse_' + data.item_category_id).find('#ScquotationItems_item_description-' + data.item_category_id).val(data.item_description);
                CKEDITOR.replaceAll('text_editor');
                $('#collapse_' + data.item_category_id).find('.item_save').attr('id', data.item_id);
                if (data.item_type == 2) {
                    $('#collapse_' + data.item_category_id).find('#quantity_div').hide();
                    $('#collapse_' + data.item_category_id).find('#rate_div').hide();
                    $('#collapse_' + data.item_category_id).find('#amount_div input').attr('readonly',false);
                } else {
                    $('#collapse_' + data.item_category_id).find('#quantity_div').show();
                    $('#collapse_' + data.item_category_id).find('#rate_div').show();
                    $('#collapse_' + data.item_category_id).find('#amount_div  input').attr('readonly',true);
                }
                $('#collapse_' + data.item_category_id).find('#ScquotationItems_item_id').val(data.item_id);
                $('#collapse_' + data.item_category_id).find("#ScquotationItems_item_type option[value = '" + data.item_type + "']").attr('selected', 'selected')
                $('#collapse_' + data.item_category_id).find('#ScquotationItems_item_quantity').attr('value', data.item_quantity);
                $('#collapse_' + data.item_category_id).find('#ScquotationItems_item_unit').attr('value', data.item_unit);
                $('#collapse_' + data.item_category_id).find('#ScquotationItems_item_rate').attr('value', data.item_rate);
                $('#collapse_' + data.item_category_id).find('#ScquotationItems_item_amount').attr('value', data.item_amount);
            }
        })
    })

    $(document).on('click', '.delete-category', function() {
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            var id = $(this).attr("id");
            if (id != null) {
                $.ajax({
                    type: "POST",
                    data: {
                        id: id
                    },
                    url: "<?php echo $this->createUrl('subcontractor/deleteCategoryData') ?>",
                    dataType: "JSON",
                    success: function(result) {
                        if (result.response == 'success') {
                            $().toastmessage('showSuccessToast', "" + result.msg + "");
                            $('#item_table').load(document.URL + ' #item_table');
                            window.location.href = window.location.href.split('#')[0] + '#tab2';
                          window.location.reload();  
                        } else {
                            $().toastmessage('showErrorToast', "" + result.msg + "");
                        }

                    }

                });
            }

        } else {

            return false;
        }

    });

    $(document).on('click', '.delete_item', function() {
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            var id = $(this).attr("id");
            if (id != null) {
                $.ajax({
                    type: "POST",
                    data: {
                        id: id
                    },
                    url: "<?php echo $this->createUrl('subcontractor/deleteCategory') ?>",
                    dataType: "JSON",
                    success: function(result) {
                        if (result.response == 'success') {
                            $().toastmessage('showSuccessToast', "" + result.msg + "");
                            $('#item_table').load(document.URL + ' #item_table');
                            window.location.href = window.location.href.split('#')[0] + '#tab2';
                            window.location.reload();
                        } else {
                            $().toastmessage('showErrorToast', "" + result.msg + "");
                        }

                    }

                });
            }

        } else {

            return false;
        }

    });


    $(document).on('change', '#ScquotationItems_item_quantity', function() {
     var quantity = $('#ScquotationItems_item_quantity').val();
     var item_id = $('#ScquotationItems_item_id').val();   
     var quotation_id = $('#ScquotationItems_scquotation_id').val();
     $.ajax({
            type: "POST",
            data: {'id': item_id,'quotation_id': quotation_id,},
            dataType: 'JSON',
            url: "<?php echo Yii::app()->createUrl("subcontractor/getitemquantity") ?>",
            success: function(data) {
                if (data.executed_quantity!='') {
                    if(quantity < data.executed_quantity){
                        $().toastmessage('showErrorToast', "Quantity should not less than billed quantity ");
                        $('#ScquotationItems_item_quantity').val(data.old_quantity);
                    }
                }
            }
        })
    });

    

    function itemDescFieldChange(form_id, collapse_id) {
        var categ_id = $('#' + form_id).find("#ScQuotationItemCategory_id").val();
        var form = $('#' + form_id);
        var key_val = form.find('textarea[data-key-val]').data('key-val');
        var categ_desc = form.find("#ScQuotationItemCategory_main_description-" + key_val).val();
        if(categ_desc==null){
          var categ_desc = form.find("#ScQuotationItemCategory_main_description").val();
        }
        $.ajax({
            type: "POST",
            data: {
                'id': categ_id,
                'description': categ_desc,
            },
            dataType: 'JSON',
            url: "<?php echo Yii::app()->createUrl("subcontractor/addCategoryDesc") ?>",
            success: function(data) {
                console.log(data);

                if (data.status === 1) {
                    $().toastmessage('showSuccessToast', data.message);
                    window.location.href = window.location.href.split('#')[0] + '#tab2';
                    window.location.reload();
                } else {
                    $().toastmessage('showWarningToast', data.message);
                }

            }
        })
    }
    function getDynamicQuotNo(company_id){
        
        $.ajax({
            type: "POST",
            data: {
                'company_id': company_id,
            
            },
            dataType: 'JSON',
            url: "<?php echo Yii::app()->createUrl("subcontractor/getDynamicQuotNo") ?>",
            success: function(data) {
            
                console.log(data);

                if (data.status === 1) {
                    $("#Scquotation_scquotation_no").val(data.scqno);
                    $("#Scquotation_scquotation_no").attr('readonly','readonly');
                } else if(data.status === 2){
                    $("#Scquotation_scquotation_no").val(data.default_sqno);
                    $("#Scquotation_scquotation_no").attr('readonly','readonly');
                }else{
                    $("#Scquotation_scquotation_no").val('');
                }

            }
        })
    }


    function itemFieldChange(form_id, collapse_id) {
        var allRequired = true;
        var $inputs = $('#' + form_id + ' :input').filter('.require');
        var values = {};
        $inputs.each(function() {
            if ($(this).val() == '') {
                allRequired = false;
            }
            values[this.name] = $(this).val();
        });
        if (allRequired) {
            // $('.loading-overlay').addClass('is-active');
            $.ajax({
                type: "POST",
                data: $('#' + form_id).serialize(),
                dataType: 'JSON',
                url: "<?php echo Yii::app()->createUrl("subcontractor/addCategory") ?>",
                success: function(data) {

                    if (data.status === 1) {
                        if (typeof collapse_id === "undefined") {
                        } else {
                            $("div").find(`[data-target='#` + collapse_id + `']`).find('.categ_item').text(data.model.main_title);
                        }
                        $("#addCategory").find('#ScQuotationItemCategory_id').val(data.model.id);
                        $().toastmessage('showSuccessToast', data.message);
                        if (data.model.main_title != "" && data.model.main_description != "" && data.model.id != "") {
                            console.log("data here");
                            // location.reload();
                            id=data.model.sc_quotaion_id;
                            window.location.href = '<?php echo Yii::app()->createAbsoluteUrl('subcontractor/addquotation&scquotation_id='); ?>' + id + '&activateTab=2';

                            $(window).on('load', function() {
                                var stepToActivate = 2;
                                activateTab(stepToActivate);
                            });
                            $('.loading-overlay').addClass('is-active');
                        } else {
                            console.log("no data !!!");
                        }
                    } else {
                        $().toastmessage('showWarningToast', data.message);
                    }
                    $("#" + form_id).find('.text-danger').addClass('hide');
                    // location.reload();
                    
                }
            })
        } else {
            $("#" + form_id).find('.text-danger').removeClass('hide');
        }
    }
    $(document).on('click', '.permission_item', function(e) {
        e.preventDefault();
        var element = $(this);
        var item_id = $(this).attr('id');
        $('#loading').show();
        $.ajax({
            url: '<?php echo  Yii::app()->createAbsoluteUrl('subcontractor/permissionapprove'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                item_id: item_id
            },
            success: function(response) {
                $(".popover").removeClass("in");
                if (response.response == 'success') {
                    $(".approveoption_" + item_id).hide();
                    element.closest('tr').removeClass('permission_style');
                    $().toastmessage('showSuccessToast', "" + response.msg + "");
                    if (response.mail_send == 'Y') {
                        $('.mail_send').show();
                    } else {
                        $('.mail_send').hide();
                    }
                } else if (response.response == 'warning') {
                    $(".approveoption_" + item_id).hide();
                    element.closest('tr').removeClass('permission_style');
                    $().toastmessage('showWarningToast', "" + response.msg + "");
                } else {
                    $().toastmessage('showErrorToast', "" + response.msg + "");
                }
            }
        });
    });
    // $(".btn_reset").click(function() {

    // })

    $(".item_save").click(function(e) {
        // Find the panel-body containing the current item
        var panelBody = $(this).closest('.panel-body');  
        var rows = panelBody.find(".quotation_items_form");
        console.log(rows.html());
        var x = rows.filter(".no_data").length;
        rows.each(function() {
            var iframe_content = $(this).find(".desc_sec").find("div.cke.cke_reset").find("iframe").contents().find("body").html();
            console.log(iframe_content);
            var errorContainerDesc = $(this).find(".error-discription"); 
            if (iframe_content == "<p><br></p>") {
                $(this).removeClass('data_here');
                $(this).addClass('no_data');
                errorContainerDesc.text("Description cannot be empty."); // Set the error message
                errorContainerDesc.show(); // Display the error message
            } else {
                $(this).removeClass('no_data');
                $(this).addClass('data_here');
                errorContainerDesc.hide(); // Hide the error message if valid
            }

            var dropdownType = $(this).find(".item_type");
            var errorContainerType = $(this).find(".error-type"); 
            var selectedValue = dropdownType.val();
            errorContainerType.text(""); 
            if (!selectedValue) {
                isValid = false;
                errorContainerType.text("Please select a type.");
                x=1;
                errorContainerType.show();
            } else {
              errorContainerType.hide();
              x=0;
            }
            var quantityInput = $(this).find(".quantity");
            var errorContainerQuantity = $(this).find(".error-quantity");
            errorContainerQuantity.text(""); 
            if (quantityInput.length && (!quantityInput.val() || isNaN(quantityInput.val()) || parseFloat(quantityInput.val()) <= 0)) {
                isValid = false;
                errorContainerQuantity.text("Please enter a valid quantity.");
                errorContainerQuantity.show();
                x=1;
            } else {
                errorContainerQuantity.hide();
                x=0;
            }
              // Validate Unit
              var unitInput = $(this).find(".unit");
            var errorContainerUnit = $(this).find(".error-unit");
            errorContainerUnit.text("");
            if (unitInput.length && (!unitInput.val() || unitInput.val().trim() === "")) {
                isValid = false;
                errorContainerUnit.text("Please enter a valid unit.");
                errorContainerUnit.show();
                x=1;
            } else {
                errorContainerUnit.hide();
                x=0;
            }

            // Validate rate
            var rateInput = $(this).find(".rate");
            var errorContainerRate = $(this).find(".error-rate");
            errorContainerRate.text("");
            if (rateInput.length && (!rateInput.val() || isNaN(rateInput.val()) || parseFloat(rateInput.val()) <= 0)) {
                isValid = false;
                errorContainerRate.text("Please enter a valid rate.");
                errorContainerRate.show();
                x=1;
            } else {
                errorContainerRate.hide();
                x=0;
            }
            var amountInput = $(this).find(".form-control.amount");
            var errorContainerAmount = $(this).find(".error-amount");
            errorContainerAmount.text("");
            if (amountInput.length && (!amountInput.val() || isNaN(amountInput.val()) || parseFloat(amountInput.val()) <= 0)) {
                isValid = false;
                errorContainerAmount.text("Please enter a valid amount.");
                errorContainerAmount.show();
                x=1;
            } else {
                errorContainerAmount.hide();
                x=0;
            }
        });
        
        isValid = true;
      
        // If there are any rows with no data, show an alert and prevent form submission
        if (x > 0) {
            return false;
        } else {
            $(this).parents(".scquotation-items-form").submit(); // Submit the form
            return true;
        }
    });
    $(".remove_row").click(function(e) {
    e.preventDefault(); 
        var panelBody = $(this).closest('.panel').find('.panel-body');
        var rows = panelBody.find(".quotation_items_form");
        var row_len = rows.children().length;
        if (row_len > 0) {
            rows.find(".form_datas:last").remove();
        }
        else{
          panelBody.find(".remove_row").hide();
        }
    });

    $(document).on("click",".addstage",function(){ 
        var total_input_percentage = 0;
		$(".panel-body .row .percentage").each(function () {
			var percentage = parseFloat($(this).val());
			if(!isNaN(percentage)){
				total_input_percentage = total_input_percentage + percentage;
			}
		});
		var total_stagepercent = $(".total_stagepercent").val();
		var input1 = parseFloat(total_stagepercent);
  		var input2 = parseFloat(total_input_percentage);
		var input3 = parseFloat($(".selected_percent").val());
		if (isNaN(input1) || isNaN(input2)) {
			$('#total').text('Both inputs must be numbers');
		} else {          
			var total = input1 + input2 - input3;
		}
		if(total > 100){
			alert("Sum of percentages must  be <= 100");
			return false;
		}else{
            $(this).attr("disabled","disabled");
            var submitData = $(".stage-form");
            $(submitData).each(function(){
                var id = $(this).find(".stage_id").val();
                var quotation_id = $(this).find("#Scquotation_scquotation_id").val();
                var payment_stage = $(this).find(".payment_stage").val();
                var floated_percentage = $(this).find(".floated_percentage").val();
                var stage_percentage = $(this).find(".percentage").val();
                var stage_amount = $(this).find(".stage_amount").val();
                var serial_number = $(this).find(".serial_number").val();
                var quotation_amount = $("#quotation_amount").val();
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('subcontractor/addPaymentStage'); ?>',
                    method: 'POST',
                    data: {
                        id:id,
                        quotation_id: quotation_id,
                        payment_stage:payment_stage,
                        stage_percent:stage_percentage,
                        floated_percentage:floated_percentage,
                        stage_amount:stage_amount,
                        serial_number : serial_number,
                        quotation_amount:quotation_amount
                    },
                    dataType: "json",
                    success: function(response) {
                       //alert(response);                     
                    },
                    complete:function(){
                        $().toastmessage('showSuccessToast', "Payment Stage Added Successfully");
                        setTimeout(function(){
                          window.location.href = window.location.href.split('#')[0] + '#tab3';
                          window.location.reload();  
                        }, 1000);
                    }
                })
            })
        }
    })
    $(document).ready(function() {
    var hash = window.location.hash;
    if (hash === '#tab3') {
        // Activate the third tab
        activateTabDiv(3);
    }else if(hash === '#tab2'){
      activateTabDiv(2);
    }
});

	$(document).on("click",".editStage",function(){
		var id = $(this).attr("data-id");
		$.ajax({
            url: '<?php echo Yii::app()->createUrl('subcontractor/getPaymentStage'); ?>',
            method: 'POST',
            data: {
                id: id,                
            },
            dataType: "json",
            success: function(response) {
				
				$(".stage_id").val(response.id);
				$(".payment_stage").val(response.payment_stage);
				$(".floated_percentage").val(response.stage_percent);
                $(".percentage").val(response.stage_percent);
                $(".selected_percent").val(response.stage_percent); 
				$(".stage_amount").val(response.stage_amount);
                $(".serial_number").val(response.serial_number);
                $(".more_sec").hide();
            }
        })
	})

    $(document).on("click",".deleteStage",function(){
		var id = $(this).attr("data-id");
    if (confirm("Are you sure you want to delete this payment stage?")) {
		    $.ajax({
            url: '<?php echo Yii::app()->createUrl('subcontractor/removePaymentStage'); ?>',
            method: 'POST',
            data: {
                id: id,                
            },
            dataType: "json",
            success: function(response) {
				$("#msg_box").html('<div class="alert alert-'+response.response+' alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'+response.msg+'</div>');
                setTimeout(function(){
                  window.location.href = window.location.href.split('#')[0] + '#tab3';
                  window.location.reload();
                }, 1000);
            }
        })
      }
	});
  function redirectToBill(quotationId, qStageId, approveStatus) {
    if (approveStatus == 'No') {
        if (confirm("This quotation is not approved. Do you want to approve it to convert to a bill?")) {
            approve(quotationId, qStageId, function() { // Call approve with a callback
                var url = "<?php echo Yii::app()->createUrl('subcontractorbill/createquotationbill'); ?>";
                url += "&quotation_id=" + quotationId + "&q_stage_id=" + qStageId;
                window.open(url, '_blank');
            });
        }
    } else {
        var url = "<?php echo Yii::app()->createUrl('subcontractorbill/createquotationbill'); ?>";
        url += "&quotation_id=" + quotationId + "&q_stage_id=" + qStageId;
        window.open(url, '_blank');
    }
}

function approve(quotationId, qStageId, callback) {
    $.ajax({
        url: '<?php echo Yii::app()->createAbsoluteUrl('subcontractor/approveScItems'); ?>',
        type: 'POST',
        data: {
            quotation_id: quotationId
        },
        success: function(data) {
            if (data == 1) {
                $("#errorMsg").show()
                    .html('<div class="alert alert-success"><b>Success!</b> Items Approved Successfully.</div>')
                    .fadeOut(5000);
                setTimeout(function() {
                  
                }, 3000);
                // Call the callback function after approval success
                if (callback) {
                    callback();
                }
            } else {
                $("#errorMsg").show()
                    .html('<div class="alert alert-danger"><b>Error Occured.</b></div>')
                    .fadeOut(5000);
            }
        }
    });
}

	$(document).on("keyup change",".percentage",function(){
        
		var stage_percentage =parseFloat($(this).val());
        var total_input_percentage = 0;
		$(this).parents(".append_div").find(".percentage").each(function(){			
			total_input_percentage = total_input_percentage + parseFloat($(this).val());
		})

        var quotation_amount = $("#quotation_amount").val();
		var stage_amount = parseFloat(quotation_amount * (stage_percentage / 100));
		$(this).parents(".row").find(".stage_amount").val(stage_amount);
        $(this).parents(".row").find(".floated_percentage").val(stage_percentage);

        var total_stagepercent = parseFloat($(".total_stagepercent").val());
		var input1 = parseFloat(total_stagepercent);        
  		var input2 = parseFloat(total_input_percentage);
        var input3 = parseFloat($(".selected_percent").val());
        console.log(input1,input2,input3);
		if (isNaN(input1) || isNaN(input2)) {
			$('#total').text('Both inputs must be numbers');
		} else {          
			var total = input1 + input2- input3;
		}

		if(stage_percentage > 100){
			$(this).siblings('.error').text("Must be less than 100");
			$(".addstage").attr("disabled",true);
            $(this).parents(".row").find(".add_more_stage").attr("disabled",true);
		}else if(stage_percentage <=0){
            $(this).siblings('.error').text("Must be greater than Zero");
			$(".").attr("disabled",true);
            $(this).parents(".row").find(".add_more_stage").attr("disabled",true);
        }else if(total > 100){
			$(this).siblings('.error').text("Sum of percentages must  be <= 100");
			$(".addstage").attr("disabled",true);
            $(this).parents(".row").find(".add_more_stage").attr("disabled",true);
		}else{
			$(this).siblings('.error').text("");
			$(".addstage").attr("disabled",false);
            $(this).parents(".row").find(".add_more_stage").attr("disabled",false);
		}

        if(stage_percentage.length === 0){
			$(this).siblings('.error').text("");
			$(".addstage").attr("disabled",false);
            $(this).parents(".row").find(".add_more_stage").attr("disabled",false);
		}

	});

    
    $(document).on("keyup change",".stage_amount",function(){
       var stage_amount = parseFloat($(this).val());
       var quotation_amount = parseFloat($("#quotation_amount").val());
       var percentage = parseFloat((stage_amount/quotation_amount)*100);
       if( stage_amount < 0){
         $(this).siblings('.error').text("Amount should not be less than zero");
         $(this).parents(".row").find(".add_more_stage").attr("disabled",true);
       }else if(stage_amount > quotation_amount){
        $(this).siblings('.error').text("Amount should not be greater than Quotation amount");
         $(this).parents(".row").find(".add_more_stage").attr("disabled",true);
      
       }else{
         $(this).siblings('.error').text("");
			$(".addstage").attr("disabled",false);
            $(this).parents(".row").find(".add_more_stage").attr("disabled",false);
		
       }
       if(percentage > 100){
        $(this).parents(".row").find(".percentage").siblings('.error').text("Percentage should be less than 100");
         
       }else{
        $(this).parents(".row").find(".percentage").siblings('.error').text("");
        $(".addstage").attr("disabled",false);
        $(this).parents(".row").find(".add_more_stage").attr("disabled",false);
       }
       $(this).parents(".row").find(".percentage").val(isNaN(percentage) ? "" : percentage.toFixed(2));
       $(this).parents(".row").find(".floated_percentage").val(isNaN(percentage) ? "" : percentage);
       var total_input_percentage = 0;
       $(this).parents(".append_div").find(".percentage").each(function(){     
        total_input_percentage = total_input_percentage + parseFloat($(this).val());
      });
        var stage_percentage =parseFloat($(this).parents(".row").find(".percentage").val());
        var total_stagepercent = parseFloat($(".total_stagepercent").val());
        var input1 = parseFloat(total_stagepercent);        
        var input2 = parseFloat(total_input_percentage);
        var input3 = parseFloat($(".selected_percent").val());
        if (isNaN(input1) || isNaN(input2)) {
        $('#total').text('Both inputs must be numbers');
        } else {          
        var total = input1 + input2- input3;
        }
        if(total > 100){
            $(this).parents(".row").find(".percentage").siblings('.error').text("Sum of percentages must  be <= 100");
        $(".addstage").attr("disabled",true);
                $(this).parents(".row").find(".add_more_stage").attr("disabled",true);
        }else{
            $(this).parents(".row").find(".percentage").siblings('.error').text("");
            $(".addstage").attr("disabled",false);
            $(this).parents(".row").find(".add_more_stage").attr("disabled",false);
        }
    });

    $(".remove_stage").hide();
	$(document).on("click",".add_more_stage",function(){
		var clone = $(this).parents(".row").clone();
		$(clone).find("input[type=text], input[type=number]").each(function() {
			if (!$(this).hasClass("serial_number")) {
				$(this).val("");
			}else{
				var currentValue = $(this).val();
				var incrementedValue = parseInt(currentValue) + 1; // Increment the value
				$(this).val(incrementedValue); 
			}
    	});
      $(clone).addClass("cloned-row");
        var appendParent = $(this).parents(".append_div");
        $(clone).appendTo(appendParent);
		$(this).parents(".more_sec").hide();
		var row_len = $(".panel-body").find(".row").length;
				
        if (row_len > 1) {
            $(clone).find(".remove_stage").show();
        }
	});
  $("#resetBtn").on("click", function(event) {
    event.preventDefault(); 
    $("#payment-stage")[0].reset();
    $(".payment-stage-new .cloned-row").remove();
    $(".more_sec").show();
});
	$(document).on("click",".remove_stage",function(){
		var selectedrow = $(this).parents(".row");
		var row_len = $(".panel-body").children(".row").length;
        if (row_len > 1) {
            $(selectedrow).remove();            
        }	
		$(".panel-body").find(".row:last-child").find(".more_sec").show();
        $(".panel-body").find(".row:last-child").find(".percentage").trigger("keyup");
        
	});

    $(document).on("change",".sgst_percent,.cgst_percent,.igst_percent",function(){        
        var qtid = $(this).attr("data-qid");
        var sgst_percent = $(".sgst_percent").val();
        var cgst_percent = $(".cgst_percent").val();
        var igst_percent = $(".igst_percent").val();
        $.ajax({
            url: '<?php echo Yii::app()->createUrl('subcontractor/addQuotationTax'); ?>',
            method: 'POST',
            data: {
                qtid :qtid,
                sgst_percent :sgst_percent,
                cgst_percent:cgst_percent,
                igst_percent:igst_percent             
            },       
            success: function(response) {
                $('#tax_div').load(' #tax_div');                
            }
        })
    })
    $(document).ready(function(){
        $('.edit').click(function() {
            var targetId = $(this).closest('.categ_toggle').data('target');
            $(targetId).collapse('toggle');
        });
    });
</script>