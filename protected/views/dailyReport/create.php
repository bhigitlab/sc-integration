<?php
/* @var $this DailyReportController */
/* @var $model Dailyreport */

$this->breadcrumbs=array(
	'Dailyreports'=>array('index'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List Dailyreport', 'url'=>array('index')),
	//array('label'=>'Manage Dailyreport', 'url'=>array('admin')),
);
?>

<div class="container" id="expense">
    <div class="row">
        <div class="col-md-6">
           <h2>Add Dailyreport</h2>
        </div>
        <div class="col-md-6">
            <a href="index.php?r=DailyReport/admin"><span class="btn btn-default" title="List Daily Reports" style="margin-top:10px;">List Daily Reports</span></a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <?php $this->renderPartial('_form', array('model'=>$model,'completion_date'=>$completion_date)); ?>
    
        </div>
    </div>
</div>