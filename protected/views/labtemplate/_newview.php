<?php
/* @var $this ProjectsController */
/* @var $data Projects */
?>
<?php 
$pms_api_integration=0;
$pms_api_integration_model=ApiSettings::model()->findByPk(1);
 $pms_api_integration =$pms_api_integration_model->api_integration_settings;
?>
<style>
    .legend {
        list-style: none;
        padding-left: 0px;

    }

    .legend li {
        float: left;
        margin-right: 10px;
    }

    .legend span {
        border: 1px solid #ccc;
        float: left;
        width: 12px;
        height: 12px;
        margin: 2px;
    }

    .legend .project_not_mapped {
        background-color: #A9E9EC;
        border: 2px solid #f8cbcb;
        margin-right: 4px;
    }
</style>

<?php
if ($index == 0) {
    ?>
   <?php if ($pms_api_integration == 1): ?>
    <ul class="legend">
        <li><span class="project_not_mapped"></span>Labour Template Not Mapped In Integration</li>
    </ul>
    <?php endif; ?>
    <thead>
    <tr> 
    <?php 
        if(isset(Yii::app()->user->role) && (in_array('/labtemplate/update', Yii::app()->user->menuauthlist))){
        ?>
        <th>Action</th>
        <?php } ?>
        <th>SI No</th>
        <th>Template Name</th>
        <th>Description</th>
        <th>Labour Types</th>
        
    </tr>   
    </thead>
<?php } ?>
<?php
 if($data->pms_labour_template_id !=='0'){
        $styleval= '';
    }else{
        $styleval='background-color:#A9E9EC;';
    }             
?>
    <tr style="<?php echo $styleval; ?>"> 
    <td style="width: 50px;">
        <?php 
            if(isset(Yii::app()->user->role) && (in_array('/labtemplate/update', Yii::app()->user->menuauthlist))){
            ?>        
           <a class="fa fa-edit editProject" data-toggle="modal" data-target=".edit" data-id="<?php echo $data->id; ?>"></a>
         <?php } 
         if(isset(Yii::app()->user->role) && (in_array('/labtemplate/delete', Yii::app()->user->menuauthlist))){
            if( $pms_api_integration!=1){
            ?>
            <a class="fa fa-trash deleteProject" data-toggle="modal" data-target=".delete" data-id="<?php echo $data->id; ?>"></a>
         <?php } }?>
        </td>    
        <td style="width: 50px;"><?php echo $index+1; ?></td>
        <td>
            <span><?php echo $data->template_label ?></span>            
        </td>        
        <td>
        <span><?php echo $data->description ?></span>   
        </td>
        <?php 
        $labour_type_arr = '';
        $labour_type_arr = explode(',',$data->labour_type);
        $labour_type='';
        //echo "<pre>";print_r($labour_type_arr);exit;
        $combined_worktype=array();
        if(!empty($labour_type_arr)){
            for($i=0; $i<count($labour_type_arr);$i++){
                $worktype = LabourWorktype::Model()->findByPK($labour_type_arr[$i]);
                if(!empty($worktype)){
                    
                    $wrktype_name= $worktype['worktype']." (".$worktype['rate'].")";
                   array_push($combined_worktype,$wrktype_name) ;
                  // echo "<pre>";print_r($combined_worktype);exit;
                }
               
            }
           $labour_type=implode(',',$combined_worktype) ;
        }
         ?>
        <td>
        <span><?php echo  $labour_type ?></span>   
        </td>
            
    </tr>  
