<?php
if ($index == 0) {
?>

<?php } ?>
<?php
$tblpx = Yii::app()->db->tablePrefix;
if ($index == 0) {
?>
	<thead class="entry-table">
		<tr>
			<th>Sl No.</th>
			<th>Bill No</th>
			<th>Purchase No</th>
			<th>Bill date</th>
			<th>Vendor</th>
			<th>Item</th>
			<th>Quantity</th>
			<th>Rate</th>
			<th>Amount</th>
			<th>Total Amount</th>
		</tr>
	</thead>
<?php
}
?>

<tbody>
	<tr>
		<td><?php echo ($index + 1); ?></td>
		<td>
			<?php
			if ($data['purchase_no'] != NULL) {
			?>
				<a href="index.php?r=bills/view&id=<?php echo $data['bill_id']; ?>" title="View Bill" target="_blank"><?php echo $data['bill_number']; ?></a>
			<?php } else { ?>
				<a href="index.php?r=bills/billview&id=<?php echo $data['bill_id']; ?>" title="View Bill" target="_blank"><?php echo $data['bill_number']; ?></a>
			<?php } ?>
		</td>
		<td>
			<?php
			if ($data['purchase_no'] != NULL) {
			?>
				<a href="index.php?r=purchase/viewpurchase&pid=<?php echo $data['p_id']; ?>" title="View Purchase" target="_blank"><?php echo $data['purchase_no']; ?></a>
			<?php } ?>
		</td>
		<td><?php echo date('Y-m-d', strtotime($data['bill_date'])); ?></td>
		<td>
			<?php
			$vendor = Vendors::model()->findByPk($data['vendor_id']);
			echo $vendor['name']; ?>
		</td>
		<td>
			<?php
			if ($data['category_id'] != NULL) {
				$sql = "SELECT id, cat_id, brand_id, specification, unit"
					. " FROM {$tblpx}specification "
					. " WHERE id=" . $data['category_id'] . "";
				$specification  = Yii::app()->db->createCommand($sql)->queryRow();
				if ($specification['brand_id'] != NULL) {
					$brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $specification['brand_id'] . "")->queryRow();
					$brand = '-' . ucwords($brand_details['brand_name']);
				} else {
					$brand = '';
				}
				$catsql = "SELECT category_name "
					. " FROM {$tblpx}purchase_category "
					. " WHERE id=" . $specification['cat_id'] . "";
				$category  = Yii::app()->db->createCommand($catsql)->queryRow();
				echo ucwords($category['category_name']) . $brand . '-' . ucwords($specification['specification']);
			} else {
				echo "";
			}
			?>
		</td>
		<td style="text-align: right"><?php echo $data['billitem_quantity']; ?></td>
		<td style="text-align: right"><?php echo Controller::money_format_inr($data['billitem_rate'], 2); ?></td>
		<td style="text-align: right"><?php echo Controller::money_format_inr($data['billitem_amount'], 2); ?></td>
		<td style="text-align: right"><?php echo Controller::money_format_inr(($data['billitem_amount'] + $data['billitem_taxamount']) - $data['billitem_discountamount'], 2); ?></td>
	</tr>
	</tfoot>