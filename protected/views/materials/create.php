<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<?php
/* @var $this MaterialsController */
/* @var $model Materials */

$this->breadcrumbs=array(
	'Materials'=>array('index'),
	'Create',
);

?>

<div class="panel panel-gray">
<div class="panel-heading form-head">
        <h3 class="panel-title">Add Material</h3>
    </div>
        <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
