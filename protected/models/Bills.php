<?php

/**
 * This is the model class for table "{{bills}}".
 *
 * The followings are the available columns in table '{{bills}}':
 * @property integer $bill_id
 * @property integer $purchase_id
 * @property string $bill_number
 * @property string $bill_date
 * @property double $bill_amount
 * @property double $bill_totalamount
 * @property integer $created_by
 * @property string $created_date
 * @property string $updated_date
 */
class Bills extends CActiveRecord
{
	public $date_from;
	public $date_to;
	public $created_date_from;
	public $created_date_to;
	public $purchase_no;
	public $project_id;
	public $vendor_id;
	public $expensehead_id;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{bills}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('purchase_id, bill_number, bill_date, bill_amount, bill_totalamount, created_by', 'required'),
			array('purchase_id, created_by, bill_status', 'numerical', 'integerOnly' => true),
			array('bill_amount, bill_taxamount, bill_discountamount, bill_totalamount', 'numerical'),
			array('created_date, updated_date,round_off', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('bill_id, purchase_id, bill_number, bill_date, bill_amount, bill_taxamount, bill_discountamount, bill_totalamount, created_by, created_date, updated_date, bill_status, date_from,date_to,purchase_no,project_id,vendor_id,expensehead_id', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'bill_id' => 'Bill',
			'purchase_id' => 'Purchase',
			'bill_number' => 'Bill Number',
			'bill_date' => 'Bill Date',
			'bill_amount' => 'Amount',
			'bill_taxamount' => 'Tax Amount',
			'bill_discountamount' => 'Discount Amount',
			'bill_totalamount' => 'Total Amount',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_date' => 'Updated Date',
			'bill_status' => 'Bill Status',
			'company_id' => 'Company',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		$tblpx = Yii::app()->db->tablePrefix;

		$criteria->compare('bill_id', $this->bill_id);
		$criteria->compare('purchase_id', $this->purchase_id);
		$criteria->compare('bill_number', $this->bill_number);
		$criteria->compare('bill_date', $this->bill_date, true);
		$criteria->compare('bill_amount', $this->bill_amount);
		$criteria->compare('bill_taxamount', $this->bill_taxamount);
		$criteria->compare('bill_discountamount', $this->bill_discountamount);
		$criteria->compare('bill_totalamount', $this->bill_totalamount);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_date', $this->updated_date, true);
		$criteria->compare('bill_status', $this->bill_status, true);
		//$criteria->addCondition("company_id =".Yii::app()->user->company_id."");
		$userId         = Yii::app()->user->id;
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$newQuery = "";
		$newQuery1 = " AND 1=1";
		if($this->company_id!=''){
			$newQuery .= "t.company_id =".$this->company_id."";
		}else{
			foreach ($arrVal as $arr) {
				if ($newQuery) $newQuery .= ' OR';
				$newQuery .= " FIND_IN_SET('" . $arr . "', t.company_id)";
			}
		}
		if (!empty($this->date_from) && empty($this->date_to)) {
			$newQuery1 .= " AND t.bill_date  >= '" . date("Y-m-d", strtotime($this->date_from)) . "'";
		} elseif (!empty($this->date_to) && empty($this->date_from)) {
			$newQuery1 .= " AND t.bill_date  <= '" . date("Y-m-d", strtotime($this->date_to)) . "'";
		} elseif (!empty($this->date_to) && !empty($this->date_from)) {
			$newQuery1 .= " AND t.bill_date between '" . date('Y-m-d', strtotime($this->date_from)) . "' and  '" . date('Y-m-d', strtotime($this->date_to)) . "'";
		}
		if (!empty($this->created_date_from) && empty($this->created_date_to)) {
			$newQuery1 .= " AND t.created_date  >= '" . date("Y-m-d", strtotime($this->created_date_from)) . "'";
		} elseif (!empty($this->created_date_to) && empty($this->created_date_from)) {
			$newQuery1 .= " AND t.created_date  <= '" . date("Y-m-d", strtotime($this->created_date_to)) . "'";
		} elseif (!empty($this->created_date_to) && !empty($this->created_date_from)) {
			$newQuery1 .= " AND t.created_date between '" . date('Y-m-d', strtotime($this->created_date_from)) . "' and  '" . date('Y-m-d', strtotime($this->created_date_to)) . "'";
		}
		if (!empty($this->purchase_no)) {
			$newQuery1 .= " AND pu.purchase_no ='" . $this->purchase_no . "' ";
		}
		if (!empty($this->vendor_id)) {
			$newQuery1 .= " AND pu.vendor_id ='" . $this->vendor_id . "' ";
		}
		if (!empty($this->project_id)) {
			$newQuery1 .= " AND pu.project_id ='" . $this->project_id . "' ";
		}
		if(!empty($this->expensehead_id)){
			$newQuery1 .= " AND pu.expensehead_id ='" . $this->expensehead_id . "' ";
		}

		//die($newQuery1);
		$criteria->select = 't.*,pu.purchase_no,pu.vendor_id,pu.project_id,e.type_id';
		$criteria->join = 'LEFT JOIN ' . $tblpx . 'purchase pu ON t.purchase_id = pu.p_id  LEFT JOIN ' . $tblpx . 'projects pr ON pr.pid = pu.project_id LEFT JOIN ' . $tblpx . 'project_assign pa ON pr.pid = pa.projectid LEFT JOIN ' . $tblpx . 'expense_type e ON e.type_id = pu.expensehead_id' ;
		$criteria->addCondition('pa.userid = ' . $userId . ' AND (' . $newQuery . ') ' . $newQuery1 . '');
		//$criteria->addCondition("purchase_id IS NOT NULL");
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array('pageSize' => 20,),
			//   'pagination'=>false
		));
	}

	public function billwithoutpo()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('bill_id', $this->bill_id);
		$criteria->compare('purchase_id', $this->purchase_id);
		$criteria->compare('bill_number', $this->bill_number);
		$criteria->compare('bill_date', $this->bill_date, true);
		$criteria->compare('bill_amount', $this->bill_amount);
		$criteria->compare('bill_taxamount', $this->bill_taxamount);
		$criteria->compare('bill_discountamount', $this->bill_discountamount);
		$criteria->compare('bill_totalamount', $this->bill_totalamount);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_date', $this->updated_date, true);
		$criteria->compare('bill_status', $this->bill_status, true);
		$criteria->addCondition("company_id =" . Yii::app()->user->company_id . "");
		$criteria->addCondition("purchase_id IS NULL");

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Bills the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public static function warehousename($warehouse_id)
	{
		$warehouse_id        = $warehouse_id;
        $warehousedetails    = Warehouse::model()->findByPk($warehouse_id);
        $warehouse_name      = $warehousedetails["warehouse_name"];
        return $warehouse_name;
		
	}
	
	public function getAllPurchaseItemsById($pId)
	{
		$prId = $pId;
		$pData = "";
		$tblpx = Yii::app()->db->tablePrefix;
		//$pData = Yii::app()->db->createCommand("select * from ".$tblpx."purchase_item where purchase_id = ".$pId."")->queryAll();



		$pData = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "purchase_items WHERE purchase_id = " . $prId . " ORDER BY item_id")->queryAll();
		/*foreach($pData as $purchase){
                        $pitem["id"][]          = $purchase['item_id'];
                        $pitem["purchase_id"][] = $purchase['purchase_id'];
                        $pitem["description"][] = $purchase['description'];
                        $pitem["quantity"][]    = $purchase['quantity'];
                        $pitem["unit"][]        = $purchase['unit'];
                        $pitem["rate"][]        = $purchase['rate'];
                        $pitem["amount"][]      = $purchase['amount'];
            }*/
		//echo "<pre>";
		return json_encode($pData);
	}

	public function getBillTaxTotal($bill_id)
	{
		$bill_tax_sum = Billitem::model()->find(array('select' => 'sum(billitem_cgst) as billitem_cgst,sum(billitem_sgst) as billitem_sgst,sum(billitem_igst) as billitem_igst', 'condition' => 'bill_id = ' . $bill_id));
		$tax_total = $bill_tax_sum['billitem_cgst'] + $bill_tax_sum['billitem_sgst'] + $bill_tax_sum['billitem_igst'];
		return $tax_total;
	}

	public function getTotalAmount($bill_id){
		$tblpx = Yii::app()->db->tablePrefix;
		$data1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as bill_amount FROM {$tblpx}billitem WHERE bill_id=" . $bill_id)->queryRow();

		$data2 = Yii::app()->db->createCommand("SELECT SUM(billitem_taxamount) as bill_taxamount FROM {$tblpx}billitem WHERE bill_id=" . $bill_id)->queryRow();

		$data3 = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as bill_discountamount FROM {$tblpx}billitem WHERE bill_id=" . $bill_id)->queryRow();

		$addcharges = Yii::app()->db->createCommand("SELECT SUM(amount) as amount FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $bill_id)->queryRow();

		$roundoff = Yii::app()->db->createCommand("SELECT round_off as round_off FROM {$tblpx}bills WHERE bill_id=" . $bill_id)->queryRow();

		$sql = "SELECT SUM(billitem_sgst)+SUM(billitem_cgst)+SUM(billitem_igst) "
				. " AS total_tax FROM `jp_billitem` "
				. " WHERE bill_id=". $bill_id;
		$total_tax = Yii::app()->db->createCommand($sql)->queryScalar();

		$net_amount = ($data1['bill_amount'] + $total_tax) - $data3['bill_discountamount'];
		$grand_total = ($net_amount + $addcharges['amount']) + $roundoff['round_off'];

		return $grand_total;
	}

	public function DaybookStatus($bill_id){
		$tblpx = Yii::app()->db->tablePrefix;
		$expanse_data = Yii::app()->db->createCommand("SELECT COUNT(bill_id) as billcount FROM " . $tblpx . "expenses WHERE `bill_id`=" . $bill_id)->queryRow();
		$count_bill = $expanse_data['billcount'];
		return $count_bill;
	}
}
