<?php

class LabtemplateController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        /*return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'newlist'),
                'users' => array('@'),
                'expression' => 'yii::app()->user->role<=3',
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'savetopdf','savetoexcel'),
                'users' => array('@'),
                'expression' => 'yii::app()->user->role<=2',
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
                'expression' => 'yii::app()->user->role==1',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );*/
        $accessArr = array();
        $accessauthArr = array();
        $controller = Yii::app()->controller->id;
        if (isset(Yii::app()->user->menuall)) {
            if (array_key_exists($controller, Yii::app()->user->menuall)) {
                $accessArr = Yii::app()->user->menuall[$controller];
            }
        }

        if (isset(Yii::app()->user->menuauth)) {
            if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                $accessauthArr = Yii::app()->user->menuauth[$controller];
            }
        }
        $access_privlg = count($accessauthArr);
        return array(
            array(
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => $accessArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),

            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        
        $model = new LabourTemplate;
        $template_id='';
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        //echo "<pre>";print_r($_POST);exit;
        if (isset($_POST['LabourTemplate'])) {
            $model->attributes = $_POST['LabourTemplate'];
     
             
        }
        if(isset($_POST['labour_type']) && count($_POST['labour_type'])){
            $type=$_POST['labour_type'];
             $labour_types= implode(',', $type);
            // die($labour_types);
             $model->labour_type=$labour_types;
             $rate='';
        $name='';
        if ($model->save()){
            $template_id= Yii::app()->db->getLastInsertID();
            if(!empty($_POST['labour_type'])){
                foreach($_POST['labour_type'] as $tyval){
                    $workModel=LabourWorktype::Model()->findByPk($tyval);
                    if(!empty($workModel)){
                        $rate=$workModel->rate;
                        $name=$workModel->worktype;
                    }
                    $temp_worktype_model = new LabourTemplateWorktype;
                    $temp_worktype_model->template_id=$template_id;
                    $temp_worktype_model->worktype_id=$tyval;
                    $temp_worktype_model->worktype_rate=$rate;
                    $temp_worktype_model->worktype_name=$name;
                    $temp_worktype_model->created_at=date('Y-m-d H:i:s');
                    $temp_worktype_model->updated_at=date('Y-m-d H:i:s');
                   if(! $temp_worktype_model->save()){
                   // echo "<pre>";print_r($model->getErrors());exit;
                   Yii::app()->user->setFlash('success', "Template Created successfully. ");
                   }
                }
                //API integration
                if ($_POST['execute_api'] == 1) {
                    $pms_api_integration_model = ApiSettings::model()->findByPk(1);
                    $pms_api_integration = $pms_api_integration_model->api_integration_settings;
                    if ($pms_api_integration == 1) {
                        $labours = [];  
                    
                        foreach ($_POST['labour_type'] as $index => $tyval) {
                            $workModel = LabourWorktype::Model()->findByPk($tyval);
                            if (!empty($workModel)) {
                                $labours[] = [
                                    'name' => $workModel->worktype,
                                    'pms_labour_id' => $workModel->pms_labour_id,
                                ];
                            }
                        }
                    
                        // API request payload
                        $request = [
                            'origin' => 'coms',
                            'labour_template_id' => $template_id,
                            'labours' => $labours,
                            'coms_api_log_id' => $model->id,
                            'labour_template_name' => $model->template_label,
                        ];
                    
                        // API call
                        $slug = "api/create-labour-template";
                        $method = "POST";
                        $globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
                    }
                    
                }
                $this->redirect(array('newlist'));
            }
            
          }

        }
        
        if (isset($_GET['layout']))
            $this->layout = false;
        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['LabourTemplate'])) {
            $model->attributes = $_POST['LabourTemplate'];
     
             
        }
        if (isset($_POST['labour_type'])) {
            $selected_types = $_POST['labour_type'];
        } else {
            $selected_types = array();
        }
        if(isset($_POST['labour_type']) && count($_POST['labour_type'])){
            $type=$_POST['labour_type'];
             $labour_types= implode(',', $type);
            // die($labour_types);
             $model->labour_type=$labour_types;
             $rate='';
             $name='';
            if ($model->save()){
                //$template_id= Yii::app()->db->getLastInsertID();
                if(!empty($_POST['labour_type'])){
                    // Get existing types
                    $existing_types = LabourTemplateWorktype::model()->findAllByAttributes(array('template_id' => $id));
                    $existing_type_ids = array();
                    foreach ($existing_types as $existing_type) {
                        $existing_type_ids[] = $existing_type->worktype_id;
                    }
    
                // Determine types to delete (exist in DB but not in posted data)
                $types_to_delete = array_diff($existing_type_ids, $selected_types);
                foreach ($types_to_delete as $type_id) {
                    $existing_type = LabourTemplateWorktype::model()->findByAttributes(array(
                        'template_id' => $id,
                        'worktype_id' => $type_id
                    ));
                    $existing_type->delete();
                }

                // Determine types to add (in posted data but not exist in DB)
                $types_to_add = array_diff($selected_types, $existing_type_ids);
                foreach ($types_to_add as $type_id) {
                    $workModel = LabourWorktype::model()->findByPk($type_id);
                    if (!empty($workModel)) {
                        $rate = $workModel->rate;
                        $name = $workModel->worktype;
                        $temp_worktype_model = new LabourTemplateWorktype;
                        $temp_worktype_model->template_id = $id;
                        $temp_worktype_model->worktype_id = $type_id;
                        $temp_worktype_model->worktype_rate = $rate;
                        $temp_worktype_model->worktype_name = $name;
                        $temp_worktype_model->created_at = date('Y-m-d H:i:s');
                        $temp_worktype_model->updated_at = date('Y-m-d H:i:s');
                        $temp_worktype_model->save();
                    }
                }

                 $pms_api_integration_model = ApiSettings::model()->findByPk(1);
                $pms_api_integration = $pms_api_integration_model->api_integration_settings;
                
                if ($pms_api_integration == 1) {
                    if($_POST['execute_api'] == 1){
                        $request = [
                            'origin' => 'coms',
                            'labour_template_id' => $model->id,
                            'labour_template_name' => $model->template_label
                        ];
                        if (!empty($model->pms_labour_template_id)) {
                            $add_labours = []; 
                            $delete_labours = []; 
                            
                            foreach ($types_to_add as $type_id) {
                                $labour = LabourWorktype::model()->findByPk($type_id);
                                if ($labour) {
                                    $add_labours[] = [
                                        'name' => $labour->worktype,
                                        'pms_labour_id' => $labour->pms_labour_id,
                                    ];
                                }
                            }

                            $request['add_labours'] = $add_labours;
                            $request['delete_labours'] = implode(',', $types_to_delete);
                            $request['pms_labour_template_id'] = $model->pms_labour_template_id;

                            $slug = "api/update-labour-template";
                        } else {
                            // API Create Labour Template
                            $labours = [];
                            foreach ($selected_types as $type_id) {
                                $labour = LabourWorktype::model()->findByPk($type_id);
                                if ($labour) {
                                    $labours[] = [
                                        'name' => $labour->worktype,
                                        'pms_labour_id' => $labour->pms_labour_id,
                                    ];
                                }
                            }

                            $request['labours'] = $labours;
                            $slug = "api/create-labour-template";
                        }
                        // Make the API call
                        $method = "POST";
                        $globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
                    }
                }

                $this->redirect(array('newlist'));
                
                $response = [
                    'success' => 'success',
                    'message' => 'Record Updated successfully.'
                ];
                echo json_encode($response);
            }
            
          }

        }
        
        if (isset($_GET['layout']))
            $this->layout = false;
       
        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    	public function actionDelete($id)
    	{
           
    		$model = $this->loadModel($id);
            try{
                $sql1 = "SELECT COUNT(*) FROM `jp_projects` WHERE labour_template=$id";
				$relatedlabourTypeCount = Yii::app()->db->createCommand($sql1)->queryScalar();
                if(!$relatedlabourTypeCount){    
                    if ($model->delete()) {
                        LabourTemplateWorktype::model()->deleteAllByAttributes(array('template_id' => $id));

                        $response = [
                            'success' => 'success',
                            'message' => 'Record deleted successfully.'
                        ];
                    } else {
                        throw new Exception(json_encode($model->getErrors()));
                    
                    }
                }else{
                    $error_message = "You cannot delete this record because it is associated with other records.";
                    throw new Exception(json_encode($error_message));
                }
            }catch(Exception $e){
                $error_message = "You cannot delete this record because it is associated with other records.";
                $response = [
                    'success' => 'danger',
                    'message' =>$error_message
                ];
            }            
            echo json_encode($response);
            }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('LabourWorktype');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new LabourTemplate('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['LabourTemplate']))
            $model->attributes = $_GET['LabourTemplate'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return LabourWorktype the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = LabourTemplate::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param LabourTemplate $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'labour-template-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionNewlist()
    {
        $model = new LabourTemplate('search');
        $model->unsetAttributes();  // clear any default values

        // If there are GET parameters, apply them to the model
        if (isset($_GET['LabourTemplate']))
            $model->attributes = $_GET['LabourTemplate'];

        $dataProvider = $model->search();

        $this->render('newlist', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionSavetopdf()
    {
        $this->logo = $this->realpath_logo;
        $tblpx = Yii::app()->db->tablePrefix;
        $qry = "SELECT * FROM " . $tblpx . "expense_type";
        $data = Yii::app()->db->createCommand($qry)->queryAll();

        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
        $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $mPDF1->AddPage('','', '', '', '', 0,0,40, 30, 10,0);
        $mPDF1->WriteHTML($this->renderPartial('LabourWorktypepdf', array(
            'model' => $data,
        ), true));
        $filename = "Expense Heads";
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    public function actionSavetoexcel()
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $qry = "SELECT * FROM " . $tblpx . "expense_type";
        $data = Yii::app()->db->createCommand($qry)->queryAll();
        $finaldata = array();
        $arraylabel = array('Sl No.', 'Expense Head');
        $i = 1;
        $finaldata = array();
        foreach ($data as $key => $datavalue) {
            $finaldata[$key][] = $i;
            $finaldata[$key][] = $datavalue['type_name'];
            $i++;
        }

        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'expense_head.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }
}
