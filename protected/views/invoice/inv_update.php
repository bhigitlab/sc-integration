<link rel="stylesheet" type="text/css" href="uploads/new_template/css/newstyle.css" />

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
  <script>
  $( function() {
     $( "#datepicker" ).datepicker({dateFormat: 'dd-mm-yy'});
    
  } );
  </script>

<br/> 


<?php 
$tblpx = Yii::app()->db->tablePrefix;

if(Yii::app()->user->role == 1 || Yii::app()->user->role == 2)
{
	$project = Yii::app()->db->createCommand("SELECT DISTINCT pid, name FROM `{$tblpx}projects` `t` ORDER BY name")->queryAll();
}
else
{
	$project = Yii::app()->db->createCommand("SELECT DISTINCT pid, name FROM `{$tblpx}projects` `t` WHERE pid in (select
projectid from jp_project_assign where userid=83) ORDER BY name")->queryAll();
}
?>

<?php

	
//$id= $model['invoice_id'];
$proid= $model['project_id'];
$sql=Yii::app()->db->createCommand("select {$tblpx}clients.name from {$tblpx}clients
left join {$tblpx}projects ON {$tblpx}projects.client_id={$tblpx}clients.cid 
where {$tblpx}projects.pid=$proid")->queryRow();
?>
<div class="invoicemaindiv" >


                 <a style="cursor:pointer; float:right;" id="download" class="save_btn2 pdf_excel" >
                 <div class="save_pdf"></div>SAVE AS PDF
                 </a>
                 
                 <a style="cursor:pointer; float:right;margin-right:2px;" id="download" class="excelbtn2 pdf_excel" >
                 <div class="save_pdf"></div>SAVE AS EXCEL
                 </a><br/><br/><br/>
                
 
 
 <?php
 //$id = $model['invoice_id'];
 ?>
 <form id="pdfvals2" method="post" action="<?php echo $this->createAbsoluteUrl('invoice/updateinvoice&invid='.$invid)?>">
	
	
	<div class="invoiceheader">
	  <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" style="height:100px;padding:5px;">
	  <textarea class="txtBox pastweek" name="details" placeholder="Please click to edit" style="float:right;width:50%;height:110px"/>
	  3rd Floor,Dr.Rajkrishnan's Building &#13;
	  Nort Fort Gate,Tripunithura,Kochi &#13;&#10;
	  Call:0484 4030077, 9995107007 &#13;
	  Mail:info@jpventures.in
	  </textarea>
	</div>
	<h2>INVOICE</h2>
	
	<div class="inv_client_det">
	<label>BILL TO : </label>
	<input type="text" class="txtBox pastweek" name="billto"   value ="<?php echo $sql['name']; ?>" />
	</div>
	
	<div class="rightdiv">
	<table border="1" style="width:100%;">
	<tr>
		<td>
		<label>DATE : </label>
		<input type="text" id="datepicker" class="txtBox pastweek" name="date"  value ="<?php echo date("d-m-Y", strtotime($model['date']));?>" >
		 <span class="error_message"><?php  echo ((isset($error['date'][0]) && $error['date'][0] != '') ? $error['date'][0] : ''); ?> </span>
		</td>
		</tr>
	<tr>
		<td>
		<label>INVOICE NO : </label>
		<input type="text" class="txtBox pastweek" name="invoiceno" value ="<?php echo $model['inv_no'];?>"/>
		 <span class="error_message"><?php  echo ((isset($error['inv_no'][0]) && $error['inv_no'][0] != '') ? $error['inv_no'][0] : ''); ?> </span>
		<p id="demo"></p>
		</td>
	</tr>
	</table>
	</div>
	<br><br><br><br>
	
	<div class="inv_client_det">
	<label>PROJECT NAME : </label>
	<?php
	if(!empty($projectid))
	{
	$selectedOptions[$projectid] = array('selected'=>'selected');

	echo CHtml::dropdownlist('project[]', '', CHtml::listData($project, 'pid', 'name'), array("style" =>"width:190px",'options'=>$selectedOptions));
	?>
	 <span class="error_message"><?php  echo ((isset($error['project_id'][0]) && $error['project_id'][0] != '') ? $error['project_id'][0] : ''); ?> </span>
	<?php
	}
	else
	{
	$selectedOptions[$model['project_id']] = array('selected'=>'selected');
	echo CHtml::dropdownlist('project[]', '', CHtml::listData($project, 'pid', 'name'), array("style" =>"width:190px",'options'=>$selectedOptions));
	?>
	 <span class="error_message"><?php  echo ((isset($error['project_id'][0]) && $error['project_id'][0] != '') ? $error['project_id'][0] : ''); ?> </span>
	<?php
	}
	
	?>
	 <span class="error_message"><?php  echo ((isset($error['project_id'][0]) && $error['project_id'][0] != '') ? $error['project_id'][0] : ''); ?> </span>
	</div>
	<br>
	<br>
	
	<table border="1" class="table">
			<thead>
				<tr>
					
					<th>Sl.No</th>
					<th>Description</th>
					<th>Quantity</th>
					<th>Unit</th>
					<th>Rate</th>
					<th>Amount</th>
					<th></th>
				</tr>
			</thead>
			<tbody class="addrow">

				<?php
						
				$i=1;
				foreach($newmodel as $new) 
				{
				?>
				<tr>
					<input type="hidden" class="txtBox pastweek" name="ids[]" value="<?php echo $new['id'];?>" >
					<td><input type="text" class="txtBox pastweek" name="sl_No[]" value="<?php echo $i;?>" readonly></td>
					<td><textarea class="txtBox pastweek" name="description[]" /><?php echo $new['description'];?></textarea></td>
					<td class="quantity"><input type="text" class="txtBox pastweek" name="quantity[]"  value="<?php echo $new['quantity'];?>" /></td>
					<td><input type="text" class="txtBox pastweek" name="unit[]"  value="<?php echo $new['unit'];?>"/></td>
					<td class="rate"><input type="text" class="txtBox pastweek" name="rate[]"  value="<?php echo $new['rate'];?>"/></td>
					<td><input type="text" class="txtBox pastweek amt" name="amount[]"  value="<?php echo $new['amount'];?>"/></td>
                                        <td><button type="button" class="removebtn">Remove</button></td>
				</tr>
				
				<?php
				$i++;
				}
				
				?>
				<?php
				if($_POST){
					
					$a = count($newmodel)+1;
					foreach($ids as $k=>$id){
							
						if($id==NULL)
						{	
				?>
			
				<tr>
					<input type="hidden" class="txtBox pastweek" name="ids[]"  />
					<td><input type="text" value ="<?php echo $a++;?>" class="txtBox pastweek" name="sl_No[]" placeholder="Please click to edit" readonly></td>
					<td><textarea class="txtBox pastweek" name="description[]"/><?php echo $description[$k];?></textarea></td>
					<td class="quantity"><input type="text" value ="<?php echo $quantity[$k];?>" class="txtBox pastweek quantity" name="quantity[]" placeholder="Please click to edit"/></td>
					<td><input type="text" value ="<?php echo $unit[$k];?>" class="txtBox pastweek" name="unit[]" placeholder="Please click to edit"/></td>
					<td><input type="text" value ="<?php echo $rate[$k];?>" class="txtBox pastweek rate" name="rate[]" placeholder="Please click to edit"/></td>
					<td><input type="text" value ="<?php echo $amount[$k];?>" class="txtBox pastweek amt" name="amount[]" placeholder="Please click to edit"/></td>
					<td><button type="button" class="removebtn">Remove</button></td>
				</tr>
				<?php
						}
					}
					
				}
				
				?>
				
				
			</tbody>
	</table>
	
	<div class="addcolumn">+</div>
	
	
	<table border="1" style="width:100%;">
		<tr>
		
			<td>SUB TOTAL :<input type="text" class="txtBox pastweek" name="subtot" value="<?php echo ((count($newmodel) ==0) ?  0 : $model['subtotal']);?>" readonly=true /></td>
			<td>FEES :<input type="text" class="txtBox pastweek" name="fees" value="<?php echo $model['fees'];?>" placeholder="Please click to edit" /></td>
			<td>GRAND TOTAL :<input type="text" class="txtBox pastweek grand" name="grand" value="<?php echo ((count($newmodel) ==0) ?  $model['fees'] : $model['amount']);?>" readonly=true />
			 <span class="error_message"><?php  echo ((isset($error['amount'][0]) && $error['amount'][0] != '') ? $error['amount'][0] : ''); ?> </span>
			</td>
		</tr>
	</table>
	<br><br>
 <center><input type="submit" name="invoice" value="Submit" class="form_submit2"></center>
        
              
</form>
</div>


<style>
	.error_message{
		color:red;
	}

	a.pdf_excel {
    background-color: #6a8ec7;
    display:inline-block;
	padding:8px;
    color: #fff;
    border: 1px solid #6a8ec8;
}
</style>

<script>

	
	$(document).ready(function() {
	//for change in fees	
	$(document).on('change', 'input[name="fees"]', function(e){
		
	    var val = Number($(this).val()) + Number($('input[name="subtot"]').val());
		$('input[name="grand"]').val(val);
	});	
	
	$(document).on('change', '.table input[name="amount[]"],.table input[name="quantity[]"],.table input[name="rate[]"]', function(e){
	var v1 = '';
	var v2 = '';
	var v = '';
	
				$(this).closest('tr').find("input[name='rate[]']").each(function() {
				var va1 = this.value;
				v1 +=  va1;
				//alert(v1);
				});
				
				$(this).closest('tr').find("input[name='quantity[]']").each(function() {
				var va2 = this.value;
				v2 +=  va2;
				//alert(v2);
				
				});
			
			if (!(v1 == '' && v2 == '')) {
			//alert('hi');
			
			var v = v1*v2;
		    $(this).closest('tr').find("input[name='amount[]']").val(v); 
			}
			else
			{
			//alert('hi');
			$(this).closest('tr').find("input[name='amount[]']").val(''); 
			}
                        var val = 0;
		$('.table input[name="amount[]"]').each(function() {
		val += Number($(this).val());
		});
		$('input[name="subtot"]').val(val);
		val = val+Number($('input[name="fees"]').val());
		$('input[name="grand"]').val(val);
			
	});
	
	
	$(document).on('focus', '.table input[name="amount[]"]', function(e){
		//alert('hi');
		var val = 0;
		$('.table input[name="amount[]"]').each(function() {
		val += Number($(this).val());
		});
		$('input[name="subtot"]').val(val);
		val = val+Number($('input[name="fees"]').val());
		$('input[name="grand"]').val(val);
		});
		
	});
	
	
	


    $(function () {


        
        
         $('.save_btn2').click(function () {
			
			$("form#pdfvals2").attr('action', "<?php echo $this->createAbsoluteUrl('invoice/saveinvoice'); ?>"); 
            $("form#pdfvals2").submit();

        });
        
         $('.excelbtn2').click(function () {
			
			$("form#pdfvals2").attr('action', "<?php echo $this->createAbsoluteUrl('invoice/exportinvoice'); ?>"); 
            $("form#pdfvals2").submit();

        });
        
         $('.form_submit2').click(function () {
			$("form#pdfvals2").attr('action', "<?php echo $this->createAbsoluteUrl('invoice/updateinvoice&invid='.$invid); ?>"); 
            $("form#pdfvals2").submit();

        });
        
          
        
       
        
       
        
         
        $('.addcolumn').on('click', function () {
			
			/*var val = 1;
			$("table.table tr:last input[name='sl_No[]']").each(function() {
			val += Number($(this).val());
			}); */
			
			//alert(val);
			$('.addrow').append('<tr><input type="hidden" class="txtBox pastweek" name="ids[]" /><td><input type="text" class="txtBox pastweek" name="sl_No[]" placeholder="Please click to edit" readonly></td><td><textarea class="txtBox pastweek" name="description[]"/></textarea></td><td><input type="text" class="txtBox pastweek" name="quantity[]" placeholder="Please click to edit"/></td><td><input type="text" class="txtBox pastweek" name="unit[]" placeholder="Please click to edit"/></td><td><input type="text" class="txtBox pastweek" name="rate[]" placeholder="Please click to edit"/></td><td><input type="text" class="txtBox pastweek amt" name="amount[]" placeholder="Please click to edit"/></td><td><button type="button" class="removebtn_1">Remove</button></td></tr>');		
			//$("table.table tr:last input[name='sl_No[]']").val(val);
			
		
		 });
		  $('.removebtn').on('click', function () {
			
			var answer = confirm("Are you sure you want to delete?");
            if (answer)
            {
				var id = $(this).closest('tr').find("input[name='ids[]']").val(); 
				if(id!=null)
				{
				$.ajax({
						type: "POST", 
						url: "<?php echo $this->createUrl('invoice/deleteInvdetails') ?>",
						dataType:"JSON",
						data:{id:id}, 
						success:function(response){
						   if(response.response = 'success')
						   {
							 location.reload();  
							   
						   }
						  
						  
						}
					
					});
				  }		
				 
		   }
		   else
		   {
			  
			   return false;
		   }
		   
		});
		
		
		 
		 $('table').on('click', '.removebtn_1', function(e){
			$(this).closest('tr').find('input[name="amount[]"]').each(function() {
                            $(this).closest('tr').remove();
                            var re = this.value;
                            var v1 = $('input[name="subtot"]').val();
                            $('input[name="subtot"]').val(eval(v1)-eval(re));
                        });
		});
		
		$(document).on("click", ".addcolumn, .removebtn,.removebtn_1", function(){
			
			$("table.table  input[name='sl_No[]']").each(function(index,element) {               
            $(element).val(index + 1); 
			});
		});
		 
			
			 
		
        
       


        $(".inputSwitch span").on("click", function() {

            var $this = $(this);

            $this.hide().siblings("input").val($this.text()).show();

        });

        $(".inputSwitch input").bind('blur', function() {

                   var $this = $(this);

                   $(this).attr('value', $(this).val());

                   $this.hide().siblings("span").text($this.val()).show();

        }).hide();

 

    });
</script>



<?php $url = Yii::app()->createAbsoluteUrl("invoice/getclientByProject");?>
<?php Yii::app()->clientScript->registerScript('myscript','
    $(document).ready(function(){
    $("#project").change(function(){
    var pro_id= $("#project").val();
    $.ajax({
       url: "'.$url.'",
        data: {"id": pro_id}, 
        //dataType: "json",
        type: "POST",
        success:function(data){
       // alert(data);
           $("#billto").val(data); 
            
        }
        
});

});
$(".flash-success").animate({opacity: 1.0}, 5000).fadeOut("slow");
  
    });
        ');?>
