<?php
$tblpx = Yii::app()->db->tablePrefix;
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
$newQuery1 = "";
$newQuery2 = "";
$newQuery3 = "";
$newQuery4 = "";
foreach ($arrVal as $arr) {
    if ($newQuery) $newQuery .= ' OR';
    if ($newQuery1) $newQuery1 .= ' OR';
    if ($newQuery2) $newQuery2 .= ' OR';
    if ($newQuery3) $newQuery3 .= ' OR';
    if ($newQuery4) $newQuery4 .= ' OR';
    $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
    $newQuery1 .= " FIND_IN_SET('" . $arr . "', id)";
    $newQuery2 .= " FIND_IN_SET('" . $arr . "', b.company_id)";
    $newQuery3 .= " FIND_IN_SET('" . $arr . "', i.company_id)";
    $newQuery4 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "purchase_return.company_id)";
}
$billsDatasql = "SELECT b.bill_id as billid, b.bill_number as billnumber FROM " . $tblpx . "bills b LEFT JOIN " . $tblpx . "purchase p ON b.purchase_id = p.p_id WHERE (" . $newQuery2 . ") AND b.bill_totalamount IS NOT NULL AND b.purchase_id IS NOT NULL AND b.bill_id NOT IN (Select bill_id from " . $tblpx . "expenses WHERE bill_id IS NOT NULL) GROUP BY b.bill_id  ORDER BY b.bill_id DESC";
$billsData = Yii::app()->db->createCommand($billsDatasql)->queryAll();

$invoicesDatasql = "SELECT i.invoice_id, i.inv_no as invoice_no FROM " . $tblpx . "invoice i LEFT JOIN " . $tblpx . "projects p ON i.project_id = p.pid WHERE (" . $newQuery3 . ") AND i.amount >0 AND i.invoice_id NOT IN(Select invoice_id from " . $tblpx . "expenses WHERE invoice_id IS NOT NULL) GROUP BY i.invoice_id ORDER BY i.invoice_id DESC";
$invoicesData = Yii::app()->db->createCommand($invoicesDatasql)->queryAll();

$purchasereturnDatasql = "SELECT * FROM " . $tblpx . "purchase_return LEFT JOIN " . $tblpx . "bills ON " . $tblpx . "purchase_return.bill_id=" . $tblpx . "bills.bill_id WHERE " . $tblpx . "purchase_return.return_totalamount >0 AND (" . $newQuery4 . ") AND  " . $tblpx . "purchase_return.return_id NOT IN(Select return_id from " . $tblpx . "expenses WHERE return_id IS NOT NULL) GROUP BY " . $tblpx . "purchase_return.return_id ORDER BY " . $tblpx . "purchase_return.return_id";
$purchasereturnData = Yii::app()->db->createCommand($purchasereturnDatasql)->queryAll();

$worktype = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "daily_work_type")->queryAll();
$pms_api_integration_model = ApiSettings::model()->findByPk(1);
$pms_api_integration = $pms_api_integration_model->api_integration_settings;
?>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>

<style type="text/css">
    .select2-selection.select2-selection--single {
        min-width: 100px;
    }

    *:focus {
        border: 1px solid #333;
        box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
    }

    .btn.addentries:focus,
    .btn.addentries:hover {
        border: 1px solid transparent;
        box-shadow: none;
    }

    .avoid_clicks {
        pointer-events: none;
    }

    .amntblock,
    .expvalue {
        display: none;
    }

    .deleteclass {
        background-color: #efca92;
    }

    .filter_elem.links_hold {
        margin-top: 6px;
    }

    .mr-10 {
        margin-right: 10px;
        ;
    }

    .add-btn,
    .remove-btn {
        text-align: right;
        margin-top: 23px;
        margin-right: 2px;
        font-weight: bolder;
        border-radius: 2px;
    }

    .legend {
        list-style: none;
        padding-left: 0px;

    }

    /* .legend li {
        float: left;
        margin-right: 10px;
    } */

    .legend span {
        border: 1px solid #ccc;
        float: left;
        width: 12px;
        height: 12px;
        margin: 2px;
    }

    /* your colors */


    .legend .project_not_mapped {
        background-color: #A9E9EC;
        border: 2px solid #f8cbcb;
        margin-right: 4px;
    }
</style>
<div class="container" id="expense">

    
    <div class="expenses-heading header-container">
        <!-- <div class="clearfix"> -->

            <h3>Equipments Entries</h3>
            <div>
                <?php echo CHtml::button('Add Entries', array(
                    'class' => 'btn btn-info collapsed',
                    'data-toggle' => 'collapse',
                    'data-target' => '#daybookform',
                    
                )); ?>


                <?php echo CHtml::button('Entries', array(
                    'class' => 'btn btn-info ',
                    'onclick' => 'window.open("' . Yii::app()->createUrl("EquipmentsEntries/admin", array("status" => 3)) . '", "_blank")'
                )); ?>
                <?php echo CHtml::button('Estimation', array(
                    'class' => 'btn btn-info ',
                    'onclick' => 'window.open("' . Yii::app()->createUrl("purchase/itemestimation") . '", "_blank")'
                )); ?>
                <?php
                $acc_proj_id_empty_style = '';
                if ($pms_api_integration == 1) {
                    echo CHtml::link(CHtml::encode("Refresh"),  array('EquipmentsEntries/refreshButton', 'type' => 1), array('class' => 'btn btn-info pull-right addentries mr-10'));
                }
                ?>
            </div>

        <!-- </div> -->
    </div>
    <div class="daybook form">
        <?php
        $form = $this->beginWidget(
            'CActiveForm',
            array(
                'id' => 'Equipments-form',
                'enableAjaxValidation' => false,
            )
        );
        ?>
        <input type="hidden" id="equipment_id" name="EquipmentsEntries[id]" value="<?php echo $model->id; ?>">

        <div class="entries-wrapper">
            <div class="datepicker">
                <div class="page_filter clearfix">
                     <div class="filter_elem">
                        <label for="EquipmentsEntries_expense_date">Entry Date:</label>
                        <input 
                            type="text" 
                            id="EquipmentsEntries_expense_date" 
                            name="EquipmentsEntries[date]" 
                            class="form-control" 
                            readonly 
                            autocomplete="off" 
                            value="<?php echo date('d-m-Y'); ?>" 
                            onchange="changedate()" 
                        />
                    </div>

                    <div class="filter_elem links_hold">
                        <a href="#" id="previous" class="link">Previous</a> |
                        <a href="#" id="current" class="link">Current</a> |
                        <a href="#" id="next" class="link">Next</a>
                    </div>
                    <?php
                    $tblpx = Yii::app()->db->tablePrefix;
                    $expenseData = Yii::app()->db->createCommand("select MAX(date) as entry_date FROM " . $tblpx . "expenses")->queryRow();
                    ?>
                    <div class="pull-right mt">
                        <span id="lastentry">
                            Last Entry date :
                            <span id="entrydate">
                                <?php echo (isset($lastdate['date']) ? date("d-M-Y", strtotime($lastdate['date'])) : ''); ?>
                            </span>
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <!-- <div class="block_hold shdow_box clearfix collapse custom-form-style" id="daybookform"> -->

        <div class="entries-wrapper collapse" id="daybookform">
            <div class="row">
                <div class="col-xs-12">
                    <div class="heading-title">Add Equipment Entry</div>
                    <div class="dotted-line"></div>
                </div>
            </div>
            <div>
                
                <div class="row">
                    <div class="col-xs-12 col-md-2">
                        <div class="form-group">
                            <label for="project">Company</label>
                            <?php
                            echo $form->dropDownList($model, 'company_id', CHtml::listData(Company::model()->findAll(
                                array(
                                    'select' => array('id, name'),
                                    'order' => 'name ASC',
                                    'condition' => '(' . $newQuery1 . ')',
                                    'distinct' => true,
                                )
                            ), 'id', 'name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Company-', 'style' => 'width:100%;'));
                            ?>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-2">
                        <div class="form-group">
                            <label for="project">Project</label>
                            <?php
                            echo $form->dropDownList($model, 'project_id', CHtml::listData(Projects::model()->findAll(
                                array(
                                    'select' => array('pid, name'),
                                    'order' => 'name ASC',
                                    'condition' => '(' . $newQuery . ')',
                                    'distinct' => true,
                                )
                            ), 'pid', 'name'), array('class' => 'form-control js-example-basic-single', 'empty' => '-Select Project-', 'style' => 'width:100%;'));
                            ?>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-2">
                        <div class="form-group">
                            <label for="vendor">Subcontractor</label>
                            <select class="form-control js-example-basic-single"
                                name="EquipmentsEntries[subcontractor_id]" id="EquipmentsEntries_subcontractor_id"
                                style="width:100%;">
                                <option value="">-Select Subcontractor-</option>
                            </select>
                            <input type="hidden" name="bill_vendor" id="bill_vendor" value="">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-2">
                        <div class="form-group">
                            <label for="vendor">Expense Head</label>
                            <select class="form-control js-example-basic-single"
                                name="EquipmentsEntries[expensehead_id]" id="EquipmentsEntries_expensehead_id"
                                style="width:100%;">
                                <option value="">-Select Expense Head-</option>
                            </select>
                            <input type="hidden" name="bill_vendor" id="bill_vendor" value="">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <div class="form-group">
                            <label for="project">Description</label>
                            <?php echo $form->textArea($model, 'description', array('value' => $model['description'], 'rows' => 6, 'cols' => 50, 'class' => "form-control")); ?>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-2">
                        <label for="date">Date</label>
                        <?php
                        $savedDate = !empty($model->Date) ? date('d-m-Y', strtotime($model->Date)) : date('d-m-Y');
                        echo $form->textField($model, 'date', array(
                            'class' => 'form-control',
                            'type' => 'date', 
                            'style' => 'width:100%;',
                            'value' => $savedDate, 
                        ));
                        ?>
                    </div>
                </div>
            </div>
            <div>
                <!-- <h4 class="section_title">Equipments Details</h4> -->
                <!-- <div class="row" id="target-row">
                    <div class="col-md-2">
                        <div class="form-group item-equipment">
                            <?php echo $form->labelEx($EquipmentsItems, 'Equipments'); ?>
                            <?php
                            $EquipmentsData = $equipment_arr;
                            // echo '<pre>';print_r($EquipmentsData);exit;
                            echo $form->dropDownList(
                                $EquipmentsItems,
                                'equipments[]',
                                $EquipmentsData,
                                array(
                                    'class' => 'form-control js-example-basic-single',
                                    'empty' => '-- Select Equipments --',
                                    'style' => 'width:100%;'
                                )
                            );
                            ?>
                            <?php echo $form->error($EquipmentsItems, 'Equipments'); ?>
                            <div class="errorMessageequipments" style="display:none;"></div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <?php echo $form->labelEx($EquipmentsItems, 'unit'); ?>
                            <?php
                            $unitData = CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name');
                            echo $form->dropDownList(
                                $EquipmentsItems,
                                'unit[]',
                                $unitData,
                                array(
                                    'class' => 'form-control',
                                    'empty' => '-- Select Unit --'
                                )
                            );
                            ?>
                            <?php echo $form->error($EquipmentsItems, 'unit'); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <?php echo $form->labelEx($EquipmentsItems, 'quantity'); ?>
                            <?php echo $form->textField($EquipmentsItems, 'quantity[]', array(
                                'class' => 'form-control', 
                                'oninput' => 'this.value = this.value.replace(/[^0-9]/g, ""); calculateEquipmentAmount(this);'
                            )); ?>
                            <?php echo $form->hiddenField($EquipmentsItems, 'id[]', array('class' => 'form-control')); ?>
                            <?php echo $form->error($EquipmentsItems, 'quantity'); ?>
                            <span class="error_count"></span>
                        </div>
                        
                    </div>
                    
                    <div class="col-md-2">
                        <div class="form-group">
                            <?php echo $form->labelEx($EquipmentsItems, 'rate'); ?>
                            <?php echo $form->textField($EquipmentsItems, 'rate[]', array(
                                'class' => 'form-control', 
                                'oninput' => 'this.value = this.value.replace(/[^0-9.]/g, ""); calculateEquipmentAmount(this);'
                            )); ?>
                            <?php echo $form->hiddenField($EquipmentsItems, 'id[]', array('class' => 'form-control')); ?>
                            <?php echo $form->error($EquipmentsItems, 'rate'); ?>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <?php echo $form->labelEx($EquipmentsItems, 'amount'); ?>
                            <?php echo $form->textField($EquipmentsItems, 'amount[]', array(
                                'class' => 'form-control', 
                                'readonly' => true
                            )); ?>
                            <?php echo $form->hiddenField($EquipmentsItems, 'id[]', array('class' => 'form-control')); ?>
                            <?php echo $form->error($EquipmentsItems, 'amount'); ?>
                        </div>
                    </div>

                    <div class="col-md-1">
                        <div class="form-group">
                            <button type="button" id="add-more-button" class="add-btn btn btn-info btn-sm"
                                onClick="addFormField();">+</button>
                        </div>
                    </div>
                </div> -->
                <div id="dynamicInputs" style="width: 100%;"></div>
                <!-- <div id="add_more_item"></div> -->
            </div>
            <input type="hidden" name="EquipmentsEntries[dr_id]" value="" id="txtEquipmentsEntriesId" />
            <input type="hidden" name="integrationStatus" id="integrationStatus"
                value="<?= $pms_api_integration == 1 ? ($model->status == 0 ? 1 : 2) : false; ?>">
            <div class="form-group submit-button text-right">
                <button type="button" class="btn btn-info" id="buttonsubmit">ADD</button>
                <button type="button" class="btn btn-warning" id="btnReset">RESET</button>
            </div>

        </div>
        <?php $this->endWidget(); ?>
    </div>
    <div class="">
        <div class="">
            <div id="errormessage"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div id="errorMsg"></div>
        </div>
    </div>
    
    <div id="daybook-entry">

    <div class="" id="newlist">
            <?php $this->renderPartial('newlist', array('newmodel' => $newmodel,)); ?>
        </div>
    </div>
</div>
<div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="rejectModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="rejectModalLabel">Reject Equipment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="rejectForm">
                    <input type="hidden" id="rejectId" name="id">
                    <div class="form-group">
                        <label for="remarks">Remarks</label>
                        <textarea class="form-control" id="remarks" name="remarks" rows="3" required></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onClick="submitReject()">Submit</button>
            </div>
        </div>
    </div>
</div>

<?php $dropdownUrl = Yii::app()->createAbsoluteUrl("EquipmentsEntries/dynamicDropdown"); ?>
<?php $vendorUrl = Yii::app()->createAbsoluteUrl("EquipmentsEntries/dynamicVendor"); ?>
<?php $invoiceUrl = Yii::app()->createAbsoluteUrl("EquipmentsEntries/GetInvoiceDetails"); ?>
<?php $returnUrl = Yii::app()->createAbsoluteUrl("EquipmentsEntries/GetPurchaseReturnDetails"); ?>
<?php $getUrl = Yii::app()->createAbsoluteUrl("EquipmentsEntries/getDataByDate"); ?>
<?php $getBillUrl = Yii::app()->createAbsoluteUrl("EquipmentsEntries/GetBillDetails"); ?>

<?php $bill_invoice = Yii::app()->createAbsoluteUrl("EquipmentsEntries/DynamicBillorInvoice"); ?>
<?php $projectUrl = Yii::app()->createAbsoluteUrl("EquipmentsEntries/DynamicProject"); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-dateFormat/1.0/jquery.dateFormat.js"></script>
<script>
$(function() {
        $("#EquipmentsEntries_date").datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: new Date()
        });
    });

        function addNewInputRow(isFirstRow) {
            let index = $('#dynamicInputs .dynamic-row').length;
            if(index === '') {
                index = 0;
            }
            var project = $("#EquipmentsEntries_project_id").val();
            //alert(project);
            $.ajax({
                url: 'index.php?r=EquipmentsEntries/GetNewInputRow',
                type: 'POST',
                data: { index: index, project:project },
                success: function(response) {
                    $('#dynamicInputs').append(response);
                    // $('.coms_material_id').select2();
                    // $('.specification_id').select2();
                    // $('.item_rate').select2();
                    
                }
            });
        }

        var firstTimeChange = true;

        $('#EquipmentsEntries_date').change(function() {
            if (firstTimeChange) {
                addNewInputRow(0);
                firstTimeChange = false; // Prevent further calls
            }
        });
        $('#dynamicInputs').on('click', '.add-btn', function() {
            addNewInputRow(false);
        });
        $('#dynamicInputs').on('click', '.remove-btn', function() {
                    $(this).closest('.dynamic-row').remove();
         });

         function handleQuantityChange(element, index) {
            // Find the parent container of the inputs
            const formGroup = element.closest('.form-group').parentElement;

            // Get the quantity input value
            const quantityInput = formGroup.querySelector(`input[name="EquipmentsItems[${index}][quantity]"]`);
            const quantity = parseInt(quantityInput.value, 10) || 0; // Convert to integer, default to 0

            // Get the rate input value
            const rateInput = formGroup.querySelector(`input[name="EquipmentsItems[${index}][rate]"]`);
            const rate = parseFloat(rateInput.value) || 0; // Convert to float, default to 0

            // Calculate the amount
            const amount = quantity * rate;

            // Update the amount field
            const amountInput = formGroup.querySelector(`input[name="EquipmentsItems[${index}][amount]"]`);
            if (amountInput) {
                amountInput.value = amount.toFixed(2); // Display amount with 2 decimal places
            } else {
                console.error(`Amount input field not found for index ${index}`);
            }
        }

        function calculateAmount(index) {
            var rateField = document.querySelector(`input[name="EquipmentsEntries[items][${index}][rate]"]`);
            var quantityField = document.querySelector(`input[name="EquipmentsEntries[items][${index}][quantity]"]`);
            var amountField = document.querySelector(`input[name="EquipmentsEntries[items][${index}][amount]"]`);

            if (rateField && quantityField && amountField) {
                var rate = parseFloat(rateField.value) || 0;
                var quantity = parseFloat(quantityField.value) || 0;

                var amount = rate * quantity; // Perform calculation
                amountField.value = amount.toFixed(2); // Update amount field
            }
        }


        $(document).ready(function() {
            var equipmentId = $('#equipment_id').val();
            if(equipmentId==''){
            // addNewInputRow(true);
            }
            else{
                getFieldsForEdit(equipmentId);
            }
            
        });

        function getFieldsForEdit(id){
            let index = $('#dynamicInputs .dynamic-row').length;
            if(index === '') {
                index = 0;
            }
                $('#loading').show();
                $.ajax({
                    type: "GET",
                    data: {
                        id :id ,
                        index:index,
                    },
                    url: "<?php echo $this->createUrl('EquipmentsEntries/getfieldsForEdit'); ?>",
                    success: function(response) {
                        $('#dynamicInputs').append(response);
                        $('.coms_material_id').select2();
                        $('.specification_id').select2();
                        $('.item_rate').select2();
                    }
                });
        }
    $(document).ready(function () {

        
        $(".popover-test").popover({
            html: true,
            content: function () {
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function (e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function (e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function (e) {
            $('[data-toggle=popover]').each(function () {
                // hide any open popovers when the anywhere else in the body is clicked
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
        $(document).ajaxComplete(function () {
            $(".popover-test").popover({
                html: true,
                content: function () {
                    return $(this).next('.popover-content').html();
                }
            });
        });
        $(".js-example-basic-single").select2();
        $(".project").select2();
        $(".client").select2();
        $(".company_id").select2();

    });
    $(document).ready(function () {
        // Hide the row by default
        // $('#target-row').hide();

        $('#EquipmentsItems_equipments').on('change', function (event, data = {}) {
            var Equipments = $(this).val();
            if (!data.flag) {
                $.ajax({
                    url: 'index.php?r=EquipmentsEntries/GetUnit',
                    type: 'POST',
                    data: { equipments_id: Equipments },
                    success: function (response) {
                        try {
                            var obj = JSON.parse(response);

                            if (obj.status === "success" && obj.data) {
                                var EquipmentsUnits = obj.data.equipment_unit.split(',');

                                $('#EquipmentsItems_unit').val('');
                                // Handle the response from the controller
                                console.log(response, 5);
                                if (EquipmentsUnits.length === 1) {

                                    // If there's only one unit, directly select it
                                    $('#EquipmentsItems_unit').val(EquipmentsUnits).trigger('change');
                                    // $('#EquipmentsItems_unit').css('pointer-events', 'none');
                                } else {
                                    // Otherwise, loop through the options and show/hide based on materialUnit`
                                    $('#EquipmentsItems_unit option').each(function () {
                                        var unitId = $(this).val();
                                        if (EquipmentsUnits.indexOf(unitId) === -1) {
                                            $(this).hide();
                                        } else {
                                            $(this).show();
                                        }
                                    });
                                }
                            } else {
                                console.log("Error: Invalid response format or status");
                                alert("Error: Invalid response format or status");
                            }
                        } catch (e) {
                            console.log('Error parsing JSON response: ' + e);
                            alert('Error parsing JSON response: ' + e);
                        }
                    },
                    error: function (xhr, status, error) {
                        // Handle any errors
                        console.log('Error: ' + error);
                    }
                });
            }

        });
    });
    let count = 0;

    

    function calculateEquipmentAmount(element) {
        let row = element.closest('.form-group').parentNode.parentNode;
        let quantityInput = row.querySelector('[name="EquipmentsItems[quantity][]"]');
        let rateInput = row.querySelector('[name="EquipmentsItems[rate][]"]');
        let amountInput = row.querySelector('[name="EquipmentsItems[amount][]"]');

        let quantity = parseFloat(quantityInput.value) || 0;
        let rate = parseFloat(rateInput.value) || 0;
        let amount = quantity * rate;
        
        amountInput.value = amount.toFixed(2);
    }

    $(document).on('click', '.remove-btn', function () {
        let buttonId = $(this).attr('id');
        $('#parent_id_' + buttonId).remove();
    });


    $(".js-example-basic-single").select2();
    $(function () {
        $("#EquipmentsEntries_expense_date").datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: new Date()
        });
    });
    $(document).ready(function () {
        var urlParams = new URLSearchParams(window.location.search);
        if (urlParams.has('id')) {
            editRowClick('', urlParams.get('id'));
        }

        $('#loading').hide();
        $('#daybookform').on('shown.bs.collapse', function () {
            $('select').first().focus();
        });

        var model_company_id = '<?php echo $model->company_id; ?>';
        var model_project_id = '<?php echo $model->project_id; ?>';
        var model_subid = '<?php echo $model->subcontractor_id; ?>';
        var model_expensehead_id = '<?php echo $model->expensehead_id; ?>';

        getProjectList(model_company_id, 0, model_project_id, model_subid);
        getExpenseHead(model_subid, model_expensehead_id);

    });
</script>
<script type="text/javascript">
    function edit(id) {
        window.location.href = '<?php echo Yii::app()->createAbsoluteUrl('EquipmentsEntries/create&id='); ?>' + id;
    }

    function view(id) {
        window.location.href = '<?php echo Yii::app()->createAbsoluteUrl('EquipmentsEntries/viewequipment&id='); ?>' + id;
    }

    // $(document).on('click', '.view_consumption', function() {
    //      var id = $(this).attr('data-id');
    //     location.href = url + '/index.php?r=wh/warehouse/viewconsumption&layout=1&id=' + id;
    //  });

    function approve(id) {
    $('.loading-overlay').addClass('is-active');
    $.ajax({
        url: '<?php echo Yii::app()->createAbsoluteUrl('EquipmentsEntries/approve'); ?>',
        type: 'POST',
        data: { id: id },
        dataType: 'json', // Expect JSON response
        success: function (response) {
            if (response.status == 1) {
                $("#errorMsg").show()
                    .html('<div class="alert alert-success"><b>Success!</b> ' + response.message + '</div>')
                    .fadeOut(5000);
                setTimeout(function () {
                    location.reload();
                }, 3000);
            } else if (response.response == 'warning') {

            var warningAlert = '<div class="alert alert-warning alert-dismissible" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                '</button>' +
                '<strong>Sorry!</strong> ' + response.msg +
            '</div>';
            $("#errorMsg").html(warningAlert).show();
            }
            
        },
        error: function () {
            $("#errorMsg").show()
                .html('<div class="alert alert-danger"><b>Error Occurred.</b></div>')
                .fadeOut(5000);
        }
    });
}


    function openRejectModal(id) {
        $('#rejectId').val(id); 
        $('#rejectModal').modal('show'); 
    }
    
    function submitReject() {
        var id = $('#rejectId').val();
        var remarks = $('#remarks').val();

        if (remarks === "") {
            alert("Remarks field cannot be empty.");
            return;
        }

        $.ajax({
            type: "POST",
            url: "index.php?r=EquipmentsEntries/rejectEquipment",
            data: {
                id: id,
                remarks: remarks,
            },
            success: function(response) {
                var result = JSON.parse(response);
                if (result.response === 'success') {
                    alert('Equipment rejected successfully');
                    $('#rejectModal').modal('hide');  
                    location.reload();  
                } else {
                    alert('Error: ' + result.msg);
                }
            },
            error: function() {
                alert('An error occurred. Please try again.');
            }
            });
    }

    function getFullData(newDate) {
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            url: "<?php echo $getUrl; ?>",
            data: {
                "date": newDate
            },
            type: "POST",
            success: function (data) {
                $("#newlist").html(data);
            }
        });
    }

    function changedate() {
        var cDate = $("#EquipmentsEntries_expense_date").val();
        getFullData(cDate);
        validateDateSelection(cDate);
    }

    function getDropdownList(project_id, bill_id, exptypeid, edit, expId) {
        $('#loading').show();
        if (edit == undefined || edit == '') {
            edit = '';
        }
        $.ajax({
            url: "<?php echo $dropdownUrl; ?>",
            data: {
                "project": project_id,
                "bill": bill_id,
                "expId": expId
            },
            type: "POST",
            success: function (data) {
                var result = JSON.parse(data);
                $("#EquipmentsEntries_bill_id").html(result["bills"]);
                if (edit == 'edit') {
                    $("#EquipmentsEntries_project_id").select2("focus");
                } else {
                    $("#EquipmentsEntries_bill_id").select2("focus");
                }

                if (exptypeid == 0) {
                    $("#EquipmentsEntries_bill_id").val(0);
                    $("#EquipmentsEntries_vendor_id").html('<option value="">-Select Vendor-</option>');
                } else {
                    if (bill_id === null) {
                        $("#EquipmentsEntries_bill_id").val(0);
                    } else {
                        if (result['type'] == 'bill') {
                            $("#EquipmentsEntries_bill_id").val('1,' + bill_id);
                        } else if (result['type'] == 'invoice') {
                            $("#EquipmentsEntries_bill_id").val('2,' + bill_id);
                        } else {
                            $("#EquipmentsEntries_bill_id").val('3,' + bill_id);
                        }
                    }

                }

                $("#EquipmentsEntries_expensetype_id").html(result["expenses"]);
                if (exptypeid != 0)
                    $("#EquipmentsEntries_expensetype_id").val(exptypeid);
            },
        });
    }

    function getDynamicBillorInvoice() {
        $('#loading').show();
        $.ajax({
            url: "<?php echo $bill_invoice; ?>",
            type: "POST",
            success: function (data) {
                $("#EquipmentsEntries_bill_id").html(data);
            }
        });
    }

    function getProjectList(comId, opt, pro, sub) {
        var EquipmentsId = $("#txtEquipmentsEntriesId").val();
       // $('#loading').show();
        $.ajax({
            url: "<?php echo $projectUrl; ?>",
            data: {
                "comId": comId,
                "EquipmentsId": EquipmentsId
            },
            type: "POST",
            dataType: 'json',
            success: function (data) {
               // console.log(data.EquipmentsItems);
                if (comId == 0) {
                    $("#EquipmentsEntries_project_id").val('').trigger('change.select2');
                    $("#EquipmentsEntries_subcontractor_id").val('').trigger('change.select2');
                } else {
                    $("#EquipmentsEntries_project_id").html(data.project);
                    $("#EquipmentsEntries_subcontractor_id").html(data.subcontractor);
                }
                $('#EquipmentsEntries_bill_id').val('0').trigger('change.select2');
                $("#EquipmentsEntries_subcontractor_id").val(sub).trigger('change.select2');
                $("#EquipmentsEntries_project_id").val(pro).trigger('change.select2');

                if (sub != 0) {
                    $("#EquipmentsEntries_subcontractor_id").val(sub).trigger('change.select2');
                }
                if (pro != 0) {
                    $("#EquipmentsEntries_project_id").val(pro).trigger('change.select2');
                }

                // Populate the first row with the first item of the returned data
                
                
                if ($("#integrationStatus").val() == 1) {
                    $('.add-btn').hide();
                    $('.remove-btn').hide();
                    var $select = $('#EquipmentsEntries_project_id');
                    // Add readonly styling
                    var $select2Container = $select.next('.select2-container').find('.select2-selection--single');
                    // class="select2-selection select2-selection--single"
                    $select2Container.css({
                        'background-color': '#e9ecef',
                        'pointer-events': 'none',
                        'cursor': 'not-allowed'
                    });

                    // Prevent interaction with the select element
                    $select.on('select2:opening select2:unselecting', function (e) {
                        e.preventDefault();
                    });
                }
              //  $('#loading').hide();
            }
        });
    }

    

    $(document).ready(function () {

        $(document).on('change', '.equipment_qty', function() {
            var qty = $(this).val();
            var index = $(this).attr('name').match(/\d+/)[0]; // Extract index dynamically
            var equipment = $('select[name="EquipmentsItems[equipments][' + index + '][equipment_id]"]').val();
            var project_id = $('#EquipmentsEntries_project_id').val();

            // if (!equipment) {
            //     $('.error_count').text("Please select equipment first.");
            //     return;
            // }

            $.ajax({
                type: 'POST',
                url: "index.php?r=EquipmentsEntries/qtybalance",
                data: { qty: qty, equipment: equipment, project_id: project_id },
                dataType: 'json',
                success: function(response) {
                    if (response.success) {
                        $('.error_count').text("Estimated balance: " + response.balanceQty);
                    } else {
                        $('.error_count').text(response.message);
                    }
                },
                error: function(xhr, status, error) {
                    console.error("AJAX Error: " + error);
                }
            });
        });

        $(document).on('input', '.quantityBalance', function() {
            var count = $(this).attr('id').split('_')[1]; // Extract the dynamic count from the ID
            var qty = $(this).val();
            var equipment = $('#equipments_' + count).val();
            var project_id = $('#EquipmentsEntries_project_id').val()
            alert(equipment);
            $.ajax({
                type: 'POST',
                url: "index.php?r=EquipmentsEntries/qtybalance",
                data: { qty: qty, equipment: equipment, project_id: project_id }, 
                dataType: 'json',
                success: function(response) {
                    if (response.success) {
                        // Update the corresponding balance quantity span
                        $('.error_count_' + count).text("Estimated balance: " + response.balanceQty);
                    } else {
                        $('.error_count_' + count).text(response.message);
                    } 
                },
                error: function(xhr, status, error) {
                    console.error("AJAX Error: " + error);
                }
            });
        });



        $(document).on('change', '.Equipments_add', function (event, data = {}) {
            var Equipments = $(this).val();
            var index = $(this).closest('.form-group').find('select[name^="EquipmentsItems"]').data('index'); // Get the index dynamically
            
            if (!data.flag) {
                $.ajax({
                    url: 'index.php?r=EquipmentsEntries/GetUnit',
                    type: 'POST',
                    data: { equipments_id: Equipments },
                    success: function (response) {
                        try {
                            var obj = JSON.parse(response);
                            if (obj.status === "success" && obj.equipment_unit) {
                        var EquipmentsUnit = obj.equipment_unit; // Single unit value
                        var unitDropdown = $(`select[name="EquipmentsItems[${index}][unit]"]`);

                        // Reset dropdown
                        unitDropdown.val('');

                        // Show only the relevant unit
                        unitDropdown.find('option').each(function () {
                            var unitId = $(this).val();
                            if (unitId && unitId != EquipmentsUnit) {
                                $(this).hide();
                            } else {
                                $(this).show();
                                unitDropdown.val(EquipmentsUnit).trigger('change');
                            }
                        });

                    }else {
                                console.log("Error: Invalid response format or status");
                              //  alert("Error: Invalid response format or status");
                            }
                        } catch (e) {
                            console.log('Error parsing JSON response: ' + e);
                            //alert('Error parsing JSON response: ' + e);
                        }
                    },
                    error: function (xhr, status, error) {
                        console.log('Error: ' + error);
                        $(`select[name="EquipmentsItems[${index}][unit]"]`).val('');
                    }
                });
            }
        });


    });
    var project_id = $("#EquipmentsEntries_project_id").val();
    var integration = $("#integrationStatus").val();

    $("#EquipmentsEntries_company_id").change(function () {
        var comId = $("#EquipmentsEntries_company_id").val();
        var comId = comId ? comId : 0;
        getProjectList(comId, 0, project_id, 0);

    });
    if (project_id && integration == 1) {
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('EquipmentsEntries/GetCompany'); ?>',
            type: 'GET',
            data: { project_id: project_id },
            success: function (response) {
                var jsonResponse = JSON.parse(response);
                if (jsonResponse.status == 1) {
                    var companies = jsonResponse.companies;
                    var $dropdown = $('#EquipmentsEntries_company_id');
                    $dropdown.empty(); // Remove existing options
                    $dropdown.append('<option value="">-Select Company-</option>'); // Add the default option
                    $.each(companies, function (id, name) {
                        $dropdown.append($('<option></option>').attr('value', id).text(name));
                    });
                } else {
                    console.log(jsonResponse.message);
                }
            },
            error: function () {
                console.log('An error occurred while fetching company data.');
            }
        });
    }


    $("#EquipmentsEntries_project_id").change(function () {
        var prId = $("#EquipmentsEntries_project_id").val();
        var prId = prId ? prId : 0;
        getVendorList(prId, 0);
    });

    function getVendorList(prId, expvendor, edit) {
        $('#loading').show();
        if (edit == undefined) {
            edit = '';
        }
        if (prId === null) {
            prId = 0;
        } else {
            prId = prId;
        }

        $.ajax({
            url: "<?php echo $vendorUrl; ?>",
            data: {
                "prId": prId
            },
            type: "POST",
            success: function (data) {
                $("#EquipmentsEntries_vendor_id").html(data);
                if (edit == 'edit') {
                    $("#EquipmentsEntries_company_id").select2("focus");
                } else {
                    $("#EquipmentsEntries_vendor_id").select2("focus");
                }
                if (expvendor != 0)
                    $("#EquipmentsEntries_vendor_id").val(expvendor);
            }
        });
    }

    function actdelete(id, elem) {

        var item_count = $(elem).attr('data-item-id');
        if (item_count > 0) {
            var answer = confirm("This Quotation contains " + item_count + " items.Are you sure you want to delete?");
        } else {
            var answer = confirm("Are you sure you want to delete?");
        }

        if (answer) {
            $.ajax({
                url: 'index.php?r=EquipmentsEntries/DeleteEquipments',
                type: 'POST',
                dataType: 'json',
                data: {
                    id: id
                },
                success: function (data) {
                    $("#errorMsg").show()
                        .html('<div class="alert alert-' + data.response + '"><b>' + data.msg + '</b></div>')
                        .fadeOut(5000);

                    setTimeout(function () {
                        location.reload();
                    }, 10)
                }
            })
        }
    }

    $(document).ready(function () {
        $("#EquipmentsEntries_project_id").focus();
        $("#EquipmentsEntries_bank_id").attr("disabled", true);
        $("#txtChequeno").attr("readonly", true);
        $("#txtPurchaseType").attr("disabled", true);
        $("#txtPaid").attr("readonly", true);
        $("a#next").css("pointer-events", "none").css("color", "#ccccc");
        $("#buttonsubmit").click(function (event) {
            event.preventDefault()
            $("#buttonsubmit").attr('disabled', true);
            var crDate = $("#EquipmentsEntries_expense_date").val();
            var project = $("#EquipmentsEntries_project_id").val();
            var company = $("#EquipmentsEntries_company_id").val();
            var subcontractor_id = $("#EquipmentsEntries_subcontractor_id").val();
            var expensehead_id = $("#EquipmentsEntries_expensehead_id").val();

            var materail = $("#EquipmentsItems_Equipments").val();
            var quantity = $("#EquipmentsItems_quantity").val();
            var unit = $("#EquipmentsItems_quantity").val();
            var rate = $("#EquipmentsItems_rate").val();
            var amount = $("#EquipmentsItems_rate").val();
            if (company == "") {
                $("#errormessage").show()
                    .html('<div class="alert alert-danger">Please select a company from the dropdown.</div>')
                    .fadeOut(5000);
                $("#buttonsubmit").attr('disabled', false);
                return false;
            }
            if (project == "") {
                $("#errormessage").show()
                    .html('<div class="alert alert-danger">Please select a project from the dropdown.</div>')
                    .fadeOut(5000);
                $("#buttonsubmit").attr('disabled', false);
                return false;
            }
            if (subcontractor_id == "") {
                $("#errormessage").show()
                    .html('<div class="alert alert-danger">Please select a subcontractor from the dropdown.</div>')
                    .fadeOut(5000);
                $("#buttonsubmit").attr('disabled', false);
                return false;
            }

            var EquipmentsId = $("#equipment_id").val();

            var actionUrl;
            if (EquipmentsId == "") {
                actionUrl = "<?php echo Yii::app()->createAbsoluteUrl("EquipmentsEntries/addEquipmentsEntries"); ?>";
                localStorage.setItem("action", "add");
            } else {
                actionUrl = "<?php echo Yii::app()->createAbsoluteUrl("EquipmentsEntries/UpdateEquipmentsEntries"); ?>";
                localStorage.setItem("action", "update");
            }
            var data = $("#Equipments-form").serialize();
            console.log(data, 1);
            $('.loading-overlay').addClass('is-active');


            $.ajax({
                type: 'POST',
                url: actionUrl,
                data: data,
                success: function (data) {
                    $("#buttonsubmit").attr('disabled', false);
                    $('.loading-overlay').removeClass('is-active');
                    if (data == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-warning"><strong>Failed !</strong> please try again.</div>')
                            .fadeOut(10000);
                        $("#buttonsubmit").attr('disabled', false);
                        return false;
                    } else {

                        var currAction = localStorage.getItem("action");
                        if (currAction == "add") {
                            $("#errormessage").show()
                                .html('<div class="alert alert-success"><strong>Successfully !</strong> record added.</div>')
                                .fadeOut(10000);
                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth() + 1;
                            var yyyy = today.getFullYear();
                            if (dd < 10)
                                dd = '0' + dd
                            if (mm < 10)
                                mm = '0' + mm
                            today = dd + '-' + mm + '-' + yyyy;
                            $("#entrydate").text(today);
                            $('#EquipmentsEntries_subcontractor_id').val('').trigger('change.select2');
                            $('#EquipmentsEntries_expensehead_id').val('').trigger('change.select2');
                            $('#EquipmentsEntries_project_id').val('').trigger('change.select2');
                            $('#EquipmentsEntries_company_id').val('').trigger('change.select2');
                            $('#EquipmentsEntries_bill_id').val('0').trigger('change.select2');
                            $('#EquipmentsEntries_expensetype_id').html('<option value="">-Select Expense Head-</option>');
                            $('#txtPurchaseType').val("").trigger('change.select2');
                            $('#EquipmentsEntries_expense_type').val('').trigger('change.select2');
                            $('#EquipmentsEntries_employee_id').val('').trigger('change.select2');
                            $('#EquipmentsEntries_bank_id').val('').trigger('change.select2');
                            $('#EquipmentsEntries_vendor_id').html('<option value="">-Select Vendor-</option>');
                        } else if (currAction == "update") {
                            $("#txtDaybookId").val('');
                            $("#buttonsubmit").text("ADD");
                            if (data == 2) {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-success"><strong>Successfully !</strong> Update request send succesfully. Please wait for approval</div>')
                                    .fadeOut(10000);
                                $('.loading-overlay').addClass('is-active');
                                $.ajax({
                                    type: "POST",
                                    data: {
                                        date: crDate
                                    },
                                    url: "<?php echo Yii::app()->createUrl("EquipmentsEntries/getAllData") ?>",
                                    success: function (response) {
                                        document.getElementById("Equipments-form").reset();
                                        console.log(response);
                                        $("#EquipmentsEntries_expense_date").val(crDate);
                                        $("#newlist").html(response);
                                        $("#EquipmentsEntries_project_id").focus();
                                        $("#EquipmentsEntries_bill_id").attr("disabled", false);
                                        $("#EquipmentsEntries_expensetype_id").attr("disabled", false);
                                        $("#EquipmentsEntries_payment_type").attr("disabled", false);
                                        $("#txtReceipt").attr("readonly", false);
                                        $("#buttonsubmit").attr("disabled", false);


                                        $('#EquipmentsEntries_project_id').val('').trigger('change.select2');
                                        $('#EquipmentsEntries_company_id').val('').trigger('change.select2');
                                        $('#EquipmentsEntries_bill_id').val('0').trigger('change.select2');
                                        $('#EquipmentsEntries_expensetype_id').html('<option value="">-Select Expense Head-</option>');
                                        $('#txtPurchaseType').val("").trigger('change.select2');
                                        $('#EquipmentsEntries_expense_type').val('').trigger('change.select2');
                                        $('#EquipmentsEntries_employee_id').val('').trigger('change.select2');
                                        $('#EquipmentsEntries_bank_id').val('').trigger('change.select2');
                                        $('#EquipmentsEntries_vendor_id').html('<option value="">-Select Vendor-</option>');
                                    }
                                });
                            } else if (data == 3) {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-danger"><strong>Failed !</strong> Cannot send update request.</div>')
                                    .fadeOut(10000);
                            } else if (data == 4) {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-warning"><strong>Failed !</strong> Cannot send update request. Already have a request for this.</div>')
                                    .fadeOut(10000);
                            } else {
                                $("#errormessage").show()
                                    .html('<div class="alert alert-success"><strong>Successfully !</strong> record updated.</div>')
                                    .fadeOut(10000);
                            }
                        }
                        document.getElementById("Equipments-form").reset();
                        $("#EquipmentsEntries_expense_date").val(crDate);
                        if (data == 2 || data == 3 || data == 4) {

                        } else {
                            $("#newlist").html(data);
                        }
                        $('#EquipmentsEntries_bill_id').val('0').trigger('change.select2');
                        $('#EquipmentsEntries_expensetype_id').html('<option value="">-Select Expense Head-</option>');
                        $('#txtPurchaseType').val("").trigger('change.select2');
                        $('#EquipmentsEntries_expense_type').val('').trigger('change.select2');
                        $('#EquipmentsEntries_employee_id').val('').trigger('change.select2');
                        $('#EquipmentsEntries_bank_id').val('').trigger('change.select2');
                        $('#EquipmentsEntries_vendor_id').html('<option value="">-Select Vendor-</option>');
                    }
                    window.location.href = '<?php echo Yii::app()->createAbsoluteUrl('EquipmentsEntries/create'); ?>';
                },
                error: function (data) {
                    alert("Error occured.please try again");
                    // alert(data);
                }
            });
        });
        $("#txtPaid").keyup(function () {
            var totalamount = parseFloat($("#txtTotal").val());
            var purchasetype = $("#txtPurchaseType").val();
            var paid = parseFloat($("#txtPaid").val());
            if (purchasetype == 3) {
                if (paid >= totalamount) {
                    $("#errormessage").show()
                        .html('<div class="alert alert-warning">You have selected the purchase type as Partially Paid.<br/>The paid amount must be less than the total amount.</div>')
                        .fadeOut(10000);
                    $("#buttonsubmit").attr('disabled', true);
                    return false;
                } else if (paid <= 0) {
                    $("#errormessage").show()
                        .html('<div class="alert alert-warning">You have selected the purchase type as Partially Paid.<br/>The paid amount must be greater than 0.</div>')
                        .fadeOut(10000);
                    $("#buttonsubmit").attr('disabled', true);
                    return false;
                } else {

                    $("#buttonsubmit").attr('disabled', false);
                }
            }
            if (paid == "") {
                if (totalamount == "") {
                    getTdsCalculations(0);
                } else {
                    getTdsCalculations(totalamount);
                }
            } else {
                getTdsCalculations(paid);
            }
        });

        $("body").on("click", ".row-daybook", function (e) {
            var rowId = $(this).attr("id");
            var expId = $("#EquipmentsId" + rowId).val();
            editRowClick(rowId, expId);
        });



        $("#previous").click(function (e) {
            var cDate = $("#EquipmentsEntries_expense_date").val();
            cDate = cDate.split("-").reverse().join("-");
            var newDate = new Date(cDate);
            var prDate = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate() - 1);
            var newPrDate = $.format.date(prDate, "dd-MM-yyyy");
            $("#EquipmentsEntries_expense_date").val(newPrDate);
            getFullData(newPrDate);
            validateDateSelection(newPrDate);
        });
        $("#current").click(function () {
            var cDate = new Date();
            var newDate = $.format.date(cDate, "dd-MM-yyyy");
            $("#EquipmentsEntries_expense_date").val(newDate);
            getFullData(newDate);
            validateDateSelection(newDate);
        });
        $("#next").click(function () {
            var cDate = $("#EquipmentsEntries_expense_date").val();
            cDate = cDate.split("-").reverse().join("-");
            var newDate = new Date(cDate);
            var nxtDate = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate() + 1);
            var newNxtDate = $.format.date(nxtDate, "dd-MM-yyyy");
            $("#EquipmentsEntries_expense_date").val(newNxtDate);
            getFullData(newNxtDate);
            validateDateSelection(newNxtDate);
        });


        $('.numbersOnly').keyup(function () {
            this.value = this.value.replace(/[^0-9\.]/g, '');
        });


        $("#btnReset, [data-toggle='collapse']").click(function () {
            $("#txtDaybookId").val("");
            $("#buttonsubmit").text("ADD");
            $('#Equipments-form').find('input, select, textarea').not("#EquipmentsEntries_expense_date").val('');
            $("#EquipmentsEntries_expensetype_id").html('<option value="">-Select Expense Head-</option>');
            $("#EquipmentsEntries_vendor_id").html('<option value="">-Select Vendor-</option>');
            $("#EquipmentsEntries_expensetype_id").attr("disabled", false);
            $("#txtPurchaseType").attr("disabled", true);
            $('#EquipmentsEntries_project_id').val('').trigger('change.select2');
            $('#EquipmentsEntries_bill_id').val('0').trigger('change.select2');
            $('#EquipmentsEntries_expensetype_id').val('').trigger('change.select2');
            $('#txtPurchaseType').val('').trigger('change.select2');
            $('#EquipmentsEntries_expense_type').val('').trigger('change.select2');
            $('#EquipmentsEntries_bank_id').val('').trigger('change.select2');
            $('#EquipmentsEntries_vendor_id').val('').trigger('change.select2');
            $('#EquipmentsEntries_employee_id').val('').trigger('change.select2');
            $('#EquipmentsEntries_company_id').val('').trigger('change.select2');
            $('.employee_box').hide();
            $("#txtSgst1").text("");
            $("#txtCgst1").text("");
            $("#txtIgst1").text("");
            $("#txtTotal1").html("");
            $("#txtgstTotal").text("");
            $("#txtTdsp").val("");
            $("#txtTdsp").attr("readonly", false);
            $("#txtTds1").text("");
            $("#txtTds").val("");
            $("#txtTdsPaid").html("");
            $("#EquipmentsEntries_paidamount").val("");
        });
    });


    $("#txtSgstp").keyup(function (e) {
        if (e.keyCode == 13) {
            $("#txtCgstp").focus();
        }
    });

    $("#txtCgstp").keyup(function (e) {
        if (e.keyCode == 13) {
            $("#txtIgstp").focus();
        }
    });

    $("#txtIgstp").keyup(function (e) {
        if (e.keyCode == 13) {
            $("#txtDescription").focus();
        }
    });

    $("#EquipmentsEntries_company_id").change(function () {
        $('#loading').show();
        var company_id = $("#EquipmentsEntries_company_id").val();
        if (company_id != "")
            $.ajax({
                method: "POST",
                dataType: "json",
                url: '<?php echo Yii::app()->createUrl('EquipmentsEntries/ajaxcall'); ?>',
                success: function (result) {
                    $("#EquipmentsEntries_project_id").select2("focus");
                }
            });
    });

    // decimal point check
    $(document).on('keydown', '.check', function (event) {
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
            (event.keyCode >= 96 && event.keyCode <= 105) ||
            event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
            event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
            var splitfield = $(this).val().split(".");
            if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
                event.preventDefault();
            }
        } else {
            event.preventDefault();
        }
        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();
    });

    $('.percentage').keyup(function () {
        if ($(this).val() > 100) {
            $(this).val('');
        }
    });
    // delete confirmation
    $(document).on('click', '.delete_confirmation', function (e) {
        var rowId = $(this).attr("id");
        var expId = $("#EquipmentsId" + rowId).val();
        var date = $("#EquipmentsEntries_expense_date").val();
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                type: "POST",
                data: {
                    expId: expId,
                    expense_date: date,
                    status: 1,
                    delId: "",
                    action: 1
                },
                url: "<?php echo Yii::app()->createUrl("EquipmentsEntries/deleteconfirmation") ?>",
                success: function (data) {
                    $('.loading-overlay').removeClass('is-active');
                    if (data == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-danger"><strong>Failed!</strong> This request unable to process now. Please try again later.</div>')
                            .fadeOut(5000);
                        return false;
                    } else {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Success!</strong> Request for deleting this row is send to admin. Please wait for response.</div>')
                            .fadeOut(5000);
                        $("#newlist").html(data);
                    }
                }
            });
        } else {
            return false;
        }
    });
    // delete restore
    $(document).on('click', '.delete_restore', function (e) {
        var rowId = $(this).attr("id");
        var delId = $(this).attr("data-id");
        var expId = $("#EquipmentsId" + rowId).val();
        var date = $("#EquipmentsEntries_expense_date").val();
        var answer = confirm("Are you sure you want to restore?");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                type: "POST",
                data: {
                    expId: expId,
                    expense_date: date,
                    status: 2,
                    delId: delId,
                    action: 1
                },
                url: "<?php echo Yii::app()->createUrl("EquipmentsEntries/deleteconfirmation") ?>",
                success: function (data) {
                    $('.loading-overlay').removeClass('is-active');
                    if (data == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-danger"><strong>Failed!</strong> This request unable to process now. Please try again later.</div>')
                            .fadeOut(5000);
                        return false;
                    } else {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Success!</strong> Restored data successfully.</div>')
                            .fadeOut(5000);
                        $("#newlist").html(data);
                    }
                }
            });
        } else {
            return false;
        }
    });
    // delete row
    $(document).on('click', '.delete_row', function (e) {
        var answer = confirm("Are you sure you want to delete?");
        if (answer) {
            $('.loading-overlay').addClass('is-active');
            var rowId = $(this).attr("id");
            var expId = $("#EquipmentsId" + rowId).val();
            var date = $("#EquipmentsEntries_expense_date").val();
            $.ajax({
                type: "POST",
                data: {
                    expId: expId,
                    expense_date: date
                },
                url: "<?php echo Yii::app()->createUrl("EquipmentsEntries/deletereport") ?>",
                success: function (data) {
                    $('.loading-overlay').removeClass('is-active');
                    if (data == 1) {
                        $("#errormessage").show()
                            .html('<div class="alert alert-warning"><strong>Failed !</strong> please try again.</div>')
                            .fadeOut(5000);
                        $("#buttonsubmit").attr('disabled', false);
                        return false;
                    } else {
                        $("#errormessage").show()
                            .html('<div class="alert alert-success"><strong>Success!</strong> Row Deleted.</div>')
                            .fadeOut(5000);
                        $("#newlist").html(data);
                    }

                    $('#EquipmentsEntries_project_id').val('').trigger('change.select2');
                    $('#EquipmentsEntries_bill_id').val('0').trigger('change.select2');
                    $('#EquipmentsEntries_expensetype_id').html('<option value="">-Select Expense Head-</option>');
                    $('#txtPurchaseType').val("").trigger('change.select2');
                    $('#EquipmentsEntries_expense_type').val('').trigger('change.select2');
                    $('#EquipmentsEntries_employee_id').val('').trigger('change.select2');
                    $('#EquipmentsEntries_bank_id').val('').trigger('change.select2');
                    $('#EquipmentsEntries_vendor_id').html('<option value="">-Select Vendor-</option>');
                    $('#EquipmentsEntries_company_id').val('').trigger('change.select2');
                    $("#txtAmount").attr("readonly", true);
                    $("#txtSgstp").attr("readonly", true);
                    $("#txtCgstp").attr("readonly", true);
                    $("#txtIgstp").attr("readonly", true);
                    $("#txtPurchaseType").attr("disabled", true);
                    $("#txtPaid").attr("readonly", true);
                    $("#EquipmentsEntries_bank_id").attr("disabled", true);
                    $("#txtChequeno").attr("readonly", true);
                    $("#txtSgst1").text("");
                    $("#txtCgst1").text("");
                    $("#txtIgst1").text("");
                    $("#txtTotal1").html("");
                    $("#txtgstTotal").text("");
                }
            })
        }

    })

    function getTdsCalculations(total) {
        if ($("#txtTdsp").val() != "") {
            var tdsp = parseFloat($("#txtTdsp").val());
        } else {
            var tdsp = 0;
        }

        var tdsamount = (tdsp / 100) * total;
        if (isNaN(tdsamount))
            tdsamount = 0;
        var paidamount = total - parseFloat(tdsamount);
        if (isNaN(paidamount))
            paidamount = 0;

        $("#txtTds1").text(tdsamount.toFixed(2));
        $("#txtTds").val(tdsamount.toFixed(2));
        $("#txtTdsPaid").html("<b>Amount To Be Paid: </b>" + paidamount.toFixed(2) + "");
        $("#EquipmentsEntries_paidamount").val(paidamount.toFixed(2));
    }

    function editRowClick(rowId, expId) {
        $('html, body').animate({
            scrollTop: 0
        }, 'slow');
        $(".daybook .collapse").addClass("in");
        $(".addentries").removeClass("collapsed");
        if ($(".daybook .collapse").css('display') == 'visible') {
            $(".daybook .collapse").css({
                "height": "auto"
            });
        } else {
            $(".daybook .collapse").css({
                "height": ""
            });
        }
        $(".popover").removeClass("in");
        $("#buttonsubmit").text("UPDATE");
        $("#txtEquipmentsEntriesId").val(expId);
        $('.loading-overlay').addClass('is-active');

        // $.ajax({
        //     url: "",
        //     data: {
        //         "EquipmentsEntries_id": expId
        //     },
        //     type: "POST",
        //     success: function(data) {
        //         $('.loading-overlay').removeClass('is-active');
        //         var result = JSON.parse(data);

        //         $("#EquipmentsEntries_company_id").select2("focus");
        //         $('#EquipmentsEntries_company_id').val(result["company_id"]).trigger('change.select2');
        //         $('#EquipmentsEntries_project_id').val(result["project_id"]).trigger('change.select2');
        //         $("#EquipmentsEntries_project_id").val(result["project_id"]);
        //         getProjectList(result["company_id"], 0, result["project_id"], result["subcontractor_id"]);
        //         getExpenseHead(result["subcontractor_id"], result["expensehead_id"]);
        //         $('#EquipmentsEntries_subcontractor_id').val(result["subcontractor_id"]).trigger('change.select2');
        //         $('#EquipmentsEntries_amount').val(result["amount"]).trigger('change.select2');
        //         $(".field_details").html(result["html"]);
        //         $('#EquipmentsEntries_expense_date').val(result['date']);
        //         $('#EquipmentsEntries_description').val(result['description']);
        //     }

        // });
    }


    /*  new section  */
    $("#EquipmentsEntries_expensehead_id").change(function () {
        var id = $(this).val();
        // if (id != '') {
        //     getExpenseFields(id);
        // }
    })

    $("#EquipmentsEntries_subcontractor_id").change(function () {
        var id = $(this).val();
        if (id != '') {
            id = id ? id : 0;
            getExpenseHead(id, 0);
        }
    });

    function getExpenseHead(id, exp) {
        $('#loading').show();
        $.ajax({
            url: "<?php echo $this->createUrl('EquipmentsEntries/getexpensehead'); ?>",
            data: {
                "id": id
            },
            type: "GET",
            success: function (response) {
                if (id == 0) {
                    $("#EquipmentsEntries_expensehead_id").val('-Select Expense Head-').trigger('change.select2');
                } else {
                    $("#EquipmentsEntries_expensehead_id").html(response);
                }
                $("#EquipmentsEntries_expensehead_id").val(exp).trigger('change.select2');
                // if (exp != 0) {

                // }
            }
        });
    }

    // function getExpenseFields(id) {
    //     $('#loading').show();
    //     $.ajax({
    //         type: "GET",
    //         data: {
    //             id: id
    //         },
    //         url: "",
    //         success: function(response) {
    //             $(".field_details").html(response);
    //         }
    //     });
    // }

    $(document).on('blur', '.calculation', function () {
        var amount = parseFloat($("#EquipmentsEntries_amount").val());
        var labour = parseFloat($("#EquipmentsEntries_labour").val());
        var wage = parseFloat($("#EquipmentsEntries_wage").val());
        var wage_rate = parseFloat($("#EquipmentsEntries_wage_rate").val());
        var helper = parseFloat($("#EquipmentsEntries_helper").val());
        var helper_labour = parseFloat($("#EquipmentsEntries_helper_labour").val());
        var lump_sum = parseFloat($("#EquipmentsEntries_lump_sum").val());

        var labour_wage = parseFloat($("#EquipmentsEntries_labour_wage").val());
        var helper_wage = parseFloat($("#EquipmentsEntries_helper_wage").val());

        var total_amount = 0;

        if (isNaN(labour))
            labour = 0;
        if (isNaN(wage))
            wage = 0;
        if (isNaN(wage_rate))
            wage_rate = 0;
        if (isNaN(helper))
            helper = 0;
        if (isNaN(helper_labour))
            helper_labour = 0;
        if (isNaN(lump_sum))
            lump_sum = 0;
        if (isNaN(labour_wage))
            labour_wage = 0;
        if (isNaN(helper_wage))
            helper_wage = 0;

        if ((lump_sum != '') && (labour == '' && wage == '' && wage_rate == '' && helper == '' && helper_labour == '')) {
            total_amount = lump_sum;
        } else if ((labour != '' && wage != '' && wage_rate != '') && (helper == '' && helper_labour == '' && lump_sum == '')) {
            total_amount = labour * wage * wage_rate;
        } else if ((labour != '' && helper != '' && wage != '' && wage_rate != '') && (helper_labour == '' && lump_sum == '')) {
            total_amount = (labour + helper) * (wage * wage_rate);
        } else if ((labour >= 0 && wage_rate >= 0 && labour_wage >= 0 && helper >= 0 && helper_labour >= 0 && helper_wage >= 0)) {
            total_amount = (labour * wage_rate * labour_wage) + (helper * helper_labour * helper_wage);
        } else {
            total_amount = '';
        }
        if (total_amount != '') {
            $("#EquipmentsEntries_amount").val(total_amount.toFixed(2));
        } else {
            $("#EquipmentsEntries_amount").val('');
        }
    });

    $(document).on('click', '.approve', function (e) {
        e.preventDefault();
        var element = $(this);
        var item_id = $(this).attr('id');
        $('.loading-overlay').addClass('is-active');
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('EquipmentsEntries/approve'); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                item_id: item_id
            },
            success: function (response) {
                if (response.response == 'success') {
                    $(".approveoption_" + item_id).hide();
                    element.closest("tr").removeClass("approve_section");
                    element.closest("tr").addClass("tobe_approve");
                    $("#errormessage").show()
                        .html('<div class="alert alert-success"><strong>Success!</strong>' + response.msg + '</div>')
                        .fadeOut(5000);
                } else if (response.response == 'warning') {

                    $("#errormessage").show()
                        .html('<div class="alert alert-warning"><strong>Sorry!</strong>' + response.msg + '</div>')
                        .fadeOut(5000);
                } else {
                    $("#errormessage").show()
                        .html('<div class="alert alert-danger"><strong>Sorry!</strong> ' + response.msg + '</div>')
                        .fadeOut(5000);
                }
            }
        });
    });

    function validateDateSelection(cDate) {
        var selDate = cDate;
        var currDate = new Date();
        currDate = $.format.date(currDate, "dd-MM-yyyy");
        if (selDate == currDate) {
            $("a#next").css("pointer-events", "none").css("color", "#ccccc");
        } else {
            $("a#next").css("pointer-events", "visible").css("color", "#337ab7");
        }
        $("#btnReset").click();
    }
    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
</script>
<style>
    .tobe_approve td:first-child {
        background-color: #fff !important;
    }

    .popover {
        white-space: nowrap;
    }
</style>