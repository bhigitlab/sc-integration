<?php
/* @var $this SubcontractorController */
/* @var $model Subcontractor */

$this->breadcrumbs=array(
	'Subcontractors'=>array('index'),
	'Create',
);

/*$this->menu=array(
	array('label'=>'List Subcontractor', 'url'=>array('index')),
	array('label'=>'Manage Subcontractor', 'url'=>array('admin')),
);*/
?>

<div class="modal-dialog modal-lg" id="projectform">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
            <h4 class="modal-title">Add Quotations</h4>
        </div>
        <?php echo $this->renderPartial('_newform', array('model' => $model)); ?>
    </div>

</div>