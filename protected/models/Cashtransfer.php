<?php

/**
 * This is the model class for table "jp_cashtransfer".
 *
 * The followings are the available columns in table 'jp_cashtransfer':
 * @property integer $cashtransfer_id
 * @property string $cashtransfer_date
 * @property integer $sender_company
 * @property integer $sender_paymenttype
 * @property integer $sender_bank
 * @property string $sender_cheque
 * @property integer $beneficiary_company
 * @property integer $beneficiary_paymenttype
 * @property integer $beneficiary_bank
 * @property string $beneficiary_cheque
 * @property double $cashtransfer_amount
 * @property string $cashtransfer_description
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class Cashtransfer extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jp_cashtransfer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cashtransfer_date, sender_company, sender_paymenttype, beneficiary_company, beneficiary_paymenttype, cashtransfer_amount, cashtransfer_description, created_by, created_date', 'required'),
			array('sender_company, sender_paymenttype, sender_bank, beneficiary_company, beneficiary_paymenttype, beneficiary_bank, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('cashtransfer_amount', 'numerical'),
			array('sender_cheque, beneficiary_cheque', 'length', 'max'=>20),
			array('cashtransfer_description', 'length', 'max'=>500),
			array('updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('cashtransfer_id, cashtransfer_date, sender_company, sender_paymenttype, sender_bank, sender_cheque, beneficiary_company, beneficiary_paymenttype, beneficiary_bank, beneficiary_cheque, cashtransfer_amount, cashtransfer_description, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cashtransfer_id' => 'Cashtransfer',
			'cashtransfer_date' => 'Cashtransfer Date',
			'sender_company' => 'Sender Company',
			'sender_paymenttype' => 'Sender Paymenttype',
			'sender_bank' => 'Sender Bank',
			'sender_cheque' => 'Sender Cheque',
			'beneficiary_company' => 'Beneficiary Company',
			'beneficiary_paymenttype' => 'Beneficiary Paymenttype',
			'beneficiary_bank' => 'Beneficiary Bank',
			'beneficiary_cheque' => 'Beneficiary Cheque',
			'cashtransfer_amount' => 'Cashtransfer Amount',
			'cashtransfer_description' => 'Cashtransfer Description',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cashtransfer_id',$this->cashtransfer_id);
		$criteria->compare('cashtransfer_date',$this->cashtransfer_date,true);
		$criteria->compare('sender_company',$this->sender_company);
		$criteria->compare('sender_paymenttype',$this->sender_paymenttype);
		$criteria->compare('sender_bank',$this->sender_bank);
		$criteria->compare('sender_cheque',$this->sender_cheque,true);
		$criteria->compare('beneficiary_company',$this->beneficiary_company);
		$criteria->compare('beneficiary_paymenttype',$this->beneficiary_paymenttype);
		$criteria->compare('beneficiary_bank',$this->beneficiary_bank);
		$criteria->compare('beneficiary_cheque',$this->beneficiary_cheque,true);
		$criteria->compare('cashtransfer_amount',$this->cashtransfer_amount);
		$criteria->compare('cashtransfer_description',$this->cashtransfer_description,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cashtransfer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
