<?php
Yii::app()->clientscript->scriptMap['jquery.min.js'] = false;
?>
<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script>;-->
<!--<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/additional-methods.js"></script>-->

    <?php
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
$newQuery1 = "";
foreach($arrVal as $arr) {
    if ($newQuery) $newQuery .= ' OR';
    if ($newQuery1) $newQuery1 .= ' OR';
    $newQuery .= " FIND_IN_SET('".$arr."', id)";
    $newQuery1 .= " FIND_IN_SET('".$arr."', company_id)";
}
?>

<div class="">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'payment-reminders-form',
        'enableAjaxValidation' => true,
        'enableClientValidation'=>true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>


<div class="panel-body">
    <div class="row addRow">
        <div class="col-md-6">
            <?php echo $form->labelEx($model, 'company_id'); ?>
            <?php
            $company = Company::model()->findAll(array('condition' =>"($newQuery)"));
            ?>
            <?php echo $form->dropDownList($model, 'company_id', CHtml::listData($company, 'id', 'name'), array('class'=>'form-control','empty' => '--')); ?>
            <?php echo $form->error($model, 'company_id'); ?>
        </div>
        <div class="col-md-6">
            <?php echo $form->labelEx($model, 'client_id'); ?>
            <?php
            $clients = Clients::model()->findAll(array('condition' =>"($newQuery1)"));
            ?>
            <?php echo $form->dropDownList($model, 'client_id', CHtml::listData($clients, 'cid', 'name'), array('class'=>'form-control PaymentReminders_client_id','empty' => '--')); ?>
            <?php echo $form->error($model, 'client_id'); ?>
        </div>
    </div>
    
    <div class="row addRow">
        <div class="col-md-6">
            <?php echo $form->labelEx($model, 'project_id'); ?>
            <?php
             if (!$model->isNewRecord) {
                $projects = Projects::model()->findAll(array('condition' =>"($newQuery1) AND client_id='$model->client_id'"));
            ?>
            <?php echo $form->dropDownList($model, 'project_id', CHtml::listData($projects, 'pid', 'name'), array('class'=>'form-control','empty' => '--')); ?>
             <?php }else { ?>
            <?php echo $form->dropDownList($model, 'project_id', array()); ?>
             <?php } ?>
            <?php echo $form->error($model, 'project_id'); ?>
        </div>
    </div>
    
    <div class="row addRow">
        <div class="col-md-6">
            <?php echo $form->labelEx($model, 'amount'); ?>
            <?php echo $form->textField($model, 'amount',array('class'=>'form-control')); ?>
            <?php echo $form->error($model, 'amount'); ?>
        </div>
        <div class="col-md-6">
            <?php echo $form->labelEx($model, 'date'); ?>
            <?php echo CHtml::activeTextField($model, 'date', array(
                'class' => 'form-control',
                'readonly' => 'true',
                'value' => isset($model->date) ? date("d-M-Y", strtotime($model->date)) : ''
            )); ?>
            
            <?php
            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => CHtml::activeId($model, 'date'), // use activeId to get the correct input field ID
                'ifFormat' => '%d-%b-%Y',
            ));
            ?>

            <?php echo $form->error($model, 'date'); ?>
        </div>
    </div>


<!--        <div class="row addRow">
                    <div class="col-md-6">	
                    <label>Company :</label>
                    <ul class="checkboxList">
                            <?php
                            $user = Users::model()->findByPk(Yii::app()->user->id);
                            $arrVal = explode(',', $user->company_id);
                            $newQuery = "";
                            foreach($arrVal as $arr) {
                                if ($newQuery) $newQuery .= ' OR';
                                $newQuery .= " FIND_IN_SET('".$arr."', id)";
                            }
                            $typelist = Company::model()->findAll(array('condition' => $newQuery));
                            $assigned_company_array = array();
                            if (!$model->isNewRecord) {
                                $assigned_types = PaymentReminders::model()->find(array('condition' => 'id=' . $model->id));
                                $assigned_company_array = explode(",",$assigned_types->company_id);
                                if((isset(Yii::app()->user->role) && (in_array('/clients/companypermission', Yii::app()->user->menuauthlist)))){
                                    $readonly = "";
                                }else{
                                    $readonly = "readonly";
                                }
                                
                            }else{
                                $assigned_company_array ="";
                                $readonly = "";
                            }
                           echo CHtml::checkBoxList('Clients[company_id]', $assigned_company_array, CHtml::listData($typelist, 'id','name'), array('checkAll' => 'Check all','template' => '<li class="checkboxtype">{input}{label}</li>', 'separator' => '','readonly'=>$readonly));
                        ?>				
                        </ul>				
                    <?php echo $form->error($model, 'company_id'); ?>
                </div>
                </div>-->
         <div class="row addRow">
           <div class="col-md-12">
                <?php echo $form->labelEx($model, 'description'); ?>
                <?php echo $form->textArea($model, 'description',array('class'=>'form-control','rows' => 6, 'cols' => 50)); ?>
                <?php echo $form->error($model, 'description'); ?>
            </div>
            
        </div>
    </div>

    <div class="panel-footer save-btnHold text-center">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-info')); ?> 
        <?php if (Yii::app()->user->role == 1) { 
        	if(!$model->isNewRecord){  /*
        	?> 
            <a class="btn del-btn" href="<?php echo $this->createUrl('clients/deleteclients', array('id' => $model->cid)) ?>" class="deletebtn" onclick="return confirm('Are you sure that you want to delete?');">Delete </a>                           
        <?php */ } } ?>
<!--        <button data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>-->
            <?php if(!$model->isNewRecord){
                echo CHtml::ResetButton('Close', array('onclick'=>'closeaction(this,event);','class'=>'btn')); 
                } 
                else{ 
                    echo CHtml::ResetButton('Reset', array('style'=>'margin-right:3px;','class'=>'btn btn-default')); 
                    echo CHtml::ResetButton('Close', array('onclick'=>'closeaction(this,event);','class'=>'btn'));  
                }
                ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->

<script>

 
    
    $(".PaymentReminders_client_id").change(function(){
	var val = $(this).val();
	$("#PaymentReminders_project_id").html('<option value="">Select Project</option>'); 
        $.ajax({
            url:'<?php echo Yii::app()->createUrl('paymentReminders/dynamicproject'); ?>',
            method:'GET',
            data:{client_id:val},
            dataType:"json",
            success:function(response){
                $("#PaymentReminders_project_id").html(response.html);
            }

        })
    })
</script>
<script>

jQuery.browser = {};
(function () {
jQuery.browser.msie = false;
jQuery.browser.version = 0;
if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
jQuery.browser.msie = true;
jQuery.browser.version = RegExp.$1;
}
})();
</script>
<style>
    .addRow label {display: inline-block;}
    input[type="radio"]{
        margin: 4px 4px 0;
    }

    input[type="checkbox"][readonly] {
        pointer-events: none;
      }            
</style>
<script>
     

</script>

