<?php
$pms_api_integration = ApiSettings::model()->pmsIntegrationStatus();


$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
foreach ($arrVal as $arr) {
    if ($newQuery)
        $newQuery .= ' OR';
    $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
}

if ((isset(Yii::app()->user->role) && (in_array('/projects/companypermission', Yii::app()->user->menuauthlist)))) {
    $company_readonly = "";
} else {
    $company_readonly = "readonly";
}

if ((isset(Yii::app()->user->role) && (in_array('/projects/userspermission', Yii::app()->user->menuauthlist)))) {
    $user_readonly = "";
} else {
    $user_readonly = "readonly='readonly'";
}

if ((isset(Yii::app()->user->role) && (in_array('/projects/expensetypepermission', Yii::app()->user->menuauthlist)))) {
    $expense_readonly = "";
} else {
    $expense_readonly = "readonly='readonly'";
}

if ((isset(Yii::app()->user->role) && (in_array('/projects/worktypespermission', Yii::app()->user->menuauthlist)))) {
    $worktpe_readonly = "";
} else {
    $worktpe_readonly = "readonly='readonly'";
}

if ((isset(Yii::app()->user->role) && (in_array('/projects/projectquotepermission', Yii::app()->user->menuauthlist)))) {
    $quote_readonly = "";
} else {
    $quote_readonly = true;
}

if ((isset(Yii::app()->user->role) && (in_array('/projects/profitmarginpermission', Yii::app()->user->menuauthlist)))) {
    $margin_readonly = "";
} else {
    $margin_readonly = true;
}
?>

<div class="">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'projects-form',
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,
        ),
    ));
    ?>
    <?php echo $form->hiddenField($model, 'pms_project_id'); ?>
    <div class="custom-form-style">
        <div class="row">
            <input type="hidden" id="pms_integration" value="<?php echo $pms_api_integration; ?>">
            <div class="col-lg-2 col-md-3 col-sm-4 form-group">
                <?php if (Yii::app()->user->role != 10) { ?>
                    <?php echo $form->labelEx($model, 'name'); ?>
                    <?php
                    if ($model->created_from == 3) {
                        // Render the field as read-only
                        echo CHtml::textField('name', $model->name, array('size' => 60, 'maxlength' => 100, 'class' => 'form-control', 'readonly' => true));
                    } else {
                        // Render the field as editable
                        echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control'));
                    }
                    ?>
                    <?php echo $form->error($model, 'name'); ?>
                <?php } ?>
            </div>

            <div class="col-lg-2 col-md-3 col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'Client <span class="required">*</span>'); ?>
                <?php
                if ($model->created_from == 3) {
                    // Render the dropdown as read-only by disabling it
                    echo CHtml::dropDownList(
                        'client_id',
                        $model->client_id,
                        CHtml::listData(
                            Clients::model()->findAll(array(
                                'order' => 'name ASC',
                                'condition' => 'status= 1 AND (' . $newQuery . ')'
                            )),
                            'cid',
                            'name'
                        ),
                        array(
                            'empty' => '--',
                            'class' => 'form-control',
                            'disabled' => true // Disable the dropdown
                        )
                    );
                } else {
                    // Render the dropdown as editable
                    echo $form->dropDownList(
                        $model,
                        'client_id',
                        CHtml::listData(
                            Clients::model()->findAll(array(
                                'order' => 'name ASC',
                                'condition' => 'status= 1 AND (' . $newQuery . ')'
                            )),
                            'cid',
                            'name'
                        ),
                        array(
                            'empty' => '--',
                            'class' => 'form-control'
                        )
                    );
                }
                ?>
                <?php echo $form->error($model, 'client_id'); ?>
            </div>
            <div class="col-md-2 col-sm-4 form-group">
                <?php echo $form->label($model, 'start_date'); ?>
                <?php
                $startDateValue = !empty($model->start_date) ? date("d-M-Y", strtotime($model->start_date)) : '';
                echo CHtml::activeTextField($model, 'start_date', array(
                    'class' => 'form-control',
                    'size' => 10,
                    'value' => $startDateValue
                ));
                ?>
                <?php
                $this->widget('application.extensions.calendar.SCalendar', array(
                    'inputField' => 'Projects_start_date',
                    'ifFormat' => '%d-%b-%Y',
                ));
                echo $form->error($model, 'start_date');
                ?>
            </div>
            <div class="col-md-2 col-sm-4 form-group">
                <?php echo $form->label($model, 'completion_date'); ?>
                <?php
                // Check if completion_date is null or empty
                $completionDateValue = !empty($model->completion_date) ? date("d-M-Y", strtotime($model->completion_date)) : '';
                echo CHtml::activeTextField($model, 'completion_date', array(
                    'class' => 'form-control',
                    'size' => 10,
                    'readonly' => 'true', // Set to readonly
                    'value' => $completionDateValue // Set the value dynamically
                ));
                ?>
                <?php
                $this->widget('application.extensions.calendar.SCalendar', array(
                    'inputField' => 'Projects_completion_date',
                    'ifFormat' => '%d-%b-%Y',
                ));
                ?>
            </div>
            <div class="col-md-2 col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'project_duration'); ?>
                <?php echo $form->textField($model, 'project_duration', array('size' => 60, 'maxlength' => 200, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'project_duration'); ?>
            </div>
            <?php if (Yii::app()->user->role != 10) { ?>
                <div class="col-lg-2 col-md-3 col-sm-4 form-group">
                    <?php echo $form->labelEx($model, 'site'); ?>
                    <?php echo $form->textField($model, 'site', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                </div>
            <?php } ?>
            <div class="col-lg-2 col-md-3 col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'project_category'); ?>
                <div>
                    <?php echo $form->radioButton($model, 'project_category', array('value' => '104')) . ' Residential'; ?>
                    <?php echo $form->radioButton($model, 'project_category', array('value' => '105')) . ' Commercial'; ?>
                </div>
                <?php echo $form->error($model, 'project_category'); ?>
            </div>

            <?php if (Yii::app()->user->role != 10) { ?>
                <div class="col-lg-2 col-md-3 col-sm-4 form-group">
                    <?php echo $form->labelEx($model, 'project_status'); ?>
                    <?php echo $form->dropDownList($model, 'project_status', CHtml::listData(Status::model()->findAll(
                        array(
                            'select' => array('sid,caption'),
                            'condition' => 'status_type="project_status"',
                            'order' => 'caption',
                            'distinct' => true
                        )
                    ), 'sid', 'caption'), array('empty' => '--', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'project_status'); ?>
                </div>

                <div class="col-lg-2 col-md-3 col-sm-4 form-group">
                    <?php echo $form->labelEx($model, 'project_type'); ?>
                    <?php echo $form->dropDownList($model, 'project_type', CHtml::listData(Status::model()->findAll(
                        array(
                            'select' => array('sid,caption'),
                            'condition' => 'status_type="project_type"',
                            'order' => 'caption',
                            'distinct' => true
                        )
                    ), 'sid', 'caption'), array('empty' => '--', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'project_type'); ?>
                </div>

                <div class="col-lg-2 col-md-3 col-sm-4 form-group">
                    <?php echo $form->labelEx($model, 'sqft'); ?>
                    <?php echo $form->textField($model, 'sqft', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'sqft'); ?>
                </div>

                <div class="col-lg-2 col-md-3 col-sm-4 form-group hidbox">
                    <?php echo $form->labelEx($model, 'sqft_rate'); ?>
                    <?php echo $form->textField($model, 'sqft_rate', array('onChange' => 'sum1()', 'onBlur' => 'sum1()', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'sqft_rate'); ?>
                </div>

                <div class="col-lg-2 col-md-3 col-sm-4 form-group hidbox1">
                    <?php echo $form->labelEx($model, 'percentage'); ?>
                    <?php echo $form->textField($model, 'percentage', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'percentage'); ?>
                </div>
            <?php } ?>
            <?php if (Yii::app()->user->role != 10) { ?>
                <div class="col-md-2 col-sm-4 form-group">
                    <?php echo $form->labelEx($model, 'project_quote'); ?>
                    <?php echo $form->textField($model, 'project_quote', array('class' => 'form-control', 'readonly' => $quote_readonly)); ?>
                    <?php echo $form->error($model, 'project_quote'); ?>
                </div>
                <div class="col-md-2 col-sm-4 form-group">
                    <label>Profit Margin (%)</label>
                    <?php echo $form->textField($model, 'profit_margin', array('class' => 'form-control', 'readonly' => $margin_readonly)); ?>
                    <?php echo $form->error($model, 'profit_margin'); ?>
                </div>

            <?php } ?>

            <?php
            $buyermodule = GeneralSettings::model()->checkBuyerModule();
            if ($buyermodule) { ?>
                <div class="col-lg-2 col-md-3 col-sm-4 form-group">
                    <?php echo $form->labelEx($model, 'number_of_flats'); ?>
                    <?php echo $form->textField($model, 'number_of_flats', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'number_of_flats'); ?>
                </div>
                <div class="col-md-3 col-sm-4 form-group">
                    <?php echo $form->labelEx($model, 'flat_names'); ?>
                    <?php echo $form->textArea($model, 'flat_names', array('rows' => 3, 'cols' => 10, 'class' => 'form-control')); ?>
                    <span class="comment">(comma separted values)</span>
                    <?php echo $form->error($model, 'flat_names'); ?>
                </div>
            <?php } ?>
            <div class="col-md-2 col-sm-4 form-group">
                <?php echo $form->labelEx($model, 'incharge'); ?>
                <?php echo $form->textField($model, 'incharge', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'incharge'); ?>
            </div>
        </div>
        <div class="row">
            <?php if (Yii::app()->user->role != 10) { ?>
                <div class="col-lg-4 col-md-6 col-md-12 textArea-box">
                    <?php echo $form->labelEx($model, 'description'); ?>
                    <?php echo $form->textArea($model, 'description', array('rows' => 3, 'cols' => 50, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'description'); ?>
                </div>
            <?php } ?>
            <div class="col-lg-4 col-md-6 form-group">
                <?php echo $form->labelEx($model, 'company_id'); ?>
                <ul class="checkboxList">
                    <?php
                    $user = Users::model()->findByPk(Yii::app()->user->id);
                    $arrVal = explode(',', $user->company_id);
                    $newQuery = "";
                    foreach ($arrVal as $arr) {
                        if ($newQuery)
                            $newQuery .= ' OR';
                        $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
                    }
                    $typelist = Company::model()->findAll(array('condition' => $newQuery));
                    $assigned_company_array = array();
                    $assigned_types = Projects::model()->find(array('condition' => 'pid=' . $model->pid));
                    if ($assigned_types->company_id != NULL) {
                        $assigned_types_array = explode(",", $assigned_types->company_id);
                    } else {
                        $assigned_types_array = '';
                    }
                    echo CHtml::checkBoxList('Projects[company_id]', $assigned_types_array, CHtml::listData($typelist, 'id', 'name'), array('checkAll' => 'Select All', 'template' => '<li class="checkboxtype companyall">{input}{label}</li>', 'separator' => '', 'readonly' => $company_readonly));
                    ?>
                </ul>
                <?php echo $form->error($model, 'company_id'); ?>
            </div>
            <div class="col-lg-4 col-md-6 form-group">
                <label>Assign to :</label>
                <ul class="checkboxList">
                    <li><input <?php echo $user_readonly; ?> type="checkbox" id='select_all' value='0'>Select All</li>
                    <?php
                    $userslist = Users::model()->findAll(array('condition' => ''));
                    $assigned_users_array = array();
                    if (!$model->isNewRecord) {
                        $assigned_users = ProjectAssign::model()->findAll(array('condition' => 'projectid=' . $model->pid));
                        foreach ($assigned_users as $auser) {
                            $assigned_users_array[] = $auser['userid'];
                        }
                    }

                    foreach ($userslist as $user) {
                        ?>
                        <li><input <?php echo $user_readonly; ?> type="checkbox" class="checkbox" <?php echo (in_array($user->userid, $assigned_users_array) ? 'checked="checked"' : ''); ?>
                                name="user[]" value='<?php echo $user->userid ?>' /> <?php echo $user->first_name ?>
                        </li>
                        <?php
                    }
                    ?>
                </ul>

            </div>
            <div class="col-lg-4 col-md-6 form-group">
                <label style="display:inline-block;margin-right: 5px;">Expense Head :</label>
                <div style="display:inline-block">
                    <?php echo $form->checkBox($model, 'auto_update'); ?>
                    <label style="display:inline-block;margin-right: 5px;">Auto Update</label><i>( Loads Expense head
                        list automatically, disregarding the assigned expense head )</i>
                </div>

                <ul class="checkboxList">
                    <li><input <?php echo $expense_readonly; ?> type="checkbox" id='select_alltype' value='0'>Select All
                    </li>
                    <?php
                    $typelist = ExpenseType::model()->findAll();
                    $assigned_types_array = array();
                    if (!$model->isNewRecord) {
                        $assigned_types = ProjectExpType::model()->findAll(array('condition' => 'project_id=' . $model->pid));
                        foreach ($assigned_types as $atype) {
                            $assigned_types_array[] = $atype['type_id'];
                        }
                    }

                    foreach ($typelist as $type) {
                        ?>
                        <li><input <?php echo $expense_readonly; ?> type="checkbox" class="checkboxtype1" <?php echo (in_array($type->type_id, $assigned_types_array) ? 'checked="checked"' : ''); ?>
                                name="type[]" value='<?php echo $type->type_id ?>' /> <?php echo $type->type_name ?>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
            <div class="col-lg-4 col-md-6 form-group">
                <label>Labour Template :</label>
                <ul class="checkboxList">
                    <?php
                    $templatelist = LabourTemplate::model()->findAll(
                        $pms_api_integration == 1 ?
                        array('condition' => 'pms_labour_template_id != :pms_id', 'params' => array(':pms_id' => 0)) :
                        array()
                    );
                    $assigned_template_array = [];

                    if (!$model->isNewRecord) {
                        $assigned_template_array = Yii::app()->db->createCommand("SELECT DISTINCT template_id FROM `jp_project_labour` WHERE project_id=" . $model->pid)->queryColumn();

                        $existingDailyReport = Dailyreport::model()->findAllByAttributes(array('projectid' => $model->pid));
                        $dailyLabourTemplateData = [];
                        foreach ($existingDailyReport as $dailyReport) {
                            $dailyLabourTemplateData[] = $dailyReport->labour_template;
                        }

                        $usedTemplateIds = array_unique($dailyLabourTemplateData);

                        foreach ($templatelist as $template) {
                            $isChecked = in_array($template->id, $assigned_template_array) ? 'checked="checked"' : '';
                            $isUnclickable = in_array($template->id, $usedTemplateIds) ? 'onclick="return false;"' : '';
                            ?>
                            <li>
                                <input type="checkbox" class="po_checkbox" <?php echo $isChecked . ' ' . $isUnclickable; ?>
                                    name="Projects[labour_template][]" value="<?php echo $template->id ?>" />
                                <?php echo $template->template_label ?>
                            </li>
                            <?php
                        }
                    } else {
                        foreach ($templatelist as $template) {
                            ?>
                            <li><input type="checkbox" class="po_checkbox" name="Projects[labour_template][]"
                                    value='<?php echo $template->id ?>' /> <?php echo $template->template_label ?>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
                <span style="color:red">**</span><span>NOTE: Used labour templates cannot be editable to ensure
                    accurate record-keeping </span>
                <?php echo $form->error($model, 'labour_template'); ?>
            </div>
            <div class="col-lg-4 col-md-6 form-group" id="selected_labour_templates">
                <label>&nbsp;</label>
                <div id="dialog-modal" class="" style="max-height: 150px;overflow: auto;"></div>
            </div>
            <div class="col-lg-4 col-md-6 form-group">
                <?php echo $form->labelEx($model, 'remarks'); ?>
                <?php echo $form->textArea($model, 'remarks', array('rows' => 3, 'cols' => 50, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'remarks'); ?>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xs-12 text-right">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info')); ?>
                <?php echo CHtml::ResetButton('Close', array('onclick' => 'closeaction(this,event);', 'class' => 'btn btn-defualt')); ?>
                <?php echo CHtml::hiddenField('execute_api', 0); ?>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>

<script>
    function clearSelectedLaboursTable() {
        var selectedLaboursTable = document.getElementById('dialog-modal');
        selectedLaboursTable.innerHTML = '';
    }

    function podefault() {
        var selectedTemplates = [];
        var selectElement = $('select.po_select2');
        $('input.po_checkbox:checked').each(function () {
            selectedTemplates.push($(this).val());
        });

        var project = <?php echo isset($model->pid) ? $model->pid : '' ?>;
        var type = '';
        type = 1;
        if (selectedTemplates == '') {
            // Do something if template is empty
        } else {
            if (selectedTemplates != 0) {
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('projects/getTemplateDetails'); ?>',
                    method: 'POST',
                    data: {
                        template_ids: selectedTemplates,
                        type: type,
                        project: project,
                    },
                    success: function (response) {
                        $('#dialog-modal').html(response);
                        $('#selected_labour_templates').show();
                        $('.labour-rate').each(function () {
                            $(this).prop('readonly', false); // Make input fields editable
                        });
                    }
                });
            } else {
                $('#dialog-modal').html("");
                $('#selected_labour_templates').hide();
            }
        }
    }

    podefault();

    function sum1() {
        var txtFirstNumberValue = $('#Projects_sqft').val();
        var txtSecondNumberValue = $('#Projects_sqft_rate').val();
        if (txtFirstNumberValue == "")
            txtFirstNumberValue = 0;
        if (txtSecondNumberValue == "")
            txtSecondNumberValue = 0;

        var result = parseInt(txtFirstNumberValue) * parseInt(txtSecondNumberValue);
        if (!isNaN(result)) {
            $('#Projects_bill_amount').val(result);
        }
    }
    $(function () {
        $('#projects-form').find('ul.checkboxList li').each(function () {
            var x = $(this).parents("ul").find('li').length - 1;
            var checkedcount = $(this).parents("ul").find('li').find('input:checkbox:checked').length;
            if (x == checkedcount) {
                $(this).parents("ul.checkboxList").find("li:first-child input").attr("checked", "checked");
            }
        });

        $("#select_all").change(function () {
            var status = this.checked;
            $('.checkbox').each(function () {
                this.checked = status;
            });
        });

        $('.checkbox').change(function () {
            if (this.checked == false) {
                $("#select_all")[0].checked = false;
            }

            if ($('.checkbox:checked').length == $('.checkbox').length) {
                $("#select_all")[0].checked = true;
            }
        });
        $("#select_alltype").change(function () {
            var status = this.checked;
            $('.checkboxtype1').each(function () {
                this.checked = status;
            });
        });
        function initializeAutoUpdate() {
            if ($("#Projects_auto_update").is(':checked')) {
                $("#select_alltype").addClass('readonly-checkbox');
                // Add the class to make checkboxes appear readonly
                $('.checkboxtype1').each(function () {
                    if ($(this).is(':checked')) {
                        $(this).addClass('readonly-checkbox');
                    }

                });
            }
        }
        initializeAutoUpdate();


        $("#Projects_auto_update").change(function () {
            var isAutoUpdate = this.checked;

            if (isAutoUpdate) {
                // Check #select_alltype and apply behavior
                $("#select_alltype").prop('checked', true);
                $("#select_alltype").addClass('readonly-checkbox');
                $('.checkboxtype1').each(function () {
                    this.checked = true;
                    // Add a class to simulate read-only (but not disabled)
                    $(this).addClass('readonly-checkbox');
                });
            } else {
                $("#select_alltype").removeClass('readonly-checkbox');
                $('.checkboxtype1').each(function () {
                    $(this).removeClass('readonly-checkbox');

                });
            }
        });
        $("#select_allwrktype").change(function () {
            var status = this.checked;
            $('.checkboxwrktype').each(function () {
                this.checked = status;
            });
        });
        $("#select_allwrktypepms").change(function () {
            var status = this.checked;
            $('.checkboxwrktypepms').each(function () {
                this.checked = status;
            });
        });


        $('.checkboxtype1').change(function () {
            if (this.checked == false) {
                $("#select_alltype")[0].checked = false;
            }

            if ($('.checkboxtype1:checked').length == $('.checkboxtype1').length) {
                $("#select_alltype")[0].checked = true;
            }
        });
        $('.checkboxwrktype').change(function () {
            if (this.checked == false) {
                $("#select_allwrktype")[0].checked = false;
            }

            if ($('.checkboxwrktype:checked').length == $('.checkboxwrktype').length) {
                $("#select_allwrktype")[0].checked = true;
            }
        });
        $('.checkboxwrktypepms').change(function () {
            if (this.checked == false) {
                $("#select_allwrktypepms")[0].checked = false;
            }

            if ($('.checkboxwrktypepms:checked').length == $('.checkboxwrktypepms').length) {
                $("#select_allwrktypepms")[0].checked = true;
            }
        });
        $("#select_alltemplate").change(function () {
            var status = this.checked;
            $('.po_checkbox').each(function () {
                this.checked = status;
            });
            $('.po_checkbox').trigger('change');
        });
        var anca_pms_linked = '<?php echo ANCAPMS_LINKED; ?>';
        if (anca_pms_linked) {
            $('#Projects_name').addClass('nonClickable');
            $('#Projects_client_id').addClass('nonClickable');
            $('.companyall').addClass('nonClickable');
            $('#Projects_project_status').addClass('nonClickable');
            $('#Projects_project_type').addClass('nonClickable');
        }
    });
    if ($("#Projects_project_type").val() == 87) {
        $('.hidbox').show();
    } else {
        $('.hidbox').hide();
    }

    if ($("#Projects_project_type").val() == 86) {
        $('.hidbox1').show();
        $('.billam').hide();
    } else {
        $('.hidbox1').hide();
        $('.billam').show();
    }
    $(document).on("change", "#Projects_project_type", function () {
        if ($("#Projects_project_type").val() == 87) {
            $('.hidbox').show();
        } else {
            $('.hidbox').hide();
        }

    });
    $(document).on("change", "#Projects_project_type", function () {
        if ($("#Projects_project_type").val() == 86) {
            $('.hidbox1').show();
            $('.billam').hide();
        } else {
            $('.hidbox1').hide();
            $('.billam').show();
        }

    });
    $('input.po_checkbox').change(function () {
        // clearSelectedLaboursTable();
        var template = $(this).val();
        var selectedTemplates = [];
        var project = <?php echo isset($model->pid) ? $model->pid : '' ?>;
        var type = '';
        $('input.po_checkbox:checked').each(function () {
            selectedTemplates.push($(this).val());
        });
        type = 1;
        if (selectedTemplates.length > 0) {
            $.ajax({
                url: '<?php echo Yii::app()->createUrl('projects/getTemplateDetails'); ?>',
                method: 'POST',
                data: {
                    template_ids: selectedTemplates,
                    type: type,
                    project: project,
                },
                success: function (response) {
                    console.log(response);
                    $('#dialog-modal').html(response);
                }
            })

        } else {
            $('#dialog-modal').html("");
        }
    });
    $(document).ready(function () {
        document.getElementById('projects-form').onsubmit = function () {
            var pms_integration = $("#pms_integration").val();
            var pms_project_id = $("#Projects_pms_project_id").val();
            if (pms_integration == '1') {
                if (pms_project_id == '0') {
                    var confirmApiCall = confirm("Do you want to save the project in Pms?");
                    if (confirmApiCall) {
                        document.getElementById('execute_api').value = 1;
                    }
                }
                else if (pms_project_id != '0') {
                    document.getElementById('execute_api').value = 1;
                }

            }
        };
    });
</script>