<?php

class LaboursController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        /*return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'newlist'),
                'users' => array('@'),
                'expression' => 'yii::app()->user->role<=3',
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'savetopdf','savetoexcel'),
                'users' => array('@'),
                'expression' => 'yii::app()->user->role<=2',
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
                'expression' => 'yii::app()->user->role==1',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );*/
        $accessArr = array();
        $accessauthArr = array();
        $controller = Yii::app()->controller->id;
        if (isset(Yii::app()->user->menuall)) {
            if (array_key_exists($controller, Yii::app()->user->menuall)) {
                $accessArr = Yii::app()->user->menuall[$controller];
            }
        }

        if (isset(Yii::app()->user->menuauth)) {
            if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                $accessauthArr = Yii::app()->user->menuauth[$controller];
            }
        }
        $access_privlg = count($accessauthArr);
        return array(
            array(
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => $accessArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),

            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        
          
        $model = new LabourWorktype;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['LabourWorktype'])) {
            $model->attributes = $_POST['LabourWorktype'];
            $model->is_active='1';
             if ($model->save()){
                if ($_POST['execute_api'] == 1) {
                    $pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();

                    if($pms_api_integration ==1){
                        $request = [
                            'origin' => 'coms',
                            'labour_id' =>$model->type_id,
                            'name' => $model->worktype,
                            'coms_api_log_id' => $model->type_id,
                        ];
                        $slug="api/create-labour";
                        $method="POST";
                            
                        $globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
                    }
                }
                $this->redirect(array('newList'));
             }     
        }
        if (isset($_GET['layout']))
            $this->layout = false;
        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['LabourWorktype'])) {
            $model->attributes = $_POST['LabourWorktype'];
           
            if ($model->save())
                $pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
                if ($_POST['execute_api'] == 1) {
                if($pms_api_integration ==1){
                    $request = [
                        'origin' => 'coms',
                        'labour_id' =>$model->type_id,
                        'name' => $model->worktype,
                        'coms_api_log_id' => $model->type_id,
                        'pms_labour_id'=>$model->pms_labour_id
                    ];
                    if (!empty($model->pms_labour_id)) {
                        $slug="api/update-labour";
                        $request['pms_labour_id'] = $model->pms_labour_id;
                    } else {
                        $slug = "api/create-labour";
                    }
                    $method="POST";
                        
                    $globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
                }
                }

                if (Yii::app()->user->returnUrl != 0) {
                    $this->redirect(array('newlist', 'LabourWorktype_page' => Yii::app()->user->returnUrl));
                } else {
                    $this->redirect(array('newlist'));
                }
        }
        if (isset($_GET['layout']))
            $this->layout = false;

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    	public function actionDelete($id)
    	{
    		$model = $this->loadModel($id);
            try{
                $sql1 = "SELECT COUNT(*) FROM `jp_labour_template` WHERE FIND_IN_SET($id,labour_type)";
				$relatedlabourTypeCount = Yii::app()->db->createCommand($sql1)->queryScalar();
                if(!$relatedlabourTypeCount){    
                    if ($model->delete()) {
                        $response = [
                            'success' => 'success',
                            'message' => 'Record deleted successfully.'
                        ];
                    } else {
                        throw new Exception(json_encode($model->getErrors()));
                    
                    }
                }else{
                    $error_message = "You cannot delete this record because it is associated with other records.";
                    throw new Exception(json_encode($error_message));
                }
            }catch(Exception $e){
                $error_message = "You cannot delete this record because it is associated with other records.";
                $response = [
                    'success' => 'danger',
                    'message' =>$error_message
                ];
            }            
            echo json_encode($response);
            }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('LabourWorktype');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new LabourWorktype('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['LabourWorktype']))
            $model->attributes = $_GET['LabourWorktype'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return LabourWorktype the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = LabourWorktype::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param LabourWorktype $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'expense-type-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionNewlist()
    {
       
        $model = new LabourWorktype('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['LabourWorktype']))
            $model->attributes = $_GET['LabourWorktype'];

        $dataProvider = $model->search(); // Assuming default search without user ID

        if (Yii::app()->user->role != 1) {
            $dataProvider = $model->search(Yii::app()->user->id); // Override dataProvider if not role 1
        }

        $this->render('newlist', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
        ));

    }

    public function actionSavetopdf()
    {
        $this->logo = $this->realpath_logo;
        $tblpx = Yii::app()->db->tablePrefix;
        $qry = "SELECT * FROM " . $tblpx . "expense_type";
        $data = Yii::app()->db->createCommand($qry)->queryAll();

        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
        $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $mPDF1->AddPage('','', '', '', '', 0,0,40, 30, 10,0);
        $mPDF1->WriteHTML($this->renderPartial('LabourWorktypepdf', array(
            'model' => $data,
        ), true));
        $filename = "Expense Heads";
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    public function actionSavetoexcel()
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $qry = "SELECT * FROM " . $tblpx . "expense_type";
        $data = Yii::app()->db->createCommand($qry)->queryAll();
        $finaldata = array();
        $arraylabel = array('Sl No.', 'Expense Head');
        $i = 1;
        $finaldata = array();
        foreach ($data as $key => $datavalue) {
            $finaldata[$key][] = $i;
            $finaldata[$key][] = $datavalue['type_name'];
            $i++;
        }

        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'expense_head.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }
}
