<?php
$tblpx = Yii::app()->db->tablePrefix;
if ($index == 0) {
?>
    <thead>
        <tr>
            <th>Specifications/Remark</th>
            <th>HSN Code</th>
            <th>Batch</th>
            <th>Quantity</th>
            <th>Unit</th>
            <th>Rate</th>
            <th>Base Quantity</th>
            <th>Base Unit</th>
            <th>Base Rate</th>
            <th>Amount</th>
            <th>Tax Slab (%)</th>
            <th>Discount Amount</th>
            <th>Discount (%)</th>
            <th>CGST (%)</th>
            <th>CGST</th>
            <th>SGST (%)</th>
            <th>SGST</th>
            <th>IGST (%)</th>
            <th>IGST</th>
            <th>Total Tax</th>
            <th>Tax (%)</th>
            <th>Total Amount</th>
            <?php
            if ((isset(Yii::app()->user->role) && ((in_array('/expenses/expensesdelete', Yii::app()->user->menuauthlist)) || (in_array('/expenses/expensesedit', Yii::app()->user->menuauthlist))))) {
            ?>
                <th></th>
            <?php } ?>

        </tr>
    </thead>
<?php } ?>
<tbody>
    <?php

    if ($data['category_id']) {
        $categorymodel = $this->GetItemCategoryById($parent = 0, $spacing = '', $user_tree_array = '', $data['category_id']);
        if ($categorymodel) {
            $categoryName = $categorymodel["data"];
        } else {
            $categoryName = "Not available";
        }
    } else {
        $categoryName  = $item['remark'];
    }
    $toatal_amount =  ($data->billitem_taxamount + $data->billitem_amount) - ($data->billitem_discountamount);
    ?>
    <td><?php echo $categoryName ?></td>
    <td><?php echo $data->billitem_hsn_code ?></td>
    <td><?php echo $data->batch ?></td>
    <td><?php echo $data->billitem_quantity ?></td>
    <td><?php echo $data->billitem_unit ?></td>
    <td><?php echo $data->billitem_rate ?></td>
    <td><?php echo $data->purchaseitem_quantity ?></td>
    <td><?php echo $data->purchaseitem_unit ?></td>
    <td><?php echo $data->purchaseitem_rate ?></td>
    <td><?php echo $data->billitem_amount ?></td>
    <td><?php echo $data->billitem_taxslab ?></td>
    <td><?php echo $data->billitem_discountamount ?></td>
    <td><?php echo $data->billitem_discountpercent ?></td>
    <td><?php echo $data->billitem_cgstpercent ?></td>
    <td><?php echo $data->billitem_cgst ?></td>
    <td><?php echo $data->billitem_sgstpercent ?></td>
    <td><?php echo $data->billitem_sgst ?></td>
    <td><?php echo $data->billitem_igstpercent ?></td>
    <td><?php echo $data->billitem_igst ?></td>
    <td><?php echo ($data->billitem_cgst + $data->billitem_sgst + $data->billitem_igst) ?></td>
    <td><?php echo $data->billitem_taxpercent ?></td>
    <td><?php echo $toatal_amount ?></td>
    <td>
        <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="left" type="button" data-html="true" style="cursor: pointer;"></span>
        <div class="popover-content hide">
            <ul class="tooltip-hiden">
                <?php
                if ((in_array('/bills/editbill', Yii::app()->user->menuauthlist))) {
                ?>
                    <li><a id="<?php echo $data->billitem_id; ?>" class="btn btn-xs btn-default edit_additional_item">Edit</a></li>
                <?php } ?>
            </ul>
            <?php echo CHtml::hiddenField('cat_id', $data->category_id, array('id' => "cat_id" . $data->billitem_id)); ?>
        </div>
    </td>
</tbody>

<script>
    $(document).ready(function() {

        $("body").on("click", ".edit_additional_item", function(e) {
            var rowId = $(this).attr("id");
            var cat_id = $("#cat_id" + rowId).val();
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                url: "<?php echo Yii::app()->createUrl("bills/getAdditionalItem") ?>",
                data: {
                    "id": rowId,
                    "cat_id": cat_id
                },
                type: "POST",
                dataType: 'json',
                success: function(data) {

                    if (data !== '') {
                        $('#description').val(data.resultdata.category_id).trigger('change.select2');
                        $('select[id="unit_value"]').empty();
                        if (data.unit_title != '') {
                            $.each(data.unit_title, function(key, value) {
                                $('#base_unit').text(value.base_unit);
                                if (data.resultdata.billitem_unit == value.conversion_unit) {
                                    var selected = 'selected';
                                } else {
                                    var selected = '';
                                }
                                $('select[id="unit_value"]').append('<option value="' + value.id + '" ' + selected + '>' + value.conversion_unit + '</option>');
                            });
                        } else {
                            $('#base_unit').text(data.baseunit);
                            $('select[id="unit_value"]').append('<option value="' + data.baseunit + '">' + data.baseunit + '</option>');
                        }

                        if (data.resultdata.billitem_unit == data.resultdata.purchaseitem_unit) {
                            $(".base").hide();
                        } else {
                            $(".base").show();
                        }

                        $('#hsn_value').text(data.resultdata.billitem_hsn_code);
                        $('#add_remark').val(data.resultdata.billitem_description);
                        $('#add_quantity').val(data.resultdata.billitem_quantity);
                        $('#add_rate').val(data.resultdata.billitem_rate);
                        $('#add_dis_amount').val(data.resultdata.billitem_discountamount);
                        $('#batch').val(data.resultdata.batch);
                        $('#add_dis_perc').text(data.resultdata.billitem_discountpercent);
                        $('#add_tax_slab').val(data.resultdata.billitem_taxslab);
                        $('#add_sgstp').val(data.resultdata.billitem_sgstpercent);
                        $('#add_sgst').text(data.resultdata.billitem_sgst);
                        $('#add_cgstp').val(data.resultdata.billitem_cgstpercent);
                        $('#add_cgst').text(data.resultdata.billitem_cgst);
                        $('#add_igstp').val(data.resultdata.billitem_igstpercent);
                        $('#add_igst').text(data.resultdata.billitem_igst);
                        $('#add_tax_amount').text(data.resultdata.billitem_taxamount);
                        $('#add_tax_perc').text(data.resultdata.billitem_taxpercent);
                        $('#bill_additonal_item_id').val(data.resultdata.billitem_id);
                        var total = parseInt(data.resultdata.billitem_taxamount) + parseInt(data.resultdata.billitem_amount);
                        $('#add_total_amount').text(total);
                        $('#base_quantity').val(data.resultdata.purchaseitem_quantity);
                        $('#base_rate').val(data.resultdata.purchaseitem_rate);
                        $('#base_unit').text(data.resultdata.purchaseitem_unit);
                        $('.popover').hide();
                    }
                    $(".itemadd").show();
                }
            });
        });
    });
    $(document).ajaxComplete(function() {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
</script>