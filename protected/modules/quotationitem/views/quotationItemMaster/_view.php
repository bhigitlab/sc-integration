<?php
/* @var $this QuotationItemMasterController */
/* @var $data QuotationItemMaster */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('quotation_category_id')); ?>:</b>
	<?php echo CHtml::encode($data->quotation_category_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('work_type_id')); ?>:</b>
	<?php echo CHtml::encode($data->work_type_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('worktype_label')); ?>:</b>
	<?php echo CHtml::encode($data->worktype_label); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('work_type_description')); ?>:</b>
	<?php echo CHtml::encode($data->work_type_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image')); ?>:</b>
	<?php echo CHtml::encode($data->image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('material_id')); ?>:</b>
	<?php echo CHtml::encode($data->material_id); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('finish_id')); ?>:</b>
	<?php echo CHtml::encode($data->finish_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rate')); ?>:</b>
	<?php echo CHtml::encode($data->rate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profit')); ?>:</b>
	<?php echo CHtml::encode($data->profit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_on')); ?>:</b>
	<?php echo CHtml::encode($data->created_on); ?>
	<br />

	*/ ?>

</div>