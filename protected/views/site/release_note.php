<?php
/* @var $this SiteController */
/* @var $model  */

$this->breadcrumbs = array(
    'Site' => array('index'),
    'Release Note',
);
?>
<div>
    <div id="releaseModal" class="version-modal">

        <div class="modal-content-release">
            <div class="panel-header">
                <h4 class="panel-title">Release Note</h4>
                <button type="button" class="close closebtn pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
          
                <div class="panel panel-primary">
                    
                    <div class="container-fluid">
                        <div class="panel-body">
                            <?php echo $htmlContent; ?>
                        </div>
                    </div>
                </div>
            
        </div>
    </div>
</div>


<script type="text/javascript">


</script>