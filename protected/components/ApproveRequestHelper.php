<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ApproveRequestHelper {

    public static function createLog($model,$table,$ref_id) {        
        $newmodel = new JpLog('search');
        $newmodel->log_data = json_encode($model);
        $newmodel->log_action = 1;
        $newmodel->log_table = $table;
        $newmodel->log_primary_key = $ref_id; //column name
        $newmodel->log_datetime = date('Y-m-d H:i:s');
        $newmodel->log_action_by = Yii::app()->user->id;
        $newmodel->company_id = $model['company_id'];
        $newmodel->save();
    }
    
    public static function notificationData($pre_id,$userid) {
        $notification = new Notifications;
        $notification->action = "Edit";
        $notification->parent_id = $pre_id; //id column change
        $notification->date = date("Y-m-d");
        $notification->requested_by = $userid;
        $notification->approved_by = Yii::app()->user->id;
        return $notification;
    }

    public static function editRequestData(){
        $finaleditRequestArray = array();

        //Expenses
        $tableName = Yii::app()->Controller->tableNameAcc('pre_expenses',0);
        $command = Yii::app()->db->createCommand()->select("t.*,t.bank_id as bank,"
                . "t.expense_tds as tds_amount,t.exp_id as id ");
        $command->from($tableName . ' as t');
        $command->where = "approval_status ='0'";
        $daybookRequest = $command->queryAll(true);
        
       //Daily Vendors         
        $tableName = Yii::app()->Controller->tableNameAcc('pre_dailyvendors',0);
        $command = Yii::app()->db->createCommand()->select("t.*,t.daily_v_id as id,t.project_id as projectid,t.amount as expense_amount,t.paidamount as paid");
        $command->from($tableName . ' as t');
        $command->where = "approval_status ='0'";
        $dailyvendorRequest = $command->queryAll(true);

        //sub payment
        $tableName = Yii::app()->Controller->tableNameAcc('pre_subcontractor_payment',0);
        $command = Yii::app()->db->createCommand()->select("t.*,t.project_id as projectid,t.payment_id as id,t.amount as expense_amount,t.paidamount as paid");
        $command->from($tableName . ' as t');
        $command->where = "approval_status ='0'";
        $dailyScRequest = $command->queryAll(true);

        //dailyexpense
        $tableName = Yii::app()->Controller->tableNameAcc('pre_dailyexpenses',0);
        $command = Yii::app()->db->createCommand()->select("t.*,t.dailyexp_id as id,t.expense_type as payment_type,t.bank_id as bank,t.dailyexpense_chequeno as cheque_no,t.dailyexpense_amount as expense_amount,t.dailyexpense_paidamount as paid,t.dailyexpense_receipt as receipt");
        $command->from($tableName . ' as t');
        $command->join(Yii::app()->Controller->tableNameAcc('dailyexpense', 0) . ' as de', 'de.dailyexp_id = t.ref_id');
        $command->where = "t.approval_status ='0'";
        $dailyExpRequest = $command->queryAll(true);

        //dailyreport

        $tableName = Yii::app()->Controller->tableNameAcc('pre_dailyreport',0);
        $command = Yii::app()->db->createCommand()->select("t.*,t.dr_id as id,t.expensehead_id as exptype");
        $command->from($tableName . ' as t');
        $command->where = "approval_status ='0'";
        $dailyReportRequest = $command->queryAll(true);                

        foreach ($daybookRequest as $key => $value) {
            $finaleditRequestArray[] = $value;                    
        }

        foreach ($dailyvendorRequest as $key => $value) {
            $finaleditRequestArray[] = $value;           
        }

        foreach ($dailyScRequest as $key => $value) {
            $finaleditRequestArray[] = $value;           
        }

        foreach ($dailyExpRequest as $key => $value) {
            //echo "<pre>";print_r($value);die;
            $finaleditRequestArray[] = $value;           
        }

        foreach ($dailyReportRequest as $key => $value) {
            $finaleditRequestArray[] = $value;           
        }
                   
        return $finaleditRequestArray;
        
    }

    public static function getPaymentType($paymentType){

        $receiptType = NULL;
        $expense_type = NULL;

        if ($paymentType) {
            if ($paymentType == 88) {
                $receiptType = "Cheque";
                $expense_type = "Cheque";
            } else if ($paymentType == 89) {
                $receiptType = "Cash";
                $expense_type = "Cash";
            } else if ($paymentType == 103) {
                $receiptType = "Petty Cash";
                $expense_type = "Petty Cash";
            } else {
                $receiptType = "Credit";
                $expense_type = "Credit";
            }
        }

        return (array('receiptType'=>$receiptType,'expense_type'=>$expense_type));
    }

    public static function getPurchaseType($purchase_type){
        
        if ($purchase_type == 1)
            $purchaseType = "Credit";
        else if ($purchase_type == 2)
            $purchaseType = "Full Paid";
        else if ($purchase_type == 3)
            $purchaseType = "Partially Paid";        

        return $purchaseType;
    }

}
