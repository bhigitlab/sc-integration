<?php

class TaxSlabsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$accessArr = array();
		$accessauthArr = array();
		$accessguestArr = array();
		$controller = Yii::app()->controller->id;

		if (isset(Yii::app()->user->menuauth)) {
			if (array_key_exists($controller, Yii::app()->user->menuauth)) {
				$accessauthArr = Yii::app()->user->menuauth[$controller];
			}
		}


		$access_privlg = count($accessauthArr);
		return array(
			array(
				'allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => $accessauthArr,
				'users' => array('@'),
				'expression' => "$access_privlg > 0",
			),
			array(
				'allow',
				'actions' => array('gettaxslabdetails')
			),
			array(
				'deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new TaxSlabs;
		$this->performAjaxValidation($model);
		$status_values = CHtml::listData(Status::model()->findAll(array('order' => 'caption ASC', 'condition' => 'status_type = "active_status"')), 'sid', 'caption');
		if (isset($_POST['TaxSlabs'])) {
			$model->attributes = $_POST['TaxSlabs'];
			$model->created_by = Yii::app()->user->id;
			$model->updated_by = Yii::app()->user->id;
			$model->created_date =  date('Y-m-d H:i:s');
			$model->updated_date = date('Y-m-d H:i:s');
			if ($model->save())
				$this->redirect(array('admin'));
		}
		if (isset($_GET['layout']))
			$this->layout = false;

		$this->render('create', array(
			'model' => $model,
			'status_values' => $status_values
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$status_values = CHtml::listData(Status::model()->findAll(array('order' => 'caption ASC', 'condition' => 'status_type = "active_status"')), 'sid', 'caption');
		$this->performAjaxValidation($model);

		if (isset($_POST['TaxSlabs'])) {
			$model->attributes = $_POST['TaxSlabs'];
			$model->updated_by = Yii::app()->user->id;
			$model->updated_date = date('Y-m-d H:i:s');
			if ($model->save()) {
				$this->redirect(array('admin'));
			} else {
				echo '<pre>';
				print_r($model->getErrors());
				exit;
			}
		}

		$this->render('update', array(
			'model' => $model,
			'status_values' => $status_values
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider = new CActiveDataProvider('TaxSlabs');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new TaxSlabs('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['TaxSlabs']))
			$model->attributes = $_GET['TaxSlabs'];

		$this->render('admin', array(
			'model' => $model,
			'dataProvider' => $model->search()
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return TaxSlabs the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = TaxSlabs::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param TaxSlabs $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'tax-slabs-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actiongetTaxSlabDetails()
	{
		if (isset($_POST['tax_slab_id'])) {
			$model = $this->loadModel($_POST['tax_slab_id']);
			if (!empty($model)) {
				$return_data = array('cgst' => $model['cgst'], 'sgst' => $model['sgst'], 'igst' => $model['igst']);
				echo json_encode($return_data);
			}
		}
	}
}
