<?php
/* @var $this SubcontractorController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Subcontractors',
);

$this->menu=array(
	array('label'=>'Create Subcontractor', 'url'=>array('create')),
	array('label'=>'Manage Subcontractor', 'url'=>array('admin')),
);
?>

<h1>Subcontractors</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
