<?php
$tblpx = Yii::app()->db->tablePrefix;
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";
$newQuery1 = "";
$newQuery2 = "";
foreach($arrVal as $arr) {
    if ($newQuery) $newQuery .= ' OR';
    if ($newQuery1) $newQuery1 .= ' OR';
    if ($newQuery2) $newQuery2 .= ' OR';
    $newQuery .= " FIND_IN_SET('".$arr."', company_id)";
    $newQuery1 .= " FIND_IN_SET('".$arr."', ".$tblpx."dailyexpense.company_id)";
    $newQuery2 .= " FIND_IN_SET('".$arr."', ".$tblpx."expenses.company_id)";
}
?>
<?php
    $company_address = Company::model()->findBypk(($company_id)?$company_id:Yii::app()->user->company_id);	
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
<h3 class="text-center">Petty Cash</h3>   
<div class="container-fluid pdf_spacing">
            
            <table class="table table-bordered table-striped" border="1">

                <thead>
                    <tr>
                        <th>Sl No.</th>
                        <th>Employee Name</th>
                        <th>Company</th>
                        <th align="right" style="text-align:right">Petty Cash Advance</th>
                        <th align="right" style="text-align:right">Petty Cash Expense</th>
                        <th align="right" style="text-align:right">Petty Cash Refund</th>
                        <th align="right" style="text-align:right">Cash In Hand</th>
                    </tr>
                    <?php
                         $tblpx = Yii::app()->db->tablePrefix;
                         $advance = 0;
                         $expense =0;
                         $balance = 0;
                         $refund = 0;
                         $employee = Yii::app()->db->createCommand("SELECT * FROM ".$tblpx."users WHERE  (".$newQuery.") AND user_type NOT IN (1) ORDER BY userid DESC")->queryAll();
                         foreach($employee as $key=> $value) {
                          
                           $dailyexpense = Yii::app()->db->createCommand("SELECT SUM(".$tblpx."dailyexpense.dailyexpense_paidamount) as amount FROM ".$tblpx."dailyexpense LEFT JOIN ".$tblpx."users ON ".$tblpx."dailyexpense.employee_id= ".$tblpx."users.userid WHERE ".$tblpx."dailyexpense.exp_type =73 AND ".$tblpx."dailyexpense.expense_type=103 AND ".$tblpx."dailyexpense.employee_id = ".$value['userid']." AND (".$newQuery1.")")->queryRow();
                           $dailyexpense_refund = Yii::app()->db->createCommand("SELECT SUM(".$tblpx."dailyexpense.dailyexpense_receipt) as amount FROM ".$tblpx."dailyexpense LEFT JOIN ".$tblpx."users ON ".$tblpx."dailyexpense.employee_id= ".$tblpx."users.userid WHERE ".$tblpx."dailyexpense.exp_type =72 AND ".$tblpx."dailyexpense.dailyexpense_receipt_type=103 AND ".$tblpx."dailyexpense.employee_id = ".$value['userid']." AND (".$newQuery1.")")->queryRow();
       
                           $daybook = Yii::app()->db->createCommand("SELECT SUM(".$tblpx."expenses.paid) as amount FROM ".$tblpx."expenses LEFT JOIN ".$tblpx."users ON ".$tblpx."expenses.employee_id= ".$tblpx."users.userid WHERE ".$tblpx."expenses.type = 73  AND ".$tblpx."expenses.expense_type=103 AND ".$tblpx."expenses.employee_id = ".$value['userid']."  AND (".$newQuery2.")")->queryRow();
                           $advance += $dailyexpense['amount']; 
                           $expense += $daybook['amount'];
                           $balance += $advance-$expense;
                           $refund += $dailyexpense_refund['amount'];
                         }
                    ?>
                    <tr>
                         <td colspan="3" align="right"><b>Total</b></td>
                         <td align="right"><?php  echo Controller::money_format_inr($advance,2); //echo Controller::money_format_inr($totalcredit,0,".",""); ?></td>
                         <td align="right"><?php  echo Controller::money_format_inr($expense,2); //echo Controller::money_format_inr($totaldebit,0,".",""); ?></td>
                         <td align="right"><?php  echo Controller::money_format_inr($refund,2); //echo Controller::money_format_inr($totaldebit,0,".",""); ?></td>
                         <td align="right"><?php  echo Controller::money_format_inr($advance-($expense+$refund),2);//echo Controller::money_format_inr($totalremaining,0,".",""); ?></td>
                     </tr>
                </thead>
                         <tbody>
                <?php 
                  $tblpx = Yii::app()->db->tablePrefix;
                  $advance = 0;
                  $expense =0;
                  $balance = 0;
                  $refund = 0;
                  $employee = Yii::app()->db->createCommand("SELECT * FROM ".$tblpx."users WHERE  (".$newQuery.") AND user_type NOT IN (1) ORDER BY userid DESC")->queryAll();
                  foreach($employee as $key=> $value) {
                   
                    $dailyexpense = Yii::app()->db->createCommand("SELECT SUM(".$tblpx."dailyexpense.dailyexpense_paidamount) as amount FROM ".$tblpx."dailyexpense LEFT JOIN ".$tblpx."users ON ".$tblpx."dailyexpense.employee_id= ".$tblpx."users.userid WHERE ".$tblpx."dailyexpense.exp_type =73 AND ".$tblpx."dailyexpense.expense_type=103 AND ".$tblpx."dailyexpense.employee_id = ".$value['userid']." AND (".$newQuery1.")")->queryRow();
                    $dailyexpense_refund = Yii::app()->db->createCommand("SELECT SUM(".$tblpx."dailyexpense.dailyexpense_receipt) as amount FROM ".$tblpx."dailyexpense LEFT JOIN ".$tblpx."users ON ".$tblpx."dailyexpense.employee_id= ".$tblpx."users.userid WHERE ".$tblpx."dailyexpense.exp_type =72 AND ".$tblpx."dailyexpense.dailyexpense_receipt_type=103 AND ".$tblpx."dailyexpense.employee_id = ".$value['userid']." AND (".$newQuery1.")")->queryRow();

                    $daybook = Yii::app()->db->createCommand("SELECT SUM(".$tblpx."expenses.paid) as amount FROM ".$tblpx."expenses LEFT JOIN ".$tblpx."users ON ".$tblpx."expenses.employee_id= ".$tblpx."users.userid WHERE ".$tblpx."expenses.type = 73  AND ".$tblpx."expenses.expense_type=103 AND ".$tblpx."expenses.employee_id = ".$value['userid']."  AND (".$newQuery2.")")->queryRow();
                    $advance += $dailyexpense['amount']; 
                    $expense += $daybook['amount'];
                    $balance += $advance-$expense;
                    $refund += $dailyexpense_refund['amount'];
                ?>
                <tr>
                    <td><?php echo $key+1; ?></td>
                    <td><?php echo $value['first_name'].' '.$value['last_name']; ?></td>
                    <td><?php 
                        $companyname = array();
                        $user = Users::model()->findByPk(Yii::app()->user->id);
                        $comVal = explode(',', $user->company_id);
                        $arrVal = explode(',', $value['company_id']);
                        foreach($arrVal as $arr) {
                           if(in_array($arr, $comVal)){ 
                            $value = Company::model()->findByPk($arr);
                            if($value){
                                array_push($companyname, $value->name);
                            }
                           }
                        }
                        echo implode(', ', $companyname);
                    ?></td>
                    <td align="right"><?php echo Controller::money_format_inr($dailyexpense['amount'],2) ; ?></td>
                    <td align="right"><?php echo Controller::money_format_inr($daybook['amount'],2) ; ?></td>
                    <td align="right"><?php echo Controller::money_format_inr($dailyexpense_refund['amount'],2) ; ?></td>
                    <td align="right"><?php echo Controller::money_format_inr($dailyexpense['amount']-($daybook['amount']+$dailyexpense_refund['amount']),2) ; ?></td>
                </tr>
                
                    <?php } ?>
<!--             
-->                    
 <!-- <tr>
                         <td colspan="3" align="right"><b>Total</b></td>
                         <td align="right"><?php  echo Controller::money_format_inr($advance,2); //echo Controller::money_format_inr($totalcredit,0,".",""); ?></td>
                         <td align="right"><?php  echo Controller::money_format_inr($expense,2); //echo Controller::money_format_inr($totaldebit,0,".",""); ?></td>
                         <td align="right"><?php  echo Controller::money_format_inr($refund,2); //echo Controller::money_format_inr($totaldebit,0,".",""); ?></td>
                         <td align="right"><?php  echo Controller::money_format_inr($advance-($expense+$refund),2);//echo Controller::money_format_inr($totalremaining,0,".",""); ?></td>
                     </tr> -->

                 </tbody>

            </table>

        </div>

