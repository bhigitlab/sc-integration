ALTER TABLE `jp_dailyexpense` ADD `petty_payment_approval` ENUM('0','1') NOT NULL DEFAULT '1' COMMENT '0=>Not Approved , 1=>Approved' AFTER `duplicate_delete_status`;

ALTER TABLE `jp_expenses` ADD `petty_payment_approval` ENUM('0','1') NOT NULL DEFAULT '1' COMMENT '0=>Not Approved , 1=>Approved' AFTER `duplicate_delete_status`;

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES (783, 'Warehouse Receipt Update', '706', '0', 'wh/warehousereceipt', 'update', '', '2', '1', NULL);

INSERT INTO `jp_menu` (`menu_id`, `menu_name`, `parent_id`, `status`, `controller`, `action`, `params`, `showmenu`, `show_list`, `related`) VALUES (784, 'Ageing Report', '561', '0', 'bills', 'ageingreport', '', '2', '1', NULL);
UPDATE `jp_dailyexpense` SET `exp_type_id` = '0' WHERE `jp_dailyexpense`.`exp_type_id` IS NULL;