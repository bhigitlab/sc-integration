<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?>

<div style="float:left;"><h1 style="text-align: left; margin-left:20px;">BHI-PMS: Dashboard</i></h1></div>


<br clear="all"/>


<div id="dashboard">
 
<?php if (Yii::app()->user->role <= 2) { ?>
<?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/projects.png', '', array('class' => 'dasimage')) . '<span class="dblink">Projects</span>
  </div>', array('/projects/index')); ?>       
 <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/clients.png', '', array('class' => 'dasimage')) . '<span class="dblink">Clients</span>
  </div>', array('/clients/index')); ?> 
<?php
}
?>
    
                <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/users.png', '', array('class' => 'dasimage')) . '<span class="dblink">Users</span>
  </div>', array('/users/index')); ?> 
    
    <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/myprofile.png', '', array('class' => 'dasimage')) . '<div><span class="dblink">My Profile</span></div>
  </div>', array('/users/myprofile')); ?>

    <?php echo CHtml::link('<div class="dicons">' . CHtml::image('images/icons/logout.png', '', array('class' => 'dasimage')) . '<span class="dblink">Logout</span>
  </div>', array('site/logout')); ?>
    <br clear="all">
</div>