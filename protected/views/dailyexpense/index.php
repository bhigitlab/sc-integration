<?php
/* @var $this DailyexpenseController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Dailyexpenses',
);

$this->menu=array(
	array('label'=>'Create Dailyexpense', 'url'=>array('create')),
	array('label'=>'Manage Dailyexpense', 'url'=>array('admin')),
);
?>

<h1>Dailyexpenses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
