<?php

class WithdrawalsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','deletewithdrawal'),
				'users'=>array('@'),
				'expression' => 'yii::app()->user->role<=2',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin','create','update','ajaxcreatetype','ajaxcreatedetail','returncalc','ajaxeditstatus'),
				'users'=>array('@'),
				'expression' => 'yii::app()->user->role<=2',
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete','settings'),
				'users'=>array('@'),
				'expression' => 'yii::app()->user->role<=2',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
            $model=new Withdrawals;

            // Uncomment the following line if AJAX validation is needed
             $this->performAjaxValidation($model);

            if(isset($_POST['Withdrawals']))
            {
                $model->attributes=$_POST['Withdrawals'];
                $model->created_by = yii::app()->user->id;
                $model->created_date = date('Y-m-d H:i:s');

                $model->updated_by = yii::app()->user->id;
                $model->updated_date = date('Y-m-d');
                $model->date= date('Y-m-d',strtotime($_POST['Withdrawals']['date']));
                if($model->validate() && $model->save()){
                        $this->redirect(array('admin'));
                }
            }

 

            $this->render('create',array(
                    'model'=>$model,
            ));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['Withdrawals']))
		{
			$model->attributes=$_POST['Withdrawals'];
			$model->updated_by = yii::app()->user->id;
			$model->updated_date = date('Y-m-d');
			 $model->date= date('Y-m-d',strtotime($_POST['Withdrawals']['date']));
			if($model->validate() &&  $model->save()){
            $this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
//	public function actionDelete($id)
//	{
//		$this->loadModel($id)->delete();
//
//		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
//		if(!isset($_GET['ajax']))
//			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
//	}
//	
//	 public function actiondeletewithdrawal($id) {
//		//echo $id;die();
//
//	  $query=Yii::app()->db->createCommand('DELETE FROM withdrawals WHERE exp_id='.$id)->execute();
//	  $this->redirect(array('admin'));
//		
//	}
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Withdrawals');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Withdrawals('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Withdrawals']))
			$model->attributes=$_GET['Withdrawals'];

		$this->render('newlist',array(
			'model'=>$model,
			 'dataProvider' =>  $model->search(),
		));
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Withdrawals::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='withdrawals-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        
        
        
        public function actionSettings(){
            
            $typemodel = new Status();
            $detailsmodel = new Status();
            
            $types = Status::model()->findAll(array(
                                        'select' => array('sid,caption'),
                                        'condition' => 'status_type="withdrawal"',
                                        'order' => 'sid',
                                        'distinct' => true
                                    ));
            $details = Status::model()->findAll(array(
                                        'select' => array('sid,caption'),
                                        'condition' => 'status_type="withdrawal_details"',
                                        'order' => 'sid',
                                        'distinct' => true
                                    ));

            
            $this->render('settings',array(
                'typemodel' => $typemodel, 
                'detailsmodel' => $detailsmodel,
                'types' => $types,
                'details' => $details
                    
            ));
            
        }
        
        //Added by Kripa on 14-10-2016
        public function actionAjaxCreatetype(){
           
            $type = $_REQUEST['type'];
            
            if(!empty($type)){
                
                $typemodel = new Status();
                $typemodel->caption = $type;
                $typemodel->status_type = "withdrawal";
                if($typemodel->save()){
                    echo json_encode(array('status' => 'success'));
                }
                else{
                    echo json_encode(array('status' => 'error'));
                } 
                
            }

        }
        
        public function actionAjaxCreatedetail(){
           
            $detail = $_REQUEST['detail'];
            
            if(!empty($detail)){
                
                $detailmodel = new Status();
                $detailmodel->caption = $detail;
                $detailmodel->status_type = "withdrawal_details";
                if($detailmodel->save()){
                    echo json_encode(array('status' => 'success'));
                }
                else{
                    echo json_encode(array('status' => 'error'));
                } 
                
            }

        }
        
        public function actionReturncalc($user_sel = 0,$date = 0,$amount=0,$type=0,$detail =0, $project=0){
            //
			$tblpx = Yii::app()->db->tablePrefix;
            $data_sql = '';
            $joinsql = '';

      	 if(Yii::app()->user->role ==1){
			$data_sql = 'where userid IS NOT NULL';
		 }else{
			$data_sql = 'where created_by = '.Yii::app()->user->id;
		 }

            //if($user_sel != 0){$data_sql = ' where userid = '.$user_sel;}else{$data_sql = ' where userid IS NOT NULL';}

            if($date != 0){$data_sql = $data_sql.' and date = '.$date;}

            if($amount != 0){$data_sql = $data_sql.' and amount = '.$date;}

            if($type != 0){$data_sql = $data_sql.' and type = '.$type; $joinsql = $joinsql.' LEFT JOIN '.$tblpx.'status st1 ON '.$tblpx.'withdrawals.type = st1.sid';}
            if($detail != 0){$data_sql = $data_sql.' and details = '.$detail; $joinsql = $joinsql.' LEFT JOIN '.$tblpx.'status st2 ON '.$tblpx.'withdrawals.details = st2.sid';}

            if($project != 0){$data_sql = $data_sql.' and projectid = '.$project;}
            


            $dataArray = Yii::app()->db->createCommand("select * from '.$tblpx.'withdrawals $joinsql $data_sql ")
            ->queryAll();

            //
           
            $total = 0;
            foreach($dataArray as $data){
               $total += $data['amount'];
            }
            //
            $data = '<b>Total: Rs.'.$total.'/-</b>';
            //
            echo $data;
            //
        }
        
        //added by Kripa on 17-10-2016
        public function actionAjaxEditstatus(){
			$tblpx = Yii::app()->db->tablePrefix;
            $sid = $_REQUEST['sid'];
            $caption = $_REQUEST['caption'];
            
            $update = Yii::app()->db->createCommand("Update ".$tblpx."status SET caption = '$caption' WHERE sid = $sid")->execute();
            if($update){
                echo json_encode(array('status' => 'success'));
            }
            else{
                echo json_encode(array('status' => 'error'));
            } 
        }
        
        

        
        //added by Kripa on 17-10-2016
}
