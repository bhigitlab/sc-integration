<?php
/* @var $this ProjectsController */
/* @var $data Projects */
?>
<?php if ($index == 0) { ?>
    <thead class="entry-table sticky-thead">
        <tr>
            <th>Sl No.</th>
            <th>Project Name</th>
            <th>Quotation Amount</th>
            <th>Receipt Amount</th>
            <th>Balance Amount</th>
        </tr>
    </thead>
<?php } ?>
<?php
$quotation_amount = $data->quotationAmount($data->pid);
$receipt_amount = $data->receiptAmount($data->pid);
?>
<tr>
    <td><?php echo $index + 1; ?></td>
    <td><?php echo $data->name; ?></td>
    <td class="text-right"><?php echo  Yii::app()->controller->money_format_inr($quotation_amount, 2); ?></td>
    <td class="text-right"><?php echo  Yii::app()->controller->money_format_inr($receipt_amount, 2); ?></td>
    <td class="text-right"> <a href='<?php echo Yii::app()->createAbsoluteUrl("projects/paymentReport", array("project_id" => $data->pid)); ?>'><?php echo Yii::app()->controller->money_format_inr(($quotation_amount - $receipt_amount), 2) ?></a></td>
</tr>