<?php
$tblpx = Yii::app()->db->tablePrefix;
$second_unit ='';
$second_quantity ='';
$coversion_factor =0;
$priority =[];
$base_unit = $data->GetItemunit($data->warehousestock_itemid);
if($base_unit != null){
  $basewarehouse_unit = $base_unit;
}else{
  $basewarehouse_unit = $data->GetwarehouseItemunitid($data['warehousestock_unit']);
}
$priority = $data->checkpriority($data->warehousestock_itemid,$basewarehouse_unit);
if(count($priority) ==0){
  $unit = $data['warehousestock_unit'];
  $quantity = $data['warehousestock_stock_quantity'];
}else{
  if($base_unit != $priority['conversion_unit']){
      $second_unit = $data->GetItemunitname($priority['conversion_unit']);
      $coversion_factor = $priority['conversion_factor'];
      $second_quantity = $data['warehousestock_stock_quantity']*$coversion_factor;
      $unit = $data['warehousestock_unit']."(".$second_unit.")";
      $quantity = $data['warehousestock_stock_quantity']."(".$second_quantity.")";
  }else{
    $unit =$data['warehousestock_unit'];
    $quantity = $data['warehousestock_stock_quantity'];
  }
}

if($index == 0) {

?>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Warehouse</th>
                            <th>Item</th>
                            <th>Batch</th>
                            <th>Unit</th>
                            <th>Date</th>
                            <th>Quantity</th>
                           
                        </tr> 
                    </thead>
<?php } 
if($quantity !=0){

?>
                    <tr id="<?php echo $index; ?>" class="">
                        <td><?php echo $index+1; ?></td>
                        <td><?php echo $data->warehouse['warehouse_name']; ?></td>
                        <td><?php
                        $tblpx = Yii::app()->db->tablePrefix;
                        $desctiptions  = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}purchase_category WHERE id=".$data->warehousestock_itemid."")->queryRow();
                        $parent_category = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}purchase_category WHERE id='".$desctiptions['parent_id']."'")->queryRow();
                        if($desctiptions['brand_id'] != NULL){
                        $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=".$desctiptions['brand_id']."")->queryRow();
                        $brand= '-'.$brand_details['brand_name'];
                        } else {
                        $brand='';
                        }
                        $spc_details = $parent_category['category_name'].$brand.' - '.$desctiptions['specification'];
                        echo $spc_details;
                        ?></td>
                        <td><?php echo $data['batch']; ?></td>
                        <td><?php echo $unit; ?></td>
                        <td><?php echo date('d-m-Y',  strtotime($data['warehousestock_date'])); ?></td>
                        <td><?php echo $quantity; ?>
                            <div class="warehouseid" id="<?php echo $data['warehousestock_warehouseid']; ?>"></div>
                            <div class="itemid" id="<?php echo $data['warehousestock_itemid']; ?>"></div>
                        </td>
                        <?php /*<td>
                          <?php
                        if(Yii::app()->user->role ==1){
                          $display_class = '';
                        }else{
                          $query = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}warehouse WHERE warehouse_id=".$data['warehousestock_warehouseid']." AND FIND_IN_SET('".Yii::app()->user->id."', assigned_to)")->queryRow();
                          if(!empty($query)){
                            $display_class = '';
                          }else{
                            $display_class = 'display:none';
                          }
                        }
                        // echo Yii::app()->user->id.'/';
                        // echo $data['warehousestock_warehouseid'];
                        // $query = Yii::app()->db->createCommand("SELECT * FROM `sb_warehouse` WHERE warehouse_id=".$data['warehousestock_warehouseid']." AND FIND_IN_SET('".Yii::app()->user->id."', assigned_to)")->queryRow();
                        // print_r($query);
                        ?>
                          <a style="<?php echo $display_class; ?>" data-id="<?php echo $data->warehousestock_id; ?>" data-id="<?php echo $data->warehousestock_id; ?>" data-target=".edit" class="fa fa-edit edit_item" style="cursor:pointer;"></a>
                          <a style="<?php echo $display_class; ?>" data-id="<?php echo $data->warehousestock_id; ?>" data-id="<?php echo $data->warehousestock_id; ?>" data-target=".delete" class="fa fa-trash deletestock" style="cursor:pointer;"></a>
                        </td> */?>

                    </tr>
<?php } ?>