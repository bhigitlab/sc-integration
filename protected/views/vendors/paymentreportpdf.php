<?php
    $company_address = Company::model()->findBypk(($company_id)?$company_id:Yii::app()->user->company_id);	
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">

<h3 class="text-center">Vendor Payments</h3>
<div class="container-fluid pdf_spacing">
    <table class="table table-bordered table-striped" border="1">
        <thead>
            <tr>
                <th>Sl No.</th>
                <th>Vendor Name</th>
                <th align="right" style="text-align:right">Invoice</th>
                <th align="right" style="text-align:right">Payments</th>
                <th align="right" style="text-align:right">Balance</th>
            </tr>
        </thead>
        <tbody>
        <tr>
                <?php
                $sumTotal   = $paymentSum["daybooksum"] + $paymentSum["vendorsum"];
                $balanceSum = $paymentSum["billsum"] - $sumTotal;
                ?>
                <td colspan="2" align="right"><b>Total</b></td>
                <td align="right"><?php echo Controller::money_format_inr($paymentSum["billsum"], 2); //echo Controller::money_format_inr($totalcredit,0,".",""); ?></td>
                <td align="right"><?php echo Controller::money_format_inr($sumTotal, 2); //echo Controller::money_format_inr($totaldebit,0,".",""); ?></td>
                <td align="right"><?php echo Controller::money_format_inr($balanceSum, 2); //echo Controller::money_format_inr($totalremaining,0,".",""); ?></td>
            </tr>
            <?php
            $i = 1;
            foreach($paymentData as $paymentList) {
                $totalPayment   = $paymentList["daybookpaid"] + $paymentList["vendorpaid"];
                $balancePaid    = $paymentList["billtotal"] - $totalPayment;
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $paymentList['name']; ?></td>
                    <td align="right"><?php echo Controller::money_format_inr($paymentList['billtotal'], 2); ?></td>
                    <td align="right"><?php echo Controller::money_format_inr($totalPayment, 2); ?></td>
                    <td align="right"><?php echo Controller::money_format_inr($balancePaid, 2); ?></td>
                </tr>
            <?php
                $i = $i + 1;
            }
            ?>
           
        </tbody>
    </table>
</div>

