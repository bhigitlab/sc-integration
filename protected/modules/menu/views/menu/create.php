<?php
/* @var $this MenuController */
/* @var $model Menu */

$this->breadcrumbs=array(
	'Menus'=>array('index'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List Menu', 'url'=>array('index')),
	//array('label'=>'Manage Menu', 'url'=>array('admin')),
);
?>

<div class="container">
<h2>Create Menu</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>

