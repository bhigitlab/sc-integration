<link rel="stylesheet" type="text/css" href="uploads/new_template/css/newstyle.css" />

<div class="container">
	<style>
		.lefttdiv {
			float: left;
		}

		.purchase-title {
			border-bottom: 1px solid #ddd;
		}

		.purchase_items h3 {
			font-size: 18px;
			color: #333;
			margin: 0px;
			padding: 0px;
			text-align: left;
			padding-bottom: 10px;
		}

		.purchase_items {
			padding: 15px;
			box-shadow: 0 0 13px 1px rgba(0, 0, 0, 0.25);
		}

		.purchaseitem {
			display: inline-block;
			margin-right: 20px;
		}

		.purchaseitem last-child {
			margin-right: 0px;
		}

		.purchaseitem label {
			font-size: 12px;
		}

		.remark {
			display: none;
		}

		.padding-box {
			padding: 3px 0px;
			min-height: 17px;
			display: inline-block;
		}

		th {
			height: auto;
		}

		.quantity,
		.rate {
			max-width: 80px;
		}

		.text_align {

			text-align: center;
		}


		*:focus {
			border: 1px solid #333;
			box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
		}

		.text-right {
			text-align: right;
		}
	</style>
	<header class="headerinv">
		<table class="invoiceheader">
			<tr>
				<td><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" style="height:100px;padding:5px;"></td>
				<?php
				$company_address = Company::model()->findBypk(Yii::app()->user->company_id);
				?>
				<td class="details" style="width:300px;"><b><?php echo ((isset($company_address['address']) && !empty($company_address['address'])) ? nl2br($company_address['address']) : ""); ?> </b></td>
			</tr>
		</table>
		<br>
		<header>
			<?php
			$company = Company::model()->findByPk($model->company_id);
			?>
			<h2>Bills</h2>
			<br />

			<table border="0">
				<tr>
					<td><label>COMPANY : </label><span><?php echo $company['name']; ?></span></td>
					<td><label>PROJECT : </label><span><?php echo $project->name; ?></span></td>
				</tr>
				<tr>
					<td><label>EXPENSE HEAD : </label><span><?php echo (isset($expense_head->type_name)) ? $expense_head->type_name : ''; ?></span></td>
					<td><label>VENDOR : </label><span><?php echo (isset($vendor->name)) ? $vendor->name : ''; ?></span></td>
				</tr>

				<tr>
					<td><label>DATE : </label><span><?php echo date("d-m-Y", strtotime($model->bill_date)); ?></span></td>
					<td><label>BILL NO : </label><span><?php echo $model->bill_number; ?></span></td>
				</tr>

			</table>



			<br /><br />

			<div class="table-responsive">
				<table border="1" class="table">
					<thead>
						<tr>
							<th>Sl.No</th>
							<th class="text_align">Description</th>
							<th class="text_align">HSN Code</th>
							<th class="text_align">Quantity</th>
							<th class="text_align">Unit</th>
							<th class="text_align">Rate</th>
							<th class="text_align">Tax Slab (%)</th>
							<th class="text_align">SGST Amount</th>
							<th class="text_align">SGST (%)</th>
							<th class="text_align">CGST Amount</th>
							<th class="text_align">CGST (%)</th>
							<th class="text_align">IGST Amount</th>
							<th class="text_align">IGST (%)</th>
							<th class="text_align">Discount (%)</th>
							<th class="text_align">Discount Amount</th>
							<th class="text_align">Amount</th>
							<th class="text_align">Tax Amount</th>
						</tr>
					</thead>
					<tbody class="addrow">
						<?php
						$tblpx = Yii::app()->db->tablePrefix;
						$data1 = Yii::app()->db->createCommand("SELECT SUM(billitem_amount) as bill_amount FROM {$tblpx}billitem WHERE bill_id=" . $model->bill_id . "")->queryRow();
						$data2 = Yii::app()->db->createCommand("SELECT SUM(billitem_taxamount) as bill_taxamount FROM {$tblpx}billitem WHERE bill_id=" . $model->bill_id . "")->queryRow();
						$data3 = Yii::app()->db->createCommand("SELECT SUM(billitem_discountamount) as bill_discountamount FROM {$tblpx}billitem WHERE bill_id=" . $model->bill_id . "")->queryRow();
						$addcharges = Yii::app()->db->createCommand("SELECT SUM(amount) as amount FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $model->bill_id)->queryRow();
						$grand_total = ($data1['bill_amount'] + $data2['bill_taxamount'] + $addcharges['amount']) - $data3['bill_discountamount'];
						?>
						<?php
						if (!empty($item_model)) {
							foreach ($item_model as $key => $values) {
								$tblpx		= Yii::app()->db->tablePrefix;
								if ($values['category_id'] == '0') {
									if ($values['remark'] != '') {
										$specification = $values['remark'];
									} else {
										$specification = $values['billitem_description'];
									}
								} else {

									$data    = Yii::app()->db->createCommand("SELECT specification, brand_id, parent_id FROM {$tblpx}purchase_category WHERE id=" . $values['category_id'] . "")->queryRow();

									if ($data['brand_id'] != NULL) {
										$brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $data['brand_id'] . "")->queryRow();
										$brand = '-' . ucwords($brand_details['brand_name']);
									} else {
										$brand = '';
									}

									$parent_category = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}purchase_category WHERE id='" . $data['parent_id'] . "'")->queryRow();

									$specification = ucwords($parent_category['category_name']) . $brand . '-' . ucwords($data['specification']);
								}
						?>

								<tr>
									<td>
										<div id="item_sl_no"><?php echo ($key + 1); ?></div>
									</td>
									<td>
										<div class="item_description"><?php echo $specification; ?></div>
									</td>
									<td class="text-right">
										<div class="" id="unit"> <?php echo $values['billitem_hsn_code']; ?></div>
									</td>
									<td class="text-right">
										<div class="" id="unit"> <?php echo $values['billitem_quantity']; ?></div>
									</td>
									<td>
										<div class="unit" id="unit"> <?php echo $values['billitem_unit']; ?></div>
									</td>
									<td class="text-right">
										<div class="" id="rate"><?php echo Controller::money_format_inr($values['billitem_rate'], 2); ?></div>
									</td>
									<td class="text-right">
										<div class="" id="tax_slab"><?php echo $values['billitem_taxslab']; ?></div>
									</td>
									<td class="text-right"><?php echo Controller::money_format_inr($values['billitem_sgst'], 2); ?></td>
									<td class="text-right"><?php echo Controller::money_format_inr($values['billitem_sgstpercent'], 2); ?></td>
									<td class="text-right"><?php echo Controller::money_format_inr($values['billitem_cgst'], 2); ?></td>
									<td class="text-right"><?php echo Controller::money_format_inr($values['billitem_cgstpercent'], 2); ?></td>
									<td class="text-right"><?php echo Controller::money_format_inr($values['billitem_igst'], 2); ?></td>
									<td class="text-right"><?php echo Controller::money_format_inr($values['billitem_igstpercent'], 2); ?></td>
									<td class="text-right">
										<div class="" id="rate"><?php echo Controller::money_format_inr($values['billitem_discountpercent'], 2); ?></div>
									</td>
									<td class="text-right">
										<div class="" id="rate"><?php echo Controller::money_format_inr($values['billitem_discountamount'], 2); ?></div>
									</td>
									<td class="text-right"><?php echo Controller::money_format_inr($values['billitem_amount'], 2); ?></td>
									<td class="text-right"><?php echo Controller::money_format_inr($values['billitem_taxamount'], 2); ?></td>
								</tr>
						<?php
							}
						}
						?>


					</tbody>
					<tfoot>
						<tr>
							<th colspan="14" class="text-right">TOTAL: </th>
							<th class="text-right">
								<div id="discount_total"><?php echo ($data3['bill_discountamount'] != '') ? number_format($data3['bill_discountamount'], 2) : '0.00'; ?></div>
							</th>
							<th class="text-right">
								<div id="amount_total"><?php echo ($data1['bill_amount'] != '') ? number_format($data1['bill_amount'], 2) : '0.00'; ?>
							</th>
							<th class="text-right">
								<div id="tax_total"><?php echo ($data2['bill_taxamount'] != '') ? number_format($data2['bill_taxamount'], 2) : '0.00'; ?>
							</th>
						</tr>

						<tr>
							<th colspan="15" class="text-right">GRAND TOTAL: <div id="grand_total"><?php echo ($grand_total != '') ? number_format($grand_total, 2) : '0.00'; ?></div>
							</th>
							<th colspan="2">ADDITIONAL CHARGE: <?php echo ($addcharges['amount'] != '') ? $addcharges['amount'] : 0; ?></th>
							<!-- <th colspan="2"></th> -->
						</tr>
					</tfoot>
				</table>
			</div>

			<br /><br />




			<h4>Authorized Signatory,</h4>
			<h4></h4>
			<br /><br />
			<!--<footer class="inv_footer"></footer>-->
</div>