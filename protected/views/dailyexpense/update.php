<?php
/* @var $this DailyexpenseController */
/* @var $model Dailyexpense */

$this->breadcrumbs=array(
	'Dailyexpenses'=>array('index'),
	$model->dailyexp_id=>array('view','id'=>$model->dailyexp_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Dailyexpense', 'url'=>array('index')),
	array('label'=>'Create Dailyexpense', 'url'=>array('create')),
	array('label'=>'View Dailyexpense', 'url'=>array('view', 'id'=>$model->dailyexp_id)),
	array('label'=>'Manage Dailyexpense', 'url'=>array('admin')),
);
?>

<h1>Update Dailyexpense <?php echo $model->dailyexp_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>