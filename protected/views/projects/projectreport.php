<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
<script type="text/javascript"
    src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<?php

$this->breadcrumbs = array(
    'Purchase List',
)
    ?>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<div class="container" id="project">
    <?php
    $main_heading = "Work Status Report";
    $company_address = Company::model()->find(array('order' => ' id ASC'));
    $spacing = '';
    if (!filter_input(INPUT_GET, 'export')) {
        ?>
        <div class="header-container">
            <h3>
                <?php echo $main_heading; ?>
            </h3>
            <div class="btn-container">
                <?php
                $url_data = '';
                if (isset($_GET['expensehead_id'])) {
                    $url_data .= '&expense_head=' . $_GET['expensehead_id'];
                }
                if (isset($_GET['subcontractor_id'])) {
                    $url_data .= '&sub_contractor=' . $_GET['subcontractor_id'];
                }
                if (isset($_GET['datefrom'])) {
                    $url_data .= '&datefrom=' . $_GET['datefrom'];
                }
                if (isset($_GET['date_to'])) {
                    $url_data .= '&date_to=' . $_GET['date_to'];
                }
                ?>
                <!-- Excel Button -->
                <?php echo CHtml::link(
                    '<i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i> ',
                    Yii::app()->request->requestUri . "&export=csv" . $url_data,
                    array('class' => 'btn btn-info', 'style' => 'cursor:pointer;')
                ); ?>
                <!-- PDF Button -->
                <?php echo CHtml::link(
                    '<i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>',
                    Yii::app()->request->requestUri . "&export=pdf" . $url_data,
                    array('class' => 'btn btn-info ', 'style' => 'cursor:pointer;')
                ); ?>


            </div>
        </div>

        <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>


        <?php
        $this->renderPartial('_newsearch2', array('model' => $model, 'subcontractor' => $subcontractor, 'expense' => $expense, 'dateto' => $dateto, 'expensehead_id' => $expensehead_id, 'subcontractor_id' => $subcontractor_id)) ?>

        <?php if (Yii::app()->user->hasFlash('success')): ?>
            <div class="info"
                style="width: 500px;margin-left: 400px;padding: 8px;color: green;font-size: 20px;font-weight: bold; padding-top: 46px;">
                <?php echo Yii::app()->user->getFlash('success'); ?>
            </div>
        <?php endif; ?>


        <?php if (Yii::app()->user->hasFlash('error')): ?>
            <div class="info"
                style="width: 500px;margin-left: 400px;padding: 8px;color: red;font-size: 20px;font-weight: bold; padding-top: 46px;">
                <?php echo Yii::app()->user->getFlash('error'); ?>
            </div>
        <?php endif; ?>
    <?php } elseif (filter_input(INPUT_GET, 'export') === "pdf") {
        $spacing = 'style="margin:0px 30px"';
        ?>
        <style>
            table {
                border-collapse: collapse;
                width: 100%;
                font-family: Arial, Helvetica, sans-serif;

            }

            .details,
            .info-table {
                border: 1px solid #ccc;
                padding: 2rem;
            }

            .details td,
            .details th,
            .info-table td,
            .info-table th {
                padding: 5px 10px;
                font-size: .85rem;
            }

            .info-table {
                margin-bottom: 10px;
            }

            .text-center {
                text-align: center;
            }

            .img-hold {
                width: 10%;
            }

            .companyhead {
                font-size: 1rem;
                font-weight: bold;
                margin-top: 0px;
                color: #789AD1;
            }

            .table-header th,
            .details {

                font-size: .75rem;
                padding-left: 4px;
                padding-right: 4px;
            }

            .medium-font {
                font-size: .85rem;
            }

            .addrow th,
            .addrow td {
                font-size: .75rem;
                border: 1px solid #333;
                border-collapse: collapse;
                padding-left: 4px;
                padding-right: 4px;
            }

            .text-right {
                text-align: right;
            }

            .w-50 {
                width: 50%;
            }

            .text-sm {
                font-size: 11px;
            }

            .dotted-border-bottom {
                border-bottom: 2px dotted black;
            }

            .w-100 {
                width: 100%;
            }

            .border-right {
                border-right: 1px solid #ccc;
            }

            .border-0 {
                border-right: 1px solid #fff !important;
            }

            .header-border {
                border-bottom: 1px solid #000;
            }
        </style>
        <br>
        <h3 class="text-center mt-1">
            <?php echo $main_heading; ?>
        </h3>
        <?php
    }
    ?>
    <!--html code here -->
    <div class="text-right" <?php echo $spacing ?>>
        <span class="helper_box"></span><span>Helper details</span>
    </div>
    <div class="table-responsive">
        <table class="weekly_tbl" <?php echo $spacing ?>>
            <thead class="entry-table">
                <tr>
                    <th>Project</th>
                    <th>Expense Type</th>
                    <th>Date</th>
                    <th>No of Labour</th>
                    <th>Corresponding Wage</th>
                    <th>Wage Rate</th>
                    <th>Lumpsum</th>
                    <th>Amount</th>
                    <?php
                    if (filter_input(INPUT_GET, 'export') != 'csv') {
                        ?>
                        <th>Total</th>
                        <?php
                    }
                    ?>

                </tr>
            </thead>
            <tbody>
                <?php
                if (!empty($final_array)) {
                    // echo '<pre>';
                    // print_r($final_array);
                    // exit;
                    foreach ($final_array as $key => $data) {
                        $i = 1;
                        foreach ($data['payment'] as $dat) {
                            if (!empty($dat['items'][0]) && !empty($dat['items'][3])) {

                                $date_count = '1';
                            } else {
                                $date_count = 0;
                            }
                            ?>
                            <tr>
                                <?php
                                if ($i == 1) {
                                    ?>
                                    <td rowspan="<?php echo $data['row_count']; ?>">
                                        <?php echo $data['project'] ?>
                                    </td>
                                    <td rowspan="<?php echo $data['row_count']; ?>">
                                        <?php echo $data['expense_head_title'] ?>
                                    </td>
                                    <?php
                                }
                                ?>

                                <?php
                                if (!empty($dat['items'][0]) && !empty($dat['items'][3])) {
                                    ?>
                                    <td rowspan="2">
                                        <?php echo date('d-M-Y', strtotime($dat['date'])); ?>
                                    </td>

                                    <?php
                                } elseif ($date_count == 0) {
                                    ?>
                                    <td>
                                        <?php echo date('d-M-Y', strtotime($dat['date'])); ?>
                                    </td>
                                    <?php
                                }
                                if (!empty($dat['items'][0]) && !empty($dat['items'][3])) {
                                    ?>
                                    <td>
                                        <?php echo $dat['items'][0]; ?>
                                    </td>
                                    <td>
                                        <?php echo $dat['items'][1]; ?>
                                    </td>
                                    <td>
                                        <?php echo $dat['items'][2]; ?>
                                    </td>
                                    <td></td>
                                    <td rowspan="2">
                                        <?php echo $dat['amount']; ?>
                                    </td>

                                </tr>
                                <tr>
                                    <td class="helper_bg">
                                        <?php echo $dat['items'][3]; ?>
                                    </td>
                                    <td>
                                        <?php echo isset($dat['items'][1]) ? $dat['items'][1] : ''; ?>
                                    </td>
                                    <td class="helper_bg">
                                        <?php echo isset($dat['items'][4]) ? $dat['items'][4] : ''; ?>
                                    </td>
                                    <td></td>
                                </tr>
                                <?php
                                } else {
                                    ?>
                                <?php
                                $helper_class = "";
                                if (!empty($dat['items'][0])) {
                                    $lab_help_no = isset($dat['items'][0]) ? $dat['items'][0] : '';
                                    $lab_help_rate = isset($dat['items'][2]) ? $dat['items'][2] : '';
                                } elseif (!empty($dat['items'][3])) {
                                    $helper_class = "helper_bg";
                                    $lab_help_no = isset($dat['items'][3]) ? $dat['items'][3] : '';
                                    $lab_help_rate = isset($dat['items'][4]) ? $dat['items'][4] : '';
                                } else {
                                    $lab_help_no = "";
                                    $lab_help_rate = "";
                                }

                                ?>
                                <td class="<?php echo $helper_class; ?>">
                                    <?php echo $lab_help_no; ?>
                                </td>
                                <td>
                                    <?php echo isset($dat['items'][1]) ? $dat['items'][1] : ''; ?>
                                </td>
                                <td class="<?php echo $helper_class; ?>">
                                    <?php echo $lab_help_rate; ?>
                                </td>
                                <td>
                                    <?php echo isset($dat['items'][5]) ? $dat['items'][5] : ''; ?>
                                </td>
                                <td>
                                    <?php echo $dat['amount']; ?>
                                </td>
                                <?php
                                }
                                ?>
                            <?php
                            if ($i == 1 && filter_input(INPUT_GET, 'export') != 'csv') {
                                ?>
                                <td rowspan="<?php echo $data['row_count']; ?>">
                                    <?php echo Controller::money_format_inr($data['total_amount'], 2); ?>
                                </td>
                                <?php
                            }
                            ?>
                            </tr>
                            <?php
                            $i++;
                            $date_count = 1;
                        }
                        ?>

                        <?php

                    }
                    ?>

                    <?php
                    if (filter_input(INPUT_GET, 'export') == 'csv') {
                        ?>
                        <tr>
                            <td>Total</td>
                            <?php
                            for ($k = 0; $k < 6; $k++) {
                                echo '<td></td>';
                            }
                            ?>
                            <td>
                                <?php echo Controller::money_format_inr($data['total_amount'], 2); ?>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
    <div id="msg_box"></div>

</div>

<style>
    .weekly_tbl {
        width: 100%;
        border: 1px solid #ddd;
    }

    .weekly_tbl td,
    .weekly_tbl th {
        padding: 6px 8px;
        border: 1px solid #ccc;
    }

    .weekly-list {
        max-height: 400px;
    }

</style>
<script>
    $(document).ready(function () {
        $('#loading').hide();
        $("#weekly_tbl").tableHeadFixer({
            'left': false,
            'foot': true,
            'head': true
        });
    });
    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
</script>