    <div id="table-scroll" class="margin-horizontal-10 margin-top-20">                        
        <div id="table-wrap" class="horizontal-scroll">
            <!-- <div class="table-responsive"> -->
                <table class="table item_table w-100">
                    <thead class="entry-table">
                        <tr>
                            <th>S.NO</th>
                            <th width="30%">DESCRIPTION OF ITEM</th> 
                            <th>MATERIAL</th>                           
                            <th>UNIT</th>
                            <?php if ($this->getActiveTemplate() == 'TYPE-7'){ ?>
                                <th>LENGTH</th> 
                                <th>WIDTH</th> 
                            <?php } ?>
                            <th>QTY</th>                                    
                            <th>MRP</th>
                            <th>AFTER DISCOUNT</th>
                            <th width="10%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $new_rev = $rev_no;
                        $previous_rev = $interRev;
                        $inter_rev = $interRev.$rev_no;                        
                        $rev_no = "REV-".$rev_no."";
                        $interRev = $inter_rev;
                        $sql = 'SELECT * FROM `jp_quotation_section` '
                                . 'WHERE `qtn_id` = ' . $qtid
                                . ' AND revision_no IN( "' . $rev_no . '","' . $interRev . '")';
                        $sectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                        $i = 1;
                        $cmaterial_name='';
                        $material_name=''; 
                        $smaterial_name=''; 
                        foreach ($sectionArray as $key => $value1) {
                            
                            $sql = 'SELECT * FROM `jp_quotation_gen_category` '
                                    . 'WHERE `qid` = ' . $qtid . ' AND `section_id` = ' . $value1["id"]
                                    . ' AND revision_no IN( "' . $rev_no . '","' . $interRev . '")';
                            $subsectionArray = Yii::app()->db->createCommand($sql)->queryAll();
                            $unit = Unit::model()->findByPk($value1['unit']);
                            
                            ?>
                            <tr>
                                <?php if(empty($subsectionArray)) { ?>
                                    <th colspan="<?php echo ($this->getActiveTemplate() == 'TYPE-7')?'9':'7'; ?>" style="background:#ccc;"><?php echo $value1['section_name'] ?></th>
                                        <th style="background:#ccc;" class="text-right" data-id="" 
                                        data-sec-id = "<?php echo $value1["id"] ?>" 
                                         data-subsec-id = "">
                                            <span class="edit_sec"><i class="fa fa-edit"></i></span>
                                            <span class="delete_row" 
                                            data-type="section" data-id ="<?php echo $value1["id"] ?>"><i class="fa fa-trash"></i></span>
                                        </th>
                                <?php }else{ ?>
                                    <th colspan="<?php echo ($this->getActiveTemplate() == 'TYPE-7')?'9':'7'; ?>" style="background:#ccc;"><?php echo $value1['section_name'] ?></th>
                                    <th style="background:#ccc;" class="text-right" data-id="" 
                                        data-sec-id = "<?php echo $value1["id"] ?>" 
                                         data-subsec-id = "">                                            
                                            <span class="delete_row" 
                                            data-type="section" data-id ="<?php echo $value1["id"] ?>"><i class="fa fa-trash"></i></span>
                                        </th>
                                <?php } ?>
                            </tr>
                            <?php //echo "<pre>ss";print_R($subsectionArray);
                            foreach ($subsectionArray as $key => $value2) {                                
                                $sql = 'SELECT * FROM `jp_quotation_gen_worktype` '
                                        . ' WHERE `qid` = ' . $value2["qid"]
                                        . ' AND `section_id` = ' . $value2["section_id"]
                                        . ' AND `category_label_id` = ' . $value2["id"]
                                        . ' AND revision_no IN( "' . $rev_no . '","' . $interRev . '")';
                                $subitemArray = Yii::app()->db->createCommand($sql)->queryAll();//echo $sql;
                                $unit = Unit::model()->findByPk($value2['unit']);
                                ?>
                                <tr>
                                    <?php if(empty($subitemArray)) { 
                                       
                                        ?>
                                        
                                        <th style="background:#c5d9f1;"><?php echo $i; ?></th>
                                        <th  colspan="<?php echo ($this->getActiveTemplate() == 'TYPE-7')?'8':'6'; ?>" style="background:#c5d9f1;"><?php echo $value2['category_label'] ?></th> 
                                            <th style="background:#c5d9f1;" class="text-right" data-id="" 
                                            data-sec-id = "<?php echo $value1["id"] ?>" 
                                            data-subsec-id = "<?php echo $value2["id"] ?>">
                                                <span class="edit_sec"><i class="fa fa-edit"></i></span>
                                                <span class="delete_row" 
                                            data-type="subsection" data-id ="<?php echo $value2["id"] ?>"><i class="fa fa-trash"></i></span>
                                            </th>
                                    <?php }else{  
                                        ?>
                                        <th style="background:#c5d9f1;"><?php echo $i; ?></th>
                                        <th  colspan="<?php echo ($this->getActiveTemplate() == 'TYPE-7')?'8':'6'; ?>" style="background:#c5d9f1;"><?php echo $value2['category_label'] ?></th> 
                                        <th style="background:#c5d9f1;" class="text-right" data-id="" 
                                            data-sec-id = "<?php echo $value1["id"] ?>" 
                                            data-subsec-id = "<?php echo $value2["id"] ?>">                                                
                                            <span class="delete_row" 
                                            data-type="subsection" data-id ="<?php echo $value2["id"] ?>"><i class="fa fa-trash"></i></span>
                                            </th>
                                    <?php } ?>                                   
                                </tr>
                                
                                <?php
                                $letters = range('a', 'z');
                                $k = 0;//echo "<pre>ff";print_R($subitemArray);
                                foreach ($subitemArray as $key => $value) {
                                    $sql = 'SELECT * FROM `jp_sales_quotation` '
                                            . ' WHERE `parent_type` = 1 AND `parent_id` = ' . $value['id']
                                            . ' AND revision_no IN( "' . $rev_no . '","' . $interRev . '") 
                                            AND `deleted_status` = 1';
                                    $wrktypeitemArray = Yii::app()->db->createCommand($sql)->queryAll();
                                    $unit = Unit::model()->findByPk($value['unit']);
                                    ?>
                                    <tr class="td_group">
                                        <th><?php echo $letters[$k]; ?></th>
                                        <?php if($this->getActiveTemplate() == 'TYPE-7'){ ?>
                                            <td colspan="<?php echo empty($wrktypeitemArray) ? '1' : '8'?>">
                                        <?php }else{?>
                                            <td colspan="<?php echo empty($wrktypeitemArray) ? '1' : '6'?>">
                                        <?php } ?>
                                            <b><?php echo $value['worktype_label'] ?></b>
                                        </td>
                                        <?php if (empty($wrktypeitemArray)) { ?>
                                            <td></td>
                                            <td><?php if($value['quantity_nos']!=''){
                                                        echo "nos";
                                                    }else{
                                                    echo isset($unit) ? $unit->unit_name : "";
                                                    } ?>
                                            </td>
                                            <?php if($this->getActiveTemplate() == 'TYPE-7'){ ?>
                                                <td></td>
                                                <td></td>
                                            <?php }?>
                                            <td><?php if($model->template_type == '5'){ echo $value['quantity'];
                                                        }else if($value['quantity_nos']!=''){
                                                            echo $value['quantity_nos'];
                                                        }else{
                                                            echo $value['quantity'];
                                                        } ?>
                                            </td>
                                            <td><?php echo $value['mrp'] ?></td>
                                            <td><?php echo $value['amount_after_discount'] ?></td>
                                                                                              
                                        <?php } ?>
                                        
                                        <th class="text-right" data-id="<?php echo $value['id'] ?>" 
                                        data-sec-id = "<?php echo $value1["id"] ?>" 
                                         data-subsec-id = "<?php echo $value2["id"] ?>">
                                            <span class="edit_sec"><i class="fa fa-edit"></i></span>
                                            <span class="delete_row" 
                                            data-type="subitem" data-id ="<?php echo $value["id"] ?>"><i class="fa fa-trash"></i></span>
                                        </th>
                                    </tr>
                                    <?php if ($value['description'] != "") { ?>
                                        <tr class="td_group">
                                            <th>*</th>
                                            <td colspan="<?php echo ($this->getActiveTemplate() == 'TYPE-7')?'8':'6'; ?>"><?php echo $value['description'] ?></td>
                                            
                                        </tr>
                                    <?php }
                                    $sum = 0;//echo "<pre>";print_r($wrktypeitemArray);die;
                                    foreach ($wrktypeitemArray as $key => $value) {
                                        $sum += $value['amount_after_discount'];
                                        $unit = Unit::model()->findByPk($value['unit']);
                                        $desc = ($value['description'] !="")?" - ".$value['description']:"";

                                        if($value['description']=="" && ($value['shutterwork_description'] !="" || $value['carcass_description']!="")){
                                            
                                            $desc = " - <b>Shutter:</b> ".$value['shutterwork_description']." , <b>Carcass:</b> ".$value['carcass_description'];
                                          
                                        }
                                        ?>
                                        <tr class="td_group">
                                            <th><?php echo '*' ?></th>
                                            <td>
                                                <?php 
                                                if($value['item_name']!=""){
                                                    echo $value['item_name'];
                                                }else{
                                                    echo $value['worktype_label'].$desc;
                                                }                                                                                
                                                ?>
                                            </td>
                                            <td>
                                                <?php 
                                                  
                                                $item_master = QuotationItemMaster::model()->findByPk($value['item_id']);
                                    
                                                if ($value['shutterwork_material']) {
                                                    $shutter_material_id = $item_master['shutter_material_id'];                                        
                                                    $specification = Specification::model()->findByPk($shutter_material_id);
                                                    $brand = Brand::model()->findByPk($specification ['brand_id']);
                                                    $category = PurchaseCategory::model()->findByPk($specification ['cat_id']);
                                                    $smaterial_name = $category["category_name"].'-'.$brand["brand_name"].'-'.$specification["specification"];
                                                }
                                        
                                                if ($value['carcass_material']) {
                                                    $carcass_material_id = $item_master['carcass_material_id'];
                                                    $specification = Specification::model()->findByPk($carcass_material_id);
                                                    $brand = Brand::model()->findByPk($specification ['brand_id']);
                                                    $category = PurchaseCategory::model()->findByPk($specification ['cat_id']);
                                                    $cmaterial_name = $category["category_name"].'-'.$brand["brand_name"].'-'.$specification["specification"];
                                                }
                                                
                                                if ($value['material']) {
                                                    $material_ids = $item_master['material_ids'];
                                                    $specification = Specification::model()->findByPk($material_ids);
                                                    $brand = Brand::model()->findByPk($specification ['brand_id']);
                                                    $category = PurchaseCategory::model()->findByPk($specification ['cat_id']);
                                                    $material_name = $category["category_name"].'-'.$brand["brand_name"].'-'.$specification["specification"];
                                                }

                                                if ($value['shutterwork_material']) {
                                                    if($value['shutterwork_material'] == $value['carcass_material']){
                                                        echo $smaterial_name;
                                                    }else{
                                                        echo $smaterial_name. " , " .$cmaterial_name;
                                                    }
                                                } 

                                                if ($value["material"]) {
                                                    echo $material_name;
                                                }
                                                    
                                                ?>
                                            </td>                                            
                                            <td><?php if($value['quantity_nos']!=''){
                                                echo "nos";
                                            }else{
                                            echo isset($unit) ? $unit->unit_name : "";
                                            } ?></td>
                                            <?php if ($this->getActiveTemplate() == 'TYPE-7'){ ?>
                                            <td><?php echo (floatval($value['length'])>0)?floatval($value['length']):$value['length']; ?></td>
                                            <td><?php echo (floatval($value['width'])>0)?floatval($value['width']):$value['width']; ?></td>
                                            <?php } ?>
                                            <td><?php if($model->template_type == '5'){ echo $value['quantity'];
                                                        }else if($value['quantity_nos']!=''){
                                                            echo $value['quantity_nos'];
                                                        }else{
                                                           echo $value['quantity'];
                                                        } ?></td>
                                            <td class="text-right"><?php echo $value['mrp'] ?></td>
                                            <td class="text-right"><?php echo $value['amount_after_discount'] ?></td>
                                            <td></td>
                                            
                                        </tr>
                                    <?php }
                                    if (!empty($wrktypeitemArray)) {  } 
                                    $k++;
                                }
                                ?>
                                <?php
                                    $i++;
                                ?>
                                <!-- <tr>
                                    <td class="text-right" colspan="7">Total Cost for <b><?php //echo $value2['category_label'] ?></b></td>
                                    <th class="text-right">
                                        <?php //echo Controller::money_format_inr(Controller::getSubSectionSum($value2['id'], $qtid, $value2["section_id"], $rev_no), 2); ?>
                                    </th>
                                </tr> -->
                            <?php } 
                            //die; ?>
                            <!-- <tr>
                                <td class="text-right"  colspan="7"  style="background: #d6000a14;">Total Cost for <b><?php //echo $value1['section_name'] ?></b></td>
                                <th class="text-right" style="background: #d6000a14;">
                                    <?php //echo Controller::money_format_inr(Controller::getSectionSum($value1['id'], $qtid, $rev_no), 2); ?>
                                </th>
                            </tr> -->
                        <?php } ?>
                    </tbody>  
                    <tfoot>
                        <tr>
                            <th colspan="<?php echo ($this->getActiveTemplate() == 'TYPE-7')?'7':'5'; ?>" class="text-right">TOTAL</th>
                            <th class="text-right">
                                <h5><?php
                                $rev_status = NULL;                                
                                if($status ==1){
                                    
                                    $intrRev = $interRev;                                    
                                    $newRev = $rev_no;
                                    $revision_no = ("'$intrRev'" .','. "'$newRev'");
                                    $rev_status = 1;
                                    //$total_amount = Controller::getMrpSum($qtid, $revision_no,$rev_status);
                                    $total_amount = Controller::getQuotationSum($qtid, $revision_no,$rev_status,1);
                                    
                                }else{
                                    //$total_amount = Controller::getMrpSum($qtid, $rev_no,$rev_status);
                                    $total_amount = Controller::getQuotationSum($qtid, $rev_no,$rev_status,1);
                                }
                                
                                echo Controller::money_format_inr($total_amount, 2);
                                ?></h5>
                            </th>                        
                            <th class="text-right">
                                <h5><?php
                                $rev_status = NULL;
                                
                                if($status ==1){
                                    $intrRev = $interRev;
                                    $newRev = $rev_no;
                                    $revision_no = ("'$intrRev'" .','. "'$newRev'");
                                    $rev_status = 1;
                                    $total_amount = Controller::getQuotationSum($qtid, $revision_no,$rev_status);
                                    
                                }else{
                                    $total_amount = Controller::getQuotationSum($qtid, $rev_no,$rev_status);
                                }
                                
                                echo Controller::money_format_inr($total_amount, 2);
                                ?></h5>
                                <input type ="hidden" value="<?php echo $total_amount ?>" id="total_after_discount">
                            </th>
                            <th></th>
                        </tr>
                        <tr>
                            <td colspan="<?php echo ($this->getActiveTemplate() == 'TYPE-7')?'10':'8'; ?>">
                                <?php
                                $sql = 'SELECT * FROM jp_quotation_revision '
                                . ' WHERE qid='.$qtid
                                . ' AND revision_no="'.$rev_no.'"';
                                $revision_model = Yii::app()->db->createCommand($sql)->queryRow();
                                if(!$revision_model){//if new revision not saved
                                    $sql = 'SELECT * FROM jp_quotation_revision '
                                        . ' WHERE qid='.$qtid
                                        . ' AND revision_no="'.$previous_rev.'"';
                                    $revision_model = Yii::app()->db->createCommand($sql)->queryRow();
                                }
                                ?>
                                <div class="row" >
                                    <div class="col-md-2" >
                                        <div>
                                            <label>Tax Slab:</label>
                                            <?php
                                            $datas = TaxSlabs::model()->getAllDatas();
                                            ?>
                                            <select class="form-control" name="tax_slab" id="add_tax_slab">
                                                <option value="">Select one</option>
                                                <?php
                                                foreach ($datas as $value) {
                                                    if ($revision_model['tax_slab']==$value['tax_slab_value']) {//$value['set_default'] == 1
                                                        echo ' <option data-id="' . $value['id'] . '" value="' . $value['tax_slab_value'] . '" selected>' . $value['tax_slab_value'] . '%</option>';
                                                    } else {
                                                        echo ' <option  data-id="' . $value['id'] . '" value="' . $value['tax_slab_value'] . '">' . $value['tax_slab_value'] . '%</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-1" >                       
                                        <div>
                                            <label>SGST(%) :</label>
                                            
                                            <input type ="text" value="<?php echo $revision_model['sgst_percent'] ?>" class="allownumericdecimal form-control calculation" id="sgst_percent" >                      
                                            <div id="sgst_amount" class="padding-box"><?php echo $revision_model['sgst_amount'] ?></div>                    
                                        </div>
                                    </div>		
                                    <div class="col-md-1" >
                                        <div>
                                            <label>CGST(%) :</label>
                                            <input type ="text" value="<?php echo $revision_model['cgst_percent'] ?>" class="allownumericdecimal form-control calculation" id="cgst_percent" >                      
                                            <div id="cgst_amount" class="padding-box"><?php echo $revision_model['cgst_amount'] ?></div>                      
                                        </div>
                                    </div>
                                    <div class="col-md-1" >
                                        <div>
                                            <label>IGST(%) :</label>
                                            <input type ="text" value="<?php echo $revision_model['igst_percent'] ?>" class="allownumericdecimal form-control calculation" id="igst_percent" >                      
                                            <div id="igst_amount" class="padding-box"><?php echo $revision_model['igst_amount'] ?></div>                      
                                        </div>
                                    </div>

                                    <div class="col-md-2" >
                                        <div style='display:none;'>
                                            <label>DISCOUNT(%) :</label>
                                            <input type="text" class="form-control text-right special_discount" value="<?php
                                            echo $revision_model['discount_precent'];                                        
                                            ?>" data-qtid="<?php echo $qtid?>" 
                                            data-rev ="<?php echo $rev_no?>">
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <?php
                                $sql = 'SELECT * FROM jp_quotation_revision '
                                        . ' WHERE qid='.$qtid
                                        . ' AND revision_no="'.$rev_no.'"';
                                $revision_model = Yii::app()->db->createCommand($sql)->queryRow();
                                if(!$revision_model){//if new revision not saved
                                    $sql = 'SELECT * FROM jp_quotation_revision '
                                        . ' WHERE qid='.$qtid
                                        . ' AND revision_no="'.$previous_rev.'"';
                                    $revision_model = Yii::app()->db->createCommand($sql)->queryRow();
                                }
                                $total_tax = $revision_model['sgst_amount']+
                                $revision_model['cgst_amount']+
                                $revision_model['igst_amount'];

                                $total_tax_percent = $revision_model['sgst_percent']+
                                $revision_model['cgst_percent']+
                                $revision_model['igst_percent'];

                            

                                $total_amoun_withtax = $total_amount + $total_tax;
                                ?>
                            <th colspan="<?php echo ($this->getActiveTemplate() == 'TYPE-7')?'9':'7'; ?>" class="text-right">Total
                                </th>
                            <th  class="text-right">
                                <h5>
                                <?php   
                                echo Controller::money_format_inr($total_amount, 2);
                                ?></h5>
                            </th>
                        </tr>
                        <tr>
                            <?php
                                if($this->getActiveTemplate() == 'TYPE-8'|| ($this->getActiveTemplateID()>8)){
                            ?>
                            <th colspan="7" class="text-right">TOTAL AMOUNT INCLUDING GST<?php if($total_tax_percent >0) {
                                    echo "(".$total_tax_percent."%)";
                                }else{
                                    echo "";
                                } ?></th>
                            <?php } else{ ?>
                            <th colspan="9" class="text-right">TOTAL AMOUNT INCLUDING GST<?php if($total_tax_percent >0) {
                                    echo "(".$total_tax_percent."%)";
                                }else{
                                    echo "";
                                } ?></th>
                            <?php } ?>
                            <th class="text-right">
                                <h5 class="total_before_additional_discount"><?php
                                $discount_amount = ($total_amoun_withtax * $revision_model['discount_precent']) / 100;
                                $total_after_discount = $total_amoun_withtax - $discount_amount;
                                echo Controller::money_format_inr($total_after_discount , 2);                                        
                                ?></h5>
                            </th>
                        </tr>
                        <tr>
                            <?php
                                if($this->getActiveTemplate() == 'TYPE-8'|| ($this->getActiveTemplateID()>8)){
                            ?>
                            <th colspan="7" class="text-right">Discount</th>
                            <?php } else{ ?>
                                <th colspan="9" class="text-right">Special Discount</th>
                            <?php } ?>
                            <th class="text-right">
                                <?php 
                                $discount_amount_new =((float)$revision_model['total_after_additionl_discount']>0) ?($total_amoun_withtax -(float)$revision_model['total_after_additionl_discount']):0;
                                echo Controller::money_format_inr($discount_amount_new , 2);                                        
                                ?>                                
                            </th>
                        </tr>
                        
                        <tr>
                            <th colspan="<?php echo ($this->getActiveTemplate() == 'TYPE-7')?'9':'7'; ?>" class="text-right">Total Amount after additional Discount</th>
                            <th class="text-right">
                             <input type="text" class="form-control text-right final_amount_after_additional" value="<?php echo Controller::money_format_inr($revision_model['total_after_additionl_discount'] , 2);?>" data-qtid="<?php echo $qtid?>" 
                                            data-rev ="<?php echo $rev_no?>">
                            </th>
                        </tr>                                                             
                    </tfoot>
                </table>
            <!-- </div> -->
        </div>
    </div>

<script>
    $("#total_after_additionl_discount").blur(function(){
        saveRevisionData();
        $( ".updateRevision" ).trigger( "click" );
        $(".btn-preview").trigger("click");
    })
</script>