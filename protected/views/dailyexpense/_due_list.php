<?php
/* @var $this ProjectsController */
/* @var $data Projects */
?>



<?php
if ($index == 0) {
    ?>
    <thead class="entry-table">
    <tr> 
        <th>No</th>
        <th>Company</th>
        <th>Bill No</th>
        <th>Amount</th>
        <th>Description</th>
        <th>Due Date</th>        
        <th>Action</th>           
    </tr>   
    </thead>
<?php } ?>
    <tr> 
        <td style="width: 50px;"><?php echo $index+1; ?></td>
        <td>
        <?php
            $company = Company::model()->findByPk($data['company_id']);
            if($company)
                echo $company->name;
        ?>
        </td>
        <td><?= $data['bill_no']; ?></td>
    <td><?= $data['amount']; ?></td>
    <td><?= $data['description']; ?></td>
    <td><?= date('Y-m-d', strtotime($data['due_date'])); ?></td>
    <td  data-value="<?php echo CHtml::encode($data['id']); ?>" data-id="<?php echo $data['id']; ?>">
       
    <a href="<?php echo Yii::app()->createUrl('bills/AddDailyexpensebill', array('bill_id' => $data['id'])); ?>">
    <span class="fa fa-edit editable" style="cursor:pointer;"></span>
</a>
        </span>
    </td>
        
        		
    
    </tr>  

    
