<?php
/* @var $this MaterialEntriesController */
/* @var $model MaterialEntries */

$this->breadcrumbs=array(
	'Material Entries'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MaterialEntries', 'url'=>array('index')),
	array('label'=>'Manage MaterialEntries', 'url'=>array('admin')),
);
?>

<h1>Create MaterialEntries</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>