<?php
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$newQuery = "";

foreach ($arrVal as $arr) {
    if ($newQuery)
        $newQuery .= ' OR';
    $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
}

$url_data = '';
if (isset($_REQUEST['date_from'])) {
    $url_data .= '&date_from=' . $_REQUEST['date_from'];
}
if (isset($_REQUEST['date_to'])) {
    $url_data .= '&date_to=' . $_REQUEST['date_to'];
}
if (isset($_REQUEST['company_id'])) {
    $url_data .= '&company_id=' . $_REQUEST['company_id'];
}

$spacing = "";
if (filter_input(INPUT_GET, 'export') == 'pdf') {
    $spacing = "style='margin:0px 30px'";
    ?>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">

<?php } ?>
<div class="container" id="project">
    <div class="expenses-heading header-container">
        <div <?php echo $spacing ?>>
            <h2>Profit and Loss REPORT</h2>
        </div>
        <?php if (!filter_input(INPUT_GET, 'export')) { ?>
            <div class="btn-container">
                <a class="btn btn-info" href="<?php echo Yii::app()->request->requestUri . "&export=csv" . $url_data; ?>"
                    style="text-decoration: none; color: white;" title="SAVE AS EXCEL">
                    <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>
                </a>

                <a class="btn btn-info" href="<?php echo Yii::app()->request->requestUri . "&export=pdf" . $url_data; ?>"
                    style="text-decoration: none; color: white;" title="SAVE AS PDF">
                    <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
                </a>
            </div>
        <?php } ?>
    </div>
    <?php if (!filter_input(INPUT_GET, 'export')) { ?>
        <div class="page_filter clearfix">
            <form id="vendorform" action="<?php $url = Yii::app()->createAbsoluteUrl("expenses/Profitandloss"); ?>"
                method="POST">

                <div class="form-search">
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
                            <?php
                            if (!isset($_POST['date_from']) || $_POST['date_from'] == '') {
                                $datefrom = date("Y-") . "01-" . "01";
                            } else {
                                $datefrom = $_POST['date_from'];
                            }
                            ?>
                            <!-- <div class="display-flex"> -->
                            <label> From </label>
                            <?php echo CHtml::textField('date_from', $datefrom, array("id" => "date_from", 'class' => 'form-control')); ?>
                            <!-- </div> -->
                        </div>
                        <div class="form-group col-xs-12 col-sm-3 col-md-2 ">

                            <?php
                            if (!isset($_POST['date_to']) || $_POST['date_to'] == '') {
                                $date_to = date("Y-m-d");
                            } else {
                                $date_to = $_POST['date_to'];
                            }
                            ?>
                            <!-- <div class="display-flex"> -->
                            <label>To </label>
                            <?php echo CHtml::textField('date_to', $date_to, array("id" => "date_till", 'class' => 'form-control')); ?>
                            <!-- </div> -->
                        </div>
                        <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
                            <label for="company">Company</label>
                            <?php
                            echo CHtml::dropDownList('company_id', 'company_id', CHtml::listData(Company::model()->findAll(array(
                                'select' => array('id, name'),
                                'order' => 'id DESC',
                                'condition' => '(' . $newQuery . ')',
                                'distinct' => true
                            )), 'id', 'name'), array('empty' => 'Select Company', 'id' => 'company_id', 'class' => 'form-control', 'options' => array($company_id => array('selected' => true))));
                            ?>
                        </div>
                        <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
                            <label class="d-sm-block d-none">&nbsp;</label>
                            <div class="text-sm-left text-right">
                                <?php echo CHtml::submitButton('GO', array('id' => 'btSubmit', 'class' => 'btn btn-sm btn-primary')); ?>
                                <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('profitandloss') . '"', 'class' => 'btn btn-sm btn-default')); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    <?php } ?>

    <div id="contenthtml" class="contentdiv" <?php echo $spacing ?>>
        <i><span style="color:red">**</span>Note: Includes expense of all projects of the selected company</i>
        <div class="report-table-wrapper">
            <table class="table total-table">
                <thead class="entry-table sticky-thead">
                    <tr>
                        <th>Sl No.</th>
                        <th>Total Receipt</th>
                        <th>Total Invoice</th>
                        <th>Total Expense</th>
                        <th>Total Daily Expense</th>
                        <th>Total Deposit</th>
                        <th>Purchase Return</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td align="right">
                            <?php echo ($recepit != '') ? Controller::money_format_inr($recepit, 2) : '0.00'; ?>
                        </td>
                        <td align="right">
                            <?php echo ($invoice != '') ? Controller::money_format_inr($invoice, 2) : '0.00'; ?>
                        </td>
                        <td align="right">
                            <?php echo ($total_expense != '') ? Controller::money_format_inr($total_expense, 2) : '0.00'; ?>
                        </td>
                        <td align="right">
                            <?php echo ($total_daily_expense != '') ? Controller::money_format_inr($total_daily_expense, 2) : '0.00'; ?>
                        </td>
                        <td align="right">
                            <?php echo ($total_deposit != '') ? Controller::money_format_inr($total_deposit, 2) : '0.00'; ?>
                        </td>
                        <td align="right">
                            <?php echo ($return != '') ? Controller::money_format_inr($return, 2) : '0.00'; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
<script>
    $(document).ready(function () {
        $("#date_from").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: $("#date_till").val()
        });
        $("#date_till").datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $("#date_from").change(function () {
            $("#date_till").datepicker('option', 'minDate', $(this).val());
        });
        $("#date_till").change(function () {
            $("#date_from").datepicker('option', 'maxDate', $(this).val());
        });
    });
</script>