<?php
/* @var $this LogController */
/* @var $data JpLog */
?>
<?php 
if ($index == 0) {
    ?>

    <thead>
        <tr>
            <th width="5%">Sl No.</th>
            <th width="55%">Data</th>
            <th width="10%">Table</th>
            <th width="15">Deleted By</th>
            <th width="15%">Date & Time</th>
        </tr>   
    </thead>
            <?php
        }
        ?>
    <tbody>
	<tr>
            <td><?php echo $index + 1; ?></td>
            <td style="word-break: break-all;"><?php echo CHtml::encode($data->log_data); ?></td>
            <td><?php echo CHtml::encode($data->log_table); ?></td>
            <td>
                <?php
                    $subModel = Users::model()->findByPk($data->log_action_by);
                    echo CHtml::encode(($subModel)?$subModel->first_name." ".$subModel->last_name:"");
                ?>
            </td>
            <td><?php echo CHtml::encode($data->log_datetime); ?></td>
        </tr>
    </tbody>