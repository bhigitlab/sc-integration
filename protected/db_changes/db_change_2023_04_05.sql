ALTER TABLE `jp_project_template` ADD `template_format` blob NULL AFTER `description`;
ALTER TABLE `jp_project_template` CHANGE `template_format` `template_format` blob CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
DROP TABLE IF EXISTS `jp_project_template_revision`; 

CREATE TABLE `jp_project_template_revision` (
  `template_revision_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  `template_version` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

ALTER TABLE `jp_project_template_revision`
  ADD PRIMARY KEY (`template_revision_id`),
  ADD KEY `fk_template_revision_id` (`template_id`);

ALTER TABLE `jp_project_template_revision`
  MODIFY `template_revision_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `jp_project_template_revision`
  ADD CONSTRAINT `fk_template_revision_id` FOREIGN KEY (`template_id`) REFERENCES `jp_project_template` (`template_id`) ON UPDATE CASCADE;
