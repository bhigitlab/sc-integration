-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 23, 2021 at 02:12 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scv2_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `jp_pre_dailyreport`
--

CREATE TABLE `jp_pre_dailyreport` (
  `dr_id` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `followup_id` int(11) DEFAULT NULL,
  `record_grop_id` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `projectid` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `subcontractor_id` int(11) DEFAULT NULL,
  `expensehead_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `works_done` text NOT NULL,
  `materials_unlade` text NOT NULL,
  `wrktype_and_numbers` longtext,
  `extra_work_done` text NOT NULL,
  `amount` float(20,2) NOT NULL,
  `labour` float(20,2) DEFAULT NULL,
  `wage` float(20,2) DEFAULT NULL,
  `wage_rate` float(20,2) DEFAULT NULL,
  `helper` float(20,2) DEFAULT NULL,
  `helper_labour` float(20,2) DEFAULT NULL,
  `lump_sum` float(20,2) DEFAULT NULL,
  `approve_status` enum('1','2') DEFAULT NULL COMMENT '1=>approve, 2=>not approve',
  `description` text,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `labour_wage` float(20,2) NOT NULL,
  `helper_wage` float(20,2) NOT NULL,
  `approval_status` enum('0','1','2') DEFAULT '0' COMMENT '0=>Pending,1=>Approved,2=>Rejected',
  `approved_by` int(1) NOT NULL,
  `record_action` varchar(300) NOT NULL,
  `approve_notify_status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=>no.not visible to admin,1=>yes visible to admin',
  `cancelled_by` int(11) DEFAULT NULL,
  `cancelled_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jp_pre_dailyreport`
--

INSERT INTO `jp_pre_dailyreport` (`dr_id`, `ref_id`, `followup_id`, `record_grop_id`, `projectid`, `company_id`, `subcontractor_id`, `expensehead_id`, `date`, `works_done`, `materials_unlade`, `wrktype_and_numbers`, `extra_work_done`, `amount`, `labour`, `wage`, `wage_rate`, `helper`, `helper_labour`, `lump_sum`, `approve_status`, `description`, `created_by`, `created_date`, `updated_by`, `updated_date`, `labour_wage`, `helper_wage`, `approval_status`, `approved_by`, `record_action`, `approve_notify_status`, `cancelled_by`, `cancelled_at`) VALUES
(1, 2, NULL, '2021-07-23 09:40:19', 1, 1, 1, 1, '2021-07-23 00:00:00', '', '', NULL, '', 375.00, 5.00, 5.00, 15.00, NULL, NULL, NULL, NULL, 'new desc-updated', 100, '2021-07-23 00:00:00', 101, '2021-07-23 15:10:19', 0.00, 0.00, '1', 100, 'update', '1', NULL, '0000-00-00 00:00:00'),
(2, 2, 1, '2021-07-23 11:10:14', 1, 1, 1, 1, '2021-07-23 00:00:00', '', '', NULL, '', 1950.00, 5.00, 5.00, 78.00, NULL, NULL, NULL, NULL, 'new desc-updated', 100, '2021-07-23 00:00:00', 101, '2021-07-23 16:40:14', 0.00, 0.00, '2', 0, 'update', '0', 101, '2021-07-23 16:40:33'),
(3, 2, 2, '2021-07-23 11:10:55', 1, 1, 1, 1, '2021-07-23 00:00:00', '', '', NULL, '', 350.00, 5.00, 5.00, 14.00, NULL, NULL, NULL, NULL, 'new desc-updated', 100, '2021-07-23 00:00:00', 101, '2021-07-23 16:40:55', 0.00, 0.00, '1', 100, 'update', '1', NULL, '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jp_pre_dailyreport`
--
ALTER TABLE `jp_pre_dailyreport`
  ADD PRIMARY KEY (`dr_id`),
  ADD KEY `projectid_fk` (`projectid`),
  ADD KEY `company_id_fk` (`company_id`),
  ADD KEY `ref_id_fk` (`ref_id`),
  ADD KEY `createdby_fk` (`subcontractor_id`),
  ADD KEY `created_by_fk` (`created_by`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jp_pre_dailyreport`
--
ALTER TABLE `jp_pre_dailyreport`
  MODIFY `dr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jp_pre_dailyreport`
--
ALTER TABLE `jp_pre_dailyreport`
  ADD CONSTRAINT `company_id_fk` FOREIGN KEY (`company_id`) REFERENCES `jp_company` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `created_by_fk` FOREIGN KEY (`created_by`) REFERENCES `jp_users` (`userid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `projectid_fk` FOREIGN KEY (`projectid`) REFERENCES `jp_projects` (`pid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ref_id_fk` FOREIGN KEY (`ref_id`) REFERENCES `jp_dailyreport` (`dr_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `subcontractor_id` FOREIGN KEY (`subcontractor_id`) REFERENCES `jp_subcontractor` (`subcontractor_id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
