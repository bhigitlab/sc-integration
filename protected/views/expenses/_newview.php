<?php
$tblpx = Yii::app()->db->tablePrefix;

$gsttotal = $data["expense_sgst"] + $data["expense_cgst"] + $data["expense_igst"];
if ($index == 0) {
?>
    <thead class="entry-table">
        <tr>
            <?php
            if ((isset(Yii::app()->user->role) && ((in_array('/expenses/expensesdelete', Yii::app()->user->menuauthlist)) || (in_array('/expenses/expensesedit', Yii::app()->user->menuauthlist))))) {
            ?>
                <th></th>
            <?php } ?>
            <th>#ID</th>
            <th>Company</th>
            <th>Project</th>
            <th>Bill Number</th>
            <th>Invoice Number</th>
            <th>Return Number</th>
            <th>Expense Head</th>
            <th>Vendor/Subcontractor</th>
            <th>Expense Type</th>
            <th>Petty Cash By</th>
            <th>Bank</th>
            <th>Cheque No</th>
            <th>Description</th>
            <th>Amount</th>
            <th>Tax</th>
            <th>Total</th>
            <th>Amount Paid</th>
            <th>TDS</th>
            <th>Receipt Type</th>
            <th>Receipt</th>
            <th>Purchase Type</th>
            <th>Amount to Beneficiary</th>
            

        </tr>
    </thead>
<?php } ?>
<?php
$bgcolor = '';
$approval_status = $data["approval_status"];
$id = 'exp_id';
$tbl =$this->tableNameAcc('pre_expenses', 0);
$modelName= 'PreExpenses';

$final = Yii::app()->controller->actiongetFinalData($data, $approval_status,$id,$tbl,$modelName);

$data = $final['data'];
$approval_status = $final['approval_status'];
$count =$final['count'];

 
 $receiptType = NULL;
 
if ($data["payment_type"])
    if ($data["payment_type"] == 88) {
        $receiptType = "Cheque";
    } else if ($data["payment_type"] == 89) {
        $receiptType = "Cash";
    } else if ($data["payment_type"] == 103) {
        $receiptType = "Petty Cash";
    } else {
        $receiptType = "Credit";
    }

   
if ($data["expense_type"])
    if ($data["expense_type"] == 88) {
        $expenseType = "Cheque";
    } else if ($data["expense_type"] == 89) {
        $expenseType = "Cash";
    } else if ($data["expense_type"] == 103) {
        $expenseType = "Petty Cash";
    } else {
        $expenseType = "Credit";
    }
else
    $expenseType = NULL;
if ($data["purchase_type"]) {
    if ($data["purchase_type"] == 1) $purchaseType = "Credit";
    else if ($data["purchase_type"] == 2) $purchaseType = "Full Paid";
    else if ($data["purchase_type"] == 3) $purchaseType = "Partially Paid";
} else
    $purchaseType = "";
$class = '';
$class1 = '';
$date = strtotime("+2 days", strtotime($data["date"]));
$check_date =  date("Y-m-d", $date);
if (date('Y-m-d') <= $check_date) {
    if ($data['subcontractor_id'] == NULL) {
        $class = 'row-daybook';
        $class1 = 'delete_row';
    } else {
        $class = '';
        $class1 = '';
    }
} else {
    $class = 'row-daybook';
    $class1 = 'delete_row';
}

if ($data["employee_id"] != '') {
    $users = Users::model()->findByPk($data["employee_id"]);
    $employee = $users->first_name . ' ' . $users->last_name;
} else {
    $employee = '';
}
$expenseId      = $data["exp_id"];
$deleteconfirm  = Deletepending::model()->find(array("condition" => "deletepending_parentid = " . $expenseId . " AND deletepending_status = 0 AND deletepending_table = '{$tblpx}expenses'"));
$updateconfirm  = Editrequest::model()->find(array("condition" => "parent_id = " . $expenseId . " AND editrequest_status = 0 AND editrequest_table = '{$tblpx}expenses'"));
$confirmcount   = !empty($deleteconfirm) ? count($deleteconfirm->attributes) : 0;
$updatecount    = !empty($updateconfirm) ? count($updateconfirm->attributes) : 0;
if ($confirmcount > 0 || $updatecount > 0) {
    $deleteclass = "deleteclass";
    $deletetitle = "Waiting for delete confirmation from admin.";
} else {
    $deleteclass = "";
    $deletetitle = "";
}
$pettybgcolor ="";
if($data["petty_payment_approval"] == 0) { 
$pettybgcolor ="background:#2e6da438;";
}
$unreconcilbgcolor ="";

if(($data["expense_type"]==88 )&& ($data["reconciliation_status"] == 0)) { 
$unreconcilbgcolor =" background:#5bc0de3b;";
}
?>
<tr style="<?php echo $pettybgcolor.$unreconcilbgcolor ?>" id="<?php echo  $data['exp_id'] ?>" class="<?php echo  $deleteclass." ". (($approval_status == 0) ? 'bg_pending' : ''); ?>"  title="<?php echo $deletetitle; ?>" data-status="<?php echo $data["approval_status"] ?>" data-id="<?php echo ($data["approval_status"] == 0) ? $data['ref_id'] : $data['exp_id']; ?>">
    <?php
    $sc_vendor_name = '';
    $purchasereturn =  PurchaseReturn::model()->findByPk($data["return_id"]);
    $company = Company::model()->findByPk($data['company_id']);
    if (!empty($data['subcontractor_id']) && isset($data['scid'])) {
        $sc_model = Subcontractor::model()->findByPk($data['scid']);
        $sc_vendor_name = $sc_model['subcontractor_name'];
    } elseif (!empty($data['vendor_id'])) {
        $sc_vendor_name = $data['vname'];
    }
    ?>
    <td data-status = "<?php echo $data['reconciliation_status']; ?>">
    <?php if ($data['subcontractor_id'] == NULL) {  ?>

        <?php
        if ((isset(Yii::app()->user->role) && ((in_array('/expenses/expensesdelete', Yii::app()->user->menuauthlist)) || (in_array('/expenses/expensesedit', Yii::app()->user->menuauthlist))))) {
        ?>
                <?php
                if ($updatecount > 0) {
                    $requestmessage = "Already an update request is pending";
                    ?>
                    <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
                    <div class="popover-content hide">
                        <ul class="tooltip-hiden">
                            <li><span id="<?php echo $index; ?>" class="pending-icon fa fa-question" title="<?php echo $requestmessage; ?>"></span></li>
                        </ul>
                    </div>
                    <?php
                } else {
                    $payDate    = date("Y-m-d", strtotime($data["date"]));
                    $company    = Company::model()->findByPk($data["company_id"]);
                    
                    $updateDays = ($company->company_updateduration) - 1;
                    $editDate   = date('Y-m-d ', strtotime($payDate . ' + ' . $updateDays . ' days'));
                    $addedDate   = date("Y-m-d 23:59:59", strtotime($data["date"]));
                    $currentDate = date("Y-m-d");
                    if (Yii::app()->user->role == 1) {

                    ?>
                        <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
                        <div class="popover-content hide">
                            <ul class="tooltip-hiden">
                                <li><button data-id="<?php echo ($data["approval_status"] == 0) ? $data['ref_id'] : $data['exp_id']; ?>" class="btn btn-xs btn-default generate_voucher margin-right-5">View</button></li>
                                <?php if ($count['count'] > 0) { ?>
                                    <li>
                                        <i class="fa fa-clock-o text-danger" aria-hidden="true" title="<?php echo $count['count'] ?> edit request are pending"></i>
                                    </li>
                                <?php } ?>
                                <?php
                                if ((in_array('/expenses/expensesedit', Yii::app()->user->menuauthlist))) {
                                ?>
                                    <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default row-daybook  margin-right-5">Edit</button></li>
                                <?php } ?>
                                <?php
                                if ((in_array('/expenses/expensesdelete', Yii::app()->user->menuauthlist))) {
                                ?>
                                    <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default delete_row  margin-right-5">Delete</button></li>
                                <?php } ?>
                                
                                <?php                                
                                if($data["petty_payment_approval"] == 0) { ?>
                                    <li><button data-id="<?php echo ($data["approval_status"] == 0) ? $data['ref_id'] : $data['exp_id']; ?>" class="btn btn-xs btn-default delete_row ">Payment Approve</button></li>
                                <?php } ?>
                            </ul>
                        </div>
                        <?php
                        // }
                    } else {
                        if (isset($company->company_updateduration) && !empty($company->company_updateduration)) {
                            if ($editDate >= $currentDate) {
                        ?>
                                <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
                                <div class="popover-content hide">
                                    <ul class="tooltip-hiden">
                                        <li><button data-id="<?php echo ($data["approval_status"] == 0) ? $data['ref_id'] : $data['exp_id']; ?>" class="btn btn-xs btn-default generate_voucher  margin-right-5">View</button></li>
                                        <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default row-daybook  margin-right-5">Edit</button></li>
                                        <?php if ($confirmcount == 0) { ?>
                                            <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default delete_confirmation  margin-right-5">Delete</button></li>
                                        <?php } else { ?>
                                            <li><button id="<?php echo $index; ?>" data-id="<?php //echo $deleteconfirm["deletepending_id"]; ?>" class="btn btn-xs btn-default delete_restore">Restore</button></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            <?php
                            }
                        } else {
                            if ($currentDate <= $addedDate) {
                            ?>
                                <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
                                <div class="popover-content hide">
                                    <ul class="tooltip-hiden">
                                        <li><button data-id="<?php echo ($data["approval_status"] == 0) ? $data['ref_id'] : $data['exp_id']; ?>" class="btn btn-xs btn-default generate_voucher  margin-right-5">View</button></li>
                                        <?php
                                        if ((in_array('/expenses/expensesedit', Yii::app()->user->menuauthlist))) {
                                        ?>
                                            <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default row-daybook  margin-right-5">Edit</button></li>
                                        <?php } ?>
                                        <?php
                                        if ((in_array('/expenses/expensesdelete', Yii::app()->user->menuauthlist))) {
                                        ?>
                                            <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default delete_row  margin-right-5">Delete</button></li>
                                        <?php } ?>
                                        <li>
                                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        </li>
                                        <?php

                                        ?>
                                    </ul>
                                </div>
                <?php
                            }
                        }
                    }
                }
                ?>
            
        <?php } ?>

    <?php } else { ?>
       &nbsp;
        <?php
    } ?>
    </td>
    <td><b>#<?php echo ($data["approval_status"] == 0) ? $data['ref_id'] : $data['exp_id']; ?></b> </td>
    <td class="pro_back wrap <?php echo ($approval_status == 0) ? 'bg_pending' : ''; ?>" id="<?php echo "pname" . $index; ?>" <?php echo $pettybgcolor ?>><?php echo $company['name'] ? $company['name'] : ""; ?></td>
    <td><?php echo $data["pname"] ? $data["pname"] : ""; ?></td>
    <td id="<?php echo "billno" . $index; ?>"><a target="_blank" href="<?php echo Yii::app()->createAbsoluteUrl('bills/billview', array('id' => $data['bill_id'])) ?>"><?php echo $data["billno"] ? $data["billno"] : ""; ?></a></td>
    <td id="<?php echo "billno" . $index; ?>"><a target="_blank" href="<?php echo Yii::app()->createAbsoluteUrl('invoice/viewinvoice', array('invid' => $data['invoice_id'])) ?>"><?php echo $data["invoice_no"] ? $data["invoice_no"] : ""; ?></a></td>
    <td id="<?php echo "billno" . $index; ?>"><a target="_blank" href="<?php echo Yii::app()->createAbsoluteUrl('purchaseReturn/view', array('id' => $data['return_id'])) ?>"><?php echo (isset($purchasereturn->return_number)) ? $purchasereturn->return_number : ""; ?></a></td>
    <td id="<?php echo "typename" . $index; ?>"><?php echo $data["typename"] ? $data["typename"] : ""; ?></td>
    <td id="<?php echo "vname" . $index; ?>"><?php echo $sc_vendor_name ? $sc_vendor_name : ""; ?></td>
    <td id="<?php echo "exptype" . $index; ?>"><?php echo $expenseType; ?></td>
    <td id="<?php echo "exptype" . $index; ?>"><?php echo $employee; ?></td>
    <td id="<?php echo "bank" . $index; ?>"><?php echo $data["bankname"] ? $data["bankname"] : ""; ?></td>
    <td id="<?php echo "chequeno" . $index; ?>"><?php echo $data["cheque_no"] ? $data["cheque_no"] : ""; ?></td>
    <td id="<?php echo "desc" . $index; ?>"><?php echo $data["description"] ? $data["description"] : ""; ?></td>
    <td class="text-right" id="<?php echo "amount" . $index; ?>"><?php echo $data["expense_amount"] ? Controller::money_format_inr($data["expense_amount"], 2) : ""; ?></td>
    <td class="text-right"><span class="popover-test tax_amnt" data-toggle="popover" data-trigger="hover" data-placement="left" data-trigger="hover" type="button" data-html="true"><?php echo $gsttotal ? Controller::money_format_inr($gsttotal, 2) : ""; ?></span>
        <div class="popover-content hide">
            <div><span>SGST: (<?php echo $data["expense_sgstp"] ? $data["expense_sgstp"] : ""; ?>&#37;) <?php echo $data["expense_sgst"] ? Controller::money_format_inr($data["expense_sgst"], 2) : ""; ?></span></div>
            <div><span>CGST: (<?php echo $data["expense_cgst"] ? $data["expense_cgstp"] : ""; ?>&#37;) <?php echo $data["expense_cgst"] ? Controller::money_format_inr($data["expense_cgst"], 2) : ""; ?></span></div>
            <div><span>IGST: (<?php echo $data["expense_igst"] ? $data["expense_igstp"] : ""; ?>&#37;) <?php echo $data["expense_igst"] ? Controller::money_format_inr($data["expense_igst"], 2) : ""; ?></span></div>
        </div>
    </td>
    <!-- <td class="text-right" id="<?php echo "total" . $index; ?>"><?php echo $data["expense_amount"] ? Controller::money_format_inr($data["expense_amount"]+$gsttotal, 2) : ""; ?></td> -->
    <td class="text-right" id="<?php echo "total" . $index; ?>"><?php echo $data["amount"] ? Controller::money_format_inr($data["amount"], 2) : ""; ?></td>
    <td class="text-right" id="<?php echo "paid" . $index; ?>"><?php echo ($data["paid"] != 0) ? Controller::money_format_inr($data["paid"], 2) : ""; ?></td>
    <td class="text-right" id="<?php echo "tds" . $index; ?>"><?php echo $data["expense_tds"] ? Controller::money_format_inr($data["expense_tds"], 2) : ""; ?></td>
    <td id="<?php echo "receipttype" . $index; ?>"><?php echo $receiptType ? $receiptType : ""; ?></td>
    <td class="text-right" id="<?php echo "receipt" . $index; ?>"><?php echo $data["receipt"] ? Controller::money_format_inr($data["receipt"], 2) : ""; ?></td>
    <?php
    if ($data["billno"] == "" && $data["invoice_no"] == "") {
        if ($data["typename"] == "") {
    ?>

            <?php
            if ($data["payment_type"] == "" && $data["type"] == 72) { ?>

                <td id="<?php echo "purchasetype" . $index; ?>">Credit</td>
            <?php } else { ?>
                <td id="<?php echo "purchasetype" . $index; ?>"></td>
            <?php }
        } else {
            ?>
            <td id="<?php echo "purchasetype" . $index; ?>"><?php echo $purchaseType ? $purchaseType : ""; ?></td>

        <?php } ?>
    <?php
    } else { ?>
        <td id="<?php echo "purchasetype" . $index; ?>"><?php echo $purchaseType ? $purchaseType : ""; ?></td>
    <?php
    }
    ?>

    <td class="text-right" id="<?php echo "paidamount" . $index; ?>"><?php echo (($data["paidamount"] != 0) && ($data["purchase_type"] != 1)) ? Controller::money_format_inr($data["paidamount"], 2) : ""; ?></td>
   
    <input type="hidden" name="expenseid[]" id="expenseid<?php echo $index; ?>" value="<?php echo $data["exp_id"]; ?>" />
    <input type="hidden" name="rowid[]" id="rowid<?php echo $index; ?>" value="<?php echo $index; ?>" />
</tr>
<?php
if ($index == 0) {
?>
    <tfoot>
        <tr>
            <?php
            if ((isset(Yii::app()->user->role) && ((in_array('/expenses/expensesdelete', Yii::app()->user->menuauthlist)) || (in_array('/expenses/expensesedit', Yii::app()->user->menuauthlist))))) {
            ?>
            <th class=""></th>
            <?php } ?>
            <th class="total_amnt"></th>
            <th colspan="15" class="text-right total_amnt">Total Amount:</th>
            <!-- <th class="text-right"><?php echo Controller::money_format_inr($expamount, 2); ?></th>
            <th class="text-right"><?php echo Controller::money_format_inr($grandgst, 2); ?></th>
            <th class="text-right"><?php echo Controller::money_format_inr($grandtotal, 2); ?></th> -->
            <th class="text-right"><?php echo Controller::money_format_inr($paid, 2); ?></th>
            <th class="text-right"><?php echo Controller::money_format_inr($tdsamount, 2); ?></th>
            <th></th>
            <th class="text-right"><?php echo Controller::money_format_inr($receipt, 2); ?></th>
            <th></th>
            <th class="text-right"><?php echo Controller::money_format_inr($beneficiary_amt, 2); ?></th><!-- $paid - $tdsamount -->
            
        </tr>
    </tfoot>
<?php } ?>
<style>
    .popover {
        white-space:nowrap;
    }
</style>