<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/purchase.css">
<?php
$companyArray = explode(",", Yii::app()->user->company_ids);
$condition = "";
foreach ($companyArray as $company) {
    $condition .= "FIND_IN_SET('" . $company . "',company_id) OR ";
}
$condition = substr($condition, 0, -3);
?>

<div class="container" id="project">
    <?php
    if (!isset($_GET['exportpdf'])) { ?>
        <div class="expenses-heading header-container">
            <h3>Project Payment Report</h3>
            <?php
            if (isset($model->pid)) {
                $params = "&exportpdf=1&project_id=" . $model->pid; ?>
                <a class="btn btn-info" href="<?php echo Yii::app()->request->requestUri . $params; ?>"
                    style="text-decoration: none; color: white;">
                    <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
                </a>
            <?php } ?>
        </div>

        <div class="page_filter custom-form-style">
            <form id="projectform" action="<?php $url = Yii::app()->createAbsoluteUrl("projects/paymentReport"); ?>"
                method="POST">
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-3">
                        <label for="company">Company</label>
                        <?php
                        echo CHtml::dropDownList('project_id', 'project_id', CHtml::listData(Projects::model()->findAll(array(
                            'select' => array('pid, name'),
                            'order' => 'name',
                            'condition' => '' . $condition . '',
                            'distinct' => true
                        )), 'pid', 'name'), array('empty' => 'Select Project', 'style' => 'padding:2px 0px;', 'class' => 'form-control', 'id' => 'project_id', 'options' => array($model->pid => array('selected' => true))));
                        ?>
                    </div>
                    <div class="form-group col-xs-12 col-sm-3 text-sm-left text-right">
                        <label class="d-sm-block d-none">&nbsp;</label>
                        <div>
                            <?php echo CHtml::submitButton('GO', array('id' => 'btSubmit', 'class' => 'btn btn-sm btn-primary')); ?>
                            <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('paymentreport') . '"', 'class' => 'btn btn-sm btn-default')); ?>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    <?php } else {
        ?>
        <style>
            table {
                border-collapse: collapse;
                width: 100%;
                font-family: Arial, Helvetica, sans-serif;

            }

            .details,
            .info-table {
                border: 1px solid #ccc;
                padding: 2rem;
            }

            .details td,
            .details th,
            .info-table td,
            .info-table th {
                padding: 5px 10px;
                font-size: .85rem;
            }

            .info-table {
                margin-bottom: 10px;
            }

            .text-center {
                text-align: center;
            }

            .img-hold {
                width: 10%;
            }

            .companyhead {
                font-size: 1rem;
                font-weight: bold;
                margin-top: 0px;
                color: #789AD1;
            }

            .table-header th,
            .details {

                font-size: .75rem;
                padding-left: 4px;
                padding-right: 4px;
            }

            .medium-font {
                font-size: .85rem;
            }

            .addrow th,
            .addrow td {
                font-size: .75rem;
                border: 1px solid #333;
                border-collapse: collapse;
                padding-left: 4px;
                padding-right: 4px;
            }

            .text-right {
                text-align: right;
            }

            .w-50 {
                width: 50%;
            }

            .text-sm {
                font-size: 11px;
            }

            .dotted-border-bottom {
                border-bottom: 2px dotted black;
            }

            .w-100 {
                width: 100%;
            }

            .border-right {
                border-right: 1px solid #ccc;
            }

            .border-0 {
                border-right: 1px solid #fff !important;
            }

            .header-border {
                border-bottom: 1px solid #000;
            }
        </style>
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
        <?php
        if (!isset($_GET['exportpdf'])) {
            ?>
            <h3 class="text-center">Project Payment Report
                <?php echo !empty($model->pid) ? $model->name : ''; ?>
            </h3>
            <?php
        }
        ?>

        <?php
    } ?>
    <div id="contenthtml" class="contentdiv pdf_spacing">
        <div class="clearfix">
            <div class="pull-right">
            </div>
        </div>
        <?php
        if (!empty($model->pid))
            echo ' <h2 class="text-center">Payment Summary of ' . $model->name . '</h2>';
        if (isset($_GET['exportpdf'])) {
            ?>
            <div class="content">
                <div class="wrapper">
                    <div class="holder">
                        <?php
        }
        ?>
                    <?php if (!empty($model->pid)) { ?>
                        <div class="report-table-wrapper">
                            <table class="table total-table <?php echo isset($_GET['exportpdf']) ? "pdf-table" : ""; ?>">
                                <thead class="entry-table">
                                    <tr>
                                        <th rowspan="2" width="20px">SlNo</th>
                                        <th rowspan="2" width="30px">Payment Date</th>
                                        <th rowspan="2" width="30%">Description</th>
                                        <th rowspan="2" width="13%">Receipt No</th>
                                        <th colspan="3" class="text-center">Payment Details</th>
                                        <th rowspan="2" width="13%">Amount</th>
                                    </tr>
                                    <tr>
                                        <th width="10%">Payment Type</th>
                                        <th width="10%">Bank</th>
                                        <th width="13%">Transaction No</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    $total_received_amount = 0;
                                    foreach ($receipt_datas as $receipt) {
                                        $total_received_amount += $receipt->receipt;
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $i; ?>
                                            </td>
                                            <td>
                                                <?php echo date('d/M/y', strtotime($receipt->date)); ?>
                                            </td>
                                            <td>
                                                <?php echo $receipt->description; ?>
                                            </td>
                                            <td>
                                                <?php echo isset($receipt->invoice_id) ? $model->getInvoiceNumber($receipt->invoice_id) : ''; ?>
                                            </td>
                                            <?php
                                            if ($receipt->payment_type == 0) {
                                                echo " <td>Credit</td>";
                                                echo " <td></td>";
                                                echo " <td></td>";
                                            } elseif ($receipt->payment_type == 88) {
                                                echo " <td>Cheque/Online Payment</td>";
                                                echo "<td>" . $receipt->bank->bank_name . "</td>";
                                                echo "<td class='text-right'>" . $receipt->cheque_no . "</td>";
                                            } elseif ($receipt->payment_type == 89) {
                                                echo " <td>Cash</td>";
                                                echo " <td></td>";
                                                echo " <td></td>";
                                            } else {
                                                echo " <td></td>";
                                            }
                                            ?>
                                            <td class="text-right">
                                                <?php echo Yii::app()->controller->money_format_inr($receipt->receipt, 2); ?>
                                            </td>
                                        </tr>

                                        <?php
                                        $i++;
                                    }

                                    $total_paid_amount = $total_received_amount;

                                    ?>
                                    <tr>
                                        <td colspan="7" class="text-right no-border" bgcolor="#eee">Total Amount Received
                                        </td>
                                        <td class="text-right no-border nowrap" bgcolor="#eee"><b>
                                                <?php echo Yii::app()->controller->money_format_inr($total_received_amount, 2); ?>
                                            </b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" class="no-border">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="table-resposive">
                            <table class="table <?php echo isset($_GET['exportpdf']) ? "pdf-table" : ""; ?>">
                                <thead class="entry-table">
                                    <tr>
                                        <th width="20px">SlNo</th>
                                        <th width="30px">Invoice Date</th>
                                        <th width="28%">Invoice No</th>
                                        <th width="13%">Client</th>
                                        <th colspan="3" width="13%">Balance Amount</th>
                                        <th width="33%">Total Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    $total_received_amount = 0;
                                    foreach ($invoice_datas as $invoice) {
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $i; ?>
                                            </td>
                                            <td>
                                                <?php echo date('d/M/y', strtotime($invoice->date)); ?>
                                            </td>
                                            <td>
                                                <?php echo $invoice->inv_no; ?>
                                            </td>
                                            <td>
                                                <?php
                                                $pmodel = Projects::model()->findBypk($invoice->project_id);

                                                $clientname = $pmodel->client->name;
                                                echo $clientname; ?>
                                            </td>
                                            <td class="text-right" colspan="3">
                                                <?php
                                                $invoice_amount = $invoice->amount + $invoice->tax_amount + $invoice->round_off;
                                                echo Controller::money_format_inr(($invoice->amount + $invoice->tax_amount + ($invoice->round_off)), 2, 1); ?>
                                            </td>
                                            <td class="text-right">
                                                <?php
                                                $invoice_amount = $invoice->amount + $invoice->tax_amount + $invoice->round_off;
                                                echo Controller::money_format_inr(($invoice->amount + $invoice->tax_amount + ($invoice->round_off)), 2, 1); ?>
                                            </td>
                                        </tr>
                                        <?php
                                        $total_received_amount = $total_received_amount + $invoice_amount;
                                        $i++;
                                    }
                                    ?>
                                    <tr>
                                        <td colspan="7" class="text-right no-border" bgcolor="#eee">Total Invoice Amount
                                        </td>
                                        <td class="text-right no-border" bgcolor="#eee"><b>
                                                <?php echo Yii::app()->controller->money_format_inr($total_received_amount, 2); ?>
                                            </b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" class="no-border">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>


                        <div class="table-responsive">
                            <table class="table <?php echo isset($_GET['exportpdf']) ? "pdf-table" : ""; ?>">
                                <thead class="entry-table sticky-thead">
                                    <tr>
                                        <th width="20px">SlNo</th>
                                        <th width="30px">Quotation Date</th>
                                        <th width="28%">Description</th>
                                        <th width="13%">Quotation No</th>
                                        <th colspan="3" width="33%">&nbsp;</th>
                                        <th width="13%">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    $total_quotation_amount = 0;
                                    foreach ($quotation_datas as $quotation) {
                                        $total_quotation_amount += $quotation->total_project_cost;
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $i; ?>
                                            </td>
                                            <td>
                                                <?php echo date('d/M/y', strtotime($quotation->date)); ?>
                                            </td>
                                            <td></td>
                                            <td>
                                                <?php echo $quotation->inv_no;
                                                ; ?>
                                            </td>
                                            <td colspan="3">&nbsp;</td>

                                            <td class="text-right">
                                                <?php echo Yii::app()->controller->money_format_inr($quotation->total_project_cost, 2); ?>
                                            </td>
                                        </tr>

                                        <?php
                                        $i++;
                                    }
                                    ?>
                                    <tr>
                                        <td colspan="7" class="text-right no-border" bgcolor="#eee">Total Amount Of Contract
                                        </td>
                                        <td class="text-right no-border" bgcolor="#eee"><b>
                                                <?php echo Yii::app()->controller->money_format_inr($total_quotation_amount, 2); ?>
                                            </b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" class="no-border">&nbsp;</td>
                                    </tr>
                                    <!-- <tr>
                                        <td colspan="7" class="text-right no-border" bgcolor="#eee">Balance To Be Invoiced</td>
                                        <td class="text-right no-border" bgcolor="#eee"><b><?php echo Yii::app()->controller->money_format_inr($total_quotation_amount - $total_received_amount, 2); ?></b></td>
                                    </tr> -->
                                    <tr>
                                        <td colspan="7" class="text-right no-border" bgcolor="#eee">Balance To Be Paid</td>
                                        <td class="text-right no-border" bgcolor="#eee"><b>
                                                <?php echo Yii::app()->controller->money_format_inr($total_received_amount - $total_paid_amount, 2); ?>
                                            </b></td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php
                            if (isset($_GET['exportpdf'])) {
                                ?>
                            </div>


                        </div>
                    </div>
                    <?php
                            }
                            ?>
            </div>
            <?php
                    } else {
                        ?>
            <?php
            if (isset($_GET['exportpdf'])) {
                $pdf_class = 'table pdf-table';
                $summary = "";
            } else {
                $pdf_class = 'table';
                $summary = "<div>{summary}</div>";
            }
            ?>
            <div class="scroll-table">
                <?php
                $this->widget('zii.widgets.CListView', array(
                    'dataProvider' => $model->search(),
                    'itemView' => '_payment_report_listing',
                    'template' => '' . $summary . '<div id="parent"><table class="' . $pdf_class . '" id="fixTable">{items}</table></div>',
                ));
                ?>
            </div>
            <?php
                    }
                    ?>
    </div>
</div>
<style>
    #parent {
        max-height: 500px;
        border: 1px solid #ddd;
        border-right: 0px;
        border-top: 0px;
    }

    thead th,
    tfoot td {
        background: #eee;
    }

    .table {
        margin-bottom: 0px;
    }
</style>