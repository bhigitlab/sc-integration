<?php if ($index == 0): ?>
    <thead>
        <tr>
            <th></th>
            <th>Sl No</th>
            <th>Receipt Number</th>
            <th>Return Number</th>
            <th>Return Date</th>
            <th>Status</th>
            <th>Created Date</th>
            <th>Amount</th>                       
        </tr>
    </thead>
<?php endif; ?>

<tr class="<?php echo ($data->approval_status == 0) ? 'bg-warning' : 'bg-success'; ?>">
    <td>
        <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
        <div class="popover-content hide">
            <ul class="tooltip-hiden">                            
                <li><a href="<?php echo Yii::app()->createAbsoluteUrl('wh/warehouse/DefectView', array('id' => $data->return_id)); ?>" class="btn btn-xs btn-default">View</a></li>
                <?php if ($data->approval_status != 1): ?>
                    <li><a href="<?php echo Yii::app()->createAbsoluteUrl('wh/warehouse/DefectUpdate', array('id' => $data->return_id)); ?>" class="btn btn-xs btn-default">Edit</a></li>  
                    <li><button class="btn btn-xs btn-default approve_btn" id="<?php echo $data->return_id ?>">Approve</button></li> 
                <?php endif; ?>                           
            </ul>
        </div>
    </td>
    <td><?php echo $index + 1; ?></td>
    <td>
        <?php
        if ($data->receipt_id != NULL) {
            $wh = Warehousereceipt::model()->findByPk($data->receipt_id);
            echo $wh['warehousereceipt_no'];
        }
        ?>
    </td>
    <td><?php echo $data->return_number; ?></td>
    <td><?php echo date("d-m-Y", strtotime($data->return_date)); ?></td>
    <td><?php echo ($data->approval_status == 0) ? 'Pending' : 'Approved'; ?></td>
    <td><?php echo date("d-m-Y", strtotime($data->created_date)); ?></td>
    <td class="text-right"><?php echo ($data->return_amount != '') ? Controller::money_format_inr($data->return_amount, 2, 1) : '0.00'; ?></td>            
</tr>

<?php if ($index == $widget->dataProvider->itemCount - 1): ?>
    <tfoot>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th class="text-right"><?php echo ($amount != '') ? Controller::money_format_inr($amount, 2, 1) : '0.00'; ?></th>            
        </tr>
    </tfoot>
<?php endif; ?>

<script>
    $(document).ready(function () {
        $('[data-toggle="popover"]').popover();
    });
</script>