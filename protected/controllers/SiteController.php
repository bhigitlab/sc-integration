<?php

class SiteController extends Controller
{

    /**
     * Declares class-based actions.
     */
    public $defaultAction = 'login';

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function actionTest()
    {
        echo $this->appDate();
        echo $this->tableName('Projects', 1);
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */

    /*  public function actionIndex() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        if (!isset(Yii::app()->user->role))
            $this->redirect(array('/site/login'));


        $this->render('index');
    } */


    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                    "Reply-To: {$model->email}\r\n" .
                    "MIME-Version: 1.0\r\n" .
                    "Content-type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    public function actionviewReleaseNote($show = 0) {
        $app_root = YiiBase::getPathOfAlias('webroot');
        $filePath = $app_root . "/RELEASENOTE.md";
        if (file_exists($filePath)) {
            $date = date('Y-m-d H:i:s');
            $created_by = Yii::app()->user->id;
            $model = ReleaseNote::model()->findByPk(1);
            $fileModificationTime = filemtime($filePath);
            $userupdated = strtotime($model->created_at);
            if ($userupdated < $fileModificationTime) {
                $model->showed_users = NULL;
                $model->created_at = $date;
                $model->updated_at = $date;
                $model->created_by = $created_by;
                $model->updated_by = $created_by;
                $model->save();
            }
            $user_arr = array();
            if ($model->showed_users != NULL) {
                $user_arr = explode(',', $model->showed_users);
            }
            if (!in_array($created_by, $user_arr)) {
                $show = 1;
                $user_arr[] = $created_by;
                $model->showed_users = implode(',', $user_arr);
                $model->updated_at = $date;
                $model->updated_by = $created_by;
                $model->save();
            }
            if ($show) {
                $markdownContent = file_get_contents($filePath);
                require_once(Yii::getPathOfAlias('application.vendors.erusev.parsedown') . '/Parsedown.php');
                $parsedown = new Parsedown();
                $htmlContent = $parsedown->text($markdownContent);
                $this->layout = false;
                echo $htmlContent;
                Yii::app()->end();
            }
        }
    }


    public function getGoogleApiURL()
    {

        //Insert your cient ID and secret 
        //You can get it from : https://console.developers.google.com/
        $client_id = Yii::app()->params['client_id'];
        $client_secret = Yii::app()->params['client_secret'];
        $redirect_uri = Yii::app()->createAbsoluteUrl('users/googleauth');

        /*         * ********************************************
          Make an API request on behalf of a user. In
          this case we need to have a valid OAuth 2.0
          token for the user, so we need to send them
          through a login flow. To do this we need some
          information from our API console project.
         * ********************************************** */

        spl_autoload_unregister(array('YiiBase', 'autoload')); //Diable Default YII framewor's autoload class

        require(__DIR__ . '/../extensions/libraries/Google/autoload.php');
        $client = new Google_Client();
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);
        $client->setRedirectUri($redirect_uri);
        $client->addScope("email");
        $client->addScope("profile");


        /*         * **********************************************
          When we create the service here, we pass the
          client to it. The client then queries the service
          for the required scopes, and uses that when
          generating the authentication URL later.
         * ********************************************** */
        $service = new Google_Service_Oauth2($client);

        /*         * **********************************************
          If we have a code back from the OAuth 2.0 flow,
          we need to exchange that with the authenticate()
          function. We store the resultant access token
          bundle in the session, and redirect to ourself.
         */

        if (isset($_GET['code'])) {
            $client->authenticate($_GET['code']);
            Yii::app()->user->setState("access_token",$client->getAccessToken());
            header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
            exit;
        }

        /*         * **********************************************
          If we have an access token, we can make
          requests, else we generate an authentication URL.
         * ********************************************** */
        if (isset(Yii::app()->user->access_token) && Yii::app()->user->access_token) {
            $client->setAccessToken(Yii::app()->user->access_token);
        } else {
            $authUrl = $client->createAuthUrl();
        }

        spl_autoload_register(array('YiiBase', 'autoload'));

        if (isset($authUrl)) {
            return $authUrl;
        }
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {

        $this->layout = "login";
        $global_user_id = NULL;
        if (!Yii::app()->user->isGuest) {

           // $this->menupermissions();

            if (Yii::app()->user->role == 4) {
                $this->redirect(array('projects/projectreport'));
            } else  if (Yii::app()->user->role == 5 || Yii::app()->user->role == 3) {
                $this->redirect(array('DailyReport/create'));
            } else {
                $this->redirect(array('dashboard/index'));
            }
        }

        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_GET['switch_token']) && isset($_SERVER['HTTP_REFERER'])) {
            $switch_token = $_GET['switch_token'];
            if (strpos($_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST']) !== false) {
                $hostname = $_SERVER['HTTP_HOST'];
                $url_expiry_time = time() + (30 - time() % 30);
                $current_app = '/accounts';
                $token_condition_in_db = "md5(concat('{$hostname}','{$current_app}', '{$url_expiry_time}', userid)) = '{$switch_token}'";
                $landing_user = Users::model()->find($token_condition_in_db);

                if ($landing_user !== null) {
                    $_POST['LoginForm'] = array();
                    if (defined('LOGIN_USER_TABLE') && LOGIN_USER_TABLE != '') {
                        $global_user_id = $_POST['LoginForm']['global_user_id'] = $landing_user['global_user_id'];
                    }
                    $_POST['LoginForm']['username'] = $landing_user['username'];
                    $_POST['LoginForm']['password'] = $landing_user['password'];
                }
            }
        }
        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            if (defined('LOGIN_USER_TABLE') && LOGIN_USER_TABLE != '') {
                $model->global_user_id = $global_user_id;
            }
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
               // $this->menupermissions();

                //    $this->redirect(Yii::app()->user->returnUrl);
                if (Yii::app()->user->role == 4) {
                    $this->redirect(array('projects/projectreport'));
                } else  if (Yii::app()->user->role == 5 || Yii::app()->user->role == 3) {
                    $this->redirect(array('DailyReport/create'));
                } else {
                    $this->redirect(array('dashboard/index'));
                }
            }
        }

        $googleAuthURL = $this->getGoogleApiURL();

        // display the login form
        $this->render('login', array('model' => $model, 'googleauthurl' => $googleAuthURL));
    }

    
    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        unset(Yii::app()->user->menuall);
        unset(Yii::app()->user->menuauth);
        unset(Yii::app()->user->menuguest);
        unset(Yii::app()->user->menuauthlist);
        Yii::app()->session->clear();

        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionDbsync()
    {
        $import = 0;
        if (isset($_POST['submit'])) {
            $import = 1;
        }

        $this->render('dbsync', array('import' => $import));
    }
    /*public function actionGetreq(){
        $res = array();
        $data = json_decode(file_get_contents("php://input"));
        //echo "<pre>";print_r($data);exit;
        foreach($data as $key=>$v){
            $result = Yii::app()->dblive->createCommand("$v")->execute();
            //if($result){
                $res[] = $key;
            //}
	}
        echo json_encode($res);
        
    }*/

    public function actionApplicationlog($reset = 0)
    {

        if ($reset == 1) {
            $applog = ('protected/runtime/application.log');

            $logfile = fopen($applog, "w") or die("Unable to open file!");
            $txt = "";
            fwrite($logfile, $txt);
            fclose($logfile);

            $this->redirect(array('/site/applicationlog'));
            exit;
        }

        $lines = file("protected/runtime/application.log");
        $log = '';
        foreach ($lines as $line_num => $line) {
            $log .= $line . "\n";
        }
        $this->render('applicationlog', array('log' => $log));
    }

    public function actionJppms()
    {
        //session_start();
        $model = new LoginForm;
        $id_dec = '';
        $id = Yii::app()->getRequest()->getQuery('id');
        $mainuser_id = Yii::app()->getRequest()->getQuery('main'); //echo urldecode($id);exit;
        if ($id) {

            $key = 'url encryption';


            $data = base64_decode($id);
            $datamain = base64_decode($mainuser_id);
            $iv = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));

            $decrypted = rtrim(
                mcrypt_decrypt(
                    MCRYPT_RIJNDAEL_128,
                    hash('sha256', $key, true),
                    substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
                    MCRYPT_MODE_CBC,
                    $iv
                ),
                "\0"
            );
            $decryptedmain = rtrim(
                mcrypt_decrypt(
                    MCRYPT_RIJNDAEL_128,
                    hash('sha256', $key, true),
                    substr($datamain, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
                    MCRYPT_MODE_CBC,
                    $iv
                ),
                "\0"
            );
            $user = Users::model()->findByPk($decrypted);
            if (!empty($user)) {
                $model->autologin($decrypted, $decryptedmain);
                $_SESSION['userid'] = $decrypted;
                Yii::app()->user->setState("userid",$decrypted);
                // echo Yii::app()->user->id.'----'.Yii::app()->user->mainuser_id;exit; 
                if (!isset(Yii::app()->user->role))
                    $this->redirect(array('/site/login'));


                //$this->menupermissions();  // Modified by kripa on 04-10-2016 

                if (Yii::app()->user->role == 4) {
                    $this->redirect(array('projects/projectreport'));
                } else  if (Yii::app()->user->role == 5 || Yii::app()->user->role == 3) {
                    $this->redirect(array('DailyReport/create'));
                } else {
                    $this->redirect(array('dashboard/index'));
                }

                // Yii::app()->user->setState('company_id', 1);  
                // $this->redirect(array('dashboard/index'));
            }
        } else {
            $this->redirect(array('/site/login'));
        }
    }

    public function actionRedirect($type)
    {
        if (defined('LOGIN_USER_TABLE') && LOGIN_USER_TABLE != '') {
            $query1 = "SELECT system_access_types "
                . "FROM `global_users` WHERE  global_user_id = '" . Yii::app()->user->id . "'";
            $command1 = Yii::app()->db->createCommand($query1);
            $result1 = $command1->queryRow();
        } else {
            $query1 = "SELECT system_access_types "
                . "FROM `hrms_employee` WHERE  emp_id = '" . Yii::app()->user->id . "'";
            $command1 = Yii::app()->db->createCommand($query1);
            $result1 = $command1->queryRow();
        }
        // echo '<pre>';print_r($result1);exit;
        $allowed_apps = explode(',', $result1['system_access_types']);
        if (in_array($type, $allowed_apps)) {
            $hostname = $_SERVER['HTTP_HOST'];
            $url_expiry_time = time() + (30 - time() % 30);
            $encrypted_token = md5($hostname . '/' . strtolower($type) . $url_expiry_time . Yii::app()->user->id);
            $id = Yii::app()->user->id;
            $query = "SELECT url FROM global_settings WHERE access_types = '" . $type . "'"; // Fetch prefix from global settings table.
            $command = Yii::app()->db->createCommand($query);
            $result = $command->queryRow();
            $url = $result['url'];
            if ($url)
                $this->redirect($url . '&switch_token=' . $encrypted_token);
        } else {
            if (Yii::app()->user->role == 4) {
                $this->redirect(array('projects/projectreport'));
            } else  if (Yii::app()->user->role == 5 || Yii::app()->user->role == 3) {
                $this->redirect(array('DailyReport/create'));
            } else {
                $this->redirect(array('dashboard/index'));
            }
        }
    }
}
