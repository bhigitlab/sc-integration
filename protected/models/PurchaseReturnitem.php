<?php

/**
 * This is the model class for table "{{purchase_returnitem}}".
 *
 * The followings are the available columns in table '{{purchase_returnitem}}':
 * @property integer $returnitem_id
 * @property integer $return_id
 * @property string $returnitem_description
 * @property double $returnitem_quantity
 * @property string $returnitem_unit
 * @property double $returnitem_rate
 * @property double $returnitem_amount
 * @property double $returnitem_taxpercent
 * @property integer $category_id
 * @property string $remark
 * @property double $returnitem_taxamount
 * @property double $returnitem_discountpercent
 * @property double $returnitem_cgst
 * @property double $returnitem_cgstpercent
 * @property double $returnitem_sgst
 * @property double $returnitem_sgstpercent
 * @property double $returnitem_igst
 * @property double $returnitem_igstpercent
 * @property double $returnitem_discountamount
 * @property string $created_date
 */
class PurchaseReturnitem extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{purchase_returnitem}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('return_id, returnitem_description, returnitem_quantity, returnitem_unit, returnitem_rate, returnitem_amount, returnitem_taxpercent, returnitem_taxamount, returnitem_discountpercent, returnitem_discountamount, created_date', 'required'),
			array('return_id, category_id', 'numerical', 'integerOnly'=>true),
			array('returnitem_quantity, returnitem_rate, returnitem_amount, returnitem_taxpercent, returnitem_taxamount, returnitem_discountpercent, returnitem_cgst, returnitem_cgstpercent, returnitem_sgst, returnitem_sgstpercent, returnitem_igst, returnitem_igstpercent, returnitem_discountamount', 'numerical'),
			array('returnitem_unit', 'length', 'max'=>20),
			array('remark', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('returnitem_id, return_id, returnitem_description, returnitem_quantity, returnitem_unit, returnitem_rate, returnitem_amount, returnitem_taxpercent, category_id, remark, returnitem_taxamount, returnitem_discountpercent, returnitem_cgst, returnitem_cgstpercent, returnitem_sgst, returnitem_sgstpercent, returnitem_igst, returnitem_igstpercent, returnitem_discountamount, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'returnitem_id' => 'Returnitem',
			'return_id' => 'Return',
			'returnitem_description' => 'Returnitem Description',
			'returnitem_quantity' => 'Returnitem Quantity',
			'returnitem_unit' => 'Returnitem Unit',
			'returnitem_rate' => 'Returnitem Rate',
			'returnitem_amount' => 'Returnitem Amount',
			'returnitem_taxpercent' => 'Returnitem Taxpercent',
			'category_id' => 'Category',
			'remark' => 'Remark',
			'returnitem_taxamount' => 'Returnitem Taxamount',
			'returnitem_discountpercent' => 'Returnitem Discountpercent',
			'returnitem_cgst' => 'Returnitem Cgst',
			'returnitem_cgstpercent' => 'Returnitem Cgstpercent',
			'returnitem_sgst' => 'Returnitem Sgst',
			'returnitem_sgstpercent' => 'Returnitem Sgstpercent',
			'returnitem_igst' => 'Returnitem Igst',
			'returnitem_igstpercent' => 'Returnitem Igstpercent',
			'returnitem_discountamount' => 'Returnitem Discountamount',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('returnitem_id',$this->returnitem_id);
		$criteria->compare('return_id',$this->return_id);
		$criteria->compare('returnitem_description',$this->returnitem_description,true);
		$criteria->compare('returnitem_quantity',$this->returnitem_quantity);
		$criteria->compare('returnitem_unit',$this->returnitem_unit,true);
		$criteria->compare('returnitem_rate',$this->returnitem_rate);
		$criteria->compare('returnitem_amount',$this->returnitem_amount);
		$criteria->compare('returnitem_taxpercent',$this->returnitem_taxpercent);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('remark',$this->remark,true);
		$criteria->compare('returnitem_taxamount',$this->returnitem_taxamount);
		$criteria->compare('returnitem_discountpercent',$this->returnitem_discountpercent);
		$criteria->compare('returnitem_cgst',$this->returnitem_cgst);
		$criteria->compare('returnitem_cgstpercent',$this->returnitem_cgstpercent);
		$criteria->compare('returnitem_sgst',$this->returnitem_sgst);
		$criteria->compare('returnitem_sgstpercent',$this->returnitem_sgstpercent);
		$criteria->compare('returnitem_igst',$this->returnitem_igst);
		$criteria->compare('returnitem_igstpercent',$this->returnitem_igstpercent);
		$criteria->compare('returnitem_discountamount',$this->returnitem_discountamount);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PurchaseReturnitem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
