<?php
/* @var $this SubcontractorController */
/* @var $model Subcontractor */

$this->breadcrumbs=array(
	'Subcontractors'=>array('index'),
	$model->subcontractor_id=>array('view','id'=>$model->subcontractor_id),
	'Update',
);

/*$this->menu=array(
	array('label'=>'List Subcontractor', 'url'=>array('index')),
	array('label'=>'Create Subcontractor', 'url'=>array('create')),
	array('label'=>'View Subcontractor', 'url'=>array('view', 'id'=>$model->subcontractor_id)),
	array('label'=>'Manage Subcontractor', 'url'=>array('admin')),
);*/
?>
<!--<div class="modal-dialog modal-lg" id="projectform" >
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
			<h4 class="modal-title">Edit Subcontractor</h4>
		</div>
		<?php //echo $this->renderPartial('_form', array('model'=>$model)); ?>
	</div>
	
</div>-->
<div class="entries-wrapper">
    <div class="row">
        <div class="col-xs-12">
            <div class="heading-title">Edit Subcontractor</div>
            <div class="dotted-line"></div>
        </div>
    </div>
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>