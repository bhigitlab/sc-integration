<table class="table mt">
    <thead>
        <th>SI No</th>
        <th>Description</th>
        <th>Type</th>
        <th>Quantity</th>
        <th>Unit</th>
        <th>Rate</th>
        <th>Length</th>
        <th>Width</th>
        <th>Height</th>
        <th>Amount</th>
        <th></th>
    </thead>
    <tbody>
        <?php
        $i = 1;
        foreach ($sub_items as $sub_item) {
        ?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $sub_item->item_description; ?></td>
                <td><?php echo $sub_item->getType($sub_item->item_type); ?></td>
                <td><?php echo $sub_item->quantity; ?></td>
                <td><?php echo $sub_item->unit; ?></td>
                <td><?php echo  Yii::app()->controller->money_format_inr($sub_item->rate, 2); ?></td>
                <td><?php echo $sub_item->length; ?></td>
                <td><?php echo $sub_item->width; ?></td>
                <td><?php echo $sub_item->height; ?></td>
                <td><?php echo  Yii::app()->controller->money_format_inr($sub_item->amount, 2); ?></td>
                <td>
                    <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="left" type="button" data-html="true" style="cursor: pointer;"></span>
                    <div class="popover-content hide">
                        <ul class="tooltip-hiden">
                            <li><button id="<?php echo $sub_item->id; ?>" class="btn btn-xs btn-default edit_sub_item">Edit</button></li>
                            <li><button id="<?php echo $sub_item->id; ?>" class="btn btn-xs btn-default delete_item">Delete</button></li>

                        </ul>
                    </div>
                </td>
            </tr>
        <?php
            $i++;
        }
        ?>

    </tbody>
    <tfoot>
        <th colspan="10">Total</th>
        <th><?php echo $main_item->total_amount; ?></th>
    </tfoot>
</table>
<script>
    $(document).ready(function() {
        $(".popover-test").popover({
            html: true,
            content: function() {
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function(e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function(e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function(e) {
            $('[data-toggle=popover]').each(function() {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });


    });
</script>