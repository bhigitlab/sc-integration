<div id="previous_details" style="display: none;"></div>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'item_update_form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => false,
    ),
        ));
?>
<?php echo CHtml::hiddenField('cat_id', $model->category_id, array('id' => 'cat_id')); ?>
<?php echo CHtml::hiddenField('cat_name', '', array('id' => 'cat_name')); ?>
<?php echo CHtml::hiddenField('master_id', $model->master_id, array('id' => 'hiddenmasterid')); ?>
<?php echo CHtml::hiddenField('quotation_id', $model->id, array('id' => 'hiddenqid')); ?>
<?php echo CHtml::hiddenField('item_id', $model->item_id, array('id' => 'hiddenitem')); ?>
<h4><b>Edit Item</b></h4>

<?php if (1 == $type) { ?>
    <div class="row" id="container"> 
        <div class="row-margin">
            <div class="col-md-1">
                <label>Category Label</label>
                <?php echo $form->textField($model, 'category_label', array('class' => 'form-control require', 'id' => 'category_label', 'readonly' => true)); ?>
            </div>
            <div class="col-md-1">
                <label>Work Type Label</label>
                <?php
                $newQuery = 'work_type_id =' . $model->work_type . ' AND quotation_category_id =' . $model->category_id . '';
                echo $form->dropDownList(
                        $model, 'worktype_label', CHtml::listData(QuotationItemMaster::model()->findAll(array('condition' => $newQuery)), 'worktype_label', 'worktype_label'), array('class' => 'form-control js-example-basic-single field_change invoice_add require worktype_label', 'empty' => '-Select Worktype Label-', 'id' => 'worktype_label', 'style' => 'width:100%')
                );
                ?>
            </div>
            <?php if ($model->work_type == 1) { ?>
                <div class="col-md-1">
                    <label>Shutter Work</label>
                    <?php echo $form->textField($model, 'shutterwork_description', array('class' => 'form-control require', 'id' => 'shutterwork_description', 'readonly' => true)); ?>
                </div>
                <div class="col-md-1">
                    <label>Carcass Box Work</label>
                    <?php echo $form->textField($model, 'carcass_description', array('class' => 'form-control require', 'id' => 'caracoss_description', 'readonly' => true)); ?>
                </div>
                <?php echo $form->hiddenField($model, 'shutterwork_material', array('class' => 'form-control require', 'id' => 'shutterwork_material')); ?>
                <?php echo $form->hiddenField($model, 'shutterwork_finish', array('class' => 'form-control require', 'id' => 'shutterwork_finish')); ?>
                <?php echo $form->hiddenField($model, 'carcass_material', array('class' => 'form-control require', 'id' => 'carcass_material')); ?>
                <?php echo $form->hiddenField($model, 'carcass_finish', array('class' => 'form-control require', 'id' => 'carcass_finish')); ?>

            <?php } else { ?>
                <div class="col-md-1">
                    <label>Description</label>
                    <?php echo $form->textField($model, 'description', array('class' => 'form-control require', 'id' => 'description', 'readonly' => true)); ?>
                </div>
                <?php
                if ($model->work_type == 4) {
                    echo $form->hiddenField($model, 'material', array('class' => 'form-control require', 'id' => 'material'));
                    echo $form->hiddenField($model, 'finish', array('class' => 'form-control require', 'id' => 'finish'));
                }
            }
            ?>

            <div class="col-md-1">
                <label>Unit</label>
                <?php
                echo $form->dropDownList(
                        $model, 'unit', CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name'), array('class' => 'form-control js-example-basic-single field_change invoice_add require', 'empty' => '-Select Unit-', 'id' => 'unit', 'style' => 'width:100%')
                );
                ?>
            </div>
            <div class="col-md-1">
                <label>Quantity</label>
    <?php echo $form->textField($model, 'quantity', array('class' => 'form-control allownumericdecimal require', 'onchange' => 'getamount()', 'id' => 'qty')); ?>
                <?php echo $form->error($model, 'quantity'); ?>
            </div>
            <div class="col-md-2">
                <label>Quantity Nos</label>
    <?php echo $form->textField($model, 'quantity_nos', array('class' => 'form-control allownumericdecimal require')); ?>
                <?php echo $form->error($model, 'quantity_nos'); ?>
            </div>
            <div class="col-md-1">
                <label>Mrp</label>
    <?php echo $form->textField($model, 'mrp', array('class' => 'form-control allownumericdecimal require', 'id' => 'mrp')); ?>
                <?php echo $form->error($model, 'mrp'); ?>
            </div>
            <div class="col-md-2">
                <label>Amount After Discount</label>
    <?php echo $form->textField($model, 'amount_after_discount', array('class' => 'form-control allownumericdecimal require', 'id' => 'amount_after_discount')); ?>
    <?php echo $form->error($model, 'amount_after_discount'); ?>
            </div>

        </div>
    </div>


            <?php }if (2 == $type) { ?>
    <div class="row" id="container_extrawork">
        <div class="row-margin">
            <div class="col-md-1">
                <?php
                echo $form->labelEx($model, 'Extra Work');
                ?>
                <?php echo CHtml::hiddenField('work_type', '', array('id' => 'hiddenInput')); ?>
            </div>

            <div class="col-md-2">
                <?php echo $form->textField($model, 'description', array('class' => 'form-control', 'placeholder' => 'description')); ?>
                <?php echo $form->error($model, 'description'); ?>
            </div>
            <div class="col-md-1">
                <?php
                echo $form->dropDownList($model, 'unit', CHtml::listData(Unit::model()->findAll(), 'id', 'unit_name'), array('class' => 'form-control js-example-basic-single field_change invoice_add require', 'empty' => '-Select Unit-', 'style' => 'width:100%')
                );
                ?>
            </div>
            <div class="col-md-1">
                <?php echo $form->textField($model, 'quantity', array('class' => 'form-control allownumericdecimal', 'placeholder' => 'qty')); ?>
                <?php echo $form->error($model, 'quantity'); ?>
            </div>
            <div class="col-md-1">
                <?php echo $form->textField($model, 'quantity_nos', array('class' => 'form-control allownumericdecimal', 'placeholder' => 'qty nos')); ?>
                <?php echo $form->error($model, 'quantity_nos'); ?>
            </div>
            <div class="col-md-2">
                <?php echo $form->textField($model, 'mrp', array('class' => 'form-control allownumericdecimal', 'placeholder' => 'MRP')); ?>
                <?php echo $form->error($model, 'mrp'); ?>
            </div>
            <div class="col-md-2">
    <?php echo $form->textField($model, 'amount_after_discount', array('class' => 'form-control allownumericdecimal', 'placeholder' => 'amt after disc')); ?>
    <?php echo $form->error($model, 'amount_after_discount'); ?>
            </div>
        </div>
    </div>
    <?php } ?>  

<div class="panel-footer save-btnHold text-center button-panel">
    <?php echo CHtml::Button($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info', 'id' => 'updateitem')); ?>
    <?php
    if (!$model->isNewRecord) {
        echo CHtml::ResetButton('Close', array('onclick' => 'closeupdate(this,event);', 'class' => 'btn'));
    } else {
        echo CHtml::ResetButton('Reset', array('style' => 'margin-right:3px;', 'class' => 'btn btn-default'));
        echo CHtml::ResetButton('Close', array('onclick' => 'closeupdate(this,event);', 'class' => 'btn'));
    }
    ?>
</div>

<?php $this->endWidget(); ?>


<script>
    function closeupdate() {
        $("#item_update_form").hide();
    }
    function getamount() {
        var item_id = $("#hiddenitem").val();
        var qty = $("#qty").val();
        $.ajax({
            url: "<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getamountdetails'); ?>",
            data: {
                item_id: item_id,
                qty: qty
            },
            type: "POST",
            dataType: 'json',
            success: function (result) {
                if (result != '') {
                    $("#amount_after_discount").val(result.profit_amount);
                    var mrp_after_discount = parseInt(result.profit_amount) + parseInt((result.profit_amount * result.discount) / 100);
                    $("#mrp").val(mrp_after_discount);
                }
            }
        });
    }
    $(document).on('change', '.worktype_label', function () {
        var element = $(this);
        var worktypelabel = element.val();
        var cat_id = $("#cat_id").val();
        var worktypeid = '<?php echo $model->work_type ?>';
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl('salesquotation/SalesQuotation/getdatasofworktype'); ?>',
            type: 'GET',
            dataType: 'json',
            data: {
                cat_id: cat_id,
                worktypelabel: worktypelabel,
                worktypeid: worktypeid
            },
            success: function (result) {

                if (result.status == 1) {
                    $('#hiddenitem').val(result.item_id);
                    $('#previous_details').html(result.html);
                    $("#previous_details").show();
                    $("#pre_fixtable").tableHeadFixer();
                    getamount();
                } else {
                    $('#previous_details').html(result.html);
                    $("#pre_fixtable").tableHeadFixer();
                }

                if (worktypeid == 1) {
                    $('#shutterwork_description').val(result.shutterdesc);
                    $('#caracoss_description').val(result.carcassdesc);
                    $('#shutterwork_material').val(result.shuttermaterial);
                    $('#shutterwork_finish').val(result.shutterfinish);
                    $('#carcass_material').val(result.carcasmaterial);
                    $('#carcass_finish').val(result.carcassfinish);
                } else if (worktypeid == 4) {
                    $('#material').val(result.material);
                    $('#finish').val(result.finish);
                    $('#description').val(result.description);
                } else {
                    $('#description').val(result.description);
                }
            }
        });
    });
    $('#updateitem').click(function () {
        var data = $("#item_update_form").serialize();
        $.ajax({
            url: '<?php echo Yii::app()->createAbsoluteUrl("salesquotation/salesQuotation/updatequotationitems") ?>',
            data: data,
            type: "POST",
            dataType: 'json',
            success: function (result) {
                $('html, body').animate({
                    scrollTop: $("#content").position().top - 100
                }, 500);
                if (result.success == '1') {

                    $(".alert-success").show().html(result.msg).delay(3000).fadeOut();
                    location.reload();
                } else {
                    $(".alert-danger").show().html(result.msg).delay(3000).fadeOut();
                }
                ;

            }
        });
        return false;
    });

</script>