<?php

/**
 * This is the model class for table "{{material_items}}".
 *
 * The followings are the available columns in table '{{material_items}}':
 * @property integer $id
 * @property integer $material_entry_id
 * @property integer $material
 * @property integer $quantity
 * @property string $unit
 *
 * The followings are the available model relations:
 * @property MaterialEntries $materialEntry
 * @property Materials $material0
 */
class MaterialItems extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{material_items}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('material_entry_id, material, quantity, unit', 'required'),
			array('material_entry_id, material, quantity', 'numerical', 'integerOnly'=>true),
			array('unit', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, material_entry_id, material, quantity, unit', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'materialEntry' => array(self::BELONGS_TO, 'MaterialEntries', 'material_entry_id'),
			'material0' => array(self::BELONGS_TO, 'Materials', 'material'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'material_entry_id' => 'Material Entry',
			'material' => 'Material',
			'quantity' => 'Quantity',
			'unit' => 'Unit',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('material_entry_id',$this->material_entry_id);
		$criteria->compare('material',$this->material);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('unit',$this->unit,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MaterialItems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
