<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/purchase.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/moment.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<script src="<?php echo Yii::app()->baseUrl . '/ckeditor/ckeditor.js'; ?>"></script>
<script>
	var shim = (function() {
		document.createElement('datalist');
	})();
</script>

<script>
	$(function() {
		$("#datepicker").datepicker({
			dateFormat: 'dd-mm-yy'
		}).datepicker("setDate", new Date());
		$("#delivery_date").datepicker({
			dateFormat: 'dd-mm-yy'
		});
	});
</script>
<style>
	.base-data {
		display: none;
	}

	.addRow input.textbox {
		width: 200px;
		display: inline-block;
		margin-right: 10px;
	}
	.modal .panel.panel-gray {
		margin-bottom: 0px !important;
	}
</style>

<?php
$tblpx = Yii::app()->db->tablePrefix;
$purchase_details = Yii::app()->db->createCommand("SELECT purchase_no FROM jp_purchase WHERE purchase_no IS NOT NULL AND company_id=" . Yii::app()->user->company_id . " ORDER BY p_id DESC LIMIT 0,1")->queryRow();
$lastpurchase_no = $purchase_details['purchase_no'];
if (is_numeric($lastpurchase_no)) {
	$lastpurchase_no = $lastpurchase_no + 1;
} else {
	$p_no = substr($lastpurchase_no, 0, -1);
	$last = substr($lastpurchase_no, -1);
	if (is_numeric($last)) {
		$last_no = $last + 1;
	} else {
		$last_no = $last . '1';
	}
	$lastpurchase_no = $p_no . $last_no;
}

$company_details = Company::model()->findByPk($model->company_id);
if ($company_details->auto_purchaseno == 1) {
	$readonly = "readonly=true";
} else {
	$readonly = "";
}
?>
<div class="container">

<div class="modal fade" id="myModal1" role="dialog">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-dialog modal-lg">          
            <div class="modal-content">        
                <div class="modal-body p-0"></div>                
            </div>      
        </div>
    </div>

    <div class="modal fade" id="myModal2" role="dialog">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-dialog modal-lg">          
            <div class="modal-content">        
                <div class="modal-body p-0"></div>                
            </div>      
        </div>
    </div>

    <div class="modal fade" id="myModal3" role="dialog">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-dialog modal-lg">          
            <div class="modal-content">        
                <div class="modal-body p-0"></div>                
            </div>      
        </div>
    </div>

    <div class="modal fade" id="myModal4" role="dialog">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-dialog modal-lg">          
            <div class="modal-content">        
                <div class="modal-body p-0"></div>                
            </div>      
        </div>
    </div>

	<div class="loading-overlay">
		<span class="fa fa-spinner fa-3x fa-spin"></span>
	</div>
	<div class="entries-wrapper">
		
			<div class="row">
                    <div class="col-xs-12">
                      <div class="heading-title">Update PO Details</div>
                      <div class="dotted-line"></div>
                    </div>
                  </div>
			<div id="msg_box"></div>

		<!-- <div class="invoicemaindiv"> -->
			<form id="pdfvals1" method="post" action="<?php echo $this->createAbsoluteUrl('purchase/addpurchase'); ?>">
				<input type="hidden" name="remove" id="remove" value="">
				<input type="hidden" name="purchase_id" id="purchase_id" value="<?php echo $model->p_id; ?>">
					<div class="row">
					
						<div class="form-group col-xs-12 col-md-2 select2-width">
							
							<label>COMPANY : 
									<a class="hide" data-toggle="modal" href="<?php echo  Yii::app()->createAbsoluteUrl('company/create&layout=1'); ?>" data-target="#myModal1">Add New</a></label>
								<?php
								$user = Users::model()->findByPk(Yii::app()->user->id);
								$arrVal = explode(',', $user->company_id);
								$newQuery = "";
								foreach ($arrVal as $arr) {
									if ($newQuery) $newQuery .= ' OR';
									$newQuery .= " FIND_IN_SET('" . $arr . "', id)";
								}
								$companyInfo = Company::model()->findAll(array('condition' => $newQuery));
								?>
								<select name="company_id" class="inputs target company change_val" id="company_id" style="width:190px">
									<option value="">Choose Company</option>
									<?php
									foreach ($companyInfo as $key => $value) {
									?>
										<option value="<?php echo $value['id'] ?>" <?php echo ($value['id'] == $model->company_id) ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
									<?php
									}
									?>
								</select>
							
						</div>

						<div class="form-group col-xs-12 col-md-2 select2-width">
							
							<label>PROJECT : 
									<a class="hide" data-toggle="modal" href="<?php echo  Yii::app()->createAbsoluteUrl('projects/create&layout=1'); ?>" data-target="#myModal2">Add New</a></label>
								<select name="project" class="inputs target project change_val" id="project" style="width:190px">
									<option value="">Choose Project</option>
									<?php
									foreach ($project as $key => $value) {
									?>
										<option value="<?php echo $value['pid']; ?>" <?php echo ($value['pid'] == $model->project_id) ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
									<?php
									}
									?>
								</select>
							
						</div>

						<div class="form-group col-xs-12 col-md-2 select2-width">
							
							<label>EXPENSE HEAD : 
									<a class="hide" data-toggle="modal" href="<?php echo  Yii::app()->createAbsoluteUrl('expensetype/create&layout=1'); ?>" data-target="#myModal3" class="myModal3">Add New</a></label>
								<select name="expense_head" class="inputs target expense_head change_val" id="expense_head" style="width:190px">
									<option value="">Choose Expense Head</option>
									<?php
									foreach ($expense_head as $value) {
									?>
										<option value="<?php echo $value['type_id']; ?>" <?php echo ($value['type_id'] == $model->expensehead_id) ? 'selected' : ''; ?>><?php echo $value['type_name']; ?></option>
									<?php
									}
									?>
								</select>
							
						</div>
						<div class="form-group col-xs-12 col-md-2 select2-width">
							
							<label>VENDOR : 
									<a class="hide" data-toggle="modal" href="<?php echo  Yii::app()->createAbsoluteUrl('vendors/create&layout=1'); ?>" data-target="#myModal4">Add New</a></label>
								<select name="vendor" class="inputs target vendor change_val" id="vendor" style="width:190px">
									<option value="">Choose Vendor</option>
									<?php
									foreach ($vendor as $key => $value) {
									?>
										<option value="<?php echo $value['vendorid']; ?>" <?php echo ($value['vendorid'] == $model->vendor_id) ? 'selected' : ''; ?>><?php echo $value['vendorname']; ?></option>
									<?php
									}
									?>

								</select>
							
						</div>
						<div class="form-group col-xs-12 col-md-2 select2-width">
							
								<label>DATE : </label>
								<input type="text" value="<?php echo ((isset($model->purchase_date) && $model->purchase_date != '') ? date("d-m-Y", strtotime($model->purchase_date)) : date("d-m-Y")); ?>" id="datepicker" class="txtBox date inputs target form-control change_val" name="date" placeholder="Please click to edit">
							
						</div>
						<div class="form-group col-xs-12 col-md-2 ">
							
								<label>PURCHSE NO : </label>
								<?php
								?>
								<input type="text" required value="<?php echo ((isset($model->purchase_no) && $model->purchase_no != '') ? $model->purchase_no : ''); ?>" class="txtBox inputs target check_type purchaseno form-control change_val" name="purchaseno" id="purchaseno" placeholder="" <?php echo $readonly; ?>>
							
						</div>
						<div class="form-group col-xs-12 col-md-2 ">
								<label>CONTACT NO : </label>
								<input type="text" required value="<?php echo ((isset($model->contact_no) && $model->contact_no != '') ? $model->contact_no : ''); ?>" class="txtBox inputs target check_type purchaseno form-control change_val" name="contact_no" id="contact_no" placeholder="">
								<div class="errorMessage"></div>
							
						</div>
						<div class="form-group col-xs-12 col-md-2 ">
							
								<label>EXPECTED DELIVERY DATE : </label>
								<input type="text" value="<?php echo ((isset($model->expected_delivery_date) && $model->expected_delivery_date != '') ? date("d-m-Y", strtotime($model->expected_delivery_date)) : date("d-m-Y")); ?>" id="delivery_date" class="txtBox  inputs target form-control change_val delivery_date" name="date" placeholder="Please click to edit">
							
						</div>
						<div class="form-group col-xs-12 col-md-2 ">
							
								<label>SHIPPING ADDRESS : </label>
								<textarea name="shipping_address" id="shipping_address" class="form-control change_val" rows="2" cols="20"><?= $model->shipping_address ?></textarea>
						</div>
							
						
						<div class="form-group col-xs-12 col-md-5">
							
								<label>DESCRIPTION : </label>
								<textarea name="po_description" id="po_description" rows="2" cols="20" class="form-control change_val" ><?= $model->purchase_description ?></textarea>
						</div>	
						<div class="form-group col-xs-12 col-md-3 margin-top-0 margin-bottom-0">
								<div class="form-check">
									<input type="checkbox" class="form-check-input change_val" <?php echo $model->inclusive_gst == 1 ? 'checked="checked"' : '' ?>id="inclusive" name="inclusive">
									<label class="form-check-label" for="inclusive">Inclusive of GST
									</label>
								</div>
							</div>
					
					</div>
				<div class="clearfix"></div>
				<br>
				<div id="msg"></div>

				<div id="previous_details" style="position: fixed;top: 94px; left: 70px; right: 70px; z-index: 2; max-width:1150px; margin:0 auto;"></div>

				
					<div class="row">
						<div class="col-xs-12">
						<div class="heading-title">Add/Edit Purchase Item</div>
						<div class="dotted-line"></div>
						</div>
                  	</div>
					<div class="row d-flex">
                    <div class="col-xs-12 col-md-9">
                        <div class="row">
                            <input type = "hidden" id ="mrId" value="<?php echo isset($mrId)?$mrId:''?>">
                            <?php 
                            $project_id='';
                            if(!empty($mrId)){
                                if(!empty($itemId)){
                                    $table_id = MaterialRequisitionItems::Model()->findByPk($itemId);
                                    if(!empty($table_id)){
                                        $mr_val= MaterialRequisition::Model()->findByPk($table_id->mr_id);
                                        if(!empty($mr_val)){
                                            $project_id=$mr_val->project_id;
                                        }
                                    }
                                }
                                
                                
                                ?>
                                <input type = "hidden" id ="project" value="<?php echo isset($project_id)?$project_id:''?>">
                            <?php }
                            ?>
                            <input type = "hidden" id ="itemId" value="<?php echo isset($itemId)?$itemId:''?>">
                            <input type="hidden" class="date" value="<?php echo isset($purchasemodel->purchase_date) ? date('d-m-Y', strtotime($purchasemodel->purchase_date)) : ''; ?>">


                            <input type="hidden" name="purchase_id" id="purchase_id" value="<?php echo isset($purchasemodel->p_id)? $purchasemodel->p_id:0 ; ?>">
                            
                                
                                <div id="select_div " class="purchaseitem select2-width  form-group col-xs-12 col-md-4">
                                        <label>Specification:</label>
                                        <select class="txtBox form-control  specification description selecboxwidth" id="description" name="description[]" >
                                            <option value="">Select one</option>
                                            <?php
                                            foreach ($specification as $key => $value) {
                                            ?>
                                                <option value="<?php echo $value['id']; ?>"><?php echo $value['data']; ?></option>
                                            <?php } ?>
                                            <option value="other">Other</option>
                                        </select>
                                        <input type="hidden" id="itemid_val" name="itemid_val" value="">

                               
                                </div>
                            
                                <div class="purchaseitem form-group col-xs-12 col-md-2">
                                    <label class="text-nowrap">HSN Code </label>
                                    <p id="hsn_code" class="hsn_code form-group col-xs-12 col-md-2">&nbsp;&nbsp;</p>
                                </div>
                            
                            <div class="purchaseitem remark form-group col-xs-12 col-md-2">
                                
                                    <label>Remarks:</label>
                                    <input type="text" class="txtBox form-control" id="remarks" name="remark[]" placeholder="Remark" />
                                
                            </div>
                            <div class="purchaseitem quantity_div form-group col-xs-12 col-md-2">
                               
                                    <label>Quantity:</label>
                                    <input type="text" class="inputs target txtBox quantity allownumericdecimal form-control max-w-100" id="quantity" name="quantity[]" placeholder="" /></td>
                                    <div id="remaining_estimated_qty" class="padding-box estimation-details"></div>
                                    <input type="hidden" id="remaining_estimation_qty" name="remaining_estimation_qty" value="">
                               
                            </div>
                            <div class="purchaseitem form-group col-xs-12 col-md-2">
                               
                                    <label>Units:</label>
                                    <select class="txtBox  form-control item_unit w-100 mobile-w-200" id="item_unit" name="item_unit">

                                    </select>
                                    <input type="hidden" name="unitval" id="unitval">
                                    <!-- <div id="item_unit" class="item_unit padding-box">&nbsp;&nbsp;</div> -->
                               
                            </div>

                        </div>
                        <div class="row">
                           
                            <div class="purchaseitem base-data form-group col-xs-12 col-md-2">
                                <label>Base Quantity:</label>
                                <input type="text" class="inputs target txtBox base_qty allownumericdecimal form-control" id="base_qty" name="base_qty[]" placeholder="" />
                            </div>
                            <div class="purchaseitem base-data form-group col-xs-12 col-md-2">
                                <label>Base Unit:</label>
                                <input type=" text" class="inputs target txtBox base_unit allownumericdecimal form-control" id="base_unit" name="base_unit[]" placeholder="" readonly="true" />
                            </div>
                            <div class="purchaseitem base-data form-group col-xs-12 col-md-2">
                                <label>Base Rate:</label>
                                <input type="text" class="inputs target txtBox base_rate allownumericdecimal form-control" id="base_rate" name="base_rate[]" placeholder="" readonly="true" />
                            </div>
                        </div>
                        <div class="row">
                             <div class="purchaseitem form-group col-xs-12 col-md-2">
                                
                                    <label>Rate:</label>
                                    <input type="text" class="inputs target txtBox rate allownumericdecimal form-control max-w-100" id="rate" name="rate[]" placeholder="" />
                                    <div id="estimated_price" class="padding-box estimation-details"></div>
                                
                            </div>
                            <div class="purchaseitem form-group col-xs-12 col-md-2">
                               
                                    <label>Discount amount :</label>
                                    <input type="text" class="inputs target small_class txtBox sgst allownumericdecimal form-control calculation" id="dis_amount" name="dis_amount" placeholder=""  />
                                    <span id="disp" class="value-label-bottom">0.00</span>(%)
                               
                            </div>
							<div class="clearfix d-tab-block"></div>
                            <div class="purchaseitem form-group col-md-2" id="select_div">
                               
                                    <label>Tax Slab:</label>
                                    <?php
                                    $datas = TaxSlabs::model()->getAllDatas();
                                    ?>
                                    <select class="form-control txtBox tax_slab" id="tax_slab" name="tax_slab[]">
                                        <option value="">Select one</option>
                                        <?php
                                        foreach ($datas as  $value) {
                                            if ($value['set_default'] == 1) {
                                                echo ' <option data-id="' . $value['id'] . '" value="' . $value['tax_slab_value']  . '" selected>' . $value['tax_slab_value'] . '%</option>';
                                            } else {
                                                echo ' <option  data-id="' . $value['id'] . '" value="' . $value['tax_slab_value'] . '">' . $value['tax_slab_value'] . '%</option>';
                                            }
                                        }
                                        ?>
                                    </select>
											<span class="d-mobile-block">&nbsp;</span>
                                
                            </div>
                            <div class="purchaseitem gsts form-group col-md-2">
                                
                                    <label>SGST :</label>
                                    <input type="text" class="inputs target small_class txtBox sgstp allownumericdecimal form-control gst_percentage" id="sgstp" name="sgstp" placeholder="" />
                                    <span id="sgst_amount" class=" value-label-bottom">0.00</span>
                               
                            </div>
                            <div class="purchaseitem gsts form-group col-md-2">
                                
                                    <label>CGST :</label>
                                    <input type="text" class="inputs target txtBox cgstp small_class allownumericdecimal form-control" id="cgstp" name="cgstp" placeholder="" />
                                    <span id="cgst_amount" class="value-label-bottom">0.00</span>
                                
                            </div>
                            <div class="purchaseitem gsts form-group col-md-2">
                                
                                    <label>IGST :</label>
                                    <input type="text" class="inputs target txtBox igstp  small_class allownumericdecimal form-control" id="igstp" name="igstp" placeholder="" />
                                    <span id="igst_amount" class="value-label-bottom">0.00</span>
                               
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3 bottom-flex">
                        <div class="form-group pull-right total-value-wrapper margin-top-18">
                            <div class="input-info margin-bottom-10">
                            <label class="final-section-label w-100p" >Amount:</label>
                            <span id="item_amount" class="total-value-label w-100p">0.00</span>
                            </div>
                            <div class="input-info margin-bottom-10">
                                <label class="final-section-label w-100p">Discount Amount :</label>
                                <span id="disc_amount" class="total-value-label w-100p">0.00</span>
                            </div>
                            <div class="input-info margin-bottom-10">
                                <label class="final-section-label w-100p">Tax Amount: </label>
                                <span id="tax_amount" class="total-value-label w-100p">0.00</span>
                            </div>
                            <div class="input-info ">
                                <label class="final-section-label w-100p">Total Amount: </label>
                                <span id="total_amount" class="total-value-label w-100p">0.00</span>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="row">
                <div class="purchaseitem form-group col-xs-12 col-md-6">
                        <span id="item_baseunit_data" class="d-none text-blue"><i>Base Unit : </i><span class="txtBox item_unit_span" id="item_unit_additional_data"> </span></span>&nbsp;&nbsp;
                        <span id="item_conversion_data" class="d-none text-blue"><i>Conv Fact : </i><span class="txtBox item_unit_span" id="item_unit_additional_conv"> </span></span>
                </div>
                <div class="form-group col-xs-12 col-md-6 text-right">
                     <input type="button" class="item_save btn btn-info" id="0" value="Save">
                </div>
                <div class="blink hide"><span>Item already added</span></div>
                
                <div class="toastmessage" style="top: 58px;right: 6px;width: 350px;position: fixed;"></div>
            </div>
				
		<!-- </div> -->
		<div class="margin-horizontal-10 margin-top-20">
                    <div class="horizontal-scroll">
                      <table border="1" class="table main-table" id="main-table">
				<thead  class="entry-table">
					<tr>
						<th class="text_align">Sl.No</th>
						<th class="text_align">Specification</th>
						<th class="text_align">HSN Code</th>
						<th class="text_align">Quantity</th>
						<th class="text_align">Unit</th>
						<th class="text_align">Rate</th>
						<th class="text_align">Base Quantity</th>
						<th class="text_align">Base Unit</th>
						<th class="text_align">Base Rate</th>
						<th class="text_align">Amount</th>
						<th>Tax Slab (%)</th>
						<th>SGST Amount</th>
						<th>SGST (%)</th>
						<th>CGST Amount</th>
						<th>CGST (%)</th>
						<th>IGST Amount</th>
						<th>IGST (%)</th>
						<th>Discount (%)</th>
						<th class="text_align">Discount Amount</th>
						<th>Tax Amount</th>
						<th class="text_align">Total</th>
						<th></th>
					</tr>
				</thead>
				<tbody class="addrow">
					<?php

					$i = 1;
					$grand_total = ($model->total_amount + $total_tax_amount['tax_amount']) - $total_discount_value['discount_amount'];
					$item_amount = $item_total = 0;
					foreach ($item_model as $new) {
						$item_amount = ($new['amount'] + $new['tax_amount']) - $new['discount_amount'];
						$item_total += $item_amount;
						$tblpx = Yii::app()->db->tablePrefix;
						$desc_sql = "SELECT * FROM {$tblpx}specification"
							. " WHERE id=" . $new['category_id'] . "";
						$desctiptions  = Yii::app()->db->createCommand($desc_sql)->queryRow();
						$parent_sql = "SELECT * FROM {$tblpx}purchase_category"
							. " WHERE id='" . $desctiptions['cat_id'] . "'";
						$parent_category = Yii::app()->db->createCommand($parent_sql)->queryRow();
						if ($desctiptions['brand_id'] != NULL) {
							$brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $desctiptions['brand_id'] . "")->queryRow();
							$brand = '-' . $brand_details['brand_name'];
						} else {
							$brand = '';
						}
						if ($new['category_id'] == 0) {

							$spc_details = $new['remark'];
							$spc_id = 'other';
						} else {
							$spc_details = $parent_category['category_name'] . $brand . ' - ' . $desctiptions['specification'];
							$spc_id = $new['category_id'];
						}

					?>


						<tr class="tr_class" id="item_<?php echo $new['item_id']; ?>">

							<td>
								<div id="item_sl_no"><?php echo $i; ?></div>
							</td>
							<td>
								<div class="item_description" id="<?php echo $spc_id; ?>"> <?php echo $spc_details; ?></div>

							</td>
							<td>
								<div class="unit" id="unit"> <?php echo $new['hsn_code']; ?></div>
							</td>
							<?php
							$class = "";
							$style = "";
							if ($new['permission_status'] == 'not_approved') {
								$class = "rate_highlight";
								$style = "style='cursor:pointer'";
							} else {
								$class = "";
								$style = "";
							}
							$approval_type =Yii::app()->db->createCommand("SELECT estimation_approval_type FROM jp_purchase_items WHERE item_id=" . $new['item_id'])->queryRow();
							//echo "<pre>s$class".$new['item_id'];print_r($approval_type);
							?>
							<td class=" <?php echo (isset($approval_type['estimation_approval_type']) && in_array($approval_type['estimation_approval_type'], [2, 3]))?$class:''; ?>">
								<div class="text-right" id="unit"><?php echo $new['quantity']; ?></div>
							</td>
							<td>
								<div class="unit" id="unit"> <?php echo $new['unit']; ?></div>
							</td>
							<td class=" <?php echo (isset($approval_type['estimation_approval_type']) && in_array($approval_type['estimation_approval_type'], [2]))?'':$class; ?>" <?php echo $style; ?> id='<?php echo $new['item_id']; ?>'>
								<div class="text-right" id="rate"><?php echo number_format($new['rate'], 2, '.', ''); ?></div>
							</td>
							<input type="hidden" id="itemidval" name="itemidval" value="<?php echo  $new['item_id'] ?>">
							<td class="text-right">
								<div class="" id="base_qty"><?php echo  $new['base_qty']; ?></div>
							</td>
							<td>
								<div class="unit" id="base_unit"> <?php echo $new['base_unit']; ?></div>
							</td>
							<input type="hidden" id="itemidval" name="itemidval" value="">
							<td class="text-right <?php $class . $style ?>" id="<?php echo $new['item_id'] ?>">
								<div class="" id=" base_rate"><?php echo  number_format($new['base_rate'], 2, '.', ''); ?></div>
							</td>
							<td>
								<div class="amount text-right" id="amount" style="max-width: -3px !important;"> <?php echo number_format($new['amount'], 2, '.', ''); ?></div>
							</td>
							<td class="text-right"><?php echo $new['tax_slab']; ?></td>
							<td class="text-right"><?php echo number_format($new['sgst_amount'], 2); ?></td>
							<td class="text-right"><?php echo number_format($new['sgst_percentage'], 2, '.', ''); ?></td>
							<td class="text-right"><?php echo number_format($new['cgst_amount'], 2); ?></td>
							<td class="text-right"><?php echo number_format($new['cgst_percentage'], 2, '.', ''); ?></td>
							<td class="text-right"><?php echo number_format($new['igst_amount'], 2); ?></td>
							<td class="text-right"><?php echo number_format($new['igst_percentage'], 2, '.', ''); ?></td>

							<td class="text-right">
								<div class=""><?php echo number_format($new['discount_percentage'], 2, '.', ''); ?></div>
							</td>
							<td class="text-right">
								<div class=""><?php echo number_format($new['discount_amount'], 2, '.', ''); ?></div>
							</td>

							<td class="text-right"><?php echo number_format($new['tax_amount'], 2, '.', ''); ?></td>
							<td>
								<div class="amount text-right" id="amount" style="max-width: -3px !important;"> <?php echo number_format($item_amount, 2, '.', ''); ?></div>
							</td>
							<td width="40"><span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="left" type="button" data-html="true" style="cursor: pointer;"></span>
								<div class="popover-content hide">
									<ul class="tooltip-hiden">
										<li><a href="" id='<?php echo $new['item_id']; ?>' class="btn btn-xs btn-default removebtn">Delete</a></li>
										<li><a href="" id='<?php echo $new['item_id']; ?>' class="btn btn-xs btn-default edit_item">Edit</a></li>
										<?php if ($new['permission_status'] == 'not_approved' && Yii::app()->user->role == 1) { ?>
											<li>
												<a href="" id='<?php echo $new['item_id']; ?>' class="btn btn-xs btn-default approve_item approveoption_<?php echo $new['item_id']; ?>">
													<?php
													$approval_type_value = isset($approval_type['estimation_approval_type']) ? $approval_type['estimation_approval_type'] : null;
													if ($approval_type_value == 2) {
														echo 'Quantity Approve';
													} elseif ($approval_type_value == 3) {
														echo 'Rate & Quantity Approve';
													} else{
														echo 'Rate Approve';
													}
													?>
												</a>
											</li>
										<?php } ?>
									</ul>
								</div>
							</td>
						</tr>

					<?php
						$i++;
					}
					?>
				</tbody>

				<tfoot>
					<tr>
						<th colspan="18" class="text-right">TOTAL: </th>
						<th class="text-right">
							<div id="discount_total"><?php echo (!empty($total_discount_value)) ? number_format($total_discount_value['discount_amount'], 2) : '0.00'; ?></div>
						</th>
						<th class="text-right">
							<div id="tax_total"><?php echo (!empty($total_tax_amount)) ? number_format($total_tax_amount['tax_amount'], 2) : '0.00'; ?></div>
						</th>
						<th class="text-right">
							<div id="amount_total"><?php echo ($item_total == '') ? '0.00' : number_format($item_total, 2, '.', ''); ?></div>
						</th>
						<th></th>
					</tr>

					<tr>
						<th colspan="21" class="text-right">GRAND TOTAL:
							<span id="grand_total">
								<?php echo ($grand_total != '') ? number_format($grand_total, 2, '.', '') : '0.00'; ?>
							</span>
						</th>
						<th></th>

					</tr>
					<tr>
						<th colspan="21" class="text-right">ADDITIONAL CHARGE:						
							<?php echo ($additional_charge != '') ? number_format($additional_charge, 2, '.', '') : '0.00'; ?>						
						</th>
						<th></th>

					</tr>
				</tfoot>
			</table>	
					</div>	
		</div>						
		<div id="fix_table">
			
		</div>
		<?php
		$this->renderPartial("_purchase_additional_charge");
		?>
	</div>
	<input type="hidden" name="final_amount" id="final_amount" value="<?php echo ($model->total_amount == '') ? '0.00' : number_format($model->total_amount, 2, '.', ''); ?>">

	<input type="hidden" name="item_amount_temp" id="item_amount_temp" value="0">

	<div style="padding-right: 0px;" class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6">

		<input type="hidden" value="<?php echo ((isset($subtot) && $subtot != '') ? $subtot : ''); ?>" class="txtBox pastweek" readonly=ture name="subtot" /></td>
		<input type="hidden" value="<?php echo ((isset($grand) && $grand != '') ? $grand : ''); ?>" class="txtBox pastweek grand" name="grand" readonly=true />
		<input type="hidden" name="previousvalue" id="previousvalue" value="0">


	</div>

	<div class="text-center mt" id="loader"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>

	<div class="text-center mt">
		<a id="mail_send" class="mail_send btn btn-primary" href="#" title="Mail Send"> Save</a>
	</div>
	</form>
</div>

<script>
	$(document).ready(function() {
		CKEDITOR.replace('po_description');
		var desc = '<?php echo $model->purchase_description; ?>';
		CKEDITOR.instances.po_description.setData(desc);
		CKEDITOR.instances['po_description'].on("blur", function() {
			$("#cke_po_description").removeClass('focus_input')
			var erroCount = $(".errorMessage").not(":empty").length;
		    if(erroCount ==0){
                savePurchaseOrder();
            }
		});
		CKEDITOR.instances['po_description'].on("focus", function(e) {
			$("#cke_po_description").addClass('focus_input');
			$('#shipping_address').removeClass('focus_input');
			$('#contact_no').removeClass('focus_input');
			e.stopPropagation();
		});
		$("#main-table").tableHeadFixer({
			'left': false,
			'foot': true,
			'head': true
		});

		$(".popover-test").popover({
			html: true,
			content: function() {
				return $(this).next('.popover-content').html();
			}
		});
		$('[data-toggle=popover]').on('click', function(e) {
			$('[data-toggle=popover]').not(this).popover('hide');
		});
		$('body').on('hidden.bs.popover', function(e) {
			$(e.target).data("bs.popover").inState.click = false;
		});
		$('body').on('click', function(e) {
			$('[data-toggle=popover]').each(function() {
				if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
					$(this).popover('hide');
				}
			});
		});

		function onlySpecialchars(str)
        {        
            var regex = /^\d{10}$/;            
            var message ="";      
            
            if(!str.match(regex)) {
                var message ="Not a valid Phone Number";    
            }  
            return message;        
        }
		$(document).on("blur", "#contact_no", function() {			
            var message =  onlySpecialchars(this.value);            
            $(this).siblings(".errorMessage").show().html(message);                        
            var erroCount = $(".errorMessage").not(":empty").length;			
		    if(erroCount ==0){
                savePurchaseOrder();
            }
            
        });

		$(document).ajaxComplete(function() {
			$(".popover-test").popover({
				html: true,
				content: function() {
					return $(this).next('.popover-content').html();
				}
			});

		});
	});

	$('.mail_send').hide();
	var purchase_id = <?php echo $model->p_id; ?>;
	var mail_status = '<?php echo $model->mail_status; ?>';
	var purchase_status = '<?php echo $model->purchase_status; ?>';
	if (mail_status === 'N' && purchase_status === 'permission_needed') {
		$('.mail_send').show();
	} else {
		$('.mail_send').hide();
	}

	$(document).ready(function() {
		$(".js-example-basic-single").select2();
		$(".project").select2();
		$(".vendor").select2();
		$(".company").select2();
		$(".expense_head").select2();
	});

	$(document).ready(function() {
		$('select').first().focus();
	});

	$("#expense_head").change(function() {
		var val = $(this).val();
		$("#vendor").html('<option value="">Select Vendor</option>');
		$.ajax({
			url: '<?php echo Yii::app()->createUrl('purchase/dynamicvendor'); ?>',
			method: 'POST',
			data: {
				exp_id: val
			},
			dataType: "json",
			success: function(response) {
				$("#vendor").html(response.html);
				var erroCount = $(".errorMessage").not(":empty").length;
				if(erroCount ==0){
					savePurchaseOrder();
				}
			}

		})
	})

	$("#project").change(function() {
		var val = $(this).val();
		$("#vendor").html('<option value="">Select Vendor</option>');
		$("#expense_head").html('<option value="">Select Expense Head</option>');
		$.ajax({
			url: '<?php echo Yii::app()->createUrl('purchase/dynamicexpensehead'); ?>',
			method: 'POST',
			data: {
				project_id: val
			},
			dataType: "json",
			success: function(response) {
				if(response.msg != ""){
                    alert(response.msg);
                }
				$("#shipping_address").val(response.address);
				if (response.status == 'success') {
					$("#expense_head").html(response.html);
					$("#vendor").html('<option value="">Select Vendor</option>');
				}
				var erroCount = $(".errorMessage").not(":empty").length;
				if(erroCount ==0){
					savePurchaseOrder();
				}
			}

		})
	})

	$("#company_id").change(function() {
		var val = $(this).val();
		$("#project").html('<option value="">Select Project</option>');
		$.ajax({
			url: '<?php echo Yii::app()->createUrl('purchase/dynamicproject'); ?>',
			method: 'POST',
			data: {
				company_id: val
			},
			dataType: "json",
			success: function(response) {
				if (response.auto_pono == 1) {
					$('#purchaseno').attr('readonly', true);
					$('#purchaseno').val(response.purchase_no);
				} else {
					$('#purchaseno').attr('readonly', false);
					$('#purchaseno').val('');
				}
				if (response.status == 'success') {
					$("#project").html(response.html);
				} else {
					$("#project").html(response.html);
				}
				var erroCount = $(".errorMessage").not(":empty").length;
				if(erroCount ==0){
					savePurchaseOrder();
				}
			}

		})
	})

	jQuery.extend(jQuery.expr[':'], {
		focusable: function(el, index, selector) {
			return $(el).is('button, :input, [tabindex]');
		}
	});

	$(document).on('keypress', 'input,select', function(e) {
		if (e.which == 13) {
			e.preventDefault();
			var $canfocus = $(':focusable');
			var index = $canfocus.index(document.activeElement) + 1;
			if (index >= $canfocus.length) index = 0;
			$canfocus.eq(index).focus();
		}
	});
</script>

<script>
	$(document).ready(function() {
		$().toastmessage({
			sticky: false,
			position: 'top-right',
			inEffectDuration: 1000,
			stayTime: 3000,
			closeText: '<i class="icon ion-close-round"></i>',
		});
		$(".remark").addClass('remark_edit');


	});

	$("#quantity, #rate,#dis_amount, #sgstp, #cgstp, #igstp").blur(function() {
		changeValues();
		if (($(this).hasClass("quantity")) || ($(this).hasClass("rate"))) {
			var type = 3;
			getconversionfactor(type);

		}
	});

	function changeValues() {
		var quantity = parseFloat($("#quantity").val());
		var rate = parseFloat($("#rate").val());
		var amount = quantity * rate;
		var new_amount = 0;
		var dis_amount = parseFloat($("#dis_amount").val());
		var sgst = parseFloat($("#sgstp").val());

		if (sgst > 100 || sgst < 0) {
			$().toastmessage('showErrorToast', "Percentage value should be between 0 and 100");
			$("#sgstp").val(0);
			sgst = 0;
		}

		var cgst = parseFloat($("#cgstp").val());
		if (cgst > 100 || cgst < 0) {
			$().toastmessage('showErrorToast', "Percentage value should be between 0 and 100");
			$("#cgstp").val(0);
			cgst = 0;
		}
		var igst = parseFloat($("#igstp").val());
		if (igst > 100 || igst < 0) {
			$().toastmessage('showErrorToast', "Percentage value should be between 0 and 100");
			$("#igstp").val(0);
			igst = 0;
		}
		if (isNaN(dis_amount)) dis_amount = 0;
		if (isNaN(amount)) amount = 0;
		new_amount = amount - dis_amount;
		if (isNaN(new_amount)) new_amount = 0;
		var sgst_amount = (sgst / 100) * new_amount;
		var cgst_amount = (cgst / 100) * new_amount;
		var igst_amount = (igst / 100) * new_amount;

		if (isNaN(sgst_amount)) sgst_amount = 0;
		if (isNaN(cgst_amount)) cgst_amount = 0;
		if (isNaN(igst_amount)) igst_amount = 0;
		var tax_amount = sgst_amount + cgst_amount + igst_amount;

		var total_amount = new_amount + tax_amount;

		var disp = (dis_amount / amount) * 100;
		if (isNaN(disp)) disp = 0;
		$("#disp").html(disp.toFixed(2));
		$("#disc_amount").html(dis_amount.toFixed(2));
		$("#item_amount").html(amount.toFixed(2));
		$("#sgst_amount").html(sgst_amount.toFixed(2));
		$("#cgst_amount").html(cgst_amount.toFixed(2));
		$("#igst_amount").html(igst_amount.toFixed(2));
		$("#tax_amount").html(tax_amount.toFixed(2));
		$("#total_amount").html(total_amount.toFixed(2));
	}



	$(function() {
		$(document).on('click', '.removebtn', function(e) {
			e.preventDefault();
			element = $(this);


			var item_id = $(this).attr('id');
			if (item_id == '0') {
				$('input[name="remove"]').val('removed');
				$(this).closest('tr').find('input[name="amount[]"]').each(function() {
					$(this).closest('tr').remove();
					var re = this.value;
					var v1 = $('input[name="subtot"]').val();
					var v2 = $('input[name="grand"]').val();
					if (re == '') {
						$('input[name="subtot"]').val(v1);
						$('input[name="grand"]').val(v2);
						$('#grand_total').text(v2.toFixed(2));
					} else {
						$('input[name="subtot"]').val(eval(v1) - eval(re));
						$('input[name="grand"]').val(eval(v2) - eval(re));
						$('#grand_total').text((eval(v2) - eval(re)).toFixed(2));
					}
				});
			} else {
				var answer = confirm("Are you sure you want to delete?");
				if (answer) {
					var item_id = $(this).attr('id');
					var $tds = $(this).closest('tr').find('td');
					var amount = $tds.eq(15).text();

					var re = amount;
					var j = parseFloat(amount)
					var v1 = $('input[name="subtot"]').val();
					var v2 = $('input[name="grand"]').val();
					var t = $('#grand_total').text();
					var gamount = parseFloat(t);
					if (j == '') {
						$('input[name="subtot"]').val(gamount);
						$('input[name="grand"]').val(gamount);
						$('#grand_total').text(gamount);
					} else {
						$('input[name="subtot"]').val((eval(gamount) - eval(j)).toFixed(2));
						$('input[name="grand"]').val((eval(gamount) - eval(j)).toFixed(2));
						$('#grand_total').text((eval(gamount) - eval(j)).toFixed(2));

					}
					var subtot = $('input[name="subtot"]').val();
					var grand = $('input[name="grand"]').val();
					var data = {
						'purchase_id': purchase_id,
						'item_id': item_id,
						'grand': grand,
						'subtot': subtot
					};
					$.ajax({
						method: "GET",
						async: false,
						data: {
							data: data
						},
						dataType: "json",
						url: '<?php echo Yii::app()->createUrl('purchase/removepurchaseitem'); ?>',
						success: function(result) {
							if (result.response == 'success') {
								$('.addrow').html(result.html);
								$('#final_amount').val(result.grand_total);
								$('#amount_total').text(result.grand_total);
								$('#discount_total').text(result.discount_total);
								$('#tax_total').text(result.tax_total);
								$('#grand_total').text(result.grand_total_amount);
								$().toastmessage('showSuccessToast', "" + result.msg + "");
								if (result.mail_send == 'Y') {
									$('.mail_send').show();
								} else {
									$('.mail_send').hide();
								}
							} else {
								$().toastmessage('showErrorToast', "" + result.msg + "");
							}
							$('#description').val('').trigger('change');
							$('#remarks').val('');
							var quantity = $('#quantity').val('');
							var unit = $('#item_unit').text('');
							var rate = $('#rate').val('');
							var amount = $('#item_amount').html('');
							$('#hsn_code').text('');
							$(".item_save").attr('value', 'Save');
							$(".item_save").attr('id', 0);
							$('#previousvalue').text('0');
							$('#description').select2('focus');
						}
					});

				} else {

					return false;
				}

			}

		});



		$(document).on("click", ".addcolumn, .removebtn", function() {
			$("table.table  input[name='sl_No[]']").each(function(index, element) {
				$(element).val(index + 1);
				$('.sl_No').html(index + 1);
			});
		});

		$(".inputSwitch span").on("click", function() {

			var $this = $(this);

			$this.hide().siblings("input").val($this.text()).show();

		});

		$(".inputSwitch input").bind('blur', function() {

			var $this = $(this);

			$(this).attr('value', $(this).val());

			$this.hide().siblings("span").text($this.val()).show();

		}).hide();

	});



	$(".allownumericdecimal").keydown(function(event) {

		if (event.shiftKey == true) {
			event.preventDefault();
		}

		if ((event.keyCode >= 48 && event.keyCode <= 57) ||
			(event.keyCode >= 96 && event.keyCode <= 105) ||
			event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
			event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
			var splitfield = $(this).val().split(".");
			if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
				event.preventDefault();
			}
		} else {
			event.preventDefault();
		}

		if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
			event.preventDefault();

	});



	$('.other').click(function() {
		if (this.checked) {
			$('#autoUpdate').fadeIn('slow');
			$('#select_div').hide();
		} else {
			$('#autoUpdate').fadeOut('slow');
			$('#select_div').show();
		}

	})


	$(document).on("change", ".other", function() {
		if (this.checked) {
			$(this).closest('tr').find('#autoUpdate').fadeIn('slow');
			$(this).closest('tr').find('#select_div').hide();
		} else {
			$(this).closest('tr').find('#autoUpdate').fadeOut('slow');
			$(this).closest('tr').find('#select_div').show();
		}
	});


	$(".specification").select2({
		ajax: {
			url: '<?php echo  Yii::app()->createAbsoluteUrl('purchase/testransaction'); ?>',
			type: "POST",
			dataType: 'json',
			delay: 0,
			data: function(params) {
				return {
					searchTerm: params.term
				};
			},
			processResults: function(response) {
    $('#previous_details').html(response.html);
    $("#previous_details").show();
    $("#pre_fixtable3").tableHeadFixer();
   // $('#item_unit').text(response.unit);
    $("#base_unit").val(response.base_unit);
    $('#hsn_code').text(response.hsn_code);
    if (response.unit == response.base_unit) {
        var resunit = response.base_unit;
		
        if (resunit == null) {
            $('select[id="item_unit"]').append('<option value="" selected></option>');
        } else {
            $('select[id="item_unit"]').append('<option value="' + response.unit + '" selected>' + response.units + '</option>');
        }

    } else {
		
        if (Array.isArray(response["unit"])) { // Check if response["unit"] is an array
          consolr.log("unit"+response["unit"]);
			$.each(response["unit"], function(key, value) { // Changed result to response
                if (response.updated_unit == value.value) { // Changed result to response
                    var selected = 'selected';
					consolr.log(value.value);
                } else {
                    var selected = '';
					consolr.log('sec hi');
                }
                $('select[id="item_unit"]').append('<option value="' + value.id + '" ' + selected + '>' + value.value + '</option>');
            });
        } else {
            var selected = '';
            if (response.base_unit == response.unit) {
                selected = 'selected';
				consolr.log("base"+response.base_unit);
				consolr.log("unit"+response.unit);

            }
			
            $('select[id="item_unit"]').append('<option value="' + response.unit + '" ' + selected + '>' + response.unit + '</option>');
        }
    }
    return {
        results: response.data
    };
},
cache: true
		}
	});

	$('.specification').change(function() {
		var element = $(this);
		var category_id = $(this).val();
		var purchase_id = $('#purchase_id').val();
		var itemid_val = $(this).siblings("#itemid_val").val();

		$('#quantity').val('');
		$('#rate').val('');
		$('#item_amount').text('0.00');
		$.ajax({
			url: '<?php echo  Yii::app()->createAbsoluteUrl('purchase/previoustransaction'); ?>',
			type: 'GET',
			dataType: 'json',
			data: {
				data: category_id,
				purchase_id: purchase_id,
				itemid_val: itemid_val

			},
			success: function(result) {
				if (result.status == 1) {
					$('#previous_details').html(result.html);
					$("#previous_details").show();
					$("#pre_fixtable").tableHeadFixer();
				} else {
					$('#previous_details').html(result.html);
					$("#pre_fixtable").tableHeadFixer();
				}

				$("#base_unit").val(result.base_unit);
				//$('select[id="item_unit"]').empty();
				var edit_status = $("#edit_status").val();
				if (edit_status == 1) {

					$.each(result["unit"], function(key, value) {
						if (result.updated_unit == value.value) {
							var selected = 'selected';
						} else {
							var selected = '';
						}
						$('select[id="item_unit"]').append('<option value="' + value.id + '" ' + selected + '>' + value.value + '</option>');
					});
				} else {

					if (result.unit == result.base_unit) {
						var resunit = result.base_unit;
						if (resunit == null) {
							$('select[id="item_unit"]').append('<option value="" selected></option>');
						} else {
							$('select[id="item_unit"]').append('<option value="' + result.unit + '" selected>' + result.units + '</option>');
						}
					} else {
						$.each(result["unit"], function(key, value) {
							if (result.base_unit == value.value) {
							
								var selected = 'selected';
							} else {
								var selected = '';
							}
							$('select[id="item_unit"]').append('<option value="' + value.id + '" ' + selected + '>' + value.value + '</option>');
						});
					}
				}
				//$('#item_unit').text(result.unit);
				$('#hsn_code').text(result.hsn_code);
				if (category_id == 'other') {
					$('.remark').css("display", "inline-block");
					$('#remarks').focus();
				} else if (category_id == '') {
					$('.js-example-basic-single').select2('focus');
					$('.remark').css("display", "none");
				} else {
					$('#quantity').focus();
					$('.remark').css("display", "none");
				}
				if (result.exist == 1) {
					$('.blink').removeClass('hide');
					$('.blink').addClass('d-inline');
				} else {
					$('.blink').addClass('hide');
				}


			}

		})

	})
</script>


<script>
	$(document).on("change", ".change_val", function(event) {
		var erroCount = $(".errorMessage").not(":empty").length;
		if(erroCount ==0){
            savePurchaseOrder();
        }
		var element = $(this);

	});

	function savePurchaseOrder() {
		var purchase_id = $("#purchase_id").val();
		var default_date = $(".date").val();
		var purchaseno = $("#purchaseno").val();
		var expense_head = $('#expense_head').val();
		var contact_no = $("#contact_no").val();
		var shipping_address = $("#shipping_address").val();
		var description_po = CKEDITOR.instances.po_description.getData();
		var delivery_date = $(".delivery_date").val();
		if ($("#inclusive").prop('checked') == true) {
			var inclusive_gst = 1;
		} else {
			var inclusive_gst = 0;
		}
		if ((moment(default_date, 'DD-MM-YYYY', true).isValid())) {
			if (purchaseno === '') {
				event.preventDefault();
			} else {
				var project = $('#project').val();
				var vendor = $('#vendor').val();
				var company = $('#company_id').val();
				var date = $('.date').val();
				if (project === '' || vendor === '' || default_date === '' ||
					purchaseno === '' || expense_head === '' ||
					company === '') {

				} else {
					$.ajax({
						method: "GET",
						data: {
							purchase_id: purchase_id,
							purchaseno: purchaseno,
							default_date: default_date,
							project: project,
							vendor: vendor,
							expense_head: expense_head,
							company: company,
							shipping_address: shipping_address,
							contact_no: contact_no,
							description_po: description_po,
							inclusive_gst: inclusive_gst,
							delivery_date:delivery_date
						},
						dataType: "json",
						url: '<?php echo Yii::app()->createUrl('purchase/createnewpurchase'); ?>',
						success: function(result) {
							if (result.response === 'success') {
								$(".purchase_items").removeClass('checkek_edit');
								$("#purchase_id").val(result.p_id);
								$().toastmessage('showSuccessToast', "" + result.msg + "");
							} else {
								$().toastmessage('showErrorToast', "" + result.msg + "");
							}
							$('#description').select2('focus');
							$('.js-example-basic-single').select2('focus');
						}
					});



				}
			}

		} else {
			$(this).focus();
			$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
			$(this).focus();
		}

	}

	$("#purchaseno").keypress(function(event) {
		if (event.keyCode == 13) {
			$("#purchaseno").blur();
		}
	});

	$("#date").keypress(function(event) {
		if (event.keyCode == 13) {
			if ($(this).val()) {
				$("#purchaseno").focus();
			}
		}
	});

	$(".date").keyup(function(event) {
		if (event.keyCode == 13) {
			$(".date").click();
		}
	});

	$("#vendor").keyup(function(event) {
		if (event.keyCode == 13) {
			$("#vendor").click();
		}
	});

	$("#project").keyup(function(event) {
		if (event.keyCode == 13) {
			$("#project").click();
		}
	});

	var sl_no = 1;
	var howMany = 0;
	$('.item_save').click(function() {
		$("#previous_details").hide();
		var element = $(this);
		var item_id = $(this).attr('id');
		if (item_id == 0) {
			var description = $('.specification').val();
			var remark = $('#remarks').val();
			var quantity = $('#quantity').val();
			var unit = $('#item_unit').val();
			var hsn_code = $('#hsn_code').text();
			var rate = $('#rate').val();
			var amount = $('#item_amount').html();
			var rowCount = $('.table .addrow tr').length;
			var project = $('#project').val();
			var vendor = $('#vendor').val();
			var date = $(".date").val();
			var purchaseno = $('#purchaseno').val();
			var expense_head = $('#expense_head').val();
			var company = $('#company_id').val();
			var dis_amount = $('#dis_amount').val();
			var disp = $('#disp').html();
			var cgstp = $('#cgstp').val();
			var cgst_amount = $('#cgst_amount').html();
			var sgstp = $('#sgstp').val();
			var sgst_amount = $('#sgst_amount').html();
			var igstp = $('#igstp').val();
			var igst_amount = $('#igst_amount').html();
			var tax_amount = $('#tax_amount').html();
			var item_amount = $('#item_amount').html()
			var discount_amount = $('#disc_amount').html();
			var total_amount = $('#total_amount').html();
			var tax_slab = $("#tax_slab").val();
			var base_qty = $('#base_qty').val();
			var base_unit = $('#base_unit').val();
			var base_rate = $('#base_rate').val();
			if (project == '' || vendor == '' || date == '' || purchaseno == '' || expense_head == '' || company == '') {
				$().toastmessage('showErrorToast', "Please enter purchase details");
			} else {
				if ((moment(date, 'DD-MM-YYYY', true).isValid())) {
					if ((description == '' && remark == '') || quantity == '' || rate == '' || amount == 0) {
						if (amount == 0) {
							$().toastmessage('showErrorToast', "Please fill valid quantity and rate");
						} else {
							$().toastmessage('showErrorToast', "Please fill the details");
						}
					} else {
						howMany += 1;
						if (howMany == 1) {
							$('#loading').show();
							var subtot = $('#grand_total').text();
							var grand = $('#grand_total').text();
							var previousvalue = $('#previousvalue').text();
							var data = {
								'sl_no': rowCount,
								'quantity': quantity,
								'description': description,
								'unit': unit,
								'rate': rate,
								'amount': amount,
								'remark': remark,
								'purchase_id': purchase_id,
								'grand': grand,
								'subtot': subtot,
								'previousvalue': previousvalue,
								'hsn_code': hsn_code,
								'dis_amount': dis_amount,
								'disp': disp,
								'sgstp': sgstp,
								'sgst_amount': sgst_amount,
								'cgstp': cgstp,
								'cgst_amount': cgst_amount,
								'igstp': igstp,
								'igst_amount': igst_amount,
								'tax_amount': tax_amount,
								'discount_amount': discount_amount,
								'total_amount': total_amount,
								'tax_slab': tax_slab,
								'base_qty': base_qty,
								'base_unit': base_unit,
								'base_rate': base_rate,
								'project':project
							};
							$.ajax({
								url: '<?php echo  Yii::app()->createAbsoluteUrl('purchase/purchaseitem'); ?>',
								type: 'GET',
								dataType: 'json',
								data: {
									data: data
								},
								success: function(response) {
									$('#loading').hide();
									if (response.response == 'success') {
										$('#final_amount').val(response.final_amount);
										$('#amount_total').text(response.final_amount);
										$('#discount_total').text(response.discount_total);
										$('#tax_total').text(response.tax_total);
										$('#grand_total').text(response.grand_total);
										$().toastmessage('showSuccessToast', "" + response.msg + "");
										$('.addrow').append(response.html);
										$(".addrow").load(location.href+" .addrow>*","");
										if (response.mail_send == 'Y') {
											$('.mail_send').show();
										} else {
											$('.mail_send').hide();
										}
									} else {
										$().toastmessage('showErrorToast', "" + response.msg + "");
									}
									howMany = 0;
									$('#description').val('').trigger('change');
									$('#remarks').val('');
									var quantity = $('#quantity').val('');
									var unit = $('#item_unit').text('');
									var rate = $('#rate').val('');
									$('#hsn_code').val('');
									var amount = $('#item_amount').html('');
									$('#previousvalue').text('0');
									$('#description').select2('focus');
									$('#dis_amount').val('');
									$('#disp').html('0.00');
									$('#item_amount').html('0.00');
									$('#tax_amount').html('0.00');
									$('#sgstp').val('');
									$('#sgst_amount').html('0.00');
									$('#cgstp').val('');
									$('#cgst_amount').html('0.00');
									$('#igstp').val('');
									$('#igst_amount').html('0.00');
									$('#disc_amount').html('0.00');
									$('#total_amount').html('0.00');
									$('#tax_slab').val("18").trigger('change');
									$('.base-data').hide();
									$('#item_baseunit_data').hide();
									$('#item_conversion_data').hide();
									$("#unitval").val('');
									$("#itemid_val").val('');
									$("#edit_status").val('');
								}
							});

							$('.js-example-basic-single').select2('focus');
						}
					}

				} else {
					$(this).focus();
					$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
					$(this).focus();
				}
			}

		} else {
			var description = $('.specification').val();
			var remark = $('#remarks').val();
			var quantity = $('#quantity').val();
			var unit = $('#item_unit').val();
			var hsn_code = $('#hsn_code').text();
			var rate = $('#rate').val();
			var amount = $('#item_amount').html();

			var project = $('#project').val();
			var vendor = $('#vendor').val();
			var date = $(".date").val();
			var purchaseno = $('#purchaseno').val();
			var expense_head = $('#expense_head').val();
			var company = $('#company_id').val();
			var dis_amount = $('#dis_amount').val();
			var disp = $('#disp').html();
			var cgstp = $('#cgstp').val();
			var cgst_amount = $('#cgst_amount').html();
			var sgstp = $('#sgstp').val();
			var sgst_amount = $('#sgst_amount').html();
			var igstp = $('#igstp').val();
			var igst_amount = $('#igst_amount').html();
			var tax_amount = $('#tax_amount').html();
			var item_amount = $('#item_amount').html()
			var discount_amount = $('#disc_amount').html();
			var total_amount = $('#total_amount').html();
			var tax_slab = $("#tax_slab").val();
			var base_qty = $('#base_qty').val();
			var base_unit = $('#base_unit').val();
			var base_rate = $('#base_rate').val();
			
			if (project == '' || vendor == '' || date == '' || purchaseno == '' || expense_head == '' || company == '') {
				$().toastmessage('showErrorToast', "Please enter purchase details");
			} else {
				if ((moment(date, 'DD-MM-YYYY', true).isValid())) {
					if ((description == '' && remark == '') || quantity == '' || rate == '' || amount == 0) {
						if (amount == 0) {
							$().toastmessage('showErrorToast', "Please fill valid quantity and rate");
						} else {
							$().toastmessage('showErrorToast', "Please fill the details");
						}
					} else {
						howMany += 1;
						if (howMany == 1) {
							$('#loading').show();
							var subtot = $('#grand_total').text();
							var grand = $('#grand_total').text();
							var previousvalue = $('#previousvalue').text();
							var data = {
								'item_id': item_id,
								'sl_no': sl_no,
								'quantity': quantity,
								'description': description,
								'unit': unit,
								'rate': rate,
								'amount': amount,
								'remark': remark,
								'purchase_id': purchase_id,
								'grand': grand,
								'subtot': subtot,
								'previousvalue': previousvalue,
								'hsn_code': hsn_code,
								'dis_amount': dis_amount,
								'disp': disp,
								'sgstp': sgstp,
								'sgst_amount': sgst_amount,
								'cgstp': cgstp,
								'cgst_amount': cgst_amount,
								'igstp': igstp,
								'igst_amount': igst_amount,
								'tax_amount': tax_amount,
								'discount_amount': discount_amount,
								'total_amount': total_amount,
								'tax_slab': tax_slab,
								'base_qty': base_qty,
								'base_unit': base_unit,
								'base_rate': base_rate,
								'project':project
							};
							$.ajax({
								url: '<?php echo  Yii::app()->createAbsoluteUrl('purchase/updatepurchaseitem'); ?>',
								type: 'GET',
								dataType: 'json',
								data: {
									data: data
								},
								success: function(response) {
									$('#loading').hide();
									element.closest('tr').hide();
									if (response.response == 'success') {
										$('#final_amount').val(response.final_amount);
										$('#amount_total').text(response.final_amount);
										$('#discount_total').text(response.discount_total);
										$('#tax_total').text(response.tax_total);
										$('#grand_total').text(response.grand_total);

										$().toastmessage('showSuccessToast', "" + response.msg + "");

										$('.addrow').html(response.html);
										$(".addrow").load(location.href+" .addrow>*","");
										if (response.mail_send == 'Y') {
											$('.mail_send').show();
										} else {
											$('.mail_send').hide();
										}
									} else {
										$().toastmessage('showErrorToast', "" + response.msg + "");
									}
									howMany = 0;
									$('#description').val('').trigger('change');
									$('#remarks').val('');
									var quantity = $('#quantity').val('');
									var unit = $('#item_unit').text('');
									var rate = $('#rate').val('');
									var amount = $('#item_amount').html('');
									$('#hsn_code').text('');
									$(".item_save").attr('value', 'Save');
									$(".item_save").attr('id', 0);
									$('#previousvalue').text('0');
									$('#description').select2('focus');
									$('#dis_amount').val('');
									$('#disp').html('0.00');
									$('#item_amount').html('0.00');
									$('#tax_amount').html('0.00');
									$('#sgstp').val('');
									$('#sgst_amount').html('0.00');
									$('#cgstp').val('');
									$('#cgst_amount').html('0.00');
									$('#igstp').val('');
									$('#igst_amount').html('0.00');
									$('#disc_amount').html('0.00');
									$('#total_amount').html('0.00');
									$('#tax_slab').val("18").trigger('change');
									$('.base-data').hide();
									$('#item_baseunit_data').hide();
									$('#item_conversion_data').hide();
									$("#unitval").val('');
									$("#itemid_val").val('');
									$("#edit_status").val('');
								}
							});

							$('.js-example-basic-single').select2('focus');
						}
					}

				} else {
					$(this).focus();
					$().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
					$(this).focus();
				}
			}

		}
	});

	$(document).on('click', '.edit_item', function(e) {
		$('.loading-overlay').addClass('is-active');
		e.preventDefault();
		var item_id = $(this).attr('id');
		var $tds = $(this).closest('tr').find('td');
		var description = $tds.eq(1).text();
		var quantity = $tds.eq(3).text();
		var unit = $tds.eq(4).text();
		var hsn_code = $tds.eq(2).text();
		var rate = $tds.eq(5).text();
		var base_qty = $tds.eq(6).text();
		var base_unit = $tds.eq(7).text();
		var base_rate = $tds.eq(8).text();
		var amount = $tds.eq(9).text();
		var des_id = $(this).closest('tr').find('.item_description').attr('id');
		var tax_slab = $tds.eq(10).text();
		tax_slab = Math.round(tax_slab);
		var sgst_amount = $tds.eq(11).text();
		var sgstp = $tds.eq(12).text();
		var cgst_amount = $tds.eq(13).text();
		var cgstp = $tds.eq(14).text();
		var igst_amount = $tds.eq(15).text();
		var igstp = $tds.eq(16).text();
		var disc_amount = $tds.eq(18).text();
		var disc_perc = $tds.eq(17).text();
		var tax_amount = $tds.eq(19).text();
		$('#itemid_val').val(item_id);
		$('#edit_status').val(1);
		<?php $abc; ?>
		if (des_id == '0' || des_id == 'other') {
			$('#remarks').val(description.trim());
			$('.remark').css("display", "inline-block");
			$('#remark').focus();
			$('#remarks').focus();
			$('#description').val('other').trigger('change');
		} else {
			$('#description').val(des_id).trigger('change');
			$('.remark').css("display", "none");
			$('.js-example-basic-single').select2('focus');
		}

		if (unit != base_unit) {
			$('.base-data').css('display', 'inline-block');
		} else {
			$('.base-data').css('display', 'none');
		}

		$('#quantity').val(parseFloat(quantity));
		$('#item_unit option:selected').text(unit);
		$('#hsn_code').text(hsn_code);
		$('#base_qty').val(base_qty);
		$('#base_unit').val(base_unit);
		$('#base_rate').val(base_rate);
		
		$('#item_amount').text(parseFloat(amount).toFixed(2));
		$('#item_amount_temp').val(parseFloat(amount));
		$('#rate').val(parseFloat(rate));
		$(".item_save").attr('value', 'Update');
		$(".item_save").attr('id', item_id);
		$(".item_save").attr('id', item_id);
		$('#dis_amount').val(parseFloat(disc_amount));
		$('#disp').html(parseFloat(disc_perc));
		$('#tax_slab').val(tax_slab).trigger('change');
		$('#sgstp').val(parseFloat(sgstp));
		$('#sgst_amount').html(parseFloat(sgst_amount).toFixed(2));
		$('#cgstp').val(parseFloat(cgstp)).trigger('blur');
		$('#cgst_amount').html(parseFloat(cgst_amount).toFixed(2));
		$('#igstp').val(parseFloat(igstp));
		$('#igst_amount').html(parseFloat(igst_amount).toFixed(2));
		$('#tax_amount').html(parseFloat(tax_amount))
		$('#disc_amount').html(parseFloat(disc_amount))
		var totalamount = (parseFloat(amount) + parseFloat(tax_amount)) - parseFloat(disc_amount);
		$('#total_amount').html(parseFloat(totalamount).toFixed(2));
		$('.loading-overlay').removeClass('is-active');
		var selected ='selected';
		unit=unit.trim();
		
		$('select[id="item_unit"]').append('<option value="' + unit + '" ' + selected + '>' + unit + '</option>');
		
		console.log(unit);
		
	})


	$('.item_save').keypress(function(e) {
		if (e.keyCode == 13) {
			$('.item_save').click();
		}
	});


	$(document).on('click', '.approve_item', function(e) {
		e.preventDefault();
		var element = $(this);
		var item_id = $(this).attr('id');
		$.ajax({
			url: '<?php echo  Yii::app()->createAbsoluteUrl('purchase/permissionapprove'); ?>',
			type: 'POST',
			dataType: 'json',
			data: {
				item_id: item_id
			},
			success: function(response) {
				if (response.response == 'success') {
					$(".approveoption_" + item_id).hide();
					element.closest('tr').find('.rate_highlight').removeClass('rate_highlight');
					$().toastmessage('showSuccessToast', "" + response.msg + "");
					if (response.mail_send == 'Y') {
						$('.mail_send').show();
					} else {
						$('.mail_send').hide();
					}
				} else if (response.response == 'warning') {
					$(".approveoption_" + item_id).hide();
					element.closest('tr').find('.rate_highlight').removeClass('rate_highlight');
					$().toastmessage('showWarningToast', "" + response.msg + "");
				} else {
					$().toastmessage('showErrorToast', "" + response.msg + "");
				}
			}
		});
	});

	$("#previous_details").hide();

	$(document).on('mouseover', 'td.rate_highlight', function(e) {
		var item_id = $(this).attr('id');
		$.ajax({
			url: '<?php echo  Yii::app()->createAbsoluteUrl('purchase/previousratedetails'); ?>',
			type: 'GET',
			dataType: 'json',
			data: {
				item_id: item_id
			},
			success: function(result) {
				if (result.status == 1) {
					$('#previous_details').html(result.html);
					$("#previous_details").show();
					$("#pre_fixtable2").tableHeadFixer();
				} else {
					$('#previous_details').html(result.html);
					$("#previous_details").hide();
					$("#pre_fixtable2").tableHeadFixer();
				}
			}
		});
	});
	$(document).on('mouseout', '.rate_highlight', function(e) {
		$("#previous_details").hide();
		processDetails();
	});


	$(document).on('click', '.getprevious', function() {
		var id = $(this).attr('data-id');
		var res = id.split(",");
		//console.log(res);
		var unit = $('#item_unit').val();
		console.log("unit"+ unit);
		var amount = parseFloat(res[4]);
		$('#description').val(res[0]).trigger('change.select2');
		$('#quantity').val(res[1]);
		unit=res[2].trim();
		$('#item_unit').val(unit);	
			
		$('#hsn_code').html(res[16]);
		
		$('#rate').val(res[3]);
		$('#item_amount').text(amount.toFixed(2));
		$('#remarks').val(res[6]);
		$('#tax_slab').val(res[9]);
		$('#effective_quantity').val((res[1]*res[5]));
		var total = (res[1] * res[3]);
		if (isNaN(total)) total = 0;
		$('#previousvalue').text(total.toFixed(2));
		$('#base_qty').trigger('blur');
		$('#base_qty').trigger('change');
		$('#previousvalue').text(total.toFixed(2));
		var selected ='selected';
		unit=unit.trim();
		//$('#item_unit option:selected').text(unit);
		$('select[id="item_unit"]').append('<option value="' + unit + '" ' + selected + '>' + unit + '</option>');
		// debugger;
    		processDetails();
	})
	function processDetails() {
    var itemId = parseInt($("#description").val());
    var base_unit = $("#base_unit").val();
    var purchase_unit = $("#item_unit option:selected").text();
	
    var quantity = parseFloat($("#quantity").val());
    quantity = isNaN(quantity) ? 0 : quantity;
    quantity = quantity.toFixed(2);
    var base_quantity = parseFloat($("#base_qty").val());
    var baserate = 0;
    var amt = $("#item_amount").text();
    var rate = $("#rate").val();
    if (purchase_unit !== '' && base_unit !== '' && itemId !== "") {

        if (base_unit != purchase_unit) {
            base_quantity = base_quantity;
            if (isNaN(base_quantity))
                base_quantity = 0;
            if (base_quantity != 0) {

                baserate = amt / base_quantity;
                baserate = baserate.toFixed(2);
                if (isNaN(baserate))
                    baserate = 0;
            }
            $("#base_qty").val(base_quantity);
            $("#base_rate").val(baserate);
        } else {
            $("#base_qty").val(quantity);
            $("#base_rate").val(rate);
        }

    } else {

        $("#base_qty").val(quantity);
        $("#base_rate").val(rate);
    }
	
   	}


	$('#mail_send').click(function(e) {
		e.preventDefault();
		$('#loader').show();
		$.ajax({
			url: '<?php echo  Yii::app()->createAbsoluteUrl('purchase/permissionmail'); ?>',
			type: 'GET',
			dataType: 'json',
			data: {
				purchase_id: purchase_id
			},
			success: function(result) {
				if (result == 'success') {
					$('#loader').hide();
					$('.mail_send').hide();
					location.href = '<?php echo  Yii::app()->createAbsoluteUrl('purchase/admin'); ?>';
				} else if (result == 'warning') {
					$().toastmessage('showWarningToast', "PO notification Email ID not set");
					$('#loader').hide();
				} else {
					$('#loader').hide();
					$('.mail_send').hide();
				}
			}
		});
	});
</script>


<script>
	(function() {
		var mainTable = document.getElementById("main-table");
		if (mainTable !== null) {
			var tableHeight = mainTable.offsetHeight;
			if (tableHeight > 380) {
				var fauxTable = document.getElementById("faux-table");
				var tableWrap = document.getElementById("table-wrap");
				if (tableWrap !== null) {
					tableWrap.classList.add('fixedON');
					var clonedElement = mainTable.cloneNode(true);
					clonedElement.id = "";
					fauxTable.appendChild(clonedElement);
				}
			}
		}
	})();

	$('#item_unit').change(function() {
		var type = 2;
		getconversionfactor(type);
	});
	

	function getconversionfactor(type) {

		var itemId = parseInt($("#description").val());
		var base_unit = $("#base_unit").val();
		var amt = $("#item_amount").text();
		var rate = $("#rate").val();
		var quantity = parseFloat($("#quantity").val());
		quantity = isNaN(quantity) ? 0 : quantity;
		var purchase_unit = $("#item_unit option:selected").text();
		quantity = quantity.toFixed(2);
		var basequantity = parseFloat($("#base_qty").val());
		var baserate = 0;
		
		if (purchase_unit !== '' && base_unit !== '' && itemId !== "") {

			if (base_unit != purchase_unit) {


				// $('.base-data').show();
				$('.base-data').css('display', 'inline-block');
				$.ajax({
					method: "POST",
					data: {
						'purchase_unit': purchase_unit,
						'base_unit': base_unit,
						'item_id': itemId
					},
					dataType: "json",
					url: '<?php echo Yii::app()->createAbsoluteUrl('purchase/getUnitconversionFactor'); ?>',
					success: function(result) {
						if (result != '') {
							$("#item_conversion_data").show();
							$("#item_unit_additional_conv").html(result);
						}

						if (type == 2) {
							base_quantity = result * quantity;
							base_quantity = base_quantity.toFixed(2);
						} else if (type == 1) {
							base_quantity = result * quantity;
							base_quantity = base_quantity.toFixed(2);
						} else {
							base_quantity = basequantity.toFixed(2);
						}

						if (isNaN(base_quantity))
							base_quantity = 0;
						if (base_quantity != 0) {
							baserate = amt / base_quantity;
							baserate = baserate.toFixed(2);
							if (isNaN(baserate))
								baserate = 0;
						}

						$("#base_qty").val(base_quantity);
						$("#base_rate").val(baserate);

					}
				});
			} else {

				$("#item_conversion_data").hide();
				$('.base-data').hide();
				var baseval = 0;
				$("#base_qty").val(quantity);
				$("#base_rate").val(rate);
			}
			
		}
		
	}

	$('#base_qty').blur(function() {

		var itemId = parseInt($("#description").val());
		var base_unit = $("#base_unit").val();
		var purchase_unit = $("#item_unit option:selected").text();
		var quantity = parseFloat($("#quantity").val());
		quantity = isNaN(quantity) ? 0 : quantity;
		quantity = quantity.toFixed(2);
		var base_quantity = parseFloat($("#base_qty").val());
		var baserate = 0;
		var amt = $("#item_amount").text();
		var rate = $("#rate").val();
		if (purchase_unit !== '' && base_unit !== '' && itemId !== "") {

			if (base_unit != purchase_unit) {
				base_quantity = base_quantity;
				if (isNaN(base_quantity))
					base_quantity = 0;
				if (base_quantity != 0) {

					baserate = amt / base_quantity;
					baserate = baserate.toFixed(2);
					if (isNaN(baserate))
						baserate = 0;
				}
				$("#base_qty").val(base_quantity);
				$("#base_rate").val(baserate);
			}else{
				$("#base_qty").val(quantity);
				$("#base_rate").val(rate);
			}

		} else {

			$("#base_qty").val(quantity);
			$("#base_rate").val(rate);
		}
		

	});
	$('#quantity').blur(function() {

var itemId = parseInt($("#description").val());
var base_unit = $("#base_unit").val();
var purchase_unit = $("#item_unit option:selected").text();
var quantity = parseFloat($("#quantity").val());
quantity = isNaN(quantity) ? 0 : quantity;
quantity = quantity.toFixed(2);
var base_quantity = parseFloat($("#base_qty").val());
var baserate = 0;
var amt = $("#item_amount").text();
var rate = $("#rate").val();
if (purchase_unit !== '' && base_unit !== '' && itemId !== "") {

	if (base_unit != purchase_unit) {
		base_quantity = base_quantity;
		if (isNaN(base_quantity))
			base_quantity = 0;
		if (base_quantity != 0) {

			baserate = amt / base_quantity;
			baserate = baserate.toFixed(2);
			if (isNaN(baserate))
				baserate = 0;
		}
		$("#base_qty").val(base_quantity);
		$("#base_rate").val(baserate);
	}else{
		$("#base_qty").val(quantity);
		$("#base_rate").val(rate);
	}

} else {

	$("#base_qty").val(quantity);
	$("#base_rate").val(rate);
}


});


	$('.addcharge').click(function() {
		$(".chargeadd").toggle();
		$(this).toggleClass("icon-minus hidecharge");
	});

    $(".savecharge").on('keyup keypress keydown click', function(e) {
		var pid = "<?php echo isset($_GET['pid'])?$_GET['pid']:""?>";
		if (pid != 0) {
			var label = $("#labelcat").val();
			var currentval = $("#currentval").val();
			
            if (label == '' && currentval == '')  {
				$('.errorcharge').html('Both fields are mandatory');
			} else {
				$(".savecharge").attr('disabled', 'disabled');				
				$.ajax({
					url: "<?php echo Yii::app()->createAbsoluteUrl("purchase/additionalCharge"); ?>",
					data: {
						category: label,
						amount: currentval,
						pid: pid						
					},
					type: "POST",
					success: function(data) {						
						$(".savecharge").removeAttr('disabled');																			
						$('#labelcat').val('');
						$('#currentval').val('');
						$("#addtional_list").load(location.href+" #addtional_list>*","");
						$("#main-table").load(location.href+" #main-table>*","");
					}
				});
			} 
		} else {
			$('.errorcharge').html('Please add item');
		}
	});


	$(document).on("click",".deletecharge",function(){
		var id = $(this).attr("id");
		var purchase_id = $(this).attr("purchase-id");
		var elem = $(this);
		$.ajax({
			url: "<?php echo Yii::app()->createAbsoluteUrl("purchase/deleteadditionalCharge"); ?>",
			data: {
				id:	id	,
				purchase_id:purchase_id					
			},
			type: "POST",
			dataType:"json",
			success: function(data) {
				console.log(data);
				$(elem).parents(".display_additional").remove();
				$("#main-table").load(location.href+" #main-table>*","");
			}
		})
	})


	function closeaction(){
        $(".modal").trigger("click");
    }

	// Form submissions
    $('#myModal1').on('hidden.bs.modal', function (e) {
        $(this).find(".panel-body")
            .find("input[type=text],textarea,select")
            .val('')
            .end()
            .find("input[type=checkbox], input[type=radio]")
            .prop("checked", "")
            .end();
    })

    $('#myModal2').on('hidden.bs.modal', function (e) {
        $(this).find(".panel-body")
            .find("input[type=text],textarea,select")
            .val('')
            .end()
            .find("input[type=checkbox], input[type=radio]")
            .prop("checked", "")
            .end();
    })
    $('#myModal3').on('hidden.bs.modal', function (e) {
        $(this).find(".panel-body")
            .find("input[type=text],textarea,select")
            .val('')
            .end()
            .find("input[type=checkbox], input[type=radio]")
            .prop("checked", "")
            .end();
    })
    $('#myModal4').on('hidden.bs.modal', function (e) {
        $(this).find(".panel-body")
            .find("input[type=text],textarea,select")
            .val('')
            .end()
            .find("input[type=checkbox], input[type=radio]")
            .prop("checked", "")
            .end();
    })
	
    // 1. COmpany
    $(document).on("submit","#company-form",function(){ 
		var company = $("#company_id").val();         
        var data = $("#company-form").serialize();					
		$.ajax({
			url: '<?php echo Yii::app()->createUrl('company/create&layout=1&popup=1'); ?>',
			method: 'POST',
			data: data,						
			success: function(response) {       
				console.log("Company added successfully");                                
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('purchase/dynamiccompany'); ?>',
                    method: 'POST',
					data: {
                        company_id: company
                    },                    
                    dataType: "json",
                    success: function(response) {                        
                        $("#company_id").html(response.html);                                                    
                    }
                })
			}
		})
        return false;
    })

    // 2. Project
    $(document).on("submit","#projects-form",function(){
        var company = $("#company_id").val();
		var project = $("#project").val();        
        if(company==""){
            alert('Please choose a company');
            return false;
        }
        
        var data = $("#projects-form").serialize();					
		$.ajax({
			url: '<?php echo Yii::app()->createUrl('projects/create&layout=1'); ?>',
			method: 'POST',
			data: data,						
			success: function(response) {       
				console.log("Project added successfully");                
                if (company != '') {
                    $("#project").html('<option value="">Select rty</option>');
                    $.ajax({
                        url: '<?php echo Yii::app()->createUrl('purchase/dynamicproject'); ?>',
                        method: 'POST',
                        data: {
                            company_id: company,
							project:project
                        },
                        dataType: "json",
                        success: function(response) {
                            if (response.auto_pono == 1) {
                                $('#purchaseno').attr('readonly', true);
                                $('#purchaseno').val(response.purchase_no);
                            } else {
                                $('#purchaseno').attr('readonly', false);
                                $('#purchaseno').val('');
                            }
                            if (response.status == 'success') {
                                $("#project").html(response.html);
                            } else {
                                $("#project").html(response.html);
                            }                            
                        }
                    })
                }                               
			}
		})
        
        return false;
    })

    // 3. Expense head
    $(document).on("submit","#expense-type-form",function(){
        var company = $("#company_id").val();
        var project = $("#project").val();
		var expense_head = $("#expense_head").val();
        if(company==""){
            alert('Please choose a company');
            return false;
        }
        if(project==""){
            alert('Please choose a project');
            return false;
        }
        var data = $("#expense-type-form").serialize();					
		$.ajax({
			url: '<?php echo Yii::app()->createUrl('expensetype/create&layout=1&popup=1&projectid='); ?>'+project,
			method: 'POST',
			data: data,						
			success: function(response) {       
				console.log("Expense head added successfully");
                $("body").trigger("click");
                
                
                $("#expense_head").html('<option value="">Select Expense Head</option>');
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('purchase/dynamicexpensehead'); ?>',
                    method: 'POST',
                    data: {
                        project_id: project,
						expense_head:expense_head
                    },
                    dataType: "json",
                    success: function(response) {
                        if(response.msg != ""){
                            alert(response.msg);
                        }
                        $("#shipping_address").val(response.address);
                        
                        if (response.status == 'success') {
                            $("#expense_head").html(response.html);
                            // $("#vendor").html('<option value="">Select Vendor</option>');
                        }
                        
                    }
                })
			}
		})
        return false;
    })

    // 4. Vendor
    $(document).on("submit","#vendors-form",function(){
        var company = $("#company_id").val();
        var project = $("#project").val();
        var expense_head = $("#expense_head").val();
		var  vendor = $("#vendor").val();
        if(company==""){
            alert('Please choose a company');
            return false;
        }
        if(project==""){
            alert('Please choose a project');
            return false;
        }
        if(expense_head==""){
            alert('Please choose an Expense head');
            return false;
        }
        var data = $("#vendors-form").serialize();					
		$.ajax({
			url: '<?php echo Yii::app()->createUrl('vendors/create&layout=1'); ?>',
			method: 'POST',
			data: data,						
			success: function(response) {       
				console.log("Vendor added successfully");
                $("body").trigger("click");
                
                $("#vendor").html('<option value="">Select Vendor</option>');
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('purchase/dynamicvendor'); ?>',
                    method: 'POST',
                    data: {
                        exp_id: expense_head,
						vendor:vendor
                    },
                    dataType: "json",
                    success: function(response) {
                        $("#vendor").html(response.html);                        
                    }
                })
			}
		})
        return false;
    })
	

</script>