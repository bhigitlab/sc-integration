<?php
/* @var $this ExpensesController */
/* @var $model Expenses */
/* @var $form CActiveForm */
?>
<h5>Filter By :</h5>
<?php 
$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
        ));
?>
<div class="filter">
    <div class="row ">
	
        <div class="col-md-2 col-sm-5 col-xs-5">					
			<?php echo $form->label($model, 'Expense Type'); ?>
            <?php echo $form->dropDownList($model,'exp_type',array("" => "Please Choose","1"=>"Expense","0"=>"Receipt")); ?>
        </div>
        <div class="col-md-3 col-sm-5 col-xs-5 date">						
            <?php echo $form->label($model, 'date'); ?>
            <?php echo CHtml::activeTextField($model, 'fromdate', array('size' => 10, 'value' => isset($_REQUEST['Dailyexpense']['fromdate']) ? $_REQUEST['Dailyexpense']['fromdate'] : '')); ?>
            <?php
            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => 'Dailyexpense_fromdate',
                'ifFormat' => '%d-%b-%Y',
            ));
            ?> to
            <?php echo CHtml::activeTextField($model, 'todate', array('size' => 10, 'value' => isset($_REQUEST['Dailyexpense']['todate']) ? $_REQUEST['Dailyexpense']['todate'] : '')); ?>
            <?php
            $this->widget('application.extensions.calendar.SCalendar', array(
                'inputField' => 'Dailyexpense_todate',
                'ifFormat' => '%d-%b-%Y',
            ));
            ?>

        </div>
        <div class="col-md-2 col-sm-3 col-xs-3">	
            <label>&nbsp;</label>
            <div class= "text-left">
				<?php echo CHtml::submitButton('Go'); ?>
                <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('newlist') . '"')); ?>
            </div>
        </div>

    </div>

</div>
<?php $this->endWidget(); ?>
