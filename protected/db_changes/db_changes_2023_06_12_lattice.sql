ALTER TABLE `jp_sales_quotation` ADD `item_name` VARCHAR(300) NULL DEFAULT NULL AFTER `created_date`;
ALTER TABLE `jp_pre_dailyexpenses` CHANGE `approved_by` `approved_by` INT(11) NULL DEFAULT NULL;
ALTER TABLE `jp_pre_dailyexpenses` CHANGE `cancelled_at` `cancelled_at` DATETIME NULL DEFAULT NULL;
CREATE TABLE `jp_quotation_item_rate_master` (
  `id` int(11) NOT NULL,
  `master_category_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

ALTER TABLE `jp_quotation_item_rate_master`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `jp_quotation_item_rate_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
