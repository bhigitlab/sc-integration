<?php
/* @var $this ProjectsController */
/* @var $data Projects */
?>



<?php
if ($index == 0) {
    ?>
    <thead>
    <tr> 
        <th>No</th>
        <th>UserRole</th>
        <th style="display: none;"></th>          
    </tr>   
    </thead>
<?php } ?>
    <tr> 
        <td style="width: 50px;"><?php echo $index+1; ?></td>
        
        <td  class="text_black" data-value="<?php echo CHtml::encode($data->role); ?>" data-id="<?php echo $data->id; ?>" >
            <span class="list_item"><?php echo CHtml::encode($data->role); ?></span>
            <?php
            if(isset(Yii::app()->user->role) && (in_array('/userRoles/update', Yii::app()->user->menuauthlist))){
            ?>
            <span class="fa fa-edit editable" onclick="editaction(this,event);" data-id="<?php echo $data->id; ?>" data-value="<?php echo CHtml::encode($data->role); ?>" style="cursor:pointer;"></span>
            <?php } ?>
            <span class="delete_userrole pull-right"  title="Delete" onclick="deletUserRole(this,<?php echo $data->id; ?>)">
                <span  class="fa fa-trash deletUserRole" ></span>
            </span>
        </td>
        
        <td class='editelement test' style="display:none;">
        <input  class='user_role singletextbox' type = 'text'  name='user_role' data-value="<?php echo CHtml::encode($data->role); ?>">
        <a class='save btn btn-info btn-xs' onclick="saveaction(this,event);" type = 'submit' data-id="<?php echo $data->id; ?>" >Save</a>
        <a class='cancel btn btn-default btn-xs' onclick="cancelaction(this,event);">Cancel</a>
        <br>
        <span class='errorMessage1'></span>
       
        
        </td>		
    
    </tr>  

    
