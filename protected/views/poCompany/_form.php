<?php
/* @var $this ProjectsController */
/* @var $model Projects */
/* @var $form CActiveForm */

?>

<div class="">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'po-company-form',
        'enableClientValidation'=>true,
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>



    <!-- Popup content-->



    <div class="panel-body">
       
            <div class="row addRow">					

                <div class="col-md-6">
                    <?php echo $form->labelEx($model,'name'); ?>
                    <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
                    <?php echo $form->error($model,'name'); ?>
                </div>
                <div class="col-md-6">
                    <?php echo $form->labelEx($model,'pincode'); ?>
                    <?php echo $form->textField($model,'pincode',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
                    <?php echo $form->error($model,'pincode'); ?>
                </div>
            </div>
            <div class="row addRow">  
                <div class="col-md-6">
                   <?php echo $form->labelEx($model,'phone'); ?>
                    <?php echo $form->textField($model,'phone',array('size'=>50,'maxlength'=>50,'class'=>'form-control')); ?>
                    <?php echo $form->error($model,'phone'); ?>
                </div>
                
                
                <div class="col-md-6">
                   <?php echo $form->labelEx($model,'email_id'); ?>
                    <?php echo $form->textField($model,'email_id',array('size'=>50,'maxlength'=>50,'class'=>'form-control')); ?>
                    <?php echo $form->error($model,'email_id'); ?>
                </div>
            </div>
            <div class="row addRow">
                <div class="col-md-6">
                    <?php echo $form->labelEx($model,'company_gstnum'); ?>
                    <?php echo $form->textField($model,'company_gstnum',array('size'=>50,'maxlength'=>50,'class'=>'form-control')); ?>
                    <?php echo $form->error($model,'company_gstnum'); ?>
                </div>
                <div class="col-md-6 textArea-box">
                    <?php echo $form->labelEx($model,'description'); ?>
                    <?php echo $form->textArea($model,'description',array('rows'=>3, 'cols'=>50,'class'=>'form-control')); ?>
                    <?php echo $form->error($model,'description'); ?>
                </div>
            </div>
            <div class="row addRow">
                <div class="col-md-12 textArea-box">
                    <?php echo $form->labelEx($model,'address'); ?>
                    <?php echo $form->textArea($model,'address',array('rows'=>3, 'cols'=>50,'class'=>'form-control')); ?>
                    <?php echo $form->error($model,'address'); ?>
                </div>
            </div>
        </div>
        <div class="panel-footer save-btnHold text-center">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-info')); ?>                            
        <!--            <button data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>-->
            <?php
                        if(!$model->isNewRecord){
                        echo CHtml::ResetButton('Close', array('onclick'=>'closeaction(this,event);','class'=>'btn'));  
                            
                        } 
                    else{
                       echo CHtml::ResetButton('Reset', array('style'=>'margin-right:3px;','class'=>'btn btn-default')); 
                       echo CHtml::ResetButton('Close', array('onclick'=>'closeaction(this,event);','class'=>'btn'));  
                    }
                    ?>
        </div>


        <style>
            .tableborder td{
                border:1px solid #c2c2c2;
            }
            
        </style>



        <?php $this->endWidget(); ?>

    </div><!-- form -->

