<?php

class BankController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        $accessArr = array();
        $accessauthArr = array();
        $controller = Yii::app()->controller->id;
        if (isset(Yii::app()->user->menuall)) {
            if (array_key_exists($controller, Yii::app()->user->menuall)) {
                $accessArr = Yii::app()->user->menuall[$controller];
            }
        }

        if (isset(Yii::app()->user->menuauth)) {
            if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                $accessauthArr = Yii::app()->user->menuauth[$controller];
            }
        }
        $access_privlg = count($accessauthArr);
        return array(
            array(
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => $accessArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),

            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Bank;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Bank'])) {
            $model->attributes = $_POST['Bank'];
            if ($model->save())
                $this->redirect(array('newlist'));
        }
        if (isset($_GET['layout']))
            $this->layout = false;

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Bank'])) {
            $model->attributes = $_POST['Bank'];
            if ($model->save())
                if (Yii::app()->user->returnUrl != 0) {
                    $this->redirect(array('newlist', 'Bank_page' => Yii::app()->user->returnUrl));
                } else {
                    $this->redirect(array('newlist'));
                }
        }
        if (isset($_GET['layout'])) {
            $this->layout = false;
        }
        $this->render('update', array(
            'model' => $model,
        ));
    }
    public function actionupdate2()
    {
        $id = $_POST['id'];
        $value = $_POST['value'];

        $model = $this->loadModel($id);
        $model->bank_name = $value;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        if ($model->save()) {
            $data = null;
            print_r(json_encode($data));
        } else {
            $error = $model->getErrors();
            print_r(json_encode($error['bank_name']));
        }
    }


    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Bank');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Bank('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Bank']))
            $model->attributes = $_GET['Bank'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Bank the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Bank::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Bank $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'bank-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    public function actionNewlist()
    {

        $model = new Bank('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Bank']))
            $model->attributes = $_GET['Bank'];

        $this->render('newlist', array(
            'model' => $model,
            'dataProvider' => $model->search(),
        ));
    }
    public function actionSavetopdf()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $qry   = "SELECT * FROM " . $tblpx . "bank";
        $data  = Yii::app()->db->createCommand($qry)->queryAll();
        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
        $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
        $template = $this->getTemplate($selectedtemplate);
        $this->logo = $this->realpath_logo;
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $mPDF1->AddPage('','', '', '', '', 0,0,40, 30, 10,0);
        $mPDF1->WriteHTML($this->renderPartial('banktopdf', array(
            'model' => $data,
        ), true));
        $filename = "Bank List";
        $mPDF1->Output($filename . '.pdf', 'D');
    }
    public function actionSavetoexcel()
    {
        $tblpx       = Yii::app()->db->tablePrefix;
        $qry         = "SELECT * FROM " . $tblpx . "bank";
        $data        = Yii::app()->db->createCommand($qry)->queryAll();
        $finaldata   = array();
        $arraylabel  = array('Sl No.', 'Bank Name');
        $i           = 1;
        $finaldata   = array();
        foreach ($data as $key => $datavalue) {
            $finaldata[$key][] = $i;
            $finaldata[$key][] = $datavalue['bank_name'];
            $i++;
        }
        Yii::import('ext.ECSVExport');
        $csv         = new ECSVExport($finaldata);
        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'bank_list.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }
    public function actionReconciliation()
    {
        $model  = new Reconciliation;
        $tblpx  = Yii::app()->db->tablePrefix;
        if (isset($_GET['Reconciliation'])) {
            $model->attributes = $_GET['Reconciliation'];
            $model->fromdate = $_GET['Reconciliation']['fromdate'];
            $model->todate = $_GET['Reconciliation']['todate'];
        }

        $this->render('reconciliation', array(
            'model' => $model,
            'dataProvider' => $model->pendingreconciliation(),
        ));
    }
    public function actionReconciliationentry()
    {
        $tblpx      = Yii::app()->db->tablePrefix;
        $model      = new Reconciliation;
        $recDate    = $_REQUEST["recDate"];
        $recCreds   = $_REQUEST["recCreds"];
        $recCount   = $_REQUEST["nums"];
        $recDate    = array_values(array_filter($recDate));
        $recCreds   = array_values(array_filter($recCreds));
        $transaction = Yii::app()->db->beginTransaction();
        try {
            for ($i = 0; $i < $recCount; $i++) {
                $credsArr   = explode(',', $recCreds[$i]);

                $recId      = $credsArr[0];
                $recTable   = $credsArr[1];
                $recAction  = $credsArr[2];
                $recParent  = $credsArr[3];
                $recParents = array_slice($credsArr, 4);
                $recParents = implode(", ", $recParents);
                //echo $recId." ".$recTable." ".$recAction." ".$recParent;
                $reconciliation_model = Reconciliation::model()->findByPk($recId);
                $recNewDate = date("Y-m-d", strtotime($recDate[$i]));

                if (!empty($recNewDate)) {                
                    $current_date = date('Y-m-d');                
                    if($recNewDate < $reconciliation_model->reconciliation_paymentdate){
                        throw new Exception("Reconciliation Date  should be greater than or equal to ".$reconciliation_model->reconciliation_paymentdate);                    
                    }
    
                    if($recNewDate > $current_date){
                        throw new Exception("Reconciliation Date should not greater than ".$current_date);
                    }
                }

                if ($recTable == "jp_expenses") {
                    $pKey   = "exp_id";
                    $rec_model = 'Expenses';
                } else if ($recTable == "jp_dailyexpense") {
                    $pKey   = "dailyexp_id";
                    $rec_model = 'Dailyexpense';
                } else if ($recTable == "jp_dailyvendors") {
                    $pKey   = "daily_v_id";
                    $rec_model = 'Dailyvendors';
                } else if ($recTable == "jp_subcontractor_payment") {
                    $pKey   = "payment_id";
                    $rec_model = 'SubcontractorPayment';
                } else if ($recTable == "jp_buyer_transactions") {
                    $pKey   = "id";
                    $rec_model = 'BuyerTransactions';
                }
                if ($recNewDate != "1970-01-01") {
                    $reconcilation_datas_by_id = Reconciliation::model()->findAllByAttributes(array('reconciliation_chequeno' => $reconciliation_model->reconciliation_chequeno,'reconciliation_id' => $recId,));
                    if ($recAction == "Subcontractor Payment") {

                        foreach ($reconcilation_datas_by_id as $recon_data) {
                            $data_model = Reconciliation::model()->findByPk($recon_data->reconciliation_id);
                            $data_model->reconciliation_date = $recNewDate;
                            $data_model->reconciliation_status = 1;
                            if (!$data_model->save()) {
                                $success_status = 0;
                                throw new Exception(json_encode($data_model->getErrors()));
                            } else {
                                $success_status = 1;
                                $parent_model = $rec_model::model()->findByPk($recon_data->reconciliation_parentid);
                                if (!empty($parent_model)) {
                                    $parent_model->reconciliation_date = $recNewDate;
                                    $parent_model->reconciliation_status = 1;
                                    if ($recTable == "jp_expenses")
                                        $parent_model->payment_quotation_status = 1;
                                    if (!$parent_model->save()) {
                                        $success_status = 0;
                                        throw new Exception(json_encode($parent_model->getErrors()));
                                    } else {
                                        $success_status = 1;
                                    }
                                }
                            }
                            $subContractor  = Expenses::model()->findByPk($recon_data->reconciliation_parentid);
                            $subCId         = $subContractor->subcontractor_id;
                            if (!empty($subContractor['subcontractor_id'])) {
                                $sc_payment_model = SubcontractorPayment::model()->findAll(array('condition' => 'payment_id IN (' . $subContractor['subcontractor_id'] . ')'));
                                foreach ($sc_payment_model as $sc_payment) {
                                    $sc_payment->reconciliation_date = $recNewDate;
                                    $sc_payment->reconciliation_status = 1;
                                    if (!$sc_payment->save()) {
                                        $success_status = 0;
                                        throw new Exception(json_encode($sc_payment->getErrors()));
                                    } else {
                                        $success_status = 1;
                                    }
                                }
                            }
                        }
                    } else {

                        foreach ($reconcilation_datas_by_id as $recon_data) {
                            $data_model = Reconciliation::model()->findByPk($recon_data->reconciliation_id);
                            $data_model->reconciliation_date = $recNewDate;
                            $data_model->reconciliation_status = 1;
                            if (!$data_model->save()) {
                                $success_status = 0;
                                throw new Exception(json_encode($data_model->getErrors()));
                            } else {
                                $success_status = 1;
                                $parent_model = $rec_model::model()->findByPk($recon_data->reconciliation_parentid);
                                if (!empty($parent_model)) {
                                    $parent_model->reconciliation_date = $recNewDate;
                                    $parent_model->reconciliation_status = 1;
                                    if ($recTable == "jp_expenses")
                                        $parent_model->payment_quotation_status = 1;
                                    if (!$parent_model->save()) {
                                        $success_status = 0;
                                        throw new Exception(json_encode($parent_model->getErrors()));
                                    } else {
                                        $success_status = 1;
                                        if ($recTable == "jp_dailyexpense") {
                                            if($parent_model->expensehead_type==5){
                                                $ref_model = $rec_model::model()->find(array('condition'=>'transaction_parent='.$recon_data->reconciliation_parentid));
                                                $ref_model->reconciliation_status =NULL;
                                                if(!$ref_model->save()){
                                                    $success_status = 0;
                                                    throw new Exception(json_encode($ref_model->getErrors())); 
                                                }else{
                                                    $success_status = 1;
                                                }
                                            }
                                            if($parent_model->expensehead_type==4){
                                                $ref_model = $rec_model::model()->find(array('condition'=>'transaction_parent='.$recon_data->reconciliation_parentid));
                                                
                                                $ref_model->reconciliation_status =NULL;
                                                if(!$ref_model->save()){
                                                    $success_status = 0;
                                                    throw new Exception(json_encode($ref_model->getErrors())); 
                                                }else{
                                                    $success_status = 1;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
            $error =  $error->getMessage();
        } finally {
            if ($success_status == 1) {
                Yii::app()->user->setFlash('success', "Reconciliation record successfully added");
            } else {
                Yii::app()->user->setFlash('error', $error);
            }
        }


        $totalCount = $model->pendingreconciliation()->getTotalItemCount();
        $resRec = $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $model->pendingreconciliation(),
            'viewData' => array('itemcount' => $totalCount),
            'itemView' => '_reconnewview', 'template' => '<div>{summary}{sorter}</div><div class="tblwrap scrollTable"><div class="container1"><table cellpadding="10" class="table  list-view sorter">{items}</table></div></div>{pager}',
        ));
        return $resRec;
    }
    public function actionReconciliationreport()
    {

        $model  = new Reconciliation;
        $tblpx  = Yii::app()->db->tablePrefix;
        if (isset($_GET['Reconciliation'])) {
            $model->attributes = $_GET['Reconciliation'];
            $model->fromdate = $_GET['Reconciliation']['fromdate'];
            $model->todate = $_GET['Reconciliation']['todate'];
            $model->company_id = $_GET['Reconciliation']['company_id'];
        }
        $sql = "SELECT SUM(reconciliation_amount) as totalamount FROM {$tblpx}reconciliation WHERE reconciliation_status = 1";
        if (isset($_GET['Reconciliation']['reconciliation_payment']) && $_GET['Reconciliation']['reconciliation_payment'] != '')
            $sql .= " AND reconciliation_payment LIKE '%" . $_GET['Reconciliation']['reconciliation_payment'] . "%'";
        if (isset($_GET['Reconciliation']['reconciliation_bank']) && $_GET['Reconciliation']['reconciliation_bank'] != '')
            $sql .= " AND reconciliation_bank = {$_GET['Reconciliation']['reconciliation_bank']}";
        if (isset($_GET['Reconciliation']['company_id']) && $_GET['Reconciliation']['company_id'] != '')
            $sql .= " AND company_id = {$_GET['Reconciliation']['company_id']}";

        if (!empty($_GET['Reconciliation']['fromdate']) || !empty($_GET['Reconciliation']['todate'])) {
            if (!empty($_GET['Reconciliation']['fromdate']) && empty($_GET['Reconciliation']['todate'])) {
                $sql .= " AND reconciliation_date >= '" . date('Y-m-d', strtotime($_GET['Reconciliation']['fromdate'])) . "'";  // date is database date column field
            } else if (!empty($_GET['Reconciliation']['todate']) && empty($_GET['Reconciliation']['fromdate'])) {
                $sql .= " AND reconciliation_date <= '" . date('Y-m-d', strtotime($_GET['Reconciliation']['todate'])) . "'";
            } elseif (!empty($_GET['Reconciliation']['todate']) && !empty($_GET['Reconciliation']['fromdate'])) {
                $sql .= " AND reconciliation_date between '" . date('Y-m-d', strtotime($_GET['Reconciliation']['fromdate'])) . "' and  '" . date('Y-m-d', strtotime($_GET['Reconciliation']['todate'])) . "'";
            }
        } else {
            $datefrom = date("Y-m-") . "01";
            $date_to = date("Y-m-d");
            $sql .= " AND reconciliation_date between '" . date('Y-m-d', strtotime($datefrom)) . "' and  '" . date('Y-m-d', strtotime($date_to)) . "'";
        }
        // if(!empty($_GET['Reconciliation']['fromdate']) && empty($_GET['Reconciliation']['todate'])) {
        //     $sql .= " AND reconciliation_date >= '".date('Y-m-d',strtotime($_GET['Reconciliation']['fromdate']))."'";  // date is database date column field
        // } else if(!empty($_GET['Reconciliation']['todate']) && empty($_GET['Reconciliation']['fromdate'])) {
        //     $sql .= " AND reconciliation_date <= '".date('Y-m-d',strtotime($_GET['Reconciliation']['todate']))."'";
        // } else if(!empty($_GET['Reconciliation']['todate']) && !empty($_GET['Reconciliation']['fromdate'])) {
        //     $sql .= " AND reconciliation_date between '".date('Y-m-d',strtotime($_GET['Reconciliation']['fromdate']))."' and  '".date('Y-m-d',strtotime($_GET['Reconciliation']['todate']))."'";
        // }
        $paymentData    = Yii::app()->db->createCommand($sql . " AND reconciliation_payment LIKE '%Payment%'")->queryRow();
        $paymentAmount  = $paymentData["totalamount"];
        $receiptData    = Yii::app()->db->createCommand($sql . " AND reconciliation_payment LIKE '%Receipt%'")->queryRow();
        $receiptAmount  = $receiptData["totalamount"];
        $this->render('reconciliationreport', array(
            'model' => $model,
            'dataProvider' => $model->completedreconciliation(),
            'paymentamount' => $paymentAmount,
            'receiptamount' => $receiptAmount,
        ));
    }
    public function actionSavereconciliationpdf()
    {
        $model  = new Reconciliation;
        $model->unsetAttributes();
        $tblpx  = Yii::app()->db->tablePrefix;
        if (!empty($_REQUEST["fromdate"]))
            $model->fromdate = $_REQUEST['fromdate'];
        if (!empty($_REQUEST["todate"]))
            $model->todate = $_REQUEST['todate'];
        if (!empty($_REQUEST["payment_name"]))
            $model->reconciliation_payment = $_REQUEST['payment_name'];
        if (!empty($_REQUEST["bank_id"]))
            $model->reconciliation_bank = $_REQUEST['bank_id'];
        if (!empty($_REQUEST["company_id"]))
            $model->company_id = $_REQUEST['company_id'];

        $sql = "SELECT SUM(reconciliation_amount) as totalamount FROM {$tblpx}reconciliation WHERE reconciliation_status = 1";
        if (isset($_REQUEST['payment_name']) && $_REQUEST['payment_name'] != '')
            $sql .= " AND reconciliation_payment LIKE '%" . $_REQUEST['payment_name'] . "%'";
        if (isset($_REQUEST['bank_id']) && $_REQUEST['bank_id'] != '')
            $sql .= " AND reconciliation_bank = {$_REQUEST['bank_id']}";
        if (isset($_REQUEST['company_id']) && $_REQUEST['company_id'] != '')
            $sql .= " AND company_id = {$_REQUEST['company_id']}";
        if (!empty($_REQUEST['fromdate']) && empty($_REQUEST['todate'])) {
            $sql .= " AND reconciliation_date >= '" . date('Y-m-d', strtotime($_REQUEST['fromdate'])) . "'";  // date is database date column field
        } else if (!empty($_REQUEST['todate']) && empty($_REQUEST['fromdate'])) {
            $sql .= " AND reconciliation_date <= '" . date('Y-m-d', strtotime($_REQUEST['todate'])) . "'";
        } else if (!empty($_REQUEST['todate']) && !empty($_REQUEST['fromdate'])) {
            $sql .= " AND reconciliation_date between '" . date('Y-m-d', strtotime($_REQUEST['fromdate'])) . "' and  '" . date('Y-m-d', strtotime($_REQUEST['todate'])) . "'";
        }
        $paymentData    = Yii::app()->db->createCommand($sql . " AND reconciliation_payment LIKE '%Payment%'")->queryRow();
        $paymentAmount  = $paymentData["totalamount"];
        $receiptData    = Yii::app()->db->createCommand($sql . " AND reconciliation_payment LIKE '%Receipt%'")->queryRow();
        $receiptAmount  = $receiptData["totalamount"];
        $this->logo = $this->realpath_logo;
        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
        $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
        
        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $mPDF1->AddPage('','', '', '', '', 0,0,52, 30, 10,0);
        $mPDF1->WriteHTML($this->renderPartial('reconciliationreportpdf', array(
            'model' => $model,
            'dataProvider' => $model->completedreconciliation(),
            'paymentamount' => $paymentAmount,
            'receiptamount' => $receiptAmount,
        ), true));
        $filename = "Bank_Reconcilation_Report" . date("d-m-Y");
        $mPDF1->Output($filename . '.pdf', 'D');
    }
    public function actionSavereconciliationexcel()
    {
        $model  = new Reconciliation;
        $model->unsetAttributes();
        $tblpx  = Yii::app()->db->tablePrefix;
        if (!empty($_REQUEST["fromdate"]))
            $model->fromdate = $_REQUEST['fromdate'];
        if (!empty($_REQUEST["todate"]))
            $model->todate = $_REQUEST['todate'];
        if (!empty($_REQUEST["payment_name"]))
            $model->reconciliation_payment = $_REQUEST['payment_name'];
        if (!empty($_REQUEST["bank_id"]))
            $model->reconciliation_bank = $_REQUEST['bank_id'];
        if (!empty($_REQUEST["company_id"]))
            $model->company_id = $_REQUEST['company_id'];

        $sql = "SELECT SUM(reconciliation_amount) as totalamount FROM {$tblpx}reconciliation WHERE reconciliation_status = 1";
        if (isset($_REQUEST['payment_name']) && $_REQUEST['payment_name'] != '')
            $sql .= " AND reconciliation_payment LIKE '%" . $_REQUEST['payment_name'] . "%'";
        if (isset($_REQUEST['bank_id']) && $_REQUEST['bank_id'] != '')
            $sql .= " AND reconciliation_bank = {$_REQUEST['bank_id']}";
        if (isset($_REQUEST['company_id']) && $_REQUEST['company_id'] != '')
            $sql .= " AND company_id = {$_REQUEST['company_id']}";
        if (!empty($_REQUEST['fromdate']) && empty($_REQUEST['todate'])) {
            $sql .= " AND reconciliation_date >= '" . date('Y-m-d', strtotime($_REQUEST['fromdate'])) . "'";  // date is database date column field
        } else if (!empty($_REQUEST['todate']) && empty($_REQUEST['fromdate'])) {
            $sql .= " AND reconciliation_date <= '" . date('Y-m-d', strtotime($_REQUEST['todate'])) . "'";
        } else if (!empty($_REQUEST['todate']) && !empty($_REQUEST['fromdate'])) {
            $sql .= " AND reconciliation_date between '" . date('Y-m-d', strtotime($_REQUEST['fromdate'])) . "' and  '" . date('Y-m-d', strtotime($_REQUEST['todate'])) . "'";
        }
        $paymentData    = Yii::app()->db->createCommand($sql . " AND reconciliation_payment LIKE '%Payment%'")->queryRow();
        $paymentAmount  = $paymentData["totalamount"];
        $receiptData    = Yii::app()->db->createCommand($sql . " AND reconciliation_payment LIKE '%Receipt%'")->queryRow();
        $receiptAmount  = $receiptData["totalamount"];

        $finaldata   = array();
        $arraylabel  = array('Sl No', 'Payment Name', 'Company', 'Bill No/Transaction head', 'Vendors/Subcontractors', 'Withdrawal', 'Deposit', 'Bank', 'Cheque Number', 'Description', 'Payment Date', 'Created Date', 'Reconciliation Date');

        /************************* */
        $finaldata   = array();
        $billno      = "";
        $a           = 1;
        $final_amount1 = 0;
        $final_amount2 = 0;
        if (!empty($model->completedreconciliation()->getData())) {
            foreach ($model->completedreconciliation()->getData() as $data) {



                $paymentType = $data->reconciliation_payment;

                if (strpos($paymentType, 'Payment') !== false) {

                    $final_amount1 += $data->reconciliation_amount;
                }
                if (strpos($paymentType, 'Receipt') !== false) {

                    $final_amount2 += $data->reconciliation_amount;
                }
            }
        }
        $finaldata[0][]  = '';
        $finaldata[0][]  = '';
        $finaldata[0][]  = '';
        $finaldata[0][]  = '';
        $finaldata[0][]  = 'Total = ';
        $finaldata[0][]  = Controller::money_format_inr($final_amount1, 2);
        $finaldata[0][]  = Controller::money_format_inr($final_amount2, 2);
        $finaldata[0][]  = '';
        $finaldata[0][]  = '';
        $finaldata[0][]  = '';
        $finaldata[0][]  = '';
        $finaldata[0][]  = '';
        $finaldata[0][]  = '';
        /******************* */
        $i           = 1;

        $billno      = "";
        $a           = 1;
        $final_amount1 = 0;
        $final_amount2 = 0;
        if (!empty($model->completedreconciliation()->getData())) {
            foreach ($model->completedreconciliation()->getData() as $data) {
                $finaldata[$i][] = $i;
                $finaldata[$i][] = ($data->reconciliation_payment) ? $data->reconciliation_payment : " ";
                $company         = Company::model()->findByPk($data->company_id);
                $finaldata[$i][] = ($company->name) ? $company->name : " ";
                if ($data->reconciliation_table == 'jp_dailyvendors') {
                    $finaldata[$i][] = " ";
                } else if ($data->reconciliation_table == 'jp_dailyexpense') {
                    $data->reconciliation_parentid;
                    $value = Dailyexpense::model()->findByPk($data->reconciliation_parentid);
                    if ($value['exp_type_id'] != '') {
                        $value_data = Companyexpensetype::model()->findByPk($value['exp_type_id']);
                        $finaldata[$i][] = ($value_data['name']) ? $value_data['name'] : "l ";
                    } else {
                        $value_data = Deposit::model()->findByPk($value['expensehead_id']);
                        $finaldata[$i][] = ($value_data['deposit_name']) ? $value_data['deposit_name'] : " ";
                    }
                } else if ($data->reconciliation_table == 'jp_expenses') {
                    $value = Expenses::model()->findByPk($data->reconciliation_parentid);
                    if ($value['bill_id'] != '') {
                        $value_data = Bills::model()->findByPk($value['bill_id']);
                        $billno .= ($value_data['bill_number']) ? $value_data['bill_number'] . ' / ' : " ";
                    }
                    if ($value['exptype'] != '') {
                        $value_data = ExpenseType::model()->findByPk($value['exptype']);
                        $billno .= ($value_data['type_name']) ? $value_data['type_name'] : " ";
                    }
                    $finaldata[$i][] = ($billno) ? $billno : " ";
                } else {
                    $finaldata[$i][] = " ";
                }
                if ($data->reconciliation_table == 'jp_dailyvendors') {
                    $value = Dailyvendors::model()->findByPk($data->reconciliation_parentid);
                    if ($value['vendor_id'] != '') {
                        $value_data = Vendors::model()->findByPk($value['vendor_id']);
                        $finaldata[$i][] = ($value_data['name']) ? $value_data['name'] : " ";
                    } else {
                        $finaldata[$i][] = " ";
                    }
                } else if ($data->reconciliation_table == 'jp_expenses') {
                    $value = Expenses::model()->findByPk($data->reconciliation_parentid);
                    if ($value['vendor_id'] != '') {
                        $value_data = Vendors::model()->findByPk($value['vendor_id']);
                        $finaldata[$i][] = ($value_data['name']) ? $value_data['name'] : " ";
                    }else if($value['subcontractor_id'] != ''){
                        $value1 = SubcontractorPayment::model()->findByPk($value['subcontractor_id']);
                        if(!empty($value1)){
                            $datasub= Subcontractor::Model()->findByPk($value1["subcontractor_id"]);
                           
                            $finaldata[$i][] = ($datasub["subcontractor_name"]) ? $datasub["subcontractor_name"] : " ";
                        }
    
                    } else {
                        $finaldata[$i][] = " ";
                    }
                } else {
                    $finaldata[$i][] = " ";
                }
                $paymentType = $data->reconciliation_payment;

                if (strpos($paymentType, 'Payment') !== false) {
                    $finaldata[$i][] = ($data->reconciliation_amount) ? Controller::money_format_inr($data->reconciliation_amount, 2) : " ";
                    $final_amount1 += $data->reconciliation_amount;
                } else {
                    $finaldata[$i][] = "";
                }
                if (strpos($paymentType, 'Receipt') !== false) {
                    $finaldata[$i][] = ($data->reconciliation_amount) ? Controller::money_format_inr($data->reconciliation_amount, 2) : " ";
                    $final_amount2 += $data->reconciliation_amount;
                } else {
                    $finaldata[$i][] = "";
                }
                $bank = Bank::model()->findByPk($data->reconciliation_bank);
                $finaldata[$i][] = ($bank->bank_name) ? $bank->bank_name : " ";
                $finaldata[$i][] = ($data->reconciliation_chequeno) ? $data->reconciliation_chequeno : " ";
                if ($data->reconciliation_table == 'jp_dailyvendors') {
                    $value = Dailyvendors::model()->findByPk($data->reconciliation_parentid);
                    $finaldata[$i][] = ($value['description']) ? $value['description'] : " ";
                } else if ($data->reconciliation_table == 'jp_dailyexpense') {
                    $value = Dailyexpense::model()->findByPk($data->reconciliation_parentid);
                    $finaldata[$i][] = ($value['description']) ? $value['description'] : " ";
                } else if ($data->reconciliation_table == 'jp_expenses') {
                    $value = Expenses::model()->findByPk($data->reconciliation_parentid);
                    $finaldata[$i][] = ($value['description']) ? $value['description'] : " ";
                } else {
                    $finaldata[$i][] = " ";
                }
                $finaldata[$i][] = ($data->reconciliation_paymentdate) ? date("d-m-Y", strtotime($data->reconciliation_paymentdate)) : " ";
                $finaldata[$i][] = ($data->created_date) ? date("d-m-Y", strtotime($data->created_date)) : " ";
                $finaldata[$i][] = ($data->reconciliation_date) ? date("d-m-Y", strtotime($data->reconciliation_date)) : " ";
                $i++;
                $a = $i;
            }
        } else {
            $a = $i;
        }
        // $finaldata[$a][]  = '';
        // $finaldata[$a][]  = '';
        // $finaldata[$a][]  = '';
        // $finaldata[$a][]  = '';
        // $finaldata[$a][]  = 'Total = ';
        // $finaldata[$a][]  = Controller::money_format_inr($final_amount1,2);
        // $finaldata[$a][]  = Controller::money_format_inr($final_amount2,2);
        // $finaldata[$a][]  = '';
        // $finaldata[$a][]  = '';
        // $finaldata[$a][]  = '';
        // $finaldata[$a][]  = '';
        // $finaldata[$a][]  = '';

        Yii::import('ext.ECSVExport');
        $csv         = new ECSVExport($finaldata);
        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = "Bank_Reconcilation_Report" . date("d-m-Y") . ".csv";
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }
    public function actionReconciliationedit()
    {
        $tblpx              = Yii::app()->db->tablePrefix;
        $model              = new Reconciliation;
        $reconId            = $_REQUEST["reconId"];
        $recDate            = $_REQUEST["recDate"];
        $recNewDate         = date("Y-m-d", strtotime($recDate));
        $reconciliation     = Reconciliation::model()->findByPk($reconId);
        $reconTable         = $reconciliation->reconciliation_table;
        $reconParentId      = $reconciliation->reconciliation_parentid;
        $reconPayment       = $reconciliation->reconciliation_payment;
        $reconParentList    = $reconciliation->reconciliation_parentlist;
        if ($reconTable == "jp_expenses") {
            $pKey   = "exp_id";
        } else if ($reconTable == "jp_dailyexpense") {
            $pKey   = "dailyexp_id";
        } else if ($reconTable == "jp_dailyvendors") {
            $pKey   = "daily_v_id";
        } else if ($reconTable == "jp_subcontractor_payment") {
            $pKey   = "payment_id";
        }
        if ($recNewDate == "1970-01-01" && !empty($recDate)) {
            echo 1;
        } else {
            if (!empty($recDate)) {
                if ($reconPayment == "Subcontractor Payment") {
                    $recUpdate          = Yii::app()->db->createCommand("UPDATE " . $tblpx . "reconciliation SET reconciliation_date = '" . $recNewDate . "', reconciliation_status = 1 WHERE reconciliation_id = " . $reconId);

                    $recEParentUpdate   = Yii::app()->db->createCommand("UPDATE " . $tblpx . "subcontractor_payment SET reconciliation_date = '" . $recNewDate . "', reconciliation_status = 1 WHERE payment_id IN (SELECT subcontractor_id FROM " . $tblpx . "expenses WHERE exp_id IN (" . $reconParentList . "))");
                    $recEParentUpdate->execute();

                    $recPDateUpdate     = Yii::app()->db->createCommand("UPDATE " . $reconTable . " SET reconciliation_date = '" . $recNewDate . "', reconciliation_status = 1 WHERE " . $pKey . " IN (" . $reconParentList . ")");
                    $recPDateUpdate->execute();
                } else {
                    $recUpdate          = Yii::app()->db->createCommand("UPDATE " . $tblpx . "reconciliation SET reconciliation_date = '" . $recNewDate . "', reconciliation_status = 1 WHERE reconciliation_id = " . $reconId);
                    $recPDateUpdate     = Yii::app()->db->createCommand("UPDATE " . $reconTable . " SET reconciliation_date = '" . $recNewDate . "', reconciliation_status = 1 WHERE " . $pKey . " = " . $reconParentId);
                    $recPDateUpdate->execute();
                }
            } else {
                if ($reconPayment == "Subcontractor Payment") {
                    $recUpdate          = Yii::app()->db->createCommand("UPDATE " . $tblpx . "reconciliation SET reconciliation_date = NULL, reconciliation_status = 0 WHERE reconciliation_id = " . $reconId);

                    $recEParentUpdate   = Yii::app()->db->createCommand("UPDATE " . $tblpx . "subcontractor_payment SET reconciliation_date = NULL, reconciliation_status = 0 WHERE payment_id IN (SELECT subcontractor_id FROM " . $tblpx . "expenses WHERE exp_id IN (" . $reconParentList . "))");
                    $recEParentUpdate->execute();

                    $recParentUpdate    = Yii::app()->db->createCommand("UPDATE " . $reconTable . " SET reconciliation_date = NULL, reconciliation_status = 0 WHERE " . $pKey . " IN (" . $reconParentList . ")");
                    $recParentUpdate->execute();
                } else {
                    $recUpdate          = Yii::app()->db->createCommand("UPDATE " . $tblpx . "reconciliation SET reconciliation_date = NULL, reconciliation_status = 0 WHERE reconciliation_id = " . $reconId);
                    $recParentUpdate    = Yii::app()->db->createCommand("UPDATE " . $reconTable . " SET reconciliation_date = NULL, reconciliation_status = 0 WHERE " . $pKey . " = " . $reconParentId);
                    $recParentUpdate->execute();
                }
            }
            $recUpdate->execute();

            $sql = "SELECT SUM(reconciliation_amount) as totalamount FROM {$tblpx}reconciliation WHERE reconciliation_status = 1";
            if (isset($_REQUEST['payment']) && $_REQUEST['payment'] != '')
                $sql .= " AND reconciliation_payment LIKE '%" . $_REQUEST['payment'] . "%'";
            if (isset($_REQUEST['bank']) && $_REQUEST['bank'] != '')
                $sql .= " AND reconciliation_bank = {$_REQUEST['bank']}";
            if (isset($_REQUEST['company']) && $_REQUEST['company'] != '')
                $sql .= " AND company_id = {$_REQUEST['company']}";
            if (!empty($_REQUEST['fromdate']) && empty($_REQUEST['todate'])) {
                $sql .= " AND reconciliation_date >= '" . date('Y-m-d', strtotime($_REQUEST['fromdate'])) . "'";  // date is database date column field
            } else if (!empty($_REQUEST['todate']) && empty($_REQUEST['fromdate'])) {
                $sql .= " AND reconciliation_date <= '" . date('Y-m-d', strtotime($_REQUEST['todate'])) . "'";
            } else if (!empty($_REQUEST['todate']) && !empty($_REQUEST['fromdate'])) {
                $sql .= " AND reconciliation_date between '" . date('Y-m-d', strtotime($_REQUEST['fromdate'])) . "' and  '" . date('Y-m-d', strtotime($_REQUEST['todate'])) . "'";
            }
            $paymentData    = Yii::app()->db->createCommand($sql . " AND reconciliation_payment LIKE '%Payment%'")->queryRow();
            $paymentAmount  = $paymentData["totalamount"];
            $receiptData    = Yii::app()->db->createCommand($sql . " AND reconciliation_payment LIKE '%Receipt%'")->queryRow();
            $receiptAmount  = $receiptData["totalamount"];

            $resRec = $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $model->completedreconciliation(),
                'viewData' => array('paymentamount' => $paymentAmount, 'receiptamount' => $receiptAmount),
                'itemView' => '_reconreportnewview', 'template' => '<div>{summary}{sorter}</div><div class="tblwrap scrollTable"><div class="container1"><table cellpadding="10" class="table  list-view sorter">{items}</table></div></div>{pager}',
            ));
            return $resRec;
        }
    }

    public function actionDynamicbank()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $comId         = $_POST["comId"];
        $tblpx         = Yii::app()->db->tablePrefix;
        $bankData    = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}bank WHERE FIND_IN_SET(" . $comId . ", company_id);")->queryAll();
        $bankOptions = "<option value=''>-Select Bank-</option>";
        foreach ($bankData as $vData) {
            $bankOptions  .= "<option value='" . $vData["bank_id"] . "'>" . $vData["bank_name"] . "</option>";
        }
        echo json_encode(array('bank' => $bankOptions));
    }

    public function actionChangeStatus(){
    
        $id = $_POST['id'];
        $table = substr($_POST['table'],3);        

        $transaction = Yii::app()->db->beginTransaction();
        try {
            $model=Reconciliation::model()->findByPk($id);
            $model->reconciliation_date =NULL;
            $model->reconciliation_status = 0;
            $parentId = $model->reconciliation_parentid;            
            $parentModelName = $this->getModelName($table);                          
            $parentModel =   $parentModelName::model()->findByPk($parentId);
            $parentModel->reconciliation_date =NULL;
            $parentModel->reconciliation_status = NULL;

            if($model->reconciliation_payment =="Subcontractor Payment"){
               $subModel = SubcontractorPayment::model()->findByPk($parentModel->subcontractor_id);
               $subModel->reconciliation_date =NULL;
               $subModel->reconciliation_status = NULL;
            }
                    
            if($model->save()){  
                $success =1;        
                if(!$parentModel->save()){
                    $success =0;
                    throw new Exception(json_encode($parentModel->getErrors()));  
                }
                if($model->reconciliation_payment =="Subcontractor Payment"){
                    $subModel->save();
                }                
            }else{
                $success =0;
                throw new Exception(json_encode($model->getErrors()));                                
            }
            $transaction->commit();
        } catch(Exception $error) {
            $transaction->rollback();
            $success=0;
        } finally{
            if($success ==1){
                Yii::app()->user->setFlash('success', "Status Changed successfully");
            }else{
                Yii::app()->user->setFlash('error', "Error Occured");
            }
        }
    }
    public function actionbanktransaction_report(){
        if (isset($_REQUEST['date_from'])) {
            $date_from = $_REQUEST['date_from'];
        } else {
            $date_from = '';
        }
        if (isset($_REQUEST['date_to'])) {
            $date_to = $_REQUEST['date_to'];
        } else {
            $date_to = '';
        }
        if (isset($_REQUEST['bank_id'])){
            $bank_id = $_REQUEST['bank_id'];
        }else {
            $bank_id = '';
        }
        
        if (filter_input(INPUT_GET, 'export') == 'pdf') {
            $this->logo = $this->realpath_logo;
            $mPDF1 = Yii::app()->ePdf->mPDF();
            $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
            $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
            $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
            
            $template = $this->getTemplate($selectedtemplate);
            $mPDF1->SetHTMLHeader($template['header']);
            $mPDF1->SetHTMLFooter($template['footer']);
            $mPDF1->AddPage('','', '', '', '', 0,0,40, 40, 10,0);
            $mPDF1->WriteHTML($this->renderPartial('banktransaction_report', array('date_to' => $date_to, 'date_from' => $date_from,'bank_id' => $bank_id ), true));
            if ($_GET["ledger"] == 0) {
                $mPDF1->Output('Bank balance report.pdf', 'D');
            } else {
                $mPDF1->Output('Bank balance report - Ledger view.pdf', 'D');                
            }
        } else if(filter_input(INPUT_GET, 'export') == 'csv') {
            $exported_data = $this->renderPartial('banktransaction_report', array('date_to' => $date_to, 'date_from' => $date_from), true);
            $export_filename = 'bank-balance-report' . date('Ymd_His');
            ExportHelper::exportGridAsCSV($exported_data, $export_filename);
        }else{
            $this->render('banktransaction_report', array('date_to' => $date_to, 'date_from' => $date_from, 'bank_id' => $bank_id));
        }
    }
}
