<?php
/* @var $this LogController */
/* @var $model JpLog */

$this->breadcrumbs = array(
    'Jp Logs' => array('index'),
    'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#jp-log-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<style type="text/css">
    @media (min-width: 1024px) {
        .navbar-right {
            float: right !important;
            margin-right: -20px;
        }
    }
</style>
<div class="container">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <?php if (Yii::app()->user->hasFlash('success')) : ?>
        <div class="alert alert-success alert_box">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>


    <?php if (Yii::app()->user->hasFlash('error')) : ?>
        <div class="alert alert-danger alert_box">
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
    <?php endif; ?>
    <div class="">
        <div class="">
            <h2>Update Log</h2>
            <?php $this->renderPartial('_newsearch', array('model' => $model)) ?>

            <div id="actionlog">
                <?php $this->widget('zii.widgets.CListView', array(
                    'dataProvider' => $model->search(),
                    //'itemView'=>'_view',
                    'id' => 'jp-log-grid',
                    'sortableAttributes' => array(
                        'log_id',
                        'log_table',
                        'log_action',
                        'log_datetime',
                    ),
                    'itemView' => '_updateview', 'template' => '<div class="clearfix"><div class="pull-right">{summary}</div><div class="pull-left">{sorter}</div></div><div class="tblwrap scrollTable"><div class="container1"><table cellpadding="10" class="table  list-view sorter">{items}</table></div></div>{pager}',
                )); ?>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        $(document).on("click", ".icon-restore", function() {
            var logId = $(this).attr('data-id');
            var answer = confirm("Are you sure you want to restore?");
            if (answer) {
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    type: "POST",
                    data: {
                        'logId': logId
                    },
                    url: "<?php echo $this->createUrl('log/databackup') ?>",
                    success: function(response) {
                        location.reload();
                    }
                });
            }
        })
    })
    $(document).ajaxComplete(function() {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
    $(".alert_box").delay(3000).fadeOut();
</script>