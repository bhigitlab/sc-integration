<?php

class CompanyexpensetypeController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		/* return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('view','admin'),
				'users' => array('@'),
                                'expression' => 'yii::app()->user->role <=3',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		); */
            
            $accessArr = array();
           $accessauthArr = array();
           $controller = Yii::app()->controller->id;
           if(isset(Yii::app()->user->menuall)){
               if (array_key_exists($controller, Yii::app()->user->menuall)) {
                   $accessArr = Yii::app()->user->menuall[$controller];
               } 
           }

           if(isset(Yii::app()->user->menuauth)){
               if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                   $accessauthArr = Yii::app()->user->menuauth[$controller];
               }
           }
           $access_privlg = count($accessauthArr);
           return array(
               array('allow', // allow all users to perform 'index' and 'view' actions
                   'actions' => $accessArr,
                   'users' => array('@'),
                   'expression' => "$access_privlg > 0",
               ),

               array('allow', // allow admin user to perform 'admin' and 'delete' actions
                   'actions' => $accessauthArr,
                   'users' => array('@'),
                   'expression' => "$access_privlg > 0",
               ),
			   array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('bulkdelete' ),
                'users' => array('*'),
            ),
               array('deny',  // deny all users
                   'users'=>array('*'),
               ),
           );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Companyexpensetype;
                $model->type = 0;
		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['Companyexpensetype']))
		{
			$model->attributes=$_POST['Companyexpensetype'];
			$model->company_id   = Yii::app()->user->company_id;
			if($model->save()){
                        echo CHtml::script('$("#addCompanyExpensetype").dialog("close");window.parent.location.reload();');
                                        $this->redirect(array('companyexpensetype/admin'));
                        
                        }
		}
                if(isset($_GET['layout']))
                $this->layout = false;

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{        $tblpx = Yii::app()->db->tablePrefix;
		$model=$this->loadModel($id);
                $type= $model->type; 
                $data = Yii::app()->db->createCommand("select count(*) from " . $tblpx . "dailyexpense d where d.exp_type_id ='" . $id . "'")->queryScalar();
                
                if($data==0){
                  // do nothing
                }else{
                    $model->type = $type;
                }

		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['Companyexpensetype']))
		{
			$model->attributes=$_POST['Companyexpensetype'];
			$model->company_id   = Yii::app()->user->company_id;
			if($model->save())
				//$this->redirect(array('admin'));
				if(Yii::app()->user->returnUrl !=0){
					 $this->redirect(array('admin', 'Companyexpensetype_page' => Yii::app()->user->returnUrl));
				 } else {
					 $this->redirect(array('admin'));
				 }
		}
                if(isset($_GET['layout']))
                $this->layout = false;

		$this->render('update',array(
			'model'=>$model,'dailyexp'=>$data
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$msg='';
            try{
                $sql2 = "SELECT count(*) FROM `jp_dailyexpense` WHERE `exp_type_id` =".$id. " AND (`expensehead_type`=1 OR `expensehead_type`=2)";
				$relatedDailyExpensesCount = Yii::app()->db->createCommand($sql2)->queryScalar();
                
				if($relatedDailyExpensesCount){
					$msg=" Daily Expenses table";
				}  
				if( !$relatedDailyExpensesCount){ 
					 
                    if ($model->delete()) {
                        $response = [
                            'success' => 'success',
                            'message' => 'Record deleted successfully.',
							'msg'=>$msg,
                        ];
                    } else {
                        throw new Exception(json_encode($model->getErrors()));
                    
                    }
                }else{
                    $error_message = "You cannot delete this record because it is associated with other records in ".$msg;
                    throw new Exception(json_encode($error_message));
                }
            }catch(Exception $e){
                $error_message = "You cannot delete this record because it is associated with other records in ".$msg;
                $response = [
                    'success' => 'danger',
                    'message' =>$error_message,
					'msg'=>$msg,
					
                ];
            }            
            echo json_encode($response);		
	}
	public function actionbulkdelete(){
		$selectedIds = Yii::app()->request->getPost('ids');
		$errors =[];
		$msg=" Daily Expenses table";
		foreach($selectedIds as $id){
			if($this->alreadyUsedinDailyExpense($id)){
				$model = Companyexpensetype::model()->findByPk($id);
				$errors[] =$model->name;
			}else{
				$model = Companyexpensetype::model()->findByPk($id);
				$model->delete();
			}
		}
		if(!empty($errors)){
			$errors_str =implode($errors,',');
			$error_message = "Some of the selected records such as ".$errors_str. " already in use.So those records could not be deleted";
			$response = [
				'success' => 'danger',
				'message' =>$error_message,
				'msg'=>$msg,
				
			];
		}else{
			
			$error_message = "Successfully deleted";
			$response = [
				'success' => 'success',
				'message' =>$error_message,
				'msg'=>$msg,
				
			];
		}
		
		echo json_encode($response);
	}
	private function alreadyUsedinDailyExpense($id){
		$sql2 = "SELECT count(*) FROM `jp_dailyexpense` WHERE `exp_type_id` =".$id. " AND (`expensehead_type`=1 OR `expensehead_type`=2)";
		$relatedDailyExpensesCount = Yii::app()->db->createCommand($sql2)->queryScalar();
        return $relatedDailyExpensesCount >0;     
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Companyexpensetype');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Companyexpensetype('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Companyexpensetype']))
			$model->attributes=$_GET['Companyexpensetype'];

			$dataProvider=new CActiveDataProvider('Companyexpensetype');
			$this->render('admin',array(
				'model'=>$model,'dataProvider' => $dataProvider
			));
	}
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Companyexpensetype the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Companyexpensetype::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Companyexpensetype $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='companyexpensetype-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
}
