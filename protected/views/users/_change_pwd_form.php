<?php $this->breadcrumbs = array(
    'My profile'=> array('users/myprofile'),
    'Change password'
);
?>
<?php Yii::app()->clientScript->registerCss('myscript','
   
 .card-body{
    padding: 1.25rem;
 }
.example1consolesucces {
	color: green;
	background: black;
	padding: 15px;
	width:200px;
}

.flash-error{
	background-image: linear-gradient(to bottom, #f2dede 0, #e7c3c3 100%);
    border-color: #dca7a7;
	padding: 15px;
	color: #a94442;
}

.flash-success{
	background-image: linear-gradient(to bottom, #dff0d8 0, #c8e5bc 100%);
	padding: 15px;
	color: #3c763d;
    border-color: #b2dba1;
}
.parl_btn{margin-top: 5px;}
  
   ');
?>
<div class="container" id="expense">
    <h2>Change password</h2>
        
<?php
  foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class=" alert alert-' . $key . '">' . $message . "</div>\n";
    } 
?>    

<div class="card">
    <div class="card-body">
        <div class="form">

                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'users-form',
                    'enableAjaxValidation' => false,
                ));
                ?>

                <div class="row addRow">					
                    <div class="col-md-3">
                        <?php echo $form->labelEx($model, 'old_password'); ?>
                        <?php echo $form->passwordField($model, 'old_password', array('size' => 30, 'maxlength' => 32,'class'=>'form-control')); ?>
                        <?php echo $form->error($model, 'old_password'); ?>
                    </div>

                    <div class="col-md-3">
                        <?php echo $form->labelEx($model, 'New password'); ?>
                        <?php echo $form->passwordField($model, 'password', array('size' => 30, 'maxlength' => 32,'class'=>'form-control')); ?>
                        <?php echo $form->error($model, 'password'); ?>
                    </div>

                    <div class="col-md-3">
                        <?php echo $form->labelEx($model, 'retype_pwd'); ?>
                        <?php echo $form->passwordField($model, 'retype_pwd', array('size' => 30, 'maxlength' => 32,'class'=>'form-control')); ?>
                        <?php echo $form->error($model, 'retype_pwd'); ?>
                    </div>

                    <div class="col-md-3 buttons">
                        <label>&nbsp;</label>                        
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save',array('class'=>'btn btn-info btn-sm')); ?>                        
                    </div>
                </div>
                <?php $this->endWidget(); ?>

            </div><!-- form -->
        </div>
    </div>
</div>
<script>
     setTimeout(function() {
        $(".alert").hide('blind', {}, 500)
    }, 5000);
    </script>
