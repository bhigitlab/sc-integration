<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery.dataTables.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.dataTables.min.js"></script>
<div iv class="container">
    <div>
        <?php if (Yii::app()->user->hasFlash('error')): ?>
            <div class="alert alert-danger">
                <?php echo Yii::app()->user->getFlash('error'); ?>
            </div>
        <?php endif; ?>
        <?php if(Yii::app()->user->hasFlash('success')): ?>
            <div class="alert alert-success">
                <?php echo Yii::app()->user->getFlash('success'); ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="expenses-heading header-container">
        <h3>Consumption Request</h3>
        <!-- remove addentries class -->
        <div>
            <a class="btn btn-info addconsumption">Add consumption</a>
             <?php echo CHtml::button('Estimation', array(
                'class' => 'btn btn-info   ',
                'onclick' => 'window.open("' . Yii::app()->createUrl("purchase/itemestimation") . '", "_blank")'
            )); ?>
            <?php
            $pms_api_integration = ApiSettings::model()->pmsIntegrationStatus();
            if ($pms_api_integration == 1) {
                if (!empty($wpr_null_data) || !empty($wpr_material_datas)) {
                    $empty_project_count = count($wpr_null_data);
                    $empty_material_count = count($wpr_material_datas);

                    $total = $empty_project_count + $empty_material_count;
                    if ($total >= 1) {
                        echo CHtml::link('Refresh', array('warehouse/refreshButton', 'type' => 1), array('class' => 'btn btn-info', 'id' => 'warehouse_refresh'));
                    }
                }
            }
            ?>
        </div>
        <div id="loading" style="display: none"><img src="/themes/jpvcms/images/ajax-loader.gif" /></div>
    </div>

    <div id="addconsumptions" style="margin-top:50px;display:none;"></div>

    <div class="table-wrapper">
        <div id="msg"></div>
        <table class="table" id="consumption_table">
            <thead class="entry-table">
                <tr>
                    <th>Action</th>
                    <th>Sl No</th>
                    <th>Project</th>
                    <th>Warehouse</th>
                    <th>Task</th>
                    <th>Date</th>
                    <th>Requested From</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;

                foreach ($consume_data as $key => $value) {
                    //consumption Item
                    $consumptionItem = PmsAccWprItemConsumed::model()->findByAttributes(array('consumption_id' => $value['id']));

                    $tblpx = Yii::app()->db->tablePrefix;

                    $item_id = isset($value['item_id']) ? $value['item_id'] : null;
                    if ($item_id !== null && $item_id !== '') {
                        $command = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}specification WHERE id=:id");
                        $command->bindValue(':id', $item_id);
                        $desctiptions = $command->queryRow();
                    } else {
                        $desctiptions = null;
                    }

                    $parent_category = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}purchase_category WHERE id='" . $desctiptions['cat_id'] . "'")->queryRow();
                    if ($desctiptions['brand_id'] != NULL) {
                        $brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $desctiptions['brand_id'] . "")->queryRow();
                        $brand = '-' . $brand_details['brand_name'];
                    } else {
                        $brand = '';
                    }

                    ?>
                    <?php
                    $class = '';
                    $newclass = '';
                    if ($value['coms_project_id'] !== null) {

                        if ($value['status'] == '0') {
                            $styleval = 'background-color: #f8cbcb !important;';
                        } else if ($value['status'] == '2') {
                            $styleval = 'background-color: #f57275 !important;';
                        } else {
                            $styleval = '';
                        }
                    } else {
                        $styleval = 'background-color:#A9E9EC;';
                    }
                    ?>
                    <tr style="<?php echo $styleval; ?>">
                        <td>
                            <span class="icon icon-options-vertical popover-test" data-toggle="popover"
                                data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
                            <div class="popover-content hide">
                                <ul class="tooltip-hiden">
                                    <li>
                                        <?php
                                        // $material = Materials::model()->findByPk($value['coms_material_id']);
                                        $specification = null;
                                        if (!empty($material)) {
                                            $specification = $material->specification;
                                        }

                                        $warehouses = Warehouse::model()->findAllByAttributes(array('project_id' => $value['coms_project_id']));
                                        // $stockRecord = Warehousestock::model()->findAllByAttributes(array('warehousestock_warehouseid' => $value['warehouse_id'], 'warehousestock_itemid' => $value['item_id']));
                                    
                                        if (!empty($material) || !empty($specification) || !empty($warehouses) || !empty($stockRecord)) {
                                            // if($value['type_from']==2){
                                            //     echo CHtml::button('Edit', array(
                                            //         'class' => 'edit-btn btn btn-sm btn-default',
                                            //         'title' => 'Edit',
                                            //         'data-material-id' => $value['coms_material_id'],
                                            //         'data-project-id' => $value['coms_project_id'],
                                            //         'data-wpr-id' => $value['id']
                                            //     ));
                                            // }
                                    
                                        }
                                        ?>
                                    </li>
                                    <li>
                                        <?php
                                        if (!empty($value['warehouse_id'])) {
                                            if ($consumptionItem && $consumptionItem->item_rate != 0) {
                                                if ($value['status'] == 0) {
                                                    echo CHtml::button('Approve', array('class' => ' margin-right-5 btn btn-default btn-sm btn-approve', 'data-id' => $value['id']));
                                                    // if($value['type_from']==2){
                                                    echo CHtml::button('Reject', array('class' => 'margin-right-5 btn btn-default btn-sm btn-reject', 'data-id' => $value['id']));
                                                    // }
                                                }
                                            }
                                        }
                                        ?>
                                    </li>
                                    <li>
                                        <?php
                                        if ($value['status'] == 0) {
                                            echo CHtml::button('Edit', array(
                                                'class' => 'margin-right-5 editProject btn btn-default btn-sm',
                                                'data-toggle' => 'modal',
                                                'data-target' => '.edit',
                                                'data-id' => $value['id']
                                            ));
                                        } 
                                        // else if ($value['status'] == 1 || $value['status'] == 2) {
                                           
                                        // }
                                         echo CHtml::button('View', array(
                                                'class' => ' btn btn-default view_consumption btn-sm',
                                                'data-toggle' => 'modal',
                                                'data-target' => '.edit',
                                                'data-id' => $value['id']
                                            ));
                                        ?>
                                    </li>
                                </ul>
                            </div>
                        </td>
                        <td>
                            <?php echo $i; ?>
                        </td>
                        <td>
                            <?php
                            $projects = Projects::model()->findByPk($value['coms_project_id']);
                            echo isset($projects['name']) ? $projects['name'] : "";
                            ?>
                        </td>
                        <td>
                            <?php
                            $warehouse = Warehouse::model()->findByPk($value['warehouse_id']);
                            echo isset($warehouse['warehouse_name']) ? $warehouse['warehouse_name'] : "";
                            ?>
                        </td>
                        <td>
                            <?php echo isset($value['task_name']) ? $value['task_name'] : ""; ?>
                        </td>
                        <td>
                            <?php
                            echo isset($value['date'])
                                ? (new DateTime($value['date']))->format('d-m-y')
                                : "";
                            ?>
                        </td>
                        <td>
                            <?php
                            // $pms_api_integration_model=ApiSettings::model()->findByPk(1);
                            // $pms_api_integration =$pms_api_integration_model->api_integration_settings;
                            // if($pms_api_integration=1){
                            //     $pms='PMS';
                            //     echo $pms;
                            // }
                            // else{
                            //     $requestd_user = "";
                            //     if(!is_null($value['item_added_by'])){
                            //         $sql = "SELECT concat(first_name,' ',last_name) "
                            //         . " as fullname FROM `pms_users` "
                            //         . " WHERE userid=".$value['item_added_by'];
                            //         $requestd_user = Yii::app()->db->createCommand($sql)->queryScalar();
                            //     }
                            //     echo $requestd_user;
                            // }
                            if ($value['type_from'] == 1) {
                                $requested_from = 'ACCOUNTS';
                                echo $requested_from;
                            } else {
                                $requested_from = 'PMS';
                                echo $requested_from;
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($value['status'] == 0) {
                                echo 'Pending';
                            } else if ($value['status'] == 1) {
                                echo 'Approved';
                            } else {
                                echo 'Rejected';
                            }
                            ?>
                        </td>

                    </tr>
                    <?php $i++;
                } ?>
            </tbody>
        </table>
    </div>
</div>
<!-- 
<div id="editModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form id="editForm" class="form-horizontal">
                    <div class="row">
                        <div class="col-md-4" style="margin-left: 15px;">
                            <div class="form-group">
                                <label for="warehouseDropdown" class="control-label">Warehouse</label>
                                <select id="warehouseDropdown" class="form-control" name="warehouse">
                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4" style="margin-left: 15px;">
                            <div class="form-group">
                                <label for="specificationDropdown" class="control-label">Specification</label>
                                <select id="specificationDropdown" class="form-control" name="specification">
                                   
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2" style="margin-left: 15px;">
                            <div class="form-group">
                                <label for="rateField" class="control-label">Rate</label>
                                
                                <select id="rateField" class="form-control" name="rate">
                                   
                                </select>
                                <div id="rateError" style="color: red; margin-top: 5px;"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="saveChanges">Save changes</button>
            </div>
        </div>
    </div>
</div> -->
<div id="rejectModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-white">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Reject</h4>
            </div>
            <div class="modal-body margin-horizontal-10">
                <form id="rejectForm" class="form-horizontal">
                    <div class="form-group">
                        <label for="remarksField" class="control-label">Remarks</label>
                        <textarea id="remarksField" class="form-control" name="remarks" rows="4"
                            placeholder="Enter your remarks"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitReject">Submit</button>
            </div>
        </div>
    </div>
</div>

<script>
    var url = '<?php echo Yii::app()->getBaseUrl(true); ?>';
    $(document).on('click', '.btn-reject', function () {
        $('#rejectModal').modal('show');
    });

    $(document).on("click", ".btn-approve", function () {
        var id = $(this).attr('data-id');
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "<?php echo $this->createUrl('warehouse/approveConsumedQuantity') ?>",
            data: {
                id: id
            },
            success: function (response) {
                if (response.response === 'warning') {
                    var errorAlert = '<div class="alert alert-warning alert-dismissible" role="alert">' +
                                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                        '<span aria-hidden="true">&times;</span>' +
                                    '</button>' +
                                    response.msg +
                                '</div>';
                $("#msg").html(errorAlert);
                } else {
                    $("#msg").html('<div class="alert alert-' + response.response + '">' + response.msg + '</div>');

                    setTimeout(function () {
                        window.location.reload(1);
                    }, 2000);
                }
            }
        })
    })

    $('#submitReject').on('click', function () {
        var remarks = $('#remarksField').val();
        var id = $('.btn-reject').data('id');
        if (remarks.trim() === '') {
            alert('Please enter your remarks.');
            return;
        }

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "<?php echo $this->createUrl('warehouse/rejectConsumedQuantity') ?>",
            data: {
                id: id,
                remarks: remarks,
            },
            success: function (response) {
                if (response.success) {
                    alert('Remarks submitted successfully.');
                    $('#rejectModal').modal('hide');
                    location.reload();
                } else {
                    alert('Error submitting remarks.');
                }
            }
        });
    });

    $(".popover-test").popover({
        html: true,
        content: function () {
            return $(this).next('.popover-content').html();
        }
    });
    $('[data-toggle=popover]').on('click', function (e) {
        $('[data-toggle=popover]').not(this).popover('hide');
    });
    $('body').on('hidden.bs.popover', function (e) {
        $(e.target).data("bs.popover").inState.click = false;
    });
    $('body').on('click', function (e) {
        $('[data-toggle=popover]').each(function () {
            // hide any open popovers when the anywhere else in the body is clicked
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
    $(document).ajaxComplete(function () {
        $(".popover-test").popover({
            html: true,
            content: function () {
                return $(this).next('.popover-content').html();
            }
        });
    });
    $(document).ready(function () {
        $(document).on('click', '.edit-btn', function () {
            var materialId = $(this).data('material-id');
            var projectId = $(this).data('project-id');
            var wprId = $(this).data('wpr-id');

            // Fetch specifications
            $.ajax({
                type: 'GET',
                url: '<?php echo $this->createUrl('warehouse/getSpecifications'); ?>',
                data: { material_id: materialId },
                dataType: 'json',
                success: function (response) {
                    if (response.status === 'success') {
                        var specifications = response.specifications;
                        var dropdown = $('#specificationDropdown');
                        dropdown.empty();
                        dropdown.append('<option value="">-Select Specification-</option>');
                        specifications.forEach(function (spec) {
                            dropdown.append('<option value="' + spec.id + '">' + spec.specification + '</option>');
                        });
                    } else {
                        alert('Failed to fetch specifications');
                    }
                }
            });

            // Fetch warehouses
            $.ajax({
                type: 'GET',
                url: '<?php echo $this->createUrl('warehouse/getWarehouses'); ?>',
                data: { projectId: projectId },
                dataType: 'json',
                success: function (response) {
                    if (response.status === 'success') {
                        var warehouses = response.warehouses;
                        var dropdown = $('#warehouseDropdown');
                        dropdown.empty();
                        dropdown.append('<option value="">-Select Warehouse-</option>');
                        warehouses.forEach(function (warehouse) {
                            dropdown.append('<option value="' + warehouse.id + '">' + warehouse.name + '</option>');
                        });

                        $('#editForm').append('<input type="hidden" id="editWprId" name="wpr_id" value="' + wprId + '">');
                        $('#editModal').modal('show');
                    } else {
                        alert('Failed to fetch warehouses');
                    }
                }
            });
        });

        $('#saveChanges').click(function () {
            var wprId = $('#editWprId').val();
            var selectedSpecification = $('#specificationDropdown').val();
            var selectedWarehouse = $('#warehouseDropdown').val();
            var selectedRate = $('#rateField').val();
            $.ajax({
                type: 'POST',
                url: '<?php echo $this->createUrl('warehouse/updateSpecificationAndWarehouse'); ?>',
                data: {
                    specification_id: selectedSpecification,
                    warehouse_id: selectedWarehouse,
                    rate: selectedRate,
                    wpr_id: wprId
                },
                dataType: 'json',
                success: function (response) {
                    if (response.status === 'success') {
                        $('#editModal').modal('hide');
                        window.location.reload();
                    } else {
                        alert('Failed to update specification and warehouse');
                    }
                }
            });
        });
    });


    $("#consumption_table").dataTable({
        "scrollY": "360px",
        "scrollX": true,
        "scrollCollapse": true,
        "columnDefs": [
            {
                "searchable": false,
                "targets": [0]
            },
            { "bSortable": false, "aTargets": [0, 1, 2, 3, 4, 5] }
        ]
    });
    $(".dataTables_filter").wrap("<div class=\'custom-table-head-wrap\'></div>");
    $(".custom-table-head-wrap").prepend("<div class=\'legend status-legend\'><span class=\'project_not_mapped\'></span>Project Not Mapped In Integration</div>")

    $(document).ready(function () {
        $('#specificationDropdown').change(function () {
            updateRateBasedOnSelection();
        });

        $('#warehouseDropdown').change(function () {
            updateRateBasedOnSelection();
        });

        function updateRateBasedOnSelection() {
            var specificationId = $('#specificationDropdown').val();
            var warehouseId = $('#warehouseDropdown').val();
            var wprId = $('#editWprId').val();

            $.ajax({
                type: 'POST',
                url: '<?php echo $this->createUrl('warehouse/getRate'); ?>',
                data: {
                    specification_id: specificationId,
                    warehouse_id: warehouseId,
                    wpr_id: wprId
                },
                dataType: 'json',
                success: function (response) {
                    var rateDropdown = $('#rateField');
                    var rateErrorDiv = $('#rateError');

                    rateDropdown.empty();
                    rateErrorDiv.text('');
                    $('#saveChanges').prop('disabled', false);
                    if (response.status === 'success') {
                        response.rates.forEach(function (rate) {
                            rateDropdown.append('<option value="' + rate.rate + '">' + rate.rate + '</option>');
                        });
                    } else {
                        rateErrorDiv.text(response.message);
                        $('#saveChanges').prop('disabled', true);
                    }
                },
                error: function (xhr, status, error) {
                    console.error('AJAX Error:', status, error);
                    $('#rateField').val('');
                    alert('An error occurred while fetching the rate.');
                }
            });
        }
    });

    function closeaction() {
        $('#addconsumptions').slideUp(500);
        location.reload();
    }
    

    $(document).ready(function () {
        $('.addconsumption').click(function () {
            $('.loading-overlay').addClass('is-active');
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: "<?php echo $this->createUrl('warehouse/createconsumption&layout=1') ?>",
                success: function (response) {
                    $('#addconsumptions').html(response).slideDown();
                },
            });
        });
        $(document).on('click', '.editProject', function () {
            $('.loading-overlay').addClass('is-active');
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: "<?php echo $this->createUrl('warehouse/updateconsumption&layout=1&id=') ?>" + id,
                success: function (response) {
                    $('.loading-overlay').removeClass('is-active');
                    $('#addconsumptions').html(response).slideDown();
                },
            });
        });

        $(document).on('click', '.view_consumption', function () {
            var id = $(this).attr('data-id');
            location.href = url + '/index.php?r=wh/warehouse/viewconsumption&layout=1&id=' + id;
        });

        $('.deleteProject').click(function () {
            if (confirm("Are you sure you want to delete this project?")) {
                $('.loading-overlay').addClass('is-active');
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "POST",
                    url: "<?php echo $this->createUrl('PmsAccWprItemConsumed/delete&layout=1&id=') ?>" + id,
                    dataType: 'json',
                    success: function (response) {
                        console.log();
                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                        $("#errMsg").show()
                            .html('<div class="alert alert-' + response.success + '">' + response.message + '</div>')
                            .fadeOut(10000);
                        setTimeout(function () {
                            location.reload(true);
                        }, 1000);
                    },
                    error: function () {
                        $('.loading-overlay').removeClass('is-active');
                        alert('There was an error deleting the project. Please try again.');
                    }
                });
            }
        });



        jQuery(function ($) {
            $('#addconsumptiontype').on('keydown', function (event) {
                if (event.keyCode == 13) {
                    $("#materialssearch").submit();
                }
            });
        });

    });
    $(document).ajaxComplete(function () {
        $('.loading-overlay').removeClass('is-active');
        $('#loading').hide();
    });
</script>