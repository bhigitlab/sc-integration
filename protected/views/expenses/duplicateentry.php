<?php
/* @var $this ExpensesController */
/* @var $model Expenses */

$this->breadcrumbs = array(
    'Expenses' => array('index'),
    'Create',
);

/* $this->menu=array(
    array('label'=>'List Expenses', 'url'=>array('index')),
    array('label'=>'Manage Expenses', 'url'=>array('admin')),
); */
?>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<div class="container" id="expense">
    <div class="expenses-heading">
        <h3>DUPLICATE ENTRIES</h3>
    </div>
    <div class="popwindow" style="display:none;">
        <div id="details">

        </div>
    </div>
    <div>
        <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $alldata,
            'itemView' => '_alllisting',
            'template' => '<div class="sub-heading"><div>{sorter}</div>{summary}</div><div id="parent"><table cellpadding="10" class="table" id="fixTable">{items}</table></div>{pager}',
            'emptyText' => '<table cellpadding="10" class="table">
        <thead class="entry-table">
            <tr>
                <th></th>
                <th>Sl No.</th>
                <th>Section</th>
                <th>Company</th>
                <th>Project</th>
                <th>Subcontractor</th>
                <th>Bill Number</th>
                <th>Invoice Number</th>
                <th>Date</th>
                <th>Expense Head / Transaction Head</th>
                <th>Vendor</th>
                <th>Expense Type / Transaction Type</th>
                <th>Petty Cash By</th>
                <th>Bank</th>
                <th>Cheque No</th>
                <th>Description</th>
                <th>Amount</th>
                <th>Tax</th>
                <th>Total</th>
                <th>Amount Paid</th>
                <th>TDS</th>
                <th>Receipt Type</th>
                <th>Receipt</th>
                <th>Purchase Type / Payment Type</th>
                <th>Amount To Beneficiary</th>
                <th>Paid To</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="26" class="text-center">No results found</td>
            </tr>
        </tbody>
        </table>',
        )); ?>
    </div>
</div>
<style>
    #details {
        background-color: #fff;
        border-color: #fff;
        box-shadow: 0px 0px 8px 6px rgba(0, 0, 0, 0.25);
        color: #555;
        padding: 10px;
        border-radius: 4px;
    }

    #parent {
        max-height: 75vh;
        border: 1px solid #ddd;
        border-top: 0px;
        border-right: 0px;
        overflow: auto;
    }

    table thead th {
        background-color: #eee;
    }

    .left_fix {
        background-color: #fafafa;
    }
</style>
<script>
    $(document).ready(function () {
        // $("#fixTable").tableHeadFixer({ 'left': 3, 'foot': true, 'head': true });

        $(".popover-test").popover({
            html: true,
            content: function () {
                return $(this).next('.popover-content').html();
            }
        });

        $(".duplicaterow").click(function (e) {
            $(".popwindow").show();
            var row = $(this).parent("tr").attr("data-id");
            var projectid = $("#projectid" + row).val();
            var amount = $("#amount" + row).val();
            var paid = $("#paid" + row).val();
            var date = $("#date" + row).val();
            var description = $("#description" + row).val();
            $.ajax({
                type: "GET",
                data: { projectid: projectid, amount: amount, paid: paid, date: date, description: description },
                url: "<?php echo $this->createUrl('expenses/getduplicateentries') ?>",
                success: function (data) {
                    $("#details").html(data);
                }
            });
        });

        $(".duplicaterow2").click(function (e) {
            $(".popwindow").show();
            var row = $(this).parent("tr").attr("data-id");
            var amount = $("#amount" + row).val();
            var paid = $("#paid" + row).val();
            var date = $("#date" + row).val();
            var description = $("#description" + row).val();
            $.ajax({
                type: "GET",
                data: { amount: amount, paid: paid, date: date, description: description },
                url: "<?php echo $this->createUrl('dailyexpense/getduplicateentries') ?>",
                success: function (data) {
                    $("#details").html(data);
                }
            });
        });

        $(".duplicaterow3").click(function (e) {
            $(".popwindow").show();
            var row = $(this).parent("tr").attr("data-id");
            var subcontractor = $("#subcontractor" + row).val();
            var project = $("#projectid" + row).val();
            var amount = $("#amount" + row).val();
            var date = $("#date" + row).val();
            var description = $("#description" + row).val();
            $.ajax({
                type: "GET",
                data: { subcontractor: subcontractor, project: project, amount: amount, date: date, description: description },
                url: "<?php echo $this->createUrl('subcontractorpayment/getduplicateentries') ?>",
                success: function (data) {
                    $("#details").html(data);
                }
            });
        });

        $(".duplicaterow4").click(function (e) {
            $(".popwindow").show();
            var row = $(this).parent("tr").attr("data-id");
            var vendor = $("#vendor" + row).val();
            var date = $("#date" + row).val();
            var amount = $("#amount" + row).val();
            var description = $("#description" + row).val();
            $.ajax({
                type: "GET",
                data: { vendor: vendor, date: date, amount: amount, description: description },
                url: "<?php echo $this->createUrl('vendors/getduplicateentries') ?>",
                success: function (data) {
                    $("#details").html(data);
                }
            });
        });

        $(".duplicaterow5").click(function (e) {
            $(".popwindow").show();
            var row = $(this).parent("tr").attr("data-id");
            var vendor = $("#vendor" + row).val();
            var billno = $("#billno" + row).val();
            var project = $("#projectid" + row).val();
            $.ajax({
                type: "GET",
                data: { vendor: vendor, billno: billno, project: project },
                url: "<?php echo $this->createUrl('bills/getduplicateentries') ?>",
                success: function (data) {
                    $("#details").html(data);
                }
            });
        });

        $(document).on('click', '#details', function (e) {
            $(".popwindow").hide();
            $("#details").html("");
        });

        $(document).on("click", ".btn-ignore", function () {
            var id = $(this).attr('data-id');
            var section = $(this).attr('data-section');

            $.ajax({
                type: "POST",
                data: { id: id, section: section },
                dataType: "json",
                url: "<?php echo $this->createUrl('expenses/ignoreDuplicateEntry') ?>",
                success: function (data) {
                    alert(data.msg);
                    setTimeout(function () {
                        window.location.reload(1);
                    }, 1000);
                }
            });
        })

        $(document).on("click", ".btn-delete", function () {
            var id = $(this).attr('data-id');
            var section = $(this).attr('data-section');
            $.ajax({
                type: "POST",
                data: { id: id, section: section },
                dataType: "json",
                url: "<?php echo $this->createUrl('expenses/deleteDuplicateEntry') ?>",
                success: function (data) {
                    alert(data.msg);
                    location.reload();
                }
            });
        })

    });
</script>