<?php
/* @var $this QuotationItemMasterController */
/* @var $model QuotationItemMaster */

$this->breadcrumbs=array(
	'Quotation Item Masters'=>array('index'),
	'Create',
);


?>

<h1 class="padding-y-10 padding-left-15">Create QuotationItemMaster</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'itemdata' => $itemdata,
				'itemfinish' => $itemfinish,)); ?>

<script>

	$("#QuotationItemMaster_quotation_category_id").change(function(){
		$(".getmaster_rate").trigger("change");
	})

	$(".getmaster_rate").change(function(){
		var elem = $(this);
		var material = $(this).val();
		var category = $("#QuotationItemMaster_quotation_category_id").val();
		$.ajax({
			type: "POST",
			data: {
				material: material,
				category: category,            
			},
			url: "<?php echo $this->createUrl('quotationItemMaster/getmaterialRate') ?>",
			success: function(data) {
				if(data==""){
					alert('No rate added');
					$(elem).parents(".addRow").find(".ratedata").attr("readonly",false).val("");
					$(elem).parents(".addRow").find(".additional_rate").attr("readonly",false).val("");
					return;
				}else{
					$(elem).parents(".addRow").find(".ratedata").attr("readonly",true).val(data);
					$(elem).parents(".addRow").find(".additional_rate").attr("readonly",true).val(data);
					// find(".additional_rate")
				}
			}
		});
	})
</script>