<?php

/**
 * This is the model class for table "{{consumption_request}}".
 *
 * The followings are the available columns in table '{{consumption_request}}':
 * @property integer $id
 * @property integer $coms_project_id
 * @property integer $warehouse_id
 * @property integer $wpr_id
 * @property string $task_name
 * @property integer $pms_project_id
 * @property string $created_at
 * @property string $updated_at
 */
class ConsumptionRequest extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{consumption_request}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('coms_project_id, warehouse_id, wpr_id, pms_project_id', 'numerical', 'integerOnly'=>true),
			array('task_name', 'length', 'max'=>255),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, coms_project_id, warehouse_id, wpr_id, task_name, pms_project_id, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'coms_project_id' => 'Coms Project',
			'warehouse_id' => 'Warehouse',
			'wpr_id' => 'Wpr',
			'task_name' => 'Task Name',
			'pms_project_id' => 'Pms Project',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('coms_project_id',$this->coms_project_id);
		$criteria->compare('warehouse_id',$this->warehouse_id);
		$criteria->compare('wpr_id',$this->wpr_id);
		$criteria->compare('task_name',$this->task_name,true);
		$criteria->compare('pms_project_id',$this->pms_project_id);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ConsumptionRequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
