<?php
/* @var $this EquipmentsEntriesController */
/* @var $model EquipmentsEntries */

$this->breadcrumbs=array(
	'Equipments Entries'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EquipmentsEntries', 'url'=>array('index')),
	array('label'=>'Manage EquipmentsEntries', 'url'=>array('admin')),
);
?>

<h1>Create EquipmentsEntries</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>