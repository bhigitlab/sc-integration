ALTER TABLE `jp_warehousestock` ADD `consumed_quantity` INT NULL DEFAULT '0' AFTER `updated_by`;
ALTER TABLE `jp_material_estimation` ADD `used_quantity` INT NULL DEFAULT '0' COMMENT 'defines used quantity in consumption' AFTER `created_at`;

ALTER TABLE `jp_material_estimation` ADD `used_amount` INT(11) NULL DEFAULT '0' COMMENT 'used amount in consumption' AFTER `used_quantity`;

