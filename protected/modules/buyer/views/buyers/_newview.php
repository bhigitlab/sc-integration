<?php
if ($index == 0) {
?>
    <thead>
        <tr>
            <?php
            if ((isset(Yii::app()->user->role) && (in_array('/buyer/buyers/viewinvoice', Yii::app()->user->menuauthlist)) || (in_array('/buyer/buyers/viewinvoice', Yii::app()->user->menuauthlist)))) {
            ?>
                <th></th>
            <?php } ?>
            <th>Sl No</th>
            <th>Company</th>
            <th>Invoice No</th>
            <th>Client</th>
            <th>Project</th>
            <th>Date</th>
            <!-- <th>Status</th> -->
            <th>Sub Total</th>
            <th>Total Amount </th>

            <th>Balance</th>
            
        </tr>
    </thead>
<?php } ?>
<tr>

<?php
    if ((isset(Yii::app()->user->role) && (in_array('/buyer/buyers/viewinvoice', Yii::app()->user->menuauthlist)) || (in_array('/buyer/buyers/updateinvoice', Yii::app()->user->menuauthlist)))) {
    ?>
        <td style="width:30px;text-align: center;">
            <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
            <div class="popover-content hide">
                <ul class="tooltip-hiden">
                    <?php
                    if ((in_array('/buyer/buyers/viewinvoice', Yii::app()->user->menuauthlist))) {
                    ?>
                        <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/buyer/buyers/viewinvoice', array('invid' => $data->buyer_invoice_id)); ?>" class="btn btn-xs btn-default">View</a></li>
                    <?php } ?>
                    <?php
                    if ((in_array('/buyer/buyers/updateinvoice', Yii::app()->user->menuauthlist))) { ?>
                        <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/buyer/buyers/updateinvoice', array('invid' => $data->buyer_invoice_id)); ?>" class="btn btn-xs btn-default">Edit</a></li>
                    <?php

                    } ?>
                </ul>
            </div>
        </td>
    <?php } ?>
    <td width="50"><?php echo  $index + 1; ?></td>
    <td width="50">
        <?php
        $company = Company::model()->findByPk($data['company_id']);
        echo $company['name'];
        ?>
    </td>
    <td><?php echo $data->invoice_no; ?></td>

    <td><?php echo $data->buyer->name; ?></td>
    <td>
        <?php
        $pmodel  = Projects::model()->findBypk($data->project_id);
        echo $pmodel->name;
        ?>
    </td>
    <td><?php echo  date("d-m-Y", strtotime($data->date)); ?></td>
    <!-- <td>
        <?php
        if ($data->invoice_status == 'saved') {
            echo ucfirst($data->invoice_status);
        } else { ?>
            <div style="cursor:pointer" class="update_status invoice_status_<?php echo $data->buyer_invoice_id; ?>" id="<?php echo $data->buyer_invoice_id; ?>" data-toggle="tooltip" data-placement="bottom" title="Change status to saved">Draft</div>
            <div id="loader" class="loader_<?php echo $data->buyer_invoice_id ?>"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
        <?php }
        ?>
    </td> -->
    <td class="text-right"><?php echo Controller::money_format_inr($data->total_amount, 2, 1);  ?></td>
    <td class="text-right"><?php echo Controller::money_format_inr(($data->total_amount + $data->tax_amount), 2, 1); ?></td>
    <?php

    $balanceAmount = ($data->total_amount + $data->tax_amount);
    ?>

    <td class="text-right"><?php echo Controller::money_format_inr($balanceAmount, 2, 1); ?></td>


</tr>
<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>