<?php
/* @var $this ProjectTypeController */
/* @var $model ProjectType */

$this->breadcrumbs=array(
	'Project Types'=>array('index'),
	$model->ptid,
);

$this->menu=array(
	array('label'=>'List ProjectType', 'url'=>array('index')),
	array('label'=>'Create ProjectType', 'url'=>array('create')),
	array('label'=>'Update ProjectType', 'url'=>array('update', 'id'=>$model->ptid)),
	array('label'=>'Delete ProjectType', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ptid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProjectType', 'url'=>array('admin')),
);
?>

<h1>View ProjectType</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ptid',
		'project_type',
	),
)); ?>
