<?php
/* @var $this TaxSlabsController */
/* @var $model TaxSlabs */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tax_slab_value'); ?>
		<?php echo $form->textField($model,'tax_slab_value'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cgst'); ?>
		<?php echo $form->textField($model,'cgst'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sgst'); ?>
		<?php echo $form->textField($model,'sgst'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'igst'); ?>
		<?php echo $form->textField($model,'igst'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_date'); ?>
		<?php echo $form->textField($model,'updated_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->