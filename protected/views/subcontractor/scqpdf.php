<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">

<div class="container">
    <?php
    $company_address = Company::model()->findBypk($model->company_id);
    ?>
    <table border=0>
        <tbody>
            <tr>
                <td class="img-hold">
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" alt="img" style='margin-top:-45px;' />
                </td>
                <td class="text-center">
                    <h1><?php echo Yii::app()->name ?></h1>
                </td>
            </tr>
        </tbody>
    </table>
    <table border=0>
        <tbody>
            <tr>
                <td class="text-center">
                    <p><?php echo ((isset($company_address['address']) && !empty($company_address['address'])) ? ($company_address['address']) : ""); ?><br>
                        PIN : <?php echo ((isset($company_address['pincode']) && !empty($company_address['pincode'])) ? ($company_address['pincode']) : ""); ?>,&nbsp;
                        PHONE : <?php echo ((isset($company_address['phone']) && !empty($company_address['phone'])) ? ($company_address['phone']) : ""); ?><br>
                        EMAIL : <?php echo ((isset($company_address['email_id']) && !empty($company_address['email_id'])) ? ($company_address['email_id']) : ""); ?> <br>
                        GST NO: <?php echo isset($company_address["company_gstnum"]) ? $company_address["company_gstnum"] : ''; ?></p><br>
                </td>
            </tr>
        </tbody>
    </table>
    <h4 class="page_heading">WORK ORDER</h4>
    <?php
    $company = Company::model()->findByPk($model->company_id);
    ?>

    <table class="details">
        <tr>
            <td><label>VENDOR : </label><b>Mr.MILTON</b></td>
            <td><label>Sign Date : </label><b><?php echo (isset($client)) ? $client : ''; ?></b></td>
        </tr>
        <tr>
            <td><label>SITE : </label><b>LIC EMPLOYE UNION</b></td>
            <td><label>Completion Date: : </label><b></b></td>
        </tr>
        <tr>
            <td><label>Tel : </label><b></b></td>
        </tr>
    </table>
    <br>
    <h4 class="text-center">Work Details</h4>
    <br>
    <table class="detail-table">
        <tbody>
            <tr bgcolor="#ddd">
                <th>SI.No</th>
                <th>Descreeription</th>
                <th>Quantity</th>
                <th>Units</th>
                <th>Rate</th>
                <th>Total Amount</th>
            </tr>
            <tr bgcolor="#add8e6">
                <th colspan="6" class="text-left border-0"> MAIN WORKS</th>
            </tr>
            <tr>
                <td>1</td>
                <td>MATT WORK 150 X 150 CM</td>
                <td> 13.00 </td>
                <td>Nos</td>
                <td></td>
                <td class="text-right">-</td>
            </tr>
            <tr>
                <td>2</td>
                <td>MATT WORK 100 X 100 CM</td>
                <td> 6.00 </td>
                <td>Nos</td>
                <td></td>
                <td class="text-right">-</td>
            </tr>
            <tr>
                <td>3</td>
                <td>Tie beam</td>
                <td> 301.00 </td>
                <td>RFT</td>
                <td></td>
                <td class="text-right">-</td>
            </tr>
            <tr>
                <td>4</td>
                <td>PLINT BRICK WORK</td>
                <td> 830.00 </td>
                <td>Nos</td>
                <td></td>
                <td class="text-right">-</td>
            </tr>
            <tr>
                <td>5</td>
                <td>INSIDE PLASTRING</td>
                <td> 900.00 </td>
                <td>Sqft</td>
                <td></td>
                <td class="text-right">-</td>
            </tr>
            <tr>
                <td>6</td>
                <td>"Brick And RCC work above plinth imcluding pcc &
                    belt"</td>
                <td> 1,080.00 </td>
                <td>Sqft</td>
                <td></td>
                <td class="text-right">-</td>
            </tr>
            <tr>
                <td>7</td>
                <td></td>
                <td> 1,080.00 </td>
                <td>Sqft</td>
                <td></td>
                <td class="text-right">-</td>
            </tr>
            <tr>
                <td>8</td>
                <td></td>
                <td> 123.00 </td>
                <td>Sqft</td>
                <td></td>
                <td class="text-right">-</td>
            </tr>
            <tr>
                <td colspan="5" class="text-right">Total</td>
                <td class="text-right">-</td>
            </tr>
            <tr>
                <th colspan="6">In Words: Eleven Lakh Ninty Five Thousand Six Hundred And Sixty Two Only</th>
            </tr>
        </tbody>
    </table>
    <br>
    <table class="detail-table">
        <tbody>
            <tr bgcolor="#add8e6">
                <th colspan="6" class="text-left border-0"> Payment Terms</th>
            </tr>
            <tr>
                <td>1</td>
                <td>Advace Payment - Completed work</td>
                <td></td>
            </tr>
            <tr>
                <td>2</td>
                <td>Advance</td>
                <td></td>
            </tr>
            <tr>
                <td>3</td>
                <td>Completion of Tie Beam </td>
                <td></td>
            </tr>
            <tr>
                <td>4</td>
                <td>Completion of Brick work and Plastring </td>
                <td></td>
            </tr>
            <tr>
                <td>5</td>
                <td>Completion Rcc Coloumns all- 19 nos (in Ground Floor) </td>
                <td></td>
            </tr>
            <tr>
                <td>6</td>
                <td>Completion of Rcc Roof slab in Ground Floor </td>
                <td></td>
            </tr>
            <tr>
                <td>7</td>
                <td>Completion of Brick Work In Ground Floor at Lintel Level </td>
                <td></td>
            </tr>
            <tr>
                <td>8</td>
                <td>Completion of RCC LINTEL AND SHADE IN GROUND FLOOR </td>
                <td></td>
            </tr>
            <tr>
                <td>9</td>
                <td>Completion of RCC Coloumn in First Floor- 12 nos </td>
                <td></td>
            </tr>
            <tr>
                <td>10</td>
                <td>Completion of Rcc Roof slab in Ground Floor</td>
                <td></td>
            </tr>
            <tr>
                <td>11</td>
                <td>Completion of Brick work at Lintel Level - First Floor </td>
                <td></td>
            </tr>
            <tr>
                <td>12</td>
                <td>Completion of RCC lintel & Shade - First Floor </td>
                <td></td>
            </tr>
            <tr>
                <td>13</td>
                <td>Completion of RCC Roof Work - Stair Room </td>
                <td></td>
            </tr>
            <tr>
                <td>14</td>
                <td>Completion of Parapet </td>
                <td></td>
            </tr>
            <tr>
                <td>15</td>
                <td>Completion of out side 1 Plastring - one long and one short side </td>
                <td></td>
            </tr>
            <tr>
                <td>16</td>
                <td>Completion of out side 2 Plastring - one long and one short side </td>
                <td></td>
            </tr>
            <tr>
                <td>17</td>
                <td>Completion of Ground Interior Floor Plastring </td>
                <td></td>
            </tr>
            <tr>
                <td>18</td>
                <td>Completion of First interior Floor Plastring </td>
                <td></td>
            </tr>
            <tr>
                <td>19</td>
                <td>Completion of ALL WORKS </td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2" class="text-right">Total</td>
                <td class="text-right">-</td>
            </tr>
        </tbody>
    </table>
    <br><br>
    <b>Note:-</b>
    <br>
    <p> 1.Balance Payment as per actual Measurement & Final Bill.</p>
    <p> 2.All Materials Loading and Unlading at site is Vendor Scope</p>
    <p> 3.Scaffolding and All Tools For Working should be Vender Scope</p>
    <p> 4.Total No Of days for Work Complete is 150 Working Days Expect Public Holydays and Sundays</p>
    <p> 5.For Late Days 2000 rs per day Consider as Penalty will be cut off From Last Bill.</p>
    <p> 6.Finishing and Quality Should be Maintain As per company standard otherwise Will redoing</p>
    <p> 7.No Extra payment wont be allowed without Approval and Work Order</p>
    <p> 8.1 percentage TDS will cut off From Total Bill</p>
    <p> 9.Curing and Cleaning of site should maintain in proper manner</p>
    <p> 10.Meaurement consider as total plinth areas(Excludes Shade, Projections,Flower Beds,
        pin Walls and all type Rcc Extention)</p>

    <table>
        <tr>
            <td>Approved BY <br><br>
                <b>For <?php echo Yii::app()->name ?></b></td>
            <td>Agreed BY <br><br>
                Vender Name & Add.
            </td>
        </tr>
    </table>
</div>