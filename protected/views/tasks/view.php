<?php
/* @var $this TasksController */
/* @var $model Tasks */

$this->breadcrumbs = array(
    'Tasks' => array('index'),
    $model->title,
);

if (Yii::app()->user->role == 3) {
    $this->menu = array(
        array('label' => 'My Tasks', 'url' => array('mytask')),);
} else {

    $this->menu = array(
        array('label' => 'List Tasks', 'url' => array('index')),
        array('label' => 'Create Tasks', 'url' => array('create')),
        array('label' => 'Update Tasks', 'url' => array('update', 'id' => $model->tskid)),
        array('label' => 'Delete Tasks', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->tskid), 'confirm' => 'Are you sure you want to delete this item?')),
        array('label' => 'Manage Tasks', 'url' => array('admin')),
    );
}
?>

<h2>View Task: <span><?php echo $model->title; ?></span></h2>
<hr/>
<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'title',
        array(
            'name' => 'project_id',
            'type' => 'raw',
            'value' => (isset($model->project->name) ? $model->project->name : '<span class="null">Not set</span>'),
        ),
        'start_date',
        'due_date',
        array(
            'name' => 'status',
            'type' => 'raw',
            'value' => $model->status0->caption,
        ),
        array(
            'name' => 'priority',
            'type' => 'raw',
            'value' => $model->priority0->caption,
        ),
        'hourly_rate',
        array(
            'name' => 'assigned_to',
            'type' => 'raw',
            'value' => $model->assignedTo->first_name,
        ),
        array(
            'name' => 'report_to',
            'type' => 'raw',
            'value' => $model->reportTo->first_name,
        ),
        array(
            'name' => 'coordinator',
            'type' => 'raw',
            'value' => $model->coordinator0->first_name,
        ),
        array(
            'name' => 'billable',
            'type' => 'raw',
            'value' => $model->billable0->caption,
        ),
        'total_hrs',
        array('name'=>'description','value'=>nl2br($model->description),'type'=>'raw'),
        'created_date',
        array(
            'name' => 'created_by',
            'type' => 'raw',
            'value' => $model->createdBy->first_name,
        )
    ),
));


        $criteria = new CDbCriteria();
        $criteria->select = 'teid,tskid, date_format(`entry_date`, "%d/%m/%Y") as tentry_date , hours, billable, work_type, description';
        $criteria->condition = 'tskid = :tskid';
        $criteria->order = 'entry_date ASC';
        $criteria->params = array('tskid' => $model->tskid);

        $weekDetailEntries = TimeEntry::model()->findAll($criteria);
       
        ?>
<hr/>
<h2><span>Time Entries</span></h2>
<hr/>
<table cellpadding="3" cellspacing="0" class="timehours">
    <thead>
        <tr class="tr_title">
            <th width="100">Work type</th><th width="350">Description</th>
            <th width="70">Date</th><th width="50">Hours</th><th width="50">Billable</th>
        </tr>
    </thead>
    <?php
    if (count($weekDetailEntries) == 0) {
        ?>
        <tr class="even">
            <td colspan="9">This week's timesheet does not have any time added. 
                Click add time to start adding time.</td>
        </tr>
        <?php
    }
    ?>
    <?php
    $total_hours = 0;
    $k = 1;
    $billable = 0;
    $non_billable = 0;
    
    foreach ($weekDetailEntries as $tdentry) {
        $total_hours +=$tdentry->hours;
        //Edit link for each row
       if($tdentry->billable==3)
           $billable += $tdentry->hours;
       else
           $non_billable += $tdentry->hours;
        ?>
        <tr class='<?php echo ($k % 2 == 1 ? 'odd' : 'even'); ?>'>
            <td><?php echo WorkType::model()->findByPk($tdentry->work_type)->work_type; ?></td>
            <td><?php echo $tdentry->description; ?></td>
            <td><?php echo $tdentry->tentry_date; ?></td>
            <th class="totaltr"><?php echo $tdentry->hours; ?></th>
            <td><?php echo Status::model()->findByPk($tdentry->billable)->caption; ?></td>
            <!--<td width="72"><?php //echo CHtml::ajaxLink('Delete', array("cdelete", "id" => $tdentry->teid), array('complete' => 'js:function(request){$("#yt1").click();}')); ?> </td>-->
        </tr>

        <?php
        $k++;
    }
    ?>
    <tr class="totaltr">
        <td colspan="2" class="tbl_title">&nbsp;</td>
        <th>Billable<br/>Non-Billable<br/>Total</th>
        <th><?php echo number_format($billable, 2);?><br />
            <?php echo number_format($non_billable, 2);?><br />
            <?php echo number_format($total_hours, 2); ?>
        </th><td>&nbsp;</td>
    </tr>
</table>
