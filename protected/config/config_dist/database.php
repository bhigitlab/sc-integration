<?php

define('_DB','DB_NAME');
define ('_USER', 'DB_USERNAME');
define ('_PASS', 'DB_PASSWORD');
define ('_HOST', 'localhost');


return array(
    'connectionString' => 'mysql:host=' . (_HOST) . ';dbname=' . (_DB),
    'emulatePrepare' => true,
    'username' => (_USER),
    'password' => (_PASS),
    'charset' => 'utf8',
    'tablePrefix' => 'jp_',
    'initSQLs'=>["SET SQL_BIG_SELECTS=1"]
);
