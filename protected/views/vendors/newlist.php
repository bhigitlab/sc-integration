<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Vendors',
);
$page = Yii::app()->request->getParam('Vendors_page');
if ($page != '') {
    Yii::app()->user->setReturnUrl($page);
} else {
    Yii::app()->user->setReturnUrl(0);
}
$sixth_column_width = Yii::app()->user->role == 1 || Yii::app()->user->role == 2 ? ["width" => "350px", "targets" => 6] : '';

?>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<?php if (Yii::app()->user->hasFlash('success')): ?>
    <div class="flash-holder flash-success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
    <?php
endif;
if (Yii::app()->user->hasFlash('error')):
    ?>
    <div class="alert alert-danger  alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php endif; ?>
<div class="container" id="vendors">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="expenses-heading sub-heading mb-10">
        <h3>Vendors</h3>
        <?php if ((isset(Yii::app()->user->role) && (in_array('/vendors/create', Yii::app()->user->menuauthlist)))) { ?>
            <button class="create_vendor btn btn-info">Add Vendor</button>
        <?php } ?>
        <!-- remove addentries class -->
        <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
    </div>
    <div id="vendorform" style="display:none;"></div>
    <?php //$this->renderPartial('_search', array('model' => $model)) ?>

    <div id="msg"></div>

    <div class="table-wrapper">
        <?php
        $data_count = $dataProvider->getTotalItemCount();
        if ($data_count > 0) {
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_view',
                'template' => '<div class=""><table cellpadding="10" class="table" id="vendortable">{items}</table></div>',
                'ajaxUpdate' => false,
                'viewData' => array('sixth_column_width'=>$sixth_column_width)
            ));
        } else {
            echo 'No results found.';
        } ?>
        <script>
            $(document).ready(function () {
                $("#vendortable").dataTable({
                    "scrollY": "360px",
                    "scrollX": true,
                    "scrollCollapse": true,
                    "columnDefs": [
                        {
                            "searchable": false,
                            "targets": [0, 3]
                        },
                        { "bSortable": false, "aTargets": [-1] },
                        <?= json_encode( $sixth_column_width) ?>,
                    ],
                });
                var table = $("#vendortable").DataTable();
                $("#Vendors_name").on("keyup", function () {
                    var vendorname = $(this).val();
                    table
                        .columns(1)
                        .search(vendorname)
                        .draw();
                });
                $("#clear").on("click", function () {
                    var selectedText = " ";
                    table
                        .columns(1)
                        .search(selectedText)
                        .draw();
                });
            });

            function closeaction() {
                $('#vendorform').slideUp();
            }

            function editvendor(vendor_id) {
                var id = vendor_id;
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    type: "GET",
                    url: "<?php echo $this->createUrl('vendors/update&layout=1&id=') ?>" + id,
                    success: function (response) {
                        $("#vendorform").html(response).slideDown();

                    }
                });
            };

            function deletevendor(vendor_id) {
                var answer = confirm("Are you sure you want to delete?");
                if (answer) {
                    var id = vendor_id;
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "<?php echo $this->createUrl('vendors/deletevendor&id=') ?>" + id,
                        success: function (response) {
                            $("#msg").html("<div class='alert alert-" + response.response + "'>" + response.msg + "</div>");
                            setTimeout(() => {
                                location.reload();
                            }, 2000);

                        }
                    });
                }
            };

            $(document).ready(function () {
                $('#loading').hide();
                $('.create_vendor').click(function () {
                    $('.loading-overlay').addClass('is-active');
                    $.ajax({
                        type: "GET",
                        url: "<?php echo $this->createUrl('vendors/create&layout=1') ?>",
                        success: function (response) {
                            $("#vendorform").html(response).slideDown();
                        }
                    });
                });


                jQuery(function ($) {
                    $('#vendor').on('keydown', function (event) {
                        if (event.keyCode == 13) {
                            $("#vendorssearch").submit();
                        }
                    });
                });
            });
            $(document).ajaxComplete(function () {
                $('.loading-overlay').removeClass('is-active');
                $('#loading').hide();
            });
        </script>



    </div>
</div>


<style>
    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    .panel {
        border: 1px solid #ddd;
    }

    .panel-heading {
        background-color: #eee;
        height: 40px;
    }
</style>