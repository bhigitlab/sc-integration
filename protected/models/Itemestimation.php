<?php

/**
 * This is the model class for table "{{itemestimation}}".
 *
 * The followings are the available columns in table '{{itemestimation}}':
 * @property integer $itemestimation_id
 * @property integer $project_id
 * @property string $purchase_type
 * @property integer $category_id
 * @property double $itemestimation_width
 * @property double $itemestimation_height
 * @property double $itemestimation_length
 * @property string $itemestimation_unit
 * @property double $itemestimation_quantity
 * @property double $itemestimation_price
 * @property double $itemestimation_amount
 * @property string $itemestimation_description
 * @property integer $itemestimation_status
 */
class Itemestimation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{itemestimation}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_id, itemestimation_status', 'required'),
			array('project_id, category_id, itemestimation_status', 'numerical', 'integerOnly'=>true),
			array('itemestimation_width, itemestimation_height, itemestimation_length, itemestimation_quantity, itemestimation_price, itemestimation_amount', 'numerical'),
			array('purchase_type', 'length', 'max'=>1),
			array('itemestimation_unit', 'length', 'max'=>50),
			array('itemestimation_description', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('itemestimation_id, project_id, purchase_type, category_id, itemestimation_width, itemestimation_height, itemestimation_length, itemestimation_unit, itemestimation_quantity, itemestimation_price, itemestimation_amount, itemestimation_description, itemestimation_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'itemestimation_id' => 'Itemestimation',
			'project_id' => 'Project',
			'purchase_type' => 'Purchase Type',
			'category_id' => 'Category',
			'itemestimation_width' => 'Itemestimation Width',
			'itemestimation_height' => 'Itemestimation Height',
			'itemestimation_length' => 'Itemestimation Length',
			'itemestimation_unit' => 'Itemestimation Unit',
			'itemestimation_quantity' => 'Itemestimation Quantity',
			'itemestimation_price' => 'Itemestimation Price',
			'itemestimation_amount' => 'Itemestimation Amount',
			'itemestimation_description' => 'Itemestimation Description',
			'itemestimation_status' => 'Itemestimation Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('itemestimation_id',$this->itemestimation_id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('purchase_type',$this->purchase_type,true);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('itemestimation_width',$this->itemestimation_width);
		$criteria->compare('itemestimation_height',$this->itemestimation_height);
		$criteria->compare('itemestimation_length',$this->itemestimation_length);
		$criteria->compare('itemestimation_unit',$this->itemestimation_unit,true);
		$criteria->compare('itemestimation_quantity',$this->itemestimation_quantity);
		$criteria->compare('itemestimation_price',$this->itemestimation_price);
		$criteria->compare('itemestimation_amount',$this->itemestimation_amount);
		$criteria->compare('itemestimation_description',$this->itemestimation_description,true);
		$criteria->compare('itemestimation_status',$this->itemestimation_status);
		 $criteria->order = 'itemestimation_id DESC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Itemestimation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
