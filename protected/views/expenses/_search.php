<?php
/* @var $this ExpensesController */
/* @var $model Expenses */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'expense_id'); ?>
		<?php echo $form->textField($model,'expense_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'expense_date'); ?>
		<?php echo $form->textField($model,'expense_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'project_id'); ?>
		<?php echo $form->textField($model,'project_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bill_id'); ?>
		<?php echo $form->textField($model,'bill_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'expensetype_id'); ?>
		<?php echo $form->textField($model,'expensetype_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vendor_id'); ?>
		<?php echo $form->textField($model,'vendor_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'expense_amount'); ?>
		<?php echo $form->textField($model,'expense_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'expense_sgstp'); ?>
		<?php echo $form->textField($model,'expense_sgstp'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'expense_sgst'); ?>
		<?php echo $form->textField($model,'expense_sgst'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'expense_cgstp'); ?>
		<?php echo $form->textField($model,'expense_cgstp'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'expense_cgst'); ?>
		<?php echo $form->textField($model,'expense_cgst'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'expense_totalamount'); ?>
		<?php echo $form->textField($model,'expense_totalamount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>300)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'receipt_type'); ?>
		<?php echo $form->textField($model,'receipt_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'receipt'); ?>
		<?php echo $form->textField($model,'receipt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'purchase_type'); ?>
		<?php echo $form->textField($model,'purchase_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'paidamount'); ?>
		<?php echo $form->textField($model,'paidamount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_date'); ?>
		<?php echo $form->textField($model,'updated_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->