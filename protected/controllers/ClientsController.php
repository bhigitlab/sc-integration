<?php

class ClientsController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
          
        /* return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('view', 'viewpartial', 'newlist'),
                'users' => array('@'),
                'expression' => 'yii::app()->user->role<=3',
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
                'expression' => 'yii::app()->user->role<=2',
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('delete'),
                'users' => array('@'),
                'expression' => 'yii::app()->user->role==1',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        ); */
           $accessArr = array();
           $accessauthArr = array();
           $controller = Yii::app()->controller->id;
           if(isset(Yii::app()->user->menuall)){
               if (array_key_exists($controller, Yii::app()->user->menuall)) {
                   $accessArr = Yii::app()->user->menuall[$controller];
               } 
           }

           if(isset(Yii::app()->user->menuauth)){
               if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                   $accessauthArr = Yii::app()->user->menuauth[$controller];
               }
           }
           $access_privlg = count($accessauthArr);
           return array(
               array('allow', // allow all users to perform 'index' and 'view' actions
                   'actions' => $accessArr,
                   'users' => array('@'),
                   'expression' => "$access_privlg > 0",
               ),

               array('allow', // allow admin user to perform 'admin' and 'delete' actions
                   'actions' => $accessauthArr,
                   'users' => array('@'),
                   'expression' => "$access_privlg > 0",
               ),
               array('deny',  // deny all users
                   'users'=>array('*'),
               ),
           );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionViewpartial($id) {
        $this->renderPartial('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionView1($id) {
        if (Yii::app()->request->isAjaxRequest) {
            //outputProcessing = true because including css-files ...
            $this->renderPartial('view', array(
                'model' => $this->loadModel($id),
                    ), false, true);
            //js-code to open the dialog    
            if (!empty($_GET['asDialog']))
                echo CHtml::script('$("#dlg-clients-view").dialog("open")');
            Yii::app()->end();
        } else
            $listproject = Projects::model()->findAllByAttributes(array(
                'client_id' => $id,
            ));

        $this->render('view', array(
            'model' => $this->loadModel($id), 'getdata' => $listproject,
        ));
    }

    public function actionView($id) {
//        	if ( isset( $_GET[ 'pageSizes' ] ) )
//{
//Yii::app()->user->setState( 'pageSizes', (int) $_GET[ 'pageSizes' ] );
//unset( $_GET[ 'pageSizes' ] );
//} 


        if (Yii::app()->request->isAjaxRequest) {
            //outputProcessing = true because including css-files ...
            $this->renderPartial('view', array(
                'model' => $this->loadModel($id),
                    ), false, true);
            //js-code to open the dialog    
            if (!empty($_GET['asDialog']))
                echo CHtml::script('$("#dlg-clients-view").dialog("open")');
            Yii::app()->end();
        } else
            $Criteria = new CDbCriteria();
        $Criteria->select = "*";

        $Criteria->compare('clntid', $id);
        $Criteria->group = 'pjctid';
        $count_deal = PaymentReport::model()->count($Criteria);


        $pages = new CPagination($count_deal);
        $pages->pageSize = 5;
        $pages->applyLimit($Criteria);
        $listproject = PaymentReport::model()->findAll($Criteria);
        $this->render('view', array(
            'model' => $this->loadModel($id), 'getdata' => $listproject, 'pages' => $pages, 'clid' => $id
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
		
		 if(isset($_GET['layout']))
                $this->layout = false;
        $model = new Clients;
        $tblpx = Yii::app()->db->tablePrefix;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
		

        if (isset($_POST['Clients'])) { 
			
			//print_r($_POST['Clients']); die;         
            $model->attributes = $_POST['Clients'];
            
            if($_POST['Clients']['project_type'] != 2 )
            {
			$model->local_address = '';
            $model->contact_person = '';
			}else{
            $model->local_address = $_POST['Clients']['local_address'];
            $model->contact_person = $_POST['Clients']['contact_person'];
			}
            $model->created_by = yii::app()->user->id;
            $model->created_date = date('Y-m-d');

            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            if (!empty($_POST['Clients']['company_id'])) {
                $company = implode(",",$_POST['Clients']['company_id']);
                $model->company_id = $company;
            } else {
                $model->company_id = NULL;
            } 
            if ($model->save()) {
                if ($_POST['execute_api'] == 1) {
                $pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();

                if($pms_api_integration ==1){
                    $request = [
                        'origin' => 'coms',
                        'client_id' =>$model->cid,
                        'name' => $model->name,
                        'project_type' => $model->project_type,
                        'status' => $model->status 
                    ];
                    $slug="api/create-client";
                    $method="POST";
                        
                    $globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
                }
            }

                $this->redirect(array('newList'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    /* public function actionUpdate($id)
      {
      $model=$this->loadModel($id);

      // Uncomment the following line if AJAX validation is needed
      $this->performAjaxValidation($model);

      if(isset($_POST['Clients']))
      {
      $model->attributes=$_POST['Clients'];

      $model->updated_by = yii::app()->user->id;
      $model->updated_date = date('Y-m-d');

      if($model->save())
      $this->redirect(array('view','id'=>$model->cid));
      }

      $this->render('update',array(
      'model'=>$model,
      ));
      }
     */


    public function actionUpdate($id) {
	    $tblpx = Yii::app()->db->tablePrefix;	
		 if(isset($_GET['layout']))
                $this->layout = false;
        $model = $this->loadModel($id);
        

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        
        $userids = array();
        if (isset($_POST['Clients'])) {
            $model->attributes = $_POST['Clients'];
            
            if($_POST['Clients']['project_type'] != 2 )
            {
			$model->local_address = '';
            $model->contact_person = '';
			}else{
            $model->local_address = $_POST['Clients']['local_address'];
            $model->contact_person = $_POST['Clients']['contact_person'];
			}
            
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            if (!empty($_POST['Clients']['company_id'])) {
                $company = implode(",",$_POST['Clients']['company_id']);
                $model->company_id = $company;
            } else {
                $model->company_id = NULL;
            } 
            if ($model->save()) {
                
                $pms_api_integration=ApiSettings::model()->pmsIntegrationStatus();
                    if ($_POST['execute_api'] == 1) {
                        if($pms_api_integration ==1){
                            $request = [
                                'origin' => 'coms',
                                'client_id' =>$model->cid,
                                'name' => $model->name,
                                'project_type' => $model->project_type,
                                'status' => $model->status ,
                                'client_mapping_id'=>$model->client_mapping_id
                            ];

                            if (!empty($model->client_mapping_id)) {
                                $slug="api/update-client";
                                $request['client_mapping_id'] = $model->client_mapping_id;
                            } else {
                                $slug = "api/create-client";
                            }
                            $method="POST";
                            $globalapi_response = GlobalApiCall::callApi($slug, $method, $request);
                        }
                    }
                    if(Yii::app()->user->returnUrl !=0){
                        $this->redirect(array('newlist', 'Clients_page' => Yii::app()->user->returnUrl));
                    } else {
                        $this->redirect(array('newlist'));
                    }
                
            }
        }
        if ($id) {
            $userids = Users::model()->find(array(
                'select' => 'username',
                'condition' => 'client_id = :client_id',
                'params' => array(':client_id' => $id),
            ));            
        }
        $this->render('update', array(
            'model' => $model, 'users' => $userids
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
//    public function actionDelete($id) {
//        $this->loadModel($id)->delete();
//
//        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
//        if (!isset($_GET['ajax']))
//            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
//    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $model = new Clients('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Clients']))
            $model->attributes = $_GET['Clients'];

        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Clients('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Clients']))
            $model->attributes = $_GET['Clients'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Clients::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'clients-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionNewlist() {
        $model = new Clients('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Clients']))
            $model->attributes = $_GET['Clients'];

        if (Yii::app()->user->role == 1) {

            $this->render('newlist', array(
                'model' => $model,
                'dataProvider' => $model->search(),
            ));
        } else {
            $this->render('newlist', array(
                'model' => $model,
                'dataProvider' => $model->search(Yii::app()->user->id),
            ));
        }
    }
    public function actionremoveclient() {

        $id = $_POST['cid'];
        $model = $this->loadModel($id);
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if (!$model->delete()) {
                $success_status = 0;
                throw new Exception(json_encode($model->getErrors()));
            } else {
                $success_status = 1;
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Data Deleted Successfully'));
            } else {
                if ($error->errorInfo[1] == 1451) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }

}
