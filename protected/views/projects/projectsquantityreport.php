<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>

<?php
/* @var $this ProjectsController */

/* @var $model Projects */

Yii::app()->clientScript->registerScript('search', "
    $('#btSubmit').click(function(){
        $('#projectform').submit();
    });
");
$tblpx = Yii::app()->db->tablePrefix;
?>
<div class="container" id="project">
    <div class="sub-heading mb-10">
        <h3>Project Quantity Reports</h3>
    </div>
    <div class="page_filter clearfix custom-form-style">
        <form id="projectform"
            action="<?php $url = Yii::app()->createAbsoluteUrl("projects/projectsquantityreport"); ?>" method="POST">
            <div class="search-form">
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-3">
                        <?php echo CHtml::label('Projects  ', ''); ?>
                        <?php
                        echo CHtml::dropDownList('project_id', 'project_id', CHtml::listData(Projects::model()->findAll(array(
                            'select' => array('pid, name'),
                            'order' => 'name',
                            'condition' => 'company_id=' . Yii::app()->user->company_id . '',
                            'distinct' => true
                        )), 'pid', 'name'), array('empty' => 'Select Project', 'style' => 'padding:2px 0px;', 'id' => 'project_id', 'options' => array($project_id => array('selected' => true))));
                        ?>
                    </div>
                    <div class="form-group col-xs-12 col-sm-3 col-md-2 text-sm-left text-right">
                        <label class="d-sm-block d-none">&nbsp;</label>
                        <div>
                            <?php echo CHtml::submitButton('GO', array('id' => 'btSubmit', 'class' => 'btn btn-sm btn-primary')); ?>
                            <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('projectsquantityreport') . '"', 'class' => 'btn btn-sm btn-default')); ?>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <?php
    if (!empty($project_id)) {
        if (!empty($itemmodelcat)) {
            ?>
            <div id="contenthtml" class="contentdiv">
                <div class="header-container mb-10">
                    <div></div>
                    <h3 class="text-center">Item Estimation Report of
                        <?php echo $model["name"]; ?>
                    </h3>
                    <div class="btn-container">
                        <a style="cursor:pointer;" id="download" class="btn btn-info" title="SAVE AS EXCEL"
                            href="<?php echo $this->createAbsoluteUrl('projects/ExportItemQuantityReportCSV', array("project_id" => $project_id, "fromdate" => $date_from, "todate" => $date_to)) ?>">
                            <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i></a>
                        <a style="cursor:pointer;" id="download" class="btn btn-info" title="SAVE AS PDF"
                            href="<?php echo $this->createAbsoluteUrl('projects/saveitemquantityreportpdf', array("project_id" => $project_id, "fromdate" => $date_from, "todate" => $date_to)) ?>">
                            <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered ">
                        <thead>

                            <tr>
                                <td colspan="10"><b>Client : </b><?php echo $model["clientname"]; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="10"><b>Project Name : </b><?php echo $model["name"]; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="10"><b>Site : </b><?php echo $model["site"]; ?>
                                </td>
                            </tr>

                            <tr>
                                <th>Sl No</th>
                                <th>Item Name</th>
                                <th>Unit</th>
                                <th>Type</th>
                                <th>Estimation Quantity</th>
                                <th>Estimation Amount</th>
                                <th>Actual Quantity</th>
                                <th>Actual Amount</th>
                                <th>Quantity Variation</th>
                                <th>Amount Variation</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            $billItemGrandTotal = 0;
                            foreach ($itemmodelcat as $modelcat) {
                                $billitemTotal = ($modelcat["billitemamount"] - $modelcat["billitemdiscountamount"]) + $modelcat["billitemtaxamount"];
                                $billItemGrandTotal = $billItemGrandTotal + $billitemTotal;
                                if ($modelcat["purchase_type"] == "A") {
                                    $itemType = "By Length";
                                } else if ($modelcat["purchase_type"] == "G") {
                                    $itemType = "By Width and height";
                                } else if ($modelcat["purchase_type"] == "O") {
                                    $itemType = "By Quantity";
                                } else {
                                    $itemType = "";
                                }
                                $quantityVariation = $modelcat["itemestimation_quantity"] - $modelcat["billitemquantity"];
                                $amountVariation = $modelcat["itemestimation_amount"] - $billitemTotal;
                                if ($modelcat["itemestimation_quantity"] < $modelcat["billitemquantity"]) {
                                    $style = " style='background:#DD1035; color:#FFFFFF'";
                                } else if ($modelcat["itemestimation_quantity"] > $modelcat["billitemquantity"]) {
                                    $style = " style='background:#006600; color:#FFFFFF'";
                                } else {
                                    $style = "";
                                }
                                ?>
                                <tr>
                                    <td>
                                        <?php echo $i; ?>
                                    </td>
                                    <td>
                                        <?php echo $modelcat["category_name"] . " - " . $modelcat["brand_name"] . " - " . $modelcat["specification"]; ?>
                                    </td>
                                    <td>
                                        <?php echo $modelcat["itemestimation_unit"]; ?>
                                    </td>
                                    <td>
                                        <?php echo $itemType; ?>
                                    </td>
                                    <td>
                                        <?php echo $modelcat["itemestimation_quantity"]; ?>
                                    </td>
                                    <td class="text-right">
                                        <?php echo $modelcat["itemestimation_amount"]; ?>
                                    </td>
                                    <td<?php echo $style; ?>>
                                        <?php echo Controller::money_format_inr($modelcat["billitemquantity"], 2); ?>
                                        </td>
                                        <td class="text-right">
                                            <?php echo Controller::money_format_inr($billitemTotal, 2); ?>
                                        </td>
                                        <td>
                                            <?php echo Controller::money_format_inr($quantityVariation, 2); ?>
                                        </td>
                                        <td class="text-right">
                                            <?php echo Controller::money_format_inr($amountVariation, 2); ?>
                                        </td>
                                </tr>
                                <?php
                                $i = $i + 1;
                            }
                            $totalAmountVariation = $itemmodelcat[0]["itemestimationsum"] - $billItemGrandTotal;
                            ?>
                        <tbody>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th class="text-right">
                                    <?php echo $itemmodelcat[0]["itemestimationsum"]; ?>
                                </th>
                                <th></th>
                                <th class="text-right">
                                    <?php echo Controller::money_format_inr($billItemGrandTotal, 2); ?>
                                </th>
                                <th></th>
                                <th class="text-right">
                                    <?php echo Controller::money_format_inr($totalAmountVariation, 2); ?>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
        } else {
            echo "No estimations for this project.";
        }
    } else {
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_newprojectquantityreport',
            'template' => '<div>{summary}{sorter}</div><div class="table-responsive"><table class="table table-bordered table-hover">{items}</table></div>{pager}',
        ));
    }
    ?>
</div>
<script>
    $(document).ready(function () {
        $(".select_box").select2();
        $("#project_id").select2();
        $("#date_from").datepicker({
            dateFormat: 'dd-M-yy',
            maxDate: $("#date_to").val()
        });
        $("#date_to").datepicker({
            dateFormat: 'dd-M-yy',
        });
        $("#date_from").change(function () {
            $("#date_to").datepicker('option', 'minDate', $(this).val());
        });
        $("#date_to").change(function () {
            $("#date_from").datepicker('option', 'maxDate', $(this).val());
        });
    });
    (function () {
        'use strict';
        var container = document.querySelector('.container1');
        var table = document.querySelector('table');
        var leftHeaders = [].concat.apply([], document.querySelectorAll('tbody th'));
        var topHeaders = [].concat.apply([], document.querySelectorAll('thead th'));
        var topLeft = document.createElement('div');
        var computed = window.getComputedStyle(topHeaders[0]);
        container.appendChild(topLeft);
        topLeft.classList.add('top-left');
        topLeft.style.width = computed.width;
        topLeft.style.height = computed.height;

        container.addEventListener('scroll', function (e) {
            var x = container.scrollLeft;
            var y = container.scrollTop;

            leftHeaders.forEach(function (leftHeader) {
                leftHeader.style.transform = translate(x, 0);
            });
            topHeaders.forEach(function (topHeader, i) {
                if (i === 0) {
                    topHeader.style.transform = translate(0, y);
                } else {
                    topHeader.style.transform = translate(0, y);
                }
            });
            topLeft.style.transform = translate(x, y);
        });

        function translate(x, y) {
            return 'translate(' + x + 'px, ' + y + 'px)';
        }
    })();
</script>