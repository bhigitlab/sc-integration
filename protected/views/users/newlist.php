<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Users',
);
?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container" id="user">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix">
        <?php        
        if (isset(Yii::app()->user->role) && (in_array('/users/createusers', Yii::app()->user->menuauthlist)) && (!defined('LOGIN_USER_TABLE') || LOGIN_USER_TABLE == '')) {
            ?>
            <div class="add-btn pull-right">
                <!--    <button data-toggle="modal" data-target="#addUser" class="createUser">Add User</button>-->
                <button class="createuser">Add User</a>
            </div>
        <?php } ?>
        <h2>Users</h2>
        <?php
            foreach(Yii::app()->user->getFlashes() as $key => $message) {
                echo '<div class=" alert alert-' . $key . '">' . $message . "</div>\n";
            } 
        ?> 

    </div>
    <div id="errMsg"></div>
    <div id="userform" style="display:none;"></div>

    <?php $this->renderPartial('_newsearch', array('model' => $model)) ?>




    <div class="">
        <?php
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_newview', 'template' => '<div class="container1"><table cellpadding="10" class="table" id="usertable">{items}</table></div>',
        ));
        ?>


        <?php
        Yii::app()->clientScript->registerScript('myjavascript', '
     $(document).ready(function(){
     
       $("#usertable").dataTable( {
            "scrollY": "300px",
            "scrollCollapse": true,
          
             "columnDefs"        : [       
                { 
                    "searchable"    : false, 
                    "targets"       : [0] 
                },
                 { "bSortable": false, "aTargets": [-1]  }
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                if (aData[3] == "Default Role" && (defined("LOGIN_USER_TABLE") && LOGIN_USER_TABLE != "")) {
                  $("td", nRow).css("background-color", "rgb(253 253 184)");
                }
              }     
            
	} );
        var table = $("#usertable").DataTable();
        $("#Users_user_type").on( "change", function () {
        var selectedText=" ";
        if($(this).val()!=""){
        var selectedText = $(this).find("option:selected").text();
        }
            table
                .columns( 3 )
                .search( selectedText )
                .draw();
        } );
        $("#Users_username").on( "keyup", function () {
        var firstname = $(this).val();
            table
                .columns( 1 )
                .search( firstname )
                .draw();
        } );
        
        $("#Users_status").on( "change", function () {
        var status = $(this).val();
            table
                .columns( 8 )
                .search( status )
                .draw();
        } );
        $("#Users_reporting_person").on( "change", function () {
        var selectedText=" ";
        if($(this).val()!=""){
        var selectedText = $(this).find("option:selected").text();
        }
            table
                .columns( 6 )
                .search( selectedText )
                .draw();
        } );
        $("#clear").on( "click", function () {
                var selectedText=" ";
                    table
                        .columns([ 3, 1, 8, 6 ] )
                        .search( selectedText )
                        .draw();
                } );
        
	});
        
    ');
        ?>
        <script>
            function closeaction() {
                $('#userform').slideUp();
            }

            function edituser(user_id) {
                $('.loading-overlay').addClass('is-active');
                var id = user_id;
                $.ajax({
                    type: "GET",
                    url: "<?php echo $this->createUrl('users/update&layout=1&id=') ?>" + id,
                    success: function (response) {
                        $("#userform").html(response).slideDown();
                        if ($('#Users_company_id_all').attr('disabled') == 'disabled') {
                            $("#Users_company_id_em_").show().text('Create company first!');
                        }
                        $('.loading-overlay').removeClass('is-active');

                    }
                });

            }
            ;

            $(document).ready(function () {


                jQuery(function ($) {
                    $('#user').on('keydown', function (event) {
                        if (event.keyCode == 13) {
                            $("#usersearch").submit();

                        }


                    });
                });
                $('.createuser').click(function () {
                    $('.loading-overlay').addClass('is-active');
                    $.ajax({
                        type: "GET",
                        url: "<?php echo $this->createUrl('users/create&layout=1') ?>",
                        success: function (response) {
                            $("#userform").html(response).slideDown();
                            if ($('#Users_company_id_all').attr('disabled') == 'disabled') {
                                $("#Users_company_id_em_").show().text('Create company first!');
                            }
                        }
                    });
                });


            });
            $(document).ajaxComplete(function () {
                $('.loading-overlay').removeClass('is-active');
                $('#loading').hide();
            });
            function deletUser(elem,id){
        
        if (confirm("Do you want to remove this item?")) {
            $('.loading-overlay').addClass('is-active');
            $.ajax({
                url: '<?php echo  Yii::app()->createAbsoluteUrl('users/Delete'); ?>',
                type: 'POST',
                dataType:'json',
                data: {
                    roleId: id
                },
                success: function(data) {    
                    console.log(data)                ;
                    $("#errMsg").show()
                        .html('<div class="alert alert-'+data.response+'">'+data.msg+'</div>')
                        .fadeOut(10000);
                    setTimeout(function () {                        
                        location.reload(true);
                    }, 3000);
                        
                }
            });
        } else {
            return false;
        }
        
    }
        </script>
<script>
     setTimeout(function() {
        $(".alert").hide('blind', {}, 500)
    }, 5000);
</script>


    </div>
</div>

<style>
    .page-body h3 {
        margin: 4px 0px;
        color: inherit;
        text-align: left;
    }

    .panel {
        border: 1px solid #ddd;
    }

    .panel-heading {
        background-color: #eee;
        height: 40px;
    }

    .checkboxList label {
        margin-top: 2px;
        margin-left: 5px;
    }
</style>