<?php
/**
 * This is the model class for table "{{project_type}}".
 *
 * The followings are the available columns in table '{{project_type}}':
 * @property integer $ptid
 * @property string $project_type
 *
 * The followings are the available model relations:
 * @property Clients[] $clients
 */
class ProjectType extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProjectType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{project_type}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_type', 'required','message'=>'Please enter {attribute}'),
			array('project_type', 'length', 'max'=>50),
                        array('project_type', 'unique', 'message' => '{attribute} already exists.'),
			//array('project_type', 'checkexist'),
                        // The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ptid, project_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'clients' => array(self::HAS_MANY, 'Clients', 'project_type'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ptid' => 'Ptid',
			'project_type' => 'Client Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ptid',$this->ptid);
		$criteria->compare('project_type',$this->project_type,true);
                $user = Users::model()->findByPk(Yii::app()->user->id);
                $arrVal = explode(',', $user->company_id);
                $newQuery = "";
                foreach($arrVal as $arr) {
                    if ($newQuery) $newQuery .= ' OR';
                    $newQuery .= " FIND_IN_SET('".$arr."', company_id)";
                }
                $criteria->addCondition($newQuery);
		return new CActiveDataProvider($this, array(
                    //'pagination' => array('pageSize' => 25,),
			'criteria'=>$criteria,
                        'pagination'=>false,
			'sort' => array('defaultOrder' => 'project_type ASC',
                        
            ),
		));
	}
        
        public function checkexist($attribute, $params) {

            $id = ProjectType::model()->findByAttributes(array('ptid' => $this->ptid));
            if(!empty($id)){
                $name = ProjectType::model()->findByAttributes(array('project_type' => $this->project_type, 'company_id' => Yii::app()->user->company_id),array('condition'=>'ptid != '.$this->ptid.''));
                if(!empty($name)) {
                    $this->addError("project_type", ("Client type already exists"));
               }
            } else{
                $name = ProjectType::model()->findByAttributes(array('project_type' => $this->project_type, 'company_id' => Yii::app()->user->company_id));
                if(!empty($name)) {
                    $this->addError("project_type", ("Client type already exists"));
                }
            }
        }
}
