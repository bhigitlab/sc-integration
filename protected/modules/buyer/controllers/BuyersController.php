<?php

class BuyersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$accessArr = array();
		$accessauthArr = array();
		$controller = 'buyer/' . Yii::app()->controller->id;

		if (isset(Yii::app()->user->menuall)) {
			if (array_key_exists($controller, Yii::app()->user->menuall)) {
				$accessArr = Yii::app()->user->menuall[$controller];
			}
		}

		if (isset(Yii::app()->user->menuauth)) {
			if (array_key_exists($controller, Yii::app()->user->menuauth)) {
				$accessauthArr = Yii::app()->user->menuauth[$controller];
			}
		}
		$access_privlg = count($accessauthArr);
		return array(
			array(
				'allow', // allow all users to perform 'index' and 'view' actions
				'actions' => $accessArr,
				'users' => array('@'),
				'expression' => "$access_privlg > 0",
			),

			array(
				'allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => $accessauthArr,
				'users' => array('@'),
				'expression' => "$access_privlg > 0",
			),
			array(
				'allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('getprojects', 'addinvoiceitem', 'updateinvoiceitem', 'editinvoice', 'removeinvoiceitem', 'gettransactiondetails', 'buyerreport', 'getdatabydate'),
				'users' => array('@'),
			),
			array(
				'deny',  // deny all users
				'users' => array('*'),
			),
		);
	}


	public function actionCreate()
	{
		$flat_numbers = array();
		if (isset($_GET['layout']))
			$this->layout = false;
		$model = new Buyers;
		$this->performAjaxValidation($model);
		$projects =   CHtml::listData(Projects::model()->findAll(array('select' => 'pid,name')), 'pid', 'name');
		if (isset($_POST['Buyers'])) {
			$model->attributes = $_POST['Buyers'];
			$model->created_by = Yii::app()->user->id;
			$model->updated_by = Yii::app()->user->id;
			$model->created_date = date('Y-m-d');
			$model->updated_date =  date('Y-m-d H:i:s');
			if (!empty($_POST['Buyers']['company'])) {
				$company = implode(",", $_POST['Buyers']['company']);
				$model->company = $company;
			}
			if (!empty($_POST['Buyers']['flat_numbers'])) {
				$flat_numbers = implode(",", $_POST['Buyers']['flat_numbers']);
				$model->flat_numbers = $flat_numbers;
			}
			if ($model->save()) {
				if (!empty($model->flat_numbers)) {
					ProjectFlatNumbers::model()->updateAll(
						array(
							'status' => 0, 'updated_date' => date('Y-m-d H:i:s')
						),
						'id IN (' . $model->flat_numbers . ')'
					);
				}
				$this->redirect(array('admin'));
			}
		}
		$this->render('create', array(
			'model' => $model, 'projects' => $projects, 'flat_numbers' => $flat_numbers
		));
	}


	public function actionUpdate($id)
	{
		$flat_numbers = array();
		if (isset($_GET['layout']))
			$this->layout = false;
		$model = $this->loadModel($id);
		$flat_list_before_update = explode(',', $model->flat_numbers);
		$company_ids = explode(',', $model['company']);
		$project_list = $this->getProjectDatas($company_ids);
		$projects =   CHtml::listData($project_list, 'pid', 'name');
		if (!empty($model->project)) {
			$flat_numbers = ProjectFlatNumbers::model()->findAll(['condition' => 'project_id = ' . $model->project]);
			$flat_numbers =   CHtml::listData($flat_numbers, 'id', 'flat_number');
		}

		$this->performAjaxValidation($model);
		$userids = array();
		if (isset($_POST['Buyers'])) {
			$model->attributes = $_POST['Buyers'];
			$model->updated_by = Yii::app()->user->id;
			$model->updated_date =  date('Y-m-d H:i:s');
			if (!empty($_POST['Buyers']['company'])) {
				$company = implode(",", $_POST['Buyers']['company']);
				$model->company = $company;
			} else {
				$model->company = NULL;
			}
			if (!empty($_POST['Buyers']['flat_numbers'])) {
				$flat_numbers = implode(",", $_POST['Buyers']['flat_numbers']);
				$model->flat_numbers = $flat_numbers;
			} else {
				$model->flat_numbers = '';
			}
			$this->updateFlatDetails($model, $flat_list_before_update);
			if ($model->save()) {
				$this->updateFlatDetails($model, $flat_list_before_update);
				$this->redirect(array('admin'));
			}
		}
		if ($id) {
			$userids = Users::model()->find(array(
				'select' => 'username',
				'condition' => 'client_id = :client_id',
				'params' => array(':client_id' => $id),
			));
		}

		$this->render('update', array(
			'model' => $model, 'users' => $userids, 'projects' => $projects, 'flat_numbers' => $flat_numbers
		));
	}

	public function updateFlatDetails($model, $flat_list_before_update)
	{


		if (!empty($flat_list_before_update)) {

			$current_flat_data = explode(',', $model->flat_numbers);
			foreach ($flat_list_before_update as $flat) {
				if (!in_array($flat, $current_flat_data) && !empty($flat)) {
					ProjectFlatNumbers::model()->updateAll(
						array(
							'status' => 1, 'updated_date' => date('Y-m-d H:i:s')
						),
						'id = ' . $flat
					);
				}
			}
		}
		if (!empty($model->flat_numbers)) {
			ProjectFlatNumbers::model()->updateAll(
				array(
					'status' => 0, 'updated_date' => date('Y-m-d H:i:s')
				),
				'id IN (' . $model->flat_numbers . ')'
			);
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new Buyers('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Buyers']))
			$model->attributes = $_GET['Buyers'];

		$this->render('admin', array(
			'model' => $model,
			'dataProvider' => $model->search(),
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Buyers the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = Buyers::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Buyers $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'buyers-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actiongetProjects()
	{
		if (isset($_POST['company_ids'])) {
			$projectOptions = "<option value=''>-Select Project-</option>";
			$companies = array();
			$company_ids = $_POST['company_ids'];
			if (!empty($company_ids)) {

				$projects = $this->getProjectDatas($company_ids);
				foreach ($projects as $project) {
					$projectOptions  .= "<option value='" . $project["pid"] . "'>" . $project["name"] . "</option>";
				}
			}


			echo json_encode(array('project' => $projectOptions));
		}
	}
	public function getProjectDatas($company_ids)
	{
		$project_ids = array();
		foreach ($company_ids as $company_id) {
			$project_data = Projects::model()->findAll(array('condition' => 'FIND_IN_SET(' . $company_id . ', company_id)', 'select' => 'pid'));
			foreach ($project_data as $project) {
				if (!in_array($project['pid'], $project_ids)) {
					array_push($project_ids, $project['pid']);
				}
			}
		}
		$project_ids = implode(',', $project_ids);
		$projects = Projects::model()->findAll(['condition' => 'pid IN (' . $project_ids . ')']);
		return $projects;
	}
	public function actiongetFlatNumbers()
	{
		if (isset($_POST['project_id'])) {
			$flatOptions = "<option value=''>-Select Flat number-</option>";
			$project_id = $_POST['project_id'];
			$flat_numbers_list = ProjectFlatNumbers::model()->findAll(['condition' => 'project_id = ' . $project_id . ' AND status = 1']);
			foreach ($flat_numbers_list as $flat_number) {
				$flatOptions  .= "<option value='" . $flat_number["id"] . "'>" . $flat_number["flat_number"] . "</option>";
			}
			echo json_encode(array('flats' => $flatOptions));
		}
	}

	public function actiontransactions()
	{
		$model = new BuyerTransactions;
		$id = Yii::app()->user->id;
		$tblpx = Yii::app()->db->tablePrefix;
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$company_list = $this->getCompanyList($user);
		$buyer_list = array();
		$currDate = date('Y-m-d', strtotime(date("Y-m-d")));

		$arrVal = explode(',', $user->company_id);
		$company_check = "";
		foreach ($arrVal as $arr) {
			if ($company_check) $company_check .= ' OR';
			$company_check .= " FIND_IN_SET('" . $arr . "', company_id)";
		}

		$transactions = BuyerTransactions::model()->findAll(['condition' => '(' . $company_check . ') AND (date = "' . $currDate . '")']);

		$lastdate = BuyerTransactions::model()->findAll(array(
			'select' => 'date',
			"order" => "id DESC",
			"limit" => 1,
		));
		if (!empty($lastdate)) {
			$date_data = $lastdate[0];
		} else {
			$date_data = '';
		}

		$this->render('transactions', array('model' => $model, 'newmodel' => $transactions, 'id' => $id, 'lastdate' => $date_data, 'company_list' => $company_list, 'buyer_list' => $buyer_list));
	}

	public function getCompanyList($user)
	{
		$company_list = array();
		if (Yii::app()->user->role == 1) {
			$company_datas = Company::model()->findAll(['order' => 'name ASC',]);
		} else {
			$company_datas = Company::model()->findAll(['condition' => 'id IN (' . $user->company_id . ')',  'order' => 'name ASC',]);
		}
		if (!empty($company_datas)) {
			$company_list =   CHtml::listData($company_datas, 'id', 'name');
		}
		return $company_list;
	}

	public function actionaddTransaction()
	{
		$model = new BuyerTransactions;
		$reconmodel = new Reconciliation;
		$tblpx = Yii::app()->db->tablePrefix;
		if (isset($_POST['BuyerTransactions'])) {

			$project_expense_status = 1;
			$profit_percent_array = array();
			$allocated_budget = array();
			$model->attributes = $_POST['BuyerTransactions'];
			$model->transaction_no = $_POST['BuyerTransactions']['transaction_no'];
			$model->date = date('Y-m-d', strtotime($_POST['BuyerTransactions']['date']));
			$model->created_by = Yii::app()->user->id;
			$model->updated_by = Yii::app()->user->id;
			$model->created_date = date('Y-m-d');
			$model->updated_date = date('Y-m-d H:i:s');
			if ($model->transaction_type == 88) {
				$model->bank_id = $_POST['BuyerTransactions']['bank_id'];
				$model->cheque_no = $_POST['BuyerTransactions']['cheque_no'];
				$model->reconciliation_status = 0;
				$reconmodel->reconciliation_table = $tblpx . "buyer_transactions";
				$reconmodel->reconciliation_paymentdate = date('Y-m-d', strtotime($_POST['BuyerTransactions']['date']));
				$reconmodel->reconciliation_bank = $_POST['BuyerTransactions']['bank_id'];
				$reconmodel->reconciliation_chequeno = $_POST['BuyerTransactions']['cheque_no'];
				$reconmodel->created_date = date("Y-m-d H:i:s");
				$reconmodel->reconciliation_status = 0;
				$reconmodel->company_id = $_POST['BuyerTransactions']['company_id'];
				$reconmodel->reconciliation_payment = 'Buyer Transaction';
				$reconmodel->reconciliation_amount = $model->total_amount;
			}
			$transaction = Yii::app()->db->beginTransaction();
			try {
				$success_status = 1;
				if ($model->save()) {
					$lasttransId = Yii::app()->db->getLastInsertID();
					$reconmodel->reconciliation_parentid = $lasttransId;
					if ($model->transaction_type == 88) {
						if (!$reconmodel->save()) {
							$errors = $reconmodel->getErrors();
							throw new Exception(json_encode($errors));
						}
					}
					$user = Users::model()->findByPk(Yii::app()->user->id);
					$arrVal = explode(',', $user->company_id);
					$company_check = "";
					foreach ($arrVal as $arr) {
						if ($company_check) $company_check .= ' OR';
						$company_check .= " FIND_IN_SET('" . $arr . "', company_id)";
					}
					$transactions = BuyerTransactions::model()->findAll(['condition' => '(' . $company_check . ') AND (date = "' . $model->date . '")']);
					// $client = $this->renderPartial('newlist', array('newmodel' => $transactions));
					// return $client;
				} else {
					$errors = $model->getErrors();
					throw new Exception(json_encode($errors));
				}
				$transaction->commit();
			} catch (CDbException $e) {
				$transaction->rollBack();
				$success_status = 0;
				$error =  $e->getMessage();
			} finally {
				if ($success_status == 1) {
					echo json_encode(array('status' => 1, 'message' => 'Successfully Created'));
				} else {
					echo json_encode(array('status' => 0, 'message' => $error));
				}
			}
		}
	}
	public function actionbuyerInvoice()
	{
		$model = new BuyerInvoice;
		$invoicelist = new BuyerInvoiceList;
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Invoice'])) {
			$model->attributes = $_GET['Invoice'];
			$model->fromdate = $_GET['Invoice']['fromdate'];
			$model->todate = $_GET['Invoice']['todate'];
		}

		$this->render('invoice_list', array(
			'model' => $model,
			'dataProvider' => $model->search(),


		));
	}
	public function actionaddInvoice()
	{

		$model = new BuyerInvoice;
		$invoicelist = new BuyerInvoiceList;

		$tblpx      = Yii::app()->db->tablePrefix;
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$company_list = $this->getCompanyList($user);
		$buyer_list = array();
		$this->render('invoice', array(
			'model' => $model,
			'invoicelist' => $invoicelist,
			'company_list' => $company_list,
			'buyer_list' => $buyer_list

		));
	}
	public function actioncreateInvoice()
	{

		$invoice_id = $_REQUEST['invoice_id'];
		if (isset($_REQUEST['company']) && isset($_REQUEST['buyer_id']) && isset($_REQUEST['project']) && isset($_REQUEST['flat_id']) && isset($_REQUEST['default_date']) && isset($_REQUEST['inv_no'])) {
			if ($invoice_id == 0) {
				$tblpx = Yii::app()->db->tablePrefix;
				$res = BuyerInvoice::model()->findAll(array('condition' => 'invoice_no = "' . $_REQUEST['inv_no'] . '"'));
				if (empty($res)) {
					$model = new BuyerInvoice;
					$model->company_id   = $_REQUEST['company'];
					$model->buyer_id  = $_REQUEST['buyer_id'];
					$model->project_id  = $_REQUEST['project'];
					$model->flat_id  = $_REQUEST['flat_id'];
					$model->date  = date('Y-m-d', strtotime($_REQUEST['default_date']));
					$model->invoice_no  = $_REQUEST['inv_no'];

					$model->created_by   = Yii::app()->user->id;
					$model->created_date = date('Y-m-d');
					$model->updated_by   = Yii::app()->user->id;
					$model->updated_date = date('Y-m-d');
					$model->invoice_status  = 'draft';
					$model->save(false);
					$last_id = $model->buyer_invoice_id;
					echo json_encode(array('response' => 'success', 'msg' => 'Buyer Invoice added successfully', 'invoice_id' => $last_id));
				} else {
					echo json_encode(array('response' => 'error', 'msg' => 'Invoice number already exist'));
				}
			} else {
				$model = BuyerInvoice::model()->findByPk($invoice_id);
				$model->company_id   = $_REQUEST['company'];
				$model->buyer_id  = $_REQUEST['buyer_id'];
				$model->project_id  = $_REQUEST['project'];
				$model->flat_id  = $_REQUEST['flat_id'];
				$model->date  = date('Y-m-d', strtotime($_REQUEST['default_date']));
				$model->invoice_no  = $_REQUEST['inv_no'];
				$model->updated_by   = Yii::app()->user->id;
				$model->updated_date = date('Y-m-d');
				if ($model->save(false)) {
					echo json_encode(array('response' => 'success', 'msg' => 'Invoiceupdated successfully', 'invoice_id' => $invoice_id));
				} else {
					echo json_encode(array('response' => 'error', 'msg' => 'Please enter all details'));
				}
			}
		}
	}

	public function actionaddInvoiceItem()
	{
		$data = $_REQUEST['data'];

		$result[] = '';
		$result['html'] = '';
		$html = '';
		if ($data['invoice_id'] == 0) {
			echo json_encode(array('response' => 'error', 'msg' => 'Please enter Invoice  details'));
		} else {
			// $subtot = $data['subtot'];
			$grand = $data['grand'];
			$grand_tax = $data['grand_tax'];
			if (isset($data['sl_No'])) {
				$sl_No = $data['sl_No'];
			} else {
				$sl_No = '';
			}
			if (isset($data['description'])) {
				$description = $data['description'];
			} else {
				$description = '';
			}
			if (isset($data['amount'])) {
				$amount = $data['amount'];
			} else {
				$amount  = '';
			}

			if (isset($data['sgst'])) {
				$sgst = $data['sgst'];
			} else {
				$sgst  = '';
			}

			if (isset($data['sgst_amount'])) {
				$sgst_amount = $data['sgst_amount'];
			} else {
				$sgst_amount  = 0.00;
			}

			if (isset($data['cgst'])) {
				$cgst = $data['cgst'];
			} else {
				$cgst  = '';
			}

			if (isset($data['cgst_amount'])) {
				$cgst_amount = $data['cgst_amount'];
			} else {
				$cgst_amount  = 0.00;
			}

			if (isset($data['igst'])) {
				$igst = $data['igst'];
			} else {
				$igst  = '';
			}

			if (isset($data['igst_amount'])) {
				$igst_amount = $data['igst_amount'];
			} else {
				$igst_amount  = 0.00;
			}

			if (isset($data['tax_amount'])) {
				$tax_amount = $data['tax_amount'];
			} else {
				$tax_amount  = '';
			}
			if (isset($data['subtotal_amount'])) {
				$subtotal_amount = $data['subtotal_amount'];
			} else {
				$subtotal_amount  = '';
			}
			if (isset($data['disc_amt'])) {
				$disc_amt = $data['disc_amt'];
			} else {
				$disc_amt  = '';
			}

			$item_model = new BuyerInvoiceList();
			$item_model->buyer_inv_id = $data['invoice_id'];
			$item_model->amount = $amount;
			$item_model->description = $description;
			$item_model->transaction_type = $data['transaction_type'];
			$item_model->cgst_p = $cgst;
			$item_model->cgst_val = $cgst_amount;
			$item_model->sgst_p = $sgst;
			$item_model->sgst_val = $sgst_amount;
			$item_model->igst_p = $igst;
			$item_model->igst_val = $igst_amount;
			$item_model->tax_total = $tax_amount;
			$item_model->subtotal = $subtotal_amount;
			$item_model->discount_amount = $disc_amt;
			$item_model->created_by   = Yii::app()->user->id;
			$item_model->created_date = date('Y-m-d');
			$item_model->updated_by   = Yii::app()->user->id;
			$item_model->updated_date = date('Y-m-d');
			if ($item_model->save()) {
				$last_id = $item_model->id;
				$tblpx = Yii::app()->db->tablePrefix;
				$data1 = Yii::app()->db->createCommand("SELECT SUM(amount) as qt_amount FROM {$tblpx}buyer_invoice_list WHERE buyer_inv_id=" . $data['invoice_id'] . "")->queryRow();
				$data2 = Yii::app()->db->createCommand("SELECT SUM(tax_total) as qt_taxamount FROM {$tblpx}buyer_invoice_list WHERE buyer_inv_id=" . $data['invoice_id'] . "")->queryRow();
				$data3 = Yii::app()->db->createCommand("SELECT SUM(discount_amount) as qt_discamount FROM {$tblpx}buyer_invoice_list WHERE buyer_inv_id=" . $data['invoice_id'] . "")->queryRow();
				$model = BuyerInvoice::model()->findByPk($data['invoice_id']);
				$model->total_amount = $data1['qt_amount'];
				$model->tax_amount = $data2['qt_taxamount'];
				if (!empty($model->tax_amount)) {
					$invoice_num = $this->updateInvoiceNumber($model->invoice_no, 1);
				} else {
					$invoice_num = $this->updateInvoiceNumber($model->invoice_no, 2);
				}
				$model->invoice_no = $invoice_num;

				if ($model->save()) {
					$result = '';
					$invoice_details  = BuyerInvoiceList::model()->findAll(['condition' => 'buyer_inv_id = ' . $data['invoice_id']]);
					foreach ($invoice_details as $key => $values) {
						$result .= '<tr>';
						$result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
						$result .= '<td><div class="item_description">' . $values['description'] . '</div> </td>';

						$result .= '<td><div class="item_type">' . $values['transaction_type'] . '</div> </td>';
						$result .= '<td class="text-right">' . $values['sgst_p'] . '</td>';
						$result .= '<td class="text-right">' . number_format($values['sgst_val'], 2, '.', '') . '</td>';
						$result .= '<td class="text-right">' . $values['cgst_p'] . '</td>';
						$result .= '<td class="text-right">' . number_format($values['cgst_val'], 2, '.', '') . '</td>';
						$result .= '<td class="text-right">' . $values['igst_p'] . '</td>';
						$result .= '<td class="text-right">' . number_format($values['igst_val'], 2, '.', '') . '</td>';
						$result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['amount'], 2, '.', '') . '</div></td>';
						$result .= '<td class="text-right">' . number_format($values['tax_total'], 2, '.', '') . '</td>';
						$result .= '<td class="text-right">' . number_format($values['discount_amount'], 2, '.', '') . '</td>';
						$result .= '<td class="text-right">' . number_format($values['subtotal'], 2, '.', '') . '</td>';
						$result .= '<td width="60"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                                                            <div class="popover-content hide">
                                                                <ul class="tooltip-hiden">
                                                                       
                                                                        <li><a href="#" id=' . $values['id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li> <li><a href="" id=' . $values['id'] . ' class="btn btn-xs btn-default edit_item">Edit</a></li></ul></div></td>';

						$result .= '</tr>';
					}
				}
				$grand_tot = $data2['qt_taxamount'] + $data1['qt_amount'] - $data3['qt_discamount'];
				echo json_encode(array('response' => 'success', 'msg' => 'Sales item save successfully', 'item_id' => $last_id, 'html' => $result, 'final_amount' => number_format($data1['qt_amount'], 2), 'final_tax' => number_format($data2['qt_taxamount'], 2), 'grand_total' => number_format($grand_tot, 2), 'invoice_no' => $model->invoice_no, 'disc_total' => number_format($data3['qt_discamount'], 2)));
			} else {
				echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
			}
		}
	}
	public function updateInvoiceNumber($inv_no, $tax_flag)
	{
		if ($tax_flag == 1) {
			$inv_no =	str_replace("E", "T", $inv_no);
			return $inv_no;
		} else {
			$inv_no =	str_replace("T", "E", $inv_no);
			return $inv_no;
		}
	}
	public function actionupdateInvoiceItem()
	{
		$data = $_REQUEST['data'];

		$grand = $data['grand'];
		// $subtot = $data['subtot'];
		$grand_tax = $data['grand_tax'];
		if (isset($data['description'])) {
			$description = $data['description'];
		} else {
			$description = '';
		}
		if (isset($data['amount'])) {
			$amount = $data['amount'];
		} else {
			$amount  = '';
		}

		if (isset($data['sgst'])) {
			$sgst = $data['sgst'];
		} else {
			$sgst  = '';
		}

		if (isset($data['sgst_amount'])) {
			$sgst_amount = $data['sgst_amount'];
		} else {
			$sgst_amount  = '';
		}

		if (isset($data['cgst'])) {
			$cgst = $data['cgst'];
		} else {
			$cgst  = '';
		}

		if (isset($data['cgst_amount'])) {
			$cgst_amount = $data['cgst_amount'];
		} else {
			$cgst_amount  = '';
		}

		if (isset($data['igst'])) {
			$igst = $data['igst'];
		} else {
			$igst  = '';
		}

		if (isset($data['igst_amount'])) {
			$igst_amount = $data['igst_amount'];
		} else {
			$igst_amount  = '';
		}

		if (isset($data['tax_amount'])) {
			$tax_amount = $data['tax_amount'];
		} else {
			$tax_amount  = '';
		}
		if (isset($data['subtotal_amount'])) {
			$subtotal_amount = $data['subtotal_amount'];
		} else {
			$subtotal_amount  = '';
		}
		if (isset($data['disc_amt'])) {
			$disc_amt = $data['disc_amt'];
		} else {
			$disc_amt  = '';
		}
		if (isset($data['transaction_type'])) {
			$transaction_type = $data['transaction_type'];
		} else {
			$transaction_type  = '';
		}
		$result = ['html' => ''];
		$html = '';
		$tblpx = Yii::app()->db->tablePrefix;

		if (!empty($data) && $data['item_id'] != 0) {
			$item_model = BuyerInvoiceList::model()->findByPk($data['item_id']);
			$item_model->amount = $amount;
			$item_model->description = $description;
			$item_model->transaction_type = $transaction_type;
			$item_model->cgst_p = $cgst;
			$item_model->cgst_val = $cgst_amount;
			$item_model->sgst_p = $sgst;
			$item_model->sgst_val = $sgst_amount;
			$item_model->igst_p = $igst;
			$item_model->igst_val = $igst_amount;
			$item_model->tax_total = $tax_amount;
			$item_model->subtotal = $subtotal_amount;
			$item_model->discount_amount = $disc_amt;
			if ($item_model->save()) {

				$last_id = $item_model->id;
				$tblpx = Yii::app()->db->tablePrefix;
				$data1 = Yii::app()->db->createCommand("SELECT SUM(amount) as qt_amount FROM {$tblpx}buyer_invoice_list WHERE buyer_inv_id=" . $data['invoice_id'] . "")->queryRow();
				$data2 = Yii::app()->db->createCommand("SELECT SUM(tax_total) as qt_taxamount FROM {$tblpx}buyer_invoice_list WHERE buyer_inv_id=" . $data['invoice_id'] . "")->queryRow();
				$model = BuyerInvoice::model()->findByPk($data['invoice_id']);
				$model->total_amount = $data1['qt_amount'];
				$model->tax_amount = $data2['qt_taxamount'];

				if (empty($model->tax_amount) || ($model->tax_amount == 0)) {

					$model->invoice_no = $this->updateInvoiceNumber($model->invoice_no, 2);
				} else {

					$model->invoice_no = $this->updateInvoiceNumber($model->invoice_no, 1);
				}

				if ($model->save()) {
					$result = '';
					$invoice_details  = BuyerInvoiceList::model()->findAll(['condition' => 'buyer_inv_id = ' . $data['invoice_id']]);
					foreach ($invoice_details as $key => $values) {
						$result .= '<tr>';
						$result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
						$result .= '<td><div class="item_description">' . $values['description'] . '</div> </td>';

						$result .= '<td><div class="item_type">' . $values['transaction_type'] . '</div> </td>';
						$result .= '<td class="text-right">' . $values['sgst_p'] . '</td>';
						$result .= '<td class="text-right">' . number_format($values['sgst_val'], 2, '.', '') . '</td>';
						$result .= '<td class="text-right">' . $values['cgst_p'] . '</td>';
						$result .= '<td class="text-right">' . number_format($values['cgst_val'], 2, '.', '') . '</td>';
						$result .= '<td class="text-right">' . $values['igst_p'] . '</td>';
						$result .= '<td class="text-right">' . number_format($values['igst_val'], 2, '.', '') . '</td>';
						$result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['amount'], 2, '.', '') . '</div></td>';
						$result .= '<td class="text-right">' . number_format($values['tax_total'], 2, '.', '') . '</td>';
						$result .= '<td class="text-right">' . number_format($values['discount_amount'], 2, '.', '') . '</td>';
						$result .= '<td class="text-right">' . number_format($values['subtotal'], 2, '.', '') . '</td>';
						$result .= '<td width="60"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                                                            <div class="popover-content hide">
                                                                <ul class="tooltip-hiden">
                                                                       
                                                                        <li><a href="#" id=' . $values['id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li> <li><a href="" id=' . $values['id'] . ' class="btn btn-xs btn-default edit_item">Edit</a></li></ul></div></td>';

						$result .= '</tr>';
					}
				}

				echo json_encode(array('response' => 'success', 'msg' => 'Sales item save successfully', 'item_id' => $last_id, 'html' => $result, 'final_amount' => number_format($data1['qt_amount'], 2), 'final_tax' => number_format($data2['qt_taxamount'], 2), 'grand_total' => number_format($data2['qt_taxamount'] + $data1['qt_amount'], 2), 'invoice_no' => $model->invoice_no));
			} else {
				echo json_encode(array('response' => 'error', 'msg' => 'Some problem occured'));
			}
		}
	}

	public function actionviewInvoice($invid)
	{
		$model      = BuyerInvoice::model()->find(array("condition" => "buyer_invoice_id='$invid'"));
		$item_model = BuyerInvoiceList::model()->findAll(array("condition" => "buyer_inv_id = '$invid'"));


		if (!empty($model)) {

			$this->render('invoiceview', array(
				'model' => $model,
				'itemmodel' => $item_model,
				'invoice_id' => $invid
			));
		} else {
			$this->redirect(array('buyer/buyers/buyerinvoice'));
		}
	}
	public function actionupdateInvoice($invid)
	{
		$model 		=  BuyerInvoice::model()->find(array("condition" => "buyer_invoice_id = '$invid'"));
		$invoicelist = new BuyerInvoiceList;
		if (empty($model)) {
			$this->redirect(array('buyer/buyers/buyerinvoice'));
		}
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$company_list = $this->getCompanyList($user);
		$buyer_datas = Buyers::model()->findAll(['condition' => 'FIND_IN_SET(' . $model->company_id . ',company)']);
		$buyer_projects = Buyers::model()->findByPk($model->buyer_id);
		$buyer_list =   CHtml::listData($buyer_datas, 'id', 'name');
		$projects = Projects::model()->findAll(['condition' => 'FIND_IN_SET(' . $model->company_id . ',company_id)']);
		$project_list =   CHtml::listData($projects, 'pid', 'name');
		$flat_datas = ProjectFlatNumbers::model()->findAll(['condition' => 'id IN (' . $buyer_projects['flat_numbers'] . ')']);
		$flat_list =   CHtml::listData($flat_datas, 'id', 'flat_number');
		$item_model = BuyerInvoiceList::model()->findAll(array("condition" => "buyer_inv_id = '$invid'"));
		$this->render('invoice_update', array(
			'model' => $model,
			'item_model' => $item_model,
			'invoice_id' => $invid,
			'company_list' => $company_list,
			'buyer_list' => $buyer_list,
			'project_list' => $project_list,
			'flat_list' => $flat_list,
			'invoicelist' => $invoicelist
		));
	}
	public function actioneditInvoice()
	{
		if (isset($_POST['company']) && isset($_POST['project']) && isset($_POST['date']) && isset($_POST['invoice_id']) && isset($_POST['buyer']) && isset($_POST['flat_no'])) {
			$invid = $_POST['invoice_id'];
			$model 		=  BuyerInvoice::model()->find(array("condition" => "buyer_invoice_id = '$invid'"));
			$model->company_id = $_POST['company'];
			$model->project_id = $_POST['project'];
			$model->date = date('Y-m-d', strtotime($_POST['date']));
			$model->buyer_id = $_POST['buyer'];
			$model->flat_id = $_POST['flat_no'];
			$model->save();
			echo json_encode(array('response' => 'success', 'msg' => 'Updated successfully'));
		}
	}
	public function actionremoveinvoiceItem()
	{
		$data   = $_REQUEST['data'];
		$tblpx = Yii::app()->db->tablePrefix;

		$del = BuyerInvoiceList::model()->deleteAll("id =" . $data['item_id']);

		if ($del) {
			$tblpx = Yii::app()->db->tablePrefix;
			$data1 = Yii::app()->db->createCommand("SELECT SUM(amount) as qt_amount FROM {$tblpx}buyer_invoice_list WHERE buyer_inv_id=" . $data['invoice_id'] . "")->queryRow();

			$data2 = Yii::app()->db->createCommand("SELECT SUM(tax_total) as qt_taxamount FROM {$tblpx}buyer_invoice_list WHERE buyer_inv_id=" . $data['invoice_id'] . "")->queryRow();
			$model = BuyerInvoice::model()->findByPk($data['invoice_id']);

			$model->total_amount = (isset($data1['qt_amount'])) ? $data1['qt_amount'] : 0;

			$model->tax_amount = (isset($data2['qt_taxamount'])) ? $data2['qt_taxamount'] : 0;
			if (empty($model->tax_amount) || $model->$model->tax_amount == 0) {
				$model->invoice_no = $this->updateInvoiceNumber($model->invoice_no, 2);
			} else {
				$model->invoice_no = $this->updateInvoiceNumber($model->invoice_no, 1);
			}
			$model->save();
			$result = '';
			$invoice_details  = BuyerInvoiceList::model()->findAll(['condition' => 'buyer_inv_id =' . $data['invoice_id']]);
			foreach ($invoice_details as $key => $values) {
				$result .= '<tr>';
				$result .= '<td><div id="item_sl_no">' . ($key + 1) . '</div></td>';
				$result .= '<td><div class="item_description">' . $values['description'] . '</div> </td>';
				$result .= '<td><div class="item_description">' . $values['transaction_type'] . '</div> </td>';
				$result .= '<td class="text-right">' . $values['sgst_p'] . '</td>';
				$result .= '<td class="text-right">' . number_format($values['sgst_val'], 2, '.', '') . '</td>';
				$result .= '<td class="text-right">' . $values['cgst_p'] . '</td>';
				$result .= '<td class="text-right">' . number_format($values['cgst_val'], 2, '.', '') . '</td>';
				$result .= '<td class="text-right">' . $values['igst_p'] . '</td>';
				$result .= '<td class="text-right">' . number_format($values['igst_val'], 2, '.', '') . '</td>';
				$result .= '<td class="text-right"><div class="" id="amount"> ' . number_format($values['amount'], 2, '.', '') . '</div></td>';
				$result .= '<td class="text-right">' . number_format($values['tax_total'], 2, '.', '') . '</td>';


				$result .= '<td width="60"><span class="icon icon-options-vertical popover-test" data-toggle="popover"   data-placement="left" type="button" data-html="true" style="cursor: pointer;" ></span>
                                    <div class="popover-content hide">
                                        <ul class="tooltip-hiden">

                                                <li><a href="#" id=' . $values['id'] . ' class="btn btn-xs btn-default removebtn">Delete</a></li> <li><a href="" id=' . $values['id'] . ' class="btn btn-xs btn-default edit_item">Edit</a></li></ul></div></td>';

				$result .= '</tr>';
			}

			echo json_encode(array('response' => 'success', 'msg' => 'Sales deleted successfully', 'item_id' => $data['item_id'], 'html' => $result, 'final_amount' => number_format($data1['qt_amount'], 2), 'final_tax' => number_format($data2['qt_taxamount'], 2), 'grand_total' => number_format($data2['qt_taxamount'] + $data1['qt_amount'], 2), 'invoice_no' => $model->invoice_no));
		} else {
			echo json_encode(array('response' => 'error', 'msg' => 'Some Problem occured'));
		}
	}

	public function actiongetTransactionDetails()
	{
		$transactionId = $_REQUEST["expenseid"];
		$tblpx = Yii::app()->db->tablePrefix;
		$expenseData = BuyerTransactions::model()->findByPk($transactionId);
		$result = $expenseData->attributes;
		if (!empty($result["company_id"])) {
			$projects = Projects::model()->findAll(['condition' => 'FIND_IN_SET(' . $result["company_id"] . ',company_id)']);
			$buyer_datas = Buyers::model()->findAll(['condition' => 'FIND_IN_SET(' . $result["company_id"] . ',company)']);
		} else {
			$projects = Projects::model()->findAll();
			$buyer_datas = Buyers::model()->findAll();
		}
		if (!empty($result["buyer_id"])) {
			$buyer_id = $result["buyer_id"];
			$buyer_projects = Buyers::model()->findByPk($buyer_id);
			$flat_datas = ProjectFlatNumbers::model()->findAll(['condition' => 'id IN (' . $buyer_projects['flat_numbers'] . ')']);
		} else {
			$flat_datas = ProjectFlatNumbers::model()->findAll(['condition' => 'status = 1']);
		}
		if ($result["buyer_id"] && $result["project_id"] && $result["flat_id"]) {
			$invoices 		=  BuyerInvoice::model()->findAll(array("condition" => "buyer_id = " . $result["buyer_id"] . " AND project_id =" . $result["project_id"] . " AND flat_id =" . $result["flat_id"]));
			$invoice_list =   CHtml::listData($invoices, 'buyer_invoice_id', 'invoice_no');
		} else {
			$invoice_list =  array();
		}
		$project_list =   CHtml::listData($projects, 'pid', 'name');
		$buyer_list =   CHtml::listData($buyer_datas, 'id', 'name');
		$flat_list =   CHtml::listData($flat_datas, 'id', 'flat_number');

		echo json_encode(array('result' => $result, 'project_list' => $project_list, 'buyer_list' => $buyer_list, 'flat_list' => $flat_list, 'invoice_list' => $invoice_list));
	}

	public function actionbuyerReport()
	{
		$invoice_query = '';
		$data = array();
		$model = new  BuyerTransactions;
		$buyer_datas = Buyers::model()->findAll(['condition' => 'status = 1']);
		$buyer_list =   CHtml::listData($buyer_datas, 'id', 'name');
		if (isset($_POST['BuyerTransactions'])) {
			$model->attributes = $_POST['BuyerTransactions'];
		}
		$invoice_model = new BuyerInvoice;
		if (!empty($_POST['BuyerTransactions']['company_id'])) {
			$invoice_model->company_id = $_POST['BuyerTransactions']['company_id'];
			$model->company_id = $_POST['BuyerTransactions']['company_id'];
		}
		if (!empty($_POST['BuyerTransactions']['buyer_id'])) {
			$invoice_model->buyer_id = $_POST['BuyerTransactions']['buyer_id'];
			$model->buyer_id = $_POST['BuyerTransactions']['buyer_id'];
		}
		if (!empty($_POST['BuyerTransactions']['project_id'])) {
			$invoice_model->project_id = $_POST['BuyerTransactions']['project_id'];
			$model->project_id = $_POST['BuyerTransactions']['project_id'];
		}

		$dataProvider_invoice = $invoice_model->search();
		$invoices = $dataProvider_invoice->getData();

		$invoices = $this->invoicesArray($invoices);
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$company_list = $this->getCompanyList($user);

		$dataProvider = $model->search();
		$transactions = $dataProvider->getData();
		$transactions =  $this->transactionsArray($transactions);
		$final_array = array_merge($transactions, $invoices);
		// echo '<pre>';
		// print_r($model);
		// exit;
		$this->render(
			'buyer_report',
			array('company_list' => $company_list, 'model' => $model, 'final_array' => $final_array, 'buyer_list' => $buyer_list)
		);
	}
	public function invoicesArray($datas)
	{
		$invoice_array = array();
		foreach ($datas as $data) {
			$invoice_items = BuyerInvoiceList::model()->findAll(['condition' => 'buyer_inv_id =' . $data['buyer_invoice_id']]);
			if (!empty($invoice_items)) {
				foreach ($invoice_items as $item) {
					if ($item['transaction_type'] == 1) {
						$voucher_type = "Sales";
					} elseif ($item['transaction_type'] == 2) {
						$voucher_type = "Journal";
					} else {
						$voucher_type = "";
					}
					$invoice_data = array();
					$invoice_data['date'] = $data['date'];
					$invoice_data['buyer'] = $data->buyer->name;
					$invoice_data['desc'] = $item['description'];
					$invoice_data['voucher_type'] = $voucher_type;
					$invoice_data['type'] = 2;
					if (isset($data->flat->flat_number))
						$invoice_data['flat_no'] = $data->flat->flat_number;
					$invoice_data['name'] = $data->project->name;
					$invoice_data['debit'] = '';
					$invoice_data['credit'] = $item['subtotal'];
					array_push($invoice_array, $invoice_data);
				}
			}
		}
		return $invoice_array;
	}
	public function transactionsArray($datas)
	{

		$transaction_array = array();
		foreach ($datas as $data) {
			if ($data['transaction_for'] == 1) {
				$voucher_type = "Advance";
			} elseif ($data['transaction_for'] == 2) {
				$voucher_type = "Receipt";
			} else {
				$voucher_type = "";
			}
			$transaction_heads = BuyerTransactions::model()->getTransactionHead($data);
			$transaction_data = array();
			$transaction_data['date'] = $data['date'];
			$transaction_data['buyer'] = $data->buyer->name;
			$transaction_data['desc'] = $transaction_heads['to_head'];
			$transaction_data['voucher_type'] = $voucher_type;
			$transaction_data['type'] = 1;
			if (isset($data->flat->flat_number))
				$transaction_data['flat_no'] = $data->flat->flat_number;
			$transaction_data['name'] = $data->project->name;
			$transaction_data['debit'] = $data['total_amount'];
			$transaction_data['credit'] = '';
			array_push($transaction_array, $transaction_data);
		}
		return $transaction_array;
	}
	public function actionGetDataByDate()
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$arrVal = explode(',', $user->company_id);
		$company_check = "";
		foreach ($arrVal as $arr) {
			if ($company_check) $company_check .= ' OR';
			$company_check .= " FIND_IN_SET('" . $arr . "', company_id)";
		}
		$newDate = date('Y-m-d', strtotime($_REQUEST["date"]));
		$transactions = BuyerTransactions::model()->findAll(['condition' => '(' . $company_check . ') AND (date = "' . $newDate . '")']);
		$client = $this->renderPartial('newlist', array('newmodel' => $transactions));
		return $client;
	}

	public function actionupdateTransaction()
	{
		$model = new BuyerTransactions;
		$reconmodel = new Reconciliation;
		$tblpx = Yii::app()->db->tablePrefix;
		$client = array();
		if (isset($_POST['BuyerTransactions'])) {
			$id = $_POST['BuyerTransactions']['id'];
			$model         = BuyerTransactions::model()->findByPk($id);
			$reconmodel = Reconciliation::model()->find(['condition' => 'reconciliation_table = "buyer_transactions" AND reconciliation_parentid = ' . $id]);
			if (empty($reconmodel)) {
				$reconmodel = new Reconciliation;
			}
			$project_expense_status = 1;
			$profit_percent_array = array();
			$profit_margin_status = 1;
			$allocated_budget = array();
			$model->attributes = $_POST['BuyerTransactions'];

			$model->transaction_no = $_POST['BuyerTransactions']['transaction_no'];
			$model->date = date('Y-m-d', strtotime($_POST['BuyerTransactions']['date']));
			$model->updated_by = Yii::app()->user->id;
			$model->updated_date = date('Y-m-d H:i:s');
			if ($model->transaction_type == 88) {
				$model->bank_id = $_POST['BuyerTransactions']['bank_id'];
				$model->cheque_no = $_POST['BuyerTransactions']['cheque_no'];
				$model->reconciliation_status = 0;
				$reconmodel->reconciliation_table = $tblpx . "buyer_transactions";
				$reconmodel->reconciliation_paymentdate = date('Y-m-d', strtotime($_POST['BuyerTransactions']['date']));
				$reconmodel->reconciliation_bank = $_POST['BuyerTransactions']['bank_id'];
				$reconmodel->reconciliation_chequeno = $_POST['BuyerTransactions']['cheque_no'];
				// $reconmodel->created_date = date("Y-m-d H:i:s");
				$reconmodel->reconciliation_status = 0;
				$reconmodel->company_id = $_POST['BuyerTransactions']['company_id'];
				$reconmodel->profit_margin_status = $profit_margin_status;
			}
			$transaction = Yii::app()->db->beginTransaction();
			try {
				$success_status = 1;
				if ($model->save()) {
					$lasttransId = Yii::app()->db->getLastInsertID();
					$reconmodel->reconciliation_parentid = $lasttransId;
					if ($model->transaction_type == 88) {
						if (!$reconmodel->save()) {
							$errors = $reconmodel->getErrors();
							throw new Exception(json_encode($errors));
						}
					}
					$user = Users::model()->findByPk(Yii::app()->user->id);
					$arrVal = explode(',', $user->company_id);
					$company_check = "";
					foreach ($arrVal as $arr) {
						if ($company_check) $company_check .= ' OR';
						$company_check .= " FIND_IN_SET('" . $arr . "', company_id)";
					}

					$transactions = BuyerTransactions::model()->findAll(['condition' => $company_check]);
					// $client = $this->renderPartial('newlist', array('newmodel' => $transactions));
				} else {
					$errors = $model->getErrors();
					throw new Exception(json_encode($errors));
				}
				$transaction->commit();
			} catch (CDbException $e) {
				$transaction->rollBack();
				$success_status = 0;
				$error =  $e->getMessage();
			} finally {
				if ($success_status == 1) {
					echo json_encode(array('status' => 1, 'message' => 'Successfully Created'));
				} else {
					echo json_encode(array('status' => 0, 'message' => $error));
				}
			}
		}
	}
	public function actiondeleteTransaction()
	{
		$data = BuyerTransactions::model()->findByPk($_POST['expId']);
		$tblpx = Yii::app()->db->tablePrefix;
		if ($data) {
			$newmodel = new JpLog('search');
			$newmodel->log_data = json_encode($data->attributes);
			$newmodel->log_action = 2;
			$newmodel->log_table = $tblpx . "buyer_transactions";
			$newmodel->log_datetime = date('Y-m-d H:i:s');
			$newmodel->log_action_by = Yii::app()->user->id;
			$newmodel->company_id = $data->company_id;
			if ($newmodel->save()) {
				$model = BuyerTransactions::model()->deleteAll(array("condition" => "id=" . $_POST['expId'] . ""));
				Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $_POST['expId'] . ""));
				$expDate = date('Y-m-d', strtotime($_POST['expense_date']));
				$user = Users::model()->findByPk(Yii::app()->user->id);
				$arrVal = explode(',', $user->company_id);
				$company_check = "";
				foreach ($arrVal as $arr) {
					if ($company_check) $company_check .= ' OR';
					$company_check .= " FIND_IN_SET('" . $arr . "', company_id)";
				}

				$transactions = BuyerTransactions::model()->findAll(['condition' => '(' . $company_check . ') AND (date = "' . $expDate . '")']);
				$client = $this->renderPartial('newlist', array('newmodel' => $transactions));
				return $client;
			} else {
				echo 1;
			}
		}
	}
}
