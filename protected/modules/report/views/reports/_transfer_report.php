<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<style>
    .search-form div {
        display: inline-block;
    }

    label {
        display: block;
    }

    /* #myTable_info{
    float: right;
} */

    .table a.link {
        color: #015986db;
    }

    .table-holder {
        max-height: 350px;
        overflow: auto;
    }

    #myTable thead tr td {
        background-color: #eee;
        z-index: 1;
    }

    .table a.link_deleted {
        color: #da3727 !important;
    }
</style>
<div class="container" id="project">
    <h2>Transfer Report</h2>
    <?php
    $warehouse_from_arr = array();
    $warehouse_to_arr = array();
    $warehouse = Warehouse::model()->findAll();
    $fromdate = (isset($date_from) && !empty($date_from)) ? date('d-M-Y', strtotime($date_from)) : "";
    $todate = (isset($date_to) && !empty($date_to)) ? date('d-M-Y', strtotime($date_to)) : "";
    $filter_status = isset($filter_status) ? $filter_status : '';
    ?>



    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>

    <div class="search-form">
        <div>
            <?php
            if (yii::app()->user->role == 5) {
                $data_arr = Controller::getCategoryOptions($type = 1);
                $data = CHtml::listData(Controller::getCategoryOptions($type = 1), 'id', 'text', 'group');
            } else {
                $data_arr = Controller::getCategoryOptions();
                $data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group');
            }

            if ($warehouse_from == '' && $warehouse_to == '') {
                $warehouse_from_arr = $data;
                $warehouse_to_arr = $data;
            } else {
                if ($warehouse_from != '') {
                    $warehouse_from_arr = CHtml::listData(Controller::addWarehouseCategoryOptions($data_arr, $warehouse_from), 'id', 'text', 'group');
                } else {
                    $warehouse_from_arr = $data;
                }
                if ($warehouse_to != '') {
                    $warehouse_to_arr = CHtml::listData(Controller::addWarehouseCategoryOptions($data_arr, $warehouse_to), 'id', 'text', 'group');
                } else {
                    $warehouse_to_arr = $data;
                }
            }
            echo CHtml::label('Warehouse From: ', '');
            echo CHtml::dropDownList('warehouse_from', 'warehouse_from', $warehouse_from_arr, array(
                'class' => 'select_box', 'empty' => 'Select Warehouse    ', 'style' => 'width:200px;', 'id' => 'warehouse_from',
                'options' => array($warehouse_from => array('selected' => true)),
                'ajax' => array(
                    'url' => array('reports/getWarehouseTo'),
                    'type' => 'POST',
                    'data' => array('warehouse_from' => 'js:this.value', 'warehouse_to' => 'js:$(\'#warehouse_to\').val()'),
                    'update' => '#warehouse_to',

                )
            ));
            ?>
        </div>
        <div>
            <?php
            echo CHtml::label('Warehouse To: ', '');
            echo CHtml::dropDownList('warehouse_to', 'warehouse_to', $warehouse_to_arr, array(
                'class' => 'select_box', 'empty' => 'Select Warehouse    ', 'style' => 'width:200px;', 'id' => 'warehouse_to',
                'options' => array($warehouse_to => array('selected' => true)),
                'ajax' => array(
                    'url' => array('reports/getWarehouseFrom'),
                    'type' => 'POST',
                    'data' => array('warehouse_to' => 'js:this.value', 'warehouse_from' => 'js:$(\'#warehouse_from\').val()'),
                    'update' => '#warehouse_from',

                )
            ));

            ?>
        </div>
        <div>
            <?php echo CHtml::label('Status :', ''); ?>
            <?php

            $status_arr['1'] = 'Dispatched';
            $status_arr['2'] = 'In Transit';
            $status_arr['3'] = 'Received';
            $status_arr['4'] = 'Deleted';
            echo CHtml::dropDownList('status', 'status', $status_arr, array('class' => 'select_box', 'empty' => 'Select Status    ', 'style' => 'width:200px;', 'id' => 'status', 'options' => array($filter_status => array('selected' => true))));
            ?>
        </div>
        <div>
            <?php echo CHtml::label('Date From:', ''); ?>

            <?php echo CHtml::textField('date_from', $fromdate, array("id" => "date_from", "placeholder" => "From", "readonly" => true)); ?>

        </div>
        <div>
            <?php echo CHtml::label('Date To :', ''); ?>

            <?php echo CHtml::textField('date_to', $todate, array("id" => "date_to", "placeholder" => "To", "readonly" => true)); ?>
        </div>
        <div>
            <?php
            echo CHtml::submitButton('GO', array('id' => 'btSubmit'));
            ?>
            <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('transferreport') . '"')); ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>
    <div class="row">
        <input type="hidden" id="trasfer_report_item_count" value=<?= $result_data_count ?> />
        <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataprovider,


            'itemView' => '_report_data', 'template' => '<div class="clearfix"><div class="pull-left">{sorter}</div></div><div id="parent"><table cellpadding="10" class="table  list-view " id="fixtable">{items}</table><div class="pull-left">{summary}</div></div>{pager}',
            'emptyText' => '<table cellpadding="10" class="table"><thead>
                <tr>
                <th>SI.No</th>
                <th>Dispatch No</th>
                <th>Receipt No</th>
                <th>Type</th>
                <th>From Warehouse</th>
                <th>To Warehouse</th>
                <th>Status</th>
                <th>Dispatched Date</th>
                <th>Received Date</th>
                <th></th>
                 </tr></thead><tr><td colspan="13" class="text-center">No results found</td></tr></table>'
        )); ?>


    </div>
</div>

<script>
    $(document).ready(function() {
        var trasfer_report_item_count = $("#trasfer_report_item_count").val();
        $("#warehouse_from").select2();
        $("#warehouse_to").select2();
        $("#status").select2();
        $('#warehouse_from').change(function() {
            var warehouse_from = $(this).val();
            $("#warehouse_to option[value='" + warehouse_from + "']").remove();
        });
        $("#date_from").datepicker({
            dateFormat: 'dd-mm-yy'
        });
        $("#date_to").datepicker({
            dateFormat: 'dd-mm-yy'
        });
    });

    $("#myTable").tableHeadFixer({
        'left': false,
        'foot': true,
        'head': true
    });
</script>