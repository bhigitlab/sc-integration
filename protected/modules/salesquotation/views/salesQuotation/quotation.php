<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/moment.js            "></script>
            <script>
                        var shim = (function () {
                            document.createElement('datalist');
                        })();
                        </script>
                        <style>
                            .lefttdiv {
                                float: left;
                            }

                            .select2 {
                                width: 100%;
                            }
                        </style>
                        <script>
                            $(function () {
                                $("#datepicker").datepicker({
                                    dateFormat: 'dd-mm-yy'
                                }).datepicker("setDate", new Date());

                            });
                        </script>
                        <style>
                            .dropdown {
                                position: relative;
                                display: inline-block;
                            }

                            .dropdown::before {
                                position: absolute;
                                content: " \2193";
                                top: 0px;
                                right: -8px;
                                height: 20px;
                                width: 20px;
                            }

                            button#caret {
                                border: none;
                                background: none;
                                position: absolute;
                                top: 0px;
                                right: 0px;
                            }

                            .invoicemaindiv th,
                            .invoicemaindiv td {
                                padding: 10px;
                                text-align: left;
                            }

                            .addcolumn {
                                cursor: pointer;
                                background-color: black;
                                border: none;
                                color: white;
                                text-align: center;
                                text-decoration: none;
                                margin: 2px;
                                font-size: 30px;
                                width: 40px;
                            }

                            .invoicemaindiv .pull-right,
                            .invoicemaindiv .pull-left {
                                float: none !important;
                            }


                            .checkek_edit {
                                pointer-events: none;
                            }



                            .add_selection {
                                background: #000;
                            }




                            .toast-container {
                                width: 350px;
                            }

                            .toast-position-top-right {
                                top: 57px;
                                right: 6px;
                            }

                            .toast-item-close {
                                background-image: none;
                                cursor: pointer;
                                width: 12px;
                                height: 12px;
                                text-align: center;
                                border-radius: 2px;
                            }

                            .toast-item-image {
                                font-size: 24px;
                            }

                            .toast-item-close:hover {
                                color: red;
                            }

                            .toast-item {
                                border: transparent;
                                border-radius: 3px;
                                font-size: 10px;
                                opacity: 1;
                                background-color: rgba(34, 45, 50, 0.8);
                            }

                            .toast-item-wrapper p {
                                margin: 0px 5px 0px 42px;
                                font-size: 14px;
                                text-align: justify;
                            }


                            .toast-type-success {
                                background-color: #00A65A;
                                border-color: #00A65A;
                            }

                            .toast-type-error {
                                background-color: #DD4B39;
                                border-color: #DD4B39;
                            }

                            .toast-type-notice {
                                background-color: #00C0EF;
                                border-color: #00C0EF;
                            }

                            .toast-type-warning {
                                background-color: #F39C12;
                                border-color: #F39C12;
                            }

                            .span_class {
                                min-width: 70px;
                                display: inline-block;
                            }

                            .purchase-title {
                                border-bottom: 1px solid #ddd;
                            }

                            .purchase_items h3 {
                                font-size: 18px;
                                color: #333;
                                margin: 0px;
                                padding: 0px;
                                text-align: left;
                                padding-bottom: 10px;
                            }

                            .purchase_items h4 {
                                margin: 0px 15px 10px 15px !important;
                                line-height: 24px;
                                background-color: #fafafa;
                                font-size: 16px;
                            }

                            .purchase_items {
                                padding: 15px;
                                //box-shadow:0 0 13px 1px rgba(0,0,0,0.25);
                                margin-bottom: 15px;
                            }

                            .purchaseitem {
                                display: inline-block;
                                margin-right: 20px;
                            }

                            .purchaseitem .form-control {
                                height: 28px;
                                display: inline-block;
                                padding: 6px 3px;
                            }

                            .purchaseitem label {
                                display: block;
                            }

                            .purchaseitem last-child {
                                margin-right: 0px;
                            }

                            .purchaseitem label {
                                font-size: 12px;
                            }

                            .remark {
                                display: none;
                            }

                            .padding-box {
                                padding: 3px 0px;
                                min-height: 17px;
                                display: inline-block;
                            }

                            th {
                                height: auto;
                            }

                            .quantity,
                            .rate {
                                max-width: 80px;
                            }

                            .p_block {
                                margin-bottom: 10px;
                                padding: 0px 15px;
                            }

                            .gsts input {
                                width: 50px;
                            }

                            .amt_sec {
                                float: right;
                            }

                            .amt_sec:after,
                            .padding-box:after {
                                display: block;
                                content: '';
                                clear: both;
                            }

                            .client_data {
                                margin-top: 6px;
                            }

                            #previous_details {
                                position: fixed;
                                top: 94px;
                                left: 70px;
                                right: 70px;
                                z-index: 2;
                                max-width: 1150px;
                                margin: 0 auto;
                            }

                            .text_align {
                                text-align: center;
                            }

                            *:focus {
                                border: 1px solid #333;
                                box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.25);
                            }

                            .tooltip-hiden {
                                width: auto;
                            }

                            .form-fields {
                                margin-bottom: 10px;
                            }

                            .row_field {
                                margin-bottom: 15px;
                            }

                            @media(max-width: 767px) {
                                .purchaseitem {
                                    display: block;
                                    padding-bottom: 5px;
                                    margin-right: 0px;
                                }

                                .quantity,
                                .rate,
                                .small_class {
                                    width: auto !important;
                                }

                                .p_block {
                                    margin-bottom: 0px;
                                }

                                .padding-box {
                                    float: right;
                                }

                                #tax_amount,
                                #item_amount {
                                    display: inline-block;
                                    float: none;
                                    text-align: right;
                                }
                            }


                            @media(min-width: 767px) {
                                .invoicemaindiv .pull-right {
                                    float: right !important;
                                }

                                .invoicemaindiv .pull-left {
                                    float: left !important;
                                }
                            }
                        </style>

                        <div class="container">

                            <div class="quotationmaindiv">

                                <!-- <h2 class="purchase-title">Quotation</h2> -->

                                <?php if ($model->isNewRecord) { ?>
                                    <h2 class="purchase-title">Create Quotation</h2>
                                <?php } else { ?>
                                    <h2 class="purchase-title">Update Quotation</h2>
                                <?php }
                                ?>

                                <br>
                                <div id="msg_box"></div>
                                <?php if (Yii::app()->user->hasFlash('success')) : ?>
                                    <div class="flash-holder flash-success">
                                        <?php echo Yii::app()->user->getFlash('success'); ?>
                                    </div>
                                <?php endif; ?>
                                <!-- <div id="myflashwrapper" style="display: none;"></div> -->

                                <?php if (Yii::app()->user->hasFlash('error')) : ?>
                                    <div class="alert alert-danger  alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <?php echo Yii::app()->user->getFlash('error'); ?>
                                    </div>
                                <?php endif; ?>

                <!-- <form id="pdfvals1" method="post" action="<?php echo $this->createAbsoluteUrl('purchase/addpurchase'); ?>"  > -->
                                <input type="hidden" name="remove" id="remove" value="">
                                <input type="hidden" name="quotation_id" id="quotation_id" value="0">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="mt-0">Add Entry</h4>
                                    </div>

                                    <div class="panel-body" id="main_div">
                                        <?php
                                        $this->renderPartial('_quotation_form', array(
                                            'model' => $model, 'user_companies' => $user_companies, 'main_item_model' => $main_item_model, 'sub_item_model' => $sub_item_model, 'sub_item_desc_model' => $sub_item_desc_model, 'project' => $project,
                                            'client' => $client,
                                        ));
                                        ?>
                                    </div>
                                </div>


                                <div class="clearfix"></div>
                                <br><br>
                                <div id="msg"></div>

                                <div id="previous_details"></div>

                                <!-- <div class="purchase_items shdow_box">
                             <h4>Quotation Entries</h4>
                            <div class="p_block clearfix">
                                <div class="purchaseitem ">
                                    <label>Description:</label>
                                    <input type="text" class="inputs target txtBox description form-control"  id="description" name="description[]" placeholder=""/></td>
                                </div>
                                <div class="purchaseitem quantity_div">
                                        <label>Quantity:</label>
                                        <input type="text" class="inputs target txtBox quantity allownumericdecimal form-control"  id="quantity" name="quantity[]" placeholder=""/></td>
                                </div>
                                <div class="purchaseitem">
                                        <label>Units:</label>
                                        <input type="text" class="inputs target txtBox unit form-control"  id="unit" name="unit[]" placeholder=""/></td>
                                </div>
                                <div class="purchaseitem">
                                        <label>Rate:</label>
                                        <input type="text" class="inputs target txtBox rate allownumericdecimal form-control" id="rate" name="rate[]" placeholder=""/>
                                </div>
                                 <div class="purchaseitem amt_sec">
                                            <label>&nbsp;</label>
                                            <b>Amount: </b><div id="item_amount" class="padding-box">0.00</div>
                                </div>
                            </div>
                            <div class="p_block clearfix">
                                <div class="purchaseitem gsts">
                                        <label>SGST(%):</label>
                                        <input type="text" class="inputs target small_class txtBox sgst allownumericdecimal form-control" id="sgst" name="sgst[]" placeholder=""/>
                                        <div id="sgst_amount" class="padding-box">0.00</div>
                                </div>
                                <div class="purchaseitem gsts">
                                        <label>CGST(%):</label>item_saves="padding-box">0.00</div>
                                </div>
                                <div class="purchaseitem gsts">
                                        <label>IGST(%):</label>
                                        <input type="text" class="inputs target txtBox igst  small_class allownumericdecimal form-control" id="igst" name="igst[]" placeholder=""/>
                                        <div id="igst_amount" class="padding-box">0.00</div>
                                </div>
                                <div class="purchaseitem amt_sec">
                                            <label>&nbsp;</label>
                                            <b>Tax Amount:</b><div id="tax_amount" class="padding-box">0.00</div>
                                </div>
                            </div> 
                            <div class="p_block clearfix">
                                <div class="purchaseitem">
                                        <input type="button" class="item_save btn btn-info" id="0" value="Save">
                                </div>
                            </div> 
                                </div> -->
                            </div>
                            <br><br>


                            <!-- <div id="table-scroll" class="table-scroll">
                                    <div id="faux-table" class="faux-table" aria="hidden"></div>
                                <div id="table-wrap" class="table-wrap">
                                                    <div class="table-responsive">
                                                            <table border="1" class="table">
                                                                    <thead>
                                                                            <tr>
                                                                                    <th>Sl.No</th>
                                                                                    <th>Description</th>
                                                                                    <th>Quantity</th>
                                                                                    <th>Unit</th>
                                                                                    <th>Rate</th>
                                                                                    <th>SGST (%)</th>
                                                                                    <th>SGST Amount</th>
                                                                                    <th>CGST (%)</th>
                                                                                    <th>CGST Amount</th>
                                                                                    <th>IGST (%)</th>
                                                                                    <th>IGST Amount</th>
                                                                                    <th>Tax Amount</th>
                                                                                    <th>Amount</th>                                                        
                                                                                    <th></th>
                                                                            </tr>
                                                                    </thead>
                                                                    <tbody class="addrow">
                                            
                                                                                            </tr>
                                                                    </tbody>
                                                                    <tfoot>
                                                                            <tr>
                                                                            <th colspan="11" class="text-right">TOTAL: </th>
                                                                            <th class="text-right"><div id="tax_total">0.00</div></th>
                                                                            <th class="text-right"><div id="amount_total">0.00</div></th>
                                                                            <th></th>
                                                                            </tr>
                                                                            
                                                                            <tr>
                                                                            <th colspan="12" class="text-right">GRAND TOTAL: <div id="grand_total">0.00</div></th>
                                                                            <th></th>
                                                                            <th></th>
                                                                            </tr>
                                                                    </tfoot>                        
                                                            </table>
                                                    </div>                                
                                            </div>
                                    </div> -->


                            <div style="padding-right: 0px;" class="col-md-offset-9 col-md-3 col-sm-offset-8 col-sm-4 col-xs-offset-6 col-xs-6">

                                <input type="hidden" value="<?php echo ((isset($subtot) && $subtot != '') ? $subtot : ''); ?>" class="txtBox pastweek" readonly=ture name="subtot" /></td>
                                <input type="hidden" value="<?php echo ((isset($grand) && $grand != '') ? $grand : ''); ?>" class="txtBox pastweek grand" name="grand" readonly=true />



                            </div>

                            <br><br>

                            <center></center>
                            <!-- </form> -->
                        </div>



                        <input type="hidden" name="grand_taxamount" id="grand_taxamount" value="<?php echo ($model->tax_amount == '') ? '0.00' : number_format($model->tax_amount, 2, '.', ''); ?>">
                        <input type="hidden" name="tax_amount" id="tax_amount" value="0.00">
                        <input type="hidden" name="final_amount" id="final_amount" value="<?php echo ($model->amount == '') ? '0.00' : number_format($model->amount, 2, '.', ''); ?>">

                        <input type="hidden" name="item_amount_temp" id="item_amount_temp" value="0">

                        <input type="hidden" name="final_tax" id="final_tax" value="<?php echo ($model->tax_amount == '') ? '0.00' : number_format($model->tax_amount, 2, '.', ''); ?>">

                        <input type="hidden" name="item_tax_temp" id="item_tax_temp" value="0">




                        <style>
                            .error_message {
                                color: red;
                            }

                            a.pdf_excel {
                                background-color: #6a8ec7;
                                display: inline-block;
                                padding: 8px;
                                color: #fff;
                                border: 1px solid #6a8ec8;
                            }
                        </style>



                        <script>
                            jQuery.extend(jQuery.expr[':'], {
                                focusable: function (el, index, selector) {
                                    return $(el).is('button, :input, [tabindex]');
                                }
                            });

                            $(document).on('keypress', 'input,select', function (e) {
                                if (e.which == 13) {
                                    e.preventDefault();
                                    // Get all focusable elements on the page
                                    var $canfocus = $(':focusable');
                                    var index = $canfocus.index(document.activeElement) + 1;
                                    if (index >= $canfocus.length)
                                        index = 0;
                                    $canfocus.eq(index).focus();
                                }
                            });

                            $(document).ready(function () {

                                $(".popover-test").popover({
                                    html: true,
                                    content: function () {
                                        return $(this).next('.popover-content').html();
                                    }
                                });
                                $('[data-toggle=popover]').on('click', function (e) {
                                    $('[data-toggle=popover]').not(this).popover('hide');
                                });
                                $('body').on('hidden.bs.popover', function (e) {
                                    $(e.target).data("bs.popover").inState.click = false;
                                });
                                $('body').on('click', function (e) {
                                    $('[data-toggle=popover]').each(function () {
                                        // hide any open popovers when the anywhere else in the body is clicked
                                        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                                            $(this).popover('hide');
                                        }
                                    });
                                });
                                $(document).ajaxComplete(function () {
                                    $(".popover-test").popover({
                                        html: true,
                                        content: function () {
                                            return $(this).next('.popover-content').html();
                                        }
                                    });

                                });
                                $(".js-example-basic-single").select2();
                                $(".project").select2();
                                $(".client").select2();
                                $(".company_id").select2();
                            });

                            $('.client').change(function () {
                                var val = $(this).val();
                                $.ajax({
                                    type: "POST",
                                    url: '<?php echo Yii::app()->createUrl('quotation/getProject'); ?>',
                                    data: 'client_id=' + val,
                                    dataType: 'json',
                                    success: function (data) {
                                        if (data.status == 'success') {
                                            $('#project').val(data.id);
                                            $('#project_label').val(data.name);
                                        } else {
                                            $('#project').val('');
                                            $('#project_label').val('No Project');
                                        }

                                    },
                                })
                            })




                            $(document).ready(function () {
                                $('select').first().focus();
                            });
                        </script>



                        <script>
                            $(document).ready(function () {
                                $().toastmessage({
                                    sticky: false,
                                    position: 'top-right',
                                    inEffectDuration: 1000,
                                    stayTime: 3000,
                                    closeText: '<i class="icon ion-close-round"></i>',
                                });

                                $(".purchase_items").addClass('checkek_edit');


                            });



                            // remove item

                            $(document).on('click', '.removebtn', function (e) {
                                e.preventDefault();
                                element = $(this);
                                var item_id = $(this).attr('id');
                                var answer = confirm("Are you sure you want to delete?");
                                if (answer) {
                                    var item_id = $(this).attr('id');
                                    var quotation_id = $("#quotation_id").val();
                                    var data = {
                                        'quotation_id': quotation_id,
                                        'item_id': item_id
                                    };
                                    $.ajax({
                                        method: "GET",
                                        async: false,
                                        data: {
                                            data: data
                                        },
                                        dataType: "json",
                                        url: '<?php echo Yii::app()->createUrl('quotation/removequotationitem'); ?>',
                                        success: function (response) {
                                            if (response.response == 'success') {
                                                element.closest('tr').remove();
                                                $('#amount_total').html(response.amount_total);
                                                $('#tax_total').html(response.tax_total);
                                                $('#grand_total').html(response.grand_total);
                                                $().toastmessage('showSuccessToast', "" + response.msg + "");
                                                $('.addrow').html(response.html);
                                            } else {

                                            }
                                        }
                                    });

                                } else {

                                    return false;
                                }

                            });



                            $(document).on("click", ".addcolumn, .removebtn", function () {
                                //alert('hi');

                                $("table.table  input[name='sl_No[]']").each(function (index, element) {
                                    $(element).val(index + 1);
                                    $('.sl_No').html(index + 1);
                                });
                            });






                            $(".inputSwitch span").on("click", function () {

                                var $this = $(this);

                                $this.hide().siblings("input").val($this.text()).show();

                            });

                            $(".inputSwitch input").bind('blur', function () {

                                var $this = $(this);

                                $(this).attr('value', $(this).val());

                                $this.hide().siblings("span").text($this.val()).show();

                            }).hide();


                            $(".allownumericdecimal").keydown(function (event) {
                                if (event.shiftKey == true) {
                                    event.preventDefault();
                                }

                                if ((event.keyCode >= 48 && event.keyCode <= 57) ||
                                        (event.keyCode >= 96 && event.keyCode <= 105) ||
                                        event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
                                        event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 13) {
                                    var splitfield = $(this).val().split(".");
                                    if ((splitfield[1] !== undefined && splitfield[1].length >= 2) && event.keyCode != 8 && event.keyCode != 0 && event.keyCode != 13) {
                                        event.preventDefault();
                                    }
                                } else {
                                    event.preventDefault();
                                }

                                if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
                                    event.preventDefault();

                            });


                            $('.other').click(function () {
                                if (this.checked) {
                                    $('#autoUpdate').fadeIn('slow');
                                    $('#select_div').hide();
                                } else {
                                    $('#autoUpdate').fadeOut('slow');
                                    $('#select_div').show();
                                }

                            })


                            $(document).on("change", ".other", function () {
                                if (this.checked) {
                                    $(this).closest('tr').find('#autoUpdate').fadeIn('slow');
                                    $(this).closest('tr').find('#select_div').hide();
                                } else {
                                    $(this).closest('tr').find('#autoUpdate').fadeOut('slow');
                                    $(this).closest('tr').find('#select_div').show();
                                }
                            });



                            /* Neethu  */

                            /*    $(document).on("change", "#company_id", function () {
                             var element = $(this);
                             var quotation_id = $("#quotation_id").val();
                             var default_date = $(".date").val();
                             var company = $(this).val();
                             var client = $('#client').val();
                             var project = $('#project').val(); 
                             var inv_no = $('#inv_no').val(); 
                             if((moment(default_date, 'DD-MM-YYYY',true).isValid()))
                             {	
                             if(client == '' || default_date == '' || inv_no == '' || company ==''){
                             $.ajax({
                             method: "GET",
                             data: {quotation_id:'test'},
                             dataType:"json",
                             url: '<?php echo Yii::app()->createUrl('quotation/ajaxdate'); ?>',
                             success: function(result) {
                             $('#client').focus();
                             }
                             });
                             } else{
                             $.ajax({
                             method: "GET",
                             async: false,
                             data: {quotation_id:quotation_id,inv_no: inv_no, default_date: default_date, project: project, client: client, company:company},
                             dataType:"json",
                             url: '<?php echo Yii::app()->createUrl('quotation/createnewquotation'); ?>',
                             success: function(result) {
                             if(result.response == 'success')
                             {
                             $(".purchase_items").removeClass('checkek_edit');
                             $('#client').select2('focus');
                             $("#quotation_id").val(result.quotation_id);
                             $().toastmessage('showSuccessToast', ""+result.msg+"");
                             } else {
                             $().toastmessage('showErrorToast', ""+result.msg+"");
                             element.val('');
                             }
                             
                             }
                             });
                             }
                             } else {
                             $(this).focus();
                             $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
                             $(this).focus();
                             }
                             });*/

                            /*$(document).on("change", "#project", function () {
                             var element = $(this);
                             var quotation_id = $("#quotation_id").val();
                             var default_date = $(".date").val();
                             var project = $(this).val();
                             var client = $('#client').val(); 
                             var inv_no = $('#inv_no').val();
                             var company = $('#company_id').val();
                             if((moment(default_date, 'DD-MM-YYYY',true).isValid()))
                             {	
                             if(client == '' || default_date == '' || inv_no == '' || company ==''){
                             $.ajax({
                             method: "GET",
                             data: {quotation_id:'test'},
                             dataType:"json",
                             url: '<?php echo Yii::app()->createUrl('quotation/ajaxdate'); ?>',
                             success: function(result) {
                             $('.date').focus();
                             }
                             });
                             } else{
                             $.ajax({
                             method: "GET",
                             async: false,
                             data: {quotation_id:quotation_id,inv_no: inv_no, default_date: default_date, project: project, client: client, company:company},
                             dataType:"json",
                             url: '<?php echo Yii::app()->createUrl('quotation/createnewquotation'); ?>',
                             success: function(result) {
                             if(result.response == 'success')
                             {
                             $(".purchase_items").removeClass('checkek_edit');
                             $('.js-example-basic-single').select2('focus');
                             $("#quotation_id").val(result.quotation_id);
                             $().toastmessage('showSuccessToast', ""+result.msg+"");
                             } else {
                             $().toastmessage('showErrorToast', ""+result.msg+"");
                             element.val('');
                             }
                             
                             }
                             });
                             }
                             } else {
                             $(this).focus();
                             $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
                             $(this).focus();
                             }
                             });*/

                            /*	$(document).on("change", "#client", function () {
                             var element = $(this);
                             var quotation_id = $("#quotation_id").val();
                             var default_date = $(".date").val();
                             var client = $(this).val();
                             var company = $('#company_id').val();
                             var project = $('#project').val();
                             var inv_no = $('#inv_no').val(); 
                             
                             if((moment(default_date, 'DD-MM-YYYY',true).isValid()))
                             {	
                             
                             if(client == '' || default_date == '' || inv_no == '' || company ==''){
                             $.ajax({
                             method: "GET",
                             data: {quotation_id:'test'},
                             dataType:"json",
                             url: '<?php echo Yii::app()->createUrl('quotation/ajaxdate'); ?>',
                             success: function(result) {
                             $('.date').focus();
                             }
                             });
                             
                             
                             
                             
                             
                             } else{
                             $.ajax({
                             method: "GET",
                             data: {quotation_id:quotation_id,inv_no: inv_no, default_date: default_date, project: project, client: client, company: company},
                             dataType:"json",
                             url: '<?php echo Yii::app()->createUrl('quotation/createnewquotation'); ?>',
                             success: function(result) {
                             if(result.response == 'success')
                             {
                             $(".purchase_items").removeClass('checkek_edit');
                             
                             $('.js-example-basic-single').select2('focus');
                             $("#quotation_id").val(result.quotation_id);
                             $().toastmessage('showSuccessToast', ""+result.msg+"");
                             
                             } else {
                             $().toastmessage('showErrorToast', ""+result.msg+"");
                             
                             }
                             
                             }
                             });
                             
                             }
                             
                             } else {
                             $(this).focus();
                             $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
                             $(this).focus();
                             }
                             
                             });*/



                            /*	$(document).on("change", ".date", function () {
                             var element = $(this);
                             var quotation_id = $("#quotation_id").val();
                             var default_date = $(this).val();
                             var project = $('#project').val();
                             var client = $('#client').val(); 
                             var inv_no = $('#inv_no').val(); 
                             var company = $('#company_id').val();
                             if((moment(default_date, 'DD-MM-YYYY',true).isValid()))
                             {
                             if(client == '' || default_date == '' || inv_no == '' || company ==''){
                             
                             } else{
                             $.ajax({
                             method: "GET",
                             async: false,
                             data: {quotation_id:quotation_id,inv_no: inv_no, default_date: default_date, project: project, client: client, company:company},
                             dataType:"json",
                             url: '<?php echo Yii::app()->createUrl('quotation/createnewquotation'); ?>',
                             success: function(result) {
                             if(result.response == 'success')
                             {
                             $(".purchase_items").removeClass('checkek_edit');
                             $('.js-example-basic-single').select2('focus');
                             $("#quotation_id").val(result.quotation_id);
                             $().toastmessage('showSuccessToast', ""+result.msg+"");
                             
                             } else {
                             $().toastmessage('showErrorToast', ""+result.msg+"");
                             
                             }
                             }
                             
                             });
                             }
                             } else {
                             $(this).focus();
                             $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
                             $(this).focus();
                             }
                             
                             $("#inv_no").focus();   
                             
                             });*/


                            /*$(document).on("blur", "#inv_no", function (event) {
                             var element = $(this);
                             var quotation_id = $("#quotation_id").val();
                             var default_date = $(".date").val();
                             var inv_no = $(this).val();	
                             
                             if((moment(default_date, 'DD-MM-YYYY',true).isValid()))
                             {		
                             if(inv_no == '') {
                             event.preventDefault();
                             } else {
                             var project = $('#project').val();
                             var client = $('#client').val(); 
                             var date = $('.date').val();
                             var company = $('#company_id').val();
                             if(client == '' || default_date == '' || company ==''){					
                             } else{
                             $.ajax({
                             method: "GET",
                             data: {quotation_id:quotation_id,inv_no: inv_no, default_date: default_date, project: project, client: client, company:company},
                             dataType:"json",
                             url: '<?php echo Yii::app()->createUrl('quotation/createnewquotation'); ?>',
                             success: function(result) {
                             if(result.response == 'success')
                             {
                             $("#items_holder").show();
                             $(".purchase_items").removeClass('checkek_edit');								 
                             $("#quotation_id").val(result.quotation_id);
                             $().toastmessage('showSuccessToast', ""+result.msg+"");
                             } else {
                             $().toastmessage('showErrorToast', ""+result.msg+"");
                             }
                             
                             $('#description').focus();						
                             }
                             
                             });															
                             }
                             }
                             
                             } else {
                             $(this).focus();
                             $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
                             $(this).focus();
                             }
                             
                             }); */




                            $("#purchaseno").keypress(function (event) {
                                if (event.keyCode == 13) {
                                    //$('#description').select2('focus');
                                    $("#purchaseno").blur();
                                }
                                //$('.js-example-basic-single').select2('focus');
                            });

                            $("#date").keypress(function (event) {
                                if (event.keyCode == 13) {
                                    //$('#description').select2('focus');
                                    if ($(this).val()) {
                                        $("#inv_no").focus();
                                    }
                                }
                                //$('.js-example-basic-single').select2('focus');
                            });

                            $(".date").keyup(function (event) {
                                if (event.keyCode == 13) {
                                    $(".date").click();
                                    //$('#purchaseno').focus();
                                    //input[name="purchaseno"]
                                }
                            });

                            $("#vendor").keyup(function (event) {
                                if (event.keyCode == 13) {
                                    $("#vendor").click();
                                }
                            });

                            $("#project").keyup(function (event) {
                                if (event.keyCode == 13) {
                                    $("#project").click();
                                }
                            });


                            var sl_no = 1;
                            var howMany = 0;
                            $('.item_save').click(function () {
                                $("#previous_details").hide();
                                var element = $(this);
                                var item_id = $(this).attr('id');

                                if (item_id == 0) {


                                    var description = $('#description').val();
                                    var quantity = $('#quantity').val();
                                    var unit = $('#unit').val();
                                    var rate = $('#rate').val();

                                    var cgst = $('#cgst').val();
                                    var cgst_amount = $('#cgst_amount').html();
                                    var sgst = $('#sgst').val();
                                    var sgst_amount = $('#sgst_amount').html();
                                    var igst = $('#igst').val();
                                    var igst_amount = $('#igst_amount').html();
                                    var tax_amount = $('#tax_amount').html();
                                    var company = $('#company_id').val();
                                    var amount = $('#item_amount').html();
                                    var project = $('input[name="project"]').val();
                                    var client = $('input[name="client"]').val();
                                    var date = $('input[name="date"]').val();
                                    var inv_no = $('input[name="inv_no"]').val();
                                    var rowCount = $('.table .addrow tr').length;


                                    if (client == '' || date == '' || inv_no == '' || company == '') {
                                        $().toastmessage('showErrorToast', "Please enter quotation details");
                                        //$('#msg_box').html('<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong></strong> Please enter purchase details. </div>');
                                    } else {
                                        if ((moment(date, 'DD-MM-YYYY', true).isValid())) {

                                            if (description == '' || quantity == '' || rate == '' || amount == 0) {
                                                if (amount == 0) {
                                                    $().toastmessage('showErrorToast', "Please fill valid quantity and rate");
                                                } else {
                                                    $().toastmessage('showErrorToast', "Please fill the details");
                                                }
                                                //$('#msg_box').html('<div class="alert alert-danger" role="alert"><strong>Sorry!</strong> Pleae fill the details. </div>');
                                            } else {
                                                howMany += 1;
                                                if (howMany == 1) {
                                                    var quotation_id = $('input[name="quotation_id"]').val();
                                                    var subtot = $('#grand_total').text();
                                                    var grand = $('#grand_total').text();
                                                    var grand_tax = $('#grand_taxamount').val();
                                                    var data = {
                                                        'sl_no': rowCount,
                                                        'quantity': quantity,
                                                        'description': description,
                                                        'unit': unit,
                                                        'rate': rate,
                                                        'amount': amount,
                                                        'sgst': sgst,
                                                        'sgst_amount': sgst_amount,
                                                        'cgst': cgst,
                                                        'cgst_amount': cgst_amount,
                                                        'igst': igst,
                                                        'igst_amount': igst_amount,
                                                        'tax_amount': tax_amount,
                                                        'quotation_id': quotation_id,
                                                        'grand': grand,
                                                        'subtot': subtot,
                                                        'grand_tax': grand_tax
                                                    };
                                                    $.ajax({
                                                        url: '<?php echo Yii::app()->createAbsoluteUrl('quotation/quotationitem'); ?>',
                                                        type: 'GET',
                                                        dataType: 'json',
                                                        data: {
                                                            data: data
                                                        },
                                                        success: function (response) {
                                                            if (response.response == 'success') {
                                                                $('#discount_total').html(response.discount_total);
                                                                $('#amount_total').html(response.amount_total);
                                                                $('#tax_total').html(response.tax_total);
                                                                $('#grand_total').html(response.grand_total);
                                                                $().toastmessage('showSuccessToast', "" + response.msg + "");
                                                                $('.addrow').html(response.html);
                                                            } else {
                                                                $().toastmessage('showErrorToast', "" + response.msg + "");
                                                                //$("#msg_box").html('<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Sorry!</strong>  &nbsp;'+response.msg+'</div>');	
                                                            }
                                                            howMany = 0;
                                                            //$('#description').val('');
                                                            $('#description').val('').trigger('change');
                                                            $('#remarks').val('');
                                                            $('#quantity').val('');
                                                            $('#unit').val('');
                                                            $('#rate').val('');
                                                            $('#item_amount').html('');
                                                            $('#tax_amount').val('0.00');
                                                            $('#sgst').val('');
                                                            $('#sgst_amount').html('0.00');
                                                            $('#cgst').val('');
                                                            $('#cgst_amount').html('0.00');
                                                            $('#igst').val('');
                                                            $('#igst_amount').html('0.00');
                                                            $('#description').focus();
                                                        }
                                                    });

                                                }
                                            }

                                        } else {

                                            $(this).focus();
                                            $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
                                            $(this).focus();
                                        }




                                    }



                                } else {
                                    // update 


                                    var description = $('#description').val();
                                    var quantity = $('#quantity').val();
                                    var unit = $('#unit').val();
                                    var rate = $('#rate').val();
                                    var amount = $('#item_amount').html();
                                    var project = $('input[name="project"]').val();
                                    var client = $('input[name="client"]').val();
                                    var date = $('input[name="date"]').val();
                                    var inv_no = $('input[name="inv_no"]').val();
                                    var company = $('#company_id').val();
                                    var cgst = $('#cgst').val();
                                    var cgst_amount = $('#cgst_amount').html();
                                    var sgst = $('#sgst').val();
                                    var sgst_amount = $('#sgst_amount').html();
                                    var igst = $('#igst').val();
                                    var igst_amount = $('#igst_amount').html();
                                    var tax_amount = $('#tax_amount').html();

                                    if (client == '' || date == '' || inv_no == '' || company == '') {
                                        $().toastmessage('showErrorToast', "Please enter quotation details");
                                    } else {

                                        if ((moment(date, 'DD-MM-YYYY', true).isValid())) {
                                            if (description == '' || quantity == '' || rate == '' || amount == 0) {
                                                if (amount == 0) {
                                                    $().toastmessage('showErrorToast', "Please fill valid quantity and rate");
                                                } else {
                                                    $().toastmessage('showErrorToast', "Please fill the details");
                                                }
                                                //$('#msg_box').html('<div class="alert alert-danger" role="alert"><strong>Sorry!</strong> Pleae fill the details. </div>');
                                            } else {

                                                howMany += 1;
                                                if (howMany == 1) {
                                                    var quotation_id = $('input[name="quotation_id"]').val();
                                                    var subtot = $('#grand_total').text();
                                                    var grand = $('#grand_total').text();
                                                    var grand_tax = $('#grand_taxamount').val();
                                                    var data = {
                                                        'item_id': item_id,
                                                        'sl_no': sl_no,
                                                        'quantity': quantity,
                                                        'description': description,
                                                        'unit': unit,
                                                        'rate': rate,
                                                        'amount': amount,
                                                        'sgst': sgst,
                                                        'sgst_amount': sgst_amount,
                                                        'cgst': cgst,
                                                        'cgst_amount': cgst_amount,
                                                        'igst': igst,
                                                        'igst_amount': igst_amount,
                                                        'tax_amount': tax_amount,
                                                        'quotation_id': quotation_id,
                                                        'grand': grand,
                                                        'subtot': subtot,
                                                        'grand_tax': grand_tax
                                                    };
                                                    $.ajax({
                                                        url: '<?php echo Yii::app()->createAbsoluteUrl('quotation/updatesquotationitem'); ?>',
                                                        type: 'GET',
                                                        dataType: 'json',
                                                        data: {
                                                            data: data
                                                        },
                                                        success: function (response) {
                                                            //element.closest('tr').hide();
                                                            if (response.response == 'success') {
                                                                $('#amount_total').html(response.amount_total);
                                                                $('#tax_total').html(response.tax_total);
                                                                $('#grand_total').html(response.grand_total);
                                                                $().toastmessage('showSuccessToast', "" + response.msg + "");
                                                                $('.addrow').html(response.html);
                                                            } else {
                                                                $().toastmessage('showErrorToast', "" + response.msg + "");
                                                                //$("#msg_box").html('<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Sorry!</strong>  &nbsp;'+response.msg+'</div>');	
                                                            }
                                                            $('#description').val('').trigger('change');
                                                            $('#remarks').val('');
                                                            $('#quantity').val('');
                                                            $('#unit').val('');
                                                            $('#rate').val('');
                                                            $('#item_amount').html('');
                                                            $('#tax_amount').val('0.00');
                                                            $('#sgst').val('');
                                                            $('#sgst_amount').html('0.00');
                                                            $('#cgst').val('');
                                                            $('#cgst_amount').html('0.00');
                                                            $('#igst').val('');
                                                            $('#igst_amount').html('0.00');
                                                            $('#description').focus();
                                                            $('#item_amount_temp').val(0);
                                                            $('#item_tax_temp').val(0);

                                                            $(".item_save").attr('value', 'Save');
                                                            $(".item_save").attr('id', 0);
                                                            //$(".js-example-basic-single").select2("val", "");
                                                            $('#description').focus();
                                                            howMany = 0;
                                                        }
                                                    });

                                                }

                                                //$('.js-example-basic-single').select2('focus');
                                            }

                                        } else {
                                            $(this).focus();
                                            $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
                                            $(this).focus();
                                        }



                                    }


                                }




                            });


                            // $('.categ_desc').blur(function()
                            // {


                            //   var item_id = $(this).attr('id');

                            //   if(item_id == 0) {


                            //   var category_name = $('#category').val();	
                            //   var categ_desc = $('#categ_desc').val();		  
                            //   var company = $('#company_id').val();
                            //   var project = $('input[name="project"]').val();
                            //   var client = $('input[name="client"]').val(); 
                            //   var date = $('input[name="date"]').val(); 
                            //   var inv_no = $('input[name="inv_no"]').val(); 
                            //   var rowCount = $('.table .addrow tr').length;


                            //   if(client == '' || date == '' || inv_no == '' || company ==''){
                            // 	 $().toastmessage('showErrorToast', "Please enter quotation details");

                            // 	} else{
                            // 		if((moment(date, 'DD-MM-YYYY',true).isValid()))
                            // 		{

                            // 		if(category_name == '' || categ_desc == '')
                            // 		{

                            // 	 		$().toastmessage('showErrorToast', "Please fill the details");


                            // 		} else {
                            // 	howMany += 1;
                            // 	if(howMany ==1)
                            // 	{
                            // 	var quotation_id = $('input[name="quotation_id"]').val();							
                            // 	var data = {'sl_no':rowCount,'category_name':category_name,'categ_desc':categ_desc, 'quotation_id' : quotation_id};
                            // 	$.ajax({
                            // 		url: '<?php echo Yii::app()->createAbsoluteUrl('quotation/quotationcateg'); ?>',
                            // 		type:'GET',
                            // 		dataType:'json',
                            // 		data:{data:data},
                            // 		success: function(response) {
                            // 		if(response.response == 'success')
                            // 		{

                            // 			$().toastmessage('showSuccessToast', ""+response.msg+"");                                        
                            // 		} else {
                            // 			 $().toastmessage('showErrorToast', ""+response.msg+"");

                            // 		}
                            // 			howMany =0;

                            // 			var category_name = $('#category').val('');	
                            //   			var categ_desc = $('#categ_desc').val('');
                            // 			$('#category').focus();
                            // 						}
                            // 		}); 

                            // 			}
                            // 			}

                            // 		} else {

                            // 		  $(this).focus();
                            // 		  $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
                            // 		  $(this).focus();	
                            // 		}




                            // 	}



                            // } else {
                            // 	// update 


                            //   var category_name = $('#category').val('');	
                            //   var categ_desc = $('#categ_desc').val('');
                            //   var project = $('input[name="project"]').val();
                            //   var client = $('input[name="client"]').val(); 
                            //   var date = $('input[name="date"]').val(); 
                            //   var inv_no = $('input[name="inv_no"]').val();
                            //   var company = $('#company_id').val();		  

                            //   if(client == '' || date == '' || inv_no == '' || company == ''){
                            // 	   $().toastmessage('showErrorToast', "Please enter quotation details");
                            //   } else {

                            // 	  if((moment(date, 'DD-MM-YYYY',true).isValid()))
                            // 		{	  
                            //   if(category_name == '' || categ_desc == '')
                            // 		{

                            // 			 $().toastmessage('showErrorToast', "Please fill the details");

                            // 		} else {

                            // 				howMany += 1;
                            // 				if(howMany ==1)
                            // 				{
                            // 		var quotation_id = $('input[name="quotation_id"]').val();							 
                            // 		var data = {'item_id': item_id, 'sl_no':sl_no,'category_name':category_name,'categ_desc':categ_desc, 'quotation_id' : quotation_id};
                            // 	$.ajax({
                            // 			url: '<?php echo Yii::app()->createAbsoluteUrl('quotation/updatequotationcateg'); ?>',
                            // 						type:'GET',
                            // 						dataType:'json',
                            // 						data:{data:data},
                            // 						success: function(response) {
                            // 							if(response.response == 'success')
                            // 							{										
                            // 								$().toastmessage('showSuccessToast', ""+response.msg+"");

                            // 							} else {
                            // 								 $().toastmessage('showErrorToast', ""+response.msg+"");

                            // 							}
                            // 							var category_name = $('#category').val('');	
                            //   							var categ_desc = $('#categ_desc').val('');
                            // 							$('#category').focus();

                            // 							$(".item_save").attr('value', 'Save');
                            // 							$(".item_save").attr('id', 0);									
                            // 							howMany =0;
                            // 						}
                            // 					}); 

                            // 			}


                            // 			}

                            // 		}  else {
                            // 			$(this).focus();
                            // 			  $().toastmessage('showErrorToast', "Please enter valid date DD-MM-YYYY format");
                            // 			  $(this).focus();
                            // 			}



                            // 		}


                            //  }




                            // });


                            $(document).on('click', '.edit_item', function (e) {
                                e.preventDefault();

                                var item_id = $(this).attr('id');
                                var $tds = $(this).closest('tr').find('td');
                                var description = $tds.eq(1).text();
                                var quantity = $tds.eq(2).text();
                                var unit = $tds.eq(3).text();
                                var rate = $tds.eq(4).text();
                                var sgst = $tds.eq(5).text();
                                var sgst_amount = $tds.eq(6).text();
                                var cgst = $tds.eq(7).text();
                                var cgst_amount = $tds.eq(8).text();

                                var igst = $tds.eq(9).text();
                                var igst_amount = $tds.eq(10).text();
                                var tax_amount = $tds.eq(11).text();
                                var amount = $tds.eq(12).text();
                                //var tax_amount = $tds.eq(12).text();
                                var sgsct1 = eval($tds.eq(6).text()).toFixed(2);
                                var sgsct2 = eval($tds.eq(8).text()).toFixed(2);
                                var sgsct3 = eval($tds.eq(10).text()).toFixed(2);
                                var tax_amount = (eval(sgsct1) + eval(sgsct2) + eval(sgsct3));

                                $('#description').val(description.trim())
                                $('#quantity').val(parseFloat(quantity))
                                $('#unit').val(unit.trim())
                                $('#item_amount').html(parseFloat(amount))
                                $('#item_amount_temp').val(parseFloat(amount))
                                $('#rate').val(parseFloat(rate))

                                $('#tax_amount').html(parseFloat(tax_amount.toFixed(2)))
                                $('#item_tax_temp').val(parseFloat(tax_amount))
                                $('#cgst').val(cgst.trim())
                                $('#cgst_amount').text(cgst_amount)
                                $('#sgst').val(sgst.trim())
                                $('#sgst_amount').text(sgst_amount)
                                $('#igst').val(igst.trim())
                                $('#igst_amount').text(igst_amount)
                                $('#tax_amount').text(tax_amount.toFixed(2))

                                $(".item_save").attr('value', 'Update');
                                $(".item_save").attr('id', item_id);
                                $(".item_save").attr('id', item_id);
                                $('#description').focus();


                            })


                            $('.item_save').keypress(function (e) {
                                if (e.keyCode == 13) {
                                    $('.item_save').click();
                                }
                            });


                            $("#quantity, #rate, #sgst, #cgst, #igst").blur(function () {
                                var quantity = parseFloat($("#quantity").val());
                                var rate = parseFloat($("#rate").val());
                                var sgst = parseFloat($("#sgst").val());
                                var cgst = parseFloat($("#cgst").val());
                                var igst = parseFloat($("#igst").val());
                                var amount = quantity * rate;
                                var new_amount = 0;

                                var sgst_amount = (sgst / 100) * amount;
                                var cgst_amount = (cgst / 100) * amount;
                                var igst_amount = (igst / 100) * amount;

                                if (isNaN(sgst_amount))
                                    sgst_amount = 0;
                                if (isNaN(cgst_amount))
                                    cgst_amount = 0;
                                if (isNaN(igst_amount))
                                    igst_amount = 0;
                                if (isNaN(amount))
                                    amount = 0;

                                var tax_amount = sgst_amount + cgst_amount + igst_amount;

                                var total_amount = amount + tax_amount;

                                $("#sgst_amount").html(sgst_amount.toFixed(2));
                                $("#cgst_amount").html(cgst_amount.toFixed(2));
                                $("#igst_amount").html(igst_amount.toFixed(2));
                                $("#item_amount").html(amount.toFixed(2));
                                $("#tax_amount").html(tax_amount.toFixed(2));
                                $("#total_amount").html(total_amount.toFixed(2));

                            });

                            $("#company_id").change(function () {
                                var val = $(this).val();
                                $("#client").html('<option value="">Select Client</option>');
                                $.ajax({
                                    url: '<?php echo Yii::app()->createUrl('quotation/dynamicclient'); ?>',
                                    method: 'POST',
                                    data: {
                                        company_id: val
                                    },
                                    dataType: "json",
                                    success: function (response) {
                                        if (response.status == 'success') {
                                            $("#client").html(response.html);
                                        } else {
                                            $("#client").html(response.html);
                                        }
                                    }

                                })
                            })
                            $('.item_name').change(function () {
                                var val = $(this).val();
                                var $dropdown = $(this);
                                $.ajax({
                                    type: "POST",
                                    url: '<?php echo Yii::app()->createUrl('quotation/getMaterial'); ?>',
                                    data: {
                                        item_id: val
                                    },
                                    dataType: 'json',
                                    success: function (data) {
                                        if (data.status == 'success') {
                                            $dropdown.parents('.form_datas').find('.item_materials').val(data.item_material);
                                        } else {
                                            console.log('error');
                                        }
                                    },
                                })
                            })
                        </script>

                        <style>
                            .table-scroll {
                                position: relative;
                                max-width: 1280px;
                                width: 100%;
                                margin: auto;
                                display: table;
                            }

                            .table-wrap {
                                width: 100%;
                                display: block;
                                overflow: auto;
                                position: relative;
                                z-index: 1;
                                border: 1px solid #ddd;
                            }

                            .table-wrap.fixedON,
                            .table-wrap.fixedON table,
                            .faux-table table {
                                height: 380px;
                                /* match heights*/
                            }

                            .table-scroll table {
                                width: 100%;
                                margin: auto;
                                border-collapse: separate;
                                border-spacing: 0;
                                border: 1px solid #ddd;
                            }

                            .table-scroll th,
                            .table-scroll td {
                                padding: 5px 10px;
                                border: 1px solid #ddd;
                                background: #fff;
                                vertical-align: top;
                            }

                            .faux-table table {
                                position: absolute;
                                top: 0;
                                left: 0;
                                width: 100%;
                                pointer-events: none;
                            }

                            .faux-table table tbody {
                                visibility: hidden;
                            }

                            /* shrink cells in cloned table so that the table height is exactly 300px so that the header and footer appear fixed */
                            .faux-table table tbody th,
                            .faux-table table tbody td {
                                padding-top: 0;
                                padding-bottom: 0;
                                border-top: none;
                                border-bottom: none;
                                line-height: 0.1;
                            }

                            .faux-table table tbody tr+tr th,
                            .faux-table tbody tr+tr td {
                                line-height: 0;
                            }

                            .faux-table thead th,
                            .faux-table tfoot th,
                            .faux-table tfoot td,
                            .table-wrap thead th,
                            .table-wrap tfoot th,
                            .table-wrap tfoot td {
                                background: #eee;
                            }

                            .faux-table {
                                position: absolute;
                                top: 0;
                                right: 0;
                                left: 0;
                                bottom: 0;
                                overflow-y: scroll;
                            }

                            .faux-table thead,
                            .faux-table tfoot,
                            .faux-table thead th,
                            .faux-table tfoot th,
                            .faux-table tfoot td {
                                position: relative;
                                z-index: 2;
                            }

                            /* ie bug */
                            .table-scroll table thead tr,
                            .table-scroll table thead tr th,
                            .table-scroll table tfoot tr,
                            .table-scroll table tfoot tr th,
                            .table-scroll table tfoot tr td {
                                height: 1px;
                            }

                            .add_sub_item {
                                color: dodgerblue;
                                font-weight: bold;
                                cursor: pointer;
                            }
                        </style>
                        <script>
                                    (function () {
                                        var mainTable = document.getElementById("main-table");
                                        if (mainTable !== null) {
                                            var tableHeight = mainTable.offsetHeight;
                                            if (tableHeight > 380) {
                                                var fauxTable = document.getElementById("faux-table");
                                                document.getElementById("table-wrap").className += ' ' + 'fixedON';
                                                var clonedElement = mainTable.cloneNode(true);
                                                clonedElement.id = "";
                                                fauxTable.appendChild(clonedElement);
                                            }
                                        }
                                    })();
                            $('.sub_item_sec').hide();
                            $('.add_sub_item').click(function () {
                                $(this).siblings('.sub_item_sec').slideToggle();
                            });
                        </script>