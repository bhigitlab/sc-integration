<?php
if ($index == 0) {
?>

    <thead class="entry-table">
        <tr width="100%">
            <th style="display:none"></th>
            <th width="2%">Sl No.</th>
            <th width="2%">#ID</th>
            <th width="10%">Payment Name</th>
            <th width="10%">Company</th>
            <th width="9%">Bill No/Transaction head</th>
            <th width="9%">Description</th>
            <th width="9%">Vendors/Subcontractors</th>
            <th width="5%">Payment Date</th>
            <th width="5%">Amount</th>
            <th width="7%">Bank</th>
            <th width="9%">Cheque Number</th>
            <?php if (Yii::app()->user->role <= 2) { ?>
                <th width="10%">Add Date</th>
            <?php } ?>
        </tr>
    </thead>
    <input type="hidden" name="txtCount" id="txtCount" value="<?php echo $itemcount; ?>" />
<?php
}
?>
<tbody>
    <tr>
        <td style="display:none"></td>
        <td><?php echo $index + 1; ?></td>
        <td>#<?php echo $data->reconciliation_parentid; ?></td>
        <td><?php echo $data->reconciliation_payment; ?></td>
        <td>
            <?php
            $company = Company::model()->findByPk($data->company_id);
            echo $company['name'];
            ?>
        </td>
        <td>
            <?php
            if ($data->reconciliation_table == 'jp_dailyvendors') {
            } else if ($data->reconciliation_table == 'jp_dailyexpense') {
                $data->reconciliation_parentid;
                $value = Dailyexpense::model()->findByPk($data->reconciliation_parentid);
                if ($value['exp_type_id'] != '') {
                    $value_data = Companyexpensetype::model()->findByPk($value['exp_type_id']);
                    echo $value_data['name'];
                } else {
                    $value_data = Deposit::model()->findByPk($value['expensehead_id']);
                    echo $value_data['deposit_name'];
                }
            } else if ($data->reconciliation_table == 'jp_expenses') {
                $value = Expenses::model()->findByPk($data->reconciliation_parentid);
                if ($value['bill_id'] != '') {
                    $value_data = Bills::model()->findByPk($value['bill_id']);
                    echo $value_data['bill_number'] . ' / ';
                }
                if ($value['exptype'] != '') {
                    $value_data = ExpenseType::model()->findByPk($value['exptype']);
                    echo $value_data['type_name'];
                }
            }
            ?>
        </td>
        <td>
            <?php
            if ($data->reconciliation_table == 'jp_dailyvendors') {
                $value = Dailyvendors::model()->findByPk($data->reconciliation_parentid);
                echo $value['description'];
            } else if ($data->reconciliation_table == 'jp_dailyexpense') {
                $value = Dailyexpense::model()->findByPk($data->reconciliation_parentid);
                echo $value['description'];
            } else if ($data->reconciliation_table == 'jp_expenses') {
                $value = Expenses::model()->findByPk($data->reconciliation_parentid);
                echo $value['description'];
            }else{
                $value = SubcontractorPayment::model()->findByPk($data->reconciliation_parentid);
                echo $value['description'];
            }
            ?>
        </td>
        <td>
            <?php
            if ($data->reconciliation_table == 'jp_dailyvendors') {
                $value = Dailyvendors::model()->findByPk($data->reconciliation_parentid);
                if ($value['vendor_id'] != '') {
                    $value_data = Vendors::model()->findByPk($value['vendor_id']);
                    echo $value_data['name'];
                }
            } else if ($data->reconciliation_table == 'jp_expenses') {
                $value = Expenses::model()->findByPk($data->reconciliation_parentid);
                if ($value['vendor_id'] != '') {
                    $value_data = Vendors::model()->findByPk($value['vendor_id']);
                    echo $value_data['name'];
                }else if($value['subcontractor_id'] != ''){
                    $value1 = SubcontractorPayment::model()->findByPk($value['subcontractor_id']);
                if(!empty($value1)){
                    $datasub= Subcontractor::Model()->findByPk($value1["subcontractor_id"]);
                    echo $datasub["subcontractor_name"];
                }

                }
                
            }
            ?>
        </td>
        <td class="no-wrap"><?php echo date("d-m-Y", strtotime($data->reconciliation_paymentdate)); ?></td>       
        <td class="text-right">
            <?php
                //$amount = Reconciliation::model()->getChequeAmount($data);
                echo $data->reconciliation_amount;
            ?>
        </td>
        <td>
            <?php
            $bank = Bank::model()->findByPk($data->reconciliation_bank);
            echo $bank->bank_name;
            ?>
        </td>
        <td><?php echo $data->reconciliation_chequeno; ?></td>
        
            <td>
                <input type="text" name="txtDate<?php echo $index; ?>" id="txtDate<?php echo $index; ?>" data-id="<?php echo $index; ?>" <?php echo ($data->reconciliation_status == 1) ? 'value="' . controller::changeDateForamt($data->reconciliation_date) . '"' : '' ?> class="reconciliation_date form-control <?php echo ($data->reconciliation_status == 0) ? 'date-entry' : '' ?>" />
                <?php 
                if($data->reconciliation_status == 1){ ?>
                <div class="mt-2 text-right">
                    <?php
                        echo CHtml::button('Change Status', array(
                        'class' => 'changeStatus btn btn-info pull-right mt-0 mb-10 margin-right-5',
                        'data-id'=> $data->reconciliation_id,
                        'data-table'=>$data->reconciliation_table
                        ));
                    ?>
                    </div>
                <?php
                }
                ?>

            </td>

            <input type="hidden" name="creds<?php echo $index; ?>" id="creds<?php echo $index; ?>" value="<?php echo $data->reconciliation_id . "," . $data->reconciliation_table . "," . $data->reconciliation_payment . "," . $data->reconciliation_parentid . "," . $data->reconciliation_parentlist; ?>" />
        
    </tr>
</tbody>