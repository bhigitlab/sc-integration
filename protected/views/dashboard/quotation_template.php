<div class="container">
<br>
    <div class="clearfix">
        <div class="pull-left">
            <h2 class="h2_heading">PDF TEMPLATE</h2>
        </div>
        <div class="pull-right" style="display:none">
            <ul class="nav nav-tabs"> 
                <li class="active">
                    <a href="#pdf_template" data-toggle="tab">PDF TEMPLATE</a>
                    <br>
                </li>
                <li>
                    <a href="#project_template" data-toggle="tab">PROJECT TEMPLATE</a>
                    <br>
                </li>                          
            </ul>
            
        </div>
    </div>
    

    <div class="tab-content">
        <div class="tab-pane fade in active" id="pdf_template">
            <!--  -->
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form">
                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'template-form',
                            'enableAjaxValidation' => false,
                        ));
                        ?>
                        <?php
                        foreach ($model as $key => $value) {
                            if ($value['status'] == '1') {
                                $activeTemplate = $value['template_name'];
                            }
                            ?>                            
                            <div class="template_row">
                                <div class="template_btn">
                                    <?php echo $form->checkBox($value, 'status', array('class' => 'template_check', 'data-id' => $value->template_id)); ?>

                            <?php echo $value->template_name ?>  
                        </div>                                    
                    </div>                               
                <?php } ?>   
                <?php $this->endWidget(); ?>                        
            </div>                              
            <input type="hidden" value="<?php echo Controller::getActiveTemplate() ?>">
                </div>
                <div class="col-md-6">
                    <?php $this->renderPartial('_templates', array('activeTemplate' => $activeTemplate)) ?>
                </div>   
            </div>                  
        </div>
        <div class="tab-pane fade" id="project_template">
            <!-- <h3>PROJECT TEMPLATE</h2> -->
            <br>
            <div class="form project_template">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'template-form',
                    'enableAjaxValidation' => false,
                ));
                ?>
                <?php
                foreach ($projectmodel as $key => $value) { 
                    if($value->template_name !="TYPE-2") {                  
                ?>                                    
                <div class="template_row">
                    <div class="template_btn">
                        <?php echo $form->checkBox($value, 'status', array('class' => 'project_template_check', 'data-id' => $value->template_id)); ?>
                        <?php echo $value->template_name ;
                            if((ENV=="development")||(ENV=="stage")) {
                                echo " - ".$value->description;
                            }
                        ?> 

                    </div>                                    
                </div>                               
                <?php } } ?>   
                <?php $this->endWidget(); ?>                        
            </div>             
        </div>
    </div>                                       
</div>

<script>

    $(function () {
        $(".template_check").click(function () {
            var id = $(this).attr('data-id');
            $(this).attr('checked', true);
            $(this).parents(".template_row").siblings().find('.template_check').attr('checked', false);
            $.ajax({
                type: "GET",
                url: "<?php echo $this->createUrl('dashboard/changeTemplateStatus') ?>",
                data: {
                    "id": id,
                },
                success: function (response)
                {
                    location.reload();
                }
            });
        })

        $(".project_template_check").click(function () {
            var id = $(this).attr('data-id');
            $(this).attr('checked', true);
            $(this).parents(".template_row").siblings().find('.template_check').attr('checked', false);
            $.ajax({
                type: "GET",
                url: "<?php echo $this->createUrl('dashboard/changeProjectTemplate') ?>",
                data: {
                    "id": id,
                },
                success: function (response)
                {
                    location.reload();
                }
            });
        })
    })

    
    $(".nav-tabs > li > a").click(function(){
        var btn_name = $(this).text();
        $(".h2_heading").html(btn_name);        
    })
</script>

<style>    
    .template_row {
        margin-bottom: 45px;
    }

    .template_btn{
        margin-left: 10px;
        border: 1px solid #5e5e5e2b;
        width: 300px;
        padding: 10px;
        border-radius: 20px;
        font-size:14px;
        font-family: monospace;
    }

    .project_template > #template-form{
        column-count:3;
    }
</style>