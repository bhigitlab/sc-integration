<?php
$warehousedespatch = Warehousedespatch::model()->findByPk($data['warehousedespatch_id']);
if ($data['delete_status'] == 0 && $data['edit_status'] == 0) {
    //  $warehousereceipt_no_link = !empty($data['warehousereceipt_no'])?CHtml::link($data['warehousereceipt_no'], 'index.php?r=warehousereceipt/deletedWarehouseView&receiptid='.$data['warehousereceipt_id'], array('class' => 'link','target'=>'_blank')):'';
    $warehousedespatch_no = !empty($data['warehousedespatch_id']) ? CHtml::link($data['warehousedespatch_no'], 'index.php?r=wh/warehousedespatch/view&despatchid=' . $data['warehousedespatch_id'], array('class' => 'link', 'target' => '_blank')) : '';
    if ($data['warehousereceipt_id'] == '' && $data['warehousedespatch_warehouseid_to'] != $data['warehouseid_to']) {
        $warehousereceipt_no_link = !empty($data['all_warehousereceipt_id']) ? CHtml::link("", 'index.php?r=wh/warehousereceipt/deletedWarehouseView&receiptid=' . $data['all_warehousereceipt_id'], array('class' => 'link_deleted', 'target' => '_blank')) : '';
        $received_date = $data['received_date'];
        $eta = '';
    } else {
        $warehousereceipt_no_link = !empty($data['all_warehousereceipt_id']) ? CHtml::link("(Deleted)", 'index.php?r=wh/warehousereceipt/deletedWarehouseView&receiptid=' . $data['warehouse_transfer_type_deleted_id'], array('class' => 'link_deleted', 'target' => '_blank')) : '';
        $received_date = $data['warehouse_transfer_date'];
        $eta = $data['eta'];
    }
    $warehousereceipt_no = "<span style='color: red;font-size: 12px;'>" . $warehousereceipt_no_link . "  </span>";
    $warehousedespatch_delete = WarehouseTransferDeletedLog::model()->findByAttributes(array('warehousereceipt_despatch_id' => $data['warehousedespatch_id'], 'warehouseid_to' => $data['warehouseid_to'], 'warehouse_transfer_type_delete' => 1, 'warehousereceipt_transfer_type' => 2));
    $warehouse_to = $warehousedespatch_delete->warehouseidTo['warehouse_name'];
    $warehouse_from = $warehousedespatch_delete->warehouseidFrom['warehouse_name'];
    $clerk = $warehousedespatch->clerk['first_name'] . ' ' . $warehousedespatch->clerk['last_name'];
    $disaptch_date = date("d-m-Y", strtotime($data['warehouse_despatch_date']));
    $eta_date = isset($data['warehouse_despatch_eta_date']) ? date("d-m-Y", strtotime($data['warehouse_despatch_eta_date'])) : '-';
    $type = $data['type'];
    $despatch_status = $data['despatch_status'];
} elseif ($data['delete_status'] == 0) {
    $warehousedespatch_no = !empty($data['warehousedespatch_id']) ? CHtml::link($data['warehousedespatch_no'], 'index.php?r=wh/warehousedespatch/view&despatchid=' . $data['warehousedespatch_id'], array('class' => 'link', 'target' => '_blank')) : '';
    $warehouse_to = $warehousedespatch->warehousedespatchWarehouseidTo['warehouse_name'];
    $warehouse_from = $warehousedespatch->warehouse['warehouse_name'];
    $clerk = $warehousedespatch->clerk['first_name'] . ' ' . $warehousedespatch->clerk['last_name'];
    $disaptch_date = date("d-m-Y", strtotime($data['warehousedespatch_date']));
    $eta_date = isset($data['warehouse_eta_date']) ? date("d-m-Y", strtotime($data['warehouse_eta_date'])) : '-';
    $warehousereceipt_no = !empty($data['all_warehousereceipt_id']) ? CHtml::link($data['all_warehousereceipt_no'], 'index.php?r=wh/warehousereceipt/view&receiptid=' . $data['all_warehousereceipt_id'], array('class' => 'link', 'target' => '_blank')) : '';
    $type = $data['type'];
    $despatch_status = $data['despatch_status'];
    $received_date = $data['received_date'];
    $eta = '';
} elseif ($data['delete_status'] == 1 && $data['edit_status'] == 1) {
    //  $warehousereceipt_no_link = !empty($data['warehousereceipt_no'])?CHtml::link($data['warehousereceipt_no'], 'index.php?r=warehousereceipt/deletedWarehouseView&receiptid='.$data['warehousereceipt_id'], array('class' => 'link','target'=>'_blank')):'';
    $warehousedespatch_no = !empty($data['warehousedespatch_id']) ? CHtml::link($data['warehousedespatch_no'], 'index.php?r=wh/warehousedespatch/view&despatchid=' . $data['warehousedespatch_id'], array('class' => 'link', 'target' => '_blank')) : '';
    if ($data['warehousereceipt_id'] == '' && $data['warehousedespatch_warehouseid_to'] != $data['warehouseid_to']) {
        $warehousereceipt_no_link = !empty($data['all_warehousereceipt_id']) ? CHtml::link("", 'index.php?r=wh/warehousereceipt/deletedWarehouseView&receiptid=' . $data['all_warehousereceipt_id'], array('class' => 'link_deleted', 'target' => '_blank')) : '';
        $received_date = $data['received_date'];
        $eta = '';
    } else {
        $warehousereceipt_no_link = !empty($data['all_warehousereceipt_id']) ? CHtml::link("(Deleted)", 'index.php?r=wh/warehousereceipt/deletedWarehouseView&receiptid=' . $data['warehouse_transfer_type_deleted_id'], array('class' => 'link_deleted', 'target' => '_blank')) : '';
        $received_date = $data['warehouse_transfer_date'];
        $eta = $data['eta'];
    }
    $warehousereceipt_no = "<span style='color: red;font-size: 12px;'>" . $warehousereceipt_no_link . "  </span>";
    $warehousedespatch_delete = WarehouseTransferDeletedLog::model()->findByAttributes(array('warehousereceipt_despatch_id' => $data['warehousedespatch_id'], 'warehouseid_to' => $data['warehouseid_to'], 'warehouse_transfer_type_delete' => 1, 'warehousereceipt_transfer_type' => 2));
    $warehouse_to = $warehousedespatch->warehousedespatchWarehouseidTo['warehouse_name'];
    $warehouse_from = $warehousedespatch_delete->warehouseidFrom['warehouse_name'];
    $clerk = $warehousedespatch->clerk['first_name'] . ' ' . $warehousedespatch->clerk['last_name'];
    $disaptch_date = date("d-m-Y", strtotime($data['warehousedespatch_date']));
    $eta_date = isset($data['warehouse_eta_date']) ? date("d-m-Y", strtotime($data['warehouse_eta_date'])) : '-';
    $type = $data['type'];
    $despatch_status = $data['despatch_status'];
} else {
    //  $warehousereceipt_no_link = !empty($data['warehousereceipt_no'])?CHtml::link($data['warehousereceipt_no'], 'index.php?r=warehousereceipt/deletedWarehouseView&receiptid='.$data['warehousereceipt_id'], array('class' => 'link','target'=>'_blank')):'';
    $warehousedespatch_no = !empty($data['warehousedespatch_id']) ? CHtml::link($data['warehousedespatch_no'], 'index.php?r=wh/warehousedespatch/view&despatchid=' . $data['warehousedespatch_id'], array('class' => 'link', 'target' => '_blank')) : '';
    $warehousereceipt_no_link = !empty($data['all_warehousereceipt_id']) ? CHtml::link("(Deleted)", 'index.php?r=wh/warehousereceipt/deletedWarehouseView&receiptid=' . $data['warehouse_transfer_type_deleted_id'], array('class' => 'link_deleted', 'target' => '_blank')) : '';
    $warehousereceipt_no = "<span style='color: red;font-size: 12px;'>" . $warehousereceipt_no_link . "  </span>";
    $warehousedespatch_delete = WarehouseTransferDeletedLog::model()->findByAttributes(array('warehousereceipt_despatch_id' => $data['warehousedespatch_id'], 'warehouseid_to' => $data['warehouseid_to'], 'warehouse_transfer_type_delete' => 1, 'warehousereceipt_transfer_type' => 2));
    $warehouse_to = $warehousedespatch_delete->warehouseidTo['warehouse_name'];
    $warehouse_from = $warehousedespatch_delete->warehouseidFrom['warehouse_name'];
    $clerk = $warehousedespatch->clerk['first_name'] . ' ' . $warehousedespatch->clerk['last_name'];
    $disaptch_date = date("d-m-Y", strtotime($data['warehouse_despatch_date']));
    $eta_date = isset($data['warehouse_despatch_eta_date']) ? date("d-m-Y", strtotime($data['warehouse_despatch_eta_date'])) : '-';
    $type = $data['type'];
    $despatch_status = $data['despatch_status'];
    $received_date = $data['received_date'];
    $eta = $data['eta'];
}


if ($index == 0) {
?>
    <thead class="entry-table">
        <tr>
            <th>SI.No</th>
            <th>Dispatch No</th>
            <th>Receipt No</th>
            <th>Type</th>
            <th>From Warehouse</th>
            <th>To Warehouse</th>
            <th>Status</th>
            <th>Dispatched Date</th>
            <th>Received Date</th>
        </tr>
    </thead>
<?php
}

?>
<tbody>
    <tr>
        <td><?= $index + 1; ?></td>

        <td><?php echo $warehousedespatch_no; ?></td>
        <td><?php echo $warehousereceipt_no; ?></td>
        <td><?= $type ?></td>
        <td><?= $warehouse_from ?></td>
        <td><?= $warehouse_to ?></td>
        <td><?= $despatch_status ?></td>
        <td><?= !empty($disaptch_date) ? date('d-M-Y', strtotime($disaptch_date)) : '' ?></td>
        <td><?= !empty($received_date) ? date('d-M-Y', strtotime($received_date)) . " " . $eta : '' . "" . $eta ?></td>
    </tr>
</tbody>