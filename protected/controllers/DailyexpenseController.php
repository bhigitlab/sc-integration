<?php

class DailyexpenseController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {

        $accessArr = array();
        $accessauthArr = array();
        $controller = Yii::app()->controller->id;
        if (isset(Yii::app()->user->menuall)) {
            if (array_key_exists($controller, Yii::app()->user->menuall)) {
                $accessArr = Yii::app()->user->menuall[$controller];
            }
        }

        if (isset(Yii::app()->user->menuauth)) {
            if (array_key_exists($controller, Yii::app()->user->menuauth)) {
                $accessauthArr = Yii::app()->user->menuauth[$controller];
            }
        }
        $access_privlg = count($accessauthArr);
        return array(
            array(
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => $accessArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => $accessauthArr,
                'users' => array('@'),
                'expression' => "$access_privlg > 0",
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Dailyexpense;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Dailyexpense'])) {
            $model->attributes = $_POST['Dailyexpense'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->dailyexp_id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Dailyexpense'])) {
            $model->attributes = $_POST['Dailyexpense'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->dailyexp_id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Dailyexpense');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Dailyexpense('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Dailyexpense']))
            $model->attributes = $_GET['Dailyexpense'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /* public function actionExpenses() {
      $model = new Dailyexpense;
      $id = Yii::app()->user->id;
      $this->render('handsontable', array('model' => $model, 'id' => $id));
      } */

    public function actionExpenses()
    {
        $model = new Dailyexpense;
        $cashmodel = new Cashtransfer;
        $id = Yii::app()->user->id;
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
        }
        $currDate = date('Y-m-d', strtotime(date("Y-m-d")));
        $expdatasql = "SELECT e.*, e.bill_id as billno, et.name "
            . " as typename,v.name vname,ba.bank_name as bankname "
            . " FROM " . $tblpx . "dailyexpense e"
            . " LEFT JOIN " . $tblpx . "bills b ON e.bill_id = b.bill_id"
            . " LEFT JOIN " . $tblpx . "company_expense_type et "
            . " ON e.expensehead_id = et.company_exp_id"
            . " LEFT JOIN " . $tblpx . "company_expense_type v "
            . " ON e.dailyexpense_receipt_head = v.company_exp_id"
            . " LEFT JOIN " . $tblpx . "bank ba ON e.bank_id = ba.bank_id"
            . " WHERE e.date = '" . $currDate . "' AND (" . $newQuery . ") "
            . " AND e.transaction_parent IS NULL";
        $expenseData = Yii::app()->db->createCommand($expdatasql)->queryAll();
        $lastdatesql = "SELECT date FROM " . $tblpx . "dailyexpense "
            . " WHERE created_by =" . Yii::app()->user->id
            . " ORDER BY date DESC LIMIT 1";
        $lastdate = Yii::app()->db->createCommand($lastdatesql)->queryRow();
        $due_bills_sql = "SELECT * from " . $tblpx . "dailyexpense_bill "
            . " WHERE paid_status=0 ORDER BY created_date ASC";
        $due_bills = Yii::app()->db->createCommand($due_bills_sql)->queryAll();
        $this->render('dailyexpense', array('model' => $model, 'cashmodel' => $cashmodel, 'newmodel' => $expenseData, 'id' => $id, 'lastdate' => $lastdate, 'due_bills' => $due_bills));
    }

    public function actiongetdata()
    {


        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($_REQUEST['cadate']) || isset($_REQUEST['userid']))
            $cadate = date("Y-m-d", strtotime($_REQUEST['cadate']));
        $userid = isset($_REQUEST['userid']) ? $_REQUEST['userid'] : 0;

        $lastdatesql = "SELECT date FROM " . $tblpx . "dailyexpense  "
            . " ORDER BY date DESC LIMIT 1";
        $lastdate = Yii::app()->db->createCommand($lastdatesql)->queryRow();

        $qry = "SELECT d.dailyexp_id,d.amount,d.description,d.exp_type_id,c.name,c.name "
            . " as name,d.exp_type as type "
            . " FROM " . $tblpx . "dailyexpense d "
            . " JOIN " . $tblpx . "company_expense_type c "
            . " ON c.company_exp_id = d.exp_type_id "
            . " WHERE d.date='" . $cadate . "' "
            . " ORDER BY d.dailyexp_id asc";

        $data = Yii::app()->db->createCommand($qry)->queryAll();
        if ($data) {
            $data1 = array();
            $receipt = $paymnt = $reamnt = $payamnt = '';
            foreach ($data as $dt) {
                if ($dt['type'] == 0) {
                    $paymnt = '';
                    $payamnt = '';
                    $sql = "SELECT name FROM " . $tblpx . "company_expense_type"
                        . "  WHERE company_exp_id = '" . $dt['exp_type_id'] . "'"
                        . " AND (type = 0  || type = 2)";
                    $getreceipt = Yii::app()->db->createCommand($sql)->queryRow();
                    $receipt = $getreceipt['name'];
                    $reamnt = $dt['amount'];
                } elseif ($dt['type'] == 1) {
                    $receipt = '';
                    $reamnt = '';
                    $gexpsql = "SELECT name FROM " . $tblpx . "company_expense_type "
                        . " WHERE company_exp_id = '" . $dt['exp_type_id'] . "'"
                        . "  AND (type = 1 || type =2)";
                    $getExpense = Yii::app()->db->createCommand($gexpsql)->queryRow();
                    $paymnt = $getExpense['name'];
                    $payamnt = $dt['amount'];
                }

                $res = array($dt['dailyexp_id'], $paymnt, $payamnt, $receipt, $reamnt, $dt['description']);
                array_push($data1, $res);
            }

            echo json_encode(array('result' => $data1, 'lastdate' => isset($lastdate['date']) ? date("d-M-Y", strtotime($lastdate['date'])) : ''));
        } else {
            $data1 = array();
            $res = array("", "", "", "", "", "");
            array_push($data1, $res);
            echo json_encode(array("result" => $data1, 'lastdate' => isset($lastdate['date']) ? date("d-M-Y", strtotime($lastdate['date'])) : ''));
        }
    }

    public function actionsavedata()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        if (!empty($_POST['data'])) {
            $data = $_POST['data'];
            foreach ($data as $dt) {
                if (isset($dt) && ((($dt[2] > 0) && ($dt[4] == '')) || (($dt[4] > 0) && ($dt[2] == '')))) {

                    $id = isset($dt[0]) ? $dt[0] : '';
                    $description = $dt[5];

                    /* --Mod by Rajisha -- Column expense type */
                    $type_id = NULL;
                    if (!empty($dt[1])) {
                        $exptype = Yii::app()->db->createCommand("select `company_exp_id` from " . $tblpx . "company_expense_type where `name`='" . addslashes($dt[1]) . "'")->queryAll();

                        foreach ($exptype as $exp) {
                            $type_id = $exp['company_exp_id'];
                            $exp_type = 1;
                        }
                        $amount = $dt[2];
                    }
                    if (!empty($dt[3])) {
                        $exptype = Yii::app()->db->createCommand("select `company_exp_id` from " . $tblpx . "company_expense_type where `name`='" . addslashes($dt[3]) . "'")->queryAll();

                        foreach ($exptype as $exp) {
                            $type_id = $exp['company_exp_id'];
                            $exp_type = 0;
                        }
                        $amount = $dt[4];
                    }
                    $res2 = '';
                    if ($amount == '') {
                        $res2 = "error";
                    }
                    /* --Mod by Rajisha -- Column expense type */
                    $sql = Yii::app()->db->createCommand("select * from " . $tblpx . "dailyexpense where `dailyexp_id`='$id'")->queryAll();

                    foreach ($sql as $sq) {
                        $exp_id = $sq['dailyexp_id'];
                    }
                    $exp_found = Yii::app()->db->createCommand("select dailyexp_id from " . $tblpx . "dailyexpense where dailyexp_id='$id'")->queryAll();
                    if (count($exp_found) == 0) {

                        $model = new Dailyexpense();
                        $model->description = $dt[5];
                        $model->amount = $amount;
                        $model->exp_type_id = $type_id;
                        $model->date = date("Y-m-d", strtotime($_POST['date']));
                        $model->created_by = Yii::app()->user->id;
                        $model->created_date = date('Y-m-d');
                        $model->updated_by = Yii::app()->user->id;
                        $model->data_entry = "Offline";
                        $model->exp_type = $exp_type;
                        $model->save(false);
                    } else {
                        $model = $this->loadModel($exp_id);
                        $model->description = $dt[5];
                        $model->amount = $amount;
                        $model->exp_type_id = $type_id;
                        $model->date = date("Y-m-d", strtotime($_POST['date']));
                        $model->created_date = date('Y-m-d');
                        $model->updated_by = Yii::app()->user->id;
                        $model->updated_date = date('Y-m-d');
                        $model->data_entry = "Offline";
                        $model->exp_type = $exp_type;
                        //$model->save(false);
                    }
                } else {
                    echo json_encode(array("result" => "error"));
                }
            }
            if ($res2 != '') {
                echo json_encode(array("result" => "empty"));
            } else {
                // $model->save(false);
                echo json_encode(array("result" => "ok"));
            }
        } else {

            echo json_encode(array("result" => "empty"));
        }
    }

    public function actionExpenseData()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($_REQUEST['cadate']) || isset($_REQUEST['userid'])) {
            $cadate = date("Y-m-d", strtotime($_REQUEST['cadate']));
            $userid = isset($_REQUEST['userid']) ? $_REQUEST['userid'] : 0;
            $sql = "SELECT * FROM " . $tblpx . "dailyexpense d "
                . " JOIN  " . $tblpx . "company_expense_type c "
                . " ON c.company_exp_id = d.exp_type_id  "
                . " WHERE d.date ='" . $cadate . "' "
                . " ORDER BY d.dailyexp_id ASC";
            $data = Yii::app()->db->createCommand($sql)->queryAll();

            if ($data) {
                $data1 = array();
                $slno = 1;
                $receipt = $paymnt = $reamnt = $payamnt = '';
                foreach ($data as $dt) {
                    if ($dt['type'] == 0) {
                        $paymnt = '';
                        $payamnt = '';
                        $recsql = "SELECT `name` FROM " . $tblpx . "company_expense_type "
                            . " WHERE company_exp_id = '" . $dt['exp_type_id'] . "'"
                            . "  AND type = 0";
                        $getreceipt = Yii::app()->db->createCommand($recsql)->queryRow();
                        $receipt = $getreceipt['name'];
                        $reamnt = $dt['amount'];
                    } elseif ($dt['type'] == 1) {
                        $receipt = '';
                        $reamnt = '';
                        $expsql = "SELECT `name` FROM " . $tblpx . "company_expense_type "
                            . " WHERE company_exp_id = '" . $dt['exp_type_id'] . "' AND type = 1";
                        $getExpense = Yii::app()->db->createCommand($expsql)->queryRow();
                        $paymnt = $getExpense['name'];
                        $payamnt = $dt['amount'];
                    }
                    $res = array($dt['dailyexp_id'], $paymnt, $payamnt, $receipt, $reamnt, $dt['description']);
                    array_push($data1, $res);
                    $slno++;
                }
                echo json_encode(array('result' => $data1));
            } else {
                $data1 = array();
                $res = array("", "", "", "", "", "");
                array_push($data1, $res);
                echo json_encode(array("result" => $data1));
            }
        }
    }

    public function actionHandsonDelete()
    {

        $id = $_POST['id'];
        $tblpx = Yii::app()->db->tablePrefix;
        $query = Yii::app()->db->createCommand('DELETE FROM ' . $tblpx . 'dailyexpense WHERE dailyexp_id=' . $id)->execute();
        if ($query) {
            $query1 = Yii::app()->db->createCommand("UPDATE " . $tblpx . "db_changes SET `status`=2 WHERE data_id = '" . $id . "' AND status=0 AND table_name='" . $tblpx . "dailyexpense'")->execute();
            $queryval = Yii::app()->db->createCommand('SELECT count(*) FROM  ' . $tblpx . 'db_changes WHERE data_id=' . $id . ' AND status=1 AND table_name="' . $tblpx . 'dailyexpense"')->queryScalar();
            if ($queryval) {
                $query2 = Yii::app()->db->createCommand("UPDATE " . $tblpx . "db_changes SET `status`=0 WHERE data_id = '" . $id . "' AND action='DELETE' AND table_name='" . $tblpx . "dailyexpense'")->execute();
            }
        }

        $out = array('result' => 'ok');
        echo json_encode($out);
    }

    public function actionProgressSave()
    {
        $tblpx = Yii::app()->db->tablePrefix;

        if (!empty($_REQUEST['desc']) && ($_REQUEST['amnt'] > 0 && !empty($_REQUEST['type'])) || ($_REQUEST['reamnt'] > 0 && (!empty($_REQUEST['retype'])))) {
            /* --Mod by Rajisha -- Column expense type */
            $type_id = NULL;
            if (isset($_REQUEST['type']) || isset($_REQUEST['retype'])) {
                if ($_REQUEST['type'] != '') {
                    $type = $_REQUEST['type'];
                    $amount = $_REQUEST['amnt'];
                    $exp_type = 1;
                }
                if ($_REQUEST['retype'] != '') {
                    $type = $_REQUEST['retype'];
                    $amount = $_REQUEST['reamnt'];
                    $exp_type = 0;
                }
                $type_name = Yii::app()->db->createCommand("select `company_exp_id` from " . $tblpx . "company_expense_type where `name`='" . addslashes($type) . "'")->queryRow();
                $type_id = $type_name['company_exp_id'];
            }
            /* --Mod by Rajisha -- Column expense type */

            $model = new Dailyexpense();
            $model->date = date("Y-m-d", strtotime($_REQUEST['date']));
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date('Y-m-d');
            $model->updated_by = Yii::app()->user->id;
            $model->description = $_REQUEST['desc'];
            $model->amount = $amount;
            $model->exp_type_id = $type_id;
            $model->data_entry = "Offline";
            $model->exp_type = $exp_type;
            $model->save(false);

            echo json_encode(array("result" => "ok", "id" => Yii::app()->db->getLastInsertId()));
        } else {
            echo json_encode(array("result" => "empty"));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Dailyexpense the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Dailyexpense::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Dailyexpense $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'dailyexpense-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionNewlist()
    {

        $where = 'WHERE 1=1';

        $model = new Dailyexpense('search');
        $model->unsetAttributes();  // clear any default values

        if (isset($_GET['Dailyexpense'])) {

            //print_r($_GET['Dailyexpense']);die;
            $model->attributes = $_GET['Dailyexpense'];


            if ($_GET['Dailyexpense']['exp_type'] == 1) {
                $where .= ' AND exp_type = 1';
            }
            if ($_GET['Dailyexpense']['exp_type'] == 0) {
                $where .= ' AND exp_type = 0';
            }

            if ($_GET['Dailyexpense']['exp_type'] == "") {
                $where = 'WHERE 1=1';
            }


            if (!empty($_GET['Dailyexpense']['fromdate']) && !empty($_GET['Dailyexpense']['todate'])) {
                $fromdate = date('Y-m-d', strtotime($_GET['Dailyexpense']['fromdate']));
                $todate = date('Y-m-d', strtotime($_GET['Dailyexpense']['todate']));
                $where .= ' AND date between "' . $fromdate . '" and"' . $todate . '"';
            } else {
                if (!empty($_GET['Dailyexpense']['fromdate'])) {
                    $fromdate = date('Y-m-d', strtotime($_GET['Dailyexpense']['fromdate']));
                    $where .= ' AND date >= "' . $fromdate . '"';
                }
                if (!empty($_GET['Dailyexpense']['todate'])) {
                    $todate = date('Y-m-d', strtotime($_GET['Dailyexpense']['todate']));
                    $where .= ' AND date <= "' . $todate . '"';
                }
            }

            //die($where);
            $model->grandtotaldebit($where);
            $model->grandtotalcredit($where);
        }



        $model->grandtotaldebit($where);
        $model->grandtotalcredit($where);
        //$dataProvider = new CActiveDataProvider('Dailyexpense');
        $this->render('newlist', array(
            'model' => $model,
            'dataProvider' => $model->search(),
        ));
    }

    public function actionUpdatedailyexp($id)
    {
        //die($id);
        $this->layout = false;
        $model = $this->loadModel($id);
        $this->performAjaxValidation($model);

        if (isset($_POST['Dailyexpense'])) {

            //print_r($_POST['Dailyexpense']); die;
            $model->attributes = $_POST['Dailyexpense'];

            $model->exp_type = $_POST['Dailyexpense']['exp_type'];
            $model->exp_type_id = $_POST['Dailyexpense']['exp_type_id'];

            $model->date = date('Y-m-d', strtotime($_POST['Dailyexpense']['date']));
            $model->updated_by = yii::app()->user->id;
            $model->updated_date = date('Y-m-d');

            if ($model->save()) {
                $this->redirect(array('newList'));
            }
        }

        $this->render('updatedailyexp', array(
            'model' => $model,
        ));
    }

    public function actionGetexpensetypes()
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $exp_type = $_POST['exp_type'];
        $exptypes = CHtml::listData(Companyexpensetype::model()->findAll(array(
            'condition' => 'type in (select type from ' . $tblpx . 'company_expense_type where type=' . $exp_type . ' OR type = 2)',
        )), 'company_exp_id', 'name');

        if (count($exptypes)) {
            $exptypeslist = '<option value="">Select Expense Type</option>';
            foreach ($exptypes as $company_exp_id => $name) {
                $exptypeslist .= '<option value="' . $company_exp_id . '">' . $name . '</option>';
            }
        } else {
            $exptypeslist = '<option value="">No Vendors</option>';
        }

        $projarray = array('exptypeslist' => $exptypeslist);
        echo json_encode($projarray);
    }

    public function actionDeletedailyexp($id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $query = Yii::app()->db->createCommand('DELETE FROM ' . $tblpx . 'dailyexpense WHERE dailyexp_id=' . $id)->execute();
        if ($query) {
            echo json_encode(array('response' => 'success'));
        }
    }

    public function actionSavetopdf1($exp_type, $fromdate, $todate)
    {

        $model = new Dailyexpense('search');

        //echo $exp_type; die;
        $where = 'WHERE 1=1';

        if ($exp_type != "") {
            $where .= ' AND exp_type = "' . $exp_type . '"';
        }

        if (!empty($fromdate) && !empty($todate)) {
            $fromdate = date('Y-m-d', strtotime($fromdate));
            $todate = date('Y-m-d', strtotime($todate));
            $where .= ' AND date between "' . $fromdate . '" and"' . $todate . '"';
        }
        if (!empty($fromdate)) {
            $fromdate = date('Y-m-d', strtotime($fromdate));
            $where .= ' AND date >= "' . $fromdate . '"';
        }
        if (!empty($todate)) {
            $todate = date('Y-m-d', strtotime($todate));
            $where .= ' AND date <= "' . $todate . '"';
        }

        //echo $where; die;
        $model->grandtotaldebit($where);
        $model->grandtotalcredit($where);


        $tblpx = Yii::app()->db->tablePrefix;
        $data = Yii::app()->db->createCommand("SELECT {$tblpx}dailyexpense.*,{$tblpx}company_expense_type.name
            FROM `{$tblpx}dailyexpense`
            INNER JOIN `{$tblpx}company_expense_type` ON `{$tblpx}company_expense_type`.company_exp_id = {$tblpx}dailyexpense.exp_type_id
            " . $where . " ORDER BY date")->queryAll();

        //echo "<pre>";
        //print_r($data); die;

        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->WriteHTML($this->renderPartial('dailyexpensepdf', array(
            'model' => $data,
        ), true));
        $filename = "Daily Expense Details";
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    public function actionSavetoexcel1($exp_type, $fromdate, $todate)
    {

        $model = new Dailyexpense('search');

        //echo $exp_type; die;
        $where = 'WHERE 1=1';

        if ($exp_type != "") {
            $where .= ' AND exp_type = "' . $exp_type . '"';
        }

        if (!empty($fromdate) && !empty($todate)) {
            $fromdate = date('Y-m-d', strtotime($fromdate));
            $todate = date('Y-m-d', strtotime($todate));
            $where .= ' AND date between "' . $fromdate . '" and"' . $todate . '"';
        }
        if (!empty($fromdate)) {
            $fromdate = date('Y-m-d', strtotime($fromdate));
            $where .= ' AND date >= "' . $fromdate . '"';
        }
        if (!empty($todate)) {
            $todate = date('Y-m-d', strtotime($todate));
            $where .= ' AND date <= "' . $todate . '"';
        }

        //echo $where; die;
        $model->grandtotaldebit($where);
        $model->grandtotalcredit($where);


        $tblpx = Yii::app()->db->tablePrefix;
        $data = Yii::app()->db->createCommand("SELECT {$tblpx}dailyexpense.*,{$tblpx}company_expense_type.name
            FROM `{$tblpx}dailyexpense`
            INNER JOIN `{$tblpx}company_expense_type` ON `{$tblpx}company_expense_type`.company_exp_id = {$tblpx}dailyexpense.exp_type_id
            " . $where . " ORDER BY date")->queryAll();


        $arraylabel = array('Sl No.', 'Date', 'Description', 'Expense Type', 'Credit Amount', 'Debit Amount');

        $i = 0;
        $finaldata = array();


        $finaldata[$i][] = '';
        $finaldata[$i][] = '';
        $finaldata[$i][] = '';
        $finaldata[$i][] = 'Grand Total';
        $finaldata[$i][] = Yii::app()->user->totalcredit[0]['sumamt'];
        $finaldata[$i][] = Yii::app()->user->totaldebit[0]['sumamt'];

        $i = 1;

        foreach ($data as $key => $datavalue) {

            $finaldata[$key + 1][] = $i;
            $finaldata[$key + 1][] = ($datavalue['date'] != "") ? date("d-M-Y", strtotime($datavalue['date'])) : "";
            $finaldata[$key + 1][] = ($datavalue['description'] != "") ? $datavalue['description'] : "";
            $finaldata[$key + 1][] = ($datavalue['name'] != "") ? $datavalue['name'] : "";

            if ($datavalue['exp_type'] == 0) {
                $finaldata[$key + 1][] = $datavalue['amount'];
            } else {
                $finaldata[$key + 1][] = "";
            }

            if ($datavalue['exp_type'] == 1) {
                $finaldata[$key + 1][] = $datavalue['amount'];
            } else {
                $finaldata[$key + 1][] = "";
            }
            $i++;
        }

        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Daily Expense Details' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actionDynamicVendor()
    {
        $expId = $_POST["expid"];
        $tblpx = Yii::app()->db->tablePrefix;
        $vendorData = Yii::app()->db->createCommand("SELECT v.vendor_id as vendorid, v.name as vendorname FROM " . $tblpx . "vendors v LEFT JOIN " . $tblpx . "vendor_exptype vet ON v.vendor_id = vet.vendor_id WHERE vet.type_id = " . $expId)->queryAll();
        $vendorOptions = "<option value=''>-Select Vendor-</option>";
        foreach ($vendorData as $vData) {
            $vendorOptions .= "<option value='" . $vData["vendorid"] . "'>" . $vData["vendorname"] . "</option>";
        }
        echo $vendorOptions;
    }

    public function actionGetDataByDate()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
        }
        $newDate = date('Y-m-d', strtotime($_POST["date"]));
        $sql = "SELECT e.*, e.bill_id as billno, "
            . " et.name as typename,v.name vname,"
            . " ba.bank_name as bankname "
            . " FROM " . $tblpx . "dailyexpense e "
            . " LEFT JOIN " . $tblpx . "bills b "
            . " ON e.bill_id = b.bill_id "
            . " LEFT JOIN " . $tblpx . "company_expense_type et "
            . " ON e.expensehead_id = et.company_exp_id "
            . " LEFT JOIN " . $tblpx . "company_expense_type v "
            . " ON e.dailyexpense_receipt_head = v.company_exp_id "
            . " LEFT JOIN " . $tblpx . "bank ba "
            . " ON e.bank_id = ba.bank_id "
            . " WHERE e.date = '" . $newDate . "' "
            . " AND (" . $newQuery . ") "
            . " AND e.transaction_parent IS NULL";
        $expenseData = Yii::app()->db->createCommand($sql)->queryAll();

        $client = $this->renderPartial('newlist', array('newmodel' => $expenseData));
        return $client;
        //echo $newDate;
    }

    public function actionGetBillDetails()
    {
        $billId = $_POST["billid"];
        $tblpx = Yii::app()->db->tablePrefix;
        $billData = Yii::app()->db->createCommand("SELECT * from " . $tblpx . "billitem WHERE bill_id = " . $billId)->queryAll();
        $result["amount"] = 0;
        $result["sgstp"] = 0;
        $result["sgst"] = 0;
        $result["cgstp"] = 0;
        $result["cgst"] = 0;
        $result["total"] = 0;
        foreach ($billData as $item) {
            $result["amount"] = $result["amount"] + ($item["billitem_amount"] - $item["billitem_discountamount"]);
            $result["sgstp"] = $result["sgstp"] + $item["billitem_sgstpercent"];
            $result["sgst"] = $result["sgst"] + $item["billitem_sgst"];
            $result["cgstp"] = $result["cgstp"] + $item["billitem_cgstpercent"];
            $result["cgst"] = $result["cgst"] + $item["billitem_cgst"];
        }
        $result["total"] = $result["total"] + ($result["amount"] + $result["sgst"] + $result["cgst"]);
        echo json_encode($result);
    }

    public function actionGetDaybookDetails()
    {
        $expenseId = $_REQUEST["expenseid"];
        $expStatus = $_REQUEST["expstatus"];
        $tblpx = Yii::app()->db->tablePrefix;
        $expenseTbl = "" . $tblpx . "dailyexpense";
        if ($expStatus == 0) {
            $expenseTbl = "" . $tblpx . "pre_dailyexpenses";
        }
        $sql = "SELECT * "
            . " FROM " . $expenseTbl . " WHERE dailyexp_id = " . $expenseId;

        $expenseData = Yii::app()->db->createCommand($sql)->queryRow();
        $result["expid"] = $expenseData["dailyexp_id"];
        $result["expdate"] = $expenseData["date"];
        $result["comexptype"] = $expenseData["exp_type_id"];
        $result["expbill"] = $expenseData["bill_id"];
        $result["exptypeid"] = $expenseData["expensehead_id"];
        $result["exptypecc"] = $expenseData["expense_type"];
        $result["expamount"] = $expenseData["dailyexpense_amount"];
        $result["expsgstp"] = $expenseData["dailyexpense_sgstp"];
        $result["expsgst"] = $expenseData["dailyexpense_sgst"];
        $result["expcgstp"] = $expenseData["dailyexpense_cgstp"];
        $result["expcgst"] = $expenseData["dailyexpense_cgst"];
        $result["expigstp"] = $expenseData["dailyexpense_igstp"];
        $result["expigst"] = $expenseData["dailyexpense_igst"];
        $result["exptotal"] = $expenseData["amount"];
        $result["expdesc"] = $expenseData["description"];
        $result["exptype"] = $expenseData["exp_type"];
        $result["exprtype"] = $expenseData["dailyexpense_receipt_type"];
        $result["exprhead"] = $expenseData["dailyexpense_receipt_head"];
        $result["expreceipt"] = $expenseData["dailyexpense_receipt"];
        if ($expenseData["expensehead_type"] == 5) {
            $internalTrans = Dailyexpense::model()->findAll(array('condition' => 'transaction_parent="' . $expenseData["dailyexp_id"] . '"'));
            $result["expptype"] = $internalTrans[0]["dailyexpense_purchase_type"];
            $result["exppaid"] = $internalTrans[0]["dailyexpense_paidamount"];
        } else {
            $result["expptype"] = $expenseData["dailyexpense_purchase_type"];
            $result["exppaid"] = $expenseData["dailyexpense_paidamount"];
        }
        $result["bank"] = $expenseData["bank_id"];
        $result["chequeno"] = $expenseData["dailyexpense_chequeno"];
        $result["expuser"] = $expenseData["user_id"];
        $result["expemployee"] = $expenseData["employee_id"];
        $result["dexptype"] = $expenseData["dailyexpense_type"];
        $result["company"] = $expenseData["company_id"];
        $result["headtype"] = $expenseData["expensehead_type"];
        echo json_encode($result);
    }

    public function actionAddDailyexpense()
    {
        $model = new Dailyexpense;
        $reconmodel = new Reconciliation;
        $tblpx = Yii::app()->db->tablePrefix;
        if (isset($_POST['Dailyexpense'])) {
            $headType = $_REQUEST["headtype"];
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
            }
            if ($headType == 4 || $headType == 5) {
                $model1 = new Dailyexpense;
                $model2 = new Dailyexpense;
                $reconmodel1 = new Reconciliation;
                $expensetype = Companyexpensetype::model()->findByAttributes(array('name' => 'Cash Withdrawal'));

                $model2->reconciliation_status = NULL;
                if ($headType == 4) {
                    // widthdrowal from bank
                    //expense
                    $model1->attributes = $_POST['Dailyexpense'];
                    $headType = 1;
                    $model1->date = date('Y-m-d', strtotime($_POST['Dailyexpense']['date']));
                    $expHead = $_POST['Dailyexpense']['expensehead_id'];
                    $expHeadArr = explode(',', $expHead);
                    $expHeadId = $expHeadArr[1];
                    $model1->reconciliation_status = NULL;
                    $model1->exp_type_id = 0;
                    $model1->dailyexpense_type = 1;
                    $model1->expense_type = 88;  // default as cheque type
                    $reconmodel->reconciliation_payment = "Dailyexpense Payment";
                    $model1->expensehead_id = $expensetype['company_exp_id'];
                    $model1->dailyexpense_receipt_type = NULL;
                    $model1->dailyexpense_receipt_head = NULL;
                    $model1->dailyexpense_receipt = NULL;
                    $model1->dailyexpense_paidamount = $_POST['Dailyexpense']['dailyexpense_paidamount'];
                    if (isset($_POST['Dailyexpense']['petty_type'])) {
                        $model1->petty_payment_approval = $_POST['Dailyexpense']['petty_type'];
                    }
                    $reconmodel1->reconciliation_amount = $_POST['Dailyexpense']['dailyexpense_paidamount'];
                    if ($model1->expense_type == 88) {
                        $reconmodel1->reconciliation_table = $tblpx . "dailyexpense";
                        $reconmodel1->reconciliation_paymentdate = date('Y-m-d', strtotime($_POST['Dailyexpense']['date']));
                        $reconmodel1->reconciliation_bank = $_POST['Dailyexpense']['bank_id'];
                        $reconmodel1->reconciliation_chequeno = $_POST['Dailyexpense']['dailyexpense_chequeno'];
                        $reconmodel1->created_date = date("Y-m-d H:i:s");
                        $reconmodel1->reconciliation_status = 0;
                        $reconmodel1->company_id = $_POST['Dailyexpense']['company_id'];
                        $reconmodel1->reconciliation_payment = "Dailyexpense Payment";
                        $model1->reconciliation_status = 0;
                        $model1->reconciliation_date = date("Y-m-d");
                    } else {
                        $model1->bank_id = NULL;
                        if ($_POST['Dailyexpense']['bank_id'] != '') {
                            $model1->bank_id = $_POST['Dailyexpense']['bank_id'];
                        }
                        $model1->dailyexpense_chequeno = NULL;
                        if ($_POST['Dailyexpense']['dailyexpense_chequeno'] != '') {
                            $model1->dailyexpense_chequeno = $_POST['Dailyexpense']['dailyexpense_chequeno'];
                        }
                    }
                    $model1->user_id = Yii::app()->user->id;
                    $model1->created_by = Yii::app()->user->id;
                    $model1->created_date = date('Y-m-d');
                    $model1->exp_type = 73;
                    $model1->display_flg = 'No';
                    $model1->expensehead_type = 4;

                    // receipt

                    $model2->attributes = $_POST['Dailyexpense'];
                    $headType = 2;
                    $model2->date = date('Y-m-d', strtotime($_POST['Dailyexpense']['date']));
                    $expHead = $_POST['Dailyexpense']['expensehead_id'];
                    $expHeadArr = explode(',', $expHead);
                    $expHeadId = $expHeadArr[1];
                    $model2->reconciliation_status = 0;
                    $model2->exp_type_id = 0;
                    $model2->expensehead_id = NULL;
                    $model2->expense_type = NULL;
                    $model2->dailyexpense_receipt_type = 89;
                    if ($model2->dailyexpense_receipt_type == 88) {
                        $model2->parent_status = 0;
                    }
                    $model2->dailyexpense_receipt_head = $expensetype['company_exp_id'];
                    $model2->dailyexpense_receipt = $_POST['Dailyexpense']['amount'];
                    $model2->dailyexpense_purchase_type = NULL;
                    $model2->dailyexpense_paidamount = NULL;
                    $model2->dailyexpense_type = 3;
                    $model2->bank_id = NULL;
                    $model2->dailyexpense_chequeno = NULL;
                    $model2->user_id = Yii::app()->user->id;
                    $model2->created_by = Yii::app()->user->id;
                    $model2->created_date = date('Y-m-d');
                    $model2->exp_type = 72;
                    $model2->display_flg = 'No';
                    $model2->expensehead_type = 4;
                    $model1->expense_type = $_POST['Dailyexpense']['expense_type']; //assign selected type
                    if ($model1->save()) {
                        $lastInsExpId = Yii::app()->db->getLastInsertID();
                        $reconmodel1->reconciliation_parentid = $lastInsExpId;
                        $model2->transaction_parent = $lastInsExpId;
                        $reconmodel1->save();
                        if ($model2->save()) {
                            $result_ = array('status' => '', 'id' => $model1->dailyexp_id);
                            echo json_encode($result_);
                            exit;
                        }
                    } else {
                        $error_message = $this->setErrorMessage($model1->getErrors());
                        if ($error_message == 'insufficient balance') {
                            $invoice_amount = BalanceHelper::cashInvoice(
                                $model1->expense_type,
                                $model1->company_id,
                                $model1->user_id,
                                $model1->bank_id,
                                $model1->date
                            );
                            $expense_amount = BalanceHelper::cashExpense(
                                $model1->expense_type,
                                $model1->company_id,
                                $model1->user_id,
                                $model1->dailyexp_id,
                                $model1->bank_id,
                                $model1->date
                            );

                            $available_amount = $invoice_amount - $expense_amount;
                            $result_ = array('status' => 5, 'negative_amount' => $available_amount);
                            echo json_encode($result_);
                            exit;
                        } else {
                            $result_ = array('status' => 1, 'error_message' => $error_message);
                            echo json_encode($result_);
                            exit;
                        }
                    }

                } else {
                    // cash deposit to bank
                    $expensetype = Companyexpensetype::model()->findByAttributes(array('name' => 'Cash Deposit'));
                    // epense
                    $model1->attributes = $_POST['Dailyexpense'];
                    $headType = $_REQUEST["headtype"];
                    $model1->date = date('Y-m-d', strtotime($_POST['Dailyexpense']['date']));
                    $model1->bill_id = $_POST['Dailyexpense']['bill_id'];
                    $expHead = $_POST['Dailyexpense']['expensehead_id'];
                    $expHeadArr = explode(',', $expHead);
                    $expHeadId = $expHeadArr[1];
                    $model1->dailyexpense_igstp = $_POST['Dailyexpense']['dailyexpense_igstp'];
                    $model1->dailyexpense_igst = $_POST['Dailyexpense']['dailyexpense_igst'];
                    $model1->reconciliation_status = 0;
                    $model1->employee_id = $_POST['Dailyexpense']['employee_id'];
                    $model1->exp_type_id = 0;
                    $model1->dailyexpense_type = 1;
                    $model1->expensehead_id = $expensetype['company_exp_id'];
                    $model1->expense_type = 89;
                    $model1->dailyexpense_receipt_type = NULL;
                    $model1->dailyexpense_receipt_head = NULL;
                    $model1->dailyexpense_receipt = NULL;
                    $model1->dailyexpense_purchase_type = $_POST['Dailyexpense']['dailyexpense_purchase_type'];
                    $model1->dailyexpense_paidamount = $_POST['Dailyexpense']['dailyexpense_paidamount'];
                    $model1->bank_id = $_POST['Dailyexpense']['bank_id'];
                    $model1->dailyexpense_chequeno = NULL;
                    $model1->user_id = Yii::app()->user->id;
                    $model1->created_by = Yii::app()->user->id;
                    $model1->created_date = date('Y-m-d');
                    $model1->exp_type = 73;
                    $model1->display_flg = 'No';
                    $model1->expensehead_type = 5;
                    if (isset($_POST['Dailyexpense']['petty_type'])) {
                        $model1->petty_payment_approval = $_POST['Dailyexpense']['petty_type'];
                    }
                    // receipt

                    $model2->attributes = $_POST['Dailyexpense'];
                    $headType = $_REQUEST["headtype"];
                    $model2->date = date('Y-m-d', strtotime($_POST['Dailyexpense']['date']));
                    $model2->bill_id = $_POST['Dailyexpense']['bill_id'];
                    $expHead = $_POST['Dailyexpense']['expensehead_id'];
                    $expHeadArr = explode(',', $expHead);
                    $expHeadId = $expHeadArr[1];
                    $model2->dailyexpense_igstp = $_POST['Dailyexpense']['dailyexpense_igstp'];
                    $model2->dailyexpense_igst = $_POST['Dailyexpense']['dailyexpense_igst'];
                    $model2->reconciliation_status = NULL;
                    $model2->employee_id = $_POST['Dailyexpense']['employee_id'];
                    $model2->exp_type_id = 0;
                    $model2->expensehead_id = NULL;
                    $model2->expense_type = NULL;
                    $model2->dailyexpense_receipt_type = 88;
                    $model2->dailyexpense_receipt_head = $expensetype['company_exp_id'];
                    $model2->dailyexpense_receipt = $_POST['Dailyexpense']['amount'];
                    $model2->dailyexpense_purchase_type = NULL;
                    $model2->dailyexpense_paidamount = NULL;
                    $model2->dailyexpense_type = 3;
                    $reconmodel1->reconciliation_payment = "Dailyexpense Receipt";
                    $reconmodel1->reconciliation_amount = $_POST['Dailyexpense']['amount'];
                    $reconmodel->reconciliation_payment = "Dailyexpense Receipt";
                    if ($model2->dailyexpense_receipt_type == 88) {
                        $model2->bank_id = $_POST['Dailyexpense']['bank_id'];
                        $model2->dailyexpense_chequeno = $_POST['Dailyexpense']['dailyexpense_chequeno'];
                        $reconmodel1->reconciliation_table = $tblpx . "dailyexpense";
                        $reconmodel1->reconciliation_paymentdate = date('Y-m-d', strtotime($_POST['Dailyexpense']['date']));
                        $reconmodel1->reconciliation_bank = $_POST['Dailyexpense']['bank_id'];
                        $reconmodel1->reconciliation_chequeno = $_POST['Dailyexpense']['dailyexpense_chequeno'];
                        $reconmodel1->created_date = date("Y-m-d H:i:s");
                        $reconmodel1->reconciliation_status = 0;
                        $reconmodel1->company_id = $_POST['Dailyexpense']['company_id'];
                        $model2->reconciliation_status = 0;
                        $model2->reconciliation_date = date("Y-m-d");
                    } else {
                        $model2->bank_id = NULL;
                        if ($_POST['Dailyexpense']['bank_id'] != '') {
                            $model2->bank_id = $_POST['Dailyexpense']['bank_id'];
                        }
                        $model2->dailyexpense_chequeno = NULL;
                        if ($_POST['Dailyexpense']['dailyexpense_chequeno'] != '') {
                            $model2->dailyexpense_chequeno = $_POST['Dailyexpense']['dailyexpense_chequeno'];
                        }
                    }
                    $model2->user_id = Yii::app()->user->id;
                    $model2->created_by = Yii::app()->user->id;
                    $model2->created_date = date('Y-m-d');
                    $model2->exp_type = 72;
                    $model2->display_flg = 'No';
                    $model2->expensehead_type = 5;

                    if ($model2->save()) {
                        $lastInsExpId = Yii::app()->db->getLastInsertID();
                        $reconmodel1->reconciliation_parentid = $lastInsExpId;
                        $model1->transaction_parent = $lastInsExpId;
                        if ($model2->dailyexpense_receipt_type == 88) {
                            if (!$reconmodel1->save()) {
                                echo "recon not saved";
                            }
                        }
                    }


                    if ($model1->save()) {
                        $result_ = array('status' => '', 'id' => $model1->dailyexp_id);
                        echo json_encode($result_);
                        exit;
                    } else {
                        $model2->delete();
                        $error_message = $this->setErrorMessage($model1->getErrors());
                        if ($error_message == 'insufficient balance') {
                            $invoice_amount = BalanceHelper::cashInvoice(
                                $model1->expense_type,
                                $model1->company_id,
                                $model1->user_id,
                                $model1->bank_id,
                                $model1->date
                            );
                            $expense_amount = BalanceHelper::cashExpense(
                                $model1->expense_type,
                                $model1->company_id,
                                $model1->user_id,
                                $model1->dailyexp_id,
                                $model1->bank_id,
                                $model1->date
                            );
                            $available_amount = $invoice_amount - $expense_amount;
                            $result_ = array('status' => 5, 'negative_amount' => $available_amount);
                            echo json_encode($result_);
                            exit;
                        } else {
                            $result_ = array('status' => 1, 'error_message' => $error_message);
                            echo json_encode($result_);
                            exit;
                        }
                    }
                }





                $expDate = date('Y-m-d', strtotime($_POST['Dailyexpense']['date']));
                if ($expDate) {
                    $expsql = "SELECT e.*, e.bill_id as billno, et.name "
                        . " as typename,v.name vname,ba.bank_name as bankname "
                        . " FROM " . $tblpx . "dailyexpense e"
                        . " LEFT JOIN " . $tblpx . "bills b ON e.bill_id = b.bill_id"
                        . " LEFT JOIN " . $tblpx . "company_expense_type et "
                        . " ON e.expensehead_id = et.company_exp_id"
                        . " LEFT JOIN " . $tblpx . "company_expense_type v "
                        . " ON e.dailyexpense_receipt_head = v.company_exp_id"
                        . " LEFT JOIN " . $tblpx . "bank ba ON e.bank_id = ba.bank_id"
                        . " WHERE e.date = '" . $expDate . "' AND (" . $newQuery . ") "
                        . " AND e.transaction_parent IS NULL";
                    $expenseData = Yii::app()->db->createCommand($expsql)->queryAll();
                    $client = $this->renderPartial('newlist', array('newmodel' => $expenseData));
                    return $client;
                } else {
                    $result_ = array('status' => 1);
                    echo json_encode($result_);
                    exit;
                }
            } else {
                $model->attributes = $_POST['Dailyexpense'];
                $headType = $_REQUEST["headtype"];
                $model->date = date('Y-m-d', strtotime($_POST['Dailyexpense']['date']));
                $model->bill_id = $_POST['Dailyexpense']['bill_id'];
                $expHead = $_POST['Dailyexpense']['expensehead_id'];
                $expHeadArr = explode(',', $expHead);
                $expHeadId = $expHeadArr[1];

                $model->dailyexpense_igstp = $_POST['Dailyexpense']['dailyexpense_igstp'];
                $model->dailyexpense_igst = $_POST['Dailyexpense']['dailyexpense_igst'];
                $model->reconciliation_status = NULL;
                $model->employee_id = $_POST['Dailyexpense']['employee_id'];
                $model->expensehead_type = $headType;
                if (isset($_POST['Dailyexpense']['petty_type'])) {
                    $model->petty_payment_approval = $_POST['Dailyexpense']['petty_type'];
                }
                if ($headType == 1 || $headType == 3) {
                    if ($headType == 1) {
                        $model->exp_type_id = $expHeadId;
                        $model->dailyexpense_type = 1;
                        $reconmodel->reconciliation_payment = "Dailyexpense Payment";
                    } else {
                        $model->exp_type_id = NULL;
                        $model->dailyexpense_type = 2;
                        $reconmodel->reconciliation_payment = "Dailyexpense Deposit";
                    }
                    $model->expensehead_id = $expHeadId;
                    $model->expense_type = $_POST['Dailyexpense']['expense_type'];
                    $model->dailyexpense_receipt_type = NULL;
                    $model->dailyexpense_receipt_head = NULL;
                    $model->dailyexpense_receipt = NULL;
                    $model->dailyexpense_purchase_type = $_POST['Dailyexpense']['dailyexpense_purchase_type'];
                    $model->dailyexpense_paidamount = $_POST['Dailyexpense']['dailyexpense_paidamount'];
                    $reconmodel->reconciliation_amount = $_POST['Dailyexpense']['dailyexpense_paidamount'];
                } else if ($headType == 2) {
                    $model->exp_type_id = $expHeadId;
                    $model->expensehead_id = NULL;
                    $model->expense_type = NULL;
                    $model->dailyexpense_receipt_type = $_POST['Dailyexpense']['expense_type'];
                    $model->dailyexpense_receipt_head = $expHeadId;
                    $model->dailyexpense_receipt = $_POST['Dailyexpense']['amount'];
                    $model->dailyexpense_purchase_type = NULL;
                    $model->dailyexpense_paidamount = NULL;
                    $model->dailyexpense_type = 3;
                    $reconmodel->reconciliation_payment = "Dailyexpense Receipt";
                    $reconmodel->reconciliation_amount = $_POST['Dailyexpense']['amount'];
                }
                if ($_POST['Dailyexpense']['expense_type'] == 88) {
                    $model->reconciliation_status = 0;
                    $model->bank_id = $_POST['Dailyexpense']['bank_id'];
                    $model->dailyexpense_chequeno = $_POST['Dailyexpense']['dailyexpense_chequeno'];
                    $reconmodel->reconciliation_table = $tblpx . "dailyexpense";
                    $reconmodel->reconciliation_paymentdate = date('Y-m-d', strtotime($_POST['Dailyexpense']['date']));
                    $reconmodel->reconciliation_bank = $_POST['Dailyexpense']['bank_id'];
                    $reconmodel->reconciliation_chequeno = $_POST['Dailyexpense']['dailyexpense_chequeno'];
                    $reconmodel->created_date = date("Y-m-d H:i:s");
                    $reconmodel->reconciliation_status = 0;
                    $reconmodel->company_id = $_POST['Dailyexpense']['company_id'];
                } else {
                    $model->bank_id = NULL;
                    $model->bank_id = NULL;
                    $model->dailyexpense_chequeno = NULL;
                }

                $model->user_id = Yii::app()->user->id;
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date('Y-m-d');

                if ($headType == 2) {
                    $model->exp_type = 72;
                } else {
                    $model->exp_type = 73;
                }
                if ($model->save()) {
                    $lastInsExpId = Yii::app()->db->getLastInsertID();
                    $reconmodel->reconciliation_parentid = $lastInsExpId;
                    if ($_POST['Dailyexpense']['expense_type'] == 88) {
                        $reconmodel->save();
                    }
                    //due bill status update block
                    $billId = isset($_POST['Dailyexpense']['bill_id']) ? $_POST['Dailyexpense']['bill_id'] : null;
                    $companyId = isset($_POST['Dailyexpense']['company_id']) ? $_POST['Dailyexpense']['company_id'] : null;
                    if ($billId !== null && $companyId !== null) {
                        $dailyExpensesExist = DailyexpenseBill::model()->exists('bill_no = :billId AND company_id = :companyId', array(':billId' => $billId, ':companyId' => $companyId));
                        if ($dailyExpensesExist) {
                            DailyexpenseBill::model()->updateAll(array('paid_status' => 1), 'bill_no = :billId', array(':billId' => $billId));
                        }
                    }
                    //due bill status update block ends
                    $expDate = date('Y-m-d', strtotime($_POST['Dailyexpense']['date']));
                    $exp_sql = "SELECT e.*, e.bill_id as billno, et.name "
                        . " as typename,v.name vname,ba.bank_name as bankname "
                        . " FROM " . $tblpx . "dailyexpense e"
                        . " LEFT JOIN " . $tblpx . "bills b ON e.bill_id = b.bill_id"
                        . " LEFT JOIN " . $tblpx . "company_expense_type et "
                        . " ON e.expensehead_id = et.company_exp_id"
                        . " LEFT JOIN " . $tblpx . "company_expense_type v "
                        . " ON e.dailyexpense_receipt_head = v.company_exp_id"
                        . " LEFT JOIN " . $tblpx . "bank ba "
                        . " ON e.bank_id = ba.bank_id"
                        . " WHERE e.date = '" . $expDate . "' AND (" . $newQuery . ") "
                        . " AND e.transaction_parent IS NULL";
                    $expenseData = Yii::app()->db->createCommand($exp_sql)->queryAll();

                    $result_ = array('status' => '', 'id' => $model->dailyexp_id);
                    echo json_encode($result_);
                    exit;
                } else {
                    $error_message = $this->setErrorMessage($model->getErrors());
                    if ($error_message == 'insufficient balance') {
                        $invoice_amount = BalanceHelper::cashInvoice(
                            $model->expense_type,
                            $model->company_id,
                            $model->user_id,
                            $model->bank_id,
                            $model->date
                        );
                        $expense_amount = BalanceHelper::cashExpense(
                            $model->expense_type,
                            $model->company_id,
                            $model->user_id,
                            $model->dailyexp_id,
                            $model->bank_id,
                            $model->date
                        );

                        $available_amount = $invoice_amount - $expense_amount;
                        $result_ = array('status' => 5, 'negative_amount' => $available_amount);
                        echo json_encode($result_);
                        exit;
                    } else {
                        $result_ = array('status' => 1, 'error_message' => $error_message);
                        echo json_encode($result_);
                        exit;
                    }
                }
            }
        }
    }
    public function setErrorMessage($modelErrors)
    {
        if (!empty($modelErrors)) {
            foreach ($modelErrors as $key => $value) {
                return $value[0];
            }
        }
    }

    public function actionUpdateDailyexpense()
    {

        if (isset($_REQUEST['Dailyexpense'])) {
            $id = $_REQUEST['Dailyexpense']['txtExpensesId'];
            $model = Dailyexpense::model()->findByPk($id);
            $premodel = new PreDailyexpenses;
            $created_by = $model->created_by;
            $created_date = $model->created_date;
            $reconmodel = array();
            if ($_REQUEST['Dailyexpense']['expense_type'] == 88) {
                $reconmodel = Reconciliation::model()->findByAttributes(array(
                    'reconciliation_parentid' => $id,
                ));
            }

            $headType = $_REQUEST["headtype"];
            $transCount = Dailyexpense::model()->countByAttributes(array(
                'transaction_parent' => $id
            ));

            if ($transCount > 0) {
                $internalTrans = Dailyexpense::model()->findAll(array('condition' => 'transaction_parent="' . $id . '"'));
                $transId = $internalTrans[0]["dailyexp_id"];
                $model2 = Dailyexpense::model()->findByPk($id);
                $model1 = Dailyexpense::model()->findByPk($transId);

                if ($headType == 4) { // widthdrowal from bank                    
                    $withdrawData = $this->saveWithdrawalFromBankData($id, $transId, $_REQUEST['Dailyexpense'], $premodel, $reconmodel, $created_by, $created_date);
                    $model1 = $withdrawData['model1'];
                    $model2 = $withdrawData['model2'];
                    $premodel = $withdrawData['premodel'];
                    $reconmodel = $withdrawData['reconmodel'];

                    if ($model1->save()) {
                        if ($model1->expense_type == 88) {
                            $reconmodel->save();
                        }
                        if (Yii::app()->user->role != 1) {
                            $premodel->save();
                        }
                        if ($model2->save()) {
                            $result_ = array('status' => '', 'id' => $model1->dailyexp_id);
                            echo json_encode($result_);
                            exit;
                        }
                    }
                } else {// cash deposit to bank
                    $depositData = $this->saveDepositToBankData($id, $transId, $_REQUEST['Dailyexpense'], $premodel, $reconmodel, $created_by, $created_date);
                    $model1 = $depositData['model1'];
                    $model2 = $depositData['model2'];
                    $premodel = $depositData['premodel'];
                    $reconmodel = $depositData['reconmodel'];

                    if ($model2->save()) {
                        if ($model2->dailyexpense_receipt_type == 88) {
                            $reconmodel->save();
                        }
                        if (Yii::app()->user->role != 1) {
                            $premodel->save();
                        }
                    }

                    if ($model1->save()) {
                        $result_ = array('status' => '', 'id' => $model1->dailyexp_id);
                        echo json_encode($result_);
                        exit;
                    }
                }
            } else {
                if (Yii::app()->user->role == 1) {
                    $model->attributes = $_REQUEST['Dailyexpense'];
                    $headType = $_REQUEST["headtype"];
                    $model->date = date('Y-m-d', strtotime($_REQUEST['Dailyexpense']['date']));
                    $model->reconciliation_status = NULL;

                    if ($model->expense_type == 88) {
                        $model->reconciliation_status = 0;
                        $reconmodel->reconciliation_bank = $model->bank_id;
                        $reconmodel->reconciliation_chequeno = $model->dailyexpense_chequeno;
                        $reconmodel->reconciliation_status = 0;
                        $reconmodel->company_id = $model->company_id;
                        $reconmodel->reconciliation_amount = $model->amount;
                        $reconmodel->reconciliation_payment = "Dailyexpense Payment";
                    }
                    $expHead = $_REQUEST['Dailyexpense']['expensehead_id'];
                    $expHeadArr = explode(',', $expHead);
                    $expHeadId = $expHeadArr[1];
                    $model->employee_id = $_REQUEST['Dailyexpense']['employee_id'];
                    $model->expensehead_type = $headType;
                    $model->exp_type = 73;

                    if ($headType == 1 || $headType == 3) {
                        if ($headType == 1) {
                            $model->exp_type_id = $expHeadId;
                            $model->dailyexpense_type = 1;
                        } else {
                            $model->exp_type_id = NULL;
                            $model->dailyexpense_type = 2;
                        }
                        $model->expensehead_id = $expHeadId;
                        $model->expense_type = $_REQUEST['Dailyexpense']['expense_type'];
                        $model->dailyexpense_receipt_type = NULL;
                        $model->dailyexpense_receipt_head = NULL;
                        $model->dailyexpense_receipt = NULL;
                        $model->dailyexpense_purchase_type = $_REQUEST['Dailyexpense']['dailyexpense_purchase_type'];
                        $model->dailyexpense_paidamount = $_REQUEST['Dailyexpense']['dailyexpense_paidamount'];
                    } else if ($headType == 2) {
                        $model->exp_type_id = $expHeadId;
                        $model->expensehead_id = NULL;
                        $model->expense_type = NULL;
                        $model->dailyexpense_receipt_type = $_REQUEST['Dailyexpense']['expense_type'];
                        $model->dailyexpense_receipt_head = $expHeadId;
                        $model->dailyexpense_receipt = $_REQUEST['Dailyexpense']['amount'];
                        $model->dailyexpense_purchase_type = NULL;
                        $model->dailyexpense_paidamount = NULL;
                        $model->dailyexpense_type = 3;
                        $model->exp_type = 72;
                    }


                    if ($model->save()) {
                        if ($model->expense_type == 88) {
                            if (!$reconmodel->save()) {
                                print_r($reconmodel->getErrors());
                            }
                        }
                        $result_ = array('status' => '', 'id' => $model->dailyexp_id);
                        echo json_encode($result_);
                        exit;
                    } else {
                        $error_message = $this->setErrorMessage($model->getErrors());
                        $result_ = array('status' => 1, 'error_message' => $error_message);
                        echo json_encode($result_);
                        exit;

                    }
                } else {  //update by non -admin user                    
                    $model->approval_status = 0;
                    $premodel->attributes = $_REQUEST['Dailyexpense'];
                    $premodel->ref_id = $id;
                    $headType = $_REQUEST["headtype"];
                    $premodel->date = date('Y-m-d', strtotime($_REQUEST['Dailyexpense']['date']));
                    $sql = "SELECT count(*) as count,MAX(dailyexp_id) as lastId "
                        . " FROM " . $this->tableNameAcc('pre_dailyexpenses', 0) . " "
                        . " WHERE ref_id='" . $id . "'";
                    $followupdata = Yii::app()->db->createCommand($sql)->queryRow();
                    if ($followupdata['count'] > 0) {
                        $premodel->followup_id = $followupdata['lastId'];
                    } else {
                        $premodel->followup_id = NULL;
                    }


                    $premodel->record_grop_id = date('Y-m-d H:i:s');
                    $premodel->approval_status = 0;
                    $premodel->record_action = "update";
                    $premodel->approve_notify_status = 1;
                    $premodel->user_id = Yii::app()->user->id;
                    $premodel->created_by = $created_by;
                    $premodel->created_date = $created_date;
                    $premodel->updated_by = Yii::app()->user->id;
                    $premodel->updated_date = date('Y-m-d H:i:s');

                    $expHead = $_REQUEST['Dailyexpense']['expensehead_id'];
                    $expHeadArr = explode(',', $expHead);
                    $expHeadId = $expHeadArr[1];

                    $premodel->reconciliation_status = NULL;
                    if ($_REQUEST['Dailyexpense']['expense_type'] == 88) {
                        $premodel->reconciliation_status = 0;
                    }

                    $premodel->employee_id = $_REQUEST['Dailyexpense']['employee_id'];
                    $premodel->expensehead_type = $headType;
                    $premodel->exp_type = 73;
                    if ($headType == 1 || $headType == 3) {
                        if ($headType == 1) {
                            $premodel->exp_type_id = $expHeadId;
                            $premodel->dailyexpense_type = 1;
                        } else {
                            $premodel->exp_type_id = NULL;
                            $premodel->dailyexpense_type = 2;
                        }
                        $premodel->expensehead_id = $expHeadId;
                        $premodel->expense_type = $_REQUEST['Dailyexpense']['expense_type'];
                        $premodel->dailyexpense_receipt_type = NULL;
                        $premodel->dailyexpense_receipt_head = NULL;
                        $premodel->dailyexpense_receipt = NULL;
                        $premodel->dailyexpense_purchase_type = $_REQUEST['Dailyexpense']['dailyexpense_purchase_type'];
                        $premodel->dailyexpense_paidamount = $_REQUEST['Dailyexpense']['dailyexpense_paidamount'];
                    } else if ($headType == 2) {
                        $premodel->exp_type_id = $expHeadId;
                        $premodel->expensehead_id = NULL;
                        $premodel->expense_type = NULL;
                        $premodel->dailyexpense_receipt_type = $_REQUEST['Dailyexpense']['expense_type'];
                        $premodel->dailyexpense_receipt_head = $expHeadId;
                        $premodel->dailyexpense_receipt = $_REQUEST['Dailyexpense']['amount'];
                        $premodel->dailyexpense_purchase_type = NULL;
                        $premodel->dailyexpense_paidamount = NULL;
                        $premodel->dailyexpense_type = 3;
                        $premodel->exp_type = 72;
                    }


                    if ($model->save()) {
                        if (!$premodel->save()) {
                            $error_message = $this->setErrorMessage($premodel->getErrors());
                            $result_ = array('status' => 1, 'error_message' => $error_message);
                            echo json_encode($result_);
                            exit;
                        } else {
                            $result_ = array('status' => 'Edit', 'id' => $model1->dailyexp_id);
                            echo json_encode($result_);
                            exit;
                        }

                    } else {
                        $error_message = $this->setErrorMessage($model->getErrors());
                        $result_ = array('status' => 1, 'error_message' => $error_message);
                        echo json_encode($result_);
                        exit;
                    }
                }
            }
        }
    }

    public function actionDailyexpensereport()
    {
        $model = new Dailyexpense;
        $model->unsetAttributes();
        $expensehead1 = '';
        $expensehead2 = '';
        $type = '';
        $expHead = '';
        if (isset($_REQUEST["Dailyexpense"])) {
            $model->fromdate = $_REQUEST["Dailyexpense"]["date_from"];
            $model->todate = $_REQUEST["Dailyexpense"]["date_to"];
            $model->company_id = $_REQUEST["Dailyexpense"]["company_id"];
            $model->exp_type = $_REQUEST["Dailyexpense"]["exp_type"];
            $model->expensehead_type = $_REQUEST["Dailyexpense"]["expensehead_type"];
            $model->employee_id = $_REQUEST['Dailyexpense']['paid_to'];
            $expHead = $_REQUEST['Dailyexpense']['expensehead_id'];
            if ($expHead != '') {
                $expHeadArr = explode(',', $expHead);
                if ($expHeadArr[0] == 1 || $expHeadArr[0] == 3 || $expHeadArr[0] == 4) {
                    $model->expensehead_id = $expHeadArr[1];
                    $expensehead1 = $expHeadArr[1];
                    $type = $expHeadArr[0];
                } else {
                    $model->exp_type_id = $expHeadArr[1];
                    $expensehead2 = $expHeadArr[1];
                    $type = $expHeadArr[0];
                }
            }
            if ($model->exp_type == 72) {
                $model->dailyexpense_receipt_type = $_REQUEST["Dailyexpense"]["expense_type"];
            } else if ($model->exp_type == 73) {
                $model->dailyexpense_purchase_type = $_REQUEST["Dailyexpense"]["purchase_type"];
                $model->expense_type = $_REQUEST["Dailyexpense"]["expense_type"];
            } else {
                $model->expense_type = $_REQUEST["Dailyexpense"]["expense_type"];
                $model->dailyexpense_receipt_type = $_REQUEST["Dailyexpense"]["expense_type"];
                $model->dailyexpense_purchase_type = $_REQUEST["Dailyexpense"]["purchase_type"];
            }
        } else {
            $model->fromdate = date("Y-m-d", strtotime('-30 days', strtotime(date("Y-m-d"))));
            $model->todate = date("Y-m-d");
        }
        $this->render('dailyexpensereport', array('model' => $model, 'dataProvider' => $model->newsearch(), 'expensehead1' => $expensehead1, 'expensehead2' => $expensehead2, 'type' => $type, 'expHead' => $expHead));
    }

    public function actionSavetopdfdailyexpense()
    {
        $model = new Dailyexpense;
        $model->unsetAttributes();
        $model->fromdate = $_REQUEST["fromdate"];
        $model->todate = $_REQUEST["todate"];
        $model->company_id = $_REQUEST["company_id"];
        $model->exp_type = $_REQUEST["exp_type"];
        $model->dailyexpense_purchase_type = $_REQUEST["dailyexpense_purchase_type"];
        $model->expense_type = $_REQUEST["expense_type"];
        $expHead = $_REQUEST["expHead"];
        if ($expHead != '') {
            $expHeadArr = explode(',', $expHead);
            if ($expHeadArr[0] == 1 || $expHeadArr[0] == 3 || $expHeadArr[0] == 4) {
                $model->expensehead_id = $expHeadArr[1];
            } else {
                $model->exp_type_id = $expHeadArr[1];
            }
        }

        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $sql = 'SELECT template_name '
            . 'FROM jp_quotation_template '
            . 'WHERE status="1"';
        $selectedtemplate = Yii::app()->db->createCommand($sql)->queryScalar();
        $this->logo = $this->realpath_logo;
        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $mPDF1->AddPage('', '', '', '', '', 0, 0, 52, 20, 10, 0);
        $mPDF1->SetAutoPageBreak(true, 40);
        $mPDF1->WriteHTML($this->renderPartial('dailyexpense_report', array(
            'model' => $model,
            'dataProvider' => $model->newsearch(),
        ), true));
        $filename = "Daily Expense report";
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    public function actionSavetoexceldailyexpense()
    {
        $arraylabel = array(' ', 'Daily Expense Report', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
        $model = new Dailyexpense;
        $model->unsetAttributes();
        $model->fromdate = $_REQUEST["fromdate"];
        $model->todate = $_REQUEST["todate"];
        $model->company_id = $_REQUEST["company_id"];
        $model->exp_type = $_REQUEST["exp_type"];
        $model->dailyexpense_purchase_type = $_REQUEST["dailyexpense_purchase_type"];
        $model->expense_type = $_REQUEST["expense_type"];
        $expHead = $_REQUEST["expHead"];
        if ($expHead != '') {
            $expHeadArr = explode(',', $expHead);
            if ($expHeadArr[0] == 1 || $expHeadArr[0] == 3 || $expHeadArr[0] == 4) {
                $model->expensehead_id = $expHeadArr[1];
            } else {
                $model->exp_type_id = $expHeadArr[1];
            }
        }
        $dataProvider = $model->newsearch();
        $finaldata = array();
        $key = 0;
        $finaldata[$key][] = ' ';
        $finaldata[$key][] = ' ';
        $finaldata[$key][] = ' ';
        $finaldata[$key][] = ' ';
        $finaldata[$key][] = ' ';
        $finaldata[$key][] = ' ';
        $finaldata[$key][] = ' ';
        $finaldata[$key][] = ' ';
        $finaldata[$key][] = ' ';
        $finaldata[$key][] = ' ';
        $key = $key + 1;
        $finaldata[$key][] = 'Sl No.';
        $finaldata[$key][] = 'Company';
        $finaldata[$key][] = 'Bill No';
        $finaldata[$key][] = 'Expense Head';
        $finaldata[$key][] = 'Date';
        $finaldata[$key][] = 'Expense Type';
        $finaldata[$key][] = 'Payment';
        $finaldata[$key][] = 'Receipt Type';
        $finaldata[$key][] = 'Receipt';
        $finaldata[$key][] = 'Description';
        $key = $key + 1;
        $finaldata[$key][] = ' ';
        $finaldata[$key][] = ' ';
        $finaldata[$key][] = ' ';
        $finaldata[$key][] = ' ';
        $finaldata[$key][] = ' ';
        $finaldata[$key][] = 'Total';
        $finaldata[$key][] = Controller::money_format_inr($model->getSum($model->newsearch(), 'dailyexpense_paidamount'), 2, 1);
        $finaldata[$key][] = ' ';
        $finaldata[$key][] = Controller::money_format_inr($model->getSum($model->newsearch(), 'dailyexpense_receipt'), 2, 1);
        $finaldata[$key][] = ' ';
        $key = $key + 1;
        $s = 0;
        foreach ($dataProvider->getData() as $value) {
            $s++;
            $finaldata[$key][] = $s;
            $finaldata[$key][] = ($value->company_id) ? $value->Company->name : " ";
            ;
            $finaldata[$key][] = ($value->bill_id) ? $value->bill_id : " ";
            if ($value->dailyexpense_type == "deposit") {
                $finaldata[$key][] = ($value->expensehead_id) ? "Deposit - " . $value->Deposit->deposit_name : " ";
            } else if ($value->dailyexpense_type == "expense") {
                $finaldata[$key][] = ($value->expensehead_id) ? "Expense - " . $value->CompExpType->name : " ";
            } else {
                $finaldata[$key][] = ($value->exp_type_id) ? "Receipt - " . $value->expType->name : " ";
            }
            $finaldata[$key][] = ($value->date) ? date('d-m-Y', strtotime($value->date)) : " ";
            $finaldata[$key][] = ($value->expense_type) ? $value->ExpenseType->caption : " ";
            $finaldata[$key][] = ($value->dailyexpense_paidamount != '') ? Controller::money_format_inr($value->dailyexpense_paidamount, 2, 1) : '0.00';
            $finaldata[$key][] = ($value->dailyexpense_receipt_type) ? $value->ReceiptType->caption : " ";
            $finaldata[$key][] = ($value->dailyexpense_receipt != '') ? Controller::money_format_inr($value->dailyexpense_receipt, 2, 1) : '0.00';
            $finaldata[$key][] = ($value->description) ? $value->description : "";
            $key = $key + 1;
        }


        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Daily Expense Report' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actionAjaxcall()
    {
        echo json_encode('success');
    }

    public function actionDeletexpense()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $data = Dailyexpense::model()->findByPk($_REQUEST['expId']);
        $checkBalance = Dailyexpense::model()->checkDeleteBalance($data);
        $sql = "SELECT count(ref_id) as expcount  FROM {$tblpx}pre_dailyexpenses WHERE  approval_status=0 and ref_id =" . $_REQUEST['expId'];
        $dailyexpanse_details = Yii::app()->db->createCommand($sql)->queryRow();
        $dailyexpanse_count = $dailyexpanse_details['expcount'];
        if ($checkBalance == "1" && $dailyexpanse_count == 0) {

            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
            }
            $transferId = $_REQUEST["transferId"];
            if ($transferId > 0) {

                $transferData = Cashtransfer::model()->findByPk($transferId);
                $dailyexpense_entries = Dailyexpense::model()->findAll(array('condition' => 'transfer_parentid="' . $transferId . '"'));
                if (!empty($dailyexpense_entries)) {
                    foreach ($dailyexpense_entries as $entry) {
                        $recon = Reconciliation::model()->find(array("condition" => "reconciliation_parentid=" . $entry->dailyexp_id . ""));
                        if (!empty($recon)) {
                            $newmodel = new JpLog('search');
                            $newmodel->log_data = json_encode($recon->attributes);
                            $newmodel->log_action = 2;
                            $newmodel->log_table = $tblpx . "reconciliation";
                            $newmodel->log_datetime = date('Y-m-d H:i:s');
                            $newmodel->log_action_by = Yii::app()->user->id;
                            $newmodel->company_id = $recon->company_id;
                            $newmodel->save();

                        }
                        if (!empty($entry)) {
                            $newmodel1 = new JpLog('search');
                            $newmodel1->log_data = json_encode($entry->attributes);
                            $newmodel1->log_action = 2;
                            $newmodel1->log_table = $tblpx . "dailyexpense";
                            $newmodel1->log_datetime = date('Y-m-d H:i:s');
                            $newmodel1->log_action_by = Yii::app()->user->id;
                            $newmodel1->company_id = $recon->company_id;
                            $newmodel1->save();

                        }

                        $recon = Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $entry->dailyexp_id . ""));


                    }
                }

                $dailyexpense = Dailyexpense::model()->findAll(array('condition' => 'transfer_parentid="' . $transferId . '"'));

                Dailyexpense::model()->deleteAll(array('condition' => 'transfer_parentid = ' . $transferId));
                Cashtransfer::model()->deleteAll(array('condition' => 'cashtransfer_id = ' . $transferId));
                $expDate = date('Y-m-d', strtotime($_REQUEST['expense_date']));


                $sql = "SELECT e.*, e.bill_id as billno, et.name as typename,v.name vname, "
                    . " ba.bank_name as bankname FROM " . $tblpx . "dailyexpense e "
                    . " LEFT JOIN " . $tblpx . "bills b ON e.bill_id = b.bill_id "
                    . " LEFT JOIN " . $tblpx . "company_expense_type et "
                    . " ON e.expensehead_id = et.company_exp_id "
                    . " LEFT JOIN " . $tblpx . "company_expense_type v "
                    . " ON e.dailyexpense_receipt_head = v.company_exp_id "
                    . " LEFT JOIN " . $tblpx . "bank ba ON e.bank_id = ba.bank_id "
                    . " WHERE e.date = '" . $expDate . "' AND (" . $newQuery . ") "
                    . " AND e.transaction_parent IS NULL";
                $expenseData = Yii::app()->db->createCommand($sql)->queryAll();
                // $client         = $this->renderPartial('newlist', array('newmodel' => $expenseData));
                echo json_encode(array('response' => 0, 'data' => ''));
            } else {
                if ($data) {
                    $newmodel = new JpLog('search');
                    $newmodel->log_data = json_encode($data->attributes);
                    $newmodel->log_action = 2;
                    $newmodel->log_table = $tblpx . "dailyexpense";
                    $newmodel->log_datetime = date('Y-m-d H:i:s');
                    $newmodel->log_action_by = Yii::app()->user->id;
                    $newmodel->company_id = $data->company_id;
                    if ($newmodel->save()) {
                        if ($data->expensehead_type == 4 || $data->expensehead_type == 5) {
                            $model1 = Dailyexpense::model()->deleteAll(array("condition" => "transaction_parent=" . $data->dailyexp_id . ""));
                        }
                        $model = Dailyexpense::model()->deleteAll(array("condition" => "dailyexp_id=" . $_REQUEST['expId'] . ""));
                        Reconciliation::model()->deleteAll(array("condition" => "reconciliation_parentid=" . $_REQUEST['expId'] . ""));
                        $expDate = date('Y-m-d', strtotime($_REQUEST['expense_date']));

                        $sql = "SELECT e.*, e.bill_id as billno, et.name as typename,v.name vname, "
                            . " ba.bank_name as bankname FROM " . $tblpx . "dailyexpense e "
                            . " LEFT JOIN " . $tblpx . "bills b ON e.bill_id = b.bill_id "
                            . " LEFT JOIN " . $tblpx . "company_expense_type et "
                            . " ON e.expensehead_id = et.company_exp_id "
                            . " LEFT JOIN " . $tblpx . "company_expense_type v "
                            . " ON e.dailyexpense_receipt_head = v.company_exp_id "
                            . " LEFT JOIN " . $tblpx . "bank ba ON e.bank_id = ba.bank_id "
                            . " WHERE e.date = '" . $expDate . "' AND (" . $newQuery . ") "
                            . " AND e.transaction_parent IS NULL";
                        $expenseData = Yii::app()->db->createCommand($sql)->queryAll();
                        ;
                        // $client = $this->renderPartial('newlist', array('newmodel' => $expenseData));
                        echo json_encode(array('response' => 0, 'data' => ''));
                    } else {
                        echo json_encode(array('response' => 1, 'data' => ''));
                    }
                }
            }
        } else {
            echo json_encode(array('response' => 2, 'data' => $checkBalance));
        }
    }

    public function actionPettycash()
    {

        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery1 = "";
        $newQuery2 = "";
        $newQuery3 = "";
        foreach ($arrVal as $arr) {
            if ($newQuery1)
                $newQuery1 .= ' OR';
            if ($newQuery2)
                $newQuery2 .= ' OR';
            if ($newQuery3)
                $newQuery3 .= ' OR';
            $newQuery1 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "dailyexpense.company_id)";
            $newQuery2 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "expenses.company_id)";
            $newQuery3 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "subcontractor_payment.company_id)";
        }

        $dailyexpense = array();
        $employee_id = '';
        $date_from = '';
        $date_to = '';
        $company_id = '';
        $daybook = array();
        $dailyexpense_refund = array();
        $dailyexpense_exp = array();
        $scpayment = array();
        // if (isset($_REQUEST['employee_id']) && !empty($_REQUEST['employee_id'])) {
        $where = '';
        $where1 = '';
        $where2 = '';
        $employee_id = '';
        $date_from = '';
        $date_to = '';
        $company_id = '';
        $chquery1 = "";
        $chquery2 = "";
        $chcondition1 = "";
        $chcondition2 = "";
        if (isset($_REQUEST['employee_id']) && !empty($_REQUEST['employee_id'])) {
            $employee_id = $_REQUEST['employee_id'];
            $where .= " AND {$tblpx}dailyexpense.employee_id = " . $_REQUEST['employee_id'] . "";
            $where1 .= " AND {$tblpx}expenses.employee_id = " . $_REQUEST['employee_id'] . "";
            $where2 .= " AND {$tblpx}subcontractor_payment.employee_id = " . $_REQUEST['employee_id'] . "";
        }
        if (isset($_REQUEST['company_id']) && !empty($_REQUEST['company_id'])) {
            $company_id = $_REQUEST['company_id'];
            $where .= " AND {$tblpx}dailyexpense.company_id = " . $_REQUEST['company_id'] . "";
            $where1 .= " AND {$tblpx}expenses.company_id = " . $_REQUEST['company_id'] . "";
            $where2 .= " AND {$tblpx}subcontractor_payment.company_id = " . $_REQUEST['company_id'] . "";
        } else {
            $where .= " AND (" . $newQuery1 . ")";
            $where1 .= " AND (" . $newQuery2 . ")";
            $where2 .= " AND (" . $newQuery3 . ")";
        }

        if (!empty($_REQUEST['date_from']) && !empty($_REQUEST['date_to'])) {
            $date_from = date('Y-m-d', strtotime($_REQUEST['date_from']));
            $date_to = date('Y-m-d', strtotime($_REQUEST['date_to']));
            $where .= " AND {$tblpx}dailyexpense.date between '" . $date_from . "' and '" . $date_to . "'";
            $where1 .= " AND {$tblpx}expenses.date between '" . $date_from . "' and '" . $date_to . "'";
            $where2 .= " AND {$tblpx}subcontractor_payment.date between '" . $date_from . "' and '" . $date_to . "'";
        } else {
            if (!empty($_REQUEST['date_from'])) {
                $date_from = date('Y-m-d', strtotime($_REQUEST['date_from']));
                $where .= " AND {$tblpx}dailyexpense.date >= '" . $date_from . "'";
                $where1 .= " AND {$tblpx}expenses.date >= '" . $date_from . "'";
                $where2 .= " AND {$tblpx}subcontractor_payment.date >= '" . $date_from . "'";
            }
            if (!empty($_REQUEST['date_to'])) {
                $date_to = date('Y-m-d', strtotime($_REQUEST['date_to']));
                $where .= " AND {$tblpx}dailyexpense.date <= '" . $date_to . "'";
                $where1 .= " AND {$tblpx}expenses.date <= '" . $date_to . "'";
                $where2 .= " AND {$tblpx}subcontractor_payment.date <= '" . $date_to . "'";
            }
        }


        //petty cash advance
        $reconcilsql = "SELECT *  FROM `jp_reconciliation` "
            . " WHERE `reconciliation_status`='0' "
            . " and reconciliation_payment='Dailyexpense Payment' and "
            . "reconciliation_table='jp_dailyexpense'";
        $reconcil_dexpense_payment_data = Yii::app()->db->createCommand($reconcilsql)->queryAll();

        $payment_cheque_array = array();
        foreach ($reconcil_dexpense_payment_data as $data) {

            $value = "'{$data['reconciliation_chequeno']}'";
            if ($value != '') {
                $recon_data = $value;
                array_push($payment_cheque_array, $recon_data);
            }
        }
        $payment_cheque_array1 = implode(',', $payment_cheque_array);
        if (($payment_cheque_array1)) {
            $chquery1 = "(dailyexpense_chequeno NOT IN ( $payment_cheque_array1) OR dailyexpense_chequeno IS NULL)";
            $chcondition1 = " AND " . $chquery1 . "";
        }

        $petty_issued_id = Yii::app()->db->createCommand("SELECT `company_exp_id` 
            FROM `jp_company_expense_type` WHERE  UPPER(`name`)='PETTY CASH ISSUED'")->queryScalar();
        $dailyexpense = array();
        if ($petty_issued_id) {
            $dailyexpensesql = "SELECT * FROM " . $tblpx . "dailyexpense "
                . " LEFT JOIN " . $tblpx . "users "
                . " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid"
                . " WHERE " . $tblpx . "dailyexpense.exp_type =73 "
                . " AND " . $tblpx . "dailyexpense.expense_type  IN('89','88') " . $chcondition1 . "  "
                . " AND jp_dailyexpense.exp_type_id= $petty_issued_id " . $where . ""
                . " order by " . $tblpx . "dailyexpense.date asc";

            $dailyexpense = Yii::app()->db->createCommand($dailyexpensesql)->queryAll();
        }
        //petty cash refund
        $reconcil_receipt_sql = "SELECT *  FROM `jp_reconciliation` "
            . " WHERE `reconciliation_status`='0' "
            . " and reconciliation_payment='Dailyexpense Receipt' and "
            . "reconciliation_table='jp_dailyexpense'";
        $reconcil_dexpense_receipt_data = Yii::app()->db->createCommand($reconcil_receipt_sql)->queryAll();
        $receipt_cheque_array = array();
        foreach ($reconcil_dexpense_receipt_data as $receiptdata) {
            $receiptvalue = "'{$receiptdata['reconciliation_chequeno']}'";
            if ($receiptvalue != '') {
                $recon_receipt_data = $receiptvalue;
                array_push($receipt_cheque_array, $recon_receipt_data);
            }
        }
        $receipt_cheque_array1 = implode(',', $receipt_cheque_array);
        if (($receipt_cheque_array1)) {
            $chquery2 = "(dailyexpense_chequeno NOT IN ( $receipt_cheque_array1) OR dailyexpense_chequeno IS NULL)";
            $chcondition2 = " AND " . $chquery2 . "";
        }

        $petty_refund_id = Yii::app()->db->createCommand("SELECT `company_exp_id` 
            FROM `jp_company_expense_type` WHERE  UPPER(`name`)='PETTY CASH REFUND'")->queryScalar();
        $dailyexpense_refund = array();
        if ($petty_refund_id) {
            $dexp_refund_sql = "SELECT * FROM " . $tblpx . "dailyexpense "
                . " LEFT JOIN " . $tblpx . "users "
                . " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid "
                . " WHERE " . $tblpx . "dailyexpense.exp_type =72 "
                . " AND " . $tblpx . "dailyexpense.dailyexpense_receipt_type IN('88','89') " . $chcondition2 . "  "
                . "AND jp_dailyexpense.exp_type_id= $petty_refund_id " . $where . " "
                . " order by " . $tblpx . "dailyexpense.date asc";

            $dailyexpense_refund = Yii::app()->db->createCommand($dexp_refund_sql)->queryAll();
        }

        //petty cash expense
        $daybook_sql = "SELECT * ," . $tblpx . "expenses.description as dbookdesc "
            . " FROM " . $tblpx . "expenses LEFT JOIN " . $tblpx . "users "
            . " ON " . $tblpx . "expenses.employee_id= " . $tblpx . "users.userid "
            . " LEFT JOIN " . $tblpx . "projects "
            . " ON " . $tblpx . "projects.pid = " . $tblpx . "expenses.projectid "
            . " WHERE " . $tblpx . "expenses.type = 73  "
            . " AND " . $tblpx . "expenses.expense_type=103 " . $where1 . " "
            . " GROUP BY " . $tblpx . "expenses.projectid";
        $daybook = Yii::app()->db->createCommand($daybook_sql)->queryAll();
        $dailyexpense_exp_sql = "SELECT * FROM " . $tblpx . "dailyexpense "
            . " LEFT JOIN " . $tblpx . "users "
            . " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid "
            . " WHERE (" . $tblpx . "dailyexpense.dailyexpense_receipt_type = 103 "
            . " OR  " . $tblpx . "dailyexpense.expense_type = 103  )" . $where . " "
            . " order by " . $tblpx . "dailyexpense.date asc";
        $dailyexpense_exp = Yii::app()->db->createCommand($dailyexpense_exp_sql)->queryAll();
        $scp_sql = "SELECT * ," . $tblpx . "subcontractor_payment.description as description "
            . " FROM " . $tblpx . "subcontractor_payment LEFT JOIN " . $tblpx . "users "
            . " ON " . $tblpx . "subcontractor_payment.employee_id= " . $tblpx . "users.userid "
            . " LEFT JOIN " . $tblpx . "projects "
            . " ON " . $tblpx . "projects.pid = " . $tblpx . "subcontractor_payment.project_id "
            . " WHERE " . $tblpx . "subcontractor_payment.payment_type=103 " . $where2 . " ";
        //. " GROUP BY " . $tblpx . "subcontractor_payment.project_id";                
        $scpayment = Yii::app()->db->createCommand($scp_sql)->queryAll();




        $render_datas = array(
            'employee_id' => $employee_id,
            'dailyexpense' => $dailyexpense,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'daybook' => $daybook,
            'dailyexpense_refund' => $dailyexpense_refund,
            'company_id' => $company_id,
            'dailyexpense_exp' => $dailyexpense_exp,
            'scpayment' => $scpayment,
            'where' => $where,
            'where1' => $where1,
            'where2' => $where2,
            'petty_issued_id' => $petty_issued_id,
            'petty_refund_id' => $petty_refund_id
        );

        $this->render('pettycashreport', $render_datas);
    }

    public function actionpettycashpdfdetails($employee_id, $date_from, $date_to, $company_id, $ledger)
    {
        $where = '';
        $where1 = '';
        $finaldata = "";
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery1 = "";
        $newQuery2 = "";
        foreach ($arrVal as $arr) {
            if ($newQuery1)
                $newQuery1 .= ' OR';
            if ($newQuery2)
                $newQuery2 .= ' OR';
            $newQuery1 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "dailyexpense.company_id)";
            $newQuery2 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "expenses.company_id)";
        }
        if ($employee_id != '') {
            $where .= " AND {$tblpx}dailyexpense.employee_id = " . $employee_id . "";
            $where1 .= " AND {$tblpx}expenses.employee_id = " . $employee_id . "";
        }

        if (isset($company_id) && !empty($company_id)) {
            $where .= " AND {$tblpx}dailyexpense.company_id = " . $company_id . "";
            $where1 .= " AND {$tblpx}expenses.company_id = " . $company_id . "";
        } else {
            $where .= " AND (" . $newQuery1 . ")";
            $where1 .= " AND (" . $newQuery2 . ")";
        }

        if (($date_from != '') && ($date_to != '')) {
            $date_from = date('Y-m-d', strtotime($date_from));
            $date_to = date('Y-m-d', strtotime($date_to));
            $where .= " AND {$tblpx}dailyexpense.date between '" . $date_from . "' and '" . $date_to . "'";
            $where1 .= " AND {$tblpx}expenses.date between '" . $date_from . "' and '" . $date_to . "'";
        } else {
            if ($date_from != '') {
                $date_from = date('Y-m-d', strtotime($date_from));
                $where .= " AND {$tblpx}dailyexpense.date >= '" . $date_from . "'";
                $where1 .= " AND {$tblpx}expenses.date >= '" . $date_from . "'";
            }
            if ($date_to != '') {
                $date_to = date('Y-m-d', strtotime($date_to));
                $where .= " AND {$tblpx}dailyexpense.date <= '" . $date_to . "'";
                $where1 .= " AND {$tblpx}expenses.date <= '" . $date_to . "'";
            }
        }
        $dailyexpense = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid WHERE " . $tblpx . "dailyexpense.exp_type =73 AND " . $tblpx . "dailyexpense.expense_type=103 " . $where . "")->queryAll();
        $daybook = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "expenses LEFT JOIN " . $tblpx . "users ON " . $tblpx . "expenses.employee_id= " . $tblpx . "users.userid LEFT JOIN " . $tblpx . "projects ON " . $tblpx . "projects.pid = " . $tblpx . "expenses.projectid WHERE " . $tblpx . "expenses.type = 73  AND " . $tblpx . "expenses.expense_type=103 " . $where1 . " GROUP BY " . $tblpx . "expenses.projectid")->queryAll();
        $dailyexpense_refunds = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid WHERE " . $tblpx . "dailyexpense.exp_type =72 AND " . $tblpx . "dailyexpense.dailyexpense_receipt_type=103 " . $where . "")->queryAll();

        if ($ledger == 0) {
            if ((!empty($dailyexpense) || !empty($daybook) || !empty($dailyexpense_refund)) && ($employee_id != 0)) {

                $dailyexpense_amount = 0;
                $finaldata .= "<tr>
                                            <td colspan='2'><b>Petty Cash Advance</b></td>
                                        </tr>";
                foreach ($dailyexpense as $value) {
                    $dailyexpense_amount += $value['dailyexpense_paidamount'];
                    $finaldata .= "<tr>
                                                <td>" . date('d-M-Y', strtotime($value['date'])) . " " . $value['description'] . "</td>
                                                <td align='right'>" . Controller::money_format_inr($value['dailyexpense_paidamount'], 2) . "</td>
                                            </tr>";
                }

                $finaldata .= " <tr>
                                    <td><b>Net Amount</b></td>
                                    <td align='right'><b>" . Controller::money_format_inr($dailyexpense_amount, 2) . "</b></td>
                                </tr>";

                $finaldata .= "<tr>
                                            <td colspan='2' class='whiteborder'>&nbsp;</td>
                                        </tr>";

                $finaldata .= "<tr>
                                            <td colspan='2'><b>Petty Cash Expense</b></td>
                                        </tr>";
                $daybook_amount = 0;
                foreach ($daybook as $value) {
                    $finaldata .= "<tr>
                                            <th colspan='2'>" . $value['name'] . "</th>
                                        </tr>";

                    $where1 = '';
                    if (isset($employee_id)) {
                        $where1 .= " AND {$tblpx}expenses.employee_id = " . $employee_id . "";
                    }

                    if (isset($company_id) && !empty($company_id)) {
                        $where1 .= " AND {$tblpx}expenses.company_id = " . $company_id . "";
                    } else {
                        $where1 .= " AND (" . $newQuery2 . ")";
                    }

                    if (!empty($date_from) && !empty($date_to)) {
                        $date_from = date('Y-m-d', strtotime($date_from));
                        $date_to = date('Y-m-d', strtotime($date_to));
                        $where1 .= " AND {$tblpx}expenses.date between '" . $date_from . "' and '" . $date_to . "'";
                    } else {
                        if (!empty($date_from)) {
                            $date_from = date('Y-m-d', strtotime($date_from));
                            $where1 .= " AND {$tblpx}expenses.date >= '" . $date_from . "'";
                        }
                        if (!empty($date_to)) {
                            $date_to = date('Y-m-d', strtotime($date_to));
                            $where1 .= " AND {$tblpx}expenses.date <= '" . $date_to . "'";
                        }
                    }

                    $daybook_details = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "expenses LEFT JOIN " . $tblpx . "users ON " . $tblpx . "expenses.employee_id= " . $tblpx . "users.userid WHERE " . $tblpx . "expenses.type = 73  AND " . $tblpx . "expenses.expense_type=103 " . $where1 . "  AND " . $tblpx . "expenses.projectid = " . $value['pid'] . "")->queryAll();

                    $i = 0;
                    $item_amount = 0;
                    foreach ($daybook_details as $newsql) {
                        $i++;
                        if ($i % 2 == 0) {
                            $class = 'even';
                        } else {
                            $class = 'odd';
                        }
                        $item_amount += $newsql['paid'];

                        $finaldata .= "<tr class=" . $class . ">
                                                        <td>" . date('d-M-Y', strtotime($newsql['date'])) . " " . $newsql['description'] . "</td>
                                                        <td align='right'>" . Controller::money_format_inr($newsql['paid'], 2) . "</td>
                                                    </tr>";
                    }
                    $daybook_amount += $item_amount;
                    $finaldata .= " <tr>
                                    <td align='right' colspan='2'><b>" . Controller::money_format_inr($item_amount, 2) . "</b></td>
                                </tr>";
                }



                $finaldata .= " <tr>
                                    <td><b>Net Amount</b></td>
                                    <td align='right'><b>" . Controller::money_format_inr($daybook_amount, 2) . "</b></td>
                                </tr>";
                $finaldata .= "<tr>
                                            <td colspan='2' class='whiteborder'>&nbsp;</td>
                                        </tr>";

                $dailyexpense_refund = 0;
                $finaldata .= "<tr>
                                            <td colspan='2'><b>Petty Cash Refund</b></td>
                                        </tr>";
                foreach ($dailyexpense_refunds as $value) {
                    $dailyexpense_refund += $value['dailyexpense_receipt'];
                    $finaldata .= "<tr>
                                                <td>" . date('d-M-Y', strtotime($value['date'])) . " " . $value['description'] . "</td>
                                                <td align='right'>" . Controller::money_format_inr($value['dailyexpense_receipt'], 2) . "</td>
                                            </tr>";
                }

                $finaldata .= " <tr>
                                    <td><b>Net Amount</b></td>
                                    <td align='right'><b>" . Controller::money_format_inr($dailyexpense_refund, 2) . "</b></td>
                                </tr>";



                $finaldata .= "
        <tr>

                                    <td>&nbsp;</td>

                                    <td align='right'></td>


                                </tr>


                                <tr>

                                    <td>Petty Cash Advance : </td>
                                    <td align='right'><b>" . Controller::money_format_inr($dailyexpense_amount, 2) . "</b></td>



                                </tr>

                                <tr>

                                    <td>Petty Cash Expense :</td>
                                    <td align='right'><b>" . Controller::money_format_inr($daybook_amount, 2) . "</b></td>



                                </tr>
                                <tr>

                                    <td>Petty Cash Refund :</td>
                                    <td align='right'><b>" . Controller::money_format_inr($dailyexpense_refund, 2) . "</b></td>



                                </tr>

                                    ";





                $balance = $dailyexpense_amount - ($daybook_amount + $dailyexpense_refund);


                $titlebal = "Cash In Hand";


                $finaldata .= "<tr>

                                    <td>" . $titlebal . "</td>

                                    <td align='right'><b>" . Controller::money_format_inr($balance, 2) . "</b></td>


                                </tr>";
            } else {

                $finaldata .= "<tr>

                                    <td colspan='5'>No results found.</td>

                                </tr>";
            }

            $petty_entries = $finaldata;

            $users = Users::model()->findByPk($employee_id);
            $name = $users->first_name . ' ' . $users->last_name;



            $mPDF1 = Yii::app()->ePdf->mPDF();

            $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');

            $mPDF1->WriteHTML($this->renderPartial('pettycashdetails_pdf', array(
                'petty_entries' => $petty_entries,
                'name' => $name,
                'company_id' => $company_id
            ), true));

            $mPDF1->Output('Pettycashreport_' . $name . '.pdf', 'D');
        } else {
            $tblpx = Yii::app()->db->tablePrefix;
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery1 = "";
            $newQuery2 = "";
            $newQuery3 = "";
            foreach ($arrVal as $arr) {
                if ($newQuery1)
                    $newQuery1 .= ' OR';
                if ($newQuery2)
                    $newQuery2 .= ' OR';
                if ($newQuery3)
                    $newQuery3 .= ' OR';
                $newQuery1 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "dailyexpense.company_id)";
                $newQuery2 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "expenses.company_id)";
                $newQuery3 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "subcontractor_payment.company_id)";
            }

            $dailyexpense = array();
            $employee_id = '';
            $date_from = '';
            $date_to = '';
            $company_id = '';
            $daybook = array();
            $dailyexpense_refund = array();
            $dailyexpense_exp = array();
            $scpayment = array();
            // if (isset($_REQUEST['employee_id']) && !empty($_REQUEST['employee_id'])) {
            $where = '';
            $where1 = '';
            $where2 = '';
            $employee_id = '';
            $date_from = '';
            $date_to = '';
            $company_id = '';
            $chquery1 = "";
            $chquery2 = "";
            $chcondition1 = "";
            $chcondition2 = "";
            if (isset($_REQUEST['employee_id']) && !empty($_REQUEST['employee_id'])) {
                $employee_id = $_REQUEST['employee_id'];
                $where .= " AND {$tblpx}dailyexpense.employee_id = " . $_REQUEST['employee_id'] . "";
                $where1 .= " AND {$tblpx}expenses.employee_id = " . $_REQUEST['employee_id'] . "";
                $where2 .= " AND {$tblpx}subcontractor_payment.employee_id = " . $_REQUEST['employee_id'] . "";
            }
            if (isset($_REQUEST['company_id']) && !empty($_REQUEST['company_id'])) {
                $company_id = $_REQUEST['company_id'];
                $where .= " AND {$tblpx}dailyexpense.company_id = " . $_REQUEST['company_id'] . "";
                $where1 .= " AND {$tblpx}expenses.company_id = " . $_REQUEST['company_id'] . "";
                $where2 .= " AND {$tblpx}subcontractor_payment.company_id = " . $_REQUEST['company_id'] . "";
            } else {
                $where .= " AND (" . $newQuery1 . ")";
                $where1 .= " AND (" . $newQuery2 . ")";
                $where2 .= " AND (" . $newQuery3 . ")";
            }

            if (!empty($_REQUEST['date_from']) && !empty($_REQUEST['date_to'])) {
                $date_from = date('Y-m-d', strtotime($_REQUEST['date_from']));
                $date_to = date('Y-m-d', strtotime($_REQUEST['date_to']));
                $where .= " AND {$tblpx}dailyexpense.date between '" . $date_from . "' and '" . $date_to . "'";
                $where1 .= " AND {$tblpx}expenses.date between '" . $date_from . "' and '" . $date_to . "'";
                $where2 .= " AND {$tblpx}subcontractor_payment.date between '" . $date_from . "' and '" . $date_to . "'";
            } else {
                if (!empty($_REQUEST['date_from'])) {
                    $date_from = date('Y-m-d', strtotime($_REQUEST['date_from']));
                    $where .= " AND {$tblpx}dailyexpense.date >= '" . $date_from . "'";
                    $where1 .= " AND {$tblpx}expenses.date >= '" . $date_from . "'";
                    $where2 .= " AND {$tblpx}subcontractor_payment.date >= '" . $date_from . "'";
                }
                if (!empty($_REQUEST['date_to'])) {
                    $date_to = date('Y-m-d', strtotime($_REQUEST['date_to']));
                    $where .= " AND {$tblpx}dailyexpense.date <= '" . $date_to . "'";
                    $where1 .= " AND {$tblpx}expenses.date <= '" . $date_to . "'";
                    $where2 .= " AND {$tblpx}subcontractor_payment.date <= '" . $date_to . "'";
                }
            }


            //petty cash advance
            $reconcilsql = "SELECT *  FROM `jp_reconciliation` "
                . " WHERE `reconciliation_status`='0' "
                . " and reconciliation_payment='Dailyexpense Payment' and "
                . "reconciliation_table='jp_dailyexpense'";
            $reconcil_dexpense_payment_data = Yii::app()->db->createCommand($reconcilsql)->queryAll();

            $payment_cheque_array = array();
            foreach ($reconcil_dexpense_payment_data as $data) {

                $value = "'{$data['reconciliation_chequeno']}'";
                if ($value != '') {
                    $recon_data = $value;
                    array_push($payment_cheque_array, $recon_data);
                }
            }
            $payment_cheque_array1 = implode(',', $payment_cheque_array);
            if (($payment_cheque_array1)) {
                $chquery1 = "(dailyexpense_chequeno NOT IN ( $payment_cheque_array1) OR dailyexpense_chequeno IS NULL)";
                $chcondition1 = " AND " . $chquery1 . "";
            }

            $petty_issued_id = Yii::app()->db->createCommand("SELECT `company_exp_id` 
            FROM `jp_company_expense_type` WHERE  UPPER(`name`)='PETTY CASH ISSUED'")->queryScalar();
            $dailyexpense = array();
            if ($petty_issued_id) {
                $dailyexpensesql = "SELECT * FROM " . $tblpx . "dailyexpense "
                    . " LEFT JOIN " . $tblpx . "users "
                    . " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid"
                    . " WHERE " . $tblpx . "dailyexpense.exp_type =73 "
                    . " AND " . $tblpx . "dailyexpense.expense_type  IN('89','88') " . $chcondition1 . "  "
                    . " AND jp_dailyexpense.exp_type_id= $petty_issued_id " . $where . ""
                    . " order by " . $tblpx . "dailyexpense.date asc";

                $dailyexpense = Yii::app()->db->createCommand($dailyexpensesql)->queryAll();
            }
            //petty cash refund
            $reconcil_receipt_sql = "SELECT *  FROM `jp_reconciliation` "
                . " WHERE `reconciliation_status`='0' "
                . " and reconciliation_payment='Dailyexpense Receipt' and "
                . "reconciliation_table='jp_dailyexpense'";
            $reconcil_dexpense_receipt_data = Yii::app()->db->createCommand($reconcil_receipt_sql)->queryAll();
            $receipt_cheque_array = array();
            foreach ($reconcil_dexpense_receipt_data as $receiptdata) {
                $receiptvalue = "'{$receiptdata['reconciliation_chequeno']}'";
                if ($receiptvalue != '') {
                    $recon_receipt_data = $receiptvalue;
                    array_push($receipt_cheque_array, $recon_receipt_data);
                }
            }
            $receipt_cheque_array1 = implode(',', $receipt_cheque_array);
            if (($receipt_cheque_array1)) {
                $chquery2 = "(dailyexpense_chequeno NOT IN ( $receipt_cheque_array1) OR dailyexpense_chequeno IS NULL)";
                $chcondition2 = " AND " . $chquery2 . "";
            }

            $petty_refund_id = Yii::app()->db->createCommand("SELECT `company_exp_id` 
            FROM `jp_company_expense_type` WHERE  UPPER(`name`)='PETTY CASH REFUND'")->queryScalar();
            $dailyexpense_refund = array();
            if ($petty_refund_id) {
                $dexp_refund_sql = "SELECT * FROM " . $tblpx . "dailyexpense "
                    . " LEFT JOIN " . $tblpx . "users "
                    . " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid "
                    . " WHERE " . $tblpx . "dailyexpense.exp_type =72 "
                    . " AND " . $tblpx . "dailyexpense.dailyexpense_receipt_type IN('88','89') " . $chcondition2 . "  "
                    . "AND jp_dailyexpense.exp_type_id= $petty_refund_id " . $where . " "
                    . " order by " . $tblpx . "dailyexpense.date asc";

                $dailyexpense_refund = Yii::app()->db->createCommand($dexp_refund_sql)->queryAll();
            }

            //petty cash expense
            $daybook_sql = "SELECT * ," . $tblpx . "expenses.description as dbookdesc "
                . " FROM " . $tblpx . "expenses LEFT JOIN " . $tblpx . "users "
                . " ON " . $tblpx . "expenses.employee_id= " . $tblpx . "users.userid "
                . " LEFT JOIN " . $tblpx . "projects "
                . " ON " . $tblpx . "projects.pid = " . $tblpx . "expenses.projectid "
                . " WHERE " . $tblpx . "expenses.type = 73  "
                . " AND " . $tblpx . "expenses.expense_type=103 " . $where1 . " "
                . " GROUP BY " . $tblpx . "expenses.projectid";
            $daybook = Yii::app()->db->createCommand($daybook_sql)->queryAll();
            $dailyexpense_exp_sql = "SELECT * FROM " . $tblpx . "dailyexpense "
                . " LEFT JOIN " . $tblpx . "users "
                . " ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid "
                . " WHERE (" . $tblpx . "dailyexpense.dailyexpense_receipt_type = 103 "
                . " OR  " . $tblpx . "dailyexpense.expense_type = 103  )" . $where . " "
                . " order by " . $tblpx . "dailyexpense.date asc";
            $dailyexpense_exp = Yii::app()->db->createCommand($dailyexpense_exp_sql)->queryAll();
            $scp_sql = "SELECT * ," . $tblpx . "subcontractor_payment.description as description "
                . " FROM " . $tblpx . "subcontractor_payment LEFT JOIN " . $tblpx . "users "
                . " ON " . $tblpx . "subcontractor_payment.employee_id= " . $tblpx . "users.userid "
                . " LEFT JOIN " . $tblpx . "projects "
                . " ON " . $tblpx . "projects.pid = " . $tblpx . "subcontractor_payment.project_id "
                . " WHERE " . $tblpx . "subcontractor_payment.payment_type=103 " . $where2 . " ";
            //. " GROUP BY " . $tblpx . "subcontractor_payment.project_id";                
            $scpayment = Yii::app()->db->createCommand($scp_sql)->queryAll();
            $viewFile = "_petty_ledger_view";
            //$viewFile="pettycashreport";
            $html = $this->renderPartial($viewFile, [
                'employee_id' => $employee_id,
                'dailyexpense' => $dailyexpense,
                'date_from' => $date_from,
                'date_to' => $date_to,
                'daybook' => $daybook,
                'dailyexpense_refund' => $dailyexpense_refund,
                'company_id' => $company_id,
                'dailyexpense_exp' => $dailyexpense_exp,
                'scpayment' => $scpayment,
                'where' => $where,
                'where1' => $where1,
                'where2' => $where2,
                'petty_issued_id' => $petty_issued_id,
                'petty_refund_id' => $petty_refund_id
                // Pass any other necessary data here
            ], true);
            $mPDF1 = Yii::app()->ePdf->mPDF();
            $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
            $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
            $selectedtemplate = Yii::app()->db->createCommand($sql)->queryScalar();

            $template = $this->getTemplate($selectedtemplate);
            $mPDF1->SetHTMLHeader($template['header']);
            $mPDF1->SetHTMLFooter($template['footer']);
            $mPDF1->AddPage('', '', '', '', '', 0, 0, 50, 40, 10, 0);
            $mPDF1->WriteHTML($html);
            //if ($_GET[//"ledger"] == 0) {
            $mPDF1->Output('PettyCash Report.pdf', 'D');
            // }

            // Set the content to the view
            //$mpdf->WriteHTML($html);

            // Output the PDF
            //$mpdf->Output('petty_cash_report.pdf', 'D'); 

        }
    }

    public function actionpettycashexceldetails($employee_id, $date_from, $date_to, $company_id)
    {
        $where = '';
        $where1 = '';
        $finaldata = "";
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery1 = "";
        $newQuery2 = "";
        foreach ($arrVal as $arr) {
            if ($newQuery1)
                $newQuery1 .= ' OR';
            if ($newQuery2)
                $newQuery2 .= ' OR';
            $newQuery1 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "dailyexpense.company_id)";
            $newQuery2 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "expenses.company_id)";
        }
        if ($employee_id != '') {
            $where .= " AND {$tblpx}dailyexpense.employee_id = " . $employee_id . "";
            $where1 .= " AND {$tblpx}expenses.employee_id = " . $employee_id . "";
        }

        if (isset($company_id) && !empty($company_id)) {
            $where .= " AND {$tblpx}dailyexpense.company_id = " . $company_id . "";
            $where1 .= " AND {$tblpx}expenses.company_id = " . $company_id . "";
        } else {
            $where .= " AND (" . $newQuery1 . ")";
            $where1 .= " AND (" . $newQuery2 . ")";
        }

        if (($date_from != '') && ($date_to != '')) {
            $date_from = date('Y-m-d', strtotime($date_from));
            $date_to = date('Y-m-d', strtotime($date_to));
            $where .= " AND {$tblpx}dailyexpense.date between '" . $date_from . "' and '" . $date_to . "'";
            $where1 .= " AND {$tblpx}expenses.date between '" . $date_from . "' and '" . $date_to . "'";
        } else {
            if ($date_from != '') {
                $date_from = date('Y-m-d', strtotime($date_from));
                $where .= " AND {$tblpx}dailyexpense.date >= '" . $date_from . "'";
                $where1 .= " AND {$tblpx}expenses.date >= '" . $date_from . "'";
            }
            if ($date_to != '') {
                $date_to = date('Y-m-d', strtotime($date_to));
                $where .= " AND {$tblpx}dailyexpense.date <= '" . $date_to . "'";
                $where1 .= " AND {$tblpx}expenses.date <= '" . $date_to . "'";
            }
        }
        $dailyexpense = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid WHERE " . $tblpx . "dailyexpense.exp_type =73 AND " . $tblpx . "dailyexpense.expense_type=103 " . $where . "")->queryAll();
        $daybook = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "expenses LEFT JOIN " . $tblpx . "users ON " . $tblpx . "expenses.employee_id= " . $tblpx . "users.userid LEFT JOIN " . $tblpx . "projects ON " . $tblpx . "projects.pid = " . $tblpx . "expenses.projectid WHERE " . $tblpx . "expenses.type = 73  AND " . $tblpx . "expenses.expense_type=103 " . $where1 . " GROUP BY " . $tblpx . "expenses.projectid")->queryAll();
        $dailyexpense_refunds = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid WHERE " . $tblpx . "dailyexpense.exp_type =72 AND " . $tblpx . "dailyexpense.dailyexpense_receipt_type=103 " . $where . "")->queryAll();
        if ((!empty($dailyexpense) || !empty($daybook) || !empty($dailyexpense_refunds)) && ($employee_id != 0)) {
            $users = Users::model()->findByPk($employee_id);
            $arraylabel = array('Petty Cash report of # ' . $users->first_name . ' ' . $users->last_name, '', '', '', '', '', '', '', '', '', '', '');
            $finaldata = array();
            $i = 0;
            $x = 0;
            $y = 0;
            $k = 0;
            $p = 1;
            $j = 0;
            $n = 0;

            $company_name = '';
            if (!empty($company_id)) {
                $company = Company::model()->findByPk($company_id);
                $company_name = "   Company: " . $company->name;
            }

            $finaldata[0][] = 'Employee Name: ' . $users->first_name . ' ' . $users->last_name . ' ' . $company_name;
            $finaldata[0][] = 'Amount';

            $l = 1;

            $dailyexpense_amount = 0;
            if (!empty($dailyexpense)) {
                foreach ($dailyexpense as $value) {
                    $dailyexpense_amount += $value['dailyexpense_paidamount'];
                    $finaldata[$l][] = date('d-M-Y', strtotime($value['date'])) . " " . $value['description'];
                    $finaldata[$l][] = $value['dailyexpense_paidamount'] ? Controller::money_format_inr($value['dailyexpense_paidamount'], 2) : 0;
                    $l++;
                    $i = $l;
                }
            } else {
                $i = 0;
            }

            $finaldata[$i][] = 'Net Expense';
            $finaldata[$i][] = Controller::money_format_inr($dailyexpense_amount, 2);
            $finaldata[$i + 1][] = 'Expenses';
            $x = $i + 2;

            $daybook_amount = 0;
            if (!empty($daybook)) {
                foreach ($daybook as $value) {
                    $finaldata[$x][] = "Project Name  :" . $value['name'];
                    $where1 = '';
                    if (isset($employee_id)) {
                        $where1 .= " AND {$tblpx}expenses.employee_id = " . $employee_id . "";
                    }

                    if (isset($company_id) && !empty($company_id)) {
                        $where1 .= " AND {$tblpx}expenses.company_id = " . $company_id . "";
                    } else {
                        $where1 .= " AND (" . $newQuery2 . ")";
                    }

                    if (!empty($date_from) && !empty($date_to)) {
                        $date_from = date('Y-m-d', strtotime($date_from));
                        $date_to = date('Y-m-d', strtotime($date_to));
                        $where1 .= " AND {$tblpx}expenses.date between '" . $date_from . "' and '" . $date_to . "'";
                    } else {
                        if (!empty($date_from)) {
                            $date_from = date('Y-m-d', strtotime($date_from));
                            $where1 .= " AND {$tblpx}expenses.date >= '" . $date_from . "'";
                        }
                        if (!empty($date_to)) {
                            $date_to = date('Y-m-d', strtotime($date_to));
                            $where1 .= " AND {$tblpx}expenses.date <= '" . $date_to . "'";
                        }
                    }

                    $daybook_details = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "expenses LEFT JOIN " . $tblpx . "users ON " . $tblpx . "expenses.employee_id= " . $tblpx . "users.userid WHERE " . $tblpx . "expenses.type = 73  AND " . $tblpx . "expenses.expense_type=103 " . $where1 . " AND " . $tblpx . "expenses.projectid = " . $value['pid'] . "")->queryAll();

                    $i = 0;
                    $item_amount = 0;
                    $y = $x + 1;
                    foreach ($daybook_details as $newsql) {

                        $i++;
                        if ($i % 2 == 0) {
                            $class = 'even';
                        } else {
                            $class = 'odd';
                        }
                        $item_amount += $newsql['paid'];

                        $finaldata[$y][] = date('d-M-Y', strtotime($newsql['date'])) . " " . $newsql['description'];
                        $finaldata[$y][] = $newsql['paid'] ? Controller::money_format_inr($newsql['paid'], 2) : 0;
                        $y++;
                        $x = $y;
                        $k = $y;
                    }

                    $daybook_amount += $item_amount;
                    //                 $finaldata[$y+1][]='Net Expense';
                    //                 $finaldata[$y+1][]=Controller::money_format_inr($item_amount,2);
                }

                $finaldata[$k][] = 'Net Expense';
                $finaldata[$k][] = Controller::money_format_inr($daybook_amount, 2);
                $p = $k + 1;
            } else {

                $p = $i + 2;
            }


            $dailyexpense_refund = 0;
            if (!empty($dailyexpense_refunds)) {
                $finaldata[$p][] = 'Petty Cash Refund';
                $j = $p + 1;
                foreach ($dailyexpense_refunds as $value) {
                    $dailyexpense_refund += $value['dailyexpense_receipt'];
                    $finaldata[$j][] = date('d-M-Y', strtotime($value['date'])) . " " . $value['description'];
                    $finaldata[$j][] = $value['dailyexpense_receipt'] ? Controller::money_format_inr($value['dailyexpense_receipt'], 2) : 0;
                    $j++;
                    $i = $j;
                }
                $finaldata[$j][] = 'Net Expense';
                $finaldata[$j][] = Controller::money_format_inr($dailyexpense_refund, 2);
                $n = $j + 1;
            } else {
                $n = $p;
            }






            $finaldata[$n][] = 'Petty Cash Advance';
            $finaldata[$n][] = Controller::money_format_inr($dailyexpense_amount, 2);
            $finaldata[$n + 1][] = 'Petty Cash expense';
            $finaldata[$n + 1][] = Controller::money_format_inr($daybook_amount, 2);
            $finaldata[$n + 2][] = 'Petty Cash refund';
            $finaldata[$n + 2][] = Controller::money_format_inr($dailyexpense_refund, 2);
            $balance = $dailyexpense_amount - ($daybook_amount + $dailyexpense_refund);
            $finaldata[$n + 3][] = 'Cash In Hand';
            $finaldata[$n + 3][] = Controller::money_format_inr($balance, 1);
        }

        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Petty Cash Report' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actionsavetopdfpetty()
    {

        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $sql = 'SELECT template_name '
            . 'FROM jp_quotation_template '
            . 'WHERE status="1"';
        $selectedtemplate = Yii::app()->db->createCommand($sql)->queryScalar();

        $this->logo = $this->realpath_logo;
        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $mPDF1->AddPage('', '', '', '', '', 0, 0, 52, 30, 10, 0);
        $mPDF1->WriteHTML($this->renderPartial('pettycashpdf', array(
            'vendor' => $vendor,
            'vendor_debit_data_array' => $vendor_debit_data_array,
        ), true));
        $filename = "Pettycash report";
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    public function actionsavetoexcelpetty()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        $newQuery1 = "";
        $newQuery2 = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            if ($newQuery1)
                $newQuery1 .= ' OR';
            if ($newQuery2)
                $newQuery2 .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
            $newQuery1 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "dailyexpense.company_id)";
            $newQuery2 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "expenses.company_id)";
        }
        $arraylabel = array('sl no', 'Employee name', 'Company', 'Petty Cash Advance', 'Petty Cash Expense', 'Petty Cash Refund', 'Cash In Hand');
        $finaldata = array();
        $employee = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "users WHERE  (" . $newQuery . ") AND user_type NOT IN (1) ORDER BY userid DESC")->queryAll();
        /*********************** */
        $advance = 0;
        $expense = 0;
        $balance = 0;
        $refund = 0;
        foreach ($employee as $key => $value) {

            $dailyexpense = Yii::app()->db->createCommand("SELECT SUM(" . $tblpx . "dailyexpense.dailyexpense_paidamount) as amount FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid WHERE " . $tblpx . "dailyexpense.exp_type =73 AND " . $tblpx . "dailyexpense.expense_type=103 AND " . $tblpx . "dailyexpense.employee_id = " . $value['userid'] . " AND (" . $newQuery1 . ")")->queryRow();
            $dailyexpense_refund = Yii::app()->db->createCommand("SELECT SUM(" . $tblpx . "dailyexpense.dailyexpense_receipt) as amount FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid WHERE " . $tblpx . "dailyexpense.exp_type =72 AND " . $tblpx . "dailyexpense.dailyexpense_receipt_type=103 AND " . $tblpx . "dailyexpense.employee_id = " . $value['userid'] . " AND (" . $newQuery1 . ")")->queryRow();
            $daybook = Yii::app()->db->createCommand("SELECT SUM(" . $tblpx . "expenses.paid) as amount FROM " . $tblpx . "expenses LEFT JOIN " . $tblpx . "users ON " . $tblpx . "expenses.employee_id= " . $tblpx . "users.userid WHERE " . $tblpx . "expenses.type = 73  AND " . $tblpx . "expenses.expense_type=103 AND " . $tblpx . "expenses.employee_id = " . $value['userid'] . "  AND (" . $newQuery2 . ")")->queryRow();
            $advance += $dailyexpense['amount'];
            $expense += $daybook['amount'];
            $balance += $advance - $expense;
            $refund += $dailyexpense_refund['amount'];
        }

        $finaldata[0][] = '';
        $finaldata[0][] = '';
        $finaldata[0][] = 'Total';
        $finaldata[0][] = Controller::money_format_inr($advance, 2);
        $finaldata[0][] = Controller::money_format_inr($expense, 2);
        $finaldata[0][] = Controller::money_format_inr($refund, 2);
        $finaldata[0][] = Controller::money_format_inr($advance - ($expense + $refund), 2);

        /*********************** */

        $advance = 0;
        $expense = 0;
        $balance = 0;
        $refund = 0;
        $j = 0;

        foreach ($employee as $key => $value) {
            $companyname = array();
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $comVal = explode(',', $user->company_id);
            $arrVal = explode(',', $value['company_id']);
            foreach ($arrVal as $arr) {
                if (in_array($arr, $comVal)) {
                    $values = Company::model()->findByPk($arr);
                    array_push($companyname, $values->name);
                }
            }
            $j++;
            $dailyexpense = Yii::app()->db->createCommand("SELECT SUM(" . $tblpx . "dailyexpense.dailyexpense_paidamount) as amount FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid WHERE " . $tblpx . "dailyexpense.exp_type =73 AND " . $tblpx . "dailyexpense.expense_type=103 AND " . $tblpx . "dailyexpense.employee_id = " . $value['userid'] . " AND (" . $newQuery1 . ")")->queryRow();
            $dailyexpense_refund = Yii::app()->db->createCommand("SELECT SUM(" . $tblpx . "dailyexpense.dailyexpense_receipt) as amount FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid WHERE " . $tblpx . "dailyexpense.exp_type =72 AND " . $tblpx . "dailyexpense.dailyexpense_receipt_type=103 AND " . $tblpx . "dailyexpense.employee_id = " . $value['userid'] . " AND (" . $newQuery1 . ")")->queryRow();
            $daybook = Yii::app()->db->createCommand("SELECT SUM(" . $tblpx . "expenses.paid) as amount FROM " . $tblpx . "expenses LEFT JOIN " . $tblpx . "users ON " . $tblpx . "expenses.employee_id= " . $tblpx . "users.userid WHERE " . $tblpx . "expenses.type = 73  AND " . $tblpx . "expenses.expense_type=103 AND " . $tblpx . "expenses.employee_id = " . $value['userid'] . "  AND (" . $newQuery2 . ")")->queryRow();
            $advance += $dailyexpense['amount'];
            $expense += $daybook['amount'];
            $balance += $advance - $expense;
            $refund += $dailyexpense_refund['amount'];
            $finaldata[$key + 1][] = $j;
            $finaldata[$key + 1][] = $value['first_name'] . ' ' . $value['last_name'];
            $finaldata[$key + 1][] = implode(', ', $companyname);
            $finaldata[$key + 1][] = Controller::money_format_inr($dailyexpense['amount'], 2);
            $finaldata[$key + 1][] = Controller::money_format_inr($daybook['amount'], 2);
            $finaldata[$key + 1][] = Controller::money_format_inr($dailyexpense_refund['amount'], 2);
            $finaldata[$key + 1][] = Controller::money_format_inr($dailyexpense['amount'] - ($daybook['amount'] + $dailyexpense_refund['amount']), 2);
        }

        $finaldata[$key + 1][] = '';
        $finaldata[$key + 1][] = '';
        $finaldata[$key + 1][] = 'Total';
        $finaldata[$key + 1][] = Controller::money_format_inr($advance, 2);
        $finaldata[$key + 1][] = Controller::money_format_inr($expense, 2);
        $finaldata[$key + 1][] = Controller::money_format_inr($refund, 2);
        $finaldata[$key + 1][] = Controller::money_format_inr($advance - ($expense + $refund), 2);


        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Perrtycash' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actionsalaryreport()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery1 = "";
        foreach ($arrVal as $arr) {
            if ($newQuery1)
                $newQuery1 .= ' OR';
            $newQuery1 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "dailyexpense.company_id)";
        }
        if (isset($_POST['employee_id']) && !empty($_POST['employee_id'])) {
            $where = '';
            $where1 = '';
            $employee_id = '';
            $date_from = '';
            $date_to = '';
            $company_id = '';
            if (isset($_POST['employee_id'])) {
                $employee_id = $_POST['employee_id'];
                $where .= " AND {$tblpx}dailyexpense.employee_id = " . $_POST['employee_id'] . "";
            }

            if (isset($_POST['company_id']) && !empty($_POST['company_id'])) {
                $company_id = $_POST['company_id'];
                $where .= " AND {$tblpx}dailyexpense.company_id = " . $_POST['company_id'] . "";
            } else {
                $where .= " AND (" . $newQuery1 . ")";
            }

            if (!empty($_POST['date_from']) && !empty($_POST['date_to'])) {
                $date_from = date('Y-m-d', strtotime($_POST['date_from']));
                $date_to = date('Y-m-d', strtotime($_POST['date_to']));
                $where .= " AND {$tblpx}dailyexpense.date between '" . $date_from . "' and '" . $date_to . "'";
            } else {
                if (!empty($_POST['date_from'])) {
                    $date_from = date('Y-m-d', strtotime($_POST['date_from']));
                    $where .= " AND {$tblpx}dailyexpense.date >= '" . $date_from . "'";
                }
                if (!empty($_POST['date_to'])) {
                    $date_to = date('Y-m-d', strtotime($_POST['date_to']));
                    $where .= " AND {$tblpx}dailyexpense.date <= '" . $date_to . "'";
                }
            }
            $dailyexpense_advance = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid LEFT JOIN " . $tblpx . "company_expense_type ON " . $tblpx . "dailyexpense.exp_type_id = " . $tblpx . "company_expense_type.company_exp_id   WHERE " . $tblpx . "dailyexpense.exp_type =73 " . $where . " AND " . $tblpx . "company_expense_type.name='SALARY ADVANCE'")->queryAll();
            $dailyexpense_salary = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid LEFT JOIN " . $tblpx . "company_expense_type ON " . $tblpx . "dailyexpense.exp_type_id = " . $tblpx . "company_expense_type.company_exp_id   WHERE " . $tblpx . "dailyexpense.exp_type =73 " . $where . " AND " . $tblpx . "company_expense_type.name='SALARY'")->queryAll();
        } else {
            $dailyexpense_advance = array();
            $employee_id = '';
            $date_from = '';
            $date_to = '';
            $dailyexpense_salary = array();
            $company_id = '';
        }


        $this->render('salaryreport', array('employee_id' => $employee_id, 'dailyexpense_advance' => $dailyexpense_advance, 'date_from' => $date_from, 'date_to' => $date_to, 'dailyexpense_salary' => $dailyexpense_salary, 'company_id' => $company_id));
    }

    public function actionsavetopdfsalary()
    {
        $this->logo = $this->realpath_logo;
        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $sql = 'SELECT template_name '
            . 'FROM jp_quotation_template '
            . 'WHERE status="1"';
        $selectedtemplate = Yii::app()->db->createCommand($sql)->queryScalar();

        $template = $this->getTemplate($selectedtemplate);
        $mPDF1->SetHTMLHeader($template['header']);
        $mPDF1->SetHTMLFooter($template['footer']);
        $mPDF1->AddPage('', '', '', '', '', 0, 0, 52, 30, 10, 0);
        $mPDF1->WriteHTML($this->renderPartial('salarypdf', array(
            'vendor' => $vendor,
            'vendor_debit_data_array' => $vendor_debit_data_array,
        ), true));
        $filename = "Salary report";
        $mPDF1->Output($filename . '.pdf', 'D');
    }

    public function actionsavetoexcelsalary()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery1 = "";
        foreach ($arrVal as $arr) {
            if ($newQuery1)
                $newQuery1 .= ' OR';
            $newQuery1 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "dailyexpense.company_id)";
        }
        $arraylabel = array('sl no', 'Employee name', 'Company', 'Salary Advance', 'Salary Paid', 'Total');
        $finaldata = array();
        $employee = Yii::app()->db->createCommand("SELECT *," . $tblpx . "users.company_id as companyid FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users ON " . $tblpx . "dailyexpense.employee_id = " . $tblpx . "users.userid LEFT JOIN " . $tblpx . "company_expense_type ON " . $tblpx . "dailyexpense.exp_type_id = " . $tblpx . "company_expense_type.company_exp_id  WHERE (" . $newQuery1 . ") AND " . $tblpx . "dailyexpense.exp_type=73 AND (" . $tblpx . "company_expense_type.name='SALARY ADVANCE' OR " . $tblpx . "company_expense_type.name='SALARY') AND " . $tblpx . "dailyexpense.dailyexpense_paidamount > 0 AND " . $tblpx . "users.user_type NOT IN (1) GROUP BY " . $tblpx . "users.userid")->queryAll();
        /******************* */
        $t_advance = 0;
        $t_salary = 0;
        $t_total = 0;

        foreach ($employee as $key => $value) {

            $companyname = array();
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $comVal = explode(',', $user->company_id);
            $arrVal = explode(',', $value['companyid']);
            foreach ($arrVal as $arr) {
                if (in_array($arr, $comVal)) {
                    $values = Company::model()->findByPk($arr);
                    array_push($companyname, $values->name);
                }
            }


            $advance_salary = Yii::app()->db->createCommand("SELECT SUM(" . $tblpx . "dailyexpense.dailyexpense_paidamount) as amount FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid LEFT JOIN " . $tblpx . "company_expense_type ON " . $tblpx . "dailyexpense.exp_type_id = " . $tblpx . "company_expense_type.company_exp_id   WHERE " . $tblpx . "dailyexpense.exp_type =73 AND " . $tblpx . "dailyexpense.employee_id = " . $value['userid'] . " AND (" . $newQuery1 . ") AND " . $tblpx . "company_expense_type.name='SALARY ADVANCE'")->queryRow();
            $salary = Yii::app()->db->createCommand("SELECT SUM(" . $tblpx . "dailyexpense.dailyexpense_paidamount) as amount FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid LEFT JOIN " . $tblpx . "company_expense_type ON " . $tblpx . "dailyexpense.exp_type_id = " . $tblpx . "company_expense_type.company_exp_id   WHERE " . $tblpx . "dailyexpense.exp_type =73 AND " . $tblpx . "dailyexpense.employee_id = " . $value['userid'] . " AND (" . $newQuery1 . ") AND " . $tblpx . "company_expense_type.name='SALARY'")->queryRow();
            $t_advance += $advance_salary['amount'];
            $t_salary += $salary['amount'];
            $t_total += $t_advance + $t_salary;
        }

        $finaldata[0][] = '';
        $finaldata[0][] = '';
        $finaldata[0][] = 'Total';
        $finaldata[0][] = Controller::money_format_inr($t_advance, 2);
        $finaldata[0][] = Controller::money_format_inr($t_salary, 2);
        $finaldata[0][] = Controller::money_format_inr($t_advance + $t_salary, 2);

        /******************* */

        $t_advance = 0;
        $t_salary = 0;
        $t_total = 0;
        $j = 0;

        foreach ($employee as $key => $value) {

            $companyname = array();
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $comVal = explode(',', $user->company_id);
            $arrVal = explode(',', $value['companyid']);
            foreach ($arrVal as $arr) {
                if (in_array($arr, $comVal)) {
                    $values = Company::model()->findByPk($arr);
                    array_push($companyname, $values->name);
                }
            }

            $j++;
            $advance_salary = Yii::app()->db->createCommand("SELECT SUM(" . $tblpx . "dailyexpense.dailyexpense_paidamount) as amount FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid LEFT JOIN " . $tblpx . "company_expense_type ON " . $tblpx . "dailyexpense.exp_type_id = " . $tblpx . "company_expense_type.company_exp_id   WHERE " . $tblpx . "dailyexpense.exp_type =73 AND " . $tblpx . "dailyexpense.employee_id = " . $value['userid'] . " AND (" . $newQuery1 . ") AND " . $tblpx . "company_expense_type.name='SALARY ADVANCE'")->queryRow();
            $salary = Yii::app()->db->createCommand("SELECT SUM(" . $tblpx . "dailyexpense.dailyexpense_paidamount) as amount FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid LEFT JOIN " . $tblpx . "company_expense_type ON " . $tblpx . "dailyexpense.exp_type_id = " . $tblpx . "company_expense_type.company_exp_id   WHERE " . $tblpx . "dailyexpense.exp_type =73 AND " . $tblpx . "dailyexpense.employee_id = " . $value['userid'] . " AND (" . $newQuery1 . ") AND " . $tblpx . "company_expense_type.name='SALARY'")->queryRow();
            $t_advance += $advance_salary['amount'];
            $t_salary += $salary['amount'];
            $t_total += $t_advance + $t_salary;

            $finaldata[$key + 1][] = $j;
            $finaldata[$key + 1][] = $value['first_name'] . ' ' . $value['last_name'];
            $finaldata[$key + 1][] = implode(', ', $companyname);
            $finaldata[$key + 1][] = Controller::money_format_inr($advance_salary['amount'], 2);
            $finaldata[$key + 1][] = Controller::money_format_inr($salary['amount'], 2);
            $finaldata[$key + 1][] = Controller::money_format_inr($advance_salary['amount'] + $salary['amount'], 2);
        }

        // $finaldata[$key + 1][] = '';
        // $finaldata[$key + 1][] = '';
        // $finaldata[$key + 1][] = 'Total';
        // $finaldata[$key + 1][] = Controller::money_format_inr($t_advance, 2);
        // $finaldata[$key + 1][] = Controller::money_format_inr($t_salary, 2);
        // $finaldata[$key + 1][] = Controller::money_format_inr($t_advance + $t_salary, 2);


        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Salaryreport' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actionsalarypdfdetails($employee_id, $date_from, $date_to, $company_id)
    {
        $where = '';
        $where1 = '';
        $finaldata = "";
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery1 = "";
        foreach ($arrVal as $arr) {
            if ($newQuery1)
                $newQuery1 .= ' OR';
            $newQuery1 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "dailyexpense.company_id)";
        }
        if ($employee_id != '') {
            $where .= " AND {$tblpx}dailyexpense.employee_id = " . $employee_id . "";
        }


        if (isset($_POST['company_id']) && !empty($_POST['company_id'])) {
            $company_id = $_POST['company_id'];
            $where .= " AND {$tblpx}dailyexpense.company_id = " . $_POST['company_id'] . "";
        } else {
            $where .= " AND (" . $newQuery1 . ")";
        }

        if (($date_from != '') && ($date_to != '')) {
            $date_from = date('Y-m-d', strtotime($date_from));
            $date_to = date('Y-m-d', strtotime($date_to));
            $where .= " AND {$tblpx}dailyexpense.date between '" . $date_from . "' and '" . $date_to . "'";
        } else {
            if ($date_from != '') {
                $date_from = date('Y-m-d', strtotime($date_from));
                $where .= " AND {$tblpx}dailyexpense.date >= '" . $date_from . "'";
            }
            if ($date_to != '') {
                $date_to = date('Y-m-d', strtotime($date_to));
                $where .= " AND {$tblpx}dailyexpense.date <= '" . $date_to . "'";
            }
        }
        $dailyexpense_advance = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid LEFT JOIN " . $tblpx . "company_expense_type ON " . $tblpx . "dailyexpense.exp_type_id = " . $tblpx . "company_expense_type.company_exp_id   WHERE " . $tblpx . "dailyexpense.exp_type =73 " . $where . " AND " . $tblpx . "company_expense_type.name='SALARY ADVANCE'")->queryAll();
        $dailyexpense_salary = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid LEFT JOIN " . $tblpx . "company_expense_type ON " . $tblpx . "dailyexpense.exp_type_id = " . $tblpx . "company_expense_type.company_exp_id   WHERE " . $tblpx . "dailyexpense.exp_type =73 " . $where . " AND " . $tblpx . "company_expense_type.name='SALARY'")->queryAll();
        if ((!empty($dailyexpense_advance) || !empty($dailyexpense_salary)) && ($employee_id != 0)) {

            $advance_amount = 0;
            if (!empty($dailyexpense_advance)) {
                $finaldata .= "<tr>
                            <td colspan='2'><b>Salary Advance</b></td>
                           </tr>";
                foreach ($dailyexpense_advance as $value) {
                    $advance_amount += $value['dailyexpense_paidamount'];
                    $finaldata .= "<tr>
                                            <td>" . date('d-M-Y', strtotime($value['date'])) . "</td>
                                            <td align='right'>" . Controller::money_format_inr($value['dailyexpense_paidamount'], 2) . "</td>
                                        </tr>";
                }

                $finaldata .= " <tr>
                                <td><b>Net Amount</b></td>
                                 <td align='right'><b>" . Controller::money_format_inr($advance_amount, 2) . "</b></td>
                            </tr>";
            }


            $salary_amount = 0;
            if (!empty($dailyexpense_salary)) {
                $finaldata .= "<tr>
                            <td colspan='2'><b>Salary Paid</b></td>
                           </tr>";
                foreach ($dailyexpense_salary as $value) {
                    $salary_amount += $value['dailyexpense_paidamount'];
                    $finaldata .= "<tr>
                                            <td>" . date('d-M-Y', strtotime($value['date'])) . "</td>
                                            <td align='right'>" . Controller::money_format_inr($value['dailyexpense_paidamount'], 2) . "</td>
                                        </tr>";
                }

                $finaldata .= " <tr>
                                <td><b>Net Amount</b></td>
                                 <td align='right'><b>" . Controller::money_format_inr($salary_amount, 2) . "</b></td>
                            </tr>";
            }

            $finaldata .= "<tr>
                                        <td colspan='2' class='whiteborder'>&nbsp;</td>
                                       </tr>";

            $finaldata .= " <tr>
                                <td><b>Total</b></td>
                                 <td align='right'><b>" . Controller::money_format_inr($salary_amount + $advance_amount, 2) . "</b></td>
                            </tr>";
        } else {

            $finaldata .= "<tr>

                                <td colspan='5'>No results found.</td>

                            </tr>";
        }

        $petty_entries = $finaldata;

        $users = Users::model()->findByPk($employee_id);
        $name = $users->first_name . ' ' . $users->last_name;



        $mPDF1 = Yii::app()->ePdf->mPDF();

        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');

        $mPDF1->WriteHTML($this->renderPartial('salarycashdetails_pdf', array(
            'petty_entries' => $petty_entries,
            'name' => $name,
            'company_id' => $company_id
        ), true));

        $mPDF1->Output('Salaryreport_' . $name . '.pdf', 'D');
    }

    public function actionsalaryexceldetails($employee_id, $date_from, $date_to, $company_id)
    {
        $where = '';
        $where1 = '';
        $finaldata = "";
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery1 = "";
        foreach ($arrVal as $arr) {
            if ($newQuery1)
                $newQuery1 .= ' OR';
            $newQuery1 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "dailyexpense.company_id)";
        }
        if ($employee_id != '') {
            $where .= " AND {$tblpx}dailyexpense.employee_id = " . $employee_id . "";
        }

        if (isset($_POST['company_id']) && !empty($_POST['company_id'])) {
            $company_id = $_POST['company_id'];
            $where .= " AND {$tblpx}dailyexpense.company_id = " . $_POST['company_id'] . "";
        } else {
            $where .= " AND (" . $newQuery1 . ")";
        }

        if (($date_from != '') && ($date_to != '')) {
            $date_from = date('Y-m-d', strtotime($date_from));
            $date_to = date('Y-m-d', strtotime($date_to));
            $where .= " AND {$tblpx}dailyexpense.date between '" . $date_from . "' and '" . $date_to . "'";
        } else {
            if ($date_from != '') {
                $date_from = date('Y-m-d', strtotime($date_from));
                $where .= " AND {$tblpx}dailyexpense.date >= '" . $date_from . "'";
            }
            if ($date_to != '') {
                $date_to = date('Y-m-d', strtotime($date_to));
                $where .= " AND {$tblpx}dailyexpense.date <= '" . $date_to . "'";
            }
        }
        $dailyexpense_advance = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid LEFT JOIN " . $tblpx . "company_expense_type ON " . $tblpx . "dailyexpense.exp_type_id = " . $tblpx . "company_expense_type.company_exp_id   WHERE " . $tblpx . "dailyexpense.exp_type =73 " . $where . "  AND " . $tblpx . "company_expense_type.name='SALARY ADVANCE'")->queryAll();
        $dailyexpense_salary = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "dailyexpense LEFT JOIN " . $tblpx . "users ON " . $tblpx . "dailyexpense.employee_id= " . $tblpx . "users.userid LEFT JOIN " . $tblpx . "company_expense_type ON " . $tblpx . "dailyexpense.exp_type_id = " . $tblpx . "company_expense_type.company_exp_id   WHERE " . $tblpx . "dailyexpense.exp_type =73 " . $where . " AND " . $tblpx . "company_expense_type.name='SALARY'")->queryAll();
        if ((!empty($dailyexpense_advance) || !empty($dailyexpense_salary)) && ($employee_id != 0)) {
            $users = Users::model()->findByPk($employee_id);
            $arraylabel = array('Salary Report of # ' . $users->first_name . ' ' . $users->last_name, '', '', '', '', '', '', '', '', '', '', '');
            $finaldata = array();
            $i = 0;
            $x = 0;
            $y = 0;
            $k = 0;
            $p = 1;
            $companyname = '';
            if (!empty($company_id)) {
                $company = Company::model()->findByPk($company_id);
                $companyname = "  Company: " . $company->name;
            }

            $finaldata[0][] = 'Employee Name: ' . $users->first_name . ' ' . $users->last_name . ' ' . $companyname;
            $finaldata[0][] = 'Amount';

            $l = 1;

            $advance_amount = 0;
            if (!empty($dailyexpense_advance)) {
                $finaldata[$l][] = 'Salary Advance';
                $k = $l + 1;
                foreach ($dailyexpense_advance as $value) {
                    $advance_amount += $value['dailyexpense_paidamount'];
                    $finaldata[$k][] = date('d-M-Y', strtotime($value['date']));
                    $finaldata[$k][] = $value['dailyexpense_paidamount'] ? Controller::money_format_inr($value['dailyexpense_paidamount'], 2) : 0;
                    $k++;
                }

                $finaldata[$k][] = 'Net Amount';
                $finaldata[$k][] = Controller::money_format_inr($advance_amount, 2);
                $i = $k;
            } else {
                $i = 0;
            }


            $salary_amount = 0;
            if (!empty($dailyexpense_salary)) {

                $finaldata[$i + 1][] = 'Salary Paid';
                $y = $i + 2;
                foreach ($dailyexpense_salary as $value) {
                    $salary_amount += $value['dailyexpense_paidamount'];
                    $finaldata[$y][] = date('d-M-Y', strtotime($value['date']));
                    $finaldata[$y][] = $value['dailyexpense_paidamount'] ? Controller::money_format_inr($value['dailyexpense_paidamount'], 2) : 0;
                    $y++;
                    ;
                }
                $finaldata[$y][] = 'Net Amount';
                $finaldata[$y][] = Controller::money_format_inr($salary_amount, 2);
                $x = $y;
            } else {
                $x = $i;
            }


            $finaldata[$x + 1][] = 'Total';
            $finaldata[$x + 1][] = Controller::money_format_inr($salary_amount + $advance_amount, 2);
        }



        Yii::import('ext.ECSVExport');

        $csv = new ECSVExport($finaldata);

        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Petty Cash Report' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }
    public function actionAddCashtransfer()
    {
        $crDate = $_REQUEST["crDate"];
        $scompany = $_REQUEST["scompany"];
        $sptype = $_REQUEST["sptype"];
        $sbank = $_REQUEST["sbank"];
        $scheque = $_REQUEST["scheque"];
        $bcompany = $_REQUEST["bcompany"];
        $bptype = $_REQUEST["bptype"];
        $bbank = $_REQUEST["bbank"];
        $bcheque = $_REQUEST["bcheque"];
        $amount = $_REQUEST["amount"];
        $description = $_REQUEST["description"];
        $tblpx = Yii::app()->db->tablePrefix;
        try {
            $model = new Cashtransfer;
            $model->cashtransfer_date = date("Y-m-d", strtotime($crDate));
            $model->sender_company = $scompany;
            $model->sender_paymenttype = $sptype;
            if ($sptype == 88) {
                $model->sender_bank = $sbank;
                $model->sender_cheque = $scheque;
            }
            $model->beneficiary_company = $bcompany;
            $model->beneficiary_paymenttype = $bptype;
            if ($bptype == 88) {
                $model->beneficiary_bank = $bbank;
                $model->beneficiary_cheque = $bcheque;
            }
            $model->beneficiary_cheque = $bcheque;
            $model->cashtransfer_amount = $amount;
            $model->cashtransfer_description = $description;
            $model->created_by = Yii::app()->user->id;
            $model->created_date = date("Y-m-d H:i:s");
            $expensetype = Companyexpensetype::model()->findByAttributes(array('name' => 'Fund Transfer'));
            $transaction = Yii::app()->db->beginTransaction();
            if ($model->save()) {
                $lastId = $model->cashtransfer_id;
                $model1 = new Dailyexpense;
                $model2 = new Dailyexpense;
                $reconmodel1 = new Reconciliation;
                $reconmodel2 = new Reconciliation;
                $model1->date = date("Y-m-d", strtotime($crDate));
                $model1->expense_type = $_REQUEST["sptype"];
                $model1->dailyexpense_amount = $_REQUEST["amount"];
                $model1->amount = $_REQUEST["amount"];
                $model1->description = $_REQUEST["description"];
                $model1->dailyexpense_receipt_type = NULL;
                $model1->dailyexpense_receipt = NULL;
                $model1->dailyexpense_paidamount = $_REQUEST["amount"];
                if ($sptype == 88) {
                    $model1->bank_id = $_REQUEST["sbank"];
                    $model1->dailyexpense_chequeno = $_REQUEST["scheque"];
                    $model1->reconciliation_status = 0;
                    $model1->reconciliation_date = NULL;

                    $reconmodel1->reconciliation_table = $tblpx . "dailyexpense";
                    $reconmodel1->reconciliation_payment = "Dailyexpense Payment - Fund Transfer";
                    $reconmodel1->reconciliation_paymentdate = date("Y-m-d", strtotime($_REQUEST["crDate"]));
                    $reconmodel1->reconciliation_amount = $_REQUEST["amount"];
                    $reconmodel1->reconciliation_chequeno = $_REQUEST["scheque"];
                    $reconmodel1->reconciliation_bank = $_REQUEST["sbank"];
                    $reconmodel1->created_date = date("Y-m-d H:i:s");
                    $reconmodel1->reconciliation_status = 0;
                    $reconmodel1->company_id = $_REQUEST["scompany"];
                }
                $model1->user_id = Yii::app()->user->id;
                $model1->created_by = Yii::app()->user->id;
                $model1->created_date = date("Y-m-d", strtotime($crDate));
                $model1->exp_type = 73;
                $model1->dailyexpense_type = 1;
                $model1->company_id = $_REQUEST["scompany"];
                $model1->expensehead_type = 1;
                $model1->transfer_parentid = $lastId;
                $model1->dailyexpense_purchase_type = 2;
                $model1->exp_type_id = $expensetype['company_exp_id'];
                $model1->expensehead_id = $expensetype['company_exp_id'];
                $model1->dailyexpense_receipt_head = NULL;

                $model2->date = date("Y-m-d", strtotime($crDate));
                $model2->expense_type = NULL;
                $model2->dailyexpense_amount = $_REQUEST["amount"];
                $model2->amount = $_REQUEST["amount"];
                $model2->description = $_REQUEST["description"];
                $model2->dailyexpense_receipt_type = $_REQUEST["bptype"];
                $model2->dailyexpense_receipt = $_REQUEST["amount"];
                $model2->dailyexpense_paidamount = NULL;
                if ($bptype == 88) {
                    $model2->bank_id = $_REQUEST["bbank"];
                    $model2->dailyexpense_chequeno = $_REQUEST["bcheque"];
                    $model2->reconciliation_status = 0;
                    $model2->reconciliation_date = NULL;

                    $reconmodel2->reconciliation_table = $tblpx . "dailyexpense";
                    $reconmodel2->reconciliation_payment = "Dailyexpense Payment - Fund Transfer";
                    $reconmodel2->reconciliation_paymentdate = date("Y-m-d", strtotime($_REQUEST["crDate"]));
                    $reconmodel2->reconciliation_amount = $_REQUEST["amount"];
                    $reconmodel2->reconciliation_chequeno = $_REQUEST["bcheque"];
                    $reconmodel2->reconciliation_bank = $_REQUEST["bbank"];
                    $reconmodel2->created_date = date("Y-m-d H:i:s");
                    $reconmodel2->reconciliation_status = 0;
                    $reconmodel2->company_id = $_REQUEST["bcompany"];
                }
                $model2->user_id = Yii::app()->user->id;
                $model2->created_by = Yii::app()->user->id;
                $model2->created_date = date("Y-m-d", strtotime($crDate));
                $model2->exp_type = 72;
                $model2->dailyexpense_type = 3;
                $model2->company_id = $_REQUEST["bcompany"];
                $model2->expensehead_type = 2;
                $model2->transfer_parentid = $lastId;
                $model2->dailyexpense_purchase_type = NULL;
                $model2->exp_type_id = $expensetype['company_exp_id'];
                $model2->expensehead_id = NULL;
                $model2->dailyexpense_receipt_head = $expensetype['company_exp_id'];
                if ($model1->save()) {
                    $lastIdmodel1 = $model1->dailyexp_id;
                    $user_id = Yii::app()->user->id;
                    $expdate = date("Y-m-d", strtotime($crDate));

                    $invoice_amount = BalanceHelper::cashInvoice(
                        $model1->expense_type,
                        $model1->company_id,
                        $model1->user_id,
                        $model1->bank_id,
                        $model1->date
                    );
                    //die($lastIdmodel1);
                    $expense_amount = BalanceHelper::cashExpense(
                        $model1->expense_type,
                        $model1->company_id,
                        $model1->user_id,
                        $lastIdmodel1,
                        $model1->bank_id,
                        $model1->date
                    );
                    $available_amount = $invoice_amount - $expense_amount;
                    //die("hello".$available_amount);
                    if ($amount > $available_amount) {
                        $transaction->rollback();
                        echo 3;
                        exit;
                    }

                    $reconmodel1->reconciliation_parentid = $lastIdmodel1;
                    if ($sptype == 88) {
                        $reconmodel1->save();
                    }
                    if ($model2->save()) {
                        $lastIdmodel2 = $model2->dailyexp_id;
                        $reconmodel2->reconciliation_parentid = $lastIdmodel2;
                        if ($bptype == 88) {
                            $reconmodel2->save();
                        }
                        $user = Users::model()->findByPk(Yii::app()->user->id);
                        $arrVal = explode(',', $user->company_id);
                        $newQuery = "";
                        foreach ($arrVal as $arr) {
                            if ($newQuery)
                                $newQuery .= ' OR';
                            $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
                        }
                        $expDate = date('Y-m-d', strtotime($_REQUEST["crDate"]));
                        $expenseData = Yii::app()->db->createCommand("SELECT e.*, e.bill_id as billno, et.name as typename,v.name vname,ba.bank_name as bankname FROM " . $tblpx . "dailyexpense e
                                LEFT JOIN " . $tblpx . "bills b ON e.bill_id = b.bill_id
                                LEFT JOIN " . $tblpx . "company_expense_type et ON e.expensehead_id = et.company_exp_id
                                LEFT JOIN " . $tblpx . "company_expense_type v ON e.dailyexpense_receipt_head = v.company_exp_id
                                LEFT JOIN " . $tblpx . "bank ba ON e.bank_id = ba.bank_id
                                WHERE e.date = '" . $expDate . "' AND (" . $newQuery . ") AND e.transaction_parent IS NULL")->queryAll();
                        $transaction->commit();
                        $client = $this->renderPartial('newlist', array('newmodel' => $expenseData));
                        return $client;
                    }
                } else {
                    $transaction->rollback();
                    $error_message = $this->setErrorMessage($model1->getErrors());
                    echo 2;
                    $result_ = array('status' => 1, 'error_message' => $error_message);
                    echo json_encode($result_);
                    exit;
                }
            } else {
                $transaction->rollback();
                $error_message = $this->setErrorMessage($model->getErrors());
                echo 2;
                $result_ = array('status' => 1, 'error_message' => $error_message);
                echo json_encode($result_);
                exit;

            }
        } catch (Exception $error) {
            $transaction->rollback();
            $error_message = $this->setErrorMessage($error->getMessage());
            echo 2;
            $result_ = array('status' => 1, 'error_message' => $error_message);

            echo json_encode($result_);
            exit;
        }

    }
    public function actionUpdateCashtransfer()
    {
        $transferId = $_REQUEST["transferId"];
        $crDate = $_REQUEST["crDate"];
        $scompany = $_REQUEST["scompany"];
        $sptype = $_REQUEST["sptype"];
        $sbank = $_REQUEST["sbank"];
        $scheque = $_REQUEST["scheque"];
        $bcompany = $_REQUEST["bcompany"];
        $bptype = $_REQUEST["bptype"];
        $bbank = $_REQUEST["bbank"];
        $bcheque = $_REQUEST["bcheque"];
        $amount = $_REQUEST["amount"];
        $description = $_REQUEST["description"];
        $oldmodel = Cashtransfer::model()->findByPk($transferId);
        $model = new Cashtransfer;
        $tblpx = Yii::app()->db->tablePrefix;
        $model->cashtransfer_date = date("Y-m-d", strtotime($crDate));
        $model->sender_company = $scompany;
        $model->sender_paymenttype = $sptype;
        if ($sptype == 88) {
            $model->sender_bank = $sbank;
            $model->sender_cheque = $scheque;
        }
        $model->beneficiary_company = $bcompany;
        $model->beneficiary_paymenttype = $bptype;
        if ($bptype == 88) {
            $model->beneficiary_bank = $bbank;
            $model->beneficiary_cheque = $bcheque;
        }
        $model->beneficiary_cheque = $bcheque;
        $model->cashtransfer_amount = $amount;
        $model->cashtransfer_description = $description;
        $model->created_by = $oldmodel->created_by;
        $model->created_date = $oldmodel->created_date;
        $model->updated_by = Yii::app()->user->id;
        $model->updated_date = date("Y-m-d H:i:s");
        $expensetype = Companyexpensetype::model()->findByAttributes(array('name' => 'Fund Transfer'));
        if ($model->save()) {
            $lastId = $model->cashtransfer_id;

            $daily1 = Dailyexpense::model()->findAll(array('condition' => 'transfer_parentid="' . $transferId . '" AND exp_type = 73'));
            $daily1Id = $daily1[0]["dailyexp_id"];

            $daily2 = Dailyexpense::model()->findAll(array('condition' => 'transfer_parentid="' . $transferId . '" AND exp_type = 72'));

            $daily2Id = $daily2[0]["dailyexp_id"];

            $model1 = new Dailyexpense;
            $model2 = new Dailyexpense;
            $reconmodel1 = new Reconciliation;
            $reconmodel2 = new Reconciliation;

            $model1->date = date("Y-m-d", strtotime($crDate));
            $model1->expense_type = $_REQUEST["sptype"];
            $model1->dailyexpense_amount = $_REQUEST["amount"];
            $model1->amount = $_REQUEST["amount"];
            $model1->description = $_REQUEST["description"];
            $model1->dailyexpense_receipt_type = NULL;
            $model1->dailyexpense_receipt = NULL;
            $model1->dailyexpense_paidamount = $_REQUEST["amount"];
            if ($sptype == 88) {
                $model1->bank_id = $_REQUEST["sbank"];
                $model1->dailyexpense_chequeno = $_REQUEST["scheque"];
                $model1->reconciliation_status = 0;
                $model1->reconciliation_date = NULL;

                $reconmodel1->reconciliation_table = $tblpx . "dailyexpense";
                $reconmodel1->reconciliation_payment = "Dailyexpense Payment - Fund Transfer";
                $reconmodel1->reconciliation_paymentdate = date("Y-m-d", strtotime($_REQUEST["crDate"]));
                $reconmodel1->reconciliation_amount = $_REQUEST["amount"];
                $reconmodel1->reconciliation_chequeno = $_REQUEST["scheque"];
                $reconmodel1->reconciliation_bank = $_REQUEST["sbank"];
                $reconmodel1->created_date = date("Y-m-d H:i:s");
                $reconmodel1->reconciliation_status = 0;
                $reconmodel1->company_id = $_REQUEST["scompany"];
            }
            $model1->user_id = Yii::app()->user->id;
            $model1->created_by = $daily1[0]["created_by"];
            $model1->created_date = $daily1[0]["created_date"];
            $model1->updated_by = Yii::app()->user->id;
            $model1->updated_date = date("Y-m-d", strtotime($crDate));
            $model1->exp_type = 73;
            $model1->dailyexpense_type = 1;
            $model1->company_id = $_REQUEST["scompany"];
            $model1->expensehead_type = 1;
            $model1->transfer_parentid = $lastId;
            $model1->dailyexpense_purchase_type = 2;
            $model1->exp_type_id = $expensetype['company_exp_id'];
            $model1->expensehead_id = $expensetype['company_exp_id'];
            $model1->dailyexpense_receipt_head = NULL;

            $model2->date = date("Y-m-d", strtotime($crDate));
            $model2->expense_type = NULL;
            $model2->dailyexpense_amount = $_REQUEST["amount"];
            $model2->amount = $_REQUEST["amount"];
            $model2->description = $_REQUEST["description"];
            $model2->dailyexpense_receipt_type = $_REQUEST["bptype"];
            $model2->dailyexpense_receipt = $_REQUEST["amount"];
            $model2->dailyexpense_paidamount = NULL;
            if ($bptype == 88) {
                $model2->bank_id = $_REQUEST["bbank"];
                $model2->dailyexpense_chequeno = $_REQUEST["bcheque"];
                $model2->reconciliation_status = 0;
                $model2->reconciliation_date = NULL;

                $reconmodel2->reconciliation_table = $tblpx . "dailyexpense";
                $reconmodel2->reconciliation_payment = "Dailyexpense Payment - Fund Transfer";
                $reconmodel2->reconciliation_paymentdate = date("Y-m-d", strtotime($_REQUEST["crDate"]));
                $reconmodel2->reconciliation_amount = $_REQUEST["amount"];
                $reconmodel2->reconciliation_chequeno = $_REQUEST["bcheque"];
                $reconmodel2->reconciliation_bank = $_REQUEST["bbank"];
                $reconmodel2->created_date = date("Y-m-d H:i:s");
                $reconmodel2->reconciliation_status = 0;
                $reconmodel2->company_id = $_REQUEST["bcompany"];
            }
            $model2->user_id = Yii::app()->user->id;
            $model2->created_by = $daily1[0]["created_by"];
            $model2->created_date = $daily1[0]["created_date"];
            $model2->updated_by = Yii::app()->user->id;
            $model2->updated_date = date("Y-m-d", strtotime($crDate));
            $model2->exp_type = 72;
            $model2->dailyexpense_type = 3;
            $model2->company_id = $_REQUEST["bcompany"];
            $model2->expensehead_type = 2;
            $model2->transfer_parentid = $lastId;
            $model2->dailyexpense_purchase_type = NULL;
            $model2->exp_type_id = $expensetype['company_exp_id'];
            $model2->expensehead_id = NULL;
            $model2->dailyexpense_receipt_head = $expensetype['company_exp_id'];
            if ($model1->save()) {
                $lastIdmodel1 = $model1->dailyexp_id;
                $reconmodel1->reconciliation_parentid = $lastIdmodel1;
                if ($sptype == 88) {
                    $reconmodel1->save();
                }
                if ($model2->save()) {
                    $lastIdmodel2 = $model2->dailyexp_id;
                    $reconmodel2->reconciliation_parentid = $lastIdmodel2;
                    if ($bptype == 88) {
                        $reconmodel2->save();
                    }
                    Cashtransfer::model()->deleteAll(array("condition" => "cashtransfer_id=" . $transferId . ""));
                    Dailyexpense::model()->deleteAll(array("condition" => "dailyexp_id=" . $daily1Id . ""));
                    Dailyexpense::model()->deleteAll(array("condition" => "dailyexp_id=" . $daily2Id . ""));
                    Reconciliation::model()->deleteAll(array("condition" => "reconciliation_table='" . $tblpx . "dailyexpense' AND reconciliation_parentid = " . $daily1Id . ""));
                    Reconciliation::model()->deleteAll(array("condition" => "reconciliation_table='" . $tblpx . "dailyexpense' AND reconciliation_parentid = " . $daily2Id . ""));

                    $user = Users::model()->findByPk(Yii::app()->user->id);
                    $arrVal = explode(',', $user->company_id);
                    $newQuery = "";
                    foreach ($arrVal as $arr) {
                        if ($newQuery)
                            $newQuery .= ' OR';
                        $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
                    }
                    $expDate = date('Y-m-d', strtotime($_REQUEST["crDate"]));
                    $expenseData = Yii::app()->db->createCommand("SELECT e.*, e.bill_id as billno, et.name as typename,v.name vname,ba.bank_name as bankname FROM " . $tblpx . "dailyexpense e
                            LEFT JOIN " . $tblpx . "bills b ON e.bill_id = b.bill_id
                            LEFT JOIN " . $tblpx . "company_expense_type et ON e.expensehead_id = et.company_exp_id
                            LEFT JOIN " . $tblpx . "company_expense_type v ON e.dailyexpense_receipt_head = v.company_exp_id
                            LEFT JOIN " . $tblpx . "bank ba ON e.bank_id = ba.bank_id
                            WHERE e.date = '" . $expDate . "' AND (" . $newQuery . ") AND e.transaction_parent IS NULL")->queryAll();
                    $client = $this->renderPartial('newlist', array('newmodel' => $expenseData));
                    return $client;
                }
            }
        } else {
            echo 2;
        }
    }
    public function actionGetTransferDetails()
    {
        $transferId = $_REQUEST["transferId"];
        $tblpx = Yii::app()->db->tablePrefix;
        $transferData = Yii::app()->db->createCommand("SELECT * FROM " . $tblpx . "cashtransfer WHERE cashtransfer_id = " . $transferId)->queryRow();
        $result["id"] = $transferData["cashtransfer_id"];
        $result["date"] = $transferData["cashtransfer_date"];
        $result["scompany"] = $transferData["sender_company"];
        $result["sptype"] = $transferData["sender_paymenttype"];
        if ($result["sptype"] == 88) {
            $result["sbank"] = $transferData["sender_bank"];
            $result["scheque"] = $transferData["sender_cheque"];
        }
        $result["bcompany"] = $transferData["beneficiary_company"];
        $result["bptype"] = $transferData["beneficiary_paymenttype"];
        if ($result["bptype"] == 88) {
            $result["bbank"] = $transferData["beneficiary_bank"];
            $result["bcheque"] = $transferData["beneficiary_cheque"];
        }
        $result["amount"] = $transferData["cashtransfer_amount"];
        $result["desc"] = $transferData["cashtransfer_description"];

        echo json_encode($result);
    }
    public function actionGetBankByCompany()
    {
        $company_id = $_REQUEST["company_id"];
        $bank = Bank::model()->findAll(array('condition' => 'company_id="' . $company_id . '"'));
        $result = "<option value=''>-Select Bank-</option>";
        foreach ($bank as $bankdata) {
            $result .= "<option value='" . $bankdata["bank_id"] . "'>" . $bankdata["bank_name"] . "</option>";
        }
        echo $result;
    }
    public function actionDeleteconfirmation()
    {
        $expId = $_REQUEST["expId"];
        $date = $_REQUEST["expense_date"];
        $status = $_REQUEST["status"];
        $delId = $_REQUEST["delId"];
        $action = $_REQUEST["action"];
        $tblpx = Yii::app()->db->tablePrefix;
        $success = "";
        if ($status == 1) {
            $deletedata = new Deletepending;
            $model = Dailyexpense::model()->findByPk($expId);
            $deletedata->deletepending_table = $tblpx . "dailyexpense";
            $deletedata->deletepending_data = json_encode($model->attributes);
            $deletedata->deletepending_parentid = $expId;
            $deletedata->user_id = Yii::app()->user->id;
            $deletedata->requested_date = date("Y-m-d");
            if ($deletedata->save()) {
                $success = "Yes";
            } else {
                echo 1;
            }
        } else if ($status == 2) {
            $model = Deletepending::model()->findByPk($delId);
            if ($model->delete()) {
                $success = "Yes";
            } else {
                echo 1;
            }
        }

        if ($success == "Yes") {
            $expDate = date('Y-m-d', strtotime($_REQUEST['expense_date']));
            $user = Users::model()->findByPk(Yii::app()->user->id);
            $arrVal = explode(',', $user->company_id);
            $newQuery = "";
            foreach ($arrVal as $arr) {
                if ($newQuery)
                    $newQuery .= ' OR';
                $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
            }
            $expenseData = Yii::app()->db->createCommand("SELECT e.*, e.bill_id as billno, et.name as typename,v.name vname,ba.bank_name as bankname FROM " . $tblpx . "dailyexpense e
                                LEFT JOIN " . $tblpx . "bills b ON e.bill_id = b.bill_id
                                LEFT JOIN " . $tblpx . "company_expense_type et ON e.expensehead_id = et.company_exp_id
                                LEFT JOIN " . $tblpx . "company_expense_type v ON e.dailyexpense_receipt_head = v.company_exp_id
                                LEFT JOIN " . $tblpx . "bank ba ON e.bank_id = ba.bank_id
                                WHERE e.date = '" . $expDate . "' AND (" . $newQuery . ") AND e.transaction_parent IS NULL")->queryAll();
            $client = $this->renderPartial('newlist', array('newmodel' => $expenseData));
            return $client;
        }
    }
    public function actionDailyexpensebyuser()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $currUser = Yii::app()->user->id;
        $model = new Dailyexpense;
        $model->unsetAttributes();
        $model->created_by = $currUser;
        if (isset($_GET)) {
            $model->fromdate = isset($_REQUEST["fromdate"]) ? $_REQUEST["fromdate"] : "";
            $model->todate = isset($_REQUEST["todate"]) ? $_REQUEST["todate"] : "";
        }

        $render_data = array(
            'model' => $model,
            'dataProvider' => $model->usersearch(),
        );
        if (filter_input(INPUT_GET, 'export') == 'pdf') {
            $this->logo = $this->realpath_logo;
            $mPDF1 = Yii::app()->ePdf->mPDF();
            $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
            $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
            $selectedtemplate = Yii::app()->db->createCommand($sql)->queryScalar();

            $template = $this->getTemplate($selectedtemplate);
            $mPDF1->SetHTMLHeader($template['header']);
            $mPDF1->SetHTMLFooter($template['footer']);
            $mPDF1->AddPage('', '', '', '', '', 0, 0, 60, 30, 10, 0);
            $mPDF1->WriteHTML($this->renderPartial('dailyexpensebyuser', $render_data, true));
            $mPDF1->Output('DAILY EXPENSE BY USER.pdf', 'D');
        } elseif (filter_input(INPUT_GET, 'export') == 'csv') {
            $exported_data = $this->renderPartial('dailyexpensebyuser', $render_data, true);
            $export_filename = 'DAILY EXPENSE BY USER' . date('Ymd_His');
            ExportHelper::exportGridAsCSV($exported_data, $export_filename);
        } else {
            $this->render('dailyexpensebyuser', $render_data);
        }

    }
    public function actionDuplicateentry()
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $model = new Dailyexpense;
        $entry = Yii::app()->db->createCommand("SELECT  count(d.dailyexp_id) as rowcount, d.* FROM {$tblpx}dailyexpense d WHERE d.dailyexpense_paidamount IS NOT NULL
                    GROUP BY d.amount, d.dailyexpense_paidamount, d.date,d.description
                    HAVING COUNT(d.dailyexp_id) > 1")->queryAll();
        $this->render('duplicateentry', array(
            'model' => $model,
            'duplicateentry' => $entry,
        ));
    }
    public function actionGetduplicateentries()
    {
        $amount = $_REQUEST["amount"];
        $paid = $_REQUEST["paid"];
        $date = $_REQUEST["date"];
        $description = $_REQUEST["description"];
        $tblpx = Yii::app()->db->tablePrefix;
        $duplicate = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}dailyexpense WHERE amount = '{$amount}' AND dailyexpense_paidamount = '{$paid}' AND  date = '{$date}' AND description='{$description}' ")->queryAll();
        $result = '<button type="button" class="close"  aria-hidden="true">×</button><h4>Dailyexpense</h4><div class="table-responsive"><table class="table table-bordered table-hover">
                            <thead class="entry-table">
                                <tr>
                                    <th>Sl No.</th>
                                    <th>Company</th>
                                    <th>Bill Number</th>
                                    <th>Transaction Head</th>
                                    <th>Transaction Type</th>
                                    <th>Bank</th>
                                    <th>Cheque Number</th>
                                    <th>Description</th>
                                    <th>Amount</th>
                                    <th>Tax</th>
                                    <th>Total</th>
                                    <th>Receipt</th>
                                    <th>Paid</th>
                                    <th>Payment Type</th>
                                    <th>Paid To</th>
                                </tr>
                            </thead>
                            <tbody>';
        $i = 1;
        foreach ($duplicate as $entry) {
            $result .= '         <tr>
                                    <td>' . $i . '</td>
                                    <td>';
            $company = Company::model()->findByPk($entry["company_id"]);
            if ($company)
                $result .= '         ' . $company->name;
            $result .= '             </td>
                                    <td>' . $entry["bill_id"] . '</td>
                                    <td>';
            if ($entry["dailyexpense_type"] == 'expense') {
                $expense = ExpenseType::model()->findByPk($entry["expensehead_id"]);
                if ($expense)
                    $result .= "Expense - " . $expense->type_name;
            } else if ($entry["dailyexpense_type"] == 'receipt') {
                $receipt = Companyexpensetype::model()->findByPk($entry["dailyexpense_receipt_head"]);
                if ($receipt)
                    $result .= "Receipt - " . $receipt->name;
            } else if ($entry["dailyexpense_type"] == 'deposit') {
                $deposit = Companyexpensetype::model()->findByPk($entry["exp_type_id"]);
                if ($deposit)
                    $result .= "Deposit - " . $deposit->deposit_name;
            }
            $result .= '             </td>
                                    <td>';
            if ($entry["dailyexpense_type"] == 'receipt')
                $exptype = Status::model()->findByPk($entry["dailyexpense_receipt_type"]);
            else
                $exptype = Status::model()->findByPk($entry["expense_type"]);
            if ($exptype)
                $result .= '         ' . $exptype->caption;
            $result .= '             </td>
                                    <td>';
            $bank = Bank::model()->findByPk($entry["bank_id"]);
            if ($bank)
                $result .= '        ' . $bank->bank_name;
            $result .= '             </td>
                                    <td>' . $entry["dailyexpense_chequeno"] . '</td>
                                    <td>' . $entry["description"] . '</td>
                                    <td class="text-right">' . Controller::money_format_inr($entry["dailyexpense_amount"], 2) . '</td>
                                    <td class="text-right">' . Controller::money_format_inr($entry["dailyexpense_sgst"] + $entry["dailyexpense_cgst"] + $entry["dailyexpense_igst"], 2) . '</td>
                                    <td class="text-right">' . Controller::money_format_inr($entry["amount"], 2) . '</td>
                                    <td class="text-right">' . Controller::money_format_inr($entry["dailyexpense_receipt"], 2) . '</td>
                                    <td class="text-right">' . Controller::money_format_inr($entry["dailyexpense_paidamount"], 2) . '</td>
                                    <td>';
            if ($entry["dailyexpense_purchase_type"] == 1)
                $result .= "Credit";
            else if ($entry["dailyexpense_purchase_type"] == 2)
                $result .= "Full Paid";
            else if ($entry["dailyexpense_purchase_type"] == 3)
                $result .= "Partially Paid";
            $result .= '             </td>
                                    <td>';
            $employee = Users::model()->findByPk($entry["employee_id"]);
            if ($employee)
                $result .= '         ' . $employee->first_name . " " . $employee->last_name;
            $result .= '             </td>
                                </tr>';
            $i++;
        }
        $result .= '     </tbody>
                       </table>
                       </div>';
        echo $result;
    }
    public function actionGetAllData()
    {
        $expDate = date('Y-m-d', strtotime($_REQUEST['date']));
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', e.company_id)";
        }
        $expenseData = Yii::app()->db->createCommand("SELECT e.*, e.bill_id as billno, et.name as typename,v.name vname,ba.bank_name as bankname FROM " . $tblpx . "dailyexpense e
                            LEFT JOIN " . $tblpx . "bills b ON e.bill_id = b.bill_id
                            LEFT JOIN " . $tblpx . "company_expense_type et ON e.expensehead_id = et.company_exp_id
                            LEFT JOIN " . $tblpx . "company_expense_type v ON e.dailyexpense_receipt_head = v.company_exp_id
                            LEFT JOIN " . $tblpx . "bank ba ON e.bank_id = ba.bank_id
                            WHERE e.date = '" . $expDate . "' AND (" . $newQuery . ") AND e.transaction_parent IS NULL")->queryAll();
        $client = $this->renderPartial('newlist', array('newmodel' => $expenseData));
        return $client;
    }

    public function actiongetbank()
    {
        $bank_id = $_POST['id'];
        $company = $_POST['company'];
        $tblpx = Yii::app()->db->tablePrefix;
        $bank = Yii::app()->db->createCommand("SELECT * FROM {$tblpx}bank WHERE bank_id NOT IN (" . $bank_id . ") AND company_id IN(" . $company . ") ORDER BY bank_id ASC")->queryAll();
        $result = "<option value=''>-Select Bank-</option>";
        foreach ($bank as $bankdata) {
            $result .= "<option value='" . $bankdata["bank_id"] . "'>" . $bankdata["bank_name"] . "</option>";
        }
        echo $result;
    }

    public function saveWithdrawalFromBankData($id, $transId, $data, $premodel, $reconmodel, $created_by, $created_date)
    {

        $expensetype = Companyexpensetype::model()->findByAttributes(
            array('name' => 'Cash Withdrawal')
        );
        $model1 = Dailyexpense::model()->findByPk($id);
        $model2 = Dailyexpense::model()->findByPk($transId);

        if (Yii::app()->user->role == 1) {
            //expense
            $model1->attributes = $data;
            $model1->date = date('Y-m-d', strtotime($data['date']));
            $model1->exp_type_id = $expensetype['company_exp_id'];
            $model1->dailyexpense_type = 1;
            $model1->expensehead_id = $expensetype['company_exp_id'];
            $model1->dailyexpense_receipt_type = NULL;
            $model1->dailyexpense_receipt_head = NULL;
            $model1->dailyexpense_receipt = NULL;
            $model1->exp_type = 73;
            $model1->display_flg = 'No';
            $model1->expensehead_type = 4;

            if ($model1->expense_type == 88) {
                //$reconmodel = $this->saveReconcilModel();
                $reconmodel->reconciliation_amount = $data['dailyexpense_paidamount'];
                $reconmodel->reconciliation_table = $this->tableNameAcc('dailyexpense', 0);
                $reconmodel->reconciliation_paymentdate = date('Y-m-d', strtotime($data['date']));
                $reconmodel->reconciliation_bank = $data['bank_id'];
                $reconmodel->reconciliation_chequeno = $data['dailyexpense_chequeno'];
                $reconmodel->created_date = date("Y-m-d H:i:s");
                $reconmodel->reconciliation_status = 0;
                $reconmodel->company_id = $data['company_id'];
                $reconmodel->reconciliation_payment = "Dailyexpense Payment";
            }

            // receipt
            $model2->attributes = $data;
            $model2->date = date('Y-m-d', strtotime($data['date']));
            $model2->exp_type_id = $expensetype['company_exp_id'];
            $model2->dailyexpense_type = 3;
            $model2->expensehead_id = NULL;
            $model2->expense_type = NULL;
            $model2->dailyexpense_receipt_type = $data['expense_type'];
            $model2->dailyexpense_receipt_head = $expensetype['company_exp_id'];
            $model2->dailyexpense_receipt = $data['amount'];
            $model2->dailyexpense_purchase_type = NULL;
            $model2->dailyexpense_paidamount = NULL;
            $model2->exp_type = 72;
            $model2->display_flg = 'No';
            $model2->expensehead_type = 4;
        } else {
            $model1->approval_status = 0;
            $model2->approval_status = 0;
            $premodel->attributes = $data;

            $premodel->ref_id = $id;
            $premodel->date = date('Y-m-d', strtotime($data['date']));
            $premodel->record_grop_id = date('Y-m-d H:i:s');
            $premodel->approval_status = 0;
            $premodel->record_action = "update";
            $premodel->approve_notify_status = 1;
            $premodel->user_id = Yii::app()->user->id;
            $premodel->created_by = $created_by;
            $premodel->created_date = $created_date;
            $premodel->updated_by = Yii::app()->user->id;
            $premodel->updated_date = date('Y-m-d H:i:s');
            $premodel->exp_type_id = $expensetype['company_exp_id'];
            $premodel->expensehead_id = $expensetype['company_exp_id'];
            $premodel->dailyexpense_receipt_type = NULL;
            $premodel->dailyexpense_receipt_head = NULL;
            $premodel->dailyexpense_receipt = NULL;
            $premodel->dailyexpense_type = 1;
            $premodel->exp_type = 73;
            $premodel->display_flg = 'No';
            $premodel->expensehead_type = 4;

        }
        return (array(
            'model1' => $model1,
            'model2' => $model2,
            'premodel' => $premodel,
            'reconmodel' => $reconmodel
        ));

    }

    public function saveDepositToBankData($id, $transId, $data, $premodel, $reconmodel, $created_by, $created_date)
    {

        $model2 = Dailyexpense::model()->findByPk($id);
        $model1 = Dailyexpense::model()->findByPk($transId);
        $expensetype = Companyexpensetype::model()->findByAttributes(array('name' => 'Cash Deposit'));


        if (Yii::app()->user->role == 1) {
            // expense
            $model1->attributes = $data;
            $model1->date = date('Y-m-d', strtotime($data['date']));
            $model1->exp_type_id = $expensetype['company_exp_id'];
            $model1->dailyexpense_type = 1;
            $model1->expensehead_id = $expensetype['company_exp_id'];
            $model1->expense_type = $data['expense_type'];
            $model1->dailyexpense_receipt_type = NULL;
            $model1->dailyexpense_receipt_head = NULL;
            $model1->dailyexpense_receipt = NULL;
            $model1->exp_type = 73;
            $model1->display_flg = 'No';
            $model1->expensehead_type = 5;


            // receipt

            $model2->attributes = $data;
            $model2->date = date('Y-m-d', strtotime($data['date']));
            $model2->exp_type_id = $expensetype['company_exp_id'];
            $model2->expensehead_id = NULL;
            $model2->expense_type = NULL;
            $model2->dailyexpense_receipt_type = $data['expense_type'];
            $model2->dailyexpense_receipt_head = $expensetype['company_exp_id'];
            $model2->dailyexpense_receipt = $data['amount'];
            $model2->dailyexpense_purchase_type = NULL;
            $model2->dailyexpense_paidamount = NULL;
            $model2->dailyexpense_type = 3;
            $model2->exp_type = 72;
            $model2->display_flg = 'No';
            $model2->expensehead_type = 5;

            if ($model2->dailyexpense_receipt_type == 88) {
                $reconmodel->reconciliation_paymentdate = date('Y-m-d', strtotime($model2->date));
                $reconmodel->reconciliation_bank = $model2->bank_id;
                $reconmodel->reconciliation_chequeno = $model2->dailyexpense_chequeno;
                $reconmodel->created_date = date("Y-m-d H:i:s");
                $reconmodel->reconciliation_status = 0;
                $reconmodel->company_id = $model2->company_id;
                $reconmodel->reconciliation_amount = $model2->amount;
                $reconmodel->reconciliation_payment = "Dailyexpense Receipt";
            }
        } else {

            $model1->approval_status = 0;
            $model2->approval_status = 0;
            $premodel->attributes = $data;
            $premodel->ref_id = $id;
            $premodel->date = date('Y-m-d', strtotime($data['date']));
            $premodel->record_grop_id = date('Y-m-d H:i:s');
            $premodel->approval_status = 0;
            $premodel->record_action = "update";
            $premodel->approve_notify_status = 1;
            $premodel->user_id = Yii::app()->user->id;
            $premodel->created_by = $created_by;
            $premodel->created_date = $created_date;
            $premodel->updated_by = Yii::app()->user->id;
            $premodel->updated_date = date('Y-m-d H:i:s');
            $premodel->exp_type_id = $expensetype['company_exp_id'];
            $premodel->expensehead_id = NULL;
            $premodel->expense_type = NULL;
            $premodel->dailyexpense_receipt_type = $data['expense_type'];
            $premodel->dailyexpense_receipt_head = $expensetype['company_exp_id'];
            $premodel->dailyexpense_receipt = $data['amount'];
            $premodel->dailyexpense_purchase_type = NULL;
            $premodel->dailyexpense_paidamount = NULL;
            $premodel->dailyexpense_type = 3;
            $premodel->exp_type = 72;
            $premodel->display_flg = 'No';
            $premodel->expensehead_type = 5;
        }

        return (array(
            'model1' => $model1,
            'model2' => $model2,
            'premodel' => $premodel,
            'reconmodel' => $reconmodel
        ));
    }

}
