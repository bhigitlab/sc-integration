
ALTER TABLE `jp_subcontractorbill` CHANGE `scquotation_id` `scquotation_id` INT(11) NULL DEFAULT NULL; 
UPDATE `jp_subcontractorbill` SET `scquotation_id` = NULL WHERE scquotation_id NOT IN(SELECT scquotation_id FROM jp_scquotation);

ALTER TABLE `jp_subcontractorbill` ADD CONSTRAINT `fk_sc_bill_quotation_id` FOREIGN KEY (`scquotation_id`) REFERENCES `jp_scquotation`(`scquotation_id`) ON DELETE RESTRICT ON UPDATE CASCADE; 