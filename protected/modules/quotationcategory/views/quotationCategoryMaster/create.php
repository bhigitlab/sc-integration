<?php
/* @var $this QuotationCategoryMasterController */
/* @var $model QuotationCategoryMaster */

$this->breadcrumbs=array(
	'Quotation Category Masters'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List QuotationCategoryMaster', 'url'=>array('index')),
	array('label'=>'Manage QuotationCategoryMaster', 'url'=>array('admin')),
);
?>

<h1>Create QuotationCategoryMaster</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>