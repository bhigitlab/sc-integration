<?php
/* @var $this AlbumsController */
/* @var $model Albums */

$this->breadcrumbs=array(
	'Albums'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Albums', 'url'=>array('index')),
	array('label'=>'Create Albums', 'url'=>array('create')),
	array('label'=>'Update Albums', 'url'=>array('update', 'id'=>$model->album_id)),
	array('label'=>'Delete Albums', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->album_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Albums', 'url'=>array('admin')),
);
?>

<h1>View Albums #<?php echo $model->album_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'album_id',
		'title',
		'description',
		'projectid',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
	),
)); ?>
