<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/js/jquery.message.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/message/css/jquery.message.css">

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<?php 
$pms_api_integration_model = ApiSettings::model()->findByPk(1);
$pms_api_integration = $pms_api_integration_model->api_integration_settings;
?>
<div class="container" id="expense">
	<div class="loading-overlay">
		<span class="fa fa-spinner fa-3x fa-spin"></span>
	</div>
	<div class="expenses-heading">
        <?php 
       
			$allBadge = '';
			$approveBadge = '';
			$nonApproveBadge = '';

			if ($_GET['status'] == 3) {
				$allBadge = 'badge-selected';
			} elseif ($_GET['status'] == 2) {
				$approveBadge = 'badge-selected';
			} elseif ($_GET['status'] == 1) {
				$nonApproveBadge = 'badge-selected';
			}
			?>
        <div class="clearfix">
			<?php if ($allData > 0) { ?>
				<button type="button" id="allbutton" class="btn btn-default pull-right mt-0 mb-10" onclick="filterData(3);" title="All Data" data-toggle="tooltip" data-placement="top">All 
				<span class="badge <?php echo $allBadge; ?>">
					<?php echo ($allData > 100) ? '99+' : $allData; ?>
				</span>
				</button>
			
			<?php } ?>
		    <?php if ($approvedData > 0) { ?>
				<button 
						type="button"
						id="approved" 
						class="btn btn-default pull-right mt-0 mb-10 margin-right-5" 
						onclick="filterData(2); return false;" 
						title="Approved Data" 
						data-toggle="tooltip" 
						data-placement="top"
					>
					
						Approved 
						<span class="badge badge-success <?php echo $approveBadge; ?>">
							<?php echo ($approvedData > 100) ? '99+' : $approvedData; ?>
						</span>
				</button>		
				
			<?php } ?> 
            <?php if ($nonApprovedData > 0) { ?>
					<button 
						type="button" 
						id="nonapproved"
						class="btn btn-default pull-right mt-0 mb-10 margin-right-5" 
						onclick="filterData(1); return false;" 
						title="Non Approved Data" 
						data-toggle="tooltip" 
						data-placement="top"
					>
						Non-Approved 
						<span class="badge badge-danger <?php echo $nonApproveBadge; ?>">
							<?php echo ($nonApprovedData > 100) ? '99+' : $nonApprovedData; ?>
						</span>
					</button>
			<?php } ?>
            <!-- <button type="button" id="bulkApprove" class="btn btn-info pull-right mt-0 mb-10 margin-right-5">Approve</button> -->
            <h3>Equipment Entries</h3>
        </div>
    </div>
	<div id="errormessage"></div>
	
	<div class="table-wrapper margin-top-25">

         <div class="new-entry-table">
		
	    <?php if ($pms_api_integration == 1): ?>
		<ul class="legend">
			<li><span class="project_not_mapped"></span> Project/PMS Template Not Mapped In Integration</li>
		</ul>

		<?php endif; ?>
        <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'pager' => array(
                'header' => '',
                'maxButtonCount' => 5,
                'pageSize' => 10, 
            ),
            'viewData' => array(),
            'itemView' => '_view', 'template' => '<div class="clearfix"><div class="pull-right">{summary}</div><div class="pull-left">{sorter}</div></div><div id="parent" class="subq_parent"><table cellpadding="10" class="table tab1" id="subqtntbl">{items}</table></div>{pager}',
            'enableSorting' => true,
            
        )); ?>
    </div>
</div>
<?php
 $this->renderPartial('_reject_modal', array('model' => $model)) 
 ?>
<script>
    $(document).ready(function() {
		$(".popover-test").popover({
			html: true,
			content: function() {
				return $(this).next('.popover-content').html();
			}
		});
		$('[data-toggle=popover]').on('click', function(e) {
			$('[data-toggle=popover]').not(this).popover('hide');
		});
		$('body').on('hidden.bs.popover', function(e) {
			$(e.target).data("bs.popover").inState.click = false;
		});
		$('body').on('click', function(e) {
			$('[data-toggle=popover]').each(function() {
				if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
					$(this).popover('hide');
				}
			});
		});
	});
    function edit(id) {
        window.location.href = '<?php echo Yii::app()->createAbsoluteUrl('EquipmentsEntries/create&id='); ?>' + id;
    }

    function view(id) {
        window.location.href = '<?php echo Yii::app()->createAbsoluteUrl('EquipmentsEntries/viewequipment&id='); ?>' + id;
    }
    function approve(id) {

$('.loading-overlay').addClass('is-active');
$.ajax({
    url: '<?php echo Yii::app()->createAbsoluteUrl('EquipmentsEntries/approve'); ?>',
    type: 'POST',
    data: { id: id },
    success: function (data) {
        if (data == 1) {
            $("#errorMsg").show()
                .html('<div class="alert alert-success"><b>Success!</b> Equipments Entry Approved Successfully.</div>')
                .fadeOut(5000);
            setTimeout(function () {
                location.reload();
            }, 3000);
        } else {
            $("#errorMsg").show()
                .html('<div class="alert alert-danger"><b>Error Occurred.</b></div>')
                .fadeOut(5000);
        }
    }
});
}

function openRejectModal(id) {
$('#rejectId').val(id); 
$('#rejectModal').modal('show'); 
}

function submitReject() {
var id = $('#rejectId').val();
var remarks = $('#remarks').val();

if (remarks === "") {
    alert("Remarks field cannot be empty.");
    return;
}

$.ajax({
    type: "POST",
    url: "index.php?r=EquipmentsEntries/rejectEquipment",
    data: {
        id: id,
        remarks: remarks,
    },
    success: function(response) {
        var result = JSON.parse(response);
        if (result.response === 'success') {
            alert('Equipment rejected successfully');
            $('#rejectModal').modal('hide');  
            location.reload();  
        } else {
            alert('Error: ' + result.msg);
        }
    },
    error: function() {
        alert('An error occurred. Please try again.');
    }
    });
}
$(document).ready(function() {
		$("#filter-status").change(function() {
        var filterStatus = $(this).val();
        
    	});
		window.filterData = function(filterStatus) {
        var url = "<?php echo Yii::app()->createUrl('EquipmentsEntries/admin'); ?>";
        window.location.href = url + "&status=" + filterStatus;
    };
  });
</script>