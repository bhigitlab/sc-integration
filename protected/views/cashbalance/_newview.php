<?php
/* @var $this ProjectsController */
/* @var $data Projects */
?>


<div class="row">
	<div class="col-md-6 col-sm-7 col-xs-5">
		<div class="list_item"><span> Cashbalance Type: </span><?php echo CHtml::encode($data->cashbalanceType->caption); ?></div>
		
		<div class="list_item"><span> Expense Type: </span><?php  echo CHtml::encode(($data->bank_id ==0)?'':$data->bank->bank_name); ?></div>
	</div>
	<div class="col-md-3 col-sm-4 col-xs-4">
		
		
	</div>
	
	<?php 
	 
	if(Yii::app()->user->role ==1 || Yii::app()->user->role ==2 ){
		?>
	<div class="col-md-3 col-sm-1 col-xs-1">
		<span class="fa fa-edit editProject" data-toggle="modal" data-target=".edit" data-id="<?php echo $data->id; ?>"></span>
	</div>
	<?php
		}
		?>
	
</div>
