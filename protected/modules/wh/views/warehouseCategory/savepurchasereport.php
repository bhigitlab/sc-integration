<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap-theme.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" />
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/tablestyles.css" />
<div class="container" id="project"> 
    <div id="contenthtml" class="contentdiv">
        <h2 class="text-center">Purchase Report of <?php echo $model["name"]; ?></h2>
        <div class="container-fluid">
            <table class="table table-bordered ">
                <tr>
                    <td><b>Client : </b><?php echo $model["clientname"]; ?></td>
                </tr>
                <tr>
                    <td><b>Project Name : </b><?php echo $model["name"]; ?></td>
                </tr>
                <tr>
                    <td><b>Site : </b><?php echo $model["site"]; ?></td>
                </tr>
            </table>
            <table class="table table-bordered ">
                <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>Category</th>
                        <th>Quantity</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $i              = 1;
                $quantitySum    = 0;
                $amountSum      = 0;
                foreach($itemmodelcat as $modelcat) {
                    $quantitySum    = $quantitySum + $modelcat["billitemsum"];
                    $amountSum      = $amountSum + $modelcat["itemtotalamount"];
                ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $modelcat["category_name"]; ?></td>
                        <td class="text-right"><?php echo Controller::money_format_inr($modelcat["billitemsum"], 2); ?></td>
                        <td class="text-right"><?php echo Controller::money_format_inr($modelcat["itemtotalamount"], 2); ?></td>
                    </tr>
                <?php
                    $i = $i + 1;
                }
                ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th class="text-right"><?php echo Controller::money_format_inr($quantitySum, 2); ?></th>
                        <th class="text-right"><?php echo Controller::money_format_inr($amountSum, 2); ?></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>