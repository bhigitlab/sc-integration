<?php

/**
 * This is the model class for table "{{warehousedespatch_items}}".
 *
 * The followings are the available columns in table '{{warehousedespatch_items}}':
 * @property integer $item_id
 * @property integer $warehousedespatch_id
 * @property integer $warehousedespatch_warehouseid
 * @property integer $warehousedespatch_itemid
 * @property string $warehousedespatch_unit
 * @property double $warehousedespatch_quantity
 * @property double $warehousedespatch_size
 * @property string $warehousedespatch_remarks
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class WarehousedespatchItems extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{warehousedespatch_items}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('warehousedespatch_id, warehousedespatch_warehouseid, warehousedespatch_itemid, warehousedespatch_unit, warehousedespatch_quantity, created_by, created_date, updated_by, updated_date', 'required'),
			array('warehousedespatch_id, warehousedespatch_warehouseid, warehousedespatch_itemid, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('warehousedespatch_quantity, warehousedespatch_size,warehousedespatch_baseunit_quantity', 'numerical'),
			array('warehousedespatch_unit', 'length', 'max'=>100),
			array('warehousedespatch_remarks', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('item_id, warehousedespatch_id, warehousedespatch_warehouseid, warehousedespatch_itemid, warehousedespatch_unit, warehousedespatch_quantity,warehousedespatch_baseunit_quantity, warehousedespatch_size, warehousedespatch_remarks, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'item_id' => 'Item',
			'warehousedespatch_id' => 'Warehousedespatch',
			'warehousedespatch_warehouseid' => 'Warehousedespatch Warehouseid',
			'warehousedespatch_itemid' => 'Warehousedespatch Itemid',
			'warehousedespatch_unit' => 'Warehousedespatch Unit',
			'warehousedespatch_quantity' => 'Warehousedespatch Quantity',
			'warehousedespatch_baseunit_quantity' => 'Warehousedespatch Quantity (Base)',
			'warehousedespatch_size' => 'Warehousedespatch Size',
			'warehousedespatch_remarks' => 'Warehousedespatch Remarks',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('warehousedespatch_id',$this->warehousedespatch_id);
		$criteria->compare('warehousedespatch_warehouseid',$this->warehousedespatch_warehouseid);
		$criteria->compare('warehousedespatch_itemid',$this->warehousedespatch_itemid);
		$criteria->compare('warehousedespatch_unit',$this->warehousedespatch_unit,true);
		$criteria->compare('warehousedespatch_quantity',$this->warehousedespatch_quantity);
		$criteria->compare('warehousedespatch_baseunit_quantity',$this->warehousedespatch_baseunit_quantity);
		$criteria->compare('warehousedespatch_size',$this->warehousedespatch_size);
		$criteria->compare('warehousedespatch_remarks',$this->warehousedespatch_remarks,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WarehousedespatchItems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
