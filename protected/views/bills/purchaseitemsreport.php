<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */
$tblpx = Yii::app()->db->tablePrefix;
$this->breadcrumbs = array(
    'Users',
);
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<div class="container" id="user">
    <div class="header-container">
        <h3>Purchase Bill Report</h3>
        <div class="btn-container">
            <a class="btn btn-info"
                href="<?php echo $this->createAbsoluteUrl('bills/purchasetopdf', array('date_from' => $date_from, 'date_to' => $date_to, 'pro_id' => $pro_id, 'vendor_id' => $vendor_id, 'pu_num' => $pu_num, 'bill_num' => $bill_num, 'company_id' => $company_id, 'item_spec' => $item_spec, 'exp_head' => $exp_head)) ?>"
                style="text-decoration: none; color: white;" title="SAVE AS PDF">
                <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
            </a>
            <a class="btn btn-info"
                href="<?php echo $this->createAbsoluteUrl('bills/purchasetoexcel', array('date_from' => $date_from, 'date_to' => $date_to, 'pro_id' => $pro_id, 'vendor_id' => $vendor_id, 'pu_num' => $pu_num, 'bill_num' => $bill_num, 'company_id' => $company_id, 'item_spec' => $item_spec, 'exp_head' => $exp_head)) ?>"
                style="text-decoration: none; color: white;" title="SAVE AS EXCEL">
                <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>
            </a>
        </div>
    </div>


    <?php $this->renderPartial('_purchasereportsearch', array(
        'model' => $model,
        'date_from' => $date_from,
        'date_to' => $date_to,
        'pro_id' => $pro_id,
        'vendor_id' => $vendor_id,
        'pu_num' => $pu_num,
        'bill_num' => $bill_num,
        'company_id' => $company_id,
        'specification' => $specification,
        'item_spec' => $item_spec,
        'expense_type' => $expense_type
    )) ?>

    <div id="errorMsg"></div>
    <div class="reconciliation-data">
        <?php
        $tot_billamount = 0;
        $tot_paidamount = 0;
        $bal = 0;
        $tot_bal = 0;
        foreach ($model as $key => $value) {
            $bal = $value['bill_totalamount'] - $value['paid'];
            $addcharges = Yii::app()->db->createCommand("SELECT ROUND(SUM(amount), 2) as amount FROM " . $tblpx . "additional_bill WHERE `bill_id`=" . $value['bill_id'])->queryRow();
            if (!empty($addcharges)) {
                $tot_billamount += $addcharges['amount'] + $value['bill_totalamount'];
            } else {
                $tot_billamount += $value['bill_totalamount'];
            }
            $tot_paidamount += $value['paid'];
            $tot_bal += $bal;
        }

        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'viewData' => array('tot_billamount' => $tot_billamount, 'tot_paidamount' => $tot_paidamount, 'tot_bal' => $tot_bal, ),
            'itemView' => '_purchasereportview',
            'template' => '<div>{summary}</div><div id="parent"><div class="report-table-wrapper"><table cellpadding="10" class="table total-table" id="fixTable">{items}</table></div></div>',
        )); ?>

    </div>
</div>

<style>
    #parent {
        max-height: 500px;
    }

   

    .table>thead>tr>th,
    .table>tbody>tr>th,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>tbody>tr>td,
    .table>tfoot>tr>td {
        border-bottom: 0px;
    }
</style>

<script>
    $(document).ready(function () {
        var item_spec = '<?php echo $item_spec; ?>';
        var exp_head = '<?php echo $exp_head; ?>';
        $("#parent").css('max-height', $(window).height() - $("#myNavbar").height() - $(".search-form").height() - (2 * $('footer').height()));
        $("#fixTable").tableHeadFixer({ 'head': true, 'foot': true });
        var array = item_spec.split(',');
        var array2 = exp_head.split(',');
        $('.category_id').val(array);
        $('.category_id').trigger('change');
        $('.expense_head').val(array2);
        $('.expense_head').trigger('change');
    });
</script>