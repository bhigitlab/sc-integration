ALTER TABLE `jp_warehouse_stock_correction` CHANGE `warehouse_id` `warehouse_id` INT(11) NULL DEFAULT NULL, CHANGE `wh_receipt_id` `wh_receipt_id` INT(11) NULL DEFAULT NULL;

ALTER TABLE `jp_warehouse_stock_correction` ADD `approval_status` ENUM('0', '1', '2') NOT NULL DEFAULT '0' COMMENT '0=>Pending,1=>approved,2=>rejected' AFTER `rate`;