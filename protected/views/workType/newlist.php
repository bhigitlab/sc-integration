<?php


$this->breadcrumbs = array(
    'work Type',
);
?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container" id="expensetype">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix">
        <div class="add-btn pull-right">
            <?php
            if (isset(Yii::app()->user->role) && (in_array('/workType/create', Yii::app()->user->menuauthlist))) {
            ?>
                <a class="button addworktype">Add Work Type</a>

            <?php } ?>
        </div>
        <h2>Work Types</h2>
    </div>
    <div id="addworktype" style="display:none;"></div>

    <?php //$this->renderPartial('_newsearch', array('model' => $model)) 
    ?>



    <div class="row">
        <div class="col-md-12">
            <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_newview',
                'template' => '<div>{sorter}</div><div class=""><table cellpadding="10" id="worktypetab" class="table">{items}</table></div>',
            ));
            ?>
        </div>
        <!-- Add Expense type Popup -->
        <div id="addworkType" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

            </div>
        </div>


        <!-- Edit Expense type Popup -->

        <div class="modal fade edit" role="dialog">
            <div class="modal-dialog modal-lg">


            </div>
        </div>
        <?php
        Yii::app()->clientScript->registerScript('myjavascript', '
     $(document).ready(function(){
     
       $("#worktypetab").dataTable( {
            "scrollY": "300px",
            "scrollCollapse": true,
            "paging": false,
             "columnDefs"        : [       
                { 
                    "searchable"    : false, 
                    "targets"       : [0,2] 
                },
            ],
              
            
	} );
	
	
      
        
	});
        
    ');
        ?>

        <script>

        </script>
        <script>
            function closeaction() {
                $('#addworktype').slideUp(500);
            }

            function editaction(elem, event) {

                $(elem).parent().hide();
                $(elem).parents().find('td:eq(2)').show();
                var rolename = $(elem).parent().find('.list_item').text();
                var id = $(elem).attr('data-id');

                $('.work_type').val(rolename);
                $(elem).parents().siblings().find('.test').hide();
                $(elem).parents().siblings().find('td:eq(1)').show();


                return false;
            }

            function cancelaction(elem, event) {
                $(elem).parents().find('.text_black').show();
                $(elem).parents().find('.test').hide();

            }

            function saveaction(elem, event) {
                var value = $(elem).parent().find('.work_type').val();
                $('.loading-overlay').addClass('is-active');
                var id = $(elem).attr('data-id');
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "<?php echo $this->createUrl('workType/update2') ?>",
                    data: {
                        id: id,
                        value: value
                    },
                    success: function(response) {
                        if (response == null) {
                            $(elem).parents().find('td:eq(2)').hide();
                            $(elem).parents().find('td:eq(1)').show();
                            $(elem).parent().parent().find('.list_item').text(value);

                            // location.reload();
                        } else {
                            var obj = eval(response);
                            $(".errorMessage1").text(obj);
                        }
                    },


                });
                return false;
            }

            $(document).ready(function() {

                $('.addworktype').click(function() {
                    $('.loading-overlay').addClass('is-active');
                    var id = $(this).attr('data-id');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $this->createUrl('WorkType/create&layout=1') ?>",
                        success: function(response) {
                            $('#addworktype').html(response).slideDown();
                        },
                    });
                });

            });
            $(document).ajaxComplete(function() {
                $('.loading-overlay').removeClass('is-active');
                $('#loading').hide();
            });
        </script>

        <style>
            span.text_black {
                color: #333
            }

            .editable {
                float: right;
            }

            .page-body h3 {
                margin: 4px 0px;
                color: inherit;
                text-align: left;
            }

            .panel {
                border: 1px solid #ddd;
            }

            .panel-heading {
                background-color: #eee;
                height: 40px;
            }

            table.dataTable>thead>tr th:last-child {
                width: 40px;
                background-image: none;
            }

            .errorMessage1 {
                color: red;
            }
        </style>

    </div>
</div>