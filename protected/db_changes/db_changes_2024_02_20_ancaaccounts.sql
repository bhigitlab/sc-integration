UPDATE `jp_menu_permissions` SET `menu_id` = 805 WHERE `menu_id` = 806;

UPDATE `jp_profile_menu_settings` SET `menu_id` = 805 WHERE `menu_id` = 806;

UPDATE `jp_menu` SET `menu_name` = 'Media Settings', `parent_id` = '487', `controller` = 'dashboard', `action` = 'mediasettings' WHERE `jp_menu`.`menu_id` = 806;
