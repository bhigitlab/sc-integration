<?php
$cs = Yii::app()->clientScript;
$cs->scriptMap = array(
    'jquery.js' => false,
    'jquery.min.js' => false,
);
$cs->coreScriptPosition = CClientScript::POS_END;
?>
<?php
$app_root = YiiBase::getPathOfAlias('webroot');
$theme_asset_url = Yii::app()->getBaseUrl(true) . '/themes/assets/';
$banner_img = $theme_asset_url . 'login-scv2.svg';

?>
<html lang="en">
    <head>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100..900&family=Roboto&family=Work+Sans:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet" />
        <style>
            
            @font-face {
                font-family: 'Roboto', sans-serif;
                
            }

           
            .custom-login-page .img1 {
                position: absolute;
                width: 100%;
                height: 100%;
                z-index: 0;
                background-image: url(<?= $this->logo ?>);
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-position: 10px 20px;
            }
        </style>
        <meta charset="utf-8" />
        <title>Cost Management System | Cost Management System</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link
            href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
            rel="stylesheet"
            type="text/css"
            />

        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?= Yii::app()->theme->baseUrl ?>/css/login.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME STYLES -->

        <link href="<?= Yii::app()->theme->baseUrl ?>/css/layout.css" rel="stylesheet" type="text/css" />

        <link href="<?= Yii::app()->theme->baseUrl ?>/css/screen.css" rel="stylesheet" type="text/css" />

        <!-- END THEME STYLES -->
        <link rel="icon" type="image/png" sizes="192x192" href="<?= $this->favicon; ?>" />
    </head>

    <body class="login-page-body">
        <div class="custom-login-page">
            <div class="img1"></div>
            <div class="container-fluid wrapper">
                <div class="view-section">
                    <div class="first-section">
                        <img src="<?= $banner_img ?>" class="coms-bg" />
                        <h3 class="coms-head">Cost Management System</h3>
                    </div>
                    <?php echo $content; ?>
                </div>
            </div>
        </div>
        <script src="<?= Yii::app()->theme->baseUrl ?>/js/jquery.min.js" type="text/javascript"></script>
        <script src="<?= Yii::app()->theme->baseUrl ?>/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->

        <!-- END PAGE LEVEL PLUGINS -->

    </body>
</html>