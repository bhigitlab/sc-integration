<?php

class PurchaseCategoryController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';
	//private $access_rules = array();

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{

		$accessArr = array();
		$accessauthArr = array();
		$controller = Yii::app()->controller->id;
		if (isset(Yii::app()->user->menuall)) {
			if (array_key_exists($controller, Yii::app()->user->menuall)) {
				$accessArr = Yii::app()->user->menuall[$controller];
			}
		}

		if (isset(Yii::app()->user->menuauth)) {
			if (array_key_exists($controller, Yii::app()->user->menuauth)) {
				$accessauthArr = Yii::app()->user->menuauth[$controller];
			}
		}
		$access_privlg = count($accessauthArr);
		return array(
			array(
				'allow', // allow all users to perform 'index' and 'view' actions
				'actions' => $accessArr,
				'users' => array('@'),
				'expression' => "$access_privlg > 0",
			),

			array(
				'allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => $accessauthArr,
				'users' => array('@'),
				'expression' => "$access_privlg > 0",
			),
			array(
				'allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('gethsncode','purchasereport','ExportPurchaseReportCSV','savepurchasereportpdf','getSpecificationDetails','checkDuplicateSpecification','importcsv','downloadsample','saveItems','createnewcategory','createnewbrand','createnewsubcategory','updateImport','deleteimports','purchasereportdetails'),
				'users' => array('*'),
			),
			array(
				'deny',  // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */

	public function actionCreate()
	{


		if (isset($_POST['PurchaseCategory'])) {
			if ($_POST['category_id'] == '' || $_POST['category_id'] == 0) {
				$model = new PurchaseCategory;
				$this->performAjaxValidation($model);
				$model->unsetAttributes();
				$model->parent_id = NULL;
				$model->category_name 	 = $_POST['PurchaseCategory']['category_name'];
				$model->spec_flag = 'N';
				$model->type = 'C';
				if (isset($_POST['PurchaseCategory']['company_id']) || !empty($_POST['PurchaseCategory']['company_id'])) {
					$company = implode(",", $_POST['PurchaseCategory']['company_id']);
					$model->company_id = $company;
				} else {
					$model->company_id = NULL;
				}
				$model->created_by = Yii::app()->user->id;
				$model->created_date =  date('Y-m-d');
				if ($model->save()) {
					/*$parentcondition="";
					if($model->parent_id!="" && $model->category_name !=""){
						$main_cat = PurchaseCategory::model()->findByPk($model->parent_id);
						$parentcondition = " main_category='".$main_cat->category_name."' AND sub_category = '".$model->category_name."'";
						$tempcategory = PurchaseItemsImport::model()->find(array('condition' => $parentcondition));
						if(!empty($tempcategory)){
							$tempcategory->sub_cat_exist = 1;
						}
          }else if($model->parent_id=="" && $model->category_name !=""){						
						$parentcondition = " main_category='".$model->category_name."'";
						$tempcategory = PurchaseItemsImport::model()->find(array('condition' => $parentcondition));
						if(!empty($tempcategory)){
							$tempcategory->main_cat_exist = 1;
						}
          }
					$tempcategory->save();
					*/
					Yii::app()->user->setFlash('success', "Added Successfully");
					$this->redirect(array('purchaseCategory/newList'));
				}
			} else {
				$id = $_POST['category_id'];
				$model = $this->loadModel($id);
				$this->performAjaxValidation($model);
				//$model->unsetAttributes();
				$model->parent_id = NULL;
				$model->category_name 	 = $_POST['PurchaseCategory']['category_name'];
				$model->hsn_code 	 = (isset($_POST['PurchaseCategory']['hsn_code']) ? $_POST['PurchaseCategory']['hsn_code'] : null);
				if (isset($_POST['PurchaseCategory']['company_id']) || !empty($_POST['PurchaseCategory']['company_id'])) {
					$company = implode(",", $_POST['PurchaseCategory']['company_id']);
					$model->company_id = $company;
				} else {
					$model->company_id = NULL;
				}
				if ($model->save()) {
					Yii::app()->user->setFlash('success', "Updated Successfully");
					$this->redirect(array('purchaseCategory/newList'));
				}
			}
		}
	}


    public function actionCreatespecification() {

          $images_path = realpath(Yii::app()->basePath . '/../uploads/purchase_category');
        $images_path2 = realpath(Yii::app()->basePath . '/../uploads/purchase_category/thumbnail');

        if (isset($_POST['PurchaseSpecification'])) {
            $transaction = Yii::app()->db->beginTransaction();
            try {
                if ($_POST['specification_id'] == '' || $_POST['specification_id'] == 0) {
                    $model = new Specification;
                    $model2 = new UnitConversion();
                    $this->performAjaxValidation1($model);

                    if ($_FILES['Specification']['name']['filename']) {
                        $filename = CUploadedFile::getInstance($model, 'filename');
                        $newfilename = rand(1000, 9999) . time();
                        $newfilename = md5($newfilename);
                        $extension = $filename->getExtensionName();
                        $model->filename = $newfilename . "." . $extension;
                    }

                    $model->attributes = $_POST['PurchaseSpecification'];
					//echo "<pre>";print_r($model);exit;
                    $model->created_by = Yii::app()->user->id;
                    $model->created_date = date('Y-m-d H:i:s');
                    $company_id = $_POST['PurchaseSpecification']['company_id'];
                    $model->company_id = NULL;

                    if (isset($company_id) || !empty($company_id)) {
                        $company = implode(",", $company_id);
                        $model->company_id = $company;
                    }

                    $model->specification_type = $_POST['PurchaseSpecification']['specification_type'];
                    if ($model->specification_type == 'A') {
                        $model->dieno = $_POST['PurchaseSpecification']['dieno'];
                        $model->specification = $model->dieno;
                    }
                    if ($model->save()) {
                        $last_insertedid = Yii::app()->db->getLastInsertID();
                        $model2->item_id = $last_insertedid;
                        $model2->base_unit = $_POST['PurchaseSpecification']['unit'];
                        $model2->conversion_factor = 1;
                        $model2->conversion_unit = $_POST['PurchaseSpecification']['unit'];
                        $model2->created_date = date('Y-m-d H:i:s');
                        $model2->priority = '1';
                        if ($_FILES['Specification']['name']['filename']) {
                            $filename->saveAs($images_path . '/' . $newfilename . "." . $extension);
                            $image = new EasyImage($images_path . '/' . $newfilename . "." . $extension);
                            $image->resize(60, 60);
                            $image->save($images_path2 . '/' . $newfilename . "." . $extension);
                        }
                        if (!$model2->save()) {
                            throw new Exception(json_encode($model2->getErrors()));
                        }

                        Yii::app()->user->setFlash('success', "Added Successfully");
                    } else {
						
                        throw new Exception(json_encode($model->getErrors()));
                    }
                } else {
                    $id = $_POST['specification_id'];
                    $model = Specification::model()->findByPk($id);
                    $this->performAjaxValidation1($model);

                    if ($_FILES['Specification']['name']['filename']) {
                        $filename = CUploadedFile::getInstance($model, 'filename');
                        $newfilename = rand(1000, 9999) . time();
                        $newfilename = md5($newfilename); //optional
                        $extension = $filename->getExtensionName();
                        $model->filename = $newfilename . "." . $extension;
                    }

                    $model->attributes = $_POST['PurchaseSpecification'];
                    $company_id = $_POST['PurchaseSpecification']['company_id'];
                    $model->company_id = NULL;

                    if (isset($company_id) || !empty($company_id)) {
                        $company = implode(",", $company_id);
                        $model->company_id = $company;
                    }

                    $model->specification_type = $_POST['PurchaseSpecification']['specification_type'];
                    if ($model->specification_type == 'A') {
                        $model->dieno = $_POST['PurchaseSpecification']['dieno'];
                        $model->specification = $model->dieno;
                    }

                    if ($model->save()) {
                        if ($_FILES['Specification']['name']['filename'] != '') {
                            $uploadfile = $filename->saveAs($images_path . '/' . $newfilename . "." . $extension);
                            $image = new EasyImage($images_path . '/' . $newfilename . "." . $extension);
                            $image->resize(60, 60);
                            $image->save($images_path2 . '/' . $newfilename . "." . $extension);
                        }
						Yii::app()->session['cat_id'] = "";
						Yii::app()->session['sub_cat_id'] = "";
                        Yii::app()->session['brand_id'] = "";
                        Yii::app()->session['dieno'] = "";
                        Yii::app()->session['unit'] = "";
                        Yii::app()->session['hsn_code'] = "";
                        Yii::app()->session['company_id'] = "";
						Yii::app()->user->setFlash('success', "Updated Successfully");
                    } else {
                        throw new Exception(json_encode($model->getErrors()));
                    }
                }
                $transaction->commit();
            } catch (Exception $error) {
                $transaction->rollback();
            } finally {
				//print_r($model->getErrors());exit;
                $this->redirect(array('purchaseCategory/newList'));
            }
        }
    }

    public function actionCheckspecification()
	{
		if ($_REQUEST['specification'] != '' && $_REQUEST['parent_id'] != '') {
			$tblpx = Yii::app()->db->tablePrefix;
			if (isset($_REQUEST['brand_id']) && ($_REQUEST['brand_id']) != '') {
				$specification = Yii::app()->db->createCommand("SELECT id FROM {$tblpx}purchase_category WHERE specification ='" . $_REQUEST['specification'] . "' AND parent_id = " . $_REQUEST['parent_id'] . " AND brand_id=" . $_REQUEST['brand_id'] . "")->queryRow();
			} else {
				$specification = Yii::app()->db->createCommand("SELECT id FROM {$tblpx}purchase_category WHERE specification ='" . $_REQUEST['specification'] . "' AND parent_id = " . $_REQUEST['parent_id'] . " AND brand_id IS NULL")->queryRow();
			}
			if ($specification) {
				echo 'false';
			} else {
				echo 'true';
			}
		}
	}


	public function actionCheckcategory()
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$specification = Yii::app()->db->createCommand("SELECT id FROM {$tblpx}purchase_category WHERE category_name ='" . $_REQUEST['category_name'] . "'")->queryRow();
		if ($specification) {
			echo 'false';
		} else {
			echo 'true';
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		if (isset($_REQUEST['data'])) {
			parse_str($_REQUEST['data'], $data);
			$model->parent_id       = (($data['parent_id'] == '') ? NULL : $data['parent_id']);
			$model->category_name   = $data['category_name'];
			if ($model->save()) {

				return true;
			} else {

				echo 1;
				exit;
			}
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider = new CActiveDataProvider('PurchaseCategory');
		$model = new PurchaseCategory();
		$model->unsetAttributes();  // clear any default values
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}



	public function actionNewlist()
	{
		$model = new PurchaseCategory();
		$model2 = new Brand();
		$model3 = new Specification('search');
		$ucmodel = new UnitConversion();
		$model4 = new Unit();
		$model5 = new PurchaseSubCategory();
		$purchaseitemlist = $this->GetItemCategory($parent = 0, $spacing = '', $user_tree_array = '');
		$model->unsetAttributes();  // clear any default values
		$model3->unsetAttributes(); 
		$ucmodel->unsetAttributes();
		if (isset($_GET['Specification']))
			$model3->attributes = $_GET['Specification'];
		$this->render('newlist', array(
			'model' => $model,
			'model2' => $model2,
			'model3' => $model3,
			'model4' => $model4,
			'model5' => $model5,
			'ucmodel' => $ucmodel,
			'dataProvider' => $model->search(Yii::app()->user->id),
			'dataProvider1' => $model3->specificationsearch(Yii::app()->user->id),
			'dataProvider3' => $model2->search(),
			'dataProvider4' => $model5->search(),
			'purchaseitemlist' => $purchaseitemlist,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new PurchaseCategory('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['PurchaseCategory']))
			$model->attributes = $_GET['PurchaseCategory'];

		$this->render('admin', array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PurchaseCategory the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = PurchaseCategory::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	public function actiontest()
	{
		$model = new PurchaseCategory;
		$this->performAjaxValidation1($model);
		$model2 = new Brand();


		/*$this->render('newlist', array(
			'model' => $model,
		   'dataProvider' => $model->search(Yii::app()->user->id),
			'dataProvider1' => $model->specificationsearch(Yii::app()->user->id),
			'dataProvider3' => $model2->search(),
		));*/
	}

	/**
	 * Performs the AJAX validation.
	 * @param PurchaseCategory $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'purchase-category-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	protected function performAjaxValidation2($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'purchase-sub-category-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


	protected function performAjaxValidation1($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'purchase-specification') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionGetItemForEdit()
	{
		$categoryId         = $_REQUEST["category"];
		$categoryRs         = PurchaseCategory::model()->findByPk($categoryId);
		$category["parent"] = $categoryRs["parent_id"];
		$category["name"]   = $categoryRs["category_name"];
		$category["company_id"]   = $categoryRs["company_id"];
		$category["hsn"]   = $categoryRs["hsn_code"];
		$categoryDet        = json_encode($category);
		echo $categoryDet;
	}
	public function actionGetSpecificationForEdit()
	{
		$specification              = $_REQUEST["specification"];
		$specification                 = Specification::model()->findByPk($specification);
		$spec["parent"]         = $specification["cat_id"];
		$spec["brand"]          = $specification["brand_id"];
		$spec["subcategory"]    = $specification["sub_cat_id"];
		$spec["specification"]  = $specification["specification"];
		$spec["unit"]           = $specification["unit"];
		$spec["company_id"]     = $specification["company_id"];
		$spec["type"]           = $specification["specification_type"];
		$spec["dieno"]          = $specification["dieno"];
		$spec["length"]         = $specification["length"];
		if (!empty($specification["hsn_code"])) {
			$hsn_code = $specification["hsn_code"];
		} else {
			$hsn_data = PurchaseCategory::model()->findByPk($specification["cat_id"]);
			$hsn_code = $hsn_data['hsn_code'];
		}
		$spec["hsn_code"]     = $specification["hsn_code"];

		$specDet                = json_encode($spec);
		echo $specDet;
	}
	public function actionUpdatespecification($id)
	{

		$model = $this->loadModel($id);
		//$this->performAjaxValidation1($model);
		//$model->unsetAttributes();


		if (isset($_REQUEST['data'])) {
			parse_str($_REQUEST['data'], $data);
			//$model->attributes=$_POST['PurchaseCategory'];
			$model->parent_id = $data['PurchaseCategory']['parent_id'];
			$model->specification = $data['PurchaseCategory']['specification'];
			$model->brand_id = $data['PurchaseCategory']['brand_id'];
			$model->unit = $data['PurchaseCategory']['unit'];
			$model->specification_type = $data['PurchaseCategory']['specification_type'];
			if ($data['PurchaseCategory']['specification_type'] == 'A') {
				$model->dieno = $data['PurchaseCategory']['dieno'];
				//$model->length = $data['PurchaseCategory']['length'];       
				$model->specification = $model->dieno;
			}
			$model->created_by = Yii::app()->user->id;
			$model->created_date =  date('Y-m-d');
			$model->company_id   = Yii::app()->user->company_id;
			if ($model->save()) {
				echo 1;
			}
		}
	}
	public function actionRemovespecification() {
        $spec_id = $_REQUEST["specification"];
        $tblpx = Yii::app()->db->tablePrefix;
        $model = Specification::model()->findByPk($spec_id);
        $material_count = $this->getMaterialCount($spec_id);
            
		$errorcount ="";
		$transaction = Yii::app()->db->beginTransaction();
		try {
			if ($material_count <= 0) {
				if (!$model->delete()) {
					$success_status = 0;
					throw new Exception(json_encode($model->getErrors()));
				} else {
					$success_status = 1;
				}
			}else{
				$success_status = 0;
				$errorcount ="1451";
			}
			$transaction->commit();
		} catch (Exception $error) {
			$transaction->rollBack();
			$success_status = 0;
		} finally {
			if ($success_status == 1) {
				// return $this->widget('zii.widgets.CListView', array(
				// 	'dataProvider' => $model->specificationsearch(Yii::app()->user->id),
				// 	'itemView' => '_specification', 'template' => '<div>{sorter}</div><div class="container1"><table id="spectable" cellpadding="10" class="table  list-view sorter">{items}</table></div>',
				// ));
			} else {
				if ((isset($error) && $error->errorInfo[1] == 1451) || $errorcount =="1451") {
					echo 2;
				} else {
					echo 1;
				}
			}
		}
    }

    public function getMaterialCount($spec_id) {
        $criteria = new CDbCriteria(array(
            'condition' => 'find_in_set(shutter_material_id,' . $spec_id . ') OR find_in_set(carcass_material_id,' . $spec_id . ') OR
        find_in_set(material_ids,' . $spec_id . ')'));
        $result = QuotationItemMaster::model()->findAll($criteria);
        return count($result);
        }

	public function actiongetHsnCode()
	{

		$hsn_code = $unit_list = '';
		$result = array();
		$item_id = $_REQUEST['id'];
		if (!empty($item_id)) {
			$item_details = PurchaseCategory::model()->findByPk($item_id);

			$unitlistsql = "SELECT * FROM " . $this->tableNameAcc('unit_conversion', 0) . " LEFT JOIN " . $this->tableNameAcc('unit', 0) . " ON " . $this->tableNameAcc('unit_conversion', 0) . ".conversion_unit=" . $this->tableNameAcc('unit', 0) . ".unit_name WHERE " . $this->tableNameAcc('unit_conversion', 0) . ".item_id=" . $item_id;
			$unit_list = Yii::app()->db->createCommand($unitlistsql)->queryAll();

			if (!empty($item_details['hsn_code'])) {
				$hsn_code = $item_details['hsn_code'];
			} else {

				$item_parent = PurchaseCategory::model()->findByPk($item_details['parent_id']);

				if (!empty($item_parent['hsn_code']))
					$hsn_code = $item_parent['hsn_code'];
			}
			$result = array('hsn_code' => $hsn_code, 'unit_title' => $unit_list, 'baseunit' => $item_details['unit']);
		}
		echo json_encode($result);
	}

	public function GetItemCategory($parent = 0, $spacing = '', $user_tree_array = '')
	{
		if (!is_array($user_tree_array))
			$user_tree_array = array();
		$data = array();
		$final = array();
		$tblpx = Yii::app()->db->tablePrefix;
		$spec_sql = "SELECT id, specification, unit, brand_id, cat_id "
			. " FROM {$tblpx}specification  "
			. " WHERE spec_status = '1'"
			. " ORDER BY created_date DESC";
		$specification = Yii::app()->db->createCommand($spec_sql)->queryAll();
		foreach ($specification as $key => $value) {
			if ($value['brand_id'] != NULL) {
				$brand_details = Yii::app()->db->createCommand("SELECT brand_name FROM {$tblpx}brand WHERE id=" . $value['brand_id'] . "")->queryRow();
				$brand = '-' . ucfirst($brand_details['brand_name']);
			} else {
				$brand = '';
			}
			if ($value['unit'] != NULL) {
				$unit = '-' . $value['unit'];
			} else {
				$unit = '';
			}

			if ($value['cat_id'] != '') {
				$result = $this->GetParent($value['cat_id']);

				$final[$key]['data'] = ucfirst($result['category_name']) . $brand . '-' . ucfirst($value['specification']);
			} else {

				$final[$key]['data'] = $brand . '-' . $value['specification'] . ' - ' . $unit . "(unit)";
			}


			$final[$key]['id'] = $value['id'];
		}
		return $final;
	}

	public function GetParent($id)
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$category = Yii::app()->db->createCommand("SELECT parent_id, id , category_name FROM {$tblpx}purchase_category WHERE id=" . $id . " ORDER BY created_date DESC")->queryRow();

		return $category;
	}

	public function actiongetUnit()
	{
		$purchase_itemid                = $_REQUEST["purchase_itemid"];
		$conversion_unit_id             = isset($_REQUEST["conversion_unit_id"]) ? $_REQUEST["conversion_unit_id"] : '';
		$ItemUnit_name                      = Specification::model()->findByPk($purchase_itemid);
		$ItemUnit = Unit::model()->findByAttributes(array('unit_name' => $ItemUnit_name['unit']));

		$purchaseItemUnit["Unit_id"]    = $ItemUnit["id"];
		$purchaseItemUnit["Unit_name"]  = $this->GetItemunit($ItemUnit["id"]);
		$purchaseItemUnit["Unit_list"]  = $this->GetItemunitlist($ItemUnit["id"], $purchase_itemid);
		if ($conversion_unit_id != '') {
			$conversion_unit_name =  $this->GetItemunit($conversion_unit_id);
			$conversion_unit_arr = ['id' => $conversion_unit_id, 'value' => $conversion_unit_name];
			array_push($purchaseItemUnit["Unit_list"], $conversion_unit_arr);
		}
		$purchaseItemUnit_dis           = json_encode($purchaseItemUnit);
		echo $purchaseItemUnit_dis;
	}
	public function GetItemunit($unit_id)
	{
		$unit_id            = $unit_id;
		$ItemUnit           = Unit::model()->findByPk($unit_id);
		$ItemUnit_name      = $ItemUnit["unit_name"];
		return $ItemUnit_name;
	}
	public function GetItemunitlist($unit_id, $purchase_itemid)
	{
		$tblpx = Yii::app()->db->tablePrefix;
		$unit_id            = $unit_id;
		$purchase_itemid    = $purchase_itemid;
		$final_list = array();
		$uc_item_units = array();
		$uc_item_count                  = UnitConversion::model()->countByAttributes(array(
			'item_id' => $purchase_itemid,
			'conversion_unit' => $unit_id,
		));
		$uc_item_units_sql = "SELECT conversion_unit FROM {$tblpx}unit_conversion WHERE item_id ='" . $purchase_itemid . "'";
		$uc_item_units_arr                  = Yii::app()->db->createCommand($uc_item_units_sql)->queryAll();
		foreach ($uc_item_units_arr as $uc_item_units_value) {
			$uc_item_units[] = $uc_item_units_value['conversion_unit'];
		}
		if ($uc_item_count == 0) {
			$ItemUnitlist           = Unit::model()->findAll(
				array("order" => "id")
			);
		} else {
			$ItemUnitlist           = Unit::model()->findAll(
				array("condition" => "id !=  $unit_id", "order" => "id")
			);
		}

		$final_list = array();
		$tblpx = Yii::app()->db->tablePrefix;
		foreach ($ItemUnitlist as $key => $value) {
			if (!in_array($value['id'], $uc_item_units)) {
				$final_list[$key]['id'] = $value['id'];
				$final_list[$key]['value'] = $value['unit_name'];
			}
		}

		return $final_list;
	}

	public function actionCheckEditUnitconversion()
	{


		$tblpx = Yii::app()->db->tablePrefix;
		$unitconversion_checkSql = '';
		$data = isset($_REQUEST['data']) ? $_REQUEST['data'] : "";
		if ($data != "") {
			if (isset($data['unitId']) && !empty($data['unitId'])) {
				$unitId = $data['unitId'];
			} else {
				$unitId = "";
			}
			if (isset($data['item_id']) && !empty($data['item_id'])) {
				$item_id = $data['item_id'];
			} else {
				$item_id = "";
			}
			if (isset($data['base_unit_id']) && !empty($data['base_unit_id'])) {
				$base_unit_id = $data['base_unit_id'];
			} else {
				$base_unit_id = "";
			}
			if (isset($data['conversion_unit_id']) && !empty($data['conversion_unit_id'])) {
				$conversion_unit_id = trim($data['conversion_unit_id']);
			} else {
				$conversion_unit_id = "";
			}
		} else {
			$unitId = '';
			$item_id = '';
			$base_unit_id = '';
			$conversion_unit_id = '';
		}

		if ($unitId != '' && $unitId != NULL) {
			$warehouseReceipt_conArray  = array('condition' => 'warehousereceipt_unitConversion_id = ' . $unitId);
			$warehouseReceipt = WarehousereceiptItems::model()->findAll($warehouseReceipt_conArray);
			$warehouseReceiptCount = count($warehouseReceipt);
			$warehouseDespatch_conArray  = array('condition' => 'warehousedespatch_unitConversion_id = ' . $unitId);
			$warehouseDespatch = WarehousedespatchItems::model()->findAll($warehouseDespatch_conArray);
			$warehouseDespatchCount = count($warehouseDespatch);
			if ($warehouseDespatchCount == 0 && $warehouseReceiptCount == 0) {
				echo json_encode(array('response' => 'success'));
			} else {
				echo json_encode(array('response' => 'error', 'msg' => 'This Item is not editable.Because it is already in use'));
			}
		} else {
			echo json_encode(array('response' => 'error', 'msg' => 'Some Problem Occured'));
		}
	}
        
    public function actionPurchasereport() {
        $tblpx = Yii::app()->db->tablePrefix;
        $project_id = "";
        $model = new Projects;
        $dataProvider = $model->search();
        $itemmodelcat = "";
        $dataProvider1 = "";
        $date_from = "";
        $date_to = "";
        if (isset($_REQUEST['project_id']) && !empty($_REQUEST['project_id'])) {
            $project_id = $_REQUEST['project_id'];
            $sqlProject = "SELECT pr.*,cl.name as clientname "
                    . "FROM {$tblpx}projects pr LEFT JOIN {$tblpx}clients cl "
                    . "ON pr.client_id = cl.cid WHERE pr.pid = {$project_id}";
            $model = Yii::app()->db->createCommand($sqlProject)->queryRow();
            $dataProvider = "";
            $date_from = isset($_REQUEST['date_from']) && !empty($_REQUEST['date_from']) ? date('Y-m-d', strtotime($_REQUEST['date_from'])) : "";
            $date_to = isset($_REQUEST['date_to']) && !empty($_REQUEST['date_to']) ? date('Y-m-d', strtotime($_REQUEST['date_to'])) : "";
            if (isset($date_from) && !empty($date_from) && isset($date_to) && !empty($date_to)) {
                $condition = " AND (bl.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
            } else if (isset($date_from) && empty($date_from) && isset($date_to) && !empty($date_to)) {
                $condition = " AND bl.bill_date <= '" . $date_to . "'";
            } else if (isset($date_from) && !empty($date_from) && isset($date_to) && empty($date_to)) {
                $condition = " AND bl.bill_date >= '" . $date_from . "'";
            } else {
                $condition = "";
            }
            $sqlQueryCat = "SELECT pc.id,pc.category_name,a.billitemsum, a.itemtotalamount "
                    . " FROM {$tblpx}purchase_category pc"
                    . " LEFT JOIN (SELECT SUM(bi.billitem_quantity) as billitemsum, "
                    . " SUM((bi.billitem_amount - bi.billitem_discountamount) + bi.billitem_taxamount) "
                    . " as itemtotalamount, pc2.id as parentid FROM {$tblpx}billitem bi"
                    . " LEFT JOIN {$tblpx}specification pc1 ON bi.category_id = pc1.id"
                    . " LEFT JOIN {$tblpx}purchase_category pc2 ON pc1.cat_id = pc2.id"
                    . " LEFT JOIN {$tblpx}bills bl ON bi.bill_id = bl.bill_id"
                    . " LEFT JOIN {$tblpx}purchase pu ON bl.purchase_id = pu.p_id "
                    . " WHERE pu.project_id = {$project_id}" . $condition . " GROUP BY pc2.id) a "
                    . " ON a.parentid = pc.id"
                    . " WHERE a.billitemsum != 0 GROUP BY pc.id";
				//	die($sqlQueryCat);
            $itemmodelcat = Yii::app()->db->createCommand($sqlQueryCat)->queryAll();
            $catCount = count($sqlQueryCat);
            $dataProvider1 = new CSqlDataProvider($sqlQueryCat, array(
                'totalItemCount' => $catCount,
                'keyField' => 'billitem_id',
                'id' => 'billitem_id',
                'sort' => array(
                    'attributes' => array(
                        'billitem_id', 'bill_id',
                    ),
                ),
                'pagination' => array(
                    'pageSize' => 10,
                ),
            ));
        }
		$render_datas = array(
            'model' => $model,
            'dataProvider' => $dataProvider,
            'project_id' => $project_id,
            'itemmodelcat' => $itemmodelcat,
            'dataProvider1' => $dataProvider1,
            'date_from' => $date_from,
            'date_to' => $date_to,
        );

		if (filter_input(INPUT_GET, 'export') == 'pdf') {
			$this->logo = $this->realpath_logo;
            $mPDF1 = Yii::app()->ePdf->mPDF();
            $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
            $sql = 'SELECT template_name '
                . 'FROM jp_quotation_template '
                . 'WHERE status="1"';
            $selectedtemplate =Yii::app()->db->createCommand($sql)->queryScalar();
            
            $template = $this->getTemplate($selectedtemplate);
            $mPDF1->SetHTMLHeader($template['header']);
            $mPDF1->SetHTMLFooter($template['footer']);
           // $mPDF1->AddPage('','', '', '', '', 0,0,52, 30, 10,0);
		   $mPDF1->AddPage(
        '', // Orientation
        '', '', '', '', // Format and other settings
        15, // Left margin
        15, // Right margin
        35, // Top margin
        30, // Bottom margin
        10, // Header margin
        0   // Footer margin
    );
			

            $mPDF1->WriteHTML($this->renderPartial('purchasereport', $render_datas, true));
            
                $mPDF1->Output('PURCHASE-REPORT.pdf', 'D');
            
        } elseif (filter_input(INPUT_GET, 'export') == 'csv') {
            $exported_data = $this->renderPartial('purchasereport', $render_datas, true);
            $export_filename = 'PURCHASE-REPOR' . date('Ymd_His');
            ExportHelper::exportGridAsCSV($exported_data, $export_filename);
        } else {
            $this->render('purchasereport', $render_datas);
        }
        
    }
	public function actionPurchasereportdetails(){
		$project_id = $_REQUEST["project_id"];
        $category_id = $_REQUEST["category_id"];
		$tblpx = Yii::app()->db->tablePrefix;
		$sqlCategoryDetails = "SELECT  pc2.id as parentid,pc2.category_name,bi.bill_id,bi.purchaseitem_id,bi.billitem_unit,bi.billitem_quantity,bi.warehouse_id,bi.purchaseitem_unit,bi.created_date as billdate,bi.billitem_hsn_code,bi.billitem_description,pu.purchase_no,bl.bill_number,bl.bill_date,bl.bill_totalamount,(bi.billitem_amount - bi.billitem_discountamount) + bi.billitem_taxamount as itemtotalamount,bi.billitem_id FROM jp_billitem bi LEFT JOIN jp_specification pc1 ON bi.category_id = pc1.id LEFT JOIN jp_purchase_category pc2 ON pc1.cat_id = pc2.id LEFT JOIN jp_bills bl ON bi.bill_id = bl.bill_id LEFT JOIN jp_purchase pu ON bl.purchase_id = pu.p_id WHERE pu.project_id = $project_id AND pc2.id=$category_id";
		$CategoryDetails = Yii::app()->db->createCommand($sqlCategoryDetails)->queryAll();
		//die($sqlCategoryDetails);
		//echo "<pre>";print_r($CategoryDetails);exit;
		$this->render("category_details",array('details'=>$CategoryDetails));
	}

    public function actionExportPurchaseReportCSV() {
        $project_id = $_REQUEST["project_id"];
        $date_from = $_REQUEST["fromdate"];
        $date_to = $_REQUEST["todate"];
        $tblpx = Yii::app()->db->tablePrefix;
        $sqlProject = "SELECT pr.*,cl.name as clientname FROM {$tblpx}projects pr "
                . "LEFT JOIN {$tblpx}clients cl ON pr.client_id = cl.cid "
                . "WHERE pr.pid = {$project_id}";
        $model = Yii::app()->db->createCommand($sqlProject)->queryRow();
        if (isset($date_from) && !empty($date_from) && isset($date_to) && !empty($date_to)) {
            $condition = " AND (bl.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
        } else if (isset($date_from) && empty($date_from) && isset($date_to) && !empty($date_to)) {
            $condition = " AND bl.bill_date <= '" . $date_to . "'";
        } else if (isset($date_from) && !empty($date_from) && isset($date_to) && empty($date_to)) {
            $condition = " AND bl.bill_date >= '" . $date_from . "'";
        } else {
            $condition = "";
        }
        $sqlQueryCat = "SELECT pc.id,pc.category_name,a.billitemsum, a.itemtotalamount "
                . " FROM {$tblpx}purchase_category pc"
                . " LEFT JOIN (SELECT SUM(bi.billitem_quantity) as billitemsum, "
                . " SUM((bi.billitem_amount - bi.billitem_discountamount) + bi.billitem_taxamount) "
                . "as itemtotalamount, pc3.id as parentid FROM {$tblpx}billitem bi"
                . " LEFT JOIN {$tblpx}purchase_category pc1 ON bi.category_id = pc1.id"
                . " LEFT JOIN {$tblpx}purchase_category pc2 ON pc1.parent_id = pc2.id"
                . " LEFT JOIN {$tblpx}purchase_category pc3 ON pc2.parent_id = pc3.id"
                . " LEFT JOIN {$tblpx}bills bl ON bi.bill_id = bl.bill_id"
                . " LEFT JOIN {$tblpx}purchase pu ON bl.purchase_id = pu.p_id "
                . " WHERE pu.project_id = {$project_id}" . $condition
                . " GROUP BY pc3.id) a ON a.parentid = pc.id"
                . " WHERE pc.parent_id IS NULL AND a.billitemsum != 0 GROUP BY pc.id";
        $itemmodelcat = Yii::app()->db->createCommand($sqlQueryCat)->queryAll();
        $arraylabel = array("", "");

        $finaldata[0][] = "Client";
        $finaldata[0][] = $model["clientname"];
        $finaldata[1][] = "Project Name";
        $finaldata[1][] = $model["name"];
        $finaldata[2][] = "Site";
        $finaldata[2][] = $model["site"];

        $finaldata[3][] = "";
        $finaldata[3][] = "";
        $finaldata[3][] = "";
        $finaldata[3][] = "";

        $finaldata[4][] = "Sl No";
        $finaldata[4][] = "Category";
        $finaldata[4][] = "Quantity";
        $finaldata[3][] = "Amount";

        $k = 5;
        $i = 1;
        $quantitySum = 0;
        $amountSum = 0;
        foreach ($itemmodelcat as $modelcat) {
            $quantitySum = $quantitySum + $modelcat["billitemsum"];
            $amountSum = $amountSum + $modelcat["itemtotalamount"];
            $finaldata[$k][] = $i;
            $finaldata[$k][] = $modelcat["category_name"];
            $finaldata[$k][] = Controller::money_format_inr($modelcat["billitemsum"], 2);
            $finaldata[$k][] = Controller::money_format_inr($modelcat["itemtotalamount"], 2);
            $k = $k + 1;
            $i = $i + 1;
        }
        $finaldata[$k][] = " ";
        $finaldata[$k][] = " ";
        $finaldata[$k][] = Controller::money_format_inr($quantitySum, 2);
        $finaldata[$k][] = Controller::money_format_inr($amountSum, 2);

        Yii::import('ext.ECSVExport');
        $csv = new ECSVExport($finaldata);
        $csv->setHeaders($arraylabel);
        $csv->setToAppend();
        $contentdata = $csv->toCSV();
        $csvfilename = 'Purchase Report' . date('d_M_Y') . '.csv';
        Yii::app()->getRequest()->sendFile($csvfilename, $contentdata, "text/csv", false);
        exit();
    }

    public function actionSavepurchasereportpdf() {
        $tblpx = Yii::app()->db->tablePrefix;
        $project_id = $_REQUEST['project_id'];
        $sqlProject = "SELECT pr.*,cl.name as clientname FROM {$tblpx}projects pr "
                . "LEFT JOIN {$tblpx}clients cl ON pr.client_id = cl.cid WHERE pr.pid = {$project_id}";
        $model = Yii::app()->db->createCommand($sqlProject)->queryRow();
        $dataProvider = "";
        $date_from = isset($_POST['date_from']) && !empty($_POST['date_from']) ? date('Y-m-d', strtotime($_POST['date_from'])) : "";
        $date_to = isset($_POST['date_to']) && !empty($_POST['date_to']) ? date('Y-m-d', strtotime($_POST['date_to'])) : "";
        if (isset($date_from) && !empty($date_from) && isset($date_to) && !empty($date_to)) {
            $condition = " AND (bl.bill_date BETWEEN '" . $date_from . "' AND '" . $date_to . "')";
        } else if (isset($date_from) && empty($date_from) && isset($date_to) && !empty($date_to)) {
            $condition = " AND bl.bill_date <= '" . $date_to . "'";
        } else if (isset($date_from) && !empty($date_from) && isset($date_to) && empty($date_to)) {
            $condition = " AND bl.bill_date >= '" . $date_from . "'";
        } else {
            $condition = "";
        }
        $sqlQueryCat = "SELECT pc.id,pc.category_name,a.billitemsum, a.itemtotalamount "
                . " FROM {$tblpx}purchase_category pc"
                . " LEFT JOIN (SELECT SUM(bi.billitem_quantity) as billitemsum, "
                . "SUM((bi.billitem_amount - bi.billitem_discountamount) + bi.billitem_taxamount) "
                . "as itemtotalamount, pc3.id as parentid FROM {$tblpx}billitem bi"
                . " LEFT JOIN {$tblpx}purchase_category pc1 ON bi.category_id = pc1.id"
                . " LEFT JOIN {$tblpx}purchase_category pc2 ON pc1.parent_id = pc2.id"
                . " LEFT JOIN {$tblpx}purchase_category pc3 ON pc2.parent_id = pc3.id"
                . " LEFT JOIN {$tblpx}bills bl ON bi.bill_id = bl.bill_id"
                . " LEFT JOIN {$tblpx}purchase pu ON bl.purchase_id = pu.p_id "
                . " WHERE pu.project_id = {$project_id}" . $condition
                . " GROUP BY pc3.id) a ON a.parentid = pc.id"
                . " WHERE pc.parent_id IS NULL AND a.billitemsum != 0 GROUP BY pc.id";
        $itemmodelcat = Yii::app()->db->createCommand($sqlQueryCat)->queryAll();
        $catCount = count($sqlQueryCat);
        $dataProvider1 = new CSqlDataProvider($sqlQueryCat, array(
            'totalItemCount' => $catCount,
            'keyField' => 'billitem_id',
            'id' => 'billitem_id',
            'sort' => array(
                'attributes' => array(
                    'billitem_id', 'bill_id',
                ),
            ),
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));
        $mPDF1 = Yii::app()->ePdf->mPDF();
        $mPDF1 = Yii::app()->ePdf->mPDF('', 'A4');
        $mPDF1->WriteHTML($this->renderPartial('savepurchasereport', array(
                    'model' => $model,
                    'dataProvider' => $dataProvider,
                    'project_id' => $project_id,
                    'itemmodelcat' => $itemmodelcat,
                    'dataProvider1' => $dataProvider1,
                    'date_from' => $date_from,
                    'date_to' => $date_to,
                        ), true));
        $mPDF1->Output('Purchase Report' . '.pdf', 'D');
    }

	public function actiongetSpecificationDetails(){

		$data = $_POST['PurchaseSpecification'];
		Yii::app()->session['cat_id']= $data['cat_id'];
		Yii::app()->session['brand_id']= $data['brand_id'];		
		Yii::app()->session['dieno']= $data['dieno'];
		Yii::app()->session['unit']= $data['unit'];
		Yii::app()->session['hsn_code']= $data['hsn_code'];
		Yii::app()->session['company_id']= isset($data['company_id'])?$data['company_id']:"";
	}

    public function actionremovecategory() {

        $id = $_POST['catId'];
        $model = $this->loadModel($id);
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if (!$model->delete()) {
                $success_status = 0;
                throw new Exception(json_encode($model->getErrors()));
            } else {
                $success_status = 1;
            }
            $transaction->commit();
        } catch (Exception $error) {
            $transaction->rollBack();
            $success_status = 0;
        } finally {
            if ($success_status == 1) {
                echo json_encode(array('response' => 'success', 'msg' => 'Data Deleted Successfully'));
            } else {
                if ($error->errorInfo[1] == 1451) {
                    echo json_encode(array('response' => 'warning', 'msg' => 'Cannot Delete ! This record alredy in use.'));
                } else {
                    echo json_encode(array('response' => 'error', 'msg' => 'Some Error Occured'));
                }
            }
        }
    }

	public function actioncheckDuplicateSpecification(){
		$where ='';
		if(isset($_POST['subcategory']) && $_POST['subcategory'] !=''){
			$subcategory =$_POST['subcategory'];
			$where .= " AND `sub_cat_id` = $subcategory";
		}else{
			$subcategory = '';
			$where .= " AND `sub_cat_id` IS NULL";
		}
		$sql = "SELECT count(*) FROM `jp_specification` "
			. " WHERE `cat_id` = ".$_POST['category']
			. " AND `brand_id` =". $_POST['brand']
			. $where
			. " AND `specification` LIKE '".$_POST['specification']."'";	
	
		$existing_spec_count = Yii::app()->db->createCommand($sql)->queryScalar();
		echo $existing_spec_count;
		
	}


	public function actionImportcsv() {

		if (isset($_GET['pageSize'])) {	  
            Yii::app()->user->setState('pageSizeImport', (int) $_GET['pageSize']);
            unset($_GET['pageSize']);
        }
       
        $tbl = Yii::app()->db->tablePrefix;  		      
       	$model=new ImportForm() ;
		$model2=new PurchaseItemsImport('search');
		$model2->unsetAttributes(); 

		if(isset($_GET['PurchaseItemsImport'])){
            $model2->attributes=$_GET['PurchaseItemsImport'];		
		}
		
		if(isset($_POST['ImportForm'])){                 
            $model->attributes=$_POST['ImportForm']; 
			if($model->validate()){
				$csvFile=CUploadedFile::getInstance($model,'file');  
                $tempLoc=$csvFile->getTempName();
                try{
					$transaction = Yii::app()->db->beginTransaction();
                    $handle 	 = fopen("$tempLoc", "r");
                    $row 		 = 1;
					
                    while (($data = fgetcsv($handle, 0, ",", '"')) !== FALSE) {  
						
                        if($row>1){ 
                            $newmodel 			 = new PurchaseItemsImport();                                  
                            $newmodel->main_category 	 = $data[0];
                            $newmodel->sub_category 	 = isset($data[1])?$data[1]:"";
                            $newmodel->brand 	 = $data[2];       
                            $newmodel->specification_type = $data[3];
                            $newmodel->specification	 = $data[4];
                            $newmodel->unit 	 = $data[5];
                            $newmodel->hsn_code 	 = isset($data[6])?$data[6] : "";
                            $newmodel->company 	 = $data[7];
                            $newmodel->created_by 	= Yii::app()->user->id;
                            $newmodel->created_on 	= date('Y-m-d');
                            $newmodel->save();								                              
						}
                        $row++;              
                    }
                    $transaction->commit();
                }catch(Exception $error){
					echo "<pre>";
                    print_r($error);exit;
                    $transaction->rollback();
                } 
            }
            Yii::app()->user->setFlash('success', "Item Imported Successfully!");
						$this->redirect(array('purchaseCategory/newlist'));
        }  
        
        $this->render("importpurchaseitems",array('model'=>$model,'model2'=>$model2));
       
    }

	public function actionDownloadsample()
    {
        $doc = "sample-item.csv";
        $path = Yii::getPathOfAlias('webroot') . '/uploads/';
        $file = $path . '' . $doc;
        if (file_exists($file)) {
            Yii::app()->getRequest()->sendFile($doc, file_get_contents($file));
        }
    }

	public function actionsaveItems(){
		$model=new ImportForm() ;
		$model2=new PurchaseItemsImport('search');
		if (isset($_POST['selectedIds'])){
			foreach ($_POST['selectedIds'] as $id){
				$data = PurchaseItemsImport::model()->findByPk($id);              
				$tempstatus = array();                        
				$newmodel = new Specification();
				$newmodel->setscenario("save");	

				$main_category = addslashes($data->main_category);
				$category = PurchaseCategory::model()->find(array('condition' => "category_name = '$main_category'"));
				if($category){
					$newmodel->cat_id = $category->id; 
					
				}else{
					$data->main_cat_exist = '0';
					$st = "Category not exists!";
					array_push($tempstatus, $st);
				}
				$sub_category = addslashes($data->sub_category);
				$subcategory = PurchaseSubCategory::model()->find(array('condition' => "sub_category_name = '$sub_category'"));
				if(empty($subcategory)){						
					$data->sub_cat_exist = '0';
					$sub_id=0;
				}else{
					$subcat_id = $subcategory->id;
					$newmodel->sub_cat_id = $subcat_id;
					$sub_id=1;
				}                           

				$temp_brand = addslashes($data->brand);
				$brand = Brand::model()->find(array('condition' => "brand_name = '$temp_brand'"));
				if($brand){
					$newmodel->brand_id = $brand->id;                            
				}else{
					$data->brand_exist = '0';
					$st = "Category not exists!";
					array_push($tempstatus, $st);
				}
				$where = '';
				if(!empty($category) && !empty($brand) ){
					if(!empty($subcategory)){
						$where = " AND `sub_cat_id` = $subcategory->id";
					}
					$sql = "SELECT count(*) FROM `jp_specification` "
					. " WHERE `cat_id` = ".$category->id
					. " AND `brand_id` =". $brand->id .$where
					. " AND `specification` LIKE '".$data->specification."'";	
			
					$existing_spec_count = Yii::app()->db->createCommand($sql)->queryScalar();
					if($existing_spec_count>0){
						$data->spec_exist = '1';
						$st = "Specification Alredy exists!";
						array_push($tempstatus, $st);
						if(!empty($sub_category)  && $sub_id ==0){
							$data->spec_exist = '0';
							$st = "Specification Alredy exists!";
							array_push($tempstatus, $st);
						}						
					}				
				}
				$type = rtrim($data->specification_type);
				if($type == 'By Length'){
					$specification_type ='A';
					$newmodel->dieno	 = $data->specification;
				}else if($type == 'By Quantity'){
					$specification_type ='O';
				}else{
					$specification_type ='G';
				}
				$companyName = trim($data->company);
				$companyInfo = Company::model()->find("name = :companyName", array(":companyName" => $companyName ));
				$companyId=0;
				if(!empty($companyInfo)){
					$companyId=$companyInfo->id;
					$data->company_exist ='1';
				}else{
					$data->company_exist ='0';
					$st = "Company Not exists!";
					array_push($tempstatus, $st);
				}

                $newmodel->specification_type = $specification_type;				
                $newmodel->specification	 = $data->specification;				
                $newmodel->unit 	 = $data->unit;
                $newmodel->hsn_code 	 = $data->hsn_code;
                $newmodel->company_id 	 = $companyId;
                $newmodel->created_by 	= Yii::app()->user->id;
                $newmodel->created_date 	= date('Y-m-d');
				if(empty($tempstatus)){
					if($newmodel->save()){
						Yii::app()->user->setFlash('success', "Item Created");
						PurchaseItemsImport::model()->findByPk($id)->delete();
					}else{
						$errors = $newmodel->getErrors();
						//echo "<pre>";print_r($errors);exit;
						Yii::app()->user->setFlash('error', "Error Occurred!");
					}
				}
				$data->save();

			}			                   
		}
		
		$this->redirect(array('purchaseCategory/newlist'));
	}

	public function actioncreatenewcategory(){
		$model = new PurchaseCategory;
		$this->performAjaxValidation($model);
		$model->unsetAttributes();
		$model->parent_id =  NULL;			
		$model->category_name 	 = $_POST['category'];
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$company = implode(",", $arrVal);
		$model->company_id = $company;
		$model->spec_flag = 'N';
		$model->type = 'C';
		$model->created_by = Yii::app()->user->id;
		$model->created_date =  date('Y-m-d');
		if($model->save()){
			$condition = "main_category = '$model->category_name'";
			$purchaseItemsImport = PurchaseItemsImport::model()->findAll($condition);
			if(!empty($purchaseItemsImport)){
				foreach($purchaseItemsImport as $purchaseItemsImports){
					$purchaseItemsImports->main_cat_exist='1';
					$purchaseItemsImports->save();
				}
			}
			echo "success";
		}else{
			echo "<pre>";
			print_r($model->getErrors());exit;
		}
	}

	public function actioncreatenewbrand(){
		$model = new Brand;
		$this->performAjaxValidation($model);
		$model->unsetAttributes();
		
		$model->brand_name 	 = $_POST['brand'];
		$user = Users::model()->findByPk(Yii::app()->user->id);
		$arrVal = explode(',', $user->company_id);
		$company = implode(",", $arrVal);
		$model->company_id = $company;		
		$model->created_by = Yii::app()->user->id;
		$model->created_date =  date('Y-m-d');
		if($model->save()){
			$purchaseItemsImport = PurchaseItemsImport::model()->find(array('condition' => "brand = '$model->brand_name'"));
			$purchaseItemsImport->brand_exist='1';
			$purchaseItemsImport->save();
			echo "success";
		}else{
			echo "<pre>";
			print_r($model->getErrors());exit;
		}
	}

	public function actioncreatenewsubcategory(){
		$model = new PurchaseSubCategory;
		$this->performAjaxValidation2($model);
		$model->unsetAttributes();
		$sub_category_check='';
		$parentcat = $_POST['parent_category'];
		if(isset($_POST['category']) && $_POST['category'] !=""){
			$sub_cat_check = $_POST['category'];
			$sub_category_check = PurchaseSubCategory::model()->find(array('condition' => "sub_category_name = '$sub_cat_check'"));	
								
		}
		if(empty($sub_category_check )){			
			$model->sub_category_name 	 = $_POST['category'];
			$user = Users::model()->findByPk(Yii::app()->user->id);
			$arrVal = explode(',', $user->company_id);
			$company = implode(",", $arrVal);
			$model->company_id = $company;
			$model->created_by = Yii::app()->user->id;
			$model->created_date =  date('Y-m-d');
			if($model->save()){
				$criteria = new CDbCriteria;
                $criteria->condition = 'sub_category = :sub_category';
                $criteria->params = array(':sub_category' => $_POST['category']);
                $sub_category_check = PurchaseItemsImport::model()->findAll($criteria);
				
				if(!empty($sub_category_check)){
					foreach($sub_category_check as $sub_category_checks){
						$sub_category_checks->sub_cat_exist='1';
						$sub_category_checks->save();
					}
				}
				echo json_encode(array('response' => 'success'));
			}else{
				echo json_encode(array('response' => 'error'));
			}
		}else{
				echo json_encode(array(
					'response' => 'warning',
					'sub_cat_check'=>$sub_cat_check,
					'parentcat'=>$parentcat));
				
		}
		
		
		
	}

	public function actionupdateImport(){
		$id = $_REQUEST['id'];
		$model=PurchaseItemsImport::model()->findByPk($id);
		
        if(isset($_POST['PurchaseItemsImport']))
		{
			$model->attributes=$_POST['PurchaseItemsImport'];
			$newcategory = addslashes($_POST['PurchaseItemsImport']['main_category']);
			$maincategory = PurchaseCategory::model()->find(array('condition' => "category_name = '$newcategory'"));
			if($maincategory){
				$model->main_cat_exist = '1';					
			}

			$newbrand = addslashes($_POST['PurchaseItemsImport']['brand']);
			$brand = Brand::model()->find(array('condition' => "brand_name = '$newbrand'"));
			if($brand){
				$model->brand_exist = '1';					
			}
			
			$newsubcat = addslashes($_POST['PurchaseItemsImport']['sub_category']);
			$subcategory = PurchaseSubCategory::model()->find(array('condition' => "sub_category_name = '$newsubcat'"));
			if($subcategory){
				$model->sub_cat_exist = '1';					
			}


			if(isset($maincategory->id) && isset($brand->id)){
				$sql = "SELECT count(*) FROM `jp_specification` "
						. " WHERE `cat_id` = ".$maincategory->id
						. " AND `brand_id` =". $brand->id
						. " AND `specification` LIKE '".$_POST['PurchaseItemsImport']['specification']."'";	
				
				$existing_spec_count = Yii::app()->db->createCommand($sql)->queryScalar();
				if($existing_spec_count == 0){
					$model->spec_exist = '0';
				}else{
					$model->spec_exist = '1';
				}
			}
			$newcompany = addslashes($_POST["PurchaseItemsImport"]["company"]);
			$companyInfo = Company::model()->find("name = :companyName", array(":companyName" => $newcompany ));
			if(!empty($companyInfo)){
				$model->company_exist = '1';
			}
			
			$model->updated_by = Yii::app()->user->id;
            $model->updated_date = date('Y-m-d');
            
			if($model->save())
			$this->redirect(array('purchaseCategory/newlist'));
        }else{
			$this->layout=false;
			$this->render('updateImport',array( 'model'=>$model));
		}
        
	}

	public function actiondeleteimports(){
		if (isset($_POST['ids'])) { 
			foreach ($_POST['ids'] as $id) { 
				PurchaseItemsImport::model()->findByPk($id)->delete();
			}			  
			echo json_encode(array('response' => 'success'));
			
		}else{
			echo json_encode(array('response' => 'error')); 	  
		}	  
	}

}
