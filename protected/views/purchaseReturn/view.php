<?php
/* @var $this PurchaseReturnController */
/* @var $model PurchaseReturn */

$this->breadcrumbs=array(
	'Purchase Returns'=>array('index'),
	$model->return_id,
);

$this->menu=array(
	array('label'=>'List PurchaseReturn', 'url'=>array('index')),
	array('label'=>'Create PurchaseReturn', 'url'=>array('create')),
	array('label'=>'Update PurchaseReturn', 'url'=>array('update', 'id'=>$model->return_id)),
	array('label'=>'Delete PurchaseReturn', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->return_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PurchaseReturn', 'url'=>array('admin')),
);
?>

<h1>View PurchaseReturn #<?php echo $model->return_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'return_id',
		'bill_id',
		'return_number',
		'return_date',
		'return_amount',
		'return_taxamount',
		'return_discountamount',
		'return_totalamount',
		'company_id',
		'created_by',
		'created_date',
		'updated_date',
		'return_status',
	),
)); ?>
