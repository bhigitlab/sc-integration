<?php

/**
 * This is the model class for table "{{warehousereceipt}}".
 *
 * The followings are the available columns in table '{{warehousereceipt}}':
 * @property integer $warehousereceipt_id
 * @property integer $warehousereceipt_no
 * @property string $warehousereceipt_date
 * @property integer $warehousereceipt_remark
 * @property integer $warehousereceipt_warehouseid
 * @property integer $warehousereceipt_despatch_id
 * @property integer $warehousereceipt_clerk
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class Warehousereceipt extends CActiveRecord {

    public $date_from;
    public $date_to;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{warehousereceipt}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('warehousereceipt_no, warehousereceipt_date, warehousereceipt_remark, warehousereceipt_warehouseid, warehousereceipt_clerk, created_by, created_date, updated_by, updated_date', 'required'),
            array('warehousereceipt_warehouseid,warehousereceipt_despatch_id, warehousereceipt_clerk, created_by, updated_by', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('warehousereceipt_id,warehousereceipt_warehouseid_from,warehousereceipt_bill_id, warehousereceipt_no, warehousereceipt_date, warehousereceipt_remark, warehousereceipt_warehouseid, warehousereceipt_clerk, created_by, created_date, updated_by, updated_date', 'warehousereceipt_despatch_id','warehousereceipt_transfer_type', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'warehouse' => array(self::BELONGS_TO, 'Warehouse', 'warehousereceipt_warehouseid'),
            // 'vendor' => array(self::BELONGS_TO, 'Vendors', 'warehousereceipt_vendor'),
            'clerk' => array(self::BELONGS_TO, 'Users', 'warehousereceipt_clerk'),
            'warehousereceiptwarehouseidfrom' => array(self::BELONGS_TO, 'Warehouse', 'warehousereceipt_warehouseid_from'),
            'warehousereceiptBillId' => array(self::BELONGS_TO, 'Bills', 'warehousereceipt_bill_id'),
            'warehousereceiptDespatch' => array(self::BELONGS_TO, 'Warehousedespatch', 'warehousereceipt_despatch_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'warehousereceipt_id' => 'Warehousereceipt',
            'warehousereceipt_no' => 'Warehousereceipt No',
            'warehousereceipt_date' => 'Warehousereceipt Date',
            'warehousereceipt_remark' => 'Remark',
            'warehousereceipt_despatch_id' => 'Warehousereceipt Despatch',
            'warehousereceipt_warehouseid' => 'Warehousereceipt Stockid',
            'warehousereceipt_warehouseid_from' => 'Warehouse From',
            'warehousereceipt_transfer_type' => 'Transfer Type',
            'warehousereceipt_clerk' => 'Warehousereceipt Clark',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('warehousereceipt_id', $this->warehousereceipt_id);
        $criteria->compare('warehousereceipt_no', $this->warehousereceipt_no);
        $criteria->compare('warehousereceipt_bill_id', $this->warehousereceipt_bill_id);       
        $criteria->compare('warehousereceipt_date', $this->warehousereceipt_date, true);
        $criteria->compare('warehousereceipt_transfer_type', $this->warehousereceipt_transfer_type, true);
        $criteria->compare('warehousereceipt_remark', $this->warehousereceipt_remark);
        $criteria->compare('warehousereceipt_despatch_id',$this->warehousereceipt_despatch_id);
        $criteria->compare('warehousereceipt_warehouseid', $this->warehousereceipt_warehouseid);
        $criteria->compare('warehousereceipt_warehouseid_from', $this->warehousereceipt_warehouseid_from);
        $criteria->compare('warehousereceipt_clerk', $this->warehousereceipt_clerk);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        $criteria->addCondition("company_id =" . Yii::app()->user->company_id . "");

        if (!empty($this->date_from) && empty($this->date_to)) {
            $criteria->addCondition("warehousereceipt_date >= '" . date('Y-m-d', strtotime($this->date_from)) . "'");  // date is database date column field
        } else if (!empty($this->date_to) && empty($this->date_from)) {
            $criteria->addCondition("warehousereceipt_date <= '" . date('Y-m-d', strtotime($this->date_to)) . "'");
        } else if (!empty($this->date_to) && !empty($this->date_from)) {
            $criteria->addCondition("warehousereceipt_date between '" . date('Y-m-d', strtotime($this->date_from)) . "' and  '" . date('Y-m-d', strtotime($this->date_to)) . "'");
        }
        // if (!empty($this->warehousereceipt_transfer_type)) {
        //     $criteria->addCondition("warehousereceipt_transfer_type == '" . $this->warehousereceipt_transfer_type . "'");  // date is database date column field
        // }
        if (Yii::app()->user->role != 1) {
            $assigned = Warehouse::model()->findAll(array('condition' => 'FIND_IN_SET(' . Yii::app()->user->id . ', assigned_to)'));
            if (!empty($assigned)) {
                $id_values = '';
                $newQuery = "";
                foreach ($assigned as $key => $value) {
                    $arrVal = explode(',', $value['warehouse_id']);

                    foreach ($arrVal as $arr) {
                        $id_values .= $arr . ',';
                        //   if ($newQuery) $newQuery .= ' OR';
                        //   $newQuery .= " FIND_IN_SET('".$arr."', warehousereceipt_warehouseid)";
                    }
				}
				$id_values = rtrim($id_values,',');
                $newQuery = "warehousereceipt_warehouseid IN (".$id_values.")";

                $criteria->addCondition($newQuery);
            } else {
                $criteria->addCondition("warehousereceipt_warehouseid IS NULL");
            }
        }
        // echo '<pre>';print_r(Yii::app()->user->company_id);exit;
        // echo '<pre>';print_r($id_values);exit;

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'warehousereceipt_id DESC',),
            'pagination' => array('pageSize' => 10,),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Warehousereceipt the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
   

}
