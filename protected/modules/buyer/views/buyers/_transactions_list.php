<?php
$gsttotal = $data["sgst_val"] + $data["cgst_val"] + $data["igst_val"];
$tblpx = Yii::app()->db->tablePrefix;
?>
<?php
if ($index == 0) {
?>
    <thead>
        <tr>
            <?php
            if ((isset(Yii::app()->user->role) && (in_array('/buyer/buyers/updatetransaction', Yii::app()->user->menuauthlist) || in_array('/buyer/buyers/deletetransaction', Yii::app()->user->menuauthlist)))) {
            ?>
                <th></th>
            <?php } ?>
            <th>Company</th>
            <th>Buyer</th>
            <th>Project</th>
            <th>Flat No</th>
            <th>Transaction For</th>
            <th>Transaction No</th>
            <th>Invoice No</th>
            <th>Transaction Type</th>
            <th>Description</th>
            <th>Bank</th>
            <th>Cheque No</th>
            <th>Amount</th>
            <th>Tax</th>
            <th>Total</th>

        </tr>
    </thead>
<?php } ?>
<tr id="<?php echo $index; ?>" class="rowedit<?php //echo $class; ?> ">
    <?php
    if ((isset(Yii::app()->user->role) && (in_array('/buyer/buyers/updatetransaction', Yii::app()->user->menuauthlist) || in_array('/buyer/buyers/deletetransaction', Yii::app()->user->menuauthlist)))) {
    ?>
        <td style="width:30px;text-align: center;">
            <span class="icon icon-options-vertical popover-test" data-toggle="popover" data-placement="right" type="button" data-html="true" style="cursor: pointer;"></span>
            <div class="popover-content hide">
                <ul class="tooltip-hiden">
                    <?php
                    if ((in_array('/buyer/buyers/updatetransaction', Yii::app()->user->menuauthlist))) {
                    ?>
                        <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default row-daybook">Edit</button></li>
                    <?php } ?>
                    <?php
                    if ((in_array('/buyer/buyers/deletetransaction', Yii::app()->user->menuauthlist))) {
                    ?>
                        <li><button id="<?php echo $index; ?>" class="btn btn-xs btn-default delete_row">Delete</button></li>
                    <?php } ?>
                </ul>
            </div>
        </td>
    <?php } ?>
    <td><?= $data->company->name ?></td>
    <td><?= $data->buyer->name ?></td>
    <td><?= $data->project->name ?></td>
    <td><?= $data->flat->flat_number ?></td>
    <td>
        <?php
        if ($data->transaction_for == 1) {
            echo 'Advance';
        } elseif ($data->transaction_for == 2) {
            echo 'Receipt';
        } elseif ($data->transaction_for == 3) {
            echo 'Expense';
        } else {
            echo '';
        }
        ?>
    </td>
    <td><?= $data->transaction_no ?></td>
    <td><?= $data->getInvoiceNo($data->invoice_no) ?></td>
    <td><?= $data->transactionHead->caption ?></td>
    <td><?= $data->description ?></td>
    <td><?= !empty($data->bank_id) ? $data->bank->bank_name : '' ?></td>
    <td><?= $data->cheque_no ?></td>
    <td><?= $data->amount ?></td>
    <td><?= $gsttotal ?></td>
    <td><?= $data->total_amount ?></td>

</tr>
<input type="hidden" name="expenseid[]" id="expenseid<?php echo $index; ?>" value="<?php echo $data["id"]; ?>" />
<input type="hidden" name="rowid[]" id="rowid<?php echo $index; ?>" value="<?php echo $index; ?>" />
<?php
if ($index == 0) {
?>
    <tfoot>
        <tr>
            <?php
            if ((isset(Yii::app()->user->role) && (in_array('/buyer/buyers/updatetransaction', Yii::app()->user->menuauthlist) || in_array('/buyer/buyers/deletetransaction', Yii::app()->user->menuauthlist)))) {
            ?>
                <th></th>
            <?php } ?>
            <th colspan="11" class="text-right">Total Amounts:</th>
            <th class="text-right"><?php echo Controller::money_format_inr($amount, 2); ?></th>
            <th class="text-right"><?php echo Controller::money_format_inr($grandgst, 2); ?></th>
            <th class="text-right"><?php echo Controller::money_format_inr($grandtotal, 2); ?></th>

        </tr>
    </tfoot>
<?php } ?>
<style>
    .popover {
        white-space:nowrap;
    }
</style>