<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs = array(
    'performa invoice',
)
    ?>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>
<div class="container" id="project">

    <div class="sub-heading mb-10">
        <h3>Defect Return</h3>
        <a href="<?php echo Yii::app()->createAbsoluteUrl('wh/warehouse/createdefectReturn'); ?>"
            class="button btn btn-info">Add Defect Return</a>
    </div>
    <?php $this->renderPartial('_defectreturnsearch', array('model' => $model)) ?>

    <?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="info"
            style="width: 500px;margin-left: 400px;padding: 8px;color: green;font-size: 20px;font-weight: bold;">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>


    <?php if (Yii::app()->user->hasFlash('error')): ?>
        <div class="info"
            style="width: 500px;margin-left: 400px;padding: 8px;color: red;font-size: 20px;font-weight: bold;">
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
    <?php endif; ?>

    <?php
    $amount = 0;
    foreach ($dataProvider->getData() as $record) {
        $amount += $record->return_amount;
    }
    ?>
    <div class="margin-top-10">
        <?php
        $dataProvider->sort->defaultOrder = 'bill_id DESC';
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_defectreturnview',
            'enableSorting' => 1,
            'viewData' => array('amount' => $amount),
            'template' => '<div class="sub-heading">
                        <div class="salesclass"><span class="clr_pal bg-success"></span><span>Approved&nbsp;&nbsp;</span><span class="clr_pal bg_warning"></span><span>Pending&nbsp;&nbsp;</span></div>
                        {summary}</div><div id="parent"><table cellpadding="10" class="table" id="billtable2">{items}</table></div>'
        ));
        ?>
    </div>
</div>



<style>

    table.dataTable tbody td {
        padding: 12px 10px;
    }

    #parent {
        max-height: 300px;
    }

</style>
<script>
    $(document).ready(function () {
        $("#billtable2").tableHeadFixer({ 'left': false, 'foot': true, 'head': true });

        $(".popover-test").popover({
            html: true,
            content: function () {
                return $(this).next('.popover-content').html();
            }
        });
        $('[data-toggle=popover]').on('click', function (e) {
            $('[data-toggle=popover]').not(this).popover('hide');
        });
        $('body').on('hidden.bs.popover', function (e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
        $('body').on('click', function (e) {
            $('[data-toggle=popover]').each(function () {
                // hide any open popovers when the anywhere else in the body is clicked
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
        $(document).ajaxComplete(function () {
            $(".popover-test").popover({
                html: true,
                content: function () {
                    return $(this).next('.popover-content').html();
                }
            });

        });

        $(document).on("click", ".approve_btn", function () {
            var id = $(this).attr('id');
            $.ajax({
                type: "POST",
                dataType: "json",
                data: {
                    "id": id,
                },
                url: "<?php echo $this->createUrl('warehouse/approveDefectReturn&id=') ?>" + id,
                success: function (reponse) {
                    alert(reponse.msg)
                    location.reload();
                }
            })
        });

    });
</script>