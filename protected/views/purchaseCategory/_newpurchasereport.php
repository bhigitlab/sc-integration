<?php
/* @var $this PurchaseCategoryController */
/* @var $data PurchaseCategory */
?>

<?php
if ($index == 0) {
?>
<thead class="entry-table sticky-thead">
    <tr>
        <th>Sl No.</th>
        <th>Project Name</th>
        <th>Start Date</th>
    </tr>   
</thead>
<?php } ?>
<tbody>
    <tr>
        <td><?php echo $index+1; ?></td>
        <td><?php echo $data['name'];?></td>
        <td><?php echo date('Y-m-d', strtotime($data['start_date'])); ?></td>
    </tr>
</tbody>