<?php

/**
 * This is the model class for table "{{projectbook}}".
 *
 * The followings are the available columns in table '{{projectbook}}':
 * @property integer $pb_id
 * @property integer $projectid
 * @property integer $userid
 * @property string $book_date
 * @property string $book_description
 * @property string $works_done
 * @property string $materials_unlade
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property Projects $project
 * @property Users $user
 * @property Users $createdBy
 * @property Users $updatedBy
 */
class Projectbook extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{projectbook}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('projectid, userid,exptype,vendor book_description, created_by, created_date, updated_by', 'required'),
			array('projectid, userid, created_by, updated_by,exptype,vendor', 'numerical', 'integerOnly'=>true),
			array('book_description', 'length', 'max'=>300),
			array('works_done', 'length', 'max'=>100),
			array('book_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('pb_id, projectid, userid, book_date, book_description, works_done, exptype,vendor, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'project' => array(self::BELONGS_TO, 'Projects', 'projectid'),
			'user' => array(self::BELONGS_TO, 'Users', 'userid'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'), 
                        'vendor0' => array(self::BELONGS_TO, 'Vendors', 'vendor'),
                        'exptype0' => array(self::BELONGS_TO,'ExpenseType', 'exptype'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pb_id' => 'Pb',
			'projectid' => 'Projectid',
			'userid' => 'Userid',
			'book_date' => 'Book Date',
			'book_description' => 'Book Description',
			'works_done' => 'Works Done',
			'exptype' => 'Expense Type',
                        'vendor'=>'Vendor',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pb_id',$this->pb_id);
		$criteria->compare('projectid',$this->projectid);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('book_date',$this->book_date,true);
		$criteria->compare('book_description',$this->book_description,true);
		$criteria->compare('works_done',$this->works_done,true);
                $criteria->compare('exptype',$this->exptype,true);
                 $criteria->compare('vendor',$this->vendor,true);
		//$criteria->compare('materials_unlade',$this->materials_unlade,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Projectbook the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
