<?php

/**
 * This is the model class for table "{{pre_expenses}}".
 *
 * The followings are the available columns in table '{{pre_expenses}}':
 * @property integer $exp_id
 * @property integer $ref_id
 * @property integer $followup_id
 * @property string $record_grop_id
 * @property integer $projectid
 * @property integer $bill_id
 * @property integer $invoice_id
 * @property integer $return_id
 * @property integer $subcontractor_id
 * @property integer $userid
 * @property string $date
 * @property double $amount
 * @property string $description
 * @property integer $type
 * @property integer $exptype
 * @property integer $expense_type
 * @property integer $vendor_id
 * @property double $expense_amount
 * @property double $expense_sgstp
 * @property double $expense_sgst
 * @property double $expense_cgstp
 * @property double $expense_cgst
 * @property double $expense_igstp
 * @property double $expense_igst
 * @property double $expense_tdsp
 * @property double $expense_tds
 * @property integer $payment_type
 * @property integer $bank_id
 * @property string $cheque_no
 * @property double $receipt
 * @property string $works_done
 * @property string $materials
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 * @property integer $purchase_type
 * @property double $paid
 * @property double $paidamount
 * @property string $data_entry
 * @property integer $company_id
 * @property integer $reconciliation_status
 * @property string $reconciliation_date
 * @property integer $employee_id
 * @property double $budget_percentage
 * @property integer $update_status
 * @property integer $delete_status
 * @property string $payment_quotation_status
 * @property double $additional_charge
 * @property integer $approval_status
 *
 * The followings are the available model relations:
 * @property Bills $bill
 * @property Users $createdBy
 * @property Invoice $invoice
 * @property Projects $project
 * @property Subcontractor $subcontractor
 * @property Users $updatedBy
 * @property Users $user
 */
class PreExpenses extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{pre_expenses}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ref_id, record_grop_id, projectid, userid, amount, created_by, created_date', 'required'),
			array('ref_id, followup_id, projectid, bill_id, invoice_id, return_id, subcontractor_id, userid, type, exptype, expense_type, vendor_id, payment_type, bank_id, created_by, updated_by, purchase_type, company_id, reconciliation_status, employee_id, update_status, delete_status, approval_status', 'numerical', 'integerOnly' => true),
			array('amount, expense_amount, expense_sgstp, expense_sgst, expense_cgstp, expense_cgst, expense_igstp, expense_igst, expense_tdsp, expense_tds, receipt, paid, paidamount, budget_percentage, additional_charge', 'numerical'),
			array('description', 'length', 'max' => 300),
			array('cheque_no, works_done, materials', 'length', 'max' => 100),
			array('data_entry', 'length', 'max' => 30),
			array('payment_quotation_status', 'length', 'max' => 1),
			array('date, updated_date, reconciliation_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('exp_id, ref_id, followup_id, record_grop_id, projectid, bill_id, invoice_id, return_id, subcontractor_id, userid, date, amount, description, type, exptype, expense_type, vendor_id, expense_amount, expense_sgstp, expense_sgst, expense_cgstp, expense_cgst, expense_igstp, expense_igst, expense_tdsp, expense_tds, payment_type, bank_id, cheque_no, receipt, works_done, materials, created_by, created_date, updated_by, updated_date, purchase_type, paid, paidamount, data_entry, company_id, reconciliation_status, reconciliation_date, employee_id, budget_percentage, update_status, delete_status, payment_quotation_status, additional_charge, approval_status', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'invoice' => array(self::BELONGS_TO, 'Invoice', 'invoice_id'),
			'project' => array(self::BELONGS_TO, 'Projects', 'projectid'),
			'subcontractor' => array(self::BELONGS_TO, 'Subcontractor', 'subcontractor_id'),
			'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
			'user' => array(self::BELONGS_TO, 'Users', 'userid'),
			'vendor' => array(self::BELONGS_TO, 'Vendors', 'vendor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'exp_id' => 'Exp',
			'ref_id' => 'Ref',
			'followup_id' => 'Followup',
			'record_grop_id' => 'Record Grop',
			'projectid' => 'Projectid',
			'bill_id' => 'Bill',
			'invoice_id' => 'Invoice',
			'return_id' => 'Return',
			'subcontractor_id' => 'Subcontractor',
			'userid' => 'Userid',
			'date' => 'Date',
			'amount' => 'Amount',
			'description' => 'Description',
			'type' => 'Type',
			'exptype' => 'Exptype',
			'expense_type' => 'Expense Type',
			'vendor_id' => 'Vendor',
			'expense_amount' => 'Expense Amount',
			'expense_sgstp' => 'Expense Sgstp',
			'expense_sgst' => 'Expense Sgst',
			'expense_cgstp' => 'Expense Cgstp',
			'expense_cgst' => 'Expense Cgst',
			'expense_igstp' => 'Expense Igstp',
			'expense_igst' => 'Expense Igst',
			'expense_tdsp' => 'Expense Tdsp',
			'expense_tds' => 'Expense Tds',
			'payment_type' => 'Payment Type',
			'bank_id' => 'Bank',
			'cheque_no' => 'Cheque No',
			'receipt' => 'Receipt',
			'works_done' => 'Works Done',
			'materials' => 'Materials',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
			'purchase_type' => 'Purchase Type',
			'paid' => 'Paid',
			'paidamount' => 'Paidamount',
			'data_entry' => 'Data Entry',
			'company_id' => 'Company',
			'reconciliation_status' => 'Reconciliation Status',
			'reconciliation_date' => 'Reconciliation Date',
			'employee_id' => 'Employee',
			'budget_percentage' => 'Budget Percentage',
			'update_status' => 'Update Status',
			'delete_status' => 'Delete Status',
			'payment_quotation_status' => 'Payment Quotation Status',
			'additional_charge' => 'Additional Charge',
			'approval_status' => 'Approval Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('exp_id', $this->exp_id);
		$criteria->compare('ref_id', $this->ref_id);
		$criteria->compare('followup_id', $this->followup_id);
		$criteria->compare('record_grop_id', $this->record_grop_id, true);
		$criteria->compare('projectid', $this->projectid);
		$criteria->compare('bill_id', $this->bill_id);
		$criteria->compare('invoice_id', $this->invoice_id);
		$criteria->compare('return_id', $this->return_id);
		$criteria->compare('subcontractor_id', $this->subcontractor_id);
		$criteria->compare('userid', $this->userid);
		$criteria->compare('date', $this->date, true);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('type', $this->type);
		$criteria->compare('exptype', $this->exptype);
		$criteria->compare('expense_type', $this->expense_type);
		$criteria->compare('vendor_id', $this->vendor_id);
		$criteria->compare('expense_amount', $this->expense_amount);
		$criteria->compare('expense_sgstp', $this->expense_sgstp);
		$criteria->compare('expense_sgst', $this->expense_sgst);
		$criteria->compare('expense_cgstp', $this->expense_cgstp);
		$criteria->compare('expense_cgst', $this->expense_cgst);
		$criteria->compare('expense_igstp', $this->expense_igstp);
		$criteria->compare('expense_igst', $this->expense_igst);
		$criteria->compare('expense_tdsp', $this->expense_tdsp);
		$criteria->compare('expense_tds', $this->expense_tds);
		$criteria->compare('payment_type', $this->payment_type);
		$criteria->compare('bank_id', $this->bank_id);
		$criteria->compare('cheque_no', $this->cheque_no, true);
		$criteria->compare('receipt', $this->receipt);
		$criteria->compare('works_done', $this->works_done, true);
		$criteria->compare('materials', $this->materials, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->compare('updated_date', $this->updated_date, true);
		$criteria->compare('purchase_type', $this->purchase_type);
		$criteria->compare('paid', $this->paid);
		$criteria->compare('paidamount', $this->paidamount);
		$criteria->compare('data_entry', $this->data_entry, true);
		$criteria->compare('company_id', $this->company_id);
		$criteria->compare('reconciliation_status', $this->reconciliation_status);
		$criteria->compare('reconciliation_date', $this->reconciliation_date, true);
		$criteria->compare('employee_id', $this->employee_id);
		$criteria->compare('budget_percentage', $this->budget_percentage);
		$criteria->compare('update_status', $this->update_status);
		$criteria->compare('delete_status', $this->delete_status);
		$criteria->compare('payment_quotation_status', $this->payment_quotation_status, true);
		$criteria->compare('additional_charge', $this->additional_charge);
		$criteria->compare('approval_status', $this->approval_status);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => false,
			'sort' => array(
				'defaultOrder' => 'exp_id DESC',
				'attributes' => array(
					'*'
				),
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PreExpenses the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
