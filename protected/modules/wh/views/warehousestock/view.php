<?php
/* @var $this WarehousestockController */
/* @var $model Warehousestock */

$this->breadcrumbs=array(
	'Warehousestocks'=>array('index'),
	$model->warehousestock_id,
);

$this->menu=array(
	array('label'=>'List Warehousestock', 'url'=>array('index')),
	array('label'=>'Create Warehousestock', 'url'=>array('create')),
	array('label'=>'Update Warehousestock', 'url'=>array('update', 'id'=>$model->warehousestock_id)),
	array('label'=>'Delete Warehousestock', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->warehousestock_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Warehousestock', 'url'=>array('admin')),
);
?>

<h1>View Warehousestock #<?php echo $model->warehousestock_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'warehousestock_id',
		'warehousestock_warehouseid',
		'warehousestock_itemid',
		'warehousestock_date',
		'warehousestock_stock_quantity',
		'warehousestock_unit',
		'warehousestock_status',
		'created_date',
		'created_by',
		'updated_date',
		'updated_by',
	),
)); ?>
