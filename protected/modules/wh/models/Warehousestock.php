<?php

/**
 * This is the model class for table "{{warehousestock}}".
 *
 * The followings are the available columns in table '{{warehousestock}}':
 * @property integer $warehousestock_id
 * @property integer $warehousestock_warehouseid
 * @property string $warehousestock_date
 * @property integer $warehousestock_itemid
 * @property integer $warehousestock_unit
 * @property double $warehousestock_stock_quantity
 * @property string $warehousestock_status
 * @property integer $company_id
 * @property string $created_date
 * @property integer $created_by
 * @property string $updated_date
 * @property integer $updated_by
 */
class Warehousestock extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */

	public $date_from;
	public $rate;
	public $date_to;
	public $add_to_Warehouse;
	public $rules_array = array();
	public function tableName()
	{
		return '{{warehousestock}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		$controller =  Yii::app()->controller->id;
		if ($controller == 'warehousestock') {
			$this->rules_array = array(
				array('warehousestock_warehouseid, warehousestock_date, warehousestock_itemid, warehousestock_unit, warehousestock_stock_quantity, company_id, created_date, created_by, updated_date,rate, updated_by', 'required'),
				array('warehousestock_warehouseid, warehousestock_itemid, warehousestock_unit, company_id, created_by, updated_by', 'numerical', 'integerOnly' => true),
				array('warehousestock_stock_quantity', 'numerical'),
				array('warehousestock_status', 'length', 'max' => 1),
				// The following rule is used by search().
				// @todo Please remove those attributes that should not be searched.
				array('warehousestock_id, warehousestock_warehouseid,batch, warehousestock_date, warehousestock_itemid, warehousestock_unit, warehousestock_stock_quantity,rate, warehousestock_status, company_id, created_date, created_by, updated_date, updated_by,rate', 'safe', 'on' => 'search'),
			);
		} elseif ($controller == 'bills') {
			$this->rules_array = array(
				array('warehousestock_warehouseid', 'checkWarehousestockRequired'),
				array('warehousestock_id, warehousestock_warehouseid,batch, warehousestock_date, warehousestock_itemid, warehousestock_unit, warehousestock_stock_quantity, warehousestock_status, company_id, created_date, created_by, updated_date, updated_by', 'safe', 'on' => 'search'),

			);

			//else{
			// 	$this->rules_array = array(
			// 		array('warehousestock_id, warehousestock_warehouseid, warehousestock_date, warehousestock_itemid, warehousestock_unit, warehousestock_stock_quantity, warehousestock_status, company_id, created_date, created_by, updated_date, updated_by', 'safe', 'on'=>'search'),

			// 	);
			// }

		} else {
			$this->rules_array = array(
				array('warehousestock_id, warehousestock_warehouseid,batch, warehousestock_date, warehousestock_itemid, warehousestock_unit, warehousestock_stock_quantity, warehousestock_status, company_id, created_date, created_by,rate, updated_date, updated_by', 'safe', 'on' => 'search'),

			);
		}
		return $this->rules_array;
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'warehouse' => array(self::BELONGS_TO, 'Warehouse', 'warehousestock_warehouseid'),
			'item' => array(self::BELONGS_TO, 'PurchaseCategory', 'warehousestock_itemid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'warehousestock_id' => 'Warehousestock',
			'warehousestock_warehouseid' => 'Warehousestock Warehouseid',
			'warehousestock_date' => 'Warehousestock Date',
			'batch' => 'Batch',
			'rate' => 'Rate',
			'warehousestock_itemid' => 'Warehousestock Itemid',
			'warehousestock_unit' => 'Warehousestock Unit',
			'warehousestock_stock_quantity' => 'Warehousestock Stock Quantity',
			'warehousestock_status' => 'Warehousestock Status',
			'company_id' => 'Company',
			'created_date' => 'Created Date',
			'created_by' => 'Created By',
			'updated_date' => 'Updated Date',
			'updated_by' => 'Updated By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('warehousestock_id', $this->warehousestock_id);
		$criteria->compare('warehousestock_warehouseid', $this->warehousestock_warehouseid);
		$criteria->compare('warehousestock_date', $this->warehousestock_date, true);
		$criteria->compare('batch', $this->batch, true);
		$criteria->compare('rate', $this->rate, true);
		$criteria->compare('warehousestock_itemid', $this->warehousestock_itemid);
		$criteria->compare('warehousestock_unit', $this->warehousestock_unit);
		$criteria->compare('warehousestock_stock_quantity', $this->warehousestock_stock_quantity);
		$criteria->compare('warehousestock_status', $this->warehousestock_status, true);
		$criteria->compare('company_id', $this->company_id);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('updated_date', $this->updated_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->addCondition("warehousestock_stock_quantity != 0");
		if (!empty($this->date_from) && empty($this->date_to)) {
			$criteria->addCondition("warehousestock_date >= '" . date('Y-m-d', strtotime($this->date_from)) . "'");  // date is database date column field
		} else if (!empty($this->date_to) && empty($this->date_from)) {
			$criteria->addCondition("warehousestock_date <= '" . date('Y-m-d', strtotime($this->date_to)) . "'");
		} else if (!empty($this->date_to) && !empty($this->date_from)) {
			$criteria->addCondition("warehousestock_date between '" . date('Y-m-d', strtotime($this->date_from)) . "' and  '" . date('Y-m-d', strtotime($this->date_to)) . "'");
		}
		// $criteria->group='warehousestock_warehouseid,warehousestock_itemid,batch,rate';
		$criteria->order = 'warehousestock_id DESC';



		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			//'pagination'=>array('pageSize'=>10,),
			'pagination' => false,
		));
	}
	public function searchStockStatusReport()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('warehousestock_id', $this->warehousestock_id);
		$criteria->compare('warehousestock_warehouseid', $this->warehousestock_warehouseid);
		$criteria->compare('warehousestock_date', $this->warehousestock_date, true);
		$criteria->compare('batch', $this->batch, true);
		$criteria->compare('rate', $this->rate, true);
		$criteria->compare('warehousestock_itemid', $this->warehousestock_itemid);
		$criteria->compare('warehousestock_unit', $this->warehousestock_unit);
		$criteria->compare('warehousestock_stock_quantity', $this->warehousestock_stock_quantity);
		$criteria->compare('warehousestock_status', $this->warehousestock_status, true);
		$criteria->compare('company_id', $this->company_id);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('updated_date', $this->updated_date, true);
		$criteria->compare('updated_by', $this->updated_by);
		$criteria->addCondition("warehousestock_stock_quantity != 0");
		if (!empty($this->date_from) && empty($this->date_to)) {
			$criteria->addCondition("warehousestock_date >= '" . date('Y-m-d', strtotime($this->date_from)) . "'");  // date is database date column field
		} else if (!empty($this->date_to) && empty($this->date_from)) {
			$criteria->addCondition("warehousestock_date <= '" . date('Y-m-d', strtotime($this->date_to)) . "'");
		} else if (!empty($this->date_to) && !empty($this->date_from)) {
			$criteria->addCondition("warehousestock_date between '" . date('Y-m-d', strtotime($this->date_from)) . "' and  '" . date('Y-m-d', strtotime($this->date_to)) . "'");
		}
		$criteria->group = 'warehousestock_itemid,batch';
		if (Yii::app()->user->role != 1) {
			$assigned = Warehouse::model()->findAll(array('condition' => 'FIND_IN_SET(' . Yii::app()->user->id . ', assigned_to)'));
			if (!empty($assigned)) {
				$id_values = '';
				$newQuery = "";
				foreach ($assigned as $key => $value) {
					$arrVal = explode(',', $value['warehouse_id']);

					foreach ($arrVal as $arr) {
						$id_values .= $arr . ',';
						//   if ($newQuery) $newQuery .= ' OR';
						//   $newQuery .= " FIND_IN_SET('".$arr."', warehousereceipt_warehouseid)";
					}
				}
				$id_values = rtrim($id_values, ',');
				$newQuery = "warehousestock_warehouseid IN (" . $id_values . ")";

				$criteria->addCondition($newQuery);
			} else {
				$criteria->addCondition("warehousestock_warehouseid IS NULL");
			}
		}
		// echo '<pre>';print_r($criteria);exit;
		//$criteria->group = 'warehousestock_warehouseid , warehousestock_itemid';
		$criteria->order = 'warehousestock_id DESC';
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			//'pagination'=>array('pageSize'=>10,),
			'pagination' => false,
		));

		/*$warehousestock_sql = "SELECT * FROM sb_warehousestock";		
		if(!empty($this->date_from) && empty($this->date_to)) {
			$warehousestock_sql .= " WHERE warehousestock_date >= '".date('Y-m-d',strtotime($this->date_from))."'";
		} else if(!empty($this->date_to) && empty($this->date_from)) {
			$warehousestock_sql .= " WHERE warehousestock_date <= '".date('Y-m-d',strtotime($this->date_to))."'";
		} else if(!empty($this->date_to) && !empty($this->date_from)) {
			$warehousestock_sql .= " WHERE warehousestock_date between '".date('Y-m-d',strtotime($this->date_from))."' and  '".date('Y-m-d',strtotime($this->date_to))."'";
		}
		$warehousestock_sql .= " GROUP BY warehousestock_warehouseid,warehousestock_itemid,batch";
		$warehousestock_sql .= " ORDER BY updated_date DESC";
		$warehousestock = Yii::app()->db->createCommand($warehousestock_sql);
		$warehousestock = $warehousestock->queryAll();
		return $warehousestock; */
	}
	public function checkWarehousestockRequired($attribute, $params)
	{
		if ($this->$attribute == '') {
			$this->addError($attribute, 'is not null');
		}
	}
	public function checkpriority($item_id, $base_unit)
	{

		$prior_units = [];
		$tblpx = Yii::app()->db->tablePrefix;

		$conArray  = array('condition' => 'item_id = ' . $item_id . ' AND base_unit ="' . $base_unit . '" AND priority ="1"');


		$unitconversion_prior = UnitConversion::model()->findAll($conArray);

		if (count($unitconversion_prior) != 0) {
			foreach ($unitconversion_prior as $key => $value) {
				$prior_units['conversion_factor'] = $value['conversion_factor'];
				$prior_units['conversion_unit'] = $value['conversion_unit'];
			}
		}

		return $prior_units;
	}
	public function GetItemunit($category_id)
	{
		$category            = PurchaseCategory::model()->findByPk($category_id);
		$categoryUnit        = $category["unit"];
		return $categoryUnit;
	}
	public function GetItemunitname($unit_id)
	{
		$unit_id            = $unit_id;
		$unitdetails           = Unit::model()->findByPk($unit_id);
		$Unit_name      = $unitdetails["unit_name"];
		return $Unit_name;
	}
	public function GetwarehouseItemunitid($unit_name)
	{
		$unit_name            = trim($unit_name);
		$conArray  = array('condition' => 'unit_name = "' . $unit_name . '"');
		$unitdetails = Unit::model()->findAll($conArray);
		$warehouseunit_id = '';
		foreach ($unitdetails as $key => $value) {
			$warehouseunit_id = $value['id'];
		}
		return $warehouseunit_id;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Warehousestock the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
