<?php

/**
 * This is the model class for table "{{projects}}".
 *
 * The followings are the available columns in table '{{projects}}':
 * @property integer $pid
 * @property integer $client_id
 * @property string $name
 * @property integer $billable
 * @property integer $status
 * @property double $budget
 * @property string $start_date
 * @property integer $end_date
 * @property string $description
 * @property string $created_date
 * @property integer $created_by
 * @property string $updated_date
 * @property integer $updated_by
 * @property integer $client_id
 * @property integer $project_status
 * @property integer $project_type
 * @property string $sqft
 * @property integer $contract
 * @property double $bill_amount
 * @property string $remarks
 * @property integer $project_category
 * @property integer $project_duration
 *
 * The followings are the available model relations:
 * @property Expenses[] $expenses
 * @property ProjectExptype[] $projectExptypes
 * @property Clients $client
 * @property Status $projectStatus
 * @property Status $projectType
 * @property Status $status0
 * @property Users $createdBy
 * @property Users $updatedBy
 * @property Tasks[] $tasks
 */
class Projects extends CActiveRecord
{

    public $billable = 3; //Default radio button selection 
    public $status = 1;  //default radio button selection on create.
    public $userid;
    public $user;
    public $fromdate;
    public $todate;
    public $ex_percent;
    public $companyId;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Projects the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{projects}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        $pms_api_integration_model = ApiSettings::model()->findByPk(1);
        $pms_api_integration = $pms_api_integration_model ? $pms_api_integration_model->api_integration_settings : 0;
        
        if ($pms_api_integration == 1) {
            if (yii::app()->user->role == 10) {
                $required = array('client_id, start_date, labour_template, completion_date, project_category, project_duration, company_id', 'required');
                $required1 = array('client_id', 'required', 'message' => 'Please enter {attribute}');
            } else {
                $required = array('status, start_date, client_id, labour_template, project_status, project_type, completion_date, company_id', 'required');
                $required1 = array('name', 'required', 'message' => 'Please enter {attribute}');
            }

            return array(
                $required,
                $required1,
                array('name', 'unique'),
                array('status, sqft, sqft_rate, percentage, created_by, updated_by, client_id, profit_margin, project_quote, number_of_flats', 'numerical', 'integerOnly' => true),
                array('name', 'length', 'max' => 100),
                array('contract', 'numerical', 'integerOnly' => true),
                array('site', 'length', 'max' => 100),
                array('company_id,labour_template, flat_names', 'safe'),
                array('description, user, userid, site, ex_percent, billed_to_client, tot_receipt, tot_expense, tot_paid_to_vendor, work_type_id, created_date, created_by, updated_date, updated_by', 'safe'),
                array('flat_names', 'checkflatcount'),
                array('start_date', 'checkstatus'),
                
                
                array('completion_date', 'checkdatevalidation'),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('pid, name, status, description, created_date, created_by, updated_date, updated_by, sqft_rate, percentage, client_id, fromdate, todate, ex_percent, completion_date, start_date, billed_to_client, tot_receipt, tot_expense, tot_paid_to_vendor, work_type_id, profit_margin, project_quote, expense_percentage, expense_amount, auto_update, project_category, project_duration, remarks, number_of_flats, flat_names', 'safe', 'on' => 'search'),
               
            );
        } else {
            if (yii::app()->user->role == 10) {
                $required = array('client_id, start_date, completion_date, project_category, project_duration, company_id', 'required');
                $required1 = array('client_id', 'required', 'message' => 'Please enter {attribute}');
            } else {
                $required = array('status, start_date, client_id, project_status, project_type, completion_date, company_id', 'required');
                $required1 = array('name', 'required', 'message' => 'Please enter {attribute}');
            }

            return array(
                $required,
                $required1,
                array('name', 'unique'),
                array('status, sqft, sqft_rate, percentage, created_by, updated_by, client_id, profit_margin, project_quote, number_of_flats', 'numerical', 'integerOnly' => true),
                array('name', 'length', 'max' => 100),
                array('contract', 'numerical', 'integerOnly' => true),
                array('site', 'length', 'max' => 100),
                array('company_id, flat_names', 'safe'),
                array('description, user, userid, site, ex_percent, billed_to_client, tot_receipt, tot_expense, tot_paid_to_vendor, work_type_id, created_date, created_by, updated_date, updated_by', 'safe'),
                array('flat_names', 'checkflatcount'),
                array('start_date', 'checkstatus'),
                array('completion_date', 'checkdatevalidation'),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('pid, name, status, description, created_date, created_by, updated_date, updated_by, sqft_rate, percentage, client_id, fromdate, todate, ex_percent, completion_date, start_date, billed_to_client, tot_receipt, tot_expense, tot_paid_to_vendor, work_type_id, profit_margin, project_quote, expense_percentage, expense_amount, auto_update, project_category, project_duration, remarks, number_of_flats, flat_names', 'safe', 'on' => 'search'),
            );
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(


            'project_assigned' => array(self::HAS_MANY, 'ProjectAssign', 'projectid'),
            'expenses' => array(self::HAS_MANY, 'Expenses', 'projectid'),
            'projectExptypes' => array(self::HAS_MANY, 'ProjectExptype', 'project_id'),
            'client' => array(self::BELONGS_TO, 'Clients', 'client_id'),
            'projectStatus' => array(self::BELONGS_TO, 'Status', 'project_status'),
            'projectType' => array(self::BELONGS_TO, 'Status', 'project_type'),
            'status0' => array(self::BELONGS_TO, 'Status', 'status'),
            'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'updatedBy' => array(self::BELONGS_TO, 'Users', 'updated_by'),
            //'receipts' => array(self::HAS_MANY, 'Receipt', 'project_id'),
            'tasks' => array(self::HAS_MANY, 'Tasks', 'project_id'),

        );
    }

    public function checkflatcount($attribute, $params)
    {
        if (!empty($this->flat_names)) {
            $flat_names = $this->flat_names;
            $number_of_flats = $this->number_of_flats;
            $flat_names_array = explode(',', $flat_names);
            $count = 0;
            foreach ($flat_names_array as $flat_name) {
                if (!empty($flat_name)) {
                    $count++;
                }
            }
            if ($count != $number_of_flats) {
                $this->addError($attribute, 'Entries should be equal to number of flats');
            }
        }
    }

    public function checkstatus($attribute, $params)
    {        
        if (!empty($this->project_status) &&($this->pid =="")) {
            $project_status = $this->project_status;                      


            if ($project_status == "83" && date('Y-m-d', strtotime($this->start_date)) < date('Y-m-d')) {                
                $this->addError($attribute, 'Date should be on or after '.date('d-M-Y'));
            }

            if ($project_status == "84" && (date('Y-m-d') < date('Y-m-d', strtotime($this->start_date)))) {
                $this->addError($attribute, 'Date should be on or before '.date('d-M-Y'));
            }
        }
    }

    public function checklabour()
    {
        if (isset($this->labour_template)) { 
            $this->addError('labour_template', 'Please Fill Labour Template');
        }
    }

    public function checkdatevalidation($attribute, $params)
    {
        if (!empty($this->start_date)) {            
            if(date('Y-m-d', strtotime($this->completion_date)) < date('Y-m-d', strtotime($this->start_date))){
                $this->addError($attribute, 'Date should be greater than start date');
            }
        }
    }
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'pid' => 'Pid',
            'name' => 'Project Name',
            'completion_date' => 'Completion Date',
            'start_date' => 'Start Date',
            'site' => 'Site Name',
            'status' => 'Status',
            'user' => 'Assign to User',
            'description' => 'Site Description',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'updated_date' => 'Updated Date',
            'updated_by' => 'Updated By',
            'client_id' => 'Client id',
            'project_status' => 'Project Status',
            'project_type' => 'Work Contract',
            'sqft' => 'Square Feet',
            'sqft_rate' => 'SQ.FT.Rate or Lumpsum',
            'percentage' => 'Percentage',
            'contract' => 'Contract/Quotation',
            'bill_amount' => 'Bill Amount',
            'remarks' => 'Remarks',
            'auto_update' => 'Auto Update',
            'project_category' => 'Category',
            'project_duration' => 'Duration',
            'number_of_flats' => 'Enter number of flats/Apartments',
            'flat_names' => 'Enter flat number',
            'incharge' => 'Incharge',
            'labour_template'=>'Labour Template'
        );
    }


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $crm_api_integration=ApiSettings::model()->crmIntegrationStatus();
        $criteria = new CDbCriteria;
        if($crm_api_integration!='1'){
            
        }
        $criteria->compare('pid', $this->pid);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('name', $this->completion_date);
        // $criteria->compare('status', $this->status);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('start_date', $this->start_date, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('client_id', $this->client_id);
        $criteria->compare('project_status', $this->project_status);
        $criteria->compare('project_type', $this->project_type);
        $criteria->compare('sqft', $this->sqft, true);
        $criteria->compare('sqft_rate', $this->sqft_rate, true);
        $criteria->compare('percentage', $this->percentage, true);
        $criteria->compare('contract', $this->contract);
        $criteria->compare('bill_amount', $this->bill_amount);
        $criteria->compare('remarks', $this->remarks, true);
        // $criteria->compare('auto_update', $this->auto_update);
        $criteria->compare('project_category', $this->project_category);
        $criteria->compare('project_duration', $this->project_duration);
        if (!empty($this->companyId)) {
            $criteria->addCondition("FIND_IN_SET('" . $this->companyId . "', company_id)");
        } else {
            $companyArray = explode(",", Yii::app()->user->company_ids);
            $condition  = "";
            foreach ($companyArray as $company) {
                $condition .= "FIND_IN_SET('" . $company . "',company_id) OR ";
            }
            $condition = substr($condition, 0, -3);
            $criteria->addCondition($condition);
        }
        if (!empty($this->fromdate) && empty($this->todate)) {
            $criteria->addCondition("created_date >= '" . date('Y-m-d', strtotime($this->fromdate)) . "'");  // date is database date column field
        } elseif (!empty($this->todate) && empty($this->fromdate)) {
            $criteria->addCondition("created_date <= '" . date('Y-m-d', strtotime($this->todate)) . "'");
        } elseif (!empty($this->todate) && !empty($this->fromdate)) {
            $criteria->addCondition("created_date between '" . date('Y-m-d', strtotime($this->fromdate)) . "' and  '" . date('Y-m-d', strtotime($this->todate)) . "'");
        }
        return new CActiveDataProvider($this, array(
            // 'pagination' => array('pageSize' => 25,),
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'created_date asc',
            ),
            'pagination' => false,
        ));
    }

    public function newsearch()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('pid', $this->pid);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('start_date', $this->start_date, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        $criteria->compare('updated_by', $this->updated_by);

        return new CActiveDataProvider($this, array(
            'pagination' => array('pageSize' => 25,),
            'criteria' => $criteria, 'sort' => array(
                'defaultOrder' => 'pid desc',
            ),
        ));
    }




    //////created by arun on 14-10-16 starting from here
    public function getAssignedusers($id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $users = Yii::app()->db->createCommand()
            ->select(['c_user.first_name as username'])
            ->from($tblpx . 'projects as p_proj')
            ->leftJoin($tblpx . 'project_assign as p_asign', 'p_proj.pid = p_asign.projectid')
            ->leftJoin($tblpx . 'users as c_user', 'p_asign.userid = c_user.userid')
            ->where('p_proj.pid=:res_projct_id', array(':res_projct_id' => $id))
            ->queryAll();
        foreach ($users as $user) {
            $username[] = $user['username'];
        }
        $usrnames = "";
        if ($username != NULL) {
            $usrnames = implode(', ', $username);
        }
        return $usrnames;
    }
    //////created by arun on 14-10-16 ends here
    /*------------------MOdified By Rajisha----------------*/
    public function getClientname($id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $users = Yii::app()->db->createCommand()
            ->select(['name'])
            ->from($tblpx . 'clients')
            ->where('cid=:cid', array(':cid' => $id))
            ->queryRow();
        return $users['name'];
    }
    public function getStatusname($id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $users = Yii::app()->db->createCommand()
            ->select(['caption'])
            ->from($tblpx . 'status')
            ->where('sid=:sid', array(':sid' => $id))
            ->queryRow();
        return $users['caption'];
    }
    /*------------------MOdified By Rajisha----------------*/
    public function customsearch($userid = 0, $pid = 0)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        //        $criteria->with = array("project_assigned" => array(
        //            'together' => true,
        //            //'select' => false,
        //        ),);

        $criteria->distinct = true;
        // $this->ex_percent = $this->getPercentageExpense(328);
        $criteria->compare('pid', $this->pid);
        $criteria->compare('client_id', $this->client_id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_date', $this->updated_date, true);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('ex_percent', $this->ex_percent);
        //$criteria->compare('project_assigned.userid',($userid==0?$this->userid:$userid));
        //$data = Yii::app()->db->createCommand("SELECT sum(e.amount) AS tot,e.*,p.* FROM `{$tblpx}expenses` e JOIN {$tblpx}projects p ON p.pid = e.projectid WHERE e.type='72' AND e.type='73'")->queryAll();
        //        foreach($data as $key => $value) {
        //    $data[$key]['ex_percent'] = $this->getPercentageExpense($value['pid']);
        //}
        //$sort = new CSort();
        //$sort->attributes = array(
        //    'ex_percent'=>array(
        //        'asc'=>'ex_percent ASC',
        //        'desc'=>'ex_percent DESC',
        //    ),
        //    '*', 
        //);
        return new CActiveDataProvider($this, array(
            'pagination' => array('pageSize' => 25,),
            'criteria' => $criteria //,'sort' => $sort,
        ));

        //echo "<pre>";
        //print_r($data);exit;
        //$dataprovider = new CArrayDataProvider($data, array(
        //   // 'id'=>'myDataProvider',
        //    'sort'=>$sort,
        //    'pagination'=>array(
        //        'pageSize'=>10,
        //    ),
        //));
        //return $dataprovider;

    }


    public function financialreport1()
    {
        $criteria = new CDbCriteria;

        /*if (!empty($fromdate) && empty($todate)) {
            ;
            $criteria->addCondition("created_date >= '" . date('Y-m-d', strtotime($fromdate)) . "'");  // date is database date column field
        } elseif (!empty($todate) && empty($fromdate)) {
            $criteria->addCondition("created_date <= '" . date('Y-m-d', strtotime($todate)) . "'");
        } elseif (!empty($todate) && !empty($fromdate)) {
            
            $criteria->addCondition("created_date between '" . date('Y-m-d', strtotime($fromdate)) . "' and  '" . date('Y-m-d', strtotime($todate)) . "'");
        }*/
        $criteria->addCondition("(project_type=87 AND project_status != 85)");

        //$criteria->addCondition("company_id=".Yii::app()->user->company_id."");
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        $criteria->addCondition($newQuery);

        $criteria->compare('name', $this->name, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('description', $this->description, true);

        $criteria->compare('created_by', $this->created_by);

        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('client_id', $this->client_id);

        $criteria->compare('project_type', $this->project_type);
        $criteria->compare('sqft', $this->sqft, true);
        $criteria->compare('sqft_rate', $this->sqft_rate, true);
        $criteria->compare('percentage', $this->percentage, true);
        $criteria->compare('contract', $this->contract);
        $criteria->compare('bill_amount', $this->bill_amount);
        $criteria->compare('remarks', $this->remarks, true);



        return new CActiveDataProvider($this, array(
            'pagination' => array('pageSize' => 25,),
            'criteria' => $criteria, 'sort' => array(
                'defaultOrder' => 'created_date asc',
            ),
        ));
    }

    public function fixedprojects()
    {

        $criteria = new CDbCriteria;

        /*if (!empty($fromdate) && empty($todate)) {
            ;
            $criteria->addCondition("created_date >= '" . date('Y-m-d', strtotime($fromdate)) . "'");  // date is database date column field
        } elseif (!empty($todate) && empty($fromdate)) {
            $criteria->addCondition("created_date <= '" . date('Y-m-d', strtotime($todate)) . "'");
        } elseif (!empty($todate) && !empty($fromdate)) {
            
            $criteria->addCondition("created_date between '" . date('Y-m-d', strtotime($fromdate)) . "' and  '" . date('Y-m-d', strtotime($todate)) . "'");
        }*/

        $criteria->addCondition("project_status != 85");

        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        $criteria->addCondition($newQuery);

        $criteria->compare('name', $this->name, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('description', $this->description, true);

        $criteria->compare('created_by', $this->created_by);

        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('client_id', $this->client_id);
        $criteria->compare('project_status', $this->project_status);
        $criteria->compare('project_type', 86);
        $criteria->compare('sqft', $this->sqft, true);
        $criteria->compare('sqft_rate', $this->sqft_rate, true);
        $criteria->compare('percentage', $this->percentage, true);
        $criteria->compare('contract', $this->contract);
        $criteria->compare('bill_amount', $this->bill_amount);
        $criteria->compare('remarks', $this->remarks, true);



        return new CActiveDataProvider($this, array(
            'pagination' => array('pageSize' => 50,),
            'criteria' => $criteria, 'sort' => array(
                'defaultOrder' => 'created_date asc',
            ),
        ));
    }

    public function financialreportcompleted1()
    {

        $criteria = new CDbCriteria;

        /*if (!empty($fromdate) && empty($todate)) {
            ;
            $criteria->addCondition("created_date >= '" . date('Y-m-d', strtotime($fromdate)) . "'");  // date is database date column field
        } elseif (!empty($todate) && empty($fromdate)) {
            $criteria->addCondition("created_date <= '" . date('Y-m-d', strtotime($todate)) . "'");
        } elseif (!empty($todate) && !empty($fromdate)) {
            
            $criteria->addCondition("created_date between '" . date('Y-m-d', strtotime($fromdate)) . "' and  '" . date('Y-m-d', strtotime($todate)) . "'");
        }*/
        $criteria->addCondition("(project_type=87 AND project_status = 85)");

        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        $criteria->addCondition($newQuery);

        $criteria->compare('name', $this->name, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('description', $this->description, true);

        $criteria->compare('created_by', $this->created_by);

        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('client_id', $this->client_id);

        $criteria->compare('project_type', $this->project_type);
        $criteria->compare('sqft', $this->sqft, true);
        $criteria->compare('sqft_rate', $this->sqft_rate, true);
        $criteria->compare('percentage', $this->percentage, true);
        $criteria->compare('contract', $this->contract);
        $criteria->compare('bill_amount', $this->bill_amount);
        $criteria->compare('remarks', $this->remarks, true);



        return new CActiveDataProvider($this, array(
            'pagination' => array('pageSize' => 25,),
            'criteria' => $criteria, 'sort' => array(
                'defaultOrder' => 'created_date asc',
            ),
        ));
    }

    public function fixedprojectscompleted()
    {

        $criteria = new CDbCriteria;

        /*if (!empty($fromdate) && empty($todate)) {
            ;
            $criteria->addCondition("created_date >= '" . date('Y-m-d', strtotime($fromdate)) . "'");  // date is database date column field
        } elseif (!empty($todate) && empty($fromdate)) {
            $criteria->addCondition("created_date <= '" . date('Y-m-d', strtotime($todate)) . "'");
        } elseif (!empty($todate) && !empty($fromdate)) {
            
            $criteria->addCondition("created_date between '" . date('Y-m-d', strtotime($fromdate)) . "' and  '" . date('Y-m-d', strtotime($todate)) . "'");
        }*/

        $criteria->addCondition("project_status = 85");

        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        foreach ($arrVal as $arr) {
            if ($newQuery) $newQuery .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
        }
        $criteria->addCondition($newQuery);

        $criteria->compare('name', $this->name, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('description', $this->description, true);

        $criteria->compare('created_by', $this->created_by);

        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('client_id', $this->client_id);
        $criteria->compare('project_status', $this->project_status);
        $criteria->compare('project_type', 86);
        $criteria->compare('sqft', $this->sqft, true);
        $criteria->compare('sqft_rate', $this->sqft_rate, true);
        $criteria->compare('percentage', $this->percentage, true);
        $criteria->compare('contract', $this->contract);
        $criteria->compare('bill_amount', $this->bill_amount);
        $criteria->compare('remarks', $this->remarks, true);



        return new CActiveDataProvider($this, array(
            'pagination' => array('pageSize' => 25,),
            'criteria' => $criteria, 'sort' => array(
                'defaultOrder' => 'created_date asc',
            ),
        ));
    }


    public function completed()
    {

        $criteria = new CDbCriteria;

        /*if (!empty($fromdate) && empty($todate)) {
            ;
            $criteria->addCondition("created_date >= '" . date('Y-m-d', strtotime($fromdate)) . "'");  // date is database date column field
        } elseif (!empty($todate) && empty($fromdate)) {
            $criteria->addCondition("created_date <= '" . date('Y-m-d', strtotime($todate)) . "'");
        } elseif (!empty($todate) && !empty($fromdate)) {
            
            $criteria->addCondition("created_date between '" . date('Y-m-d', strtotime($fromdate)) . "' and  '" . date('Y-m-d', strtotime($todate)) . "'");
        }*/

        $criteria->compare('name', $this->name, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('description', $this->description, true);

        $criteria->compare('created_by', $this->created_by);

        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('client_id', $this->client_id);
        $criteria->compare('project_status', 85);
        $criteria->compare('project_type', $this->project_type);
        $criteria->compare('sqft', $this->sqft, true);
        $criteria->compare('sqft_rate', $this->sqft_rate, true);
        $criteria->compare('percentage', $this->percentage, true);
        $criteria->compare('contract', $this->contract);
        $criteria->compare('bill_amount', $this->bill_amount);
        $criteria->compare('remarks', $this->remarks, true);



        return new CActiveDataProvider($this, array(
            'pagination' => array('pageSize' => 25,),
            'criteria' => $criteria, 'sort' => array(
                'defaultOrder' => 'created_date asc',
            ),
        ));
    }

    public function paymentreportcash()
    {

        $criteria = new CDbCriteria;

        /*if (!empty($fromdate) && empty($todate)) {
            ;
            $criteria->addCondition("created_date >= '" . date('Y-m-d', strtotime($fromdate)) . "'");  // date is database date column field
        } elseif (!empty($todate) && empty($fromdate)) {
            $criteria->addCondition("created_date <= '" . date('Y-m-d', strtotime($todate)) . "'");
        } elseif (!empty($todate) && !empty($fromdate)) {
            
            $criteria->addCondition("created_date between '" . date('Y-m-d', strtotime($fromdate)) . "' and  '" . date('Y-m-d', strtotime($todate)) . "'");
        }*/
        $criteria->addCondition("(project_status=83 OR project_status=84)");

        $criteria->compare('name', $this->name, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('description', $this->description, true);

        $criteria->compare('created_by', $this->created_by);

        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('client_id', $this->client_id);

        $criteria->compare('project_type', $this->project_type);
        $criteria->compare('sqft', $this->sqft, true);
        $criteria->compare('sqft_rate', $this->sqft_rate, true);
        $criteria->compare('percentage', $this->percentage, true);
        $criteria->compare('contract', $this->contract);
        $criteria->compare('bill_amount', $this->bill_amount);
        $criteria->compare('remarks', $this->remarks, true);



        return new CActiveDataProvider($this, array(
            'pagination' => array('pageSize' => 25,),
            'criteria' => $criteria, 'sort' => array(
                'defaultOrder' => 'created_date asc',
            ),
        ));
    }

    public function paymentreportbank()
    {

        $criteria = new CDbCriteria;

        /*if (!empty($fromdate) && empty($todate)) {
            ;
            $criteria->addCondition("created_date >= '" . date('Y-m-d', strtotime($fromdate)) . "'");  // date is database date column field
        } elseif (!empty($todate) && empty($fromdate)) {
            $criteria->addCondition("created_date <= '" . date('Y-m-d', strtotime($todate)) . "'");
        } elseif (!empty($todate) && !empty($fromdate)) {
            
            $criteria->addCondition("created_date between '" . date('Y-m-d', strtotime($fromdate)) . "' and  '" . date('Y-m-d', strtotime($todate)) . "'");
        }*/
        $criteria->addCondition("(project_status=83 OR project_status=84)");

        $criteria->compare('name', $this->name, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('description', $this->description, true);

        $criteria->compare('created_by', $this->created_by);

        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('client_id', $this->client_id);

        $criteria->compare('project_type', $this->project_type);
        $criteria->compare('sqft', $this->sqft, true);
        $criteria->compare('sqft_rate', $this->sqft_rate, true);
        $criteria->compare('percentage', $this->percentage, true);
        $criteria->compare('contract', $this->contract);
        $criteria->compare('bill_amount', $this->bill_amount);
        $criteria->compare('remarks', $this->remarks, true);



        return new CActiveDataProvider($this, array(
            'pagination' => array('pageSize' => 25,),
            'criteria' => $criteria, 'sort' => array(
                'defaultOrder' => 'created_date asc',
            ),
        ));
    }
    public static function getCreditAmount($pid = 0)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $credit = Yii::app()->db->createCommand('select sum(amount) as credit from ' . $tblpx . 'expenses
	where type=72 and projectid=' . $pid)->queryScalar();
        return $credit;
    }
    public static function getDebitAmount($pid = 0)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $debit = Yii::app()->db->createCommand('select sum(amount) as debit from ' . $tblpx . 'expenses
	where type=73 and projectid=' . $pid)->queryScalar();
        return $debit;
    }
    public static function getReceiptAmount($pid = 0)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $receipt = Yii::app()->db->createCommand("SELECT SUM(amount) as recamount FROM {$tblpx}expenses WHERE projectid = {$pid} AND type=72")->queryScalar();
        return $receipt;
    }

    public function getPercentageExpense($id)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $receipt = Yii::app()->db->createCommand("SELECT SUM(amount) as recamount FROM {$tblpx}expenses WHERE projectid = {$id} AND type=72")->queryScalar();
        //$expense = Yii::app()->db->createCommand("SELECT SUM(amount) as recamount FROM {$tblpx}expenses WHERE projectid = {$id} AND type=73")->queryScalar();
        $debit = Yii::app()->db->createCommand('select sum(amount) as debit from ' . $tblpx . 'expenses
	where type=73 and projectid=' . $pid)->queryScalar();

        //        if($expense > 0){//echo $receipt.'---'.$expense;
        //            echo  number_format($receipt/$expense*100).'%';
        //        }
        if ($receipt > 0) {
            echo  number_format($receipt / $debit * 100) . '%';
        }
    }
    public static function getexpenseAmount($pid = 0)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $expense = Yii::app()->db->createCommand("SELECT SUM(amount) as recamount FROM {$tblpx}expenses WHERE projectid = {$pid} AND type=73")->queryScalar();
        return $expense;
    }
    public static function getclientAmount($pid = 0)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $clientamount = Yii::app()->db->createCommand('select sum(amount) as clientamount from ' . $tblpx . 'expenses
	where type=72 and projectid=' . $pid)->queryScalar();
        return $clientamount;
    }
    public static function getvendorAmount($pid = 0)
    {
        $tblpx = Yii::app()->db->tablePrefix;
        $vendoramount = Yii::app()->db->createCommand('select sum(paid) as vendoramount from ' . $tblpx . 'expenses
	where type=73 and projectid=' . $pid)->queryScalar();
        return $vendoramount;
    }

    public function getCreatedBy($id)
    {
        $project_model = Projects::model()->findByPk($id);

        return isset($project_model->createdBy)?$project_model->createdBy->first_name:'';
    }

    public function receiptAmount($pid)
    {
        $expenses = Expenses::model()->find(array(
            'select' => 'projectid, SUM(receipt) as receipt',
            'condition' => 'projectid = ' . $pid . ' AND type = 72',

        ));
        return !empty($expenses['receipt']) ? $expenses['receipt'] : 0;
    }

    public function quotationAmount($pid)
    {
        // $quotation = Quotation::model()->find(array(
        //     'select' => 'project_id, SUM(amount) as amount',
        //     'condition' => 'project_id = ' . $pid,

        // ));
       $newQuery2 = "";
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);

        foreach ($arrVal as $index => $arr) {
            if ($index > 0) {
                $newQuery2 .= ' OR ';
            }
            $newQuery2 .= "FIND_IN_SET('" . $arr . "', company_id)";
        }

        $quotation = Invoice::model()->find(array(
            'select' => 'project_id, SUM(amount + tax_amount) as amount',
            'condition' => 'project_id = :pid AND (  invoice_status ="saved" ) AND (' . $newQuery2 . ')',
            'params' => array(':pid' => $pid),
        ));

        return !empty($quotation['amount']) ? $quotation['amount'] : 0;
    }

    public function getInvoiceNumber($invoice_id)
    {
        $invoice_details = Invoice::model()->findByPk($invoice_id);
        return !empty($invoice_details) ? $invoice_details->inv_no : '';
    }

    public function getProjectTotalExpense($project_model)
    {
       
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $project_id=$project_model->pid;
           $tblpx = Yii::app()->db->tablePrefix;
          $arrVal = explode(',', $user->company_id);
          $newQuery = "";
          $newQuery1 = "";
          $newQuery3="";
          $newQuery4="";
          foreach ($arrVal as $arr) {
              if ($newQuery) $newQuery .= ' OR';
              if ($newQuery1) $newQuery1 .= ' OR';
              if ($newQuery3) $newQuery3 .= ' OR';
              if ($newQuery4) $newQuery4 .= ' OR';
              $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
              $newQuery1 .= " FIND_IN_SET('" . $arr . "', {$tblpx}invoice.company_id)";
              $newQuery3 .= " FIND_IN_SET('" . $arr . "', E.company_id)";
              $newQuery4 .= " FIND_IN_SET('" . $arr . "', jp_purchase.company_id)";
          }
          $bill_amount=0;
            
           /* Purchase Bill Starts*/
           $unreconciled_datasql = "SELECT O.*, E.*  FROM jp_reconciliation O "
                . " JOIN jp_expenses E ON O.reconciliation_parentid = E.exp_id  "
                . " WHERE O.reconciliation_status=0 and E.projectid=$project_id  and (" . $newQuery3 . ") ";
            $unreconciled_data = Yii::app()->db->createCommand($unreconciled_datasql)->queryAll();

            $bill_array = array();

            foreach ($unreconciled_data as  $data) {                
                $recon_bill = "'{$data["bill_id"]}'";                
                array_push($bill_array, $recon_bill);
            }
           
            $bill_array1 = implode(',', $bill_array);

            $pendingsql = 'SELECT bill_id FROM jp_expenses '
            . ' WHERE projectid ='. $project_id
            . ' AND  reconciliation_status =1 OR  reconciliation_status IS NULL ';
            $pending_bills_array = Yii::app()->db->createCommand($pendingsql)->queryAll();

            $pending_bills =array();
            foreach ($pending_bills_array as  $data) {                
                $billid = "'{$data["bill_id"]}'";                
                array_push($pending_bills, $billid);
            }
            $pending_bills1 = implode(',', $pending_bills);
            if ($bill_array1) {
                $bill_condition = " 1=1";
                $bill_condition1 = " 1=1";
                if(!empty($pending_bills1)){
                    $bill_condition = " bill_id IN($pending_bills1)";
                    $bill_condition1 = "bill_id NOT IN($pending_bills1)";
                }

                $purchasesql = "SELECT * FROM  " . $tblpx . "bills "
                    . " LEFT JOIN " . $tblpx . "purchase "
                    . " ON " . $tblpx . "bills.purchase_id=" . $tblpx . "purchase.p_id "
                    . " WHERE " . $tblpx . "purchase.project_id = " . intval($project_id) . " 
                AND "
                    . $tblpx . "bills.purchase_id IS NOT NULL  "
                    . " AND (" . $newQuery4 . ") AND bill_id NOT IN ($bill_array1) "
                    . " AND ".$bill_condition;                    
                $purchase = Yii::app()->db->createCommand($purchasesql)->queryAll(); //reconcil


                $purchase1sql = "SELECT * FROM  " . $tblpx . "bills "
                    . " LEFT JOIN " . $tblpx . "purchase "
                    . " ON " . $tblpx . "bills.purchase_id=" . $tblpx . "purchase.p_id "
                    . " WHERE " . $tblpx . "purchase.project_id = " . intval($project_id) . " 
                AND "
                    .  $tblpx . "bills.purchase_id IS NOT NULL  "
                    . " AND (" . $newQuery4 . ")  AND ".$bill_condition1;                    
                $purchase1 = Yii::app()->db->createCommand($purchase1sql)->queryAll(); //un reconcil
            } else {

                $bill_condition = " 1=1";
                $bill_condition1 = " 1=1";
                if(!empty($pending_bills1)){
                    $bill_condition = " bill_id IN($pending_bills1)";
                    $bill_condition1 = "bill_id NOT IN($pending_bills1)";
                }

                $purchasesql = "SELECT * FROM  " . $tblpx . "bills "
                    . " LEFT JOIN " . $tblpx . "purchase "
                    . " ON " . $tblpx . "bills.purchase_id=" . $tblpx . "purchase.p_id "
                    . " WHERE " . $tblpx . "purchase.project_id = " . intval($project_id) . " 
                AND "
                     . $tblpx . "bills.purchase_id IS NOT NULL  "
                    . " AND (" . $newQuery4 . ")  and ".$bill_condition;
                $purchase = Yii::app()->db->createCommand($purchasesql)->queryAll();

                $purchase1sql = "SELECT * FROM  " . $tblpx . "bills "
                    . " LEFT JOIN " . $tblpx . "purchase "
                    . " ON " . $tblpx . "bills.purchase_id=" . $tblpx . "purchase.p_id "
                    . " WHERE " . $tblpx . "purchase.project_id = " . intval($project_id) . " 
                AND "
                    . $tblpx . "bills.purchase_id IS NOT NULL  "
                    . " AND (" . $newQuery4 . ")  and ".$bill_condition1;
                $purchase1 = Yii::app()->db->createCommand($purchase1sql)->queryAll(); //un reconcil
            }

            $bill_amount = 0;
            $purchase_list = Projects::model()->groupPurchaseBillItems($purchase);
            

            foreach ($purchase_list as $purchase) {
                    $count = count($purchase);
                    $count = ($count - 2);
                    $index = 1;
                    foreach ($purchase as $key => $value) {
                         if (is_array($value)) {
                            $amount =  ($value['bill_totalamount'] + $value['bill_additionalcharge']);
                           
                            $bill_amount += $amount; 
                        }
                     $index++;
                    }
            }
        $criteria = new CDbCriteria;
        $criteria->select    = 'sum(bill_totalamount) as bill_totalamount,sum(bill_additionalcharge) as bill_additionalcharge';
        $criteria->join      = 'LEFT JOIN ' . Yii::app()->Controller->tableName('Purchase') . ' ON t.purchase_id=' . Yii::app()->Controller->tableName('Purchase') . '.p_id ';
        $criteria->condition = '' . Yii::app()->Controller->tableName('Purchase') . '.project_id=:p_id AND  t.purchase_id IS NOT NULL AND(' . $newQuery . ')';
        $criteria->params    = array(':p_id' => $project_model['pid']);
        //$bill_amount = Bills::model()->find($criteria);
        $expense_total = Expenses::model()->find(array('select' => 'sum(paidamount) as amount', 'condition' => 'amount != 0 AND projectid = ' . $project_model['pid'] . ' AND exptype IS NOT NULL AND vendor_id IS NOT NULL AND bill_id IS NULL AND(' . $newQuery . ')'));
        $subcontractor_total = Expenses::model()->find(array('select' => 'sum(amount) as amount', 'condition' => 'amount != 0 AND TYPE = "73" AND projectid = ' . $project_model['pid'] . ' AND exptype IS  NULL AND vendor_id IS  NULL AND bill_id IS NULL AND(' . $newQuery . ')'));
        $project_det=Projects::Model()->findByPk($project_model['pid']);
        if(!empty($project_det)){
            $pro_company_str=$project_det->company_id;
            if(!empty($pro_company_str)){
                 $company_arr=explode(',',$pro_company_str);
                 $company = $company_arr['0'];
            }
        }
                   
        if(!empty($company)){
            $company_det=Company::Model()->findByPk($company);
            $exp_cal =$company_det->expense_calculation;
                       
        }
        $recon_status = 1;
        $project_id= $project_model['pid'];
        $noSublabourAmount=0;
        $consumption_amount=0;
         $ids_array = array();
        if($exp_cal == 1){
           
           $consumptionReqCriteria= new CDbCriteria();
                 $warehouse_ids = Warehouse::model()->findAll(array('condition' => 'project_id = ' . $project_id));
                 if (!empty($warehouse_ids)) {
                    foreach ($warehouse_ids as $warehouse) {
                        array_push($ids_array, $warehouse['warehouse_id']);
                    }
                    $warehouse_ids = implode(',', $ids_array);
                 }
                 if (!empty($warehouse_ids)) {
                          
                  $consumptionReqCriteria->condition="warehouse_id IN (" . $warehouse_ids . ")"; 
                
                 }
                 $consumptionReqCriteria->addCondition('status =1');
                 $consumptionReqCriteria->addCondition('coms_project_id = '.$project_id);
                 $consumptionreq_details = ConsumptionRequest::model()->findAll($consumptionReqCriteria);
                 if(!empty($consumptionreq_details)){
                        foreach ($consumptionreq_details as $datas) {
                            $consumptionreq = PmsAccWprItemConsumed::model()->findAll('consumption_id = :consumption_id', array(':consumption_id' => $datas->id));
                            foreach($consumptionreq as $req){
                                $rate=$req['item_qty'] * $req['item_rate'];
                                $consumption_amount  +=  $rate;
                            }
                            
                        }
                    }
            
        }
      

        $sql = "
            SELECT 
                SUM(amount) AS total_amount
            FROM 
                jp_dailyreport
            WHERE 
                projectid = $project_id
                AND approval_status = '$recon_status'
                AND approve_status = 1
                AND (subcontractor_id IS NULL OR subcontractor_id = -1)
        ";
            $command = Yii::app()->db->createCommand($sql);


             $result = $command->queryRow();


        $total_labour_amount = isset($result['total_amount']) ? $result['total_amount'] : 0;

        if($exp_cal == 0){
             $bill_total_amount = $bill_amount + $expense_total['amount'] + $subcontractor_total['amount'] +$total_labour_amount;
        }else{
             $bill_total_amount =  $expense_total['amount'] + $subcontractor_total['amount'] +$consumption_amount+$total_labour_amount;

        }
       
        return isset($bill_total_amount) ? $bill_total_amount : '0';
    }

    public function groupPurchaseBillItems($purchase_datas)
    {
        $data = array();
        foreach ($purchase_datas as $purchase_data) {
            if (!array_key_exists($purchase_data['expensehead_id'], $data)) {
                $amount = 0;
                $data[$purchase_data['expensehead_id']][] = $purchase_data;
                $expense_model =  ExpenseType::model()->findByPk($purchase_data['expensehead_id']);
                $data[$purchase_data['expensehead_id']]['expense_head'] = $expense_model['type_name'];
                $data[$purchase_data['expensehead_id']]['total_amount'] = $amount;
            } else {
                array_push($data[$purchase_data['expensehead_id']], $purchase_data);
            }

            $amount = $purchase_data['bill_totalamount'] + $purchase_data['bill_additionalcharge'];
            $data[$purchase_data['expensehead_id']]['total_amount'] += $amount;
        }
        return $data;
    }

    public function setPurchaseReturn($return_list)
    {
        $data = array();

        foreach ($return_list as $list) {
            $return_model = PurchaseReturn::model()->findByPk($list['return_id']);
            $return_bill = Bills::model()->findByPk($return_model['bill_id']);
            $list['return_number'] = $return_model['return_number'];
            $list['bill_number'] = $return_bill['bill_number'];
            $list['bill_id'] = $return_bill['bill_id'];
            if ($list['payment_type'] == 107) {
                $data['credit'][] = $list;
            } else {
                $data['other'][] = $list;
            }
        }
        return $data;
    }
    public function setExpenseTypesListingofPurchaseBill($lists)
    {
        $data = array();
        foreach ($lists as $key => $list) {
            if (!array_key_exists($key, $data)) {
                $data[$key]['name'] = $list['expense_head'];
                $data[$key]['amount'] = $list['total_amount'];
            }
        }
        return $data;
    }

    public function setExpenseList($list_array, $expense)
    {
        if (!array_key_exists($expense['exptype'], $list_array)) {

            $expense_model = ExpenseType::model()->findByPk($expense['exptype']);
            $list_array[$expense['exptype']]['name'] = $expense_model['type_name'];
            $list_array[$expense['exptype']]['amount'] = $expense['paid'];
        } else {
            $list_array[$expense['exptype']]['amount'] +=  $expense['paid'];
        }
        return $list_array;
    }

    public function  setSubcontractorPayment($datas)
    {
        $final_data = array();
        foreach ($datas as $data) {
            $payment = SubcontractorPayment::model()->findByPk($data['subcontractor_id']);
            if ($payment['payment_quotation_status'] == 1) {
                $sc_item = Scquotation::model()->findByAttributes(array('scquotation_id' => $payment['quotation_number']));
                $expense_head = ExpenseType::model()->findByPk($sc_item['expensehead_id']);
                $expense_head_id = $sc_item['expensehead_id'];
                $expense_head = $expense_head['type_name'];
            } else {
                $expense_head = 'N/A';
                $expense_head_id = 0;
            }
            $data['expense_head_name'] = $expense_head;
            if (!array_key_exists($expense_head_id, $final_data)) {
                $amount = 0;
                $final_data[$expense_head_id][] = $data;
                $final_data[$expense_head_id]['total_amount'] = $amount;
            } else {
                array_push($final_data[$expense_head_id], $data);
            }
            $final_data[$expense_head_id]['total_amount'] += $data['paidamount'];
        }
        return $final_data;
    }

    public static function getMaterialProjectName($pms_pjct_id){
        $sql = "SELECT acc_project_id FROM `pms_acc_project_mapping` "
                . " WHERE pms_project_id = ".$pms_pjct_id;
        $acc_pjct_id  = Yii::app()->db->createCommand($sql)->queryScalar();

        $sql = "SELECT name FROM `pms_projects` WHERE `pid` =".$pms_pjct_id;
        $pms_pjct_name = Yii::app()->db->createCommand($sql)->queryScalar();

        $sql = "SELECT name FROM `jp_projects` WHERE `pid` =".$acc_pjct_id;
        $acc_pjct_name = Yii::app()->db->createCommand($sql)->queryScalar();
        return $acc_pjct_name;
    }

    public static function getMaterialProjectArray(){
        $sql = "SELECT mp.acc_project_id,mr.requisition_project_id,p.name "
            . " FROM `pms_material_requisition` mr "
            . " LEFT JOIN pms_acc_project_mapping mp "
            . " ON mr.requisition_project_id = mp.pms_project_id "
            . " LEFT JOIN jp_projects p ON mp.acc_project_id = p.pid "
            . " GROUP BY mr.requisition_project_id";
        $projectData = Yii::app()->db->createCommand($sql)->queryAll();
        $data = CHtml::listData($projectData, 'requisition_project_id', 'name');
        return $data;
    }
    public function setSubcontractorPaymentForBill($datas)
    {
        $final_data = array();
    
        foreach ($datas as $data) {
            $billsmodelcriteria = new CDbCriteria();
            $billsmodelcriteria->addCondition('scquotation_id = :scquotation_id');
            $billsmodelcriteria->params[':scquotation_id'] = $data["scquotation_id"];
            $billsmodelcriteria->order = 'id ASC';
    
            $billsmodel_details = Subcontractorbill::model()->findAll($billsmodelcriteria);
    
            foreach ($billsmodel_details as $bills) {
                $sc_bill = Subcontractorbill::model()->findByPk($bills['id']);
                $bill_amount = $sc_bill['total_amount'];
                $sql = "SELECT SUM(amount) FROM `jp_subcontractor_payment` WHERE sc_bill_id = " . $bills['id'];
                $paidamount = Yii::app()->db->createCommand($sql)->queryScalar();
    
                // Initialize necessary keys
                $expense_head_id = $data['expensehead_id'];
                $expense_head = ExpenseType::model()->findByPk($expense_head_id);
                $expense_head_name = $expense_head ? $expense_head['type_name'] : 'N/A';
    
                // Prepare data
                $data['expense_head_name'] = $expense_head_name;
                $data['bill_id'] = isset($sc_bill) ? $bills['id'] : '';
                $data['bill_no'] = isset($sc_bill) ? $sc_bill['bill_number'] : '';
                $data['bill_amount'] = isset($sc_bill) ? $sc_bill['total_amount'] : 0;
                $data['bill_date'] = isset($sc_bill) ? $sc_bill['date'] : '';
                $data['quot_no'] = isset($data) ? $data['scquotation_no'] : '';
                $data['paid_amount'] = $paidamount;
                $data['balance'] = $bill_amount - $paidamount;
    
                // Filter and collect pending and partially paid bills
                if ($paidamount == 0 || $paidamount < $bill_amount) {
                    if (!array_key_exists($expense_head_id, $final_data)) {
                        $final_data[$expense_head_id] = [
                            //'items' => [],
                            'paid_amount' => 0,
                            'bill_total_amount' => 0,
                        ];
                    }
    
                    // Append the data to the final_data array
                    $final_data[$expense_head_id][] = $data;
                    $final_data[$expense_head_id]['paid_amount'] += $data['paid_amount'];
                    $final_data[$expense_head_id]['bill_total_amount'] += $data['bill_amount'];
                }
            }
        }
        //echo "<pre>";print_r($final_data);exit;
        return $final_data;
    }
    
}
