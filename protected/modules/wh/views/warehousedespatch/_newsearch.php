<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<div class="page_filter">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-3 col-xl-2 form-group">
            <label>Despatch No </label>
            <input type="text" name="despatch_no" id="despatch_no" value="<?php echo $model->warehousedespatch_no ?>"
                placeholder="Despatch No" class="form-control">
        </div>
        <div class="col-xs-12 col-sm-4 col-md-3 col-xl-2 form-group">
            <label>Warehouse From</label>
            <?php
            if (yii::app()->user->role == 5) {
                $warehouse_data = Controller::getCategoryOptions($type = 1);
                $data = CHtml::listData(Controller::getCategoryOptions($type = 1), 'id', 'text', 'group');
            } else {
                $warehouse_data = Controller::getCategoryOptions();
                $data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group');
            }

            if (!empty($model->warehousedespatch_warehouseid)) {
                $warehouse_from = $model->warehousedespatch_warehouseid;
            } else {
                if (count($warehouse_data) == 1) {
                    $warehouse_from = $warehouse_data[0]['id'];
                } else {
                    $warehouse_from = '';
                }
                // $warehouse_from = '';
            }
            echo CHtml::dropDownList(
                'warehouse',
                $warehouse_from,
                $data,
                array('empty' => 'Choose Warehouse From', 'class' => ' warehouse warehouse-from form-control')
            );
            ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-3 col-xl-2 form-group">
            <label>Warehouse To</label>
            <?php
            $search_item_id = $item_id;
            if (!empty($model->warehousedespatch_warehouseid_to)) {
                $warehouse_to = $model->warehousedespatch_warehouseid_to;
            } else {
                $warehouse_to = '';
            }

            $data = CHtml::listData(Controller::getCategoryOptions(), 'id', 'text', 'group');
            echo CHtml::dropDownList(
                'warehouse_to',
                $warehouse_to,
                $data,
                array('empty' => 'Choose Warehouse To', 'class' => ' warehouse_to form-control')
            );
            ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-3 col-xl-2 form-group">
            <label>Item </label>
            <select id="item_id" class="select_box form-control" name="item_id" class="sel">
                <option value="">Select Item</option>
                <?php

                if (!empty($specification)) {
                    foreach ($specification as $key => $value) {
                        ?>
                        <option value="<?php echo $value['id'] ?>" <?php echo ($value['id'] == $search_item_id) ? 'selected' : ''; ?>>
                            <?php echo $value['data']; ?>
                        </option>
                    <?php }
                } ?>
            </select>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-3 col-xl-2 form-group">
            <label>Clerk </label>
            <select id="clerk" class="select_box form-control" name="clerk">
                <option value="">Select Clerk</option>
                <?php
                if (!empty($clerk)) {
                    foreach ($clerk as $key => $value) {
                        ?>
                        <option value="<?php echo $value['userid'] ?>" <?php echo ($value['userid'] == $model->warehousedespatch_clerk) ? 'selected' : ''; ?>>
                            <?php echo $value['first_name'] . ' ' . $value['last_name']; ?>
                        </option>

                    <?php }
                } ?>
            </select>

        </div>
        <div class="col-xs-12 col-sm-4 col-md-3 col-xl-2 form-group">
            <?php
            if (!isset($_GET['date_from']) || $_GET['date_from'] == '') {
                // $datefrom = date("Y-m-") . "01";
                $datefrom = "";
            } else {
                $datefrom = $_GET['date_from'];
            }?>

            <label>From </label>
            <?php echo CHtml::textField('date_from', $datefrom, array("id" => "date_from", "readonly" => true, "placeholder" => "From","class"=>"form-control")); ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-3 col-xl-2 form-group">
            <?php
            if (!isset($_GET['date_to']) || $_GET['date_to'] == '') {
                $date_to = "";
            } else {
                $date_to = $_GET['date_to'];
            }?>
            <label>To </label>
            <?php echo CHtml::textField('date_to', $date_to, array("id" => "date_till", "readonly" => true, "placeholder" => "To","class"=>"form-control")); ?>
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-2 text-sm-left text-right">
            <label class="d-sm-block d-none">&nbsp;</label>
            <div>           
                <input name="yt0" value="Go" type="submit" class="btn btn-info btn-sm">
                <input id="reset" name="yt1" value="Clear" type="reset" class="btn btn-default btn-sm">
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script>
    $('#reset').click(function () {
        location.href = '<?php echo $this->createUrl('index'); ?>';
    })
    $(function () {
        $("#date_from").datepicker({ dateFormat: 'dd-mm-yy' });

        $("#date_till").datepicker({ dateFormat: 'dd-mm-yy' });

        $("#date_from").change(function () {
            $("#date_till").datepicker('option', 'minDate', $(this).val());
        });

        $("#date_till").change(function () {
            $("#date_from").datepicker('option', 'maxDate', $(this).val());
        });


    });

    $(document).ready(function () {
        $(".select_box").select2();
        $("#warehouse_to").select2();
        $("#warehouse").select2();
    });

</script>
