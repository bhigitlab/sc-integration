<div class="card-wrapper">
    <div class="card">
        <div>
            <h2 class="welcome">Welcome</h2>
            <p class="welcome-para">Log in to continue</p>
        </div>
        <?php
        $form = $this->beginWidget(
                'CActiveForm',
                array(
                    'id' => 'login-form',
                    'enableClientValidation' => true,
//                    'clientOptions' => array('validateOnSubmit' => true),
                    'htmlOptions' => array('class' => 'card-form')
                )
        );
        ?>
        <?php //if($model->hasErrors()){  ?>
        <!--		<div class="alert alert-danger display-hide" style="display:block">
                <button class="close" data-close="alert"></button>
        <?php echo $form->errorSummary($model); ?>
        </div>-->
        <?php //}  ?>
        <div class="card-input-container username">
            <?php echo $form->textField($model, 'username', array('placeholder' => "Enter your username")); ?>
            <div class="errorDiv">
                <?php echo $form->error($model, 'username'); ?>
            </div>
        </div>
        <div class="card-input-container password">
            <?php echo $form->passwordField($model, 'password', array('placeholder' => "Enter your password")); ?>
            <div class="errorDiv">
                <?php echo $form->error($model, 'password'); ?>
            </div>
        </div>
        <div class="form-check margin-top-10">
            <?php echo $form->checkBox($model, 'rememberMe', array('class' => 'form-check-input')); ?>
            <label class="form-check-label"> Remember Me </label>
        </div>
        <button type="submit" class="card-button">Continue</button>
        <div class="password-wrapper">
                <!-- <p class="forgot-pw">Forgot Password?</p>
        <a class="reset-pw">Reset Password</a> -->
        </div>
        <br>
        <hr class="line" />
        <a class="text-center privacy">Privacy Policy</a>
        <p class="text-center copyright">© 2025 Blue Horizon Infotech</p>
        <?php if (ENV != 'production') {
            ?>
            <p class="text-center" style="background-color: red;color:white;margin-top:5px;">Staging Environment</p>
        <?php } ?>
        <?php $this->endWidget(); ?>
        <!-- END FORGOT PASSWORD FORM -->
    </div>
</div>