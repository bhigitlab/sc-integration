<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'project Type',
);
?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<div class="container" id="expensetype">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix">
        <div class="add-btn pull-right">
            <?php
            if (isset(Yii::app()->user->role) && (in_array('/projectType/createprojecttype', Yii::app()->user->menuauthlist))) {
            ?>
                
                <a class="button addtype">Add Client type</a>
            <?php } ?>
        </div>
        <h2>Client Type</h2>
    </div>
   <div id="errMsg"></div>

    <div id="addprotype" style="display:none;"></div>
    <div class="row">
        <div class="col-md-12">
            <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_newview',
                'template' => '<div>{sorter}</div><div class=""><table cellpadding="10" id="pro_typetable" class="table">{items}</table></div>',
            ));
            ?>
        </div>

        <!-- Add Expense type Popup -->
        <div id="addClienttype" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

            </div>
        </div>


        <!-- Edit Expense type Popup -->

        <div class="modal fade edit" role="dialog">
            <div class="modal-dialog modal-lg"></div>
        </div>
        <?php
        Yii::app()->clientScript->registerScript('myjavascript', '
             $(document).ready(function(){
               $("#pro_typetable").dataTable( {
                    "scrollY": "300px",
                    "scrollCollapse": true,
                    "paging": false,
                     "columnDefs"        : [       
                        { 
                            "searchable"    : false, 
                            "targets"       : [0,2] 
                        },
                    ],
                    /*stateSave: true,
                    stateSaveCallback: function(settings,data) {
                        localStorage.setItem( "DataTables_" + settings.sInstance, JSON.stringify(data) )
                      },
                    stateLoadCallback: function(settings) {
                      return JSON.parse( localStorage.getItem( "DataTables_" + settings.sInstance ) )
                      }*/
                    
                } );

                });

            ');
        ?>
        <script>
            function editaction(elem, event) {
                $(elem).parent().hide();

                $(elem).parents().find('td:eq(2)').show();
                var rolename = $(elem).parent().find('.list_item').text();
                var id = $(elem).attr('data-id');
                $('.project_type').val(rolename);
                $(elem).parents().siblings().find('.test').hide();
                $(elem).parents().siblings().find('td:eq(1)').show();
            }

            function cancelaction(elem, event) {

                $(elem).parents().find('.text_black').show();
                $(elem).parents().find('.test').hide();

            }

            function saveaction(elem, event) {
                var value = $(elem).parent().find('.project_type').val();

                var id = $(elem).attr('data-id');
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "<?php echo $this->createUrl('projectType/update2') ?>",
                    data: {
                        id: id,
                        value: value
                    },
                    success: function(response) {
                        if (response == null) {
                            $(elem).parents().find('td:eq(2)').hide();
                            $(elem).parents().find('td:eq(1)').show();
                            $(elem).parent().parent().find('.list_item').text(value);

                            // location.reload();
                        } else {
                            var obj = eval(response);
                            $(".errorMessage1").text(obj);
                        }
                    },


                });
                return false;
            }

            function closeaction() {
                $('#addprotype').slideUp(500);
            }
            $(document).ready(function() {
                $('.addtype').click(function() {
                    $('.loading-overlay').addClass('is-active');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $this->createUrl('projectType/create&layout=1') ?>",
                        success: function(response) {
                            $('#addprotype').html(response).slideDown();
                        },
                    });
                });
            });
            $(document).ajaxComplete(function() {
                $('.loading-overlay').removeClass('is-active');
                $('#loading').hide();
            });

            function onlySpecialchars(str)
            {        
                var regex = /^[^a-zA-Z0-9]+$/;
                var message =""; 
                var disabled=false;     
                var matchedAuthors = regex.test(str);
                
                if (matchedAuthors) {
                    var message ="Special characters not allowed"; 
                    var disabled=true;           
                } 
                return {"message":message,"disabled":disabled};        
            }
      

            $(".project_type").keyup(function () {         
                var response =  onlySpecialchars(this.value);      
                $(this).siblings(".errorMessage").show().html(response.message);    
                $(this).siblings(".save ").attr('disabled',response.disabled);
            });

            function deletClientType(elem,id){
        
                if (confirm("Do you want to remove this item?")) {
                    $('.loading-overlay').addClass('is-active');
                    $.ajax({
                        url: '<?php echo  Yii::app()->createAbsoluteUrl('projectType/removeclientype'); ?>',
                        type: 'POST',
                        dataType:'json',
                        data: {
                            clientId: id
                        },
                        success: function(data) {    
                            console.log(data)                ;
                            $("#errMsg").show()
                                .html('<div class="alert alert-'+data.response+'">'+data.msg+'</div>')
                                .fadeOut(10000);
                            setTimeout(function () {                        
                                location.reload(true);
                            }, 3000);
                                
                        }
                    });
                } else {
                    return false;
                }
                
            }
        </script>
        <style>
            span.text_black {
                color: #333
            }

            .editable {
                float: right;
            }

            .page-body h3 {
                margin: 4px 0px;
                color: inherit;
                text-align: left;
            }

            .panel {
                border: 1px solid #ddd;
                box-shadow: 0px 0px 0px 0px;
            }

            .panel-heading {
                background-color: #eee;
                height: 40px;
            }

            table.dataTable>thead>tr th:last-child {
                width: 40px;
                background-image: none;
            }

            .errorMessage1 {
                color: red;
            }
        </style>
    </div>
</div>