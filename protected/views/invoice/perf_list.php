<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->breadcrumbs=array(
	'performa invoice',)
?>
<div class="container" id="project">
<h2>Performa Invoice</h2>

<?php // $this->renderPartial('_newsearch', array('model' => $model)) ?>

<a href="index.php?r=invoice/addperforma" class="button">Add Performa Invoice</a>



<br>
<br>
<div class="exp-list">
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_perfview',
	//'itemView' => '_perfview', 'template' => '<div>{summary}{sorter}</div><div class="tblwrap scrollTable"><div class="container1"><table cellpadding="10" class="table  list-view sorter">{items}</table></div></div>{pager}',
)); ?>
</div>
</div>

<style>
a.button {
    background-color: #6a8ec7;
    display: block;
    padding: 5px;
    color: #fff;
    cursor: pointer;
    float: right;
    border: 1px solid #6a8ec8;
}
</style>


<script>
    (function () {
        'use strict';
        var container = document.querySelector('.container1');
        var table = document.querySelector('table');
        var leftHeaders = [].concat.apply([], document.querySelectorAll('tbody th'));
        var topHeaders = [].concat.apply([], document.querySelectorAll('thead th'));
        var topLeft = document.createElement('div');
        var computed = window.getComputedStyle(topHeaders[0]);
        container.appendChild(topLeft);
        topLeft.classList.add('top-left');
        topLeft.style.width = computed.width;
        topLeft.style.height = computed.height;

        container.addEventListener('scroll', function (e) {
            var x = container.scrollLeft;
            var y = container.scrollTop;

            leftHeaders.forEach(function (leftHeader) {
                leftHeader.style.transform = translate(x, 0);
            });
            topHeaders.forEach(function (topHeader, i) {
                if (i === 0) {
                    topHeader.style.transform = translate(0, y);
                } else {
                    topHeader.style.transform = translate(0, y);
                }
            });
            topLeft.style.transform = translate(x, y);
        });

        function translate(x, y) {
            return 'translate(' + x + 'px, ' + y + 'px)';
        }
    })();

</script>

<style>
 .container1 {
        width: 100%;
        height: 500px;
        overflow: auto;
        position: relative;
    }

th,
.top-left {
	background: #eee;
}
.top-left {
	box-sizing: border-box;
	position: absolute;
	left: 0;
	border-right: 1px solid #ccc;
	border-bottom: 1px solid #ccc;
}

table {
   
	border-spacing: 0;
	position: absolute;
   
}
th,
td {
	border-right: 1px solid #ccc !important;

}

</style>
