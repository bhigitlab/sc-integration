<?php
$pms_api_integration_model=ApiSettings::model()->findByPk(1);
$pms_api_integration =$pms_api_integration_model->api_integration_settings;
if($type==1){
$arrVal='';
// Fetching data from the database'
$sql="";
if(!empty($template_id) && !empty($scquotation_id)){
    $sql = "SELECT DISTINCT
            t.worktype_id as labour_id ,
            t.worktype_name,
            t.template_id,
            COALESCE(sq.labour_rate, t.worktype_rate) AS labour_rate,
            COALESCE(sq.id, t.worktype_id) AS id
        FROM jp_labour_template_worktype t
        LEFT JOIN (
            SELECT id, labour_id, template_id, labour_rate
            FROM jp_sc_quot_labour_template 
            WHERE sc_quot_id = $scquotation_id
              AND template_id = $template_id
        ) sq
        ON sq.template_id = t.template_id 
           AND sq.labour_id = t.worktype_id
        WHERE t.template_id = $template_id";

$arrVal = Yii::app()->db->createCommand($sql)->queryAll();
}

if ($arrVal) {
?>
        <div class="row">
                <div class="col-md-2 element-style " style="text-align:center">
                    <label>Labour Type</label>
                </div>
                <div class="col-md-2 element-style" style="text-align:center">
                    <label>No Of Labours</label>
                </div>
                <div class="col-md-2 element-style" style="text-align:center">
                    <label>Amount</label>
                </div>
            </div>
        <?php
        foreach ($arrVal as $val) {
            $labour_data = Yii::app()->db->createCommand("SELECT * FROM `jp_labour_worktype` WHERE `type_id` = " . $val['labour_id'])->queryRow();
        ?>
            <input type="hidden" class="sc_quotation"/>
                        <input type="hidden" name="sc_quotation_id" id="" value="<?php echo $scquotation_id; ?>">
                      
                    </div>
                <div class="row">
                    <div class="col-md-2 element-style">
                       
                        <input type="hidden" class="labour_type" Name ="Dailyreport[lab_templates][<?php echo $val['id']; ?>][labour_id]" value="<?php echo $val['labour_id']; ?>">
                        <input type="text" class="labour_worktype form-control" name="Dailyreport[lab_templates][<?php echo $val["id"]; ?>][worktype]" value="<?php echo $labour_data['worktype'];?> " readonly/>
                        
                        <div class="error"></div>
                    </div>
                    <div class="col-md-2 element-style">
                      
                        <input type="text" min="0"  id="labourCount" class="labour_count form-control" name="Dailyreport[lab_templates][<?php echo $val['id']; ?>][count]" value=0  data-rate="<?php echo $val['labour_rate'];?>" onkeyup="validateInput(this)"/>
                        <span class="error_count"></span>
                    </div>
                    <div class="col-md-2 element-style">
					
						<input type="text" class="labour_total_amount form-control" name="Dailyreport[lab_templates][<?php echo $val['id']; ?>][labour_amount]" readonly/>
						<div class="error"></div>
					</div>
                    <input type="hidden" name="Dailyreport[lab_templates][<?php echo $val['id']; ?>][default_labour_type]" value='3'>
                </div>
        <?php
        }
        ?>
       
<?php
}
}else{ 
$arrVal = Yii::app()->db->createCommand("SELECT * FROM `jp_daily_report_labour_details` WHERE `daily_report_id` = " . $dr_id )->queryAll();

if ($arrVal) {
?>
        <div class="row">
                <div class="col-md-2 element-style " style="text-align:center">
                    <label>Labour Type</label>
                </div>
                <div class="col-md-2 element-style" style="text-align:center">
                    <label>No Of Labours</label>
                </div>
                <div class="col-md-2 element-style" style="text-align:center">
                    <label>Amount</label>
                </div>
            </div>
        <?php
        foreach ($arrVal as $val) {
            $labour_data = Yii::app()->db->createCommand("SELECT * FROM `jp_labour_worktype` WHERE `type_id` = " . $val['labour_id'])->queryRow();
            $dailyReport=dailyReport::Model()->findByPk($dr_id);
                if(!empty($dailyReport)){
                    $default_labour_type=$dailyReport->default_labour_type;
                }
            if(!empty($template_id)){
                $newsql="
                SELECT DISTINCT
                    t.worktype_id as labour_id, -- Use t.worktype_id as the fallback labour_id
                    t.worktype_name,
                    t.template_id,
                    COALESCE(sq.labour_rate, t.worktype_rate) AS labour_rate, -- Fallback to worktype_rate if labour_rate is NULL
                    COALESCE(sq.id, t.worktype_id) AS id -- Fallback to worktype_id if id is NULL
                FROM jp_labour_template_worktype t
                LEFT JOIN (
                    SELECT id, labour_id, template_id, labour_rate
                    FROM jp_sc_quot_labour_template
                    WHERE sc_quot_id = $scquotation_id
                    AND template_id = $template_id
                    AND labour_id = " . $val['labour_id'] . "
                ) sq
                ON sq.template_id = t.template_id 
                AND sq.labour_id = t.worktype_id
                WHERE t.template_id = $template_id
                AND t.worktype_id = " . $val['labour_id'];

                $sc_labour_data = Yii::app()->db->createCommand($newsql)->queryRow();

              }else{
                $dailyReport=dailyReport::Model()->findByPk($dr_id);
                if(!empty($dailyReport)){
                    $default_labour_type=$dailyReport->default_labour_type;
                }
                $project_id=$dailyReport->projectid;
                $labour_template=$dailyReport->labour_template;
                
                    if(!empty($labour_template)){
                        $newsqldb="SELECT DISTINCT
                            t.worktype_id as labour_id, -- Use t.worktype_id as the fallback labour_id
                            t.worktype_name,
                            t.template_id,
                            COALESCE(pl.labour_rate, t.worktype_rate) AS labour_rate, -- Fallback to worktype_rate if labour_rate is NULL
                            COALESCE(pl.id, t.worktype_id) AS id -- Fallback to worktype_id if id is NULL
                        FROM jp_labour_template_worktype t
                        LEFT JOIN (
                            SELECT id, labour_id, template_id, labour_rate
                            FROM jp_project_labour
                            WHERE project_id = " . $project_id . "
                            AND template_id = " . $labour_template . "
                            AND labour_id = " . $val['labour_id'] . "
                        ) pl
                        ON pl.template_id = t.template_id 
                        AND pl.labour_id = t.worktype_id
                        WHERE t.template_id = " . $labour_template . "
                        AND t.worktype_id = " . $val['labour_id'];
                    $sc_labour_data =Yii::app()->db->createCommand($newsqldb)->queryRow();
                    }
                
                
            }
        ?>
            <input type="hidden" class="sc_quotation"/>
                        <input type="hidden" name="sc_quotation_id" id="" value="<?php echo $scquotation_id; ?>">
                      
                    </div>
                <div class="row">
                    <div class="col-md-2 element-style">
                       
                        <input type="hidden" class="labour_type" Name ="Dailyreport[lab_templates][<?php echo $val['id']; ?>][labour_id]" value="<?php echo $val['labour_id']; ?>">
                        <input type="text" class="labour_worktype form-control" name="Dailyreport[lab_templates][<?php echo $val["id"]; ?>][worktype]" value="<?php echo $val['worktype'];?> " readonly/>
                        
                        <div class="error"></div>
                    </div>
                    <div class="col-md-2 element-style">
                      
                        <input type="text" min="0"  id="labourCount" class="labour_count form-control"  name="Dailyreport[lab_templates][<?php echo $val['id']; ?>][count]" value=<?php echo $val["labour_count"]?>  data-rate="<?php echo $sc_labour_data['labour_rate'];?>" <?php echo $pms_api_integration == 1 ? 'readonly' : ''; ?>  onkeyup="validateInput(this)"/>
                        <span class="error_count"></span>
                    </div>
                    <div class="col-md-2 element-style">
					
						<input type="text" class="labour_total_amount form-control" name="Dailyreport[lab_templates][<?php echo $val['id']; ?>][labour_amount]" value="<?php echo $val['labour_total_rate']?>" readonly/>
						<div class="error"></div>
					</div>
                    <input type="hidden" name="Dailyreport[lab_templates][<?php echo $val['id']; ?>][default_labour_type]" value="<?php echo $default_labour_type; ?> ">
             
                </div>
        <?php
        }
        ?>
       
<?php
}
}   

?>
