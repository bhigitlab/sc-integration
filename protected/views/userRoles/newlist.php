<?php
/* @var $this ExpensesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'user Roles',
);
?>
<div class="container" id="expensetype">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="clearfix">
        <div class="add-btn">
            <a class="button createUserRole">Add User Role</a>
        </div>
        <h2>User Roles</h2>
    </div>    
    <div id="errMsg"></div>

    <div>
        <div id="adduserRole"  style="display:none;"></div>
        <?php
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_newview',
            'template' => '<div>{summary}{sorter}</div><div class="container1"><table cellpadding="10" id="clientable" class="table">{items}</table></div>{pager}',

        ));
        ?>




        <!-- Edit Expense type Popup -->

        <div class="modal fade edit" role="dialog">
            <div class="modal-dialog modal-lg">


            </div>
        </div>

        <script>
            function editaction(elem, event) {
                $(elem).parent().hide();
                $(elem).parents().find('td:eq(2)').show();
                var rolename = $(elem).parent().find('.list_item').text();
                var id = $(elem).attr('data-id');
                $('.user_role').val(rolename);
                $(elem).parents().siblings().find('.test').hide();
                $(elem).parents().siblings().find('td:eq(1)').show();
            }

            function cancelaction(elem, event) {
                $(elem).parents().find('.text_black').show();
                $(elem).parents().find('.test').hide();

            }

            function saveaction(elem, event) {
                var value = $(elem).parent().find('.user_role').val();
                var id = $(elem).attr('data-id');
                $('.loading-overlay').addClass('is-active');
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "<?php echo $this->createUrl('userRoles/update2') ?>",
                    data: {
                        id: id,
                        value: value
                    },
                    success: function(response) {
                        if (response == null) {
                            $(elem).parents().find('td:eq(2)').hide();
                            $(elem).parents().find('td:eq(1)').show();
                            $(elem).parent().parent().find('.list_item').text(value);
                        } else {
                            var obj = eval(response);
                            $(".errorMessage1").text(obj);
                        }
                    },
                });
                return false;
            }

            $(document).ready(function() {
                $('.createUserRole').click(function() {
                    $('.loading-overlay').addClass('is-active');
                    $.ajax({
                        type: "GET",
                        url: "<?php echo $this->createUrl('userRoles/create') ?>",
                        success: function(response) {                            
                            $("#adduserRole").html(response).slideDown();;

                        }
                    });
                });

            });
            $(document).ajaxComplete(function() {
                $('.loading-overlay').removeClass('is-active');
                $('#loading').hide();
            });
            function closeaction() {
                $('#adduserRole').slideUp();
            }

            function deletUserRole(elem,id){
        
                if (confirm("Do you want to remove this item?")) {
                    $('.loading-overlay').addClass('is-active');
                    $.ajax({
                        url: '<?php echo  Yii::app()->createAbsoluteUrl('userRoles/removeuserole'); ?>',
                        type: 'POST',
                        dataType:'json',
                        data: {
                            roleId: id
                        },
                        success: function(data) {    
                            console.log(data)                ;
                            $("#errMsg").show()
                                .html('<div class="alert alert-'+data.response+'">'+data.msg+'</div>')
                                .fadeOut(10000);
                            setTimeout(function () {                        
                                location.reload(true);
                            }, 3000);
                                
                        }
                    });
                } else {
                    return false;
                }
                
            }
        </script>
        <style>
            .editable {
                float: right;
            }

            span.text_black {
                color: #333
            }

            .test {
                display: none;
            }

            .errorMessage1 {
                color: red;
            }
        </style>
    </div>
</div>