<table class="table table-bordered" id="fixtable">
    <?php if (!empty($result_data)) { ?>
        <thead class="entry-table">
            <tr>
                <th>Warehouse</th>
                <th>Image</th>
                <th>Item</th>
                <th>Dimension</th>
                <th>Batch</th>
                <th>Unit</th>
                <th>Quantity</th>
                <th>Rate</th>
                <th>Amount</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody id="fixtable_body">
            <?php
            $total_qty = 0;
            $total_amount = 0;
            $total_used_amount = 0;
            $i = 0;
            foreach ($result_data as $data) {
                $row = 1;
                $qty_sum = 0;
                $used_qty_sum = 0;
                $final_qty_sum = 0;
                $amount_sum = 0;
                $used_amount_sum = 0;
                if (!empty($data)) {
                    $count = count($data);
                    if ($count == 1) {
                        $rowspan = 1;
                    } else {
                        $rowspan = $count;
                    }
                    foreach ($data as $dat) {
                        $used_quanity="";                         
                        $title="";
                        $used_qty_in_pms = Yii::app()->db->createCommand('SELECT * FROM `pms_acc_wpr_item_consumed` WHERE `warehouse_id`='.$dat['warehouseId'].' AND `item_id`='.$dat['item_id'].'  AND`item_rate` ='.$dat['rate'])->queryRow();
                        if(!empty($used_qty_in_pms)){
                            $used_quanity=$used_qty_in_pms['item_qty'];
                            
                        }
                        

                        $priority = [];
                        $base_unit = Controller::GetStockItemunit($dat['item_id']);
                        $basewarehouse_unit = Controller::ItemunitID($dat['unit']);
                        if ($base_unit != null) {
                            $basewarehouse_unit = $base_unit;
                        } 

                        $priority = Controller::Stockcheckpriority($dat['item_id'], $basewarehouse_unit);
                        
                        $total_qty += $dat['qty'];
                        $total_amount += $dat['amount'];
                        $qty_sum += $dat['qty'];
                        
                        if($used_quanity !=""){
                            $used_qty_sum += $used_quanity;
                        }
                        $amount_sum += $dat['amount'];
                        $tblpx = Yii::app()->db->tablePrefix;
                        $second_unit = '';
                        $second_quantity = '';
                        $coversion_factor = 0;


                        if (count($priority) == 0) {
                            $unit = $dat['unit'];
                            $quantity = round($dat['qty'], 2);
                            $final_qty_sum = round($qty_sum, 2);
                            $final_qty_sum = round($qty_sum-$used_qty_sum, 2);
                        } else {
                            if ($base_unit != $priority['conversion_unit']) {
                                $second_unit = $priority['conversion_unit'];
                                $coversion_factor = $priority['conversion_factor'];
                                $second_quantity = $dat['qty'] / $coversion_factor;
                                $second_qty_sum = ($qty_sum-$used_qty_sum) / $coversion_factor;
                                $unit = $dat['unit'] . "(" . $second_unit . ")";
                                $quantity = round($dat['qty'], 2) . "(" . round($second_quantity, 2) . ")";
                                $final_qty_sum = round(($qty_sum-$used_qty_sum), 2) . "(" . round($second_qty_sum, 2) . ")";
                            } else {
                                $unit = $dat['unit'];
                                $quantity = round($dat['qty'], 2);
                                $final_qty_sum = round(($qty_sum-$used_qty_sum), 2);
                            }
                        }

                        if(!empty($used_qty_in_pms)){
                            $used_quanity=$used_qty_in_pms['item_qty'];
                            $title="Stock Quantity: ".$quantity ."Used Quantity: ".$used_quanity;
                        }
            ?>
                        <tr>
                            <td><?= $dat['warehouse'] ?></td>
                            <td class="imd_div2" id="<?php echo $dat['item_id']; ?>">
                                <?php
                                if ($dat['item_img'] != '') {
                                    $path = realpath(Yii::app()->basePath . '/../uploads/purchase_category/' . $dat['item_img']);
                                    if (file_exists($path)) {
                                ?>
                                        <img class="img1 img_border pop" id="img1_<?php echo $i ?>_<?php echo $dat['item_id']; ?>" src="<?php echo Yii::app()->request->baseUrl . "/uploads/purchase_category/thumbnail/" . $dat['item_img'] ?>" alt='' style='' data-toggle="popover-hover" data-img='<?php echo Yii::app()->request->baseUrl . "/uploads/purchase_category/" . $dat['item_img']; ?>' />
                                    <?php   } else { ?>
                                        <img class="img1" id="img1_<?php echo $i; ?>_<?php echo $dat['item_id']; ?>" src="<?php echo Yii::app()->request->baseUrl . "/uploads/purchase_category/thumbnail/no_img.png" ?>" alt='' style='cursor:pointer;height:60px;width:60px;' />
                                    <?php } ?>
                                <?php } else { ?>
                                    <img class="img1" id="img1_<?php echo $i; ?>_<?php echo $dat['item_id']; ?>" src="<?php echo Yii::app()->request->baseUrl . "/uploads/purchase_category/thumbnail/no_img.png" ?>" alt='' style='cursor:pointer;height:60px;width:60px;' />
                                <?php } ?>
                            </td>
                            <td>
                                <?php

                                echo CHtml::link($dat['item'], Yii::app()->createUrl('report/reports/stockValueReportdetail&item_id=' . $dat['item_id']), array('class' => '', 'target' => '_blank'));
                                ?>
                            </td>

                            <td><?= ($dat['dimension'] != '') ? "(" . $dat['dimension'] . ")" : "N/A" ?></td>
                            <td><?= $dat['batch'] ?></td>
                            <td><?= $unit ?></td>
                            <td><?= ($used_quanity !="") ? ($quantity - $used_quanity): $quantity; ?> </td>
                            <td>
                                <?php

                                echo CHtml::link(Controller::money_format_inr($dat['rate'], 2, 1), Yii::app()->createUrl('report/reports/stockValueReportdetail&item_id=' . $dat['item_id'] . '&rate=' . $dat['rate']), array('class' => '', 'target' => '_blank'));
                                ?>
                            </td>
                            <td><?php 

                            $used_amount = ($used_quanity==0)?0:($dat['rate'] * $used_quanity);
                            $used_amount_sum += $used_amount;
                            $qty_used_final = $dat['amount']-$used_amount;
                            $total_used_amount += $used_amount;
                            echo Controller::money_format_inr($qty_used_final, 2, 1) ?></td>
                            <td><?= !empty($dat['date']) ? date('d-M-Y', strtotime($dat['date'])) : '' ?></td>
                        </tr>
                    <?php
                        $row++;
                    }
                    ?>
                    <tr>
                        <td colspan="3"> <i class="text-danger">Rate adjusted to 2 decimal points. Amount might vary from actual amount</i></td>
                        <td align="right"><b>Total</b></td>
                        <td></td>
                        <td><b><?= $dat['unit'] ?></b></td>
                        <td><b><?= round(($qty_sum-$used_qty_sum), 2) ?></b></td>
                        <td></td>
                        <td><b><?= Controller::money_format_inr(($amount_sum - $used_amount_sum) , 2, 1) ?></b>

                        </td>
                        <td></td>
                    </tr>
            <?php
                }
            }
            ?>
            <tr class='notfound' id="notfound">
                <td colspan='10'>No record found</td>
            </tr>
        </tbody>
        <tfoot id="grand_total_value" class="entry-table">
            <th colspan="4" align="right">Total</th>
            <th colspan="4"></th>
            <th><?= Controller::money_format_inr(($total_amount - $total_used_amount), 2, 1) ?></b></td>
            <th></th>
        </tfoot>
    <?php } else {
    ?>
        <span class="empty">No results found</span>
    <?php } ?>
</table>
<script>
    $(document).ready(function() {
        $("#fixtable").tableHeadFixer({
            'left': false,
            'foot': true,
            'head': true
        });
        $('[data-toggle="popover-hover"]').popover({
            html: true,
            container: "body",
            trigger: 'hover',
            placement: 'bottom',
            content: function() {

                return '<img src="' + $(this).data('img') + '" style="height:auto;"/>';
            }
        });
    });
</script>