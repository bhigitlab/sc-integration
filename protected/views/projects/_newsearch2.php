<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<div class="page_filter clearfix custom-form-style">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    )); ?>
    <div class="row">
        <div class="form-group col-xs-12 col-sm-3">
            <label for="sub">Subcontractor</label>
            <select id="subcontractor_id" class="select_box" name="subcontractor_id">
                <option value="">Select subcontractor</option>
                <?php
                if (!empty($subcontractor)) {
                    foreach ($subcontractor as $key => $value) {
                        ?>
                        <option value="<?php echo $value['subcontractor_id'] ?>" <?php echo ($value['subcontractor_id'] == $subcontractor_id) ? 'selected' : ''; ?>>
                            <?php echo $value['subcontractor_name']; ?>
                        </option>

                    <?php }
                } ?>
            </select>
        </div>
        <div class="form-group col-xs-12 col-sm-3">
            <label for="expense">Expense Head</label>
            <select id="expensehead_id" class="select_box" name="expensehead_id"
                class="sel">
                <option value="">Select Expense Head</option>
                <?php
                if (!empty($expense) && $expensehead_id != '') {
                    foreach ($expense as $value) {
                        ?>
                        <option value="<?php echo $value['type_id']; ?>" <?php echo ($value['type_id'] == $expensehead_id) ? 'selected' : ''; ?>>
                            <?php echo $value['type_name']; ?>
                        </option>

                        <?php
                    }
                }
                ?>
            </select>
        </div>

        <?php
        if (!isset($_GET['datefrom']) || $_GET['datefrom'] == '') {
            $datefrom = "";
        } else {
            $datefrom = $_GET['datefrom'];
        }
        ?>
        <div class="form-group col-xs-12 col-sm-3">
            <label for="from"> From </label>
            <?php echo CHtml::textField('date_from', $datefrom, array('value' => $datefrom, 'placeholder' => 'DD-MM-YYYY', 'class' => 'form-control', )); ?>
        </div>
        <?php
        if (!isset($_GET['dateto']) || $_GET['dateto'] == '') {
            $date_to = "";
        } else {
            $date_to = $_GET['dateto'];
        }
        ?>
        <div class="form-group col-xs-12 col-sm-3">
            <label for="to"> To </label>
            <?php echo CHtml::textField('date_to', $date_to, array('value' => $date_to, 'placeholder' => 'DD-MM-YYYY', 'class' => 'form-control')); ?>
        </div>
        <div class="form-group col-xs-12 text-right">
            <input name="yt0" value="Go" type="submit" class="btn btn-sm btn-primary">
            <input id="reset" name="yt1" value="Clear" type="reset" class="btn btn-sm btn-default">
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script>
    $('#reset').click(function () {
        location.href = '<?php echo $this->createUrl('projectreport'); ?>';
    })
    $(function () { });

    $(document).ready(function () {
        $(".select_box").select2();
        $(function () {
            $("#date_from").datepicker({
                dateFormat: 'dd-mm-yy'
            });

            $("#date_to").datepicker({
                dateFormat: 'dd-mm-yy'
            });

        });
        $("#date_from").change(function () {
            $("#date_to").datepicker('option', 'minDate', $(this).val());
        });
        $("#date_to").change(function () {
            $("#date_from").datepicker('option', 'maxDate', $(this).val());
        });

    });

    $("#subcontractor_id").change(function () {
        var id = $(this).val();
        if (id != '') {
            id = id ? id : 0;
            getExpenseHead(id, 0);
        }
    });

    function getExpenseHead(id, exp) {
        $('#loading').show();
        $.ajax({
            url: "<?php echo $this->createUrl('dailyReport/getexpensehead'); ?>",
            data: {
                "id": id
            },
            type: "GET",
            success: function (response) {
                if (id == 0) {
                    $("#expensehead_id").val('-Select Expense Head-').trigger('change.select2');
                } else {
                    $("#expensehead_id").html(response);
                }
                if (exp != 0) {
                    $("#expensehead_id").val(exp).trigger('change.select2');
                }
            }
        });
    }
</script>
<style>
    .search-form {
        background-color: #fafafa;
        padding: 10px;
        box-shadow: 0px 0px 2px 0px rgba(0, 0, 0, 0.1);
        margin-bottom: 10px;
    }

    .search-form input,
    .search-form select {
        margin-bottom: 10px;
    }
</style>