<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tableHeadFixer.js"></script>

<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<!-- <style>
    .project-report-block {
        overflow-y: scroll;
        max-height: 500px;
    }

    .sticky-1 {
        position: sticky;
        top: 0px;
    }

    .sticky-2 {
        position: sticky;
        top: 34.17px;
        background-color: #eee !important;
    }
</style> -->
<?php
if (filter_input(INPUT_GET, 'export')) { ?>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdfstyle.css">
<?php }
Yii::app()->clientScript->registerScript('search', "
    $('#btSubmit').click(function(){
            $('#projectform').submit();
    });
");
$tblpx = Yii::app()->db->tablePrefix;
$newQuery = "";
$newQuery1 = "";
$newQuery2 = "";
$newQuery3 = "";
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
foreach ($arrVal as $arr) {
    if ($newQuery)
        $newQuery .= ' OR';
    $newQuery .= " FIND_IN_SET('" . $arr . "', id)";
}
if ($company_id != '') {
    $newQuery2 .= " FIND_IN_SET('" . $company_id . "', company_id)";
} else {
    foreach ($arrVal as $arr) {
        if ($newQuery2)
            $newQuery2 .= ' OR';
        $newQuery2 .= " FIND_IN_SET('" . $arr . "', company_id)";
    }
}
?>
<div class="container" id="project-report">
    <?php
    if (!filter_input(INPUT_GET, 'export')) { ?>
        <div class="header-container">
            <h3>Project Report</h3>
            <div class="btn-container">
                <a href="<?php echo Yii::app()->createUrl("projects/expenselist&aging=1") ?>"
                    class="save_btn btn btn-primary">Aging Summary</a>&nbsp;
                <a href="<?php echo Yii::app()->createUrl("projects/expenselist&aging=2") ?>"
                    class="save_btn btn btn-primary">Aging Summary By Due Date</a>
            </div>
        </div>
        <?php
    }
    $fromdate = (!empty($date_from) ? date('d-M-Y', strtotime($date_from)) : '');
    $todate = (!empty($date_to) ? date('d-M-Y', strtotime($date_to)) : '');
    $companyArray = explode(",", Yii::app()->user->company_ids);
    $condition = "";
    foreach ($companyArray as $company) {
        $condition .= "FIND_IN_SET('" . $company . "',company_id) OR ";
    }
    $condition = substr($condition, 0, -3);
    $checked = false;
    $hide = '';
    $br = '<br>';
    if (isset($_GET['summary']) && $_GET['summary'] != "") {
        $checked = ($_GET['summary'] == 0) ? false : true;
        $hide = ($_GET['summary'] == 1) ? 'hide' : '';
        $br = ($_GET['summary'] == 1) ? '' : '<br>';
    }
    ?>
    <?php
    if (!filter_input(INPUT_GET, 'export')) { ?>
        <div class="page_filter clearfix">

            <?php $form = $this->beginWidget('CActiveForm', array(
                'id' => 'projectform',
                'action' => Yii::app()->createUrl($this->route),
                'method' => 'get',
            )); ?>
            <div class="row">
                <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
                    <label for="project">Project</label>
                    <?php
                    echo CHtml::dropDownList('project_id', 'project_id', CHtml::listData(Projects::model()->cache(0)->findAll(array(
                        'select' => array('pid, name'),
                        'order' => 'name',
                        'condition' => '' . $condition . '',
                        'distinct' => true
                    )), 'pid', 'name'), array('empty' => 'Select Project', 'class' => 'form-control js-example-basic-single', 'id' => 'project_id', 'options' => array($project_id => array('selected' => true))));
                    ?>
                </div>
                <?php if ($project_id == 0) { ?>
                    <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
                        <label for="status">Status</label>
                        <?php
                        echo CHtml::dropDownList('project_status', 'project_status', CHtml::listData(Status::model()->findAll(array(
                            'select' => array('sid, caption'),
                            'order' => 'sid',
                            'condition' => 'status_type = "project_status"',
                            'distinct' => true
                        )), 'sid', 'caption'), array('empty' => 'Select Status', 'class' => 'form-control js-example-basic-single', 'id' => 'project_status', 'options' => array($project_status => array('selected' => true))));
                        ?>
                    </div>
                <?php } ?>
                <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
                    <label for="company">Company</label>
                    <?php
                    echo CHtml::dropDownList('company_id', 'company_id', CHtml::listData(Company::model()->findAll(array(
                        'select' => array('id, name'),
                        'order' => 'id DESC',
                        'condition' => '(' . $newQuery . ')',
                        'distinct' => true

                    )), 'id', 'name'), array('empty' => 'Select Company', 'id' => 'company_id', 'class' => 'form-control js-example-basic-single', 'options' => array($company_id => array('selected' => true))));
                    ?>
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
                    <!-- <div class="display-flex"> -->
                    <label>From:</label>
                    <?php echo CHtml::textField('date_from', (!empty($date_from) ? date('d-M-Y', strtotime($date_from)) : ''), array("id" => "date_from", 'class' => 'form-control ', 'placeholder' => 'Date From', 'autocomplete' => 'off')); ?>
                    <!-- </div> -->
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-2 ">
                    <!-- <div class="display-flex"> -->
                    <label>To:</label>
                    <?php echo CHtml::textField('date_to', (!empty($date_to) ? date('d-M-Y', strtotime($date_to)) : ''), array("id" => "date_to", 'class' => 'form-control', 'placeholder' => 'Date To', 'autocomplete' => 'off')); ?>
                    <!-- </div> -->
                </div>
                <input type="hidden" name="reconcil_type"
                    value="<?php echo isset($_GET['reconcil_type']) ? $_GET['reconcil_type'] : "reconcil" ?>"
                    class="reconcil_type">
                <input type="hidden" name="summary"
                    value="<?php echo isset($_GET['summary']) ? ($_GET['summary'] == 0) ? 1 : 0 : 0 ?>"
                    class="summary_checkbox">
                <?php if (isset($_GET['project_id']) && $_GET['project_id'] != "") { ?>
                    <div class="form-group col-xs-12 col-sm-3 col-md-2 ">

                        <?php
                        echo CHtml::checkBox('summary', $checked, array());
                        echo "&nbsp;";
                        echo CHtml::label(' Show Summary', ''); ?>
                    </div>

                <?php } ?>
                <div class="form-group col-xs-12 col-sm-3 col-md-2 text-sm-left text-right">
                    <label class="d-sm-block d-none">&nbsp;</label>
                    <div>
                        <?php
                        echo CHtml::submitButton('GO', array('id' => 'btSubmit', 'class' => 'btn btn-sm btn-primary'));

                        ?>
                        <?php echo CHtml::resetButton('Clear', array('onclick' => 'javascript:location.href="' . $this->createUrl('expenselist') . '"', 'class' => 'btn btn-sm btn-default')); ?>
                    </div>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>

        <?php

        $url_data = '';
        if (isset($_GET['project_id'])) {
            $url_data .= '&project_id=' . $_GET['project_id'];
        }
        if (isset($_GET['company_id'])) {
            $url_data .= '&company_id=' . $_GET['company_id'];
        }
        if (isset($_GET['date_from'])) {
            $url_data .= '&date_from=' . $_GET['date_from'];
        }
        if (isset($_GET['date_to'])) {
            $url_data .= '&date_to=' . $_GET['date_to'];
        }
        if (isset($_GET['reconcil_type'])) {
            $url_data .= '&reconcil_type=' . $_GET['reconcil_type'];
        }
        ?>
    <?php } ?>


    <?php if ((!empty($sql) || !empty($creditsql) || !empty($client) || !empty($client1) || !empty($daybook_expense) || !empty($daybook_expense1) || !empty($purchase) || !empty($purchase1) || !empty($expreceipt_return)) && ($project_id != 0)) { ?>

        <div id="contenthtml" class="contentdiv">
            <?php
            if (!filter_input(INPUT_GET, 'export')) { ?>
                <div id="" class="pull-right">
                    <?php
                    if ($_GET['reconcil_type'] == 'reconcil') {
                        ?>
                        <a style="cursor:pointer;" class="unreconciled btn-sm save_btn  btn btn-primary " 
                            href="<?php echo $this->createAbsoluteUrl('projects/expenselist', array("reconcil_type" => 'unreconcil', "project_id" => $project_id, "company_id" => $company_id, "date_from" => $date_from, "date_to" => $date_to, "summary" => isset($_GET['summary']) ? $_GET['summary'] : 0)) ?>">UnReconciled
                            Report</a>
                    <?php } else { ?>

                        <a style="cursor:pointer;" class="reconciled  btn-sm  save_btn  btn btn-primary "
                            href="<?php echo $this->createAbsoluteUrl('projects/expenselist', array("reconcil_type" => 'reconcil', "project_id" => $project_id, "company_id" => $company_id, "date_from" => $date_from, "date_to" => $date_to, "summary" => isset($_GET['summary']) ? $_GET['summary'] : 0)) ?>">Reconciled
                            Report</a>
                    <?php } ?>
                    <?php
                    echo CHtml::link('<i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>', $this->createAbsoluteUrl('projects/expenselist', array("reconcil_type" => $_GET['reconcil_type'], "project_id" => $project_id, "company_id" => $company_id, "date_from" => $date_from, "date_to" => $date_to, "summary" => isset($_GET['summary']) ? $_GET['summary'] : 0, "export" => "csv")), array('class' => ' btn-sm   save_btn btn btn-primary','title'=>'SAVE AS EXCEL')); ?>
                    <?php
                    echo CHtml::link('<i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>', $this->createAbsoluteUrl('projects/expenselist', array("reconcil_type" => $_GET['reconcil_type'], "project_id" => $project_id, "company_id" => $company_id, "date_from" => $date_from, "date_to" => $date_to, "summary" => isset($_GET['summary']) ? $_GET['summary'] : 0, "export" => "pdf")), array('class' => 'save_btn btn btn-primary  btn-sm ','title'=>'SAVE AS PDF')); ?>
                </div>
            <?php } ?>
            <?php
            $spacing = "";
            if (filter_input(INPUT_GET, 'export')) {
                $company_address = Company::model()->findBypk(($company_id) ? $company_id : Yii::app()->user->company_id);
                $spacing = "style='margin:0px 30px'";
                ?>

            <?php } ?>
            <h2 class="text-center">Project Summary of
                <?php echo $name; ?>
            </h2>
            <div <?php echo $spacing ?>>
                <?php
                $render_datas = array(
                    'sql' => $sql,
                    'daybook_purchase' => $sql,
                    'client' => $client,
                    'client1' => $client1,
                    'project_id' => $project_id,
                    'site' => $site,
                    'name' => $name,
                    'clientname' => $clientname,
                    'wc' => $wc,
                    'sqft' => $sqft,
                    'wc_type' => $wc_type,
                    'sqft_rate' => $sqft_rate,
                    'percentage' => $percentage,
                    'date_from' => $date_from,
                    'date_to' => $date_to,
                    'purchase' => $purchase,
                    'purchase1' => $purchase1,
                    'daybook_expense' => $daybook_expense,
                    'expreceipt_return' => $expreceipt_return,
                    'company_id' => $company_id,
                    'model' => $model,
                    'project_status' => $project_status,
                    'invoice_dtls' => $invoice_dtls,
                    'uinvoice_dtls' => $uinvoice_dtls,
                    'unreconciled_data' => $unreconciled_data,
                    'daybook_expense1' => $daybook_expense1,
                    'item_report_data' => $item_report_data,
                    'dispatch_details' => $dispatch_details,
                    'consumptionreq_details' => $consumptionreq_details,
                    'unconsumptionreq_details' => $unconsumptionreq_details,
                    'creditsql' => $creditsql,
                    'clientgstresult' => $clientgstresult,
                    'company' => $company
                );

                $this->renderPartial('expenselist_summary', $render_datas);
                ?>
                <?php echo $br ?>
                <?php
                if ($_GET['reconcil_type'] == 'reconcil') {
                    if (isset($_GET['summary']) && $_GET['summary'] == 0) {
                        ?>
                        <div class="project-report-block">
                            <table class="table reconcil_table <?php echo $hide ?>">
                                <!-- sales quotation approved -->
                                <tr class="sticky-1" bgcolor="#eee">
                                    <th colspan="3"> Sales Quotation(Approved) </th>
                                </tr>
                                <tr class="sticky-2" bgcolor="#fafafa">
                                    <th>Date</th>
                                    <th>Quotation No</th>
                                    <th>Amount</th>
                                </tr>
                                <?php
                                $sales_quotations = Quotation::model()->getProjectQuotationsWise($project_id, $date_from, $date_to);

                                $total_qtn = 0;
                                foreach ($sales_quotations as $sales_quotation) {
                                    ?>
                                    <tr>
                                        <!-- quotation_id -->
                                        <td>
                                            <?php echo date('d-M-Y', strtotime($sales_quotation['attributes']['date'])) ?>
                                        </td>
                                        <td>
                                            <?php echo $sales_quotation['attributes']['inv_no'] ?>
                                        </td>
                                        <td class="text-right project-report-width">
                                            <?php echo Yii::app()->controller->money_format_inr($sales_quotation['total_amount'], 2); ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $total_qtn += $sales_quotation['total_amount'];
                                }
                                ?>
                                <tr bgcolor="#eee">
                                    <th colspan="2" class="text-right">Total</th>
                                    <th class="text-right project-report-width">
                                        <?php echo Yii::app()->controller->money_format_inr($total_qtn, 2); ?>
                                    </th>
                                </tr>
                            </table>
                        </div>
                        <?php echo $br ?>
                        <div class="project-report-block">
                            <table class="table reconcil_table  <?php echo $hide ?>">

                                <tr class="sticky-1" bgcolor="#eee">
                                    <td colspan="3"><b>Salesbook entry / Invoice</b></td>
                                </tr>
                                <!-- html added -->
                                <tr class="sticky-2">
                                    <th>Date</th>
                                    <th>Invoice No</th>
                                    <th>Amount</th>
                                </tr>
                                <?php
                                $invoice = 0;

                                foreach ($invoice_dtls as $invoice_data) {
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo date('d-M-Y', strtotime($invoice_data['date'])); ?>
                                        </td>
                                        <td>
                                            <?php echo $invoice_data['inv_no']; ?>
                                        </td>
                                        <!--html added-->
                                        <td align="right" border="1">
                                            <?php echo Controller::money_format_inr($invoice_data['amount'] + $invoice_data['tax_amount'], 2, 1); ?>
                                        </td>
                                        <?php $invoice += $invoice_data['amount'] + $invoice_data['tax_amount']; ?>
                                    </tr>
                                <?php }
                                echo '<tr bgcolor="#eee"><td align="right" colspan="2"><b>Total</b></td><td align="right" class="text-right project-report-width"><b>' . Controller::money_format_inr($invoice, 2, 1) . '</b></td></tr>';
                                ?>
                            </table>
                        </div>
                        <?php echo $br ?>
                        <div class="project-report-block">
                            <table class="table reconcil_table <?php echo $hide ?>">
                                <tr class="sticky-1" bgcolor="#eee">
                                    <td colspan="4"><b>Daybook Receipt</b></td>
                                </tr>
                                <tr class="sticky-2">
                                    <th width="70px">#ID</th>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>Amount</th>
                                </tr>
                                <?php
                                $clienttotal = 0;

                                foreach ($client as $newclient) {
                                    ?>
                                    <tr>
                                        <td><b>
                                                <?php echo $newclient['exp_id'] ?>
                                            </b></td>
                                        <td>
                                            <?php echo date('d-M-Y', strtotime($newclient['date'])); ?>
                                        </td>
                                        <td>
                                            <?php print_r($newclient['description']); ?>
                                        </td>
                                        <td align="right" class="text-right project-report-width" border="1">
                                            <?php echo Controller::money_format_inr($newclient['receipt'], 2, 1); ?>
                                        </td>
                                        <?php $clienttotal += $newclient['receipt']; ?>
                                    </tr>

                                <?php }
                                echo '<tr bgcolor="#eee"><td align="right" colspan="3"><b>Total</b></td><td align="right"  class="text-right project-report-width"><b>' . Controller::money_format_inr($clienttotal, 2, 1) . '</b></td></tr>';
                                ?>


                            </table>
                        </div>
                        <?php echo $br ?>
                        <div class="project-report-block">
                            <table class="table reconcil_table <?php echo $hide ?>">
                                <tr class="sticky-1">
                                    <td colspan="8" bgcolor="#eee"><b>Purchase Bill</b></td>
                                </tr>
                                <tr class="sticky-2">
                                    <td>#ID</td>
                                    <td><b>Expense Type</b></td>
                                    <td><b>Date</b></td>
                                    <td><b>Bill No</b></td>
                                    <td><b>Vendor</b></td>
                                    <td><b>Purchase No</b></td>
                                    <td><b>Bill Amount</b></td>
                                    <td><b>Total</b></td>
                                </tr>

                                <?php
                                $bill_amount = 0;
                                $bill_total_new_amount = 0;
                                $purchase_list = Projects::model()->groupPurchaseBillItems($purchase);
                                $expense_types_listing = Projects::model()->setExpenseTypesListingofPurchaseBill($purchase_list);

                                foreach ($purchase_list as $purchase) {
                                    $count = count($purchase);
                                    $count = ($count - 2);
                                    $index = 1;
                                    foreach ($purchase as $key => $value) {
                                        if (is_array($value)) {
                                            $amount = ($value['bill_totalamount'] + $value['bill_additionalcharge']);
                                            $vendor = Vendors::model()->findByPk($value['vendor_id']);

                                            ?>
                                            <tr>
                                                <td><b>#
                                                        <?php echo $value['bill_id'] ?>
                                                    </b></td>
                                                <?php
                                                if ($index == 1) {
                                                    ?>
                                                    <td rowspan="<?php echo $count; ?>">
                                                        <?php echo $purchase['expense_head'] ?>
                                                    </td>
                                                    <?php
                                                }
                                                ?>

                                                <!--html added-->
                                                <td>
                                                    <?php echo date('d-M-Y', strtotime($value['bill_date'])); ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if (filter_input(INPUT_GET, 'export')) {
                                                        echo $value['bill_number'];
                                                    } else {
                                                        if ($value['purchase_no'] != '') {
                                                            ?>
                                                            <a target="_blank" title='' onmouseover=getBillItems(this)
                                                                href="<?php echo Yii::app()->createAbsoluteUrl('bills/view', array('id' => $value['bill_id'])); ?>"
                                                                data-bill-id="<?php echo $value['bill_id'] ?>">
                                                                <?php echo $value['bill_number']; ?>
                                                            </a>
                                                        <?php } else { ?>
                                                            <a target="_blank" title='' onmouseover=getBillItems(this)
                                                                href="<?php echo Yii::app()->createAbsoluteUrl('bills/billview', array('id' => $value['bill_id'])); ?>"
                                                                data-bill-id="<?php echo $value['bill_id'] ?>">
                                                                <?php echo $value['bill_number']; ?>
                                                            </a>
                                                        <?php }
                                                    } ?>
                                                </td>
                                                <td>
                                                    <?php echo $vendor->name; ?>
                                                </td>
                                                <td>
                                                    <?php echo $value['purchase_no']; ?>
                                                </td>
                                                <td align="right" border="1">
                                                    <?php echo Controller::money_format_inr($amount, 2, 1); ?>
                                                </td>
                                                <?php
                                                if ($index == 1) {
                                                    ?>
                                                    <td class="text-right" rowspan="<?php echo $count; ?>">
                                                        <?php echo Controller::money_format_inr($purchase['total_amount'], 2) ?>
                                                    </td>
                                                <?php } ?>
                                                <!--html  added-->
                                                <?php $bill_amount += $amount;
                                                $bill_total_new_amount += $amount;
                                                ?>
                                            </tr>

                                            <?php
                                        }
                                        $index++;
                                    }
                                }
                                // check amount html code
                                echo '<tr bgcolor="#eee"><td align="right" colspan="7"><b>Total</b></td><td align="right" class="text-right project-report-width"><b>' . Controller::money_format_inr($bill_amount, 2, 1) . '</b></td></tr>';
                                ?>

                            </table>
                        </div>
                        <br>
                        <?php echo $br ?>
                        <div class="project-report-block">
                            <table class="table reconcil_table <?php echo $hide ?>">
                                <tr class="sticky-1">
                                    <td colspan="8" bgcolor="#eee"><b>Purchase Bill(Pending)</b></td>
                                </tr>
                                <tr class="sticky-2">
                                    <td>#ID</td>
                                    <td><b>Expense Type</b></td>
                                    <td><b>Date</b></td>
                                    <td><b>Bill No</b></td>
                                    <td><b>Vendor</b></td>
                                    <td><b>Purchase No</b></td>
                                    <td><b>Bill Amount</b></td>
                                    <td><b>Total</b></td>
                                </tr>

                                <?php
                                $bill_amount = 0;
                                $purchase_lists = Projects::model()->groupPurchaseBillItems($purchase1);
                                $expense_types_listing = Projects::model()->setExpenseTypesListingofPurchaseBill($purchase_lists);

                                foreach ($purchase_lists as $purchase) {
                                    $count = count($purchase);
                                    $count = ($count - 2);
                                    $index = 1;
                                    foreach ($purchase as $key => $value) {
                                        if (is_array($value)) {
                                            $amount = $value['bill_totalamount'] + $value['bill_additionalcharge'];
                                            $vendor = Vendors::model()->findByPk($value['vendor_id']);

                                            ?>
                                            <tr>
                                                <td><b>#
                                                        <?php echo $value['bill_id'] ?>
                                                    </b></td>
                                                <?php
                                                if ($index == 1) {
                                                    ?>
                                                    <td rowspan="<?php echo $count; ?>">
                                                        <?php echo $purchase['expense_head'] ?>
                                                    </td>
                                                    <?php
                                                }
                                                ?>

                                                <!--html added-->
                                                <td>
                                                    <?php echo date('d-M-Y', strtotime($value['bill_date'])); ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if (filter_input(INPUT_GET, 'export')) {
                                                        echo $value['bill_number'];
                                                    } else {
                                                        if ($value['purchase_no'] != '') {
                                                            ?>
                                                            <a target="_blank" title='' onmouseover=getBillItems(this)
                                                                href="<?php echo Yii::app()->createAbsoluteUrl('bills/view', array('id' => $value['bill_id'])); ?>"
                                                                data-bill-id="<?php echo $value['bill_id'] ?>">
                                                                <?php echo $value['bill_number']; ?>
                                                            </a>
                                                        <?php } else { ?>
                                                            <a target="_blank" title='' onmouseover=getBillItems(this)
                                                                href="<?php echo Yii::app()->createAbsoluteUrl('bills/billview', array('id' => $value['bill_id'])); ?>"
                                                                data-bill-id="<?php echo $value['bill_id'] ?>">
                                                                <?php echo $value['bill_number']; ?>
                                                            </a>
                                                        <?php }
                                                    } ?>
                                                </td>
                                                <td>
                                                    <?php echo $vendor->name; ?>
                                                </td>
                                                <td>
                                                    <?php echo $value['purchase_no']; ?>
                                                </td>
                                                <td align="right" border="1">
                                                    <?php echo Controller::money_format_inr($amount, 2, 1); ?>
                                                </td>
                                                <?php
                                                if ($index == 1) {
                                                    ?>
                                                    <td class="text-right" rowspan="<?php echo $count; ?>">
                                                        <?php echo Controller::money_format_inr($purchase['total_amount'], 2) ?>
                                                    </td>
                                                <?php } ?>
                                                <!--html  added-->
                                                <?php $bill_amount += $amount; ?>
                                            </tr>

                                            <?php
                                        }
                                        $index++;
                                    }
                                }
                                // check amount html code
                                echo '<tr bgcolor="#eee"><td align="right" colspan="7"><b>Total</b></td><td align="right" class="text-right project-report-width"><b>' . Controller::money_format_inr($bill_amount, 2, 1) . '</b></td></tr>';
                                ?>

                            </table>
                        </div>
                        <br>
                        <?php echo $br ?>


                        <br>
                        <?php
                    }
                } else {
                    if (isset($_GET['summary']) && $_GET['summary'] == 0) {

                        ?>
                        <table class="table unreconcil_table <?php echo $hide ?>">

                            <tr bgcolor="#eee">
                                <td colspan="3"><b>Salesbook entry / Invoice - Unapproved</b></td>
                            </tr>
                            <!-- html added -->
                            <tr>
                                <th>Date</th>
                                <th>Invoice No</th>
                                <th>Amount</th>
                            </tr>
                            <?php
                            $invoice = 0;

                            foreach ($uinvoice_dtls as $invoice_data) {
                                ?>
                                <tr>
                                    <td>
                                        <?php echo date('d-M-Y', strtotime($invoice_data['date'])); ?>
                                    </td>
                                    <td>
                                        <?php echo $invoice_data['inv_no']; ?>
                                    </td>

                                    <td align="right" border="1">
                                        <?php echo Controller::money_format_inr($invoice_data['amount'] + $invoice_data['tax_amount'], 2, 1); ?>
                                    </td>
                                    <?php $invoice += $invoice_data['amount'] + $invoice_data['tax_amount']; ?>
                                </tr>
                            <?php }
                            echo '<tr bgcolor="#eee"><td align="right" colspan="2"><b>Total</b></td><td align="right" class="text-right project-report-width"><b>' . Controller::money_format_inr($invoice, 2, 1) . '</b></td></tr>';
                            ?>
                        </table>
                        <?php echo $br ?>
                        <table class="table unreconcil_table <?php echo $hide ?>">
                            <tr bgcolor="#eee">
                                <td colspan="4"><b>Daybook Receipt (UnReconciled and Unapproved)</b></td>
                            </tr>
                            <tr>
                                <th width="70px">#ID</th>
                                <th>Date</th>
                                <th>Description</th>
                                <th>Amount</th>
                            </tr>
                            <?php
                            $clienttotal = 0;

                            foreach ($client1 as $newclient) {
                                ?>
                                <tr>
                                    <td><b>
                                            <?php echo $newclient['exp_id'] ?>
                                        </b></td>

                                    <td>
                                        <?php echo date('d-M-Y', strtotime($newclient['date'])); ?>
                                    </td>
                                    <td>
                                        <?php print_r($newclient['description']); ?>
                                    </td>
                                    <td align="right" border="1">
                                        <?php echo Controller::money_format_inr($newclient['receipt'], 2, 1); ?>
                                    </td>
                                    <?php $clienttotal += $newclient['receipt']; ?>
                                </tr>

                            <?php }
                            echo '<tr bgcolor="#eee"><td align="right" colspan="3"><b>Total</b></td><td align="right"  class="text-right project-report-width"><b>' . Controller::money_format_inr($clienttotal, 2, 1) . '</b></td></tr>';
                            ?>


                        </table>
                        <?php echo $br ?>
                        <table class="table unreconcil_table <?php echo $hide ?>">
                            <tr>
                                <td colspan="8" bgcolor="#eee"><b>Purchase Bill (UnReconciled and Unapproved)</b></td>
                            </tr>
                            <tr>
                                <td>#ID</td>
                                <td><b>Expense Type</b></td>
                                <td><b>Date</b></td>
                                <td><b>Bill No</b></td>
                                <td><b>Vendor</b></td>
                                <td><b>Purchase No</b></td>
                                <td><b>Bill Amount</b></td>
                                <td><b>Total</b></td>
                            </tr>

                            <?php
                            $bill_amount = 0;
                            $bill_total_new_amount = 0;
                            $purchase_list = Projects::model()->groupPurchaseBillItems($purchase1);
                            $expense_types_listing = Projects::model()->setExpenseTypesListingofPurchaseBill($purchase_list);

                            foreach ($purchase_list as $purchase) {

                                $count = count($purchase);
                                $count = ($count - 2);
                                $index = 1;
                                foreach ($purchase as $key => $value) {
                                    if (is_array($value)) {
                                        $amount = $value['bill_totalamount'] + $value['bill_additionalcharge'];
                                        $vendor = Vendors::model()->findByPk($value['vendor_id']);

                                        ?>
                                        <tr>
                                            <td><b>#
                                                    <?php echo $value['bill_id'] ?>
                                                </b></td>
                                            <?php
                                            if ($index == 1) {
                                                ?>
                                                <td rowspan="<?php echo $count; ?>">
                                                    <?php echo $purchase['expense_head'] ?>
                                                </td>
                                                <?php
                                            }
                                            ?>

                                            <!--html added-->
                                            <td>
                                                <?php echo date('d-M-Y', strtotime($value['bill_date'])); ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($value['purchase_no'] != '') {
                                                    ?>
                                                    <a target="_blank"
                                                        href="<?php echo Yii::app()->createAbsoluteUrl('bills/view', array('id' => $value['bill_id'])); ?>">
                                                        <?php echo $value['bill_number']; ?>
                                                    </a>
                                                <?php } else { ?>
                                                    <a target="_blank"
                                                        href="<?php echo Yii::app()->createAbsoluteUrl('bills/billview', array('id' => $value['bill_id'])); ?>">
                                                        <?php echo $value['bill_number']; ?>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <?php echo $vendor->name; ?>
                                            </td>
                                            <td>
                                                <?php echo $value['purchase_no']; ?>
                                            </td>
                                            <td align="right" border="1">
                                                <?php echo Controller::money_format_inr($amount, 2, 1); ?>
                                            </td>
                                            <?php
                                            if ($index == 1) {
                                                ?>
                                                <td class="text-right" rowspan="<?php echo $count; ?>">
                                                    <?php echo Controller::money_format_inr($purchase['total_amount'], 2) ?>
                                                </td>
                                            <?php } ?>
                                            <!--html  added-->
                                            <?php $bill_amount += $amount;
                                            $bill_total_new_amount += $amount;
                                            ?>
                                        </tr>

                                        <?php
                                    }
                                    $index++;
                                }
                            }
                            // check amount html code
                            echo '<tr bgcolor="#eee"><td align="right" colspan="7"><b>Total</b></td><td align="right" ><b>' . Controller::money_format_inr($bill_amount, 2, 1) . '</b></td></tr>';
                            ?>
                        </table>
                    <?php }
                }
                $despatch_amount = 0;
                if (isset($_GET['summary']) && $_GET['summary'] == 0) {
                    echo $br;
                    //echo "<pre>";print_r($item_report_data);exit;
                    if (!empty($item_report_data)) { ?>
                        <div class="project-report-block">
                            <table class="table table-bordered">
                                <tr class="sticky-1" bgcolor="#eee">
                                    <td colspan="4"><b>Dispatch Details</b></td>
                                </tr>
                                <tr class="sticky-2">
                                    <th>Despatch No</th>
                                    <th>Warehouse From </th>

                                    <th>Qty</th>
                                    <th>Amount</th>
                                </tr>
                                <?php

                                foreach ($dispatch_details as $data) {
                                    ?>
                                    <tr>
                                        <td>
                                            <?= isset($data['warehousedespatch_no']) ?
                                                CHtml::link($data['warehousedespatch_no'], 'index.php?r=wh/warehousedespatch/view&despatchid=' . $data['warehousedespatch_id'], array('class' => 'link', 'target' => '_blank')) : '-' ?>
                                        </td>
                                        <td>
                                            <?php
                                            $wh_from = Warehouse::model()->findByPk($data['warehousedespatch_warehouseid']);
                                            echo $wh_from['warehouse_name'];
                                            ?>
                                        </td>

                                        <td>
                                            <?php echo $data['warehousedespatch_quantity'] ?>
                                        </td>
                                        <td class="text-right project-report-width">
                                            <?php
                                            $sql_despatch = "SELECT SUM(
                                CASE 
                                    WHEN warehousedespatch_amount IS NOT NULL THEN warehousedespatch_amount
                                    ELSE warehousedespatch_baseqty * warehousedespatch_baserate
                                END
                            ) AS total_amount
                            FROM jp_warehousedespatch_items
                            WHERE warehousedespatch_id =" . $data['warehousedespatch_id'];


                                            $rate = Yii::app()->db->createCommand($sql_despatch)->queryScalar();
                                            echo Controller::money_format_inr($rate, 2);
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $despatch_amount += $rate;
                                }
                                echo '<tr bgcolor="#eee"><td align="right" colspan="3"><b>Total</b></td><td align="right" class="text-right project-report-width" ><b>' . Controller::money_format_inr($despatch_amount, 2, 1) . '</b></td></tr>';
                                ?>
                            </table>
                        </div>
                    <?php }
                } ?>
                <?php echo $br ?>
                <!--CONSUMPTION REQUEST STARTS-->
                <?php

                $consumption_amount = 0;
                if ($_GET['reconcil_type'] == 'reconcil') {
                    if (isset($_GET['summary']) && $_GET['summary'] == 0) {
                        echo $br;
                        //echo "<pre>";print_r($item_report_data);exit;
                        if (!empty($item_report_data)) { ?>
                            <div class="project-report-block">
                                <table class="table table-bordered">
                                    <tr class="sticky-1" bgcolor="#eee">
                                        <td colspan="4"><b>Consumption Request Details(Approved)</b></td>
                                    </tr>
                                    <tr class="sticky-2">
                                        <th>#ID</th>
                                        <th>Warehouse From </th>
                                        <th>Material</th>

                                        <th>Amount</th>
                                    </tr>
                                    <?php
                                    $consumption_amount = 0;
                                    foreach ($consumptionreq_details as $datas) {
                                        if (!empty($datas)) {
                                            $rate = 0;


                                            $consumptionreq = PmsAccWprItemConsumed::model()->findAll('consumption_id = :consumption_id', array(':consumption_id' => $datas->id));
                                            $material_det = array();
                                            $items = '';
                                            $material_rate = 0;

                                            foreach ($consumptionreq as $req) {
                                                $material = Materials::Model()->findByPk($req['coms_material_id']);
                                                if (!empty($material)) {
                                                    $material_name = $material['material_name'];
                                                    array_push($material_det, $material_name);
                                                    $items = implode(',', $material_det);
                                                }
                                                $material_rate += $req['item_rate'] * $req['item_qty'];

                                            }
                                            $consumption_amount += $material_rate;




                                            ?>
                                            <tr>
                                                <td>
                                                    <?php echo "#" . $datas['id'] ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $wh_from = Warehouse::model()->findByPk($datas['warehouse_id']);
                                                    echo $wh_from['warehouse_name'];
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php

                                                    echo $items;
                                                    ?>
                                                </td>

                                                <td class="text-right project-report-width">
                                                    <?php

                                                    echo Controller::money_format_inr($material_rate, 2);
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php

                                        }
                                    }
                                    echo '<tr bgcolor="#eee"><td align="right" colspan="3"><b>Total</b></td><td align="right" class="text-right project-report-width" ><b>' . Controller::money_format_inr($consumption_amount, 2, 1) . '</b></td></tr>';
                                    ?>
                                </table>
                            </div>
                        <?php }
                    }

                } else {
                    if (isset($_GET['summary']) && $_GET['summary'] == 0) {
                        echo $br;
                        //echo "<pre>";print_r($item_report_data);exit;
                        if (!empty($item_report_data)) { ?>
                            <div class="project-report-block">
                                <table class="table table-bordered">
                                    <tr class="sticky-1" bgcolor="#eee">
                                        <td colspan="4"><b>Consumption Request Details(UnApproved)</b></td>
                                    </tr>
                                    <tr class="sticky-2">
                                        <th>#ID</th>
                                        <th>Warehouse From </th>
                                        <th>Material</th>

                                        <th>Amount</th>
                                    </tr>
                                    <?php

                                    foreach ($unconsumptionreq_details as $datass) {
                                        if (!empty($datass)) {

                                            $consumptionreq = PmsAccWprItemConsumed::model()->findAll('consumption_id = :consumption_id', array(':consumption_id' => $datass->id));
                                            $material_det = array();
                                            $items = '';
                                            $material_rate = 0;

                                            foreach ($consumptionreq as $req) {
                                                $material = Materials::Model()->findByPk($req['coms_material_id']);
                                                if (!empty($material)) {
                                                    $material_name = $material['material_name'];
                                                    array_push($material_det, $material_name);
                                                    $items = implode(',', $material_det);
                                                }
                                                $material_rate += $req['item_rate'] * $req['item_qty'];

                                            }
                                            $consumption_amount += $material_rate;





                                            ?>
                                            <tr>
                                                <td>
                                                    <?php echo "#" . $datass['id'] ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $wh_from = Warehouse::model()->findByPk($datass['warehouse_id']);
                                                    echo $wh_from['warehouse_name'];
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $material_name = '';

                                                    echo $items;
                                                    ?>
                                                </td>

                                                <td class="text-right project-report-width">
                                                    <?php

                                                    echo Controller::money_format_inr($material_rate, 2);
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php

                                        }
                                    }
                                    echo '<tr bgcolor="#eee"><td align="right" colspan="3"><b>Total</b></td><td align="right" class="text-right project-report-width"><b>' . Controller::money_format_inr($consumption_amount, 2, 1) . '</b></td></tr>';
                                    ?>
                                </table>
                            </div>
                        <?php }
                    }
                }
                ?>
                <!--CONSUMPTION REQUEST ENDS-->
                <?php echo $br ?>
                <?php
                $return_total_amount = 0;
                if ($_GET['reconcil_type'] == 'reconcil') {
                    if (isset($_GET['summary']) && $_GET['summary'] == 0) {
                        ?>
                        <div class="project-report-block">
                            <table class="table reconcil_table <?php echo $hide ?>">
                                <tr class="sticky-1" bgcolor="#eee">
                                    <td colspan="6"><b>Purchase Return</b></td>
                                </tr>
                                <tr class="sticky-2">
                                    <th width="70px">Expense Type</th>
                                    <th>Date</th>
                                    <th>Return No.</th>
                                    <th>Bill No.</th>
                                    <th>Vendor</th>
                                    <th>Total</th>
                                </tr>
                                <?php
                                // Initialize $purchase_return
                                $purchase_return = Projects::model()->setPurchaseReturn($expreceipt_return);

                                $return_total_amount = 0;
                                $return_data_array = [];

                                // Organize return data by expense type
                                foreach ($purchase_return as $key => $return_datas) {
                                    foreach ($return_datas as $key => $return_data) {
                                        if (is_array($return_data)) {
                                            $return_total_amount += $return_data['amount'];
                                            $return_data_array[$return_data['exptype']][] = $return_data;
                                        }
                                    }
                                }

                                // Iterate over each expense type and its return data
                                foreach ($return_data_array as $key => $return_data) {
                                    $expensehead = Yii::app()->db->createCommand("SELECT type_name FROM `jp_expense_type` WHERE type_id ='" . $key . "'")->queryRow();
                                    $rowRendered = false; // Ensure rowspan is applied only once
                                    $rowSum = 0;

                                    // Calculate total amount for each expense type
                                    foreach ($return_data as $value) {
                                        $rowSum += $value['paidamount'];
                                    }

                                    foreach ($return_data as $value) {
                                        $vendorname = Yii::app()->db->createCommand("SELECT name FROM `jp_vendors` WHERE vendor_id='" . $value['vendor_id'] . "'")->queryRow();
                                        ?>
                                        <tr>
                                            <?php if (!$rowRendered) { ?>
                                                <!-- Render Expense Type and Total only once -->
                                                <td rowspan="<?php echo count($return_data); ?>">
                                                    <?php echo $expensehead['type_name']; ?>
                                                </td>
                                            <?php } ?>
                                            <td>
                                                <?php echo date('d-M-Y', strtotime($value['date'])); ?>
                                            </td>
                                            <td>
                                                <?php echo $value['return_number']; ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo CHtml::link($value['bill_number'], Yii::app()->createUrl('bills/view&id=' . $value['bill_id']), array('class' => '', 'target' => '_blank'));
                                                ?>
                                            </td>
                                            <td>
                                                <?php echo $vendorname['name']; ?>
                                            </td>
                                            <?php if (!$rowRendered) { ?>
                                                <td class="text-right project-report-width" rowspan="<?php echo count($return_data); ?>">
                                                    <?php echo Controller::money_format_inr($rowSum, 2); ?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                        $rowRendered = true; // Mark that rowspan has been rendered
                                    }
                                }
                                ?>
                                <tr bgcolor="#eee">
                                    <td colspan="5" align="right"><b>Total</b></td>
                                    <td class="text-right project-report-width"><b>
                                            <?php echo Controller::money_format_inr($return_total_amount, 2); ?>
                                        </b></td>
                                </tr>
                            </table>
                        </div>

                        <?php echo $br ?>
                        <div class="project-report-block">
                            <table class="table reconcil_table <?php echo $hide ?>">
                                <tr class="sticky-1" bgcolor="#eee">
                                    <td colspan="5"><b>Daybook Expense</b></td>
                                </tr>
                                <tr class="sticky-2">
                                    <th width="70px">Expense Type</th>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>Amount</th>
                                    <th>Total</th>
                                </tr>
                                <?php
                                $grand = 0;
                                $daybook_exp_total = 0;
                                $daybook_exp_final_total = 0;
                                foreach ($sql as $new) {
                                    $total = 0;
                                    $where = '';

                                    // Add date filtering
                                    if (!empty($date_from)) {
                                        $where .= " AND {$tblpx}expenses.date >= '" . $date_from . "'";
                                    }
                                    if (!empty($date_to)) {
                                        $date_to = date('Y-m-d', strtotime($date_to));
                                        $where .= " AND {$tblpx}expenses.date <= '" . $date_to . "'";
                                    }

                                    $sql = "SELECT * FROM " . $tblpx . "expenses 
                                            WHERE amount != 0 AND exptype = " . $new['type_id'] . "
                                            AND projectid = " . $new['project_id'] . " " . $where . "
                                            AND  type ='73' AND exptype IS NOT NULL AND bill_id IS NULL AND vendor_id IS NOT NULL 
                                            AND (" . $newQuery2 . ")
                                            AND (reconciliation_status IS NULL OR reconciliation_status = 1)
                                            ORDER BY date";

                                    $sqlexp = Yii::app()->db->createCommand($sql)->queryAll();

                                    if (!empty($sqlexp)) {
                                        $day_book_expense_count = count($sqlexp);
                                        $daybook_expense_tot_amount = 0;

                                        // Calculate total amount for the expense type
                                        foreach ($sqlexp as $newsql) {
                                            $daybook_expense_tot_amount += $newsql['paidamount'];
                                        }

                                        $rowRendered = false; // Ensure rowspan is applied only once
                                        foreach ($sqlexp as $newsql) {
                                            ?>
                                            <tr>
                                                <?php if (!$rowRendered) { ?>
                                                    <!-- Render Expense Type and Total only once -->
                                                    <td rowspan="<?php echo $day_book_expense_count; ?>">
                                                        <?php echo $new['type_name'] ? $new['type_name'] : "N/A"; ?>
                                                    </td>
                                                <?php } ?>
                                                <td>
                                                    <?php echo date('d-M-Y', strtotime($newsql['date'])); ?>
                                                </td>
                                                <td>
                                                    <?php echo " " . $newsql['works_done']; ?>
                                                    <?php print_r($newsql['description']); ?>
                                                </td>
                                                <td align="right" class="text-right project-report-width">
                                                    <?php echo Controller::money_format_inr($newsql['paidamount'], 2, 1); ?>
                                                </td>
                                                <?php if (!$rowRendered) { ?>
                                                    <td class="text-right project-report-width" rowspan="<?php echo $day_book_expense_count; ?>">
                                                        <?php echo Controller::money_format_inr($daybook_expense_tot_amount, 2); ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                            $rowRendered = true; // Mark that rowspan has been rendered
                                        }

                                        $grand += $daybook_expense_tot_amount;
                                        $daybook_exp_total += $daybook_expense_tot_amount;
                                        $daybook_exp_final_total += $daybook_expense_tot_amount;
                                    }
                                }
                                ?>
                                <tr bgcolor="#eee">
                                    <td align="right" colspan="4"><b>Total</b></td>
                                    <td align="right" class="text-right project-report-width"><b>
                                            <?php echo Controller::money_format_inr($daybook_exp_total, 2); ?>
                                        </b></td>
                                </tr>
                            </table>
                        </div>

                        <?php echo $br ?>
                        <?php echo $br ?>
                        <div class="project-report-block">
                            <table class="table reconcil_table <?php echo $hide ?>">
                                <tr class="sticky-1" bgcolor="#eee">
                                    <td colspan="8"><b>Daybook Expense Credit Transactions</b></td>
                                </tr>
                                <tr class="sticky-2">
                                    <th>ID</th>
                                    <th width="70px">Expense Type</th>
                                    <th>Date</th>
                                    <th>Bill No</th>
                                    <th>Vendor/Subcontractor</th>
                                    <th>Description</th>
                                    <th>Amount</th>
                                    <th>Total</th>
                                </tr>
                                <?php
                                $grand = 0;
                                $daybook_exp_total = 0;

                                foreach ($creditsql as $new) {
                                    $total = 0;
                                    $where = '';

                                    // Add date filtering
                                    if (!empty($date_from)) {
                                        $where .= " AND {$tblpx}expenses.date >= '" . $date_from . "'";
                                    }
                                    if (!empty($date_to)) {
                                        $date_to = date('Y-m-d', strtotime($date_to));
                                        $where .= " AND {$tblpx}expenses.date <= '" . $date_to . "'";
                                    }

                                    $sql = "SELECT * FROM " . $tblpx . "expenses 
                                        WHERE amount != 0 AND exptype = " . $new['type_id'] . "
                                        AND projectid = " . $new['project_id'] . " " . $where . "
                                         AND  type ='73' AND exptype IS NOT NULL AND vendor_id IS NOT NULL 
                                        AND ( expense_type='107' OR expense_type = '0' ) AND (" . $newQuery2 . ")
                                        AND (reconciliation_status IS NULL OR reconciliation_status = 1)
                                         GROUP BY COALESCE(bill_id, exp_id)
                                        ORDER BY date";

                                    $sqlexp = Yii::app()->db->createCommand($sql)->queryAll();

                                    if (!empty($sqlexp)) {
                                        $day_book_expense_count = count($sqlexp);
                                        $daybook_expense_tot_amount = 0;

                                        foreach ($sqlexp as $newsql) {
                                            $daybook_expense_tot_amount += $newsql['amount'];
                                        }

                                        $rowRendered = false;
                                        foreach ($sqlexp as $newsql) {
                                            ?>
                                            <tr>
                                                <td>
                                                    <?php echo "#" . $newsql['exp_id']; ?>
                                                </td>
                                                <?php if (!$rowRendered) { ?>
                                                    <!-- Render Expense Type and Total only once -->
                                                    <td rowspan="<?php echo $day_book_expense_count; ?>">
                                                        <?php echo $new['type_name'] ? $new['type_name'] : "N/A"; ?>
                                                    </td>
                                                <?php } ?>
                                                <td>
                                                    <?php echo date('d-M-Y', strtotime($newsql['date'])); ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $bill_no = '-';
                                                    $bill_id = '';
                                                    if (!empty($newsql['bill_id'])) {
                                                        $bill_id = $newsql['bill_id'];
                                                        $bills = Bills::Model()->findByPk($newsql['bill_id']);
                                                        $bill_no = $bills->bill_number;
                                                    }

                                                    ?>
                                                    <a target="_blank" title='' onmouseover=getBillItems(this)
                                                        href="<?php echo Yii::app()->createAbsoluteUrl('bills/view', array('id' => $bill_id)); ?>"
                                                        data-bill-id="<?php echo $bill_id ?>">
                                                        <?php echo $bill_no; ?>
                                                    </a>
                                                </td>
                                                <td>
                                                    <?php
                                                    $name = '';
                                                    if (!empty($newsql['vendor_id'])) {
                                                        $vendors = Vendors::Model()->findByPk($newsql['vendor_id']);
                                                        $name = $vendors['name'];
                                                    } else if (!empty($newsql['subcontractor_id'])) {
                                                        $scpayment = SubcontractorPayment::model()->findByPk($newsql['subcontractor_id']);
                                                        $name = $scpayment->subcontractor->subcontractor_name;

                                                    }
                                                    echo $name;
                                                    ?>

                                                </td>
                                                <td>
                                                    <?php echo " " . $newsql['works_done']; ?>
                                                    <?php print_r($newsql['description']); ?>
                                                </td>
                                                <td align="right" class="text-right project-report-width">
                                                    <?php echo Controller::money_format_inr($newsql['amount'], 2, 1); ?>
                                                </td>
                                                <?php if (!$rowRendered) { ?>
                                                    <td class="text-right" rowspan="<?php echo $day_book_expense_count; ?>">
                                                        <?php echo Controller::money_format_inr($daybook_expense_tot_amount, 2); ?>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                            $rowRendered = true;
                                        }
                                        $grand += $daybook_expense_tot_amount;
                                        $daybook_exp_total += $daybook_expense_tot_amount;

                                    }
                                }
                                ?>
                                <tr bgcolor="#eee">
                                    <td align="right" colspan="7"><b>Total</b></td>
                                    <td align="right" class="text-right project-report-width"><b>
                                            <?php echo Controller::money_format_inr($daybook_exp_total, 2); ?>
                                        </b></td>
                                </tr>
                            </table>
                        </div>

                        <?php echo $br ?>
                    <?php }
                } else {
                    if (isset($_GET['summary']) && $_GET['summary'] == 0) {
                        ?>
                        <table class="table unreconcil_table <?php echo $hide ?>">
                            <tr bgcolor="#eee">
                                <td colspan="5"><b>Daybook Expense(UnReconciled)</b></td>
                            </tr>
                            <tr>
                                <th>Expense Type</th>
                                <th>Date</th>
                                <th>Description</th>
                                <th>Amount</th>
                                <th>Total</th>
                            </tr>
                            <?php
                            $grand = 0;
                            $total = 0;
                            $daybook_exp_total = 0;
                            $daybook_exp_final_total = 0;
                            foreach ($sql as $new) {
                                $daybook_expense_index = 1;
                                $where = '';
                                if (!empty($date_from) && !empty($date_to)) {
                                    $where .= " AND {$tblpx}expenses.date between '" . $date_from . "' and'" . $date_to . "'";
                                } else {
                                    if (!empty($date_from)) {
                                        $where .= " AND {$tblpx}expenses.date >= '" . $date_from . "'";
                                    }
                                    if (!empty($date_to)) {
                                        $date_to = date('Y-m-d', strtotime($date_to));
                                        $where .= " AND {$tblpx}expenses.date <= '" . $date_to . "'";
                                    }
                                }

                                $cheque_array = array();
                                foreach ($unreconciled_data as $data) {
                                    $recon_checque = "'{$data["reconciliation_chequeno"]}'";
                                    array_push($cheque_array, $recon_checque);
                                }
                                $cheque_array1 = implode(',', $cheque_array);

                                $sql = "select * from " . $tblpx . "expenses "
                                    . " WHERE amount!=0 AND exptype=" . $new['type_id']
                                    . " AND projectid=" . $new['project_id'] . " " . $where
                                    . " AND exptype IS NOT NULL AND vendor_id IS NOT NULL "
                                    . " AND bill_id IS NULL AND (" . $newQuery2 . ") "
                                    . " AND reconciliation_status= 0   "
                                    . " ORDER BY date ";
                                $sqlexp = Yii::app()->db->createCommand($sql)->queryAll();


                                if (!empty($sqlexp)) {
                                    $day_book_expense_count = count($sqlexp);

                                    ?>

                                    <?php
                                    $total = $daybook_expense_tot_amount = 0;
                                    foreach ($sqlexp as $newsql) {
                                        $expense_types_listing = Projects::model()->setExpenseList($expense_types_listing, $newsql);
                                        $daybook_expense_tot_amount += $newsql['paidamount'];
                                    }
                                    foreach ($sqlexp as $newsql) {
                                        if ($newsql['paidamount'] != 0) {
                                            ?>
                                            <tr>
                                                <?php
                                                if ($daybook_expense_index == 1) {
                                                    ?>
                                                    <td rowspan="<?php echo $day_book_expense_count ?>">
                                                        <?php echo ($new['type_name']); ?>
                                                    </td>
                                                    <?php
                                                }
                                                ?>

                                                <td>
                                                    <?php echo date('d-M-Y', strtotime($newsql['date'])); ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo " " . $newsql['works_done'];
                                                    print_r($newsql['description']);
                                                    ?>
                                                </td>
                                                <td align="right" class="text-right project-report-width">
                                                    <?php echo Controller::money_format_inr($newsql['paidamount'], 2, 1); ?>
                                                </td>
                                                <?php $total += $newsql['paidamount']; ?>
                                                <?php
                                                if ($daybook_expense_index == 1) {
                                                    ?>
                                                    <td class="text-right project-report-width" rowspan="<?php echo $day_book_expense_count ?>">
                                                        <?php echo Controller::money_format_inr($daybook_expense_tot_amount, 2) ?>
                                                    </td>
                                                    <?php
                                                }
                                                ?>
                                            </tr>
                                            <?php
                                        }
                                        $daybook_expense_index++;
                                    }

                                    ?>


                                    <?php
                                    $grand += $total;
                                    $daybook_exp_total += $total;
                                    $daybook_exp_final_total += $total;
                                }
                                ?>
                                <?php

                            }
                            ?>
                            <tr bgcolor="#eee">
                                <td align="right" colspan="4"><b>Total</b></td>
                                <td align="right" class="text-right project-report-width"><b>
                                        <?php echo Controller::money_format_inr($daybook_exp_total, 2); ?>
                                    </b>
                                </td>
                            </tr>

                        </table>
                    <?php }
                } ?>
                <!--SUBCONTRACTOR BILL STARTS-->
                <?php

                if (isset($_GET['summary']) && $_GET['summary'] == 0) {
                    ?>
                    <?php echo $br ?>

                    <div class="project-report-block">
                        <table class="table reconcil_table <?php echo $hide ?>">
                            <tr class="sticky-1" bgcolor="#eee">
                                <td colspan="9">
                                    <b>Subcontractor Bills (Pending)</b> &nbsp;&nbsp;
                                    <span style="color:green;">
                                        <b>**NOTE: Approved ScQuotation's Pending Bills Only Displayed Here</b>
                                    </span>&nbsp;&nbsp;
                                    <?php if (!empty($unApprovedSQCount)) { ?>

                                        <span style="color:red;">
                                            <b>[ UnApproved ScQuotation Count:
                                                <?php

                                                echo CHtml::link(
                                                    '<span class="badge badge-danger">' . htmlspecialchars($unApprovedSQCount) . '</span>',
                                                    Yii::app()->createUrl('subcontractor/quotations', array(
                                                        'Scquotation[company_id]' => '',
                                                        'Scquotation[project_id]' => $project_id,
                                                        'Scquotation[subcontractor_id]' => '',
                                                        'Scquotation[scquotation_no]' => '',
                                                        'Scquotation[scquotation_amount]' => '',
                                                        'Scquotation[approve_status]' => 'No'
                                                    )),
                                                    array(
                                                        'class' => 'nav-link notification',
                                                        'target' => '_blank' // Optional: Opens the link in a new tab
                                                    )
                                                );
                                                ?>
                                                ]
                                            </b>
                                        </span>

                                    <?php } ?>
                                    &nbsp; &nbsp; &nbsp;
                                </td>

                            </tr>
                            <tr class="sticky-2">
                                <th>#ID</th>
                                <th>Expense Type</th>
                                <th>Date</th>
                                <th>Sc Quotation</th>
                                <th>Bill No</th>
                                <th>Subcontractor</th>
                                <th>Bill Amount</th>
                                <th>Paid Amount</th>
                                <th>Balance</th>
                            </tr>
                            <?php
                            $check_arrays = array();
                            $daybook_expenseamounts = 0;
                            $scbill_amount = 0;
                            $total_bill_amount = 0;
                            $sc_balance_total = 0;
                            $subcontractor_payment_listss = Projects::model()->setSubcontractorPaymentForBill($sc_quotations);
                            foreach ($subcontractor_payment_listss as $daybook_expenses) {
                                $scpayment_group_counts = count($daybook_expenses) - 2;
                                $sc_indexs = 1;
                                $total_bill_amount += $daybook_expenses['bill_total_amount'];
                                $sc_balance_total += $daybook_expenses['bill_total_amount'] - $daybook_expenses['paid_amount'];
                                $scbill_amount = $daybook_expenses['bill_total_amount'];
                                foreach ($daybook_expenses as $newdatas) {
                                    if (is_array($newdatas)) {
                                        //$daybook_expenseamounts += $newdatas['paid'];
                                        $tblpx = Yii::app()->db->tablePrefix;

                                        ?>

                                        <tr>
                                            <td>
                                                <?php echo "#" . $newdatas['bill_id'] ?>
                                            </td>
                                            <?php
                                            if ($sc_indexs == 1) {
                                                // die($scpayment_group_counts);
                                                ?>
                                                <td rowspan="<?php echo $scpayment_group_counts; ?>"
                                                    style="<?php echo $daybook_expenses[0]['expense_head_name'] == 'N/A' ? "background-color: #d0686836;" : "" ?>">
                                                    <?php echo $daybook_expenses[0]['expense_head_name']; ?>
                                                </td>
                                                <?php
                                            }
                                            ?>

                                            <td>
                                                <?php echo date('d-M-Y', strtotime($newdatas['bill_date'])); ?>
                                            </td>
                                            <td>
                                                <?php echo $newdatas['quot_no']; ?>
                                            </td>

                                            <td>
                                                <?= isset($newdatas['bill_no']) ?
                                                    CHtml::link($newdatas['bill_no'], 'index.php?r=subcontractorbill/view&id=' . $newdatas['bill_id'], array('class' => 'link', 'target' => '_blank')) : '-' ?>
                                            </td>
                                            <td>
                                                <?php

                                                $subcontractor_data = Subcontractor::model()->findByPk($newdatas['subcontractor_id']);
                                                echo $subcontractor_data['subcontractor_name'];

                                                ?>
                                            </td>
                                            <td align="right" class="text-right project-report-width" border="1">
                                                <?php echo Controller::money_format_inr($newdatas['bill_amount'], 2, 1); ?>
                                            </td>
                                            <td align="right" class="text-right project-report-width" border="1">
                                                <?php echo Controller::money_format_inr($newdatas['paid_amount'], 2, 1); ?>
                                            </td>
                                            <td align="right" class="text-right project-report-width" border="1">
                                                <?php echo Controller::money_format_inr($newdatas['balance'], 2, 1); ?>
                                            </td>

                                        </tr>

                                        <?php
                                        $sc_indexs++;
                                    }
                                }
                            }
                            //$grand += $daybook_expenseamount;
                            echo '<tr bgcolor="#eee"><td align="right" colspan="8"><b>Balance</b></td><td align="right"  class="text-right project-report-width"><b>' . Controller::money_format_inr($sc_balance_total, 2, 1) . '</b></td></tr>';
                            ?>

                        </table>
                    </div>
                <?php } ?>

                <!--SUBCONTRACTOR BILL ENDS-->

                <?php
                if ($_GET['reconcil_type'] == 'reconcil') {
                    if (isset($_GET['summary']) && $_GET['summary'] == 0) {
                        ?>
                        <?php echo $br ?>
                        <div class="project-report-block">
                            <table class="table reconcil_table <?php echo $hide ?>">
                                <tr class="sticky-1" bgcolor="#eee">
                                    <td colspan="8"><b>Subcontractor Payment<b> &nbsp; &nbsp; &nbsp;
                                                <span style="color:red;"><b>* For more data check the Subcontractor payment report
                                                        <b></span>
                                                <span style="color:green;">
                                                    <b>['N/A' includes Subcontractor payment without Quotation Also]</b>
                                                </span>&nbsp;&nbsp;
                                    </td>
                                </tr>
                                <tr class="sticky-2">
                                    <th>#ID</th>
                                    <th>Expense Type</th>
                                    <th>Date</th>
                                    <th>Quotation No</th>
                                    <th>Description</th>
                                    <th>Subcontractor</th>
                                    <th>Paid Amount</th>
                                    <th>Total</th>
                                </tr>
                                <?php
                                $check_array = array();
                                $daybook_expenseamount = 0;
                                $scquotationNo = '';
                                $subcontractor_payment_lists = Projects::model()->setSubcontractorPayment($daybook_expense);
                                // echo "<pre>";print_r($subcontractor_payment_lists);exit;
                                foreach ($subcontractor_payment_lists as $daybook_expense) {
                                    $scpayment_group_count = count($daybook_expense) - 1;
                                    $sc_index = 1;
                                    foreach ($daybook_expense as $newdata) {
                                        if (is_array($newdata)) {
                                            $daybook_expenseamount += $newdata['paid'];
                                            $tblpx = Yii::app()->db->tablePrefix;
                                            $payment = SubcontractorPayment::model()->findByPk($newdata['subcontractor_id']);
                                            $scQuotation = Scquotation::model()->findByPk($payment['subcontractor_id']);
                                            if (!empty($scQuotation)) {
                                                $scquotationNo = $scQuotation->scquotation_no;
                                            }
                                            $query = "";
                                            $subcontractor_id = '';
                                            if (isset($payment->subcontractor_id)) {
                                                $query = ' AND b.subcontractor_id=' . $payment->subcontractor_id . '';
                                                $subcontractor_id = $payment->subcontractor_id;
                                            }
                                            $sub_total1 = Yii::app()->db->createCommand("select SUM(a.paidamount) as paid_amount,count(a.exp_id) as total_count,b.* from jp_expenses a LEFT JOIN jp_subcontractor_payment b ON a.subcontractor_id=b.payment_id WHERE a.amount !=0 AND a.projectid=" . $project_id . " AND a.type = '73' AND a.exptype IS NULL AND a.vendor_id IS NULL AND a.bill_id IS NULL AND a.date ='" . $newdata['date'] . "' " . $query . " GROUP BY a.date,b.subcontractor_id")->queryRow();
                                            $check_data = $newdata['date'] . '-' . $subcontractor_id;
                                            if (in_array($check_data, $check_array)) {
                                                $flag = 0;
                                                $row_count = "";
                                            } else {
                                                $flag = 1;
                                                $row_count = $sub_total1['total_count'];
                                            }
                                            array_push($check_array, $check_data);
                                            ?>

                                            <tr>
                                                <td>#
                                                    <?php echo $newdata['subcontractor_id'] ?>
                                                </td>
                                                <?php
                                                if ($sc_index == 1) {
                                                    ?>

                                                    <td rowspan="<?php echo $scpayment_group_count; ?>"
                                                        style="<?php echo $daybook_expense[0]['expense_head_name'] == 'N/A' ? "background-color: #d0686836;" : "" ?>">
                                                        <?php echo $daybook_expense[0]['expense_head_name']; ?>
                                                    </td>
                                                    <?php
                                                }
                                                ?>

                                                <td>
                                                    <?php echo date('d-M-Y', strtotime($newdata['date'])); ?>
                                                </td>
                                                <td>
                                                    <?php echo $scquotationNo; ?>
                                                </td>
                                                <td>
                                                    <?php print_r($newdata['description']); ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $payment = SubcontractorPayment::model()->findByPk($newdata['subcontractor_id']);
                                                    if (isset($payment->subcontractor_id)) {
                                                        $subcontractor_data = Subcontractor::model()->findByPk($payment->subcontractor_id);
                                                        echo $subcontractor_data['subcontractor_name'];
                                                    }
                                                    ?>
                                                </td>
                                                <td align="right" border="1" class="text-right project-report-width">
                                                    <?php echo Controller::money_format_inr($newdata['paidamount'], 2, 1); ?>
                                                </td>
                                                <?php
                                                if ($sc_index == 1) {
                                                    ?>
                                                    <td rowspan="<?php echo $scpayment_group_count; ?>" class="text-right project-report-width">
                                                        <?php echo Controller::money_format_inr($daybook_expense['total_amount'], 2, 1); ?>
                                                    </td>
                                                    <?php
                                                }
                                                ?>
                                            </tr>

                                            <?php
                                            $sc_index++;
                                        }
                                    }
                                }
                                $grand += $daybook_expenseamount;
                                echo '<tr bgcolor="#eee"><td align="right" colspan="7"><b>Total</b></td><td align="right" class="text-right project-report-width"><b>' . Controller::money_format_inr($daybook_expenseamount, 2, 1) . '</b></td></tr>';
                                ?>

                            </table>
                        </div>
                    <?php }
                } else {
                    if (isset($_GET['summary']) && $_GET['summary'] == 0) {
                        ?>
                        <?php echo $br ?>
                        <table class="table unreconcil_table <?php echo $hide ?>">
                            <tr bgcolor="#eee">
                                <td colspan="6"><b>Subcontractor Payment<b> (UnReconciled and Unapproved)
                                </td>
                            </tr>
                            <tr>
                                <th>Expense Type</th>
                                <th>Date</th>
                                <th>Description</th>
                                <th>Subcontractor</th>
                                <th>Amount</th>
                                <th>Total</th>
                            </tr>
                            <?php
                            $check_array = array();
                            $daybook_expenseamount = 0;
                            $subcontractor_payment_lists = Projects::model()->setSubcontractorPayment($daybook_expense1);
                            foreach ($subcontractor_payment_lists as $daybook_expense1) {
                                $scpayment_group_count = count($daybook_expense1) - 1;
                                $sc_index = 1;
                                foreach ($daybook_expense1 as $newdata) {
                                    if (is_array($newdata)) {
                                        $daybook_expenseamount += $newdata['paid'];
                                        $tblpx = Yii::app()->db->tablePrefix;
                                        $payment = SubcontractorPayment::model()->findByPk($newdata['subcontractor_id']);
                                        $query = "";
                                        $subcontractor_id = '';
                                        if (isset($payment->subcontractor_id)) {
                                            $query = ' AND b.subcontractor_id=' . $payment->subcontractor_id . '';
                                            $subcontractor_id = $payment->subcontractor_id;
                                        }
                                        $sub_total1 = Yii::app()->db->createCommand("select SUM(a.paidamount) as paid_amount,count(a.exp_id) as total_count,b.* from jp_expenses a LEFT JOIN jp_subcontractor_payment b ON a.subcontractor_id=b.payment_id WHERE a.amount !=0 AND a.projectid=" . $project_id . " AND a.type = '73' AND a.exptype IS NULL AND a.vendor_id IS NULL AND a.bill_id IS NULL AND a.date ='" . $newdata['date'] . "' " . $query . " GROUP BY a.date,b.subcontractor_id")->queryRow();
                                        $check_data = $newdata['date'] . '-' . $subcontractor_id;
                                        if (in_array($check_data, $check_array)) {
                                            $flag = 0;
                                            $row_count = "";
                                        } else {
                                            $flag = 1;
                                            $row_count = $sub_total1['total_count'];
                                        }
                                        array_push($check_array, $check_data);
                                        ?>

                                        <tr>
                                            <?php
                                            if ($sc_index == 1) {
                                                ?>
                                                <td rowspan="<?php echo $scpayment_group_count; ?>"
                                                    style="<?php echo $daybook_expense1[0]['expense_head_name'] == 'N/A' ? "background-color: #d0686836;" : "" ?>">
                                                    <?php echo $daybook_expense1[0]['expense_head_name']; ?>
                                                </td>
                                                <?php
                                            }
                                            ?>

                                            <td>
                                                <?php echo date('d-M-Y', strtotime($newdata['date'])); ?>
                                            </td>
                                            <td>
                                                <?php print_r($newdata['description']); ?>
                                            </td>
                                            <td>
                                                <?php
                                                $payment = SubcontractorPayment::model()->findByPk($newdata['subcontractor_id']);
                                                if (isset($payment->subcontractor_id)) {
                                                    $subcontractor_data = Subcontractor::model()->findByPk($payment->subcontractor_id);
                                                    echo $subcontractor_data['subcontractor_name'];
                                                }
                                                ?>
                                            </td>
                                            <td align="right" border="1" class="text-right project-report-width">
                                                <?php echo Controller::money_format_inr($newdata['paidamount'], 2, 1); ?>
                                            </td>
                                            <?php
                                            if ($sc_index == 1) {
                                                ?>
                                                <td rowspan="<?php echo $scpayment_group_count; ?>" class="text-right project-report-width">
                                                    <?php echo Controller::money_format_inr($daybook_expense1['total_amount'], 2, 1); ?>
                                                </td>
                                                <?php
                                            }
                                            ?>
                                        </tr>

                                        <?php
                                        $sc_index++;
                                    }
                                }
                            }
                            $grand += $daybook_expenseamount;
                            echo '<tr bgcolor="#eee"><td align="right" colspan="5"><b>Total</b></td><td align="right" class="text-right project-report-width" ><b>' . Controller::money_format_inr($daybook_expenseamount, 2, 1) . '</b></td></tr>';
                            ?>

                        </table>
                    <?php }
                }
                if (isset($_GET['summary']) && $_GET['summary'] == 0) {
                    ?>
                    <?php echo $br ?>
                    <div class="project-report-block">
                        <table class="table <?php echo $hide ?>">
                            <!--html added here -->
                            <tr class="sticky-1" bgcolor="#eee">
                                <td colspan="4"><b>Labour Details</b>&nbsp;&nbsp;<span>(include with and without quotation from
                                        Daily Labour Report)</span></td>
                            </tr>

                            <tr class="sticky-2">
                                <th>Expense Type</th>
                                <th>Subcontractor</th>
                                <th>Amount</th>
                                <th>Total</th>
                            </tr>
                            <?php
                            $recon_status = (isset($_GET['reconcil_type']) && $_GET['reconcil_type'] == "reconcil") ? '1' : '0';
                            $labour_expenses = Dailyreport::model()->getLabourExpenses($project_id, $recon_status);
                            $labour_total_amount = 0;
                            foreach ($labour_expenses as $labour_expense) {
                                $labour_exp_count = count($labour_expense) - 2;
                                $labour_exp_index = 1;

                                foreach ($labour_expense as $labour_data) {
                                    if (is_array($labour_data)) {
                                        ?>
                                        <tr>
                                            <?php
                                            if ($labour_exp_index == 1) {
                                                ?>
                                                <td rowspan="<?php echo $labour_exp_count; ?>">
                                                    <?php echo $labour_expense['expense_head'] ?>
                                                </td>
                                                <?php
                                            }
                                            ?>

                                            <td>
                                                <?php echo $labour_data['sc_name'] ? $labour_data['sc_name'] : 'No Subcontractor' ?>
                                            </td>
                                            <td class="text-right project-report-width">
                                                <?php echo Controller::money_format_inr($labour_data['amount'], 2) ?>
                                            </td>
                                            <?php
                                            if ($labour_exp_index == 1) {
                                                ?>
                                                <td class="text-right project-report-width" rowspan="<?php echo $labour_exp_count; ?>">
                                                    <?php echo Controller::money_format_inr($labour_expense['total_amount'], 2) ?>
                                                </td>
                                                <?php
                                                $labour_total_amount += $labour_expense['total_amount'];
                                            }
                                            ?>

                                        </tr>
                                        <?php
                                        $labour_exp_index++;
                                    }
                                }
                            }
                            ?>

                            <tr>
                                <th colspan="3" class="text-right">Total</th>
                                <th class="text-right project-report-width">
                                    <?php echo Controller::money_format_inr($labour_total_amount, 2) ?>
                                </th>
                            </tr>
                        </table>
                    </div>
                    <br>
                    <div class="project-report-block">
                        <table class="table <?php echo $hide; ?>">
                            <tr class="sticky-1">
                                <th>
                                    EXPENSE HEAD (Only Purchase Bill and Daybook Purchase considered)
                                </th>
                                <th>Amount</th>
                            </tr>

                            <?php
                            $exp_total_val = 0;
                            foreach ($expense_types_listing as $expense_type) {
                                $exp_total_val += $expense_type['amount'];
                                ?>
                                <tr>
                                    <td>
                                        <?php echo $expense_type['name'] ?>
                                    </td>
                                    <td class="text-right project-report-width">
                                        <?php echo Controller::money_format_inr($expense_type['amount'], 2) ?>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>

                            <tr>
                                <th class="text-right" colspan="1">Total</th>
                                <th class="text-right project-report-width">
                                    <?php echo Controller::money_format_inr($exp_total_val, 2) ?>
                                </th>
                            </tr>
                        </table>
                    </div>
                    <?php echo $br ?>
                    <table class="table <?php echo $hide ?>">
                        <tr>
                            <th>EXPENSE SUMMARY</th>
                            <th>Amount</th>
                        </tr>
                        <tr>
                            <td>Purchase Bill</td>
                            <td class="text-right project-report-width">
                                <?php echo Controller::money_format_inr($bill_total_new_amount, 2) ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Purchase Return</td>
                            <td class="text-right project-report-width">
                                <?php echo Controller::money_format_inr($return_total_amount, 2) ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Daybook Expense</td>
                            <td class="text-right project-report-width">
                                <?php echo Controller::money_format_inr($daybook_exp_final_total, 2) ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Subcontractor Payment</td>
                            <td class="text-right project-report-width">
                                <?php echo Controller::money_format_inr($daybook_expenseamount, 2) ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Despatch Amount</td>
                            <td class="text-right project-report-width">
                                <?php echo Controller::money_format_inr($despatch_amount, 2) ?>
                            </td>
                        </tr>


                        <tr>
                            <?php
                            $final_total = $bill_amount + $return_total_amount + $daybook_exp_total + $daybook_expenseamount + $despatch_amount;
                            ?>
                            <th class="text-right">Total</th>
                            <th class="text-right project-report-width">
                                <?php echo Controller::money_format_inr($final_total, 2) ?>
                            </th>
                        </tr>
                        <?php
                        $where = '';
                        if (!empty($date_from) && !empty($date_to)) {
                            $where .= " AND {$tblpx}expenses.date between '" . $date_from . "' and'" . $date_to . "'";
                        } else {
                            if (!empty($date_from)) {
                                $where .= " AND {$tblpx}expenses.date >= '" . $date_from . "'";
                            }
                            if (!empty($date_to)) {
                                $date_to = date('Y-m-d', strtotime($date_to));
                                $where .= " AND {$tblpx}expenses.date <= '" . $date_to . "'";
                            }
                        }
                        $percentage = Yii::app()->db->createCommand("SELECT percentage
                                     FROM {$tblpx}projects
                                     inner join " . $tblpx . "expenses on " . $tblpx . "expenses.projectid = " . $tblpx . "projects.pid
                                     WHERE pid = {$project_id} " . $where . "")->queryRow();

                        $servise_charge = (($bill_amount + $grand) * $percentage['percentage']) / 100;
                        if ($invoice > ($bill_amount + $grand + $servise_charge)) {
                            $balance = $clienttotal - $invoice;
                        } else {
                            $balance = $clienttotal - ($bill_amount + $grand + $servise_charge);
                        }
                        ?>

                    </table>
                    <?php echo $br ?>
                    <table class="table <?php echo $hide ?>">
                        <!--html added here-->
                        <tr>
                            <td>SALES QUOTATION :</td>
                            <td class="text-right project-report-width">
                                <?php echo Quotation::model()->getTotalQuotationAmount($project_id); ?>
                            </td>
                        </tr>

                        <tr>
                            <td>INVOICE AMOUNT :</td>

                            <td align="right" class="text-right project-report-width">
                                <?php echo Controller::money_format_inr($invoice, 2, 1); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>AMOUNT RECEIVED : </td>
                            <td align="right" class="text-right project-report-width">
                                <?php echo Controller::money_format_inr($clienttotal, 2, 1); ?>
                            </td>
                        </tr>

                        <tr>
                            <td>EXPENSES :</td>

                            <td align="right" class="expense_amount_col text-right project-report-width">
                                <?php echo Controller::money_format_inr(($grand + $bill_amount), 2, 1);
                                unset(Yii::app()->user->expense_sum_bottom);
                                $expense_sum_bottom = Controller::money_format_inr(($grand + $bill_amount), 2, 1);
                                Yii::app()->user->setState("expense_sum_bottom", $expense_sum_bottom);
                                ?>
                            </td>

                        </tr>


                        <tr>

                            <td>SERVICE CHARGE :</td>
                            <td align="right" class="text-right project-report-width">
                                <?php echo Controller::money_format_inr($servise_charge, 2, 1); ?>
                            </td>

                        </tr>

                        <tr>
                            <td>BALANCE AMOUNT : </td>
                            <td align="right" class="balance_bottom text-right project-report-width">
                                <?php echo Controller::money_format_inr(($balance), 2); ?>
                            </td>

                        </tr>

                    </table>
                <?php } ?>
            </div>

        </div>

        <?php
    } else if ((empty($sql) || empty($client) || empty($daybook_expense) || empty($purchase) || empty($expreceipt_return)) && ($project_id != 0)) {

        echo "<p> No results found.</p>";
    }
    ?>

    <?php if ((empty($sql) || empty($client) || empty($daybook_expense) || empty($purchase) || empty($expreceipt_return)) && ($project_id == 0)) { ?>
        <div id="contenthtml" class="contentdiv">
            <div class="clearfix">
                <div class="pull-right btn-container margin-bottom-10">
                    <button type="button" class="btn btn-info">
                        <a href="<?php echo Yii::app()->createUrl('projects/savetopdfreport', array('company_id' => $company_id, 'fromdate' => $fromdate, 'todate' => $todate, 'project_status' => $project_status)) ?>"
                            style="text-decoration: none; color: white;" title="SAVE AS PDF">
                            <i class="fa fa-file-pdf-o button-icon" aria-hidden="true"></i>
                        </a>
                    </button>
                    <button type="button" class="btn btn-info">
                        <a href="<?php echo Yii::app()->createUrl('projects/savetoexcelreport', array('company_id' => $company_id, 'fromdate' => $fromdate, 'todate' => $todate, 'project_status' => $project_status)) ?>"
                            style="text-decoration: none; color: white;" title="SAVE AS EXCEL">
                            <i class="fa fa-file-excel-o button-icon" aria-hidden="true"></i>
                        </a>
                    </button>
                </div>
            </div>
            <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $model->search(),
                'itemView' => '_newexpenselistview',
                'template' => '<div>{summary}</div><div id="parent" class="table-responsive report-table-wrapper"><table class="table total-table" id="fixTable">{items}</table></div>',
            ));
            ?>
        </div>
    <?php } ?>
</div>
<style>
    #parent {
        max-height: 500px;
        border: 1px solid #ddd;
        border-right: 0px;
        border-top: 0px;
    }



    .badge_count {
        position: absolute;
        top: 11rem;
        right: 23rem;
    }
</style>
<script>
    $(document).ready(function () {
        $('.js-example-basic-single').select2({
            width: '100%',
        });
    });

    $(document).ready(function () {
        $('select').first().focus();
    });
    $(document).ready(function () {
        $("#parent").css('max-height', $(window).height() - $("#myNavbar").height() - $(".page_filter").height() - (2 * $('footer').height()));
        // $("#fixTable").tableHeadFixer({
        //     'foot': true,
        //     'head': true
        // });
        $("#date_from").datepicker({
            dateFormat: 'dd-M-yy',
            maxDate: $("#date_to").val()
        });
        $("#date_to").datepicker({
            dateFormat: 'dd-M-yy',
        });
        $("#date_from").change(function () {
            $("#date_to").datepicker('option', 'minDate', $(this).val());
        });
        $("#date_to").change(function () {
            $("#date_from").datepicker('option', 'maxDate', $(this).val());
        });
    });
    //   $(".unreconcil_table").hide();
    $(".unreconciled").click(function () {
        $(this).hide();
        $(this).siblings(".reconciled").show();
        // $(".reconcil_table").hide();
        // $(".unreconcil_table").show();
    });
    $(".reconciled").click(function () {
        $(this).hide();
        $(this).siblings(".unreconciled").show();
        // $(".unreconcil_table").hide();
        // $(".reconcil_table").show();
    });
    $(function () {
        $("input#summary").click(function () {
            $("form#projectform").submit();
        })
        $(".hide").remove();
    })

    function getBillItems(element) {
        let bill_id = element.getAttribute('data-bill-id');
        console.log(bill_id);
        $.ajax({
            url: "<?php echo Yii::app()->createAbsoluteUrl("bills/getItemsBasedonBill"); ?>",
            data: { bill_id: bill_id },
            method: "GET",
            success: function (response) {
                var tooltipText = '';
                var specNames = JSON.parse(response);
                if (Array.isArray(specNames) && specNames.length == 0) {
                    tooltipText = "No Item";
                } else {
                    //alert(response);

                    for (var i = 0; i < specNames.length; i++) {
                        if (specNames[i] != '') {
                            tooltipText += (i + 1) + ')' + specNames[i] + '\n';

                        }

                    }
                }

                //var tooltipText = specNames;
                console.log(tooltipText);
                $(element).attr('title', tooltipText).tooltip();
                $(element).attr('title', tooltipText).removeAttr('data-original-title').tooltip();
            }
        });
    }
</script>