<?php
$user = Users::model()->findByPk(Yii::app()->user->id);
$arrVal = explode(',', $user->company_id);
$query_company = "";
$query_company1 = "";
foreach ($arrVal as $arr) {
    if ($query_company)
        $query_company .= ' OR';
    if ($query_company1)
        $query_company1 .= ' OR';
    $query_company .= " FIND_IN_SET('" . $arr . "', id)";
    $query_company1 .= " FIND_IN_SET('" . $arr . "', company_id)";
}

?>
<div class="page_filter clearfix custom-form-style">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'search-form',
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>
    <div class="row">
        <div class="form-group col-xs-12 col-sm-3 col-md-2">
            <label for="Project">Project</label>
            <?php
            if (Yii::app()->user->role == 1 || Yii::app()->user->role == 2) {
                echo $form->dropDownList($model, 'projectid', CHtml::listData(Projects::model()->findAll(array(
                    'select' => array('pid, name'),
                    "condition" => '(' . $query_company1 . ')',
                    'order' => 'name',
                    'distinct' => true
                )), 'pid', 'name'), array('empty' => 'Select Project', 'id' => 'project_id', 'class' => 'form-control select_box'));
            } else {
                echo $form->dropDownList($model, 'projectid', CHtml::listData(Projects::model()->findAll(array(
                    'select' => array('pid, name'),
                    'order' => 'name',
                    "condition" => '(' . $query_company1 . ')',
                    "condition" => 'pid in (select projectid from jp_project_assign where userid=' . Yii::app()->user->id . ')',
                    'distinct' => true
                )), 'pid', 'name'), array('empty' => 'select Project', 'id' => 'project_id', 'class' => 'form-control select_box'));
            }
            ?>
        </div>

        <div class="form-group col-xs-12 col-sm-3 col-md-2">
            <label for="Type">Type</label>
            <?php
            echo $form->dropDownList($model, 'type', CHtml::listData(Status::model()->findAll(array(
                'select' => array('sid,caption'),
                'condition' => 'status_type="expense"',
                'order' => 'caption',
                'distinct' => true
            )), 'sid', 'caption'), array('empty' => 'Choose a type', 'class' => 'form-control select_box'));
            ?>
        </div>

        <div class="form-group col-xs-12 col-sm-3 col-md-2">
            <label for="company">Company</label>
            <?php
            echo CHtml::dropDownList('company_id', 'company_id', CHtml::listData(Company::model()->findAll(array(
                'select' => array('id, name'),
                'order' => 'id DESC',
                'condition' => '(' . $query_company . ')',
                'distinct' => true
            )), 'id', 'name'), array('empty' => 'Select Company', 'id' => 'company_id', 'class' => 'form-control select_box', 'options' => array($company_id => array('selected' => true))));
            ?>
        </div>

        <div class="form-group col-xs-12 col-sm-3 col-md-2">
            <label for="vendor">Vendor</label>
            <?php
            echo $form->dropDownList($model, 'vendor_id', CHtml::listData(Vendors::model()->findAll(array(
                'select' => array('vendor_id,name'),
                "condition" => '(' . $query_company1 . ')',
                'order' => 'name',
                'distinct' => true
            )), 'vendor_id', 'name'), array('empty' => 'Choose a vendor', 'class' => 'form-control select_box'));
            ?>
        </div>

        <div class="form-group col-xs-12 col-sm-3 col-md-2">
            <label for="purchase_type">Purchase Type</label>
            <select class="form-control select_box" name="Expenses[purchase_type]" id="Expenses_purchase_type">
                <option value="">Choose purchase type</option>
                <option value="1" <?php if ($model->purchase_type == 1) {
                    echo "selected";
                } ?>>Credit</option>
                <option value="2" <?php if ($model->purchase_type == 2) {
                    echo "selected";
                } ?>>Full Paid</option>
                <option value="3" <?php if ($model->purchase_type == 3) {
                    echo "selected";
                } ?>>Partially Paid</option>
            </select>
        </div>

        <div class="form-group col-xs-12 col-sm-3 col-md-2">
            <label for="transaction">Transaction Type</label>
            <?php
            echo $form->dropDownList($model, 'expense_type', CHtml::listData(Status::model()->findAll(array(
                'select' => array('sid,caption'),
                'condition' => 'status_type="payment_type"',
                'order' => 'caption',
                'distinct' => true
            )), 'sid', 'caption'), array('empty' => 'Choose a transaction type', 'class' => 'form-control select_box'));
            ?>
        </div>

        <div class="form-group col-xs-12 col-sm-3 col-md-2">
            <label for="sub">Subcontractor</label>
            <?php
            echo $form->dropDownList($model, 'subcontractor_id', CHtml::listData(Subcontractor::model()->findAll(array(
                'select' => array('subcontractor_id,subcontractor_name'),
                "condition" => '(' . $query_company1 . ')',
                'order' => 'subcontractor_name',
                'distinct' => true
            )), 'subcontractor_id', 'subcontractor_name'), array('empty' => 'Choose a subcontractor', 'class' => 'form-control select_box'));
            ?>
        </div>

        <?php
        $tblpx = Yii::app()->db->tablePrefix;
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $arrVal = explode(',', $user->company_id);
        $newQuery = "";
        $newQuery1 = "";
        $newQuery2 = "";
        $newQuery4 = "";
        foreach ($arrVal as $arr) {
            if ($newQuery)
                $newQuery .= ' OR';
            if ($newQuery1)
                $newQuery1 .= ' OR';
            if ($newQuery2)
                $newQuery2 .= ' OR';
            if ($newQuery4)
                $newQuery4 .= ' OR';
            $newQuery .= " FIND_IN_SET('" . $arr . "', company_id)";
            $newQuery1 .= " FIND_IN_SET('" . $arr . "', id)";
            $newQuery2 .= " FIND_IN_SET('" . $arr . "', b.company_id)";
            $newQuery4 .= " FIND_IN_SET('" . $arr . "', " . $tblpx . "purchase_return.company_id)";
        }
        $billsData = Yii::app()->db->createCommand("SELECT b.bill_id as billid, b.bill_number as
                billnumber FROM " . $tblpx . "bills b  Where b.bill_totalamount IS NOT NULL  AND b.bill_id  IN (Select bill_id
                                       from " . $tblpx . "expenses WHERE bill_id IS NOT NULL
                                       ) GROUP BY b.bill_id  ORDER BY b.bill_id DESC")->queryAll();


        $purchasereturnData = Yii::app()->db->createCommand("SELECT p.return_id as returnid, p.return_number as
        returnnumber FROM " . $tblpx . "purchase_return p  Where    p.return_id  IN (Select return_id
                               from " . $tblpx . "expenses WHERE return_id IS NOT NULL
                               ) GROUP BY p.return_id  ORDER BY p.return_id DESC")->queryAll();
        ?>
        <div class="form-group col-xs-12 col-sm-3 col-md-2">
            <label for="bill">Bill Number/ Return No</label>
            <select class="form-control js-example-basic-single select_box" name="Expenses[bill_retunid]"
                id="Expenses_bill_return_id" style="width:100%">
                <option value="">-Bill No/ Return No-</option>
                <?php
                if (!empty($billsData)) {
                    ?>
                    <optgroup label='Bill Number' id="bill">
                        <?php foreach ($billsData as $key => $value) {
                            $selected = "";
                            if (isset($_REQUEST['Expenses']['bill_retunid']) && $_REQUEST['Expenses']['bill_retunid'] != "") {
                                $bill = explode(',', $_REQUEST['Expenses']['bill_retunid']);
                                if ($bill[1] == $value['billid']) {
                                    $selected = "selected";
                                }
                            }
                            ?>
                            <option value="1,<?php echo $value['billid']; ?>" <?php echo $selected ?>>
                                <?php echo $value['billnumber']; ?>
                            </option>
                        <?php }
                        ?>
                    </optgroup>
                    <?php
                }
                ?>
                <?php if (!empty($purchasereturnData)) {
                    ?>
                    <optgroup label='Return Number' id="return">
                        <?php foreach ($purchasereturnData as $iData) {
                            $selected = "";
                            if (isset($_REQUEST['Expenses']['bill_retunid']) && $_REQUEST['Expenses']['bill_retunid'] != "") {
                                $bill = explode(',', $_REQUEST['Expenses']['bill_retunid']);
                                if ($bill[1] == $iData["returnid"]) {
                                    $selected = "selected";
                                }
                            }
                            ?>
                            <option value="2,<?php echo $iData["returnid"]; ?>" <?php echo $selected ?>>
                                <?php echo $iData["returnnumber"]; ?>
                            </option>
                        <?php } ?>
                    </optgroup>
                <?php } ?>
            </select>
        </div>
        <div class="form-group col-xs-12 col-sm-3 col-md-2">
            <label for="from">From</label>
            <?php echo CHtml::activeTextField($model, 'fromdate', array('placeholder' => 'From Date', 'size' => 10, 'class' => 'form-control', 'value' => isset($fromdate) ? $fromdate : '')); ?>
        </div>

        <div class="form-group col-xs-12 col-sm-3 col-md-2">
            <label for="to">To</label>
            <?php echo CHtml::activeTextField($model, 'todate', array('placeholder' => 'Date To', 'size' => 10, 'class' => 'form-control', 'value' => isset($todate) ? $todate : '')); ?>
        </div>

        <div class="form-group col-xs-12 col-sm-3 col-md-2 text-sm-left text-right">
            <label class="d-sm-block d-none">&nbsp;</label>
            <div>
                <?php echo CHtml::submitButton('Go', array('class' => 'btn btn-sm btn-primary')); ?>
                <?php echo CHtml::resetButton('Clear', array('class' => 'btn btn-sm btn-default', 'onclick' => 'javascript:location.href="' . $this->createUrl('newlist') . '"')); ?>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script>
    $(document).ready(function () {
        $(".select_box").select2();
        $("#Expenses_fromdate").datepicker({
            dateFormat: 'dd-M-yy',
            maxDate: $("#Expenses_todate").val()
        });
        $("#Expenses_todate").datepicker({
            dateFormat: 'dd-M-yy',
        });
        $("#Expenses_fromdate").change(function () {
            $("#Expenses_todate").datepicker('option', 'minDate', $(this).val());
        });
        $("#Expenses_todate").change(function () {
            $("#Expenses_fromdate").datepicker('option', 'maxDate', $(this).val());
        });
    });
</script>