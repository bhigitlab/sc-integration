<style type="text/css">
    tr.pitems td input,
    tr.pitems td div.pricediv {
        text-align: right !important;
    }

    .table {
        margin-top: 10px;
        border-collapse: separate;
        border-top: 1px solid #ddd;
    }
</style>
<div class="stsmsg alert alert-success" style="display: none;"></div>
<div class="row">
    <div class="col-sm-12 table-responsive">
        <table class="table">
            <thead class="thead-inverse">
                <tr>
                    <th>Specifications/Remark</th>
                    <th>HSN Code</th>
                    <th>Batch</th>
                    <th>Actual Quantity</th>
                    <th>Billed Quantity</th>
                    <th>Remaining Quantity</th>
                    <?php if ($purchase_type  == 'A') { ?>
                        <th>Length</th>
                    <?php } ?>
                    <?php if ($purchase_type  == 'G') { ?>
                        <th>Width</th>
                        <th>Height</th>
                    <?php } ?>
                    <th>Quantity</th>
                    <th>Rate</th>
                    <th>Unit</th>
                    <th>Base Quantity</th>
                    <th>Base Unit</th>
                    <th>Base Rate</th>
                    <th>Amount</th>
                    <th>Tax Slab (%)</th>
                    <th>Discount Amount</th>
                    <th>Discount (%)</th>
                    <th>CGST (%)</th>
                    <th>CGST</th>
                    <th>SGST (%)</th>
                    <th>SGST</th>
                    <th>IGST (%)</th>
                    <th>IGST</th>
                    <th>Total Tax</th>
                    <th>Tax (%)</th>
                    <th>Total Amount</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $number    = 1;
                $amount    = 0;
                $totalQty  = 0;
                $bTotalQty = 0;
                $billed_quantity = 0;

                foreach ($purchase as $item) {
                    if (!empty($item['bill_id'])) {

                        if ($item['pcategory']) {
                            $categorymodel = $this->GetItemCategoryById($parent = 0, $spacing = '', $user_tree_array = '', $item['categoryid']);
                            if ($categorymodel) {
                                $categoryName = $categorymodel["data"];
                                $baseunit = $categorymodel["base_unit"];
                            } else {
                                $categoryName = "Not available";
                                $baseunit = $item['base_unit'];
                            }
                        } else {
                            $categoryName  = $item['bremark'] ? $item['bremark'] : $item['premark'];
                            $baseunit = $item['base_unit'];
                        }
                        $purchasemodel = PurchaseItems::model()->findByPk($item['item_id']);
                        $itembaseqty = $purchasemodel->base_qty;
                        $itembaserate = $purchasemodel->base_rate;
                        $itembaseunit = $purchasemodel->base_unit;
                        $itemunit = $purchasemodel->unit;


                        if ($itembaseqty != '' && $itembaserate != '' && $itembaseunit != '') {
                            if ($itembaseunit != $itemunit) {
                                $itemQuantity = $itembaseqty;
                            } else {
                                $itemQuantity = $itembaseqty;
                            }
                        } else {
                            $itemQuantity = $purchasemodel->quantity;
                        }


                        if (!empty($bills)) {
                            $bill = isset($bills[$item['item_id']]) ? $bills[$item['item_id']] : 0;
                        } else {
                            $bill = 0;
                        }

                        if (!empty($bill_datas)) {
                            $bill_data = isset($bill_datas[$item['item_id']]) ? $bill_datas[$item['item_id']] : 0;
                        } else {
                            $bill_data = 0;
                        }

                        $billsum1 = $bill + $bill_data;
                        $billsum =
                            $bItemQuantity = $billsum1 ? $billsum1 : 0;

                        if (strpos($bItemQuantity, ".") !== false) {
                            $bItemQuantity = number_format((float) $bItemQuantity, 4, '.', '');
                        } else {
                            $bItemQuantity = $bItemQuantity;
                        }
                        $quantityAvail = $itemQuantity - $bItemQuantity;
                        if ($quantityAvail < 0) {
                            $quantityAvail = 0;
                        }
                        $totalQty = $totalQty + $itemQuantity;
                        $bTotalQty = $bTotalQty + $bItemQuantity;
                        $billed_quantity += $bItemQuantity;
                ?>
                        <tr class="pitems">

                            <?php echo CHtml::hiddenField('ids[]', $item["item_id"], array('id' => "ids" . ($number - 1), 'class' => "txtBox pastweek ids")); ?>


                            <?php echo CHtml::hiddenField('category[]', $item["pcategory"] ? $item["pcategory"] : '', array('id' => "category" . ($number - 1), 'class' => "txtBox pastweek category")); ?>

                            <?php $totalAmount = ($item["billitem_amount"] ? $item["billitem_amount"] : 0) - ($item["billitem_discountamount"] ? $item["billitem_discountamount"] : 0) + ($item["billitem_taxamount"] ? $item["billitem_taxamount"] : 0); ?>

                            <?php echo CHtml::hiddenField('biremquantity[]', $quantityAvail, array('id' => "biremquantity" . ($number - 1))); ?>

                            <td id="<?php echo $number - 1; ?>">
                                <div id="bicategoryname<?php echo $number - 1; ?>"><?php echo $categoryName; ?></div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="hsn_code<?php echo $number - 1; ?>"><?php echo $item['hsn_code']; ?></div>

                                <?php echo CHtml::hiddenField('hsn_code[]', $item['hsn_code']); ?>
                            </td>

                            <td id="<?php echo $number - 1; ?>">

                                <input type="text" id="bibatch<?php echo $number - 1; ?>" class="form-control bibatch" value="<?php echo $item['batch']; ?>" name="bibatch[]" />
                            </td>

                            <?php echo CHtml::hiddenField('availablequantity[]', $quantityAvail, array('id' => "availablequantity" . ($number - 1))); ?>

                            <?php echo CHtml::hiddenField('orrate[]', $item["billitem_rate"] ? number_format((float) $item["billitem_rate"], 2, '.', '') : number_format((float) $item["rate"], 2, '.', ''), array('id' => "orrate" . ($number - 1))); ?>

                            <td id="<?php echo $number - 1; ?>">
                                <div id="actualquantity<?php echo $number - 1; ?>"><?php echo $itemQuantity; ?></div>
                            </td>

                            <?php
                            if ($item['rate_approve'] == 1) {
                                $rate_title = 'Order Rate: ' . $item["rate"];
                            }
                            if ($item['approve_status'] == 1) {
                                $qty_title = 'Order Quantity: ' . $itemQuantity;
                            }
                            ?>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="billedquantity<?php echo $number - 1; ?>"><?php echo $bItemQuantity; ?></div>
                            </td>

                            <td id="<?php echo $number - 1; ?>">
                                <div id="availablequantity<?php echo $number - 1; ?>"><?php echo $quantityAvail; ?></div>
                            </td>

                            <?php if ($purchase_type  == 'A') { ?>
                                <td id="<?php echo $number - 1; ?>">
                                    <div id="bilength<?php echo $number - 1; ?>"><?php echo $item["item_length"]; ?></div>
                                </td>
                            <?php } ?>
                            <?php if ($purchase_type  == 'G') { ?>
                                <td id="<?php echo $number - 1; ?>">
                                    <div id="biwidth<?php echo $number - 1; ?>"><?php echo $item["item_width"]; ?></div>
                                </td>
                                <td id="<?php echo $number - 1; ?>">
                                    <div id="biheight<?php echo $number - 1; ?>"><?php echo $item["item_height"]; ?></div>
                                </td>
                            <?php } ?>


                            <td id="<?php echo $number - 1; ?>">

                                <?php echo CHtml::textField('biquantity[]', $item["billitem_quantity"] ? $item["billitem_quantity"] : 0, array(
                                    'value' => $item["billitem_quantity"] ? $item["billitem_quantity"] : 0, 'class' => 'form-control fnext biquantity validate[required,custom[number],min[0]]', 'id' => "biquantity" . ($number - 1), 'style' => (($item['approve_status'] == 1)) ? "background-color:red;color:white;" : "", 'title' => $item['approve_status'] == 1 ? $qty_title : ''

                                )); ?>
                            </td>

                            <td id="<?php echo $number - 1; ?>">


                                <?php echo CHtml::textField('birate[]', $item["billitem_rate"] ? number_format((float) $item["billitem_rate"], 2, '.', '') : number_format((float) $item["rate"], 2, '.', ''), array(
                                    'value' => $item["billitem_rate"] ? number_format((float) $item["billitem_rate"], 2, '.', '') : number_format((float) $item["rate"], 2, '.', ''), 'class' => 'form-control  birate validate[required,custom[number],min[0]]', 'id' => "birate" . ($number - 1), 'style' => (($item['rate_approve'] == 1)) ? "background-color:red;color:white;" : "", 'title' => $item['rate_approve'] == 1 ? $rate_title : ''
                                )); ?>


                            </td>

                            <td id="<?php echo $number - 1; ?>">
                                <div id="vendorunit<?php echo $number - 1; ?>" style="min-width:80px;">
                                    <?php
                                    $val = $number - 1;
                                    $purchase_unitvendor = $item["billitem_unit"] ? $item["billitem_unit"] : $item["unit"];
                                    if ($item["category_id"] != '') {
                                        $item_cat_id = $item["category_id"];
                                    } else {
                                        $item_cat_id = $item["pcategory"];
                                    }
                                    $options = CHtml::listData(UnitConversion::model()->findAll(array(
                                        'select' => array('conversion_unit'),
                                        'condition' => 'item_id = ' . $item_cat_id . '',
                                        'order' => 'conversion_unit'
                                    )), 'conversion_unit', 'conversion_unit');

                                    if (empty($options)) {
                                        $options[$purchase_unitvendor] = $purchase_unitvendor;
                                    }

                                    echo CHtml::dropDownList('purchase_unitvendor ', $purchase_unitvendor, $options, array('empty' => 'Select Unit', 'class' => 'inputs target txtBox   form-control purchase_unitvendor', 'id' => 'purchase_unitvendor' . $val));
                                    ?>

                                </div>
                            </td>

                            <td id="<?php echo $number - 1; ?>">

                                <?php echo CHtml::textField('basequantity[]', $item["purchaseitem_quantity"] ? $item["purchaseitem_quantity"] : 0, array(
                                    'value' => $item["purchaseitem_quantity"] ? $item["purchaseitem_quantity"] : 0, 'class' => 'form-control  fnext basequantity validate[required,custom[number],min[0]]', 'id' => "basequantity" . ($number - 1),

                                )); ?>
                            </td>

                            <td id="<?php echo $number - 1; ?>">
                                <div id="biunit<?php echo $number - 1; ?>"><?php echo $item["purchaseitem_unit"] ? $item["purchaseitem_unit"] : $baseunit; ?></div>

                                <?php echo CHtml::hiddenField('biunit[]', $item["purchaseitem_unit"] ? $item["purchaseitem_unit"] : $baseunit); ?>
                            </td>

                            <td id="<?php echo $number - 1; ?>" style="min-width:80px;">
                                <div id="baserate<?php echo $number - 1; ?>"><?php echo $item["purchaseitem_rate"] ? $item["purchaseitem_rate"] : 0; ?></div>
                            </td>

                            <td id="<?php echo $number - 1; ?>">
                                <div id="biamount<?php echo $number - 1; ?>" class="pricediv"><?php echo $item["billitem_amount"] ? number_format((float) $item["billitem_amount"], 2, '.', '') : 0; ?></div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <?php
                                $datas = TaxSlabs::model()->getAllDatas();
                                ?>
                                <select class="txtBox tax_slab" style='width:75px;' id="tax_slab<?php echo $number - 1; ?>" name="tax_slab[]">
                                    <option value="">Select one</option>
                                    <?php
                                    foreach ($datas as  $value) {
                                        if ($item["billitem_taxslab"] == $value['tax_slab_value']) {
                                            echo ' <option data-id="' . $value['id'] . '" value="' . $value['tax_slab_value']  . '" selected>' . $value['tax_slab_value'] . '%</option>';
                                        } else {
                                            echo ' <option  data-id="' . $value['id'] . '" value="' . $value['tax_slab_value'] . '">' . $value['tax_slab_value'] . '%</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </td>
                            <td id="<?php echo $number - 1; ?>">


                                <?php echo CHtml::textField('damount[]', $item["billitem_discountamount"] ? number_format((float) $item["billitem_discountamount"], 2, '.', '') : number_format((float) $item["discount_amount"], 2, '.', ''), array(
                                    'value' => $item["billitem_discountamount"] ? number_format((float) $item["billitem_discountamount"], 2, '.', '') : number_format((float) $item["discount_amount"], 2, '.', ''), 'class' => 'form-control validate[required,custom[number],min[0]]', 'id' => "damount" . ($number - 1),

                                )); ?>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="dpercent<?php echo $number - 1; ?>" class="pricediv"><?php echo $item["billitem_discountpercent"] ? $item["billitem_discountpercent"] : $item["discount_percentage"]; ?></div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">

                                <?php echo CHtml::textField('cgstpercent[]', $item["billitem_cgstpercent"] ? $item["billitem_cgstpercent"] : $item["cgst_percentage"], array(
                                    'value' => $item["billitem_cgstpercent"] ? $item["billitem_cgstpercent"] : $item["cgst_percentage"], 'class' => 'cgstp form-control validate[required,custom[number],min[0]]', 'id' => "cgstpercent" . ($number - 1),

                                )); ?>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="cgst<?php echo $number - 1; ?>" class="pricediv"><?php echo $item["billitem_cgst"] ? number_format((float) $item["billitem_cgst"], 2, '.', '') : $item['cgst_amount']; ?></div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <?php echo CHtml::textField('sgstpercent[]', $item["billitem_sgstpercent"] ? $item["billitem_sgstpercent"] :  $item["sgst_percentage"], array(
                                    'value' => $item["billitem_sgstpercent"] ? $item["billitem_sgstpercent"] :  $item["sgst_percentage"], 'class' => 'sgstp form-control validate[required,custom[number],min[0]]', 'id' => "sgstpercent" . ($number - 1),

                                )); ?>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="sgst<?php echo $number - 1; ?>" class="pricediv"><?php echo $item["billitem_sgst"] ? number_format((float) $item["billitem_sgst"], 2, '.', '') : number_format((float) $item["sgst_amount"], 2, '.', ''); ?></div>
                            </td>

                            <td id="<?php echo $number - 1; ?>">

                                <?php echo CHtml::textField('igstpercent[]', $item["billitem_igstpercent"] ? $item["billitem_igstpercent"] : $item["igst_percentage"], array(
                                    'value' => $item["billitem_igstpercent"] ? $item["billitem_igstpercent"] : $item["igst_percentage"], 'class' => 'form-control lnext validate[custom[number],min[0]],min[0]]', 'id' => "igstpercent" . ($number - 1)

                                )); ?>
                            </td>

                            <td id="<?php echo $number - 1; ?>">
                                <div id="igst<?php echo $number - 1; ?>" class="pricediv"><?php echo $item["billitem_igst"] ? number_format((float) $item["billitem_igst"], 2, '.', '') : number_format((float) $item["igst_amount"], 2, '.', ''); ?></div>
                            </td>

                            <td id="<?php echo $number - 1; ?>">
                                <div id="taxamount<?php echo $number - 1; ?>" class="pricediv"><?php echo $item["billitem_taxamount"] ? number_format((float) $item["billitem_taxamount"], 2, '.', '') :  number_format((float) $item["tax_amount"], 2, '.', ''); ?></div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="taxpercent<?php echo $number - 1; ?>" class="pricediv"><?php echo $item["billitem_taxpercent"] ? $item["billitem_taxpercent"] : $item["tax_perc"]; ?></div>
                            </td>
                            <td id="<?php echo $number - 1; ?>">
                                <div id="totalamount<?php echo $number - 1; ?>" class="pricediv"><?php echo number_format((float) $totalAmount, 2, '.', ''); ?></div>
                            </td>


                            <?php echo CHtml::hiddenField('billitem[]', $item["billitem_id"], array('id' => "billitem" . ($number - 1))); ?>

                            <?php echo CHtml::hiddenField('savedquantity[]', $item["billitem_quantity"] ? $item["billitem_quantity"] : 0, array('id' => "savedquantity" . ($number - 1))); ?>

                            <td data-attr="<?php echo $item['approve_status'] ?>">
                                <div id="tickmark<?php echo $number - 1; ?>"><?php if ($item["billitem_quantity"] != 0) { ?><i class="fa fa-check" aria-hidden="true"></i><?php } ?></div>
                                <?php
                                if (in_array('/bills/approvebills', Yii::app()->user->menuauthlist) && (($item['approve_status'] == 1 && $item['approve_status'] != NULL) || $item['rate_approve'] == 1)) {
                                ?>
                                    <div data-id='<?= $item["billitem_id"] ?>' id="tickmark<?php echo $number - 1; ?>" class="icon icon-action-redo approve" title="Approve" onclick="approveQuantity(this);"></div>
                                <?php
                                }
                                ?>

                                <?php echo CHtml::hiddenField('save_status[]', '', array('id' => "save_status" . ($number - 1))); ?>



                            </td>
                        </tr>
                <?php
                        $amount = $amount + $item["amount"];
                        $number = $number + 1;
                    }
                }
                ?>

                <?php echo CHtml::hiddenField('tqty[]', $totalQty, array('id' => "tqty")); ?>
                <?php echo CHtml::hiddenField('btqty[]', $bTotalQty, array('id' => "btqty")); ?>
                <?php echo CHtml::hiddenField('totrows', $number - 1, array('id' => "totrows", 'class' => "txtBox pastweek totrows")); ?>
                <?php echo CHtml::hiddenField('purchase_type', $purchase_type, array('id' => 'purchase_type')); ?>
            </tbody>
        </table>
    </div>
</div>