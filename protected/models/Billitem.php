<?php

/**
 * This is the model class for table "{{billitem}}".
 *
 * The followings are the available columns in table '{{billitem}}':
 * @property integer $billitem_id
 * @property integer $bill_id
 * @property integer $purchaseitem_id
 * @property string $billitem_description
 * @property double $billitem_quantity
 * @property string $billitem_unit
 * @property double $billitem_rate
 * @property double $billitem_amount
 * @property double $billitem_taxpercent
 * @property integer $category_id
 * @property string $remark
 * @property double $billitem_taxamount
 * @property double $billitem_discountpercent
 * @property double $billitem_cgst
 * @property double $billitem_cgstpercent
 * @property double $billitem_sgst
 * @property double $billitem_sgstpercent
 * @property double $billitem_igst
 * @property double $billitem_igstpercent
 * @property double $billitem_discountamount
 * @property string $created_date
 */
class Billitem extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{billitem}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bill_id, billitem_quantity, billitem_unit, billitem_rate, billitem_amount, billitem_taxpercent, billitem_taxamount, billitem_discountpercent, billitem_discountamount, created_date', 'required'),
			array('bill_id, purchaseitem_id, category_id', 'numerical', 'integerOnly' => true),
			array('billitem_quantity, billitem_rate, billitem_amount, billitem_taxpercent, billitem_taxamount, billitem_discountpercent, billitem_cgst, billitem_cgstpercent, billitem_sgst, billitem_sgstpercent, billitem_igst, billitem_igstpercent, billitem_discountamount, billitem_taxslab', 'numerical'),
			array('billitem_unit', 'length', 'max' => 20),
			array('remark', 'length', 'max' => 100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('billitem_id, bill_id, purchaseitem_id, billitem_description, billitem_quantity, billitem_unit, billitem_rate, billitem_amount, billitem_taxpercent, category_id, remark, billitem_taxamount, billitem_discountpercent, billitem_cgst, billitem_cgstpercent, billitem_sgst, billitem_sgstpercent, billitem_igst, billitem_igstpercent, billitem_discountamount, created_date, billitem_taxslab,billitem_hsn_code', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'billitem_id' => 'Billitem',
			'bill_id' => 'Bill',
			'purchaseitem_id' => 'Purchaseitem',
			'billitem_description' => 'Billitem Description',
			'billitem_quantity' => 'Billitem Quantity',
			'billitem_unit' => 'Billitem Unit',
			'billitem_rate' => 'Billitem Rate',
			'billitem_amount' => 'Billitem Amount',
			'billitem_taxpercent' => 'Billitem Taxpercent',
			'category_id' => 'Category',
			'remark' => 'Remark',
			'billitem_taxamount' => 'Billitem Taxamount',
			'billitem_discountpercent' => 'Billitem Discountpercent',
			'billitem_cgst' => 'Billitem Cgst',
			'billitem_cgstpercent' => 'Billitem Cgstpercent',
			'billitem_sgst' => 'Billitem Sgst',
			'billitem_sgstpercent' => 'Billitem Sgstpercent',
			'billitem_igst' => 'Billitem Igst',
			'billitem_igstpercent' => 'Billitem Igstpercent',
			'billitem_discountamount' => 'Billitem Discountamount',
			'created_date' => 'Created Date',
			'billitem_taxslab' => 'Tax Slab',
			'billitem_hsn_code' => 'HSN Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('billitem_id', $this->billitem_id);
		$criteria->compare('bill_id', $this->bill_id);
		$criteria->compare('purchaseitem_id', $this->purchaseitem_id);
		$criteria->compare('billitem_description', $this->billitem_description, true);
		$criteria->compare('billitem_quantity', $this->billitem_quantity);
		$criteria->compare('billitem_unit', $this->billitem_unit, true);
		$criteria->compare('billitem_rate', $this->billitem_rate);
		$criteria->compare('billitem_amount', $this->billitem_amount);
		$criteria->compare('billitem_taxpercent', $this->billitem_taxpercent);
		$criteria->compare('category_id', $this->category_id);
		$criteria->compare('remark', $this->remark, true);
		$criteria->compare('billitem_taxamount', $this->billitem_taxamount);
		$criteria->compare('billitem_discountpercent', $this->billitem_discountpercent);
		$criteria->compare('billitem_cgst', $this->billitem_cgst);
		$criteria->compare('billitem_cgstpercent', $this->billitem_cgstpercent);
		$criteria->compare('billitem_sgst', $this->billitem_sgst);
		$criteria->compare('billitem_sgstpercent', $this->billitem_sgstpercent);
		$criteria->compare('billitem_igst', $this->billitem_igst);
		$criteria->compare('billitem_igstpercent', $this->billitem_igstpercent);
		$criteria->compare('billitem_discountamount', $this->billitem_discountamount);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('billitem_taxslab', $this->billitem_taxslab, true);
		$criteria->compare('billitem_hsn_code', $this->billitem_hsn_code, true);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Billitem the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
