<?php
/* @var $this BillsController */
/* @var $model Bills */

$this->breadcrumbs = array(
    'Bills' => array('index'),
    $model->return_id => array('view', 'id' => $model->return_id),
    'Update',
);
$this->menu = array(
);
?>

<div class="container" id="expense">
    <div class="loading-overlay">
        <span class="fa fa-spinner fa-3x fa-spin"></span>
    </div>
    <div class="header-container">
        <h3>Update Defect Return</h3>
        <a href="<?php echo Yii::app()->createAbsoluteUrl('wh/warehouse/defectreturn'); ?>" class="btn btn-info">
            List Defect Return</a>
    </div>
    <div id="loading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif"></div>
    <div>
        <div>
            <?php $this->renderPartial('_updatereturnform', array('model' => $model, 'client' => $client)); ?>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#loading').hide();
    });
</script>