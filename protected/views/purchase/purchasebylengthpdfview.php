<link rel="stylesheet" type="text/css" href="uploads/new_template/css/newstyle.css" />
<style>
.lefttdiv {
    float: left;
}
.purchase-title {
    border-bottom:1px solid #ddd;
}
.purchase_items h3 {
    font-size:18px;
    color:#333;
    margin:0px;
    padding:0px;
    text-align:left;
    padding-bottom:10px;
}
.purchase_items {
    padding:15px;
    box-shadow:0 0 13px 1px rgba(0,0,0,0.25);
}
.purchaseitem {
    display: inline-block;
    margin-right: 20px;
}
.purchaseitem last-child { margin-right: 0px; }
.purchaseitem labe l {
    font-size:12px;
}
.remark { display:none; }
.padding-box { padding: 3px 0px; min-height: 17px; display: inline-block; }
th { height:auto; }
.quantity, .rate { max-width:80px; }
.text_align {
    text-align: center;
}
*:focus{
    border:1px solid #333;
    box-shadow:0 0 6px 1px rgba(0,0,0,0.25);
}
.text-right{
    text-align: right;
}
</style>
<div class="container">
    <header class="headerinv">
	<table class="invoiceheader">
            <tr>
                <td>
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" style="height:100px;padding:5px;">
                </td>
		<td class="details" style="width:300px;"><b></b></td>
            </tr>
	</table>
	<br>
    <header>
    <h2>Purchase Orders</h2>
    <br/>
    <table border="1">
        <tr>
            <td colspan="2">Category</td>
            <td colspan="3"></td>
            <td colspan="3" class="text-right">Date : <?php echo date("d/m/Y", strtotime($model->purchase_date)); ?></td>
        </tr>
        <tr>
            <td colspan="2">Project</td>
            <td colspan="3"><?php echo $project; ?></td>
            <td colspan="3">Lot.No. 1st</td>
        </tr>
        <tr>
            <td colspan="2">Client:</td>
            <td colspan="3"></td>
            <td colspan="3">Rev.No:</td>
        </tr>
        <tr>
            <td colspan="2">Manufacturer / Supplier:</td>
            <td colspan="3"></td>
            <td colspan="3">PI No.: <?php echo $model->purchase_no; ?></td>
        </tr>
        <tr>
            <th class="text-center">Sl. No.</th>
            <th class="text-center">Profile/Die No.</th>
            <th class="text-center">Length</th>
            <th class="text-center">Qty</th>
            <th class="text-center">Finish</th>
            <th class="text-center">Rate</th>
            <th class="text-center">Amount</th>
            <th class="text-center">Remarks</th>
        </tr>
        <?php 
        if(count($itemmodel) > 0) {
            $i  = 1;
            $tot_qty =0;
            foreach($itemmodel as $item) {
                $tot_qty +=$item['quantity'];
        ?>
            <tr>
                <td class="text-center"><?php echo $i; ?></td>
                <td class="text-center"><?php echo $item["item_dieno"]; ?></td>
                <td class="text-center"><?php echo $item["item_length"]; ?></td>
                <td class="text-center"><?php echo $item["quantity"]; ?></td>
                <?php if($i == 1) { ?><td class="text-center" rowspan="<?php echo count($itemmodel); ?>"></td><?php } ?>
                <td class="text-right"><?php echo Controller::money_format_inr($item["rate"],2); ?></td>
                <td class="text-right"><?php echo Controller::money_format_inr($item["amount"],2); ?></td>
                <td class="text-center"><?php echo $item["remark"]; ?></td>
            </tr>
        <?php
                $i  = $i + 1;
            }
        ?>
            <tr>
            <th colspan="3"></th>
            <th ><?= $tot_qty?></th>
                <th colspan="2" class="text-right">GRAND TOTAL</th>
                <th class="text-right"><?php echo Controller::money_format_inr($model->total_amount,2); ?></th>
                <th></th>
            </tr>
        <?php
        } else { 
        ?>
        <tr><td colspan="7">No items were found.</td></tr>
        <?php } ?>
    </table>
	
	
	
    <h4>Authorized Signatory,</h4>
    <h4>For <?php echo Yii::app()->name; ?></h4>
    <p>Please make cash payment.</p>
    <br/><br/>
</div>


