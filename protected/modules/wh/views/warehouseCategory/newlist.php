<style>
    
    .error {
        color: #ff0000;
    }
    .flash_msg{
       width: 335px;
       position: absolute;
       right: 45px;
       top: 44px;
       padding: 8px 15px;
    }
    
    .dpdwntab{
        position:relative;
    }

    .exp-list {
    padding-left: 0px !important;
}

.error {
    color: #ff0000;
}
.specoption {
    display: inline;
}
#dieno {
    display: none;
}
    
    
</style>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/validation/additional-methods.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery.dataTables.min.css">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.dataTables.min.js"></script>

<?php

function fetchCategoryTree($parent = 'NULL', $spacing = '', $user_tree_array = '') {

    if (!is_array($user_tree_array))
        $user_tree_array = array();
    $tblpx = Yii::app()->db->tablePrefix;
    if ($parent == 'NULL') {
        $category = Yii::app()->db->createCommand("SELECT id , category_name FROM {$tblpx}warehouse_category WHERE parent_id IS NULL AND type !='S'")->queryAll();
    } else {
        $category = Yii::app()->db->createCommand("SELECT id , category_name FROM {$tblpx}warehouse_category WHERE  parent_id='" . $parent . "' AND type !='S'")->queryAll();
    }
    //$category = Yii::app()->db->createCommand("SELECT id , category_name FROM {$tblpx}purchase_category WHERE parent_id".$query." AND type !='S'")->queryAll();
    foreach ($category as $key => $value) {
        $user_tree_array[] = array("id" => $value['id'], "name" => $spacing . $value['category_name']);
        $user_tree_array = fetchCategoryTree($value['id'], $spacing . '&nbsp;&nbsp; -', $user_tree_array);
    }

    return $user_tree_array;
}

$categoryList = fetchCategoryTree();


$tblpx = Yii::app()->db->tablePrefix;
$unit = Yii::app()->db->createCommand("SELECT id , unit_name FROM {$tblpx}unit WHERE company_id=" . Yii::app()->user->company_id . "")->queryAll();
$brand = Yii::app()->db->createCommand("SELECT id , brand_name FROM {$tblpx}brand WHERE company_id  =" . Yii::app()->user->company_id . "")->queryAll();



$this->breadcrumbs = array(
    'Purchase List',)
?>
<div class="container" id="project">
    <h2>Warehouse Items</h2>

    <?php // $this->renderPartial('_search', array('model' => $model)) ?>
    <div class="add-btn">

    </div>
    <div class="dpdwntab">
        <ul class="nav nav-pills" id="myTab">
            <li class="active"><a data-toggle="tab" href="#viewitem" class="btnadd table-view">Item List</a></li>
            <li><a data-toggle="tab" href="#viewbrand" class="btnadd table-view">Brand List</a></li>
            <li><a data-toggle="tab" href="#viewspec"  class="btnadd table-view">Specification List</a></li>
            <li><a href="<?php echo  Yii::app()->createAbsoluteUrl('WarehouseCategory/exportItemToCSV'); ?>"  class="btnadd table-view">Export to CSV</a></li>
        </ul>
        
        <?php
		foreach(Yii::app()->user->getFlashes() as $key => $message) {
                    echo '<div class="flash-' . $key . ' alert alert-success flash_msg" style=" width:335px;"> <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>' . $message . "</div>\n";
		}
		?>

        <div class="tab-content">
            <div id="viewitem" class="tab-pane in active">
                <h4 class="text-uppercase">Item List</h4>
                <span class="error_message"></span>
               <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'purchase-category-form',
                    'htmlOptions' => array('class' => 'form-inline',),
                    'action' => Yii::app()->createUrl('WarehouseCategory/create'),
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                        'validateOnChange' => true,
                        'validateOnType' => false,),
                ));
                ?>
                    <div class="form-group">
                        <select name="WarehouseCategory[parent_id]" id="WarehouseCategory_parent_id"  class="form-control">
                            <option value="">Choose Item</option>
                            <?php
                            foreach ($categoryList as $key => $value) {
                                ?>	
                                <option value="<?php echo $value['id']; ?>" ><?php echo $value['name']; ?></option>
                                <?php
                            }
                            ?>

                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="WarehouseCategory_category_name" name="WarehouseCategory[category_name]" placeholder="Item Name">
                       <?php echo $form->error($model, 'category_name'); ?>
                        <input type="hidden" id="category_id" name="category_id">
                    </div>
                   
                
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Save'); ?> 
                    <?php echo CHtml::resetButton('Cancel'); ?>
                 <?php $this->endWidget(); ?>




                <div class="cat_common">
                    <div id="msg_box"></div>
                    <div class="exp-list_cat">
                        <?php
                        $this->widget('zii.widgets.CListView', array(
                            'dataProvider' => $dataProvider,
                            'itemView' => '_newview', 'template' => '<div>{sorter}</div><div class="container1"><table cellpadding="10" id="itemtable" class="table  list-view sorter">{items}</table></div>{pager}',
                        ));
                        ?>

                    </div>
                </div>
            </div>
            
            <div id="viewbrand" class="tab-pane fade">

                <h4 class="text-uppercase">Brand List</h4>
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'brand-form',
                    'htmlOptions' => array('class' => 'form-inline',),
                    'action' => Yii::app()->createUrl('Brand/create1'),
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                        'validateOnChange' => true,
                        'validateOnType' => false,),
                ));
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputPassword3">Brand</label>
                        <input type="text" class="form-control" id="Brand_brand_name" name="Brand[brand_name]" placeholder="Brand">
                        <?php echo $form->error($model2, 'brand_name'); ?> 
                        <input type="hidden" id="brand_id" name="brand_id">
                    </div>
                    <?php echo CHtml::submitButton($model2->isNewRecord ? 'Save' : 'Save'); ?> 
                    <?php echo CHtml::resetButton('Cancel'); ?>
                 <?php $this->endWidget(); ?>


                <div class="cat_common">
                    <div id="msg_box"></div>
                    <div class="exp-list_cat">
                        <?php
                        $this->widget('zii.widgets.CListView', array(
                            'dataProvider' => $dataProvider3,
                            'itemView' => '_brand', 'template' => '<div>{sorter}</div><div class="container1"><table id="brandtable" cellpadding="10" class="table  list-view sorter">{items}</table></div>{pager}',
                        ));
                        ?>

                    </div>

                </div>
            </div>

            <div id="viewspec" class="tab-pane fade">
<h4 class="text-uppercase">Specification List</h4>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . ' alert alert-success" style=" width:335px;">' . $message . "</div>\n";
    }
?>


<!--<form class="form-inline" id="form_specification">-->
<?php
    $form = $this->beginWidget('CActiveForm', array(
        'id'=>'purchase-category-test',
        'htmlOptions'=>array( 'class'=>'form-inline', ),
        'htmlOptions' => array('class'=>'form-inline', 'enctype' => 'multipart/form-data'),
       'action' => Yii::app()->createUrl('WarehouseCategory/createspecification'),
        'enableAjaxValidation' => true,
        'enableClientValidation'=>true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
            
    ));
    ?>
  <div class="form-group">
   <select name="WarehouseCategory[parent_id]" id="WarehouseCategory_parent_id" class="WarehouseCategory_parent_id form-control">
        <option value="">Choose Item</option>
        <?php  foreach($categoryList as $key => $value){   ?>  

                <option value="<?php echo $value['id']; ?>" ><?php echo $value['name']; ?></option>

                <?php  }  ?>

</select>
 <?php echo $form->error($model,'parent_id'); ?>
  </div>
  
  <div class="form-group">
   <select name="WarehouseCategory[brand_id]" id="WarehouseCategory_brand_id"  class="form-control">
        <option value="">Choose Brand</option>
        <?php
        foreach($brand as $key => $value){
                ?>  
                <option value="<?php echo $value['id']; ?>" ><?php echo $value['brand_name']; ?></option>
                <?php       
    }       
        ?>

</select>
<?php echo $form->error($model,'brand_id'); ?>
  </div>
<div class="form-group">
    <select name="WarehouseCategory[specification_type]" id="WarehouseCategory_specification_type" 
    class="WarehouseCategory_specification_type form-control">
    <option value="O">By Quantity</option>
    <option value="A">By Length</option>
    <option value="G">By Width * Height</option>
    </select>
    <?php echo $form->error($model,'specification_type'); ?>
</div>
<div id="dieno" class="specoption">
    <div class="form-group">
        <input type="text" class="form-control" id="WarehouseCategory_dieno" name="WarehouseCategory[dieno]" placeholder="Profile/Die No">
        <?php echo $form->error($model,'dieno'); ?>
    </div>

</div>
<div id="specification" class="specoption">
    <div class="form-group">
        <input type="text" class="form-control" id="WarehouseCategory_specification" name="WarehouseCategory[specification]" placeholder="Specification">
        <?php echo $form->error($model,'specification'); ?>
    </div>
</div>
    <div class="form-group">
    <label class="sr-only" for="exampleInputPassword3">Password</label>
    <select name="WarehouseCategory[unit]" id="WarehouseCategory_unit"  class="form-control">
            <option value="">Choose Unit</option>
            <?php
            foreach($unit as $key => $value){
                    ?>  
                    <option value="<?php echo $value['id']; ?>" ><?php echo $value['unit_name']; ?>
                        
                    </option>
                    <?php       
        }       
            ?>

    </select>
    <?php echo $form->error($model,'unit'); ?>
        <input type="hidden" id="specification_id" name="specification_id">
  </div>
    <?php
    foreach (Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="message ' . $key . '">' . $message . "</div>\n";
    }
    ?>
    <div class="form-group">
    <!-- <input type="file" class="" id="WarehouseCategory_filename" name="WarehouseCategory[filename]" placeholder="filename"> -->
    <?php echo $form->fileField($model, 'filename'); ?>
    <?php echo $form->error($model,'filename'); ?>
        <div class="img_msg"></div>
  </div>
    <div class="img_div"></div>
                <br/><br/>
                <?php echo CHtml::SubmitButton($model->isNewRecord ? 'Save' : 'Save', array("id" => "btnSpecification")); ?> 
                <?php echo CHtml::resetButton('Cancel',array('class'=>'reset')); ?>
<?php $this->endWidget(); ?>

                <div id="common">
                    <div id="msg_box"></div>
                    <div class="exp-list_spc">

                        <?php
                        $this->widget('zii.widgets.CListView', array(
                            'dataProvider' => $dataProvider1,
                            'itemView' => '_specification', 'template' => '<div>{sorter}</div><div class="container1"><table id="spectable" cellpadding="10" class="table  list-view sorter">{items}</table></div>{pager}',
                        ));
                        ?>


                    </div>


                </div>
            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview">
      </div>
    </div>
  </div>
</div>


<div id='FlickrGallery'></div>



<style>
    a.button {
        background-color: #6a8ec7;
        display: block;
        padding: 5px;
        color: #fff;
        cursor: pointer;
        float: right;
        border: 1px solid #6a8ec8;
    }
    .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover{
        color: #fff !important;
    }
    .dataTables_wrapper .dataTables_paginate .paginate_button:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active  {
        color:#fff !important;
    }
    .list-view .sorter {margin: 0px;}
    h4{
        background-color: #fafafa;
        padding: 8px;

    }
    .imagepreview{
        display: block;
        margin-left: auto;
        margin-right: auto;
        max-width: 100%;
    }
</style>
<script>
	$(document).ready(function(){
				
	$('a[data-toggle="tab"]').on('click', function(e) {
			window.localStorage.removeItem("activeTab");
            window.localStorage.setItem('activeTab', $(e.target).attr('href'));
        });
        var activeTab = window.localStorage.getItem('activeTab');
        if (activeTab) {
            $('#myTab a[href="' + activeTab + '"]').tab('show');
           // window.localStorage.removeItem("activeTab");
        }

        $("#WarehouseCategory_specification_type").change(function() {
        var specType = $("#WarehouseCategory_specification_type").val();
        //alert(specType);
        if(specType == "A") {
            $("#dieno").css("display","inline");
            $("#specification").css("display","none");
            $("#WarehouseCategory_specification").val("");
        } else {
            $("#dieno").css("display","none");
            $("#specification").css("display","inline");
            $("#WarehouseCategory_specification").val("");
        }
    });


	});
	
<?php
Yii::app()->clientScript->registerScript('myjavascript', '
     $(document).ready(function(){
     
       $("#itemtable").dataTable( {  
	} );
	$("#brandtable").dataTable( {
	} );
	$("#spectable").dataTable( {
	} );
       
        
	});
        
    ');
?>
$("a[data-toggle=\'tab\']").on("shown.bs.tab", function(e){
            $($.fn.dataTable.tables(true)).DataTable()
               .columns.adjust();
         });
    $(".editItems").click(function (e) {
            var catId = $(this).attr("data-id");
            $("#category_id").val(catId);
            $("#btn_category").text("Update");
            $.ajax({
                url:'<?php echo  Yii::app()->createAbsoluteUrl('WarehouseCategory/getItemForEdit'); ?>',
		type:'GET',
		data:{category:catId},
		async:true,
                success: function (response)
                {
                    var result = JSON.parse(response);
                    $("#WarehouseCategory_parent_id").val(result["parent"]);
                    $("#WarehouseCategory_category_name").val(result["name"]);
                }
            });
        });
        
        
        $(".editBrands").click(function (e) {
            var brandId = $(this).attr("data-id");
            var $tds    = $(this).closest('tr').find('td');
            var brand   = $tds.eq(1).text().trim();
            $("#brand_id").val(brandId);
            $("#btn_brand").text("Update");
            $("#Brand_brand_name").val(brand);
        });
        $(document).on('click','.editSpecification',function(){
        // $(".editSpecification").click(function (e) {
            var specificationId = $(this).attr("data-id");
            $("#specification_id").val(specificationId);
            $("#btnSpecification").text("Update");
            $.ajax({
                url:'<?php echo  Yii::app()->createAbsoluteUrl('WarehouseCategory/getSpecificationForEdit'); ?>',
		type:'GET',
		data:{specification:specificationId},
		async:true,
                success: function (response)
                {
                    var result          = JSON.parse(response);
                    var parent          = result["parent"];
                    var brand           = result["brand"];
                    var type            = result["type"];
                    var specification   = result["specification"];
                    var dieno           = result["dieno"];
                    var unit            = result["unit"];
                    var imd_path        = result["imd_path"];

                    $(".WarehouseCategory_parent_id").val(parent);
                    $("#WarehouseCategory_brand_id").val(brand);
                    $(".WarehouseCategory_specification_type").val(type);
                    if(imd_path){
                        $(".img_div").show();
                        $(".img_div").html(imd_path);
                    }else{
                        $(".img_div").html("");
                    }
                    $(".img_msg").show();
                    $('.img_msg').html('');
                    $("#WarehouseCategory_filename").val("");
                    $('#btnSpecification').removeAttr('disabled');
                    if(type == "A") {
                        $("#dieno").show();
                        $("#dieno").css("display","inline");
                        $("#specification").hide();
                        $("#WarehouseCategory_dieno").val(dieno);
                        //$("#WarehouseCategory_length").val(length);
                        $("#WarehouseCategory_specification").val(dieno);
                    } else {
                        $("#dieno").hide();
                        $("#specification").show();
                        $("#specification").css("display","inline");
                        $("#WarehouseCategory_specification").val(specification);
                        $("#WarehouseCategory_dieno").val("");
                        //$("#WarehouseCategory_length").val("");
                    }
                    $("#WarehouseCategory_unit").val(unit);
                }
            });
        });
        
        $('.flash_msg').fadeOut(4000);
        
        $(document).on('click','.img_delete', function(){
            var id = $('.img_delete').attr('id')
            var element = $(this);
            var answer = confirm("Are you sure you want to delete?");
            if (answer)
            {
                $.ajax({
                    url:'<?php echo Yii::app()->createAbsoluteUrl('WarehouseCategory/img_delete') ?>',
                    type:'GET',
                    data:{id:id},
                    dataType:'json',
                    async:true,
                    success:function(result){
                        if(result.status == 1){
                            element.closest(".img_div").hide();
                             $('#spectable tr').each(function() {
                                var id = $('.img_delete').attr('id')
                                var y =  $(this).find('.imd_div2').attr('id');
                                if(y === id){
                                    $(this).find('.imd_div2 img').attr('src',result.path);
                                }
                            });
                        }
                    }
                })

            }else{
                return false;
            }
        })



        var a=0;
        var result = $('.img_msg');
        //binds to onchange event of your input field
        $('#WarehouseCategory_filename').bind('change', function() {
        var ext = $('#WarehouseCategory_filename').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['gif','png','jpg','jpeg']) == -1){
            $('.img_msg').html('<span style="color: red; ">Invalid Image Format! Image Format Must Be JPG, JPEG, PNG or GIF.</span>');
            a=0;
            }else{
            var picsize = (this.files[0].size);
            if (picsize > 2000000){
                $('.img_msg').html('<span style="color: red; ">Maximum File Size Limit is 2MB.</span>');
            a=0;
            }else{
            a=1;
            $('.img_msg').html('');
            }
            if (a==1){
                $('#btnSpecification').removeAttr('disabled');
                }else{
                    $('#btnSpecification').attr('disabled','disabled');
                }
        }
        });
    
        $('.reset').click(function(){
            $('#btnSpecification').removeAttr('disabled');
            $('.img_msg').html('');
            $(".img_div").html('');
        })

        $(function() {
          $(document).on('click','.pop',function(){
              var src = $(this).attr('src');
              var link = src.replace('thumbnail/','');
              $('.imagepreview').attr('src', link);
              $('#imagemodal').modal('show');   
          });     
        });
</script>

<style>
    .container1 {
        width: 100%;
        /*        height: 330px;*/
        overflow: auto;
        position: relative;
    }

    th,
    .top-left {
        background: #eee;
    }
    .top-left {
        box-sizing: border-box;
        position: absolute;
        left: 0;
        border-right: 1px solid #ccc;
        border-bottom: 1px solid #ccc;
    }

    table {

        border-spacing: 0;
        /*	position: absolute;*/

    }
    th,
    td {
        border-right: 1px solid #ccc !important;

    }
    .img_div{
        position: absolute;
        width: 30px;
        width: 100px;
        margin-bottom: 10px;
        right: 0px;
        bottom: 50px;
    }
    
    #purchase-category-test{
        position: relative;
        margin-bottom: 25px;
    }
    .img_msg{
        color:red;
    }
    @media(min-width: 768px){
        .img_div{
            right: 11%;
            width: 30px;
            top: 48px;
            width: 100px;
            margin-bottom: 10px;
        }
    }

</style>
