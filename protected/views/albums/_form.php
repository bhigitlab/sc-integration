

<?php
/* @var $this AlbumsController */
/* @var $model Albums */
/* @var $form CActiveForm */
?>

<div class="modal-body">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
       'id'=>'albums-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>


<div class="clearfix">
    <div class="row addRow">
        <div class="col-md-12">
            <?php echo $form->labelEx($model, 'title'); ?>
            <?php echo $form->textField($model, 'title', array('size' => 66, 'maxlength' => 100)); ?>
            <?php echo $form->error($model, 'title'); ?>
        </div>
        <div class="col-md-12">
            <?php echo $form->labelEx($model, 'projectid'); ?>
			<?php echo $form->dropDownList($model, 'projectid', CHtml::listData(Projects::model()->findAll(array('order' => 'name ASC')), 'pid', 'name'), array('empty' => '-Choose a Project-')); ?>
			<?php echo $form->error($model, 'projectid'); ?>
        </div>
        <div class="col-md-12">
            <?php echo $form->labelEx($model, 'description'); ?>
            <?php echo $form->textArea($model, 'description'); ?>
            <?php echo $form->error($model, 'description'); ?>
        </div>
        
        
 </div>
   <br />
    <div class="modal-footer save-btnHold">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?> 
        <?php if (Yii::app()->user->role == 1) { 
        	if(!$model->isNewRecord){  /*
        	?> 
            <a class="btn del-btn" href="<?php echo $this->createUrl('clients/deleteclients', array('id' => $model->cid)) ?>" class="deletebtn" onclick="return confirm('Are you sure that you want to delete?');">Delete </a>                           
        <?php */ } } ?>
        <button data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
    </div>

    <?php $this->endWidget(); ?>
</div>
</div><!-- form -->
