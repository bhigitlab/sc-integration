

<?php
/* @var $this WorkTypeController */
/* @var $model WorkType */
/* @var $form CActiveForm */
?>

<div class="">
    <?php
       $form = $this->beginWidget('CActiveForm', array(
        'id'=>'daily-work-type-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => false,),
    ));
    ?>

    <!-- Popup content-->



    <div class="panel-body">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                   <?php echo $form->labelEx($model,'work_type'); ?>
                    <?php echo $form->textField($model,'work_type',array('class'=>'form-control','size'=>30,'maxlength'=>30)); ?>
                    <?php echo $form->error($model,'work_type'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="section_class">
                <div class="col-md-3">
                    <div class="form-group">
                   <?php echo $form->labelEx($model,'labour_label'); ?>
                    <?php echo $form->textField($model,'labour_label',array('class'=>'form-control text_field','size'=>30,'maxlength'=>30)); ?>
                    <?php echo $form->error($model,'labour_label'); ?>
                    </div>
                </div>
                <div class="col-md-1">
                <?php echo $form->checkBox($model,'labour_status',array('class'=> 'popermission')); ?>
                </div>
                </div>
                <div class="section_class">
                <div class="col-md-3">
                    <div class="form-group">
                   <?php echo $form->labelEx($model,'wage_label'); ?>
                    <?php echo $form->textField($model,'wage_label',array('class'=>'form-control text_field','size'=>30,'maxlength'=>30)); ?>
                    <?php echo $form->error($model,'wage_label'); ?>
                    </div>
                </div>

                <div class="col-md-1">
                <?php echo $form->checkBox($model,'wage_status',array('class'=> 'popermission')); ?>
                </div>
                </div>
                <div class="section_class">
                <div class="col-md-3">
                    <div class="form-group">
                   <?php echo $form->labelEx($model,'wage_rate_label'); ?>
                    <?php echo $form->textField($model,'wage_rate_label',array('class'=>'form-control text_field','size'=>30,'maxlength'=>30)); ?>
                    <?php echo $form->error($model,'wage_rate_label'); ?>
                    </div>
                </div>
                <div class="col-md-1">
                <?php echo $form->checkBox($model,'wagerate_status',array('class'=> 'popermission')); ?>
                </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-3">
                    <div class="form-group">
                   <?php echo $form->labelEx($model,'helper_label'); ?>
                    <?php echo $form->textField($model,'helper_label',array('class'=>'form-control text_field','size'=>30,'maxlength'=>30)); ?> 
                    <?php echo $form->error($model,'helper_label'); ?>
                    </div>
                </div>
                <div class="col-md-1">
                <?php echo $form->checkBox($model,'helper_status',array('class'=> 'popermission')); ?>
                </div>
       
                <div class="col-md-3">
                    <div class="form-group">
                   <?php echo $form->labelEx($model,'helper_labour_label'); ?>
                    <?php echo $form->textField($model,'helper_labour_label',array('class'=>'form-control text_field','size'=>30,'maxlength'=>30)); ?>
                    <?php echo $form->error($model,'helper_labour_label'); ?>
                    </div>
                </div>
                <div class="col-md-1">
                <?php echo $form->checkBox($model,'helperlabour_status',array('class'=> 'popermission')); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                   <?php echo $form->labelEx($model,'amount_label'); ?>
                    <?php echo $form->textField($model,'amount_label',array('class'=>'form-control text_field','size'=>30,'maxlength'=>30)); ?>
                    <?php echo $form->error($model,'amount_label'); ?>
                    </div>
                </div>
                
                <div class="col-md-1">
                    <?php echo $form->checkBox($model,'amount_status',array('class'=> 'popermission')); ?>
                </div>
            </div>


            <div class="row">
                <div class="col-md-6">
                    <div class="save-btnHold">
                        <label style="display:block;">&nbsp;</label>
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-info btn-sm')); ?>
                        <?php echo CHtml::ResetButton('Reset', array('class'=>'btn btn-default btn-sm')); ?>
                        <?php echo CHtml::ResetButton('Close', array('onclick'=>'closeaction(this,event);','class'=>'btn btn-sm'));  ?>

                    </div>
                </div>
            </div>
        </div>
<?php $this->endWidget(); ?>
</div><!-- form -->

<script>
$('.text_field[type="text"]').change(function () {
   if($(this).val() !=''){
    $(this).closest('.section_class').find('input[type=checkbox]').prop('checked', true);
   }
}).change()

$('input[type="checkbox"]').change(function () {
    if (!this.checked) {
        $(this).closest('.section_class').find('input[type=text]').val('');
    }
}).change()
</script>
    
    
<style>

.popermission{
    margin-top: 30px !important;
}
</style>    